package pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

public class pagefactory_PO extends LoadableComponent<pagefactory_PO> {

	WebDriver driver;
	
	@FindBy(xpath="//input[@id='email']")
	WebElement fb_username;
	
	@FindBy(name="pass")
	WebElement fb_password;
	
	@FindBy(xpath="//input[@id='u_0_2']")
	WebElement fb_clcikonloginbutton;
	
	public pagefactory_PO(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void fb_login(String username,String password) {
		fb_username.clear();
		fb_username.sendKeys(username);
		fb_password.clear();
		fb_password.sendKeys(password);
		fb_clcikonloginbutton.click();
	}
	
	@Override
	protected void isLoaded() throws Error {
		Assert.assertEquals(driver.getTitle(),"fb title");
		
	}

	@Override
	protected void load() {
		//this.get().
		
	}

}
