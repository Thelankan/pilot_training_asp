package webdriverframework.pageobjectdesignpattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

public class FaceBookPO extends LoadableComponent <FaceBookPO>{

	WebDriver driver;
	
	@FindBy(xpath = ".//*[@id='email']")
	WebElement username;
	
	@FindBy(id="pass")
	WebElement password;
	
	
	
	public FaceBookPO(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	
	
	public void SignIn(String user, String pwd) {
		
		username.sendKeys(user);
		password.sendKeys(pwd);
		
	}
	
	
	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		
	}
	
	
	

}
