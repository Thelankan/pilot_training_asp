package webdriverframework.pageobjectdesignpattern;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FaceBookTest {

	WebDriver driver;
	FBPO fb;
	
	@BeforeMethod
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "test\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		fb = new FBPO(driver);
		fb.load();
	}
	
	@Test
	public void fb_loginTest1() {
		fb.fb_login("user1", "pass1");
		assertTrue(true);
	}
	
	@Test
	public void fb_loginTest2() {
		fb.fb_login("user12", "pass12");
	}
	
}
