package webdriverframework.pageobjectdesignpattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

public class FBPO extends LoadableComponent<FBPO>{

	WebDriver driver;
	
	@FindBy(xpath = ".//*[@id='email']")
	WebElement fb_username;
	
	@FindBy(name = "pass")
	WebElement fb_password;
	
	@FindBy(id = "u_0_3")
	WebElement fb_loginBtn;
	
	public FBPO(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);	
	}
	
	public void fb_login (String u, String p) {
		fb_username.clear();
		fb_username.sendKeys(u);
		fb_password.clear();
		fb_password.sendKeys(p);
		
		fb_loginBtn.click();	
	}
	
	@Override
	protected void isLoaded() throws Error {
		Assert.assertEquals(driver.getTitle(), "Facebook � log in or sign up");
		
	}

	@Override
	protected void load() {
		driver.get("https://www.facebook.com/");
		
	}
	
	
	

}
