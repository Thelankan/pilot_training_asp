package webdriverframework.pageobjectdesignpattern;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

public class YahooMailTest {
	WebDriver driver;
	YahooMailLoginPO myYahoo;
	YahooInboxPO myInbox;

	@Test(description ="valid test-data")
	public void yahooEmail() throws IOException, InterruptedException {
		myInbox = myYahoo.yahooSignIn("seleniumtest777@yahoo.com", "anjani123");
		myInbox.composeEmail("test1@ymail.com", "test message", "Hi,\nThis is test message.\n\nThanks");	
	}
	
	@Test(description ="invalid test-data")
	public void yahooEmail2() throws IOException, InterruptedException {
		myInbox = myYahoo.yahooSignIn("seleniumtest77712@yahoo.com", "anjani12312");
		myInbox.composeEmail("test1@ymail.com", "test message", "Hi,\nThis is test message.\n\nThanks");	
	}


	@BeforeTest
	public void beforeTest() throws InvalidFormatException, IOException {
		System.setProperty("webdriver.chrome.driver",
				"test\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		// PageFactory.initElements(driver, this);
		myYahoo = new YahooMailLoginPO(driver);
		myYahoo.load();
	}

	@AfterTest
	public void afterTest() {
		myInbox.close();
	}

}
