package ApachePOI.SeleniumTestNG;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class seleniumBrowserTests {

		private WebDriver driver;	

		@Parameters({"browser", "driverPath"})
		@BeforeTest
		public void setup(String browser, String driverPath) throws Exception {
			
			System.out.println("You are testing on browser " + browser);
			if (browser.equals("chrome")) {			
				System.setProperty("webdriver.chrome.driver", driverPath);
				driver = new ChromeDriver();
				driver.manage().window().maximize();
				} 
			else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", driverPath);
				driver = new FirefoxDriver();
				driver.manage().window().setSize(new Dimension(1920, 1080));
				} 
			else if (browser.equals("IE")) {			
				System.setProperty("webdriver.ie.driver", driverPath);
				driver = new InternetExplorerDriver();
				driver.manage().window().maximize();
				} 
		}

		@DataProvider
		private Object[][] searchStrings() {
			return new Object[][] { { "TestNG" }, { "Selenium" }, { "framework" } };
		}
		
		@Test(dataProvider = "searchStrings")
		public void searchGoogle(final String searchKey) {
			
			System.out.println("Search " + searchKey + " in google");
			driver.manage().window().maximize();
			driver.get("http://www.google.com");
        	try {
				Thread.sleep(4000);
				WebElement element = driver.findElement(By.id("lst-ib"));
				System.out.println("element gettext" +element.getText());
				element.sendKeys(searchKey);
				Thread.sleep(1000);

				driver.findElement(By.name("btnG")).click();
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            String title = driver.getTitle();
            Assert.assertEquals(title, searchKey +" - Google Search");
		}

		@AfterTest
		public void quitDriver() throws Exception {
			driver.quit();
		}	
}
