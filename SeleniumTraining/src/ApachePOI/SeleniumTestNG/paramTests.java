package ApachePOI.SeleniumTestNG;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class paramTests {
	
	WebDriver driver;
	
	
	@BeforeTest
	public void setup() {
		
	}

	
	@Parameters({"firstname", "lastname"})
	@Test
	public void testcase1(String fn, String ln) {
		
		System.out.println("my firstname is: " +fn + "and last name is" +ln);
		
	}
	
	@AfterTest
	public void closeIt() {
		
	}
}
