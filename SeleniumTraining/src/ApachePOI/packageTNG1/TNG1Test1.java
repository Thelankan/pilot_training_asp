/*
 * This file is created to create examples of 
 * various configuration possible using TestNG configuration files
 */
package ApachePOI.packageTNG1;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

public class TNG1Test1 {
  @Test
  public void TNG1Test11() {
	  System.out.println("This is a the first test in TNG1Test1 Class");
	  assertTrue(false);
  }
  
  @Test
  public void TNG1Test12() {
	  System.out.println("This is a the second test in TNG1Test1 Class");
  }
  
  @Test(expectedExceptions = ArithmeticException.class)
  public void excep() {
	 int i = 1000/10;
	 System.out.println(i);
  }
  
}
