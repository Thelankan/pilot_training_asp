package calcbmi.com;

import static org.testng.Assert.fail;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.Assert;

public class Runnablefile {

	private static final boolean flase = false;
	WebDriver driver;


	@Parameters({"browser","driverpath"})	
	@BeforeClass(alwaysRun=true)
	public void beforeclass(String browsername,String driverpath) {

		System.out.println("Browsername"+browsername);

		if (browsername.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver",driverpath);
			driver=new FirefoxDriver();
			driver.manage().window().setSize(new Dimension(1920, 1080));
		}

		else if (browsername.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",driverpath);
			driver=new ChromeDriver();
			driver.manage().window().maximize();
		}

		else if (browsername.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver",driverpath);
			driver=new InternetExplorerDriver();
			driver.manage().window().maximize();
		}

		//Entering the url
		driver.get("http://calcbmi.com/");
		 

	}

	@BeforeMethod
	public void beforemethod() {

		System.out.println("before method");	

	}

	@Test(description="testcase1",groups= {"Regression","Sanity"})
	public void TC01_calculateBMI() {

		System.out.println("TC01 executed");	
		//click on login button
		driver.findElement(By.id("cm")).sendKeys("168");
		driver.findElement(By.id("kg")).sendKeys("67");
		driver.findElement(By.id("sm")).click();

		driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);


		String tagname=driver.findElement(By.id("cm")).getTagName();

		System.out.println("Tagneme:- "+tagname);

		String attributename=driver.findElement(By.id("kg")).getAttribute("id");
		System.out.println("attribute Id:- "+attributename);

		driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);
		//asserting bmi
		Assert.assertEquals("BMI",driver.findElement(By.xpath("//table[@cellspacing=5]//tr//acronym[@title='Body Mass Index']")).getText());


	}

	@Test(description="testcase1",groups= {"Regression"})
	public void TC02_radiobutton() {

		System.out.println("TC02 executed");	
		//click on login button
		boolean calculatebmiradio=driver.findElement(By.id("ctw")).isDisplayed();
		System.out.println("Calculate BMI radio button calculatebmiradio displayed:- "+calculatebmiradio);

		boolean calculatebmiradioenabled=driver.findElement(By.id("ctw")).isEnabled();
		System.out.println("Calculate BMI radio button calculatebmiradioenabled :- "+calculatebmiradioenabled);

		boolean calculatebmiradioselected=driver.findElement(By.id("ctw")).isSelected();
		System.out.println("Calculate BMI radio button calculatebmiradioselected:- "+calculatebmiradioselected);

//		fail("soem message");

	}

	@Test(enabled=false)
	public void Tc03_cssValuecolor() {

		System.out.println("TC03 executed");	
		String color=driver.findElement(By.id("sm")).getCssValue("background-color");
		System.out.println("Css background Color:- "+color);

	}

	@Test(groups="Sanity")
	public void Tc04_backgroundcolor() {

		System.out.println("TC04 executed");	
		String color=driver.findElement(By.id("sm")).getCssValue("background-color");
		System.out.println("Css background Color:- "+color);

	}


	@AfterMethod
	public void aftermethod() {

		System.out.println("After method");

	}

	@AfterClass
	public void afterclass() {

		driver.quit();
	}



}
