package calcbmi.com;

import static org.testng.Assert.fail;

import java.io.File;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.KeyUpAction;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;



public class Webelepract {
	
WebDriver driver;	
	
@BeforeClass
public void beforeclass() {
	
	System.setProperty("webdriver.gecko.driver","Test\\Resources\\geckodriver.exe");
	//webdriver
	driver=new FirefoxDriver();
	//Entering the url
	driver.get("http://calcbmi.com/");
	driver.manage().window().maximize();
	
}
	
@BeforeMethod
public void beforemethod() {
		
System.out.println("before method");	

	
}


@Test
public void Takescreenshot() {
	
File scr=((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

try {
	
	//FileUtils.copyFile(scr,new File("Test/Resources" + System.currentTimeMillis() + ".png"));
	
	FileUtils.copyFile(scr,new File("C:/TrainingSeleniumNew/Screenshot_new/" + System.currentTimeMillis() + ".png"));
	
	
	
} catch (Exception e) {
	System.out.println(e.getMessage());
}
	
}

/*//Firefox profile
@Test
public void Firefoxprofile() {
	
	ProfilesIni myprof=new ProfilesIni();	
	FirefoxProfile MyProfile=myprof.getProfile("VinayLankaff");
	MyProfile.setPreference("browser.startup.homepage","http://calcbmi.com/");
	MyProfile.setAcceptUntrustedCertificates(true);
	
	FirefoxOptions firefoxoptions=new FirefoxOptions();
	firefoxoptions.setProfile(MyProfile);
	
	driver=new FirefoxDriver(firefoxoptions);
	
}

/*
//Using actions class
@Test
public void keys() {

	driver.get("https://www.google.com/");
	Actions act=new Actions(driver);
	//Method one		
	act.keyDown(driver.findElement(By.id("lst-ib")),Keys.SHIFT)
	.sendKeys("google");
	
	Action actbuild=act
	.build();
	actbuild.perform();

//	//Method two 
//	act.sendKeys(Keys.ARROW_DOWN);
	
}*/


/*//HandlingPopups (Code for Naukari.com)
@Test
public void HandlingPopups() {
	
driver.navigate().to("https://www.naukri.com/");
String parenthandle=driver.getWindowHandle();
System.out.println("Parent Handle:-" + parenthandle);
Set<String> allhandles=driver.getWindowHandles();
System.out.println("All handles:-"+ allhandles);
for (String s1:allhandles) {

	if (!s1.equals(parenthandle)) {
		driver.switchTo().window(s1);
		driver.close();
	}
	
}

driver.switchTo().window(parenthandle);

////Using Iterator
//Iterator<String> i1=allhandles.iterator();
//
//if(i1.hasNext()) {
//	System.out.println(i1.next());
//	
//	if(!i1.equals(parenthandle)) {
//		driver.close();
//	}
//	
//}

System.out.println("Need to practice frames and alearts");
	
}*/


/*@Test
public void TC01_calculateBMI() {
	
	
//click on login button
driver.findElement(By.id("cm")).sendKeys("168");
driver.findElement(By.id("kg")).sendKeys("67");
driver.findElement(By.id("sm")).click();

driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);


String tagname=driver.findElement(By.id("cm")).getTagName();

System.out.println("Tagneme:- "+tagname);

String attributename=driver.findElement(By.id("kg")).getAttribute("id");
System.out.println("attribute Id:- "+attributename);

driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);
//asserting bmi
Assert.assertEquals("BMI",driver.findElement(By.xpath("//table[@cellspacing=5]//tr//acronym[@title='Body Mass Index']")).getText());
	
			
}

@Test
public void TC02_radiobutton() {
	
//click on login button
boolean calculatebmiradio=driver.findElement(By.id("ctw")).isDisplayed();
System.out.println("Calculate BMI radio button calculatebmiradio displayed:- "+calculatebmiradio);

boolean calculatebmiradioenabled=driver.findElement(By.id("ctw")).isEnabled();
System.out.println("Calculate BMI radio button calculatebmiradioenabled :- "+calculatebmiradioenabled);

boolean calculatebmiradioselected=driver.findElement(By.id("ctw")).isSelected();
System.out.println("Calculate BMI radio button calculatebmiradioselected:- "+calculatebmiradioselected);

fail("soem message");
			
}

@Test
public void Tc03_cssValuecolor() {
	
String color=driver.findElement(By.id("sm")).getCssValue("background-color");
System.out.println("Css background Color:- "+color);
	
}

@Test
public void Tc04_backgroundcolor() {
	
String color=driver.findElement(By.id("sm")).getCssValue("background-color");
System.out.println("Css background Color:- "+color);
	
}*/


@AfterMethod
public void aftermethod() {
		
	System.out.println("After method");
	
}

@AfterClass
public void afterclass() {
	
	//driver.quit();
}
	

}
