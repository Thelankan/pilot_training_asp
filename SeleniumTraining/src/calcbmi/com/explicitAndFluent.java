package calcbmi.com;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class explicitAndFluent {

	public static void main(String[] args) {

		//explicit wait
		WebDriver driver =new FirefoxDriver();
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("someid")));
		
	/*	WebDriverWait wait = new WebDriverWait(driver, 20);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='WebDriver']")));
	*/
		
		//Fluent wait
/*		Wait<WebDriver> waitflu = new FluentWait<WebDriver>(driver)
       .withTimeout(30, SECONDS);
       .pollingEvery(5, SECONDS);
       .ignoring(NoSuchElementException.class);*/
		
	}

}
