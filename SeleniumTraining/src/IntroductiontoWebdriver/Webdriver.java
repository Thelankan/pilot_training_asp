package IntroductiontoWebdriver;


import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import static org.testng.Assert.*;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Webdriver {
	

WebDriver driver;	
	
@BeforeClass
public void beforeclass() {
	
	System.setProperty("webdriver.gecko.driver","Test\\Resources\\geckodriver.exe");
	//webdriver
	driver=new FirefoxDriver();
	
	//Entering the url
	driver.get("https://www.nobroker.in");
	driver.manage().window().maximize();	
	
	
}
	
@BeforeMethod
public void beforemethod() {
		
	//Entering the url
	driver.get("https://www.nobroker.in");
	driver.manage().window().maximize();	
	
}

@Test(groups= {"Regression"})
public void TC01_testmethod() {
	
	
	System.out.println("Only Regression");
	
	//click on login button
	driver.findElement(By.xpath("//div[@class='navbar-item-hover navbar-item pull-right']//a[contains(text(),'Log in')]")).click();
	
	//click on user name
	driver.findElement(By.id("userName")).sendKeys("thelankan1@gmail.com");
	driver.findElement(By.xpath("(.//*[@id='user_password'])[2]")).sendKeys("Vinay7411377847");
	driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	driver.findElement(By.id("signInButton")).click();
	
	driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);
	
	Set<Cookie> ck=driver.manage().getCookies();
	
	for (Object o:ck) {
		System.out.println(o);
	}
			
}

@Test(groups= {"Sanity"})
public void TC02_Sanity() {
		
System.out.println("Only Sanity");
		
}

@Test(groups= {"Regression","Sanity"})
public void TC03_RegSanity() {

System.out.println("Regression and sanity");
				
}


@AfterMethod
public void aftermethod() {
		
	System.out.println("After method");
	
}

@AfterClass
public void afterclass() {
	
	driver.quit();
}

}
