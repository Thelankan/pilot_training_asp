package com.dwre.pages.orders;

import com.dwre.pages.ElementLayer;
import com.dwre.pages.Navigation;
import com.dwre.support.BrowserActions;
import com.dwre.support.DWRE_Utils;
import com.dwre.support.EnvironmentPropertiesReader;
import com.dwre.support.FileUtils;
import com.dwre.support.Log;
import com.dwre.support.MerchantTools;
import com.dwre.support.StopWatch;
import com.dwre.support.Utils;
import com.dwre.support.XmlUtils;
import java.io.File;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

public class ImportExportOrders extends LoadableComponent<ImportExportOrders>
{
  private static EnvironmentPropertiesReader orderProperty = EnvironmentPropertiesReader.getInstance("order");
  
  private WebDriver driver;
  private boolean isPageLoaded;
  public ElementLayer elementLayer;
  public String inputFile;
  File orderXml = null;
  
  @FindBy(id="")
  WebElement readyElement;
  
  @FindBy(id="login")
  WebElement LoginPage;
  
  @FindBy(xpath="//td[contains(text(),'Orders')]/ancestor::table[1]/following-sibling::table[1]//button[@name='OrderExport']")
  WebElement orderExportButton;
  
  @FindBy(css="input[name='OrderSearchForm2_SimpleSearchTerm']")
  WebElement txtOrderNo;
  
  @FindBy(css="button[name='simpleSearch']")
  WebElement btnSimpleFind;
  
  @FindBy(css="input[name='SelectedObjectUUID']")
  WebElement chkOrder;
  
  @FindBy(xpath="//button[@name='exportOrders'][contains(text(),'Next')]")
  WebElement btnNext;
  
  @FindBy(xpath="//button[@name='export'][contains(text(),'Export')]")
  WebElement btnExportConfigure;
  
  @FindBy(css="input[name='OrderExportForm_ExportFile']")
  WebElement txtExportFile;
  
  @FindBy(xpath="//input[@name='SelectedObjectUUID']/ancestor::td/following-sibling::td/a[@class='selection_link']")
  WebElement lnkSelectOrder;
  
  @FindBy(xpath="//a[@class='selection_link'][contains(text(),'Download Export File')]")
  WebElement lnkDownloadXml;
  
  @FindBy(xpath="//input[@name='SelectedObjectUUID']/ancestor::td/following-sibling::td[4]")
  WebElement lblOrderStatus;
  
  public ImportExportOrders(WebDriver driver)
  {
    this.driver = driver;
    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 
      Utils.maxElementWait);
    PageFactory.initElements(finder, this);
  }
  
  protected void isLoaded()
    throws Error
  {
    if (!isPageLoaded) {
      org.testng.Assert.fail();
    }
    try
    {
      if (LoginPage.isDisplayed())
        new Navigation(driver).login(driver);
      Utils.waitForPageLoad(driver);
      driver.get(DWRE_Utils.generateURL(MerchantTools.ImportExportOrder));
    }
    catch (Exception localException) {}
    

    if ((isPageLoaded) && (!Utils.waitForElement(driver, orderExportButton, 30))) {
      Log.fail("Orders Page didn't display", driver);
    }
    
    elementLayer = new ElementLayer(driver);
  }
  

  protected void load()
  {
    isPageLoaded = true;
    driver.get(DWRE_Utils.generateURL(MerchantTools.ImportExportOrder));
    Utils.waitForPageLoad(driver);
  }
  



  public void navigateToOrderExportPage()
    throws Exception
  {
    long startTime = StopWatch.startTime();
    BrowserActions.clickOnElement(orderExportButton, driver, "Order Export button is  clicked");
    Log.event("Navigated to the order Export page", StopWatch.elapsedTime(startTime));
  }
  





  public String exportOrder(String orderNumber)
    throws Exception
  {
    long startTime = StopWatch.startTime();
    
    navigateToOrderExportPage();
    

    BrowserActions.typeOnTextField(txtOrderNo, orderNumber, driver, "Entered order number");
    BrowserActions.clickOnElement(btnSimpleFind, driver, "Find is clicked");
    

    BrowserActions.getRadioOrCheckboxChecked(chkOrder);
    BrowserActions.clickOnElement(btnNext, driver, "Next button is clicked");
    


    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-hhmmss-SSS");
    inputFile = (orderNumber + "_" + sdf.format(cal.getTime()) + ".xml");
    
    BrowserActions.typeOnTextField(txtExportFile, inputFile, driver, "Entered export file name");
    BrowserActions.clickOnElement(btnExportConfigure, driver, "Next button is clicked");
    Log.event("Navigated to the order Listing page", 
      StopWatch.elapsedTime(startTime));
    

    Thread.sleep(50000L);
    driver.get(driver.getCurrentUrl());
    BrowserActions.clickOnElement(lnkSelectOrder, driver, "order xml link");
    
    BrowserActions.clickOnElement(lnkDownloadXml, driver, "clicked order xml link");
    
    String home = System.getProperty("user.home");
    File file = new File(home + "/Downloads/" + inputFile);
    
    File orderFolder = new File("/order/");
    
    if (!orderFolder.exists()) {
      orderFolder.mkdir();
    }
    
    File file1 = new File(orderFolder + "/" + inputFile);
    Thread.sleep(10000L);
    FileUtils.moveFile(file, file1);
    
    String cont = new String(Files.readAllBytes(Paths.get(orderFolder + "/" + inputFile, new String[0])));
    String filePath = orderFolder + "/" + inputFile;
    return filePath;
  }
  
  public LinkedHashMap<String, String> xmlPaymentDetails(String xmlPath, String orderNumber) throws Exception
  {
    LinkedHashMap<String, String> paymentValue = new LinkedHashMap();
    String merchandizeValue = XmlUtils.getNodeData(xmlPath, "order-total", "net-price").toString().split("price")[1].replace("=", "").replace("}", "");
    String salesTax = XmlUtils.getNodeData(xmlPath, "order-total", "tax").toString().split("\\=")[1].replace("}", "");
    String ordertotal = XmlUtils.getNodeData(xmlPath, "order-total", "gross-price").toString().split("price")[1].replace("=", "").replace("}", "");
    paymentValue.put("merchandize-total", merchandizeValue);
    paymentValue.put("sales-tax", salesTax);
    paymentValue.put("order-total", ordertotal);
    return paymentValue;
  }
  







  public boolean validateCustomer(LinkedHashMap<String, String> storeFrontCustomerDetails, String orderNumber, String xmlPath)
    throws Exception
  {
    LinkedHashMap<String, String> xmlCustomerDetails = XmlUtils.getDataFromXMLAsString(xmlPath, "billing-address");
    String[] ingoreValue = { "country-code", "custom-attribute" };
    Log.message("<h2><b><u> Verification -Customer Details </u></b></h2>");
    Log.message("<br>");
    Log.softAssertThat(
      Utils.compareAndPrintTableHashMap("Customer details", "Xml Customer Details", "Store Front Customer Details", 
      xmlCustomerDetails, 
      storeFrontCustomerDetails, new String[][] { ingoreValue }), 
      "<b>Actual Result : </b>Customer details are same in both XML and Storefront", 
      "<b>Actual Result : </b>Customer details are not same in both XML and Storefront");
    return Utils.compareTwoHashMap(xmlCustomerDetails, storeFrontCustomerDetails, new String[][] { ingoreValue });
  }
  







  public boolean validateShippingDetails(LinkedList<LinkedHashMap<String, String>> storeFrontShippingDetails, String orderNumber, String xmlPath)
    throws Exception
  {
    LinkedList<LinkedHashMap<String, String>> xmlShippingDetails = XmlUtils.getDataFromXMLLL(xmlPath, "shipping-address");
    String[] ingoreValue = { "country-code", "custom-attribute" };
    Log.message("<h2><b><u> Verification -Shipping Details </u></b></h2>");
    Log.message("<br>");
    Log.softAssertThat(
      Utils.compareAndPrintTableLinkedListHashMap("Shipping details", "Xml Shipping Details", "Store Front Shipping Details", 
      xmlShippingDetails, 
      storeFrontShippingDetails, new String[][] { ingoreValue }), 
      "<b>Actual Result : </b>Shipping details are same in both XML and Storefront", 
      "<b>Actual Result : </b>Shipping details are not same in both XML and Storefront");
    return Utils.compareTwoLinkedListHashMap(xmlShippingDetails, storeFrontShippingDetails, new String[][] { ingoreValue });
  }
  







  public boolean validateShippingMethod(LinkedList<LinkedHashMap<String, String>> storeFrontShippingMethod, String orderNumber, String xmlPath)
    throws Exception
  {
    LinkedList<LinkedHashMap<String, String>> xmlShippingDetails = XmlUtils.getNodeDataAsLL(xmlPath, "shipping-lineitem", "item-id");
    Log.message("<h2><b><u> Verification -Shipping Method Details </u></b></h2>");
    Log.message("<br>");
    Log.softAssertThat(
      Utils.compareAndPrintTableLinkedListHashMap("Shipping Method details", "Xml Shipping Method Details", "Store Front Shipping Method Details", 
      xmlShippingDetails, 
      storeFrontShippingMethod, new String[0][]), 
      "<b>Actual Result : </b>Shipping Method details are same in both XML and Storefront", 
      "<b>Actual Result : </b>Shipping Method details are not same in both XML and Storefront");
    return Utils.compareTwoLinkedListHashMap(xmlShippingDetails, storeFrontShippingMethod, new String[0][]);
  }
  







  public boolean validateCard(LinkedHashMap<String, String> storeFrontCardrDetails, String orderNumber, String xmlPath)
    throws Exception
  {
    LinkedHashMap<String, String> xmlCardDetails = XmlUtils.getDataFromXMLAsString(xmlPath, "credit-card");
    String[] ingoreValue = { "card-token", "expiration-month", "expiration-year", "custom-attribute" };
    Log.message("<h2><b><u> Verification -Payment Details </u></b></h2>");
    Log.message("<br>");
    Log.softAssertThat(
      Utils.compareAndPrintTableHashMap("Payment details", "Xml Payment Details", "Store Front Payment Details", 
      xmlCardDetails, 
      storeFrontCardrDetails, new String[][] { ingoreValue }), 
      "<b>Actual Result : </b>Payment details are same in both XML and Storefront", 
      "<b>Actual Result : </b>Payment details are not same in both XML and Storefront");
    return Utils.compareTwoHashMap(xmlCardDetails, storeFrontCardrDetails, new String[][] { ingoreValue });
  }
  






  public boolean validateProductDetails(LinkedList<LinkedHashMap<String, String>> storeFrontProductDetails, String orderNumber, String xmlPath)
    throws Exception
  {
    System.out.println("$$$$$$$$$$$$$$$$$$$$$" + XmlUtils.getDataFromXMLLL(xmlPath, "product-lineitem"));
    LinkedList<LinkedHashMap<String, String>> xmlProductDetails = XmlUtils.getDataFromXMLLL(xmlPath, "product-lineitem");
    System.out.println("^^^^^^^^^^^^^^^^^^^" + xmlProductDetails);
    String[] ingoreValue = { "tax", "gross-price", "base-price", "tax-basis", "position", "product-id", "tax-rate", "shipment-id", "gift", 
      "custom-attribute", "productColor", "productSize" };
    Log.message("<h2><b><u> Verification -Product Details </u></b></h2>");
    Log.message("<br>");
    Log.softAssertThat(
      Utils.compareAndPrintTableLinkedListHashMap("Product details", "Xml Product Details", "Store Front Product Details", 
      xmlProductDetails, 
      storeFrontProductDetails, new String[][] { ingoreValue }), 
      "<b>Actual Result : </b>Product details are same in both XML and Storefront", 
      "<b>Actual Result : </b>Product details are not same in both XML and Storefront");
    return Utils.compareTwoLinkedListHashMap(storeFrontProductDetails, xmlProductDetails, new String[0][]);
  }
  






  public boolean validateTaxDetails(LinkedHashMap<String, String> storeFrontTaxDetails, String orderNumber, String xmlPath)
    throws Exception
  {
    LinkedHashMap<String, String> xmlPayment = xmlPaymentDetails(xmlPath, orderNumber);
    Log.message("<h2><b><u> Verification -Tax Details </u></b></h2>");
    Log.message("<br>");
    Log.softAssertThat(
      Utils.compareAndPrintTableHashMap("Tax details", "Xml Tax Details", "Store Front Tax Details", 
      xmlPayment, 
      storeFrontTaxDetails, new String[0][]), 
      "<b>Actual Result : </b>Tax details are same in both XML and Storefront", 
      "<b>Actual Result : </b>Tax details are not same in both XML and Storefront");
    return Utils.compareTwoHashMap(xmlPayment, storeFrontTaxDetails, new String[0][]);
  }
  














  public boolean validateOrderXML(LinkedHashMap<String, String> storeFrontCustomerDetails, LinkedHashMap<String, String> storeFrontCardDetails, LinkedHashMap<String, String> storeFrontTaxDetails, LinkedList<LinkedHashMap<String, String>> storeFrontShippingDetails, LinkedList<LinkedHashMap<String, String>> storeFrontShippingMethod, LinkedList<LinkedHashMap<String, String>> storeFrontProductDetails, String orderNumber)
    throws Exception
  {
    String xmlPath = exportOrder(orderNumber);
    boolean flag = true;
    
    if (orderProperty.getProperty("customer").equalsIgnoreCase("true")) {
      boolean cutomerflag = validateCustomer(storeFrontCustomerDetails, orderNumber, xmlPath);
      if (cutomerflag != flag) {
        flag = false;
      }
    }
    if (orderProperty.getProperty("shipping-address").equalsIgnoreCase("true")) {
      boolean shippingAddressflag = validateShippingDetails(storeFrontShippingDetails, orderNumber, xmlPath);
      if (shippingAddressflag != flag) {
        flag = false;
      }
    }
    
    if (orderProperty.getProperty("shipping-method").equalsIgnoreCase("true")) {
      boolean shippingMethodflag = validateShippingMethod(storeFrontShippingMethod, orderNumber, xmlPath);
      if (shippingMethodflag != flag) {
        flag = false;
      }
    }
    if (orderProperty.getProperty("card-details").equalsIgnoreCase("true")) {
      boolean cardflag = validateCard(storeFrontCardDetails, orderNumber, xmlPath);
      if (cardflag != flag) {
        flag = false;
      }
    }
    if (orderProperty.getProperty("tax-validation").equalsIgnoreCase("true")) {
      boolean taxFlag = validateTaxDetails(storeFrontTaxDetails, orderNumber, xmlPath);
      if (taxFlag != flag) {
        flag = false;
      }
    }
    if (orderProperty.getProperty("product-details").equalsIgnoreCase("true")) {
      boolean productFlag = validateProductDetails(storeFrontProductDetails, orderNumber, xmlPath);
      if (productFlag != flag) {
        flag = false;
      }
    }
    return flag;
  }
}
