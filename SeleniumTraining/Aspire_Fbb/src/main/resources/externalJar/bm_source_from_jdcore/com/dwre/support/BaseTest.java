package com.dwre.support;

import org.testng.ITestContext;

public class BaseTest
{
  public BaseTest() {}
  
  @org.testng.annotations.BeforeTest(alwaysRun=true)
  public static void setDefaults(ITestContext context) {
    String dwre_id = new String();
    String product = new String();
    try {
      dwre_id = System.getProperty("dwre_id") != null ? 
        System.getProperty("dwre_id") : context.getCurrentXmlTest()
        .getParameter("dwre_id");
      
      product = System.getProperty("product") != null ? 
        System.getProperty("product") : context.getCurrentXmlTest()
        .getParameter("product");
    }
    catch (Exception localException) {}
    


    if (dwre_id != null)
      System.setProperty("dwre_id", dwre_id);
    if (product != null) {
      System.setProperty("product", product);
      System.setProperty("ProductSession", "false");
    }
  }
}
