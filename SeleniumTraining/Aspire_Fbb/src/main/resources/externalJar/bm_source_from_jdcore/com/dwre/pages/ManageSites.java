package com.dwre.pages;

import com.dwre.support.BrowserActions;
import com.dwre.support.DWRE_Utils;
import com.dwre.support.Log;
import com.dwre.support.MerchantTools;
import com.dwre.support.Utils;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;



public class ManageSites
  extends LoadableComponent<ManageSites>
{
  private WebDriver driver;
  private boolean isPageLoaded;
  public ElementLayer elementLayer;
  String runPltfrm = Utils.getRunPlatForm();
  
  @FindBy(name="detailForm")
  WebElement readyElement;
  
  @FindBy(id="login")
  WebElement LoginPage;
  
  @FindBy(css="#messageDiv")
  WebElement successMessage;
  
  @FindBy(css="td a.table_detail_link")
  List<WebElement> brandLinks;
  
  @FindBy(xpath="//a[@class='table_tabs_dis'][contains(text(),'Cache')]")
  WebElement cachelink;
  
  @FindBy(xpath="//td//span[@data-dw-tooltip='Channel.enablePageCaching']/../input")
  WebElement enablePageCacheCheckbox;
  
  @FindBy(css="#staticCacheRoot")
  WebElement validateStaticContentCacheLink;
  @FindBy(css="#pageCacheRoot")
  WebElement validateEntireSiteCache;
  
  public ManageSites(WebDriver driver)
  {
    this.driver = driver;
    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 
      Utils.maxElementWait);
    PageFactory.initElements(finder, this);
  }
  

  protected void isLoaded()
  {
    if (!isPageLoaded) {
      Assert.fail();
    }
    try
    {
      if (LoginPage.isDisplayed())
        new Navigation(driver).login(driver);
      driver.get(DWRE_Utils.generateURL(MerchantTools.ManageSites));
    }
    catch (Exception localException) {}
    

    if ((isPageLoaded) && (!Utils.waitForElement(driver, readyElement, 30))) {
      Log.fail("Manage Sites Page didn't display", driver);
    }
    
    elementLayer = new ElementLayer(driver);
  }
  

  protected void load()
  {
    isPageLoaded = true;
    driver.get(DWRE_Utils.generateURL(MerchantTools.ManageSites));
    Utils.waitForPageLoad(driver);
  }
  
  public void clearSiteCache() throws Exception
  {
    Boolean Status = Boolean.valueOf(false);
    String Brand = System.getProperty("product");
    Utils.waitForPageLoad(driver);
    
    for (WebElement e : brandLinks)
    {
      if (e.getText().trim().toLowerCase().equals(Brand))
      {
        e.click();
        break;
      }
    }
    Utils.waitForPageLoad(driver);
    if (Utils.waitForElement(driver, cachelink))
    {
      BrowserActions.clickOnElement(cachelink, driver, "Cache link");
      Utils.waitForPageLoad(driver);
      BrowserActions.clickOnElement(enablePageCacheCheckbox, driver, "Enable Page cache checkbox");
      BrowserActions.clickOnElement(validateStaticContentCacheLink, driver, "Static content and Entire site page chach clear button");
      BrowserActions.clickOnElement(validateEntireSiteCache, driver, "Entire Site page content");
      if (Utils.waitForElement(driver, successMessage))
      {
        Status = Boolean.valueOf(true);
      }
    }
    
    if (Status.booleanValue())
    {
      Log.message("Entire Site Page cache is cleared");
      driver.manage().deleteAllCookies();
    }
    else {
      Log.message("Cache is not cleared in Demandware");
    }
  }
}
