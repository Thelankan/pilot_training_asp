package com.dwre.pages.orders;

import com.dwre.pages.ElementLayer;
import com.dwre.pages.Navigation;
import com.dwre.support.BrowserActions;
import com.dwre.support.DWRE_Utils;
import com.dwre.support.Log;
import com.dwre.support.MerchantTools;
import com.dwre.support.StopWatch;
import com.dwre.support.Utils;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;


public class Orders
  extends LoadableComponent<Orders>
{
  private WebDriver driver;
  private boolean isPageLoaded;
  public ElementLayer elementLayer;
  String runPlatForm = Utils.getRunPlatForm();
  

  @FindBy(id="td[class='overview_title']")
  WebElement readyElement;
  

  @FindBy(id="login")
  WebElement LoginPage;
  

  @FindBy(css="a[class='merchant-tools-link']")
  WebElement lnkMerchantTool;
  

  @FindBy(css="td[class='overview_subtitle'][title*='orders'] a[class='overview_subtitle']")
  WebElement lnkOrdering;
  

  @FindBy(css="td[class='overview_subtitle'][title*='orders'] a[class='overview_subtitle']")
  WebElement lnkOrders;
  

  @FindBy(css="input[name='OrderSearchForm2_SimpleSearchTerm']")
  WebElement txtOrderNo;
  

  @FindBy(css="button[name='simpleSearch']")
  WebElement btnSimpleFind;
  

  @FindBy(css="a[class='table_detail_link']")
  WebElement lnkOrderNo;
  

  @FindBy(css="table[width='100%']:nth-child(4) tr")
  List<WebElement> productRowDetails;
  

  @FindBy(xpath="//td[contains(text(),'Shipment Shipping Cost:')]/following-sibling::td[1]")
  WebElement shipmentShippingCostTxt;
  

  @FindBy(xpath="//td[contains(text(),'Total Shipping')]/following-sibling::td[1]")
  WebElement totalShippingTxt;
  

  @FindBy(xpath="//td[contains(text(),'Shipping Total')]/following-sibling::td[1]")
  WebElement shippingTotalTxt;
  
  @FindBy(xpath="//td[contains(text(),'Tax Total')]/following-sibling::td[1]")
  WebElement tatToalTxt;
  
  @FindBy(xpath="//td[contains(text(),'Tax Total')]/ancestor::tr[1]/following-sibling::tr/td[2]")
  WebElement toTalTxt;
  
  @FindBy(xpath="//td/font[contains(text(),'Adjustment Free')]/ancestor::tr[1]/td[2]")
  WebElement adjustmentTxt;
  
  @FindBy(xpath="//td/a[@class='table_tabs_dis'][text()='Attributes']")
  WebElement lnkAttributes;
  
  @FindBy(xpath="//td/a[@class='table_tabs_dis'][text()='Payment']")
  WebElement lnkPayment;
  
  @FindBy(xpath="//td[@class='infobox_item top s'][2]//tr")
  List<WebElement> billingRowDetails;
  

  public Orders(WebDriver driver)
  {
    this.driver = driver;
    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 
      Utils.maxElementWait);
    PageFactory.initElements(finder, this);
  }
  

  protected void isLoaded()
  {
    if (!isPageLoaded) {
      Assert.fail();
    }
    try
    {
      if (LoginPage.isDisplayed())
        new Navigation(driver).login(driver);
      Utils.waitForPageLoad(driver);
      navigateOrderPage();
    }
    catch (Exception localException) {}
    

    if ((isPageLoaded) && (!Utils.waitForElement(driver, txtOrderNo, 30))) {
      Log.fail("Orders Page didn't display", driver);
    }
    
    elementLayer = new ElementLayer(driver);
  }
  

  protected void load()
  {
    isPageLoaded = true;
    driver.get(DWRE_Utils.generateURL(MerchantTools.Home));
    Utils.waitForPageLoad(driver);
  }
  


  public void navigateOrderPage()
    throws Exception
  {
    long startTime = StopWatch.startTime();
    BrowserActions.clickOnElement(lnkMerchantTool, driver, "Merchant Tool clicked");
    BrowserActions.javascriptClick(lnkOrdering, driver, "Ordering link is clicked");
    BrowserActions.javascriptClick(lnkOrders, driver, "Orderslink is clicked");
    Log.event("Navigated to the order page", 
      StopWatch.elapsedTime(startTime));
  }
  




  public void clickOnFindButton(String orderNumber)
    throws Exception
  {
    long startTime = StopWatch.startTime();
    BrowserActions.typeOnTextField(txtOrderNo, orderNumber, driver, "Entered order number");
    BrowserActions.clickOnElement(btnSimpleFind, driver, "Find is clicked");
    Log.event("Navigated to the order Listing page", 
      StopWatch.elapsedTime(startTime));
  }
  


  public void navigateToOrderDetailPage()
    throws Exception
  {
    long startTime = StopWatch.startTime();
    BrowserActions.clickOnElement(lnkOrderNo, driver, "Order no link  is clicked");
    Log.event("Navigated to the order Detail page", StopWatch.elapsedTime(startTime));
  }
  
  public LinkedList<LinkedHashMap<String, String>> dwreOrderDetails(String orderNo) throws Exception
  {
    clickOnFindButton(orderNo);
    
    navigateToOrderDetailPage();
    
    LinkedList<LinkedHashMap<String, String>> dwreorderDetails = new LinkedList();
    LinkedHashMap<String, String> orderDetails = new LinkedHashMap();
    
    for (int i = 3; i < ((WebElement)productRowDetails.get(i)).findElements(By.cssSelector("td")).size(); i++) {
      LinkedHashMap<String, String> orderProductDetails = new LinkedHashMap();
      List<WebElement> columns = ((WebElement)productRowDetails.get(i)).findElements(By.cssSelector("td"));
      
      orderProductDetails.put("Quantity", ((WebElement)columns.get(0)).getText());
      orderProductDetails.put("ProductID", ((WebElement)columns.get(1)).getText());
      orderProductDetails.put("ProductName", ((WebElement)columns.get(2)).getText());
      orderProductDetails.put("TaxRate", ((WebElement)columns.get(4)).getText());
      orderProductDetails.put("ProductPrice", ((WebElement)columns.get(5)).getText());
      orderProductDetails.put("TaxBasis", ((WebElement)columns.get(6)).getText());
      orderProductDetails.put("ItemTotal", ((WebElement)columns.get(7)).getText());
      dwreorderDetails.add(orderProductDetails);
    }
    

    orderDetails.put("ShippmentCost", shipmentShippingCostTxt.getText());
    orderDetails.put("AdjustmentCost", adjustmentTxt.getText());
    orderDetails.put("ToTalShipping", totalShippingTxt.getText());
    orderDetails.put("ShippingTotal", shippingTotalTxt.getText());
    orderDetails.put("TaxTotal", tatToalTxt.getText());
    orderDetails.put("total", toTalTxt.getText());
    

    BrowserActions.clickOnElement(lnkPayment, driver, "payment header clicked");
    

    int k = 0;
    billingRowDetails.size();
    orderDetails.put("billingName", ((WebElement)billingRowDetails.get(k)).findElement(By.cssSelector("td")).getText());
    orderDetails.put("billingAddress", ((WebElement)billingRowDetails.get(k + 1)).findElement(By.cssSelector("td")).getText());
    orderDetails.put("billingAddCountry", ((WebElement)billingRowDetails.get(k + 2)).findElement(By.cssSelector("td")).getText());
    orderDetails.put("billingAddState", ((WebElement)billingRowDetails.get(k + 3)).findElement(By.cssSelector("td")).getText());
    
    dwreorderDetails.add(orderDetails);
    
    return dwreorderDetails;
  }
}
