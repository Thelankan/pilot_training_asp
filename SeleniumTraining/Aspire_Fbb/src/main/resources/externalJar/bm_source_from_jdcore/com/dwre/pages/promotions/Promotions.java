package com.dwre.pages.promotions;

import com.dwre.pages.ElementLayer;
import com.dwre.pages.Navigation;
import com.dwre.support.BrowserActions;
import com.dwre.support.DWRE_TestDataExtractor;
import com.dwre.support.DWRE_Utils;
import com.dwre.support.EnvironmentPropertiesReader;
import com.dwre.support.Log;
import com.dwre.support.MerchantTools;
import com.dwre.support.Utils;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;


public class Promotions
  extends LoadableComponent<Promotions>
{
  private WebDriver driver;
  private boolean isPageLoaded;
  public ElementLayer elementLayer;
  String runPltfrm = Utils.getRunPlatForm();
  String workBook = EnvironmentPropertiesReader.getInstance("dataConfig").getProperty("workBookName");
  String workSheet = EnvironmentPropertiesReader.getInstance("dataConfig").getProperty("workSheet_Promotions");
  

  @FindBy(css="div[class='main-content dw-Promotiongrid']")
  WebElement readyElement;
  

  @FindBy(id="login")
  WebElement LoginPage;
  
  @FindBy(css="[dw-label='common.button.new']")
  WebElement btnNew;
  
  @FindBy(css="#PromotionGeneral")
  WebElement promotionPage;
  
  @FindBy(css="input[name='PromotionID']")
  WebElement txtpromotionID;
  
  @FindBy(css="input[class='inputfield_en perm_localized']")
  WebElement txtpromotionName;
  
  @FindBy(xpath="//span[@data-dw-tooltip='Promotion.calloutMsg']/../../../../..//following-sibling::td//td[@class='table_detail']/textarea")
  WebElement txtCallOutMessage;
  
  @FindBy(xpath="//span[@data-dw-tooltip='Promotion.details']/../../../../..//following-sibling::td//td[@class='table_detail']/textarea")
  WebElement txtPromotionDetails;
  
  @FindBy(xpath="//span[@data-dw-tooltip='Promotion.exclusivity']/../../../../..//following-sibling::td//td[@class='table_detail']/select")
  WebElement dropExclusivity;
  
  @FindBy(css="button[name='PromotionUpdateButton']")
  WebElement btnApply;
  
  @FindBy(css="table#DiscountTable input.inputfield_en")
  WebElement txtDiscount;
  
  @FindBy(css="select#PromotionClassSelection")
  WebElement promoClass;
  
  @FindBy(css="select#ConditionTypeSelection")
  WebElement promoConditions;
  
  @FindBy(css="table#DiscountTable select[class='select']")
  WebElement promoDiscount;
  
  @FindBy(css="table#DiscountTable input.inputfield_en")
  WebElement txtPromoDiscount;
  
  @FindBy(css="input[class='inputfield_en'][id='Row_0_ProductOptions']")
  WebElement txtPromoDiscountWithProduct;
  
  @FindBy(css="input[class='inputfield_en'][id*='BuyX']")
  WebElement txtBuyPromoDiscount;
  
  @FindBy(css="input[class='inputfield_en'][id*='Discount']")
  WebElement txtGetPromoDiscount;
  
  @FindBy(css="input[class='inputfield_en'][id*='SKU']")
  WebElement txtPromoSKU;
  
  @FindBy(css="input[class='inputfield_en'][id*='MaxBonusItems']")
  WebElement txtPromoMaxBonus;
  
  @FindBy(css="input[class='inputfield_en'][id*='Display']")
  WebElement txtPromoDisplay;
  
  @FindBy(css="input[class='inputfield_en'][id*='PriceBook']")
  WebElement txtPromoPriceBook;
  
  @FindBy(css="input[class='inputfield_en'][id*='ProductOptions']")
  WebElement txtPromoProductOption;
  
  @FindBy(css="input[class='inputfield_en'][id*='Bogo']")
  WebElement txtPromoBogo;
  
  @FindBy(css="#promotion-rule-html td.buttonspacing >button")
  WebElement btnPromoApply;
  
  @FindBy(css=".main-toolbar [dw-search-filters='searchFilters'] input[placeholder*='Search by ID']")
  WebElement txtSearchId;
  
  @FindBy(css=".main-toolbar [dw-search-filters='searchFilters'] button")
  WebElement btnSearchId;
  

  public Promotions(WebDriver driver)
  {
    this.driver = driver;
    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 
      Utils.maxElementWait);
    PageFactory.initElements(finder, this);
  }
  

  protected void isLoaded()
  {
    if (!isPageLoaded) {
      Assert.fail();
    }
    try
    {
      if (LoginPage.isDisplayed())
        new Navigation(driver).login(driver);
      driver.get(DWRE_Utils.generateURL(MerchantTools.Promotions));
    }
    catch (Exception localException) {}
    

    if ((isPageLoaded) && (!Utils.waitForElement(driver, readyElement, 30))) {
      Log.fail("Promotions Page didn't display", driver);
    }
    
    elementLayer = new ElementLayer(driver);
  }
  

  protected void load()
  {
    isPageLoaded = true;
    driver.get(DWRE_Utils.generateURL(MerchantTools.Promotions));
    Utils.waitForPageLoad(driver);
  }
  




  public String createPromotion(String PromotionID)
    throws Exception
  {
    String promotionId = null;
    HashMap<String, String> testData = DWRE_TestDataExtractor.initTestData(workBook, workSheet, new String[] { PromotionID });
    LinkedHashMap<String, WebElement> instructions = new LinkedHashMap();
    
    String promotionRule = (String)testData.get("PromotionRule");
    String promoCondition = (String)testData.get("PromoCondition");
    String promoDiscountType = (String)testData.get("PromoDiscountType");
    
    String promoBuyValue = (String)testData.get("BuyValue");
    String promoGetValue = (String)testData.get("GetValue");
    String promoPriceDetails = (String)testData.get("PriceBook");
    String promoBogoDiscount = (String)testData.get("BogoDetails");
    String promoSku = (String)testData.get("BonusProduct");
    String productOption = (String)testData.get("ProductOption");
    String maxBonusOption = (String)testData.get("NoOfBonusProduct");
    String promoFreeShipping = (String)testData.get("FreeShippig");
    String promoDisplay = (String)testData.get("PromoDisplay");
    

    BrowserActions.clickOnElement(btnNew, driver, "New button");
    Utils.waitForPageLoad(driver);
    promotionId = (String)testData.get("PromotionId");
    
    if (Utils.waitForElement(driver, promotionPage))
    {
      instructions.put("type_Promotion Id_" + (String)testData.get("PromotionId"), txtpromotionID);
      instructions.put("type_Promotion Name_" + (String)testData.get("PromotionName"), txtpromotionName);
      instructions.put("type_Call out message_" + (String)testData.get("CallOutMessage"), txtCallOutMessage);
      instructions.put("type_Promotion Details_" + (String)testData.get("PromotionDetails"), txtPromotionDetails);
      instructions.put("select_Exclusivity_" + (String)testData.get("Exclusivity"), dropExclusivity);
      instructions.put("click_Apply Button", btnApply);
      BrowserActions.fillFormDetails(instructions, driver);
    }
    



    instructions.clear();
    instructions.put("select_Promotion Class_" + promotionRule, promoClass);
    instructions.put("select_Promotion Class_" + promoCondition, promoConditions);
    instructions.put("select_Promotion Class_" + promoDiscountType, promoDiscount);
    BrowserActions.fillFormDetails(instructions, driver);
    
    String str1;
    switch ((str1 = promotionRule.toLowerCase()).hashCode()) {case -516235858:  if (str1.equals("shipping")) {} break; case -309474065:  if (str1.equals("product")) break; break; case 106006350:  if (!str1.equals("order")) {
        break label2859;
        String str2;
        switch ((str2 = promoCondition).hashCode()) {case -1936667793:  if (str2.equals("Buy X and Y/Get Z")) {} break; case -1229393781:  if (str2.equals("Buy X for Total")) {} break; case -932726729:  if (str2.equals("Without Qualifying Products")) break; break; case -807222852:  if (str2.equals("Buy X / Get Y")) {} break; case -337074476:  if (str2.equals("With Amount of Qualifying Products")) {} break; case 894556290:  if (str2.equals("With Combination of Products")) {} break; case 1476811971:  if (!str2.equals("With Number of Qualifying Products")) {
            break label2028;
            if ((promoDiscountType.equalsIgnoreCase("Percent Off Product Options")) || (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (List)"))) {
              BrowserActions.typeOnTextField(txtPromoDiscount, promoGetValue, driver, "promo discount text");
              BrowserActions.typeOnTextField(txtPromoDiscountWithProduct, productOption, driver, "promo product text");
            } else {
              BrowserActions.typeOnTextField(txtPromoDiscount, promoGetValue, driver, "promo discount text");
              

              break label2028;
              
              if ((promoDiscountType.equalsIgnoreCase("Percent Off")) || (promoDiscountType.equalsIgnoreCase("Amount Off")) || 
                (promoDiscountType.equalsIgnoreCase("Fixed Price")) || (promoDiscountType.equalsIgnoreCase("Fixed Price Shipping"))) {
                BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
                BrowserActions.typeOnTextField(txtGetPromoDiscount, promoGetValue, driver, "promo Get text");
              }
              
              if ((promoDiscountType.equalsIgnoreCase("Bonus Product(s)")) || (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (Rule)")) || 
                (promoDiscountType.equalsIgnoreCase("Price from Price Book"))) {
                BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
                if (promoDiscountType.equalsIgnoreCase("Bonus Product(s)"))
                  BrowserActions.typeOnTextField(txtPromoSKU, promoSku, driver, "promo sku text");
                if (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (Rule)"))
                  BrowserActions.typeOnTextField(txtPromoMaxBonus, maxBonusOption, driver, "No of Bonus products");
                if (promoDiscountType.equalsIgnoreCase("Price from Price Book")) {
                  BrowserActions.typeOnTextField(txtPromoPriceBook, promoPriceDetails, driver, "promo sku text");
                }
              }
              
              if (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (List)")) {
                BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
                BrowserActions.typeOnTextField(txtPromoMaxBonus, maxBonusOption, driver, "promo sku text");
              }
              

              if (promoDiscountType.equalsIgnoreCase("Percent Off Product Options")) {
                BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
                BrowserActions.typeOnTextField(txtGetPromoDiscount, promoGetValue, driver, "promo Get text");
                BrowserActions.typeOnTextField(txtPromoProductOption, productOption, driver, "promo sku text");
              }
              
              if (promoDiscountType.equalsIgnoreCase("Free Shipping")) {
                BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              }
            }
          }
          else
          {
            if ((promoDiscountType.equalsIgnoreCase("Percent Off")) || (promoDiscountType.equalsIgnoreCase("Amount Off")) || 
              (promoDiscountType.equalsIgnoreCase("Fixed Price")) || (promoDiscountType.equalsIgnoreCase("Total Price"))) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              BrowserActions.typeOnTextField(txtGetPromoDiscount, promoGetValue, driver, "promo Get text");
            }
            
            if (promoDiscountType.equalsIgnoreCase("Price from Price Book")) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              BrowserActions.typeOnTextField(txtPromoPriceBook, promoPriceDetails, driver, "promo discount text");
            }
            
            if (promoDiscountType.equalsIgnoreCase("Percent Off Product Options")) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              BrowserActions.typeOnTextField(txtGetPromoDiscount, promoGetValue, driver, "promo Get text");
              BrowserActions.typeOnTextField(txtPromoProductOption, productOption, driver, "promo discount text");
            }
            
            if (promoDiscountType.equalsIgnoreCase("Bonus Product(s)")) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              BrowserActions.typeOnTextField(txtPromoSKU, promoSku, driver, "promo sku text");
            }
            
            if (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (List)")) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              BrowserActions.typeOnTextField(txtPromoMaxBonus, maxBonusOption, driver, "promo sku text");
              BrowserActions.typeOnTextField(txtPromoDisplay, promoDisplay, driver, "promo discount text");
            }
            
            if (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (Rule)")) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              BrowserActions.typeOnTextField(txtPromoMaxBonus, maxBonusOption, driver, "promo sku text");
            }
            
            if (promoDiscountType.equalsIgnoreCase("Free Shipping")) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoFreeShipping, driver, "promo discount text");
              


              break label2028;
              

              if ((promoDiscountType.equalsIgnoreCase("Percent Off")) || (promoDiscountType.equalsIgnoreCase("Amount Off")) || 
                (promoDiscountType.equalsIgnoreCase("Fixed Price")) || (promoDiscountType.equalsIgnoreCase("Fixed Price Shipping")) || 
                (promoDiscountType.equalsIgnoreCase("Total Price"))) {
                BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              }
              
              if (promoDiscountType.equalsIgnoreCase("Bonus Product(s)")) {
                BrowserActions.typeOnTextField(txtPromoSKU, promoSku, driver, "promo sku text");
              }
              
              if (promoDiscountType.equalsIgnoreCase("Price from Price Book")) {
                BrowserActions.typeOnTextField(txtPromoPriceBook, promoPriceDetails, driver, "promo sku text");
              }
              
              if (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (List)")) {
                BrowserActions.typeOnTextField(txtPromoMaxBonus, maxBonusOption, driver, "promo sku text");
                BrowserActions.typeOnTextField(txtPromoDisplay, promoDisplay, driver, "promo discount text");
              }
              
              if (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (Rule)")) {
                BrowserActions.typeOnTextField(txtPromoMaxBonus, maxBonusOption, driver, "promo sku text");
                

                break label2028;
                

                if ((promoDiscountType.equalsIgnoreCase("Percent Off")) || (promoDiscountType.equalsIgnoreCase("Amount Off")) || 
                  (promoDiscountType.equalsIgnoreCase("Fixed Price"))) {
                  BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
                  BrowserActions.typeOnTextField(txtPromoBogo, promoBogoDiscount, driver, "promo discount text");
                  BrowserActions.typeOnTextField(txtGetPromoDiscount, promoGetValue, driver, "promo Get text");
                }
                
                if (promoDiscountType.equalsIgnoreCase("Price from Price Book")) {
                  BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
                  BrowserActions.typeOnTextField(txtPromoBogo, promoBogoDiscount, driver, "promo discount text");
                  BrowserActions.typeOnTextField(txtPromoPriceBook, promoPriceDetails, driver, "promo discount text");
                }
                
                if (promoDiscountType.equalsIgnoreCase("Free")) {
                  BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
                  BrowserActions.typeOnTextField(txtPromoBogo, promoBogoDiscount, driver, "promo discount text");
                  

                  break label2028;
                  

                  if ((promoDiscountType.equalsIgnoreCase("Percent Off")) || (promoDiscountType.equalsIgnoreCase("Amount Off")) || 
                    (promoDiscountType.equalsIgnoreCase("Fixed Price"))) {
                    BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo discount text");
                  }
                  
                  if (promoDiscountType.equalsIgnoreCase("Price from Price Book")) {
                    BrowserActions.typeOnTextField(txtPromoPriceBook, promoPriceDetails, driver, "promo discount text");
                    

                    break label2028;
                    

                    BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
                    BrowserActions.typeOnTextField(txtGetPromoDiscount, promoGetValue, driver, "promo Get text");
                  }
                } } } }
          break; } } else { label2028:
        String str3;
        switch ((str3 = promoCondition).hashCode()) {case -406427151:  if (str3.equals("With Combination of Qualifying Products")) {} break; case -337074476:  if (str3.equals("With Amount of Qualifying Products")) break; break; case 1476811971:  if (!str3.equals("With Number of Qualifying Products")) {
            break label2617;
            if ((promoDiscountType.equalsIgnoreCase("Percent Off")) || (promoDiscountType.equalsIgnoreCase("Amount Off"))) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              BrowserActions.typeOnTextField(txtGetPromoDiscount, promoGetValue, driver, "promo Get discount text");
            }
            
            if ((promoDiscountType.equalsIgnoreCase("Bonus Product(s)")) || (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (Rule)"))) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              if (promoDiscountType.equalsIgnoreCase("Bonus Product(s)"))
                BrowserActions.typeOnTextField(txtPromoSKU, promoSku, driver, "promo Bonus Product text");
              if (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (Rule)")) {
                BrowserActions.typeOnTextField(txtPromoMaxBonus, maxBonusOption, driver, "No of Bonus products");
              }
            }
            
            if (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (List)")) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoGetValue, driver, "promo Buy text");
              BrowserActions.typeOnTextField(txtPromoMaxBonus, maxBonusOption, driver, "No of Bonus products");
            }
            
          }
          else
          {
            if ((promoDiscountType.equalsIgnoreCase("Percent Off")) || (promoDiscountType.equalsIgnoreCase("Amount Off"))) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              BrowserActions.typeOnTextField(txtGetPromoDiscount, promoGetValue, driver, "promo Get discount text");
            }
            
            if ((promoDiscountType.equalsIgnoreCase("Bonus Product(s)")) || (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (Rule)"))) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              if (promoDiscountType.equalsIgnoreCase("Bonus Product(s)"))
                BrowserActions.typeOnTextField(txtPromoSKU, promoSku, driver, "promo Bonus Product text");
              if (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (Rule)")) {
                BrowserActions.typeOnTextField(txtPromoMaxBonus, maxBonusOption, driver, "No of Bonus Products text");
              }
            }
            
            if (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (List)")) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              BrowserActions.typeOnTextField(txtPromoMaxBonus, maxBonusOption, driver, "No of Bonus Products text");
              

              break label2617;
              

              if ((promoDiscountType.equalsIgnoreCase("Percent Off")) || (promoDiscountType.equalsIgnoreCase("Amount Off"))) {
                BrowserActions.typeOnTextField(txtGetPromoDiscount, promoGetValue, driver, "promo Get discount text");
              }
              
              if (promoDiscountType.equalsIgnoreCase("Bonus Product(s)")) {
                BrowserActions.typeOnTextField(txtPromoSKU, promoSku, driver, "promo Bonus Product text");
              }
              
              if (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (Rule)")) {
                BrowserActions.typeOnTextField(txtPromoMaxBonus, maxBonusOption, driver, "No of Bonus Products text");
              }
              
              if (promoDiscountType.equalsIgnoreCase("Choice of Bonus Products (List)")) {
                BrowserActions.typeOnTextField(txtPromoMaxBonus, maxBonusOption, driver, "No of Bonus Products text");
              }
            }
          }
          break;
        }
        label2617:
        String str4;
        switch ((str4 = promoCondition).hashCode()) {case -631254246:  if (str4.equals("With Combination of Shipment-Qualifying Products")) {} break; case 305065096:  if (str4.equals("With Number of Shipment-Qualifying Products")) break;  case 807429527:  if ((goto 2859) && (str4.equals("With Amount of Shipment-Qualifying Products")))
          {
            if (!promoDiscountType.equalsIgnoreCase("Free")) {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              BrowserActions.typeOnTextField(txtGetPromoDiscount, promoGetValue, driver, "promo Get discount text");
            } else {
              BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              

              break label2859;
              
              if (!promoDiscountType.equalsIgnoreCase("Free")) {
                BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
                BrowserActions.typeOnTextField(txtGetPromoDiscount, promoGetValue, driver, "promo Get discount text");
              } else {
                BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
                

                break label2859;
                
                if (!promoDiscountType.equalsIgnoreCase("Free"))
                  BrowserActions.typeOnTextField(txtBuyPromoDiscount, promoBuyValue, driver, "promo Buy text");
              }
            } }
          break; } }
      break; }
    label2859:
    BrowserActions.clickOnElement(btnPromoApply, driver, "Promotion Apply Button");
    return promotionId;
  }
  



  public void enablePromotion(String PromotionId)
  {
    try
    {
      Utils.waitForPageLoad(driver);
      BrowserActions.typeOnTextField(txtSearchId, PromotionId, driver, "Search Id in Search Panel");
      BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
      Utils.waitForPageLoad(driver);
      WebElement promotionEnableLink = driver.findElement(By.xpath("//span/a[contains(@class,'link-style dw-nc-display-block')][contains(text(),'" + PromotionId + "')]/../../../..//following-sibling::td//div/span[contains(@class,'enabled')][contains(text(),'No')]"));
      if (Utils.waitForElement(driver, promotionEnableLink))
      {
        WebElement actiondrop = driver.findElement(By.xpath("//span/a[contains(@class,'link-style dw-nc-display-block')][contains(text(),'" + PromotionId + "')]/../../../..//following-sibling::td[contains(@class,'dw-grid-actions')]//div/../*/button"));
        BrowserActions.clickOnElement(actiondrop, driver, "Action Drop down");
        Utils.waitForPageLoad(driver);
        List<WebElement> drpmenus = driver.findElements(By.cssSelector(".dw-grid-actions [class='dw-menu'] .dw-menu-item"));
        for (WebElement e : drpmenus)
        {
          if (e.getText().toString().equals("Enable"))
          {
            e.click();
            BrowserActions.nap(3L);
            break;
          }
        }
      }
    }
    catch (Exception e)
    {
      Log.message("Selected Promotion is already in Enabled Status");
    }
  }
  



  public void disablePromotion(String PromotionId)
  {
    try
    {
      Utils.waitForPageLoad(driver);
      BrowserActions.typeOnTextField(txtSearchId, PromotionId, driver, "Search Id in Search Panel");
      BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
      Utils.waitForPageLoad(driver);
      WebElement promotiondisableLink = driver.findElement(By.xpath("//span/a[contains(@class,'link-style dw-nc-display-block')][contains(text(),'" + PromotionId + "')]/../../../..//following-sibling::td//div/span[contains(@class,'enabled')][contains(text(),'Yes')]"));
      if (Utils.waitForElement(driver, promotiondisableLink))
      {
        WebElement actiondrop = driver.findElement(By.xpath("//span/a[contains(@class,'link-style dw-nc-display-block')][contains(text(),'" + PromotionId + "')]/../../../..//following-sibling::td[contains(@class,'dw-grid-actions')]//div/../*/button"));
        BrowserActions.clickOnElement(actiondrop, driver, "Action Drop down");
        Utils.waitForPageLoad(driver);
        List<WebElement> drpmenus = driver.findElements(By.cssSelector(".dw-grid-actions [class='dw-menu'] .dw-menu-item"));
        for (WebElement e : drpmenus)
        {
          if (e.getText().toString().equals("Disable"))
          {
            e.click();
            BrowserActions.nap(3L);
            break;
          }
        }
      }
    }
    catch (Exception e)
    {
      Log.message("Selected Promotion is already in Disabled Status");
    }
  }
  




  public void deletePromotion(String PromotionId)
  {
    try
    {
      Utils.waitForPageLoad(driver);
      BrowserActions.typeOnTextField(txtSearchId, PromotionId, driver, "Search Id in Search Panel");
      BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
      Utils.waitForPageLoad(driver);
      WebElement actiondrop = driver.findElement(By.xpath("//span/a[contains(@class,'link-style dw-nc-display-block')][contains(text(),'" + PromotionId + "')]/../../../..//following-sibling::td[contains(@class,'dw-grid-actions')]//div/../*/button"));
      if (Utils.waitForElement(driver, actiondrop))
      {
        BrowserActions.clickOnElement(actiondrop, driver, "Action Drop down");
        Utils.waitForPageLoad(driver);
        List<WebElement> drpmenus = driver.findElements(By.cssSelector(".dw-grid-actions [class='dw-menu'] .dw-menu-item"));
        for (WebElement e : drpmenus)
        {
          if (e.getText().toString().equals("Delete"))
          {
            e.click();
            BrowserActions.nap(3L);
            break;
          }
        }
      }
    }
    catch (Exception e)
    {
      Log.message("Selected Promotion is already in Enabled Status");
    }
  }
}
