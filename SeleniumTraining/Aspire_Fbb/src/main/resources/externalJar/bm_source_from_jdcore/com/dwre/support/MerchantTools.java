package com.dwre.support;




public enum MerchantTools
{
  None("default"), 
  Home("Home"), 
  Customers("Customers"), 
  CustomerGroup("Customer Group"), 
  CustomerImportExport("Customer Import Export"), 
  Products("Products"), 
  ProductSets("Product Sets"), 
  Catalogs("Catalogs"), 
  PriceBooks("Price Books"), 
  ProductImportExport("Product Import Export"), 
  Promotions("Promotions"), 
  Campaigns("Campaigns"), 
  Coupons("Coupons"), 
  ContentSlots("Content Slots"), 
  Stores("Stores"), 
  PromoImportExport("Online Marketing Import Export"), 
  Orders("Orders"), 
  ImportExportOrder("ImportExportOrder"), 
  ManageSites("ManageSites");
  
  private String _page;
  
  private MerchantTools(String configuration)
  {
    _page = configuration;
  }
  
  public String getConfiguration() {
    return _page;
  }
  
  public static MerchantTools fromConfiguration(String configurationName) {
    for (MerchantTools m : ) {
      if (m.name().equals(configurationName)) {
        return m;
      }
    }
    throw new IllegalArgumentException(
      "There is no value '" + configurationName + "' in Enum '" + MerchantTools.class.getName() + "'");
  }
  
  public static MerchantTools fromValue(String value) {
    for (MerchantTools m : ) {
      if (m.getConfiguration().contains(value)) {
        return m;
      }
    }
    return null;
  }
}
