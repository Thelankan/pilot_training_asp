package com.dwre.pages;

import com.dwre.support.DWRE_Utils;
import com.dwre.support.Log;
import com.dwre.support.MerchantTools;
import com.dwre.support.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

public class HomePage
  extends LoadableComponent<HomePage>
{
  private String appURL;
  private WebDriver driver;
  private boolean isPageLoaded;
  public ElementLayer elementLayer;
  public Navigation nav;
  String runPltfrm = Utils.getRunPlatForm();
  


  @FindBy(css="body[class='bm-welcome-page']")
  WebElement readyElement;
  


  @FindBy(id="login")
  WebElement LoginPage;
  

  @FindBy(name="LoginForm_Login")
  WebElement txtUserName;
  

  @FindBy(name="LoginForm_Password")
  WebElement txtPassword;
  

  @FindBy(name="login")
  WebElement btnLogin;
  


  public HomePage(WebDriver driver, String url)
  {
    appURL = url;
    this.driver = driver;
    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
    PageFactory.initElements(finder, this);
  }
  




  public HomePage(WebDriver driver)
  {
    appURL = DWRE_Utils.generateURL(MerchantTools.Home);
    this.driver = driver;
    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
    PageFactory.initElements(finder, this);
  }
  
  protected void isLoaded()
  {
    if (!isPageLoaded) {
      Assert.fail();
    }
    try
    {
      if (LoginPage.isDisplayed()) {
        new Navigation(driver).login(driver);
      }
    } catch (Exception localException) {}
    if ((isPageLoaded) && (!Utils.waitForElement(driver, readyElement))) {
      Log.fail("Home Page did not open up. Site might be down.", driver);
    }
    
    elementLayer = new ElementLayer(driver);
    nav = ((Navigation)new Navigation(driver).get());
  }
  
  protected void load()
  {
    isPageLoaded = true;
    driver.get(appURL);
    Utils.waitForPageLoad(driver);
  }
}
