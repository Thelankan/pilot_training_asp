package com.dwre.support;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;






public class BrowserActions
{
  public static String MOUSE_HOVER_JS = "var evObj = document.createEvent('MouseEvents');evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);arguments[0].dispatchEvent(evObj);";
  







  public BrowserActions() {}
  






  public static void typeOnTextField(WebElement txt, String txtToType, WebDriver driver, String elementDescription)
    throws Exception
  {
    if (!Utils.waitForElement(driver, txt)) {
      throw new Exception(elementDescription + 
        " field not found in page!!");
    }
    try {
      txt.clear();
      txt.click();
      txt.sendKeys(new CharSequence[] { txtToType });
    } catch (NoSuchElementException e) {
      throw new Exception(elementDescription + 
        " field not found in page!!");
    }
  }
  















  public static void typeOnTextField(String txt, String txtToType, WebDriver driver, String elementDescription)
    throws Exception
  {
    WebElement element = checkLocator(driver, txt);
    if (!Utils.waitForElement(driver, element, 1)) {
      throw new Exception(elementDescription + 
        " field not found in page!!");
    }
    try {
      element.clear();
      element.click();
      element.sendKeys(new CharSequence[] { txtToType });
      Log.event("Typed Text :: " + txtToType);
    } catch (NoSuchElementException e) {
      throw new Exception(elementDescription + 
        " field not found in page!!");
    }
  }
  

















  public static void javascriptType(String txt, String txtToType, WebDriver driver, String elementDescription)
    throws Exception
  {
    WebElement element = checkLocator(driver, txt);
    if (!Utils.waitForElement(driver, element, 1)) {
      throw new Exception(elementDescription + 
        " field not found in page!!");
    }
    try {
      ((JavascriptExecutor)driver).executeScript("arguments[0].value='" + txtToType + "';", new Object[] { element });
    } catch (NoSuchElementException e) {
      throw new Exception(elementDescription + 
        " field not found in page!!");
    }
  }
  

  public static void scrollToView(WebElement element, WebDriver driver)
    throws InterruptedException
  {
    ((JavascriptExecutor)driver).executeScript(
      "arguments[0].scrollIntoView();", new Object[] { element });
    nap(2L);
  }
  






  public static void scrollToViewElement(WebElement element, WebDriver driver)
  {
    try
    {
      String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);var elementTop = arguments[0].getBoundingClientRect().top;window.scrollBy(0, elementTop-(viewPortHeight/2));";
      

      ((JavascriptExecutor)driver).executeScript(
        scrollElementIntoMiddle, new Object[] { element });
      nap(2L);
    } catch (Exception ex) {
      Log.event("Moved to element..");
    }
  }
  
  public static void nap(long time) {
    try {
      TimeUnit.SECONDS.sleep(time);
    }
    catch (InterruptedException localInterruptedException) {}
  }
  








  public static int countElements(String xpath, WebDriver driver)
  {
    return driver.findElements(By.xpath(xpath)).size();
  }
  
  public static void clickOnElement(WebElement btn, WebDriver driver, String elementDescription)
    throws Exception
  {
    if (!Utils.waitForElement(driver, btn, 5)) {
      throw new Exception(elementDescription + " not found in page!!");
    }
    try {
      scrollToViewElement(btn, driver);
      btn.click();
    } catch (NoSuchElementException e) {
      throw new Exception(elementDescription + " not found in page!!");
    }
  }
  












  public static void clickOnElement(String btn, WebDriver driver, String elementDescription)
    throws Exception
  {
    WebElement element = checkLocator(driver, btn);
    if (!Utils.waitForElement(driver, element, 1)) {
      throw new Exception(elementDescription + " not found in page!!");
    }
    try {
      element.click();
    } catch (NoSuchElementException e) {
      throw new Exception(elementDescription + " not found in page!!");
    }
  }
  
  public static void actionClick(WebElement element, WebDriver driver, String elementDescription)
    throws Exception
  {
    if (!Utils.waitForElement(driver, element, 5)) {
      throw new Exception(elementDescription + " not found in page!!");
    }
    try {
      Actions actions = new Actions(driver);
      actions.moveToElement(element).click(element).build().perform();
    } catch (NoSuchElementException e) {
      throw new Exception(elementDescription + " not found in page!!");
    }
  }
  
  public static void javascriptClick(WebElement element, WebDriver driver, String elementDescription) throws Exception
  {
    if (!Utils.waitForElement(driver, element, 5)) {
      throw new Exception(elementDescription + " not found in page!!");
    }
    try {
      JavascriptExecutor executor = (JavascriptExecutor)driver;
      executor.executeScript("arguments[0].click();", new Object[] { element });
    } catch (NoSuchElementException e) {
      throw new Exception(elementDescription + " not found in page!!");
    }
  }
  













  public static String getText(WebDriver driver, WebElement fromWhichTxtShldExtract, String elementDescription)
    throws Exception
  {
    String textFromHTMLAttribute = "";
    try
    {
      textFromHTMLAttribute = fromWhichTxtShldExtract.getText().trim();
      
      if (textFromHTMLAttribute.isEmpty()) {
        textFromHTMLAttribute = 
          fromWhichTxtShldExtract.getAttribute("textContent");
      }
    } catch (NoSuchElementException e) {
      throw new Exception(elementDescription + " not found in page!!");
    }
    
    return textFromHTMLAttribute;
  }
  















  public static String getText(WebDriver driver, String fromWhichTxtShldExtract, String elementDescription)
    throws Exception
  {
    String textFromHTMLAttribute = "";
    
    WebElement element = checkLocator(driver, fromWhichTxtShldExtract);
    try
    {
      textFromHTMLAttribute = element.getText().trim();
      
      if (textFromHTMLAttribute.isEmpty()) {
        textFromHTMLAttribute = element.getAttribute("textContent");
      }
    } catch (NoSuchElementException e) {
      throw new Exception(elementDescription + " not found in page!!");
    }
    
    return textFromHTMLAttribute;
  }
  

















  public static String getTextFromAttribute(WebDriver driver, WebElement fromWhichTxtShldExtract, String attributeName, String elementDescription)
    throws Exception
  {
    String textFromHTMLAttribute = "";
    try
    {
      textFromHTMLAttribute = 
        fromWhichTxtShldExtract.getAttribute(attributeName).trim();
    } catch (NoSuchElementException e) {
      throw new Exception(elementDescription + " not found in page!!");
    }
    
    return textFromHTMLAttribute;
  }
  


















  public static String getTextFromAttribute(WebDriver driver, String fromWhichTxtShldExtract, String attributeName, String elementDescription)
    throws Exception
  {
    String textFromHTMLAttribute = "";
    WebElement element = checkLocator(driver, fromWhichTxtShldExtract);
    try
    {
      textFromHTMLAttribute = element.getAttribute(attributeName).trim();
    } catch (NoSuchElementException e) {
      throw new Exception(elementDescription + " not found in page!!");
    }
    
    return textFromHTMLAttribute;
  }
  

















  public static void selectFromComboBox(String btn, String optToSelect, WebDriver driver, String elementDescription)
    throws Exception
  {
    WebElement element = checkLocator(driver, btn);
    if (!Utils.waitForElement(driver, element, 1)) {
      throw new Exception(elementDescription + " not found in page!!");
    }
    try {
      Select selectBox = new Select(element);
      selectBox.selectByValue(optToSelect);
    } catch (NoSuchElementException e) {
      throw new Exception(elementDescription + " not found in page!!");
    }
  }
  
















  public static void selectFromComboBox(WebElement btn, String optToSelect, WebDriver driver, String elementDescription)
  {
    if (!Utils.waitForElement(driver, btn, 1)) {
      throw new SkipException(elementDescription + " not found in page!!");
    }
    try {
      Select selectBox = new Select(btn);
      selectBox.selectByValue(optToSelect);
    } catch (NoSuchElementException e) {
      throw new SkipException(elementDescription + " not found in page!!");
    }
  }
  










  public static void selectDropDownValue(WebDriver driver, WebElement dropDown, String valueToBeSelected)
  {
    dropDown.click();
    By valueBy = By.xpath("//li[@data-label='" + valueToBeSelected + "']");
    for (WebElement element : driver.findElements(valueBy)) {
      if ((valueToBeSelected.equals(element.getText())) && 
        (element.isDisplayed())) {
        element.click();
        break;
      }
    }
  }
  
  public static void mouseHover(WebDriver driver, WebElement element) {
    Actions actions = new Actions(driver);
    actions.moveToElement(element).clickAndHold(element).build().perform();
  }
  
  public static void javaScriptMouseHover(WebDriver driver, WebElement element) {
    String javaScript = 
      "var evObj = document.createEvent('MouseEvents');evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);arguments[0].dispatchEvent(evObj);";
    



    ((JavascriptExecutor)driver).executeScript(javaScript, new Object[] { element });
  }
  
  public static void javaScriptMouseHoverForIE(WebDriver driver, WebElement element) {
    String code = "var fireOnThis = arguments[0];var evObj = document.createEvent('MouseEvents');evObj.initEvent( 'mouseover', true, true );fireOnThis.dispatchEvent(evObj);";
    
    ((JavascriptExecutor)driver).executeScript(code, new Object[] { element });
  }
  


  public static void selectRadioOrCheckbox(WebElement element, String enableOrDisable)
  {
    if (("YES".equalsIgnoreCase(enableOrDisable)) && 
      (!isRadioOrCheckBoxSelected(element))) {
      element.click();
    }
    
    if (("NO".equalsIgnoreCase(enableOrDisable)) && 
      (isRadioOrCheckBoxSelected(element))) {
      element.click();
    }
  }
  
  public static boolean isRadioOrCheckBoxSelected(WebElement element)
  {
    if (element.getAttribute("class").contains("active")) {
      return true;
    }
    
    if (element.getAttribute("checked") != null) {
      return true;
    }
    
    for (WebElement childElement : element.findElements(By.xpath(".//*"))) {
      if (childElement.getAttribute("class").contains("active")) {
        return true;
      }
    }
    
    return false;
  }
  
  public static String getRadioOrCheckboxChecked(WebElement element) {
    if (element.getAttribute("class").contains("active")) {
      return "Yes";
    }
    
    if (element.getAttribute("checked") != null) {
      return "Yes";
    }
    
    for (WebElement childElement : element.findElements(By.xpath(".//*"))) {
      if (childElement.getAttribute("class").contains("active")) {
        return "Yes";
      }
    }
    
    return "No";
  }
  







  public static List<WebElement> checkLocators(WebDriver driver, String locator)
  {
    List<WebElement> elements = null;
    if (locator.startsWith("//")) {
      elements = 
      



        (List)new WebDriverWait(driver, 10L).pollingEvery(500L, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class, StaleElementReferenceException.class).withMessage("Couldn't find " + locator).until(
        ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
    } else {
      elements = 
      



        (List)new WebDriverWait(driver, 10L).pollingEvery(500L, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class, StaleElementReferenceException.class).withMessage("Couldn't find " + locator).until(
        ExpectedConditions.visibilityOfAllElementsLocatedBy(
        By.cssSelector(locator)));
    }
    return elements;
  }
  






  public static WebElement checkLocator(WebDriver driver, String locator)
  {
    WebElement element = null;
    if (locator.startsWith("//")) {
      element = 
      



        (WebElement)new WebDriverWait(driver, 10L).pollingEvery(500L, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class, StaleElementReferenceException.class).withMessage("Couldn't find " + locator).until(ExpectedConditions.visibilityOfElementLocated(
        By.xpath(locator)));
    } else {
      element = 
      



        (WebElement)new WebDriverWait(driver, 10L).pollingEvery(500L, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class, StaleElementReferenceException.class).withMessage("Couldn't find " + locator).until(ExpectedConditions.visibilityOfElementLocated(
        By.cssSelector(locator)));
    }
    return element;
  }
  
  public static WebElement checkLocator1(WebDriver driver, String locator) {
    WebElement element = null;
    
    String elementType = locator.split("\\|")[0];
    locator = locator.split("\\|")[1];
    try {
      String str1;
      switch ((str1 = elementType).hashCode()) {case -1548231387:  if (str1.equals("tagname")) {} break; case -8935421:  if (str1.equals("classname")) {} break; case 3355:  if (str1.equals("id")) break; break; case 98819:  if (str1.equals("css")) {} break; case 3373707:  if (str1.equals("name")) {} break; case 114256029:  if (str1.equals("xpath")) {} break; case 1195141159:  if (!str1.equals("linktext")) {
          return element;element = driver.findElement(By.id(locator));
          return element;
          
          element = driver.findElement(By.name(locator));
          return element;
          
          element = driver.findElement(By.cssSelector(locator));
          return element;
          
          element = driver.findElement(By.xpath(locator));
        }
        else {
          element = driver.findElement(By.linkText(locator));
          return element;
          
          element = driver.findElement(By.className(locator));
          return element;
          
          element = driver.findElement(By.tagName(locator));
        }
        break; }
    } catch (NoSuchElementException e) {
      Log.fail("Cannot find " + locator + "  --> Please give appropriate locator in locator file.");
    }
    return element;
  }
  
  public static List<WebElement> checkLocators1(WebDriver driver, String locator) {
    List<WebElement> element = null;
    
    String elementType = locator.split("\\|")[0];
    locator = locator.split("\\|")[1];
    try {
      String str1;
      switch ((str1 = elementType).hashCode()) {case -1548231387:  if (str1.equals("tagname")) {} break; case -8935421:  if (str1.equals("classname")) {} break; case 3355:  if (str1.equals("id")) break; break; case 98819:  if (str1.equals("css")) {} break; case 3373707:  if (str1.equals("name")) {} break; case 114256029:  if (str1.equals("xpath")) {} break; case 1195141159:  if (!str1.equals("linktext")) {
          return element;element = driver.findElements(By.id(locator));
          return element;
          
          element = driver.findElements(By.name(locator));
          return element;
          
          element = driver.findElements(By.cssSelector(locator));
          return element;
          
          element = driver.findElements(By.xpath(locator));
        }
        else {
          element = driver.findElements(By.linkText(locator));
          return element;
          
          element = driver.findElements(By.className(locator));
          return element;
          
          element = driver.findElements(By.tagName(locator));
        }
        break; }
    } catch (NoSuchElementException e) {
      Log.fail("Cannot find " + locator + "  --> Please give appropriate locator in locator file.");
    }
    return element;
  }
  





  public static void moveToElementJS(WebDriver driver, WebElement element)
  {
    ((JavascriptExecutor)driver).executeScript(MOUSE_HOVER_JS, new Object[] { element });
  }
  











  public static WebElement getMachingTextElementFromList(List<WebElement> elements, String contenttext, String condition)
    throws Exception
  {
    WebElement elementToBeReturned = null;
    boolean found = false;
    if (elements.size() > 0) {
      for (WebElement element : elements) {
        if (condition.toLowerCase().equals("equals"))
        {
          if (element.getText().trim().replaceAll("\\s+", " ").equalsIgnoreCase(contenttext)) {
            elementToBeReturned = element;
            found = true;
            break;
          }
        }
        if (condition.toLowerCase().equals("contains"))
        {
          if (element.getText().trim().replaceAll("\\s+", " ").contains(contenttext)) {
            elementToBeReturned = element;
            found = true;
            break;
          } }
      }
      if (!found) {
        throw new Exception("Didn't find the correct text(" + 
          contenttext + ")..! on the page");
      }
    } else {
      throw new Exception("Unable to find list element...!");
    }
    return elementToBeReturned;
  }
  




  public static void openNewTab(WebDriver driver)
  {
    driver.findElement(By.cssSelector("body")).sendKeys(new CharSequence[] { Keys.CONTROL + "t" });
  }
  
  public static void navigateToBack(WebDriver driver) {
    driver.navigate().back();
    Utils.waitForPageLoad(driver);
  }
  
  public static void selectDropdownByIndex(WebElement element)
  {
    Select selectByIndex = new Select(element);
    selectByIndex.selectByIndex(0);
  }
  






  public static boolean isElementPresent(WebDriver driver, WebElement locator)
  {
    try
    {
      locator.isDisplayed();
      return true;
    } catch (Exception e) {}
    return false;
  }
  

  public static final void fillFormDetails(LinkedHashMap<String, WebElement> instructions, WebDriver driver)
    throws Exception
  {
    try
    {
      Set billingDetailsSet = instructions.entrySet();
      Iterator billingDetailsIterator = billingDetailsSet.iterator();
      label266:
      while (billingDetailsIterator.hasNext())
      {
        Map.Entry mapEntry = (Map.Entry)billingDetailsIterator.next();
        String[] keyWithElementTypeAndDescriptionAndTextToType = mapEntry.getKey().toString().split("_");
        WebElement locator = (WebElement)mapEntry.getValue();
        String str;
        switch ((str = keyWithElementTypeAndDescriptionAndTextToType[0].toLowerCase()).hashCode()) {case -906021636:  if (str.equals("select")) {} break; case 3575610:  if (str.equals("type")) break; break; case 94627080:  if (str.equals("check")) {} break; case 94750088:  if (!str.equals("click"))
          {
            break label266;
            typeOnTextField(locator, keyWithElementTypeAndDescriptionAndTextToType[2], driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
          }
          else
          {
            clickOnElement(locator, driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
            

            break label266;
            
            Select select = new Select(locator);
            select.selectByVisibleText(keyWithElementTypeAndDescriptionAndTextToType[2]);
            Log.event("Selected(" + keyWithElementTypeAndDescriptionAndTextToType[2] + ") from " + keyWithElementTypeAndDescriptionAndTextToType[1]);
            
            break label266;
            
            selectRadioOrCheckbox(locator, keyWithElementTypeAndDescriptionAndTextToType[2]);
          }
          
          break;
        }
        
        Utils.waitForPageLoad(driver);
      }
    }
    catch (Exception localException) {}
  }
  




  public static final void executeInstructions(LinkedHashMap<String, String> instructions, WebDriver driver)
    throws Exception
  {
    try
    {
      Set instructionSet = instructions.entrySet();
      Iterator instructionIterator = instructionSet.iterator();
      label278:
      while (instructionIterator.hasNext())
      {
        Map.Entry mapEntry = (Map.Entry)instructionIterator.next();
        String[] keyWithElementTypeAndDescriptionAndTextToType = mapEntry.getKey().toString().split("_");
        WebElement locator = checkLocator1(driver, EnvironmentPropertiesReader.getInstance("elements").getProperty(mapEntry.getValue().toString()));
        String str;
        switch ((str = keyWithElementTypeAndDescriptionAndTextToType[0].toLowerCase()).hashCode()) {case -906021636:  if (str.equals("select")) {} break; case 3575610:  if (str.equals("type")) break; break; case 94627080:  if (str.equals("check")) {} break; case 94750088:  if (!str.equals("click"))
          {
            break label278;
            typeOnTextField(locator, keyWithElementTypeAndDescriptionAndTextToType[2], driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
          }
          else
          {
            clickOnElement(locator, driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
            
            break label278;
            
            Select select = new Select(locator);
            select.selectByVisibleText(keyWithElementTypeAndDescriptionAndTextToType[2]);
            Log.event("Selected(" + keyWithElementTypeAndDescriptionAndTextToType[2] + ") from " + keyWithElementTypeAndDescriptionAndTextToType[1]);
            
            break label278;
            
            selectRadioOrCheckbox(locator, keyWithElementTypeAndDescriptionAndTextToType[2]);
          }
          
          break;
        }
        
        Utils.waitForPageLoad(driver);
      }
    }
    catch (Exception localException) {}
  }
}
