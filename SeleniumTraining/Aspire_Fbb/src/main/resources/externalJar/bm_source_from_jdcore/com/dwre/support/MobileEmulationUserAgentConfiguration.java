package com.dwre.support;

import java.util.HashMap;














public class MobileEmulationUserAgentConfiguration
{
  public static final String APPLE_IPHONE_IOS8 = "iphone6plus_ios8_mobile";
  public static final String APPLE_IPAD_IOS9 = "ipad4_ios9_tablet";
  public static final String ANDROID_GALAXY_S5 = "android_galaxy_s5";
  private final HashMap<String, String> apple_iphone6plus_ios8 = new HashMap() {};
  









  private final HashMap<String, String> apple_ipad4_ios9 = new HashMap() {};
  








  private final HashMap<String, String> android_galaxy_s5 = new HashMap() {};
  






  public MobileEmulationUserAgentConfiguration() {}
  





  public HashMap<String, HashMap<String, String>> setUserAgentConfigurationValue()
  {
    HashMap<String, HashMap<String, String>> userAgentData = new HashMap();
    
    userAgentData.put("iphone6plus_ios8_mobile", apple_iphone6plus_ios8);
    userAgentData.put("ipad4_ios9_tablet", apple_ipad4_ios9);
    userAgentData.put("android_galaxy_s5", android_galaxy_s5);
    
    return userAgentData;
  }
  







  public String getUserAgent(String deviceName)
  {
    String dataToBeReturned = null;
    HashMap<String, HashMap<String, String>> getUserAgent = setUserAgentConfigurationValue();
    dataToBeReturned = hasDeviceName(deviceName) ? (String)((HashMap)getUserAgent.get(deviceName)).get("userAgent") : null;
    return dataToBeReturned;
  }
  







  public String getDeviceWidth(String deviceName)
  {
    String dataToBeReturned = null;
    HashMap<String, HashMap<String, String>> getDeviceWidth = setUserAgentConfigurationValue();
    dataToBeReturned = hasDeviceName(deviceName) ? (String)((HashMap)getDeviceWidth.get(deviceName)).get("width") : null;
    return dataToBeReturned;
  }
  







  public String getDeviceHeight(String deviceName)
  {
    String dataToBeReturned = null;
    HashMap<String, HashMap<String, String>> getDeviceHeight = setUserAgentConfigurationValue();
    dataToBeReturned = hasDeviceName(deviceName) ? (String)((HashMap)getDeviceHeight.get(deviceName)).get("height") : null;
    return dataToBeReturned;
  }
  







  public String getDevicePixelRatio(String deviceName)
  {
    String dataToBeReturned = null;
    HashMap<String, HashMap<String, String>> getDevicePixelRatio = setUserAgentConfigurationValue();
    dataToBeReturned = hasDeviceName(deviceName) ? (String)((HashMap)getDevicePixelRatio.get(deviceName)).get("pixelRatio") : 
      null;
    return dataToBeReturned;
  }
  








  private boolean hasDeviceName(String deviceName)
  {
    HashMap<String, HashMap<String, String>> hasDeviceName = setUserAgentConfigurationValue();
    return hasDeviceName.containsKey(deviceName);
  }
  













  public String getDeviceNameFromMobileEmulation(String userAgent, String pixelRatio, String width, String height)
  {
    String dataToBeReturned = null;
    boolean found = false;
    HashMap<String, HashMap<String, String>> getDeviceData = setUserAgentConfigurationValue();
    for (Object usKey : getDeviceData.keySet()) {
      if ((((String)((HashMap)getDeviceData.get(usKey)).get("userAgent")).equals(userAgent)) && 
        (((String)((HashMap)getDeviceData.get(usKey)).get("pixelRatio")).equals(pixelRatio)) && 
        (((String)((HashMap)getDeviceData.get(usKey)).get("width")).equals(width)) && 
        (((String)((HashMap)getDeviceData.get(usKey)).get("height")).equals(height))) {
        dataToBeReturned = (String)usKey;
        found = true;
      }
    }
    if (!found) {
      dataToBeReturned = null;
    }
    return dataToBeReturned;
  }
}
