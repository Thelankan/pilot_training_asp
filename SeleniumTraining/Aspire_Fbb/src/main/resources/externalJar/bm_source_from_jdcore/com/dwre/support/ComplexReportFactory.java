package com.dwre.support;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import java.util.Map;

public class ComplexReportFactory
{
  public static ExtentReports reporter;
  
  public ComplexReportFactory() {}
  
  public static Map<Long, String> threadToExtentTestMap = new java.util.HashMap();
  public static Map<String, ExtentTest> nameToTestMap = new java.util.HashMap();
  
  private static synchronized ExtentReports getExtentReport() {
    if (reporter == null)
    {

      reporter = new ExtentReports("ComplexReport.html", Boolean.valueOf(true), com.relevantcodes.extentreports.DisplayOrder.NEWEST_FIRST);
    }
    return reporter;
  }
  

  public static synchronized ExtentTest getTest(String testName, String testDescription)
  {
    if (!nameToTestMap.containsKey(testName)) {
      Long threadID = Long.valueOf(Thread.currentThread().getId());
      ExtentTest test = getExtentReport().startTest(testName, testDescription);
      nameToTestMap.put(testName, test);
      threadToExtentTestMap.put(threadID, testName);
    }
    return (ExtentTest)nameToTestMap.get(testName);
  }
  
  public static synchronized ExtentTest getTest(String testName) {
    return getTest(testName, "");
  }
  















  public static synchronized ExtentTest getTest()
  {
    Long threadID = Long.valueOf(Thread.currentThread().getId());
    
    if (threadToExtentTestMap.containsKey(threadID)) {
      String testName = (String)threadToExtentTestMap.get(threadID);
      return (ExtentTest)nameToTestMap.get(testName);
    }
    

    return null;
  }
  
  public static synchronized void closeTest(String testName)
  {
    if (!testName.isEmpty()) {
      ExtentTest test = getTest(testName);
      getExtentReport().endTest(test);
    }
  }
  
  public static synchronized void closeTest(ExtentTest test) {
    if (test != null) {
      getExtentReport().endTest(test);
    }
  }
  
  public static synchronized void closeTest() {
    ExtentTest test = getTest();
    closeTest(test);
  }
  
  public static synchronized void closeReport() {
    if (reporter != null) {
      reporter.flush();
      reporter.close();
    }
  }
}
