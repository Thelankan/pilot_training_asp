package com.dwre.support;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;








public class XmlUtils
{
  public XmlUtils() {}
  
  public static NodeList getNodeListFromXML(String xmlLocation, String originNode)
    throws Exception
  {
    File fXmlFile = new File(xmlLocation);
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    Document doc = dBuilder.parse(fXmlFile);
    
    NodeList nList = doc.getElementsByTagName(originNode);
    return nList;
  }
  




  public static NodeList getNodeListFromNode(Node node)
    throws Exception
  {
    NodeList nList = node.getChildNodes();
    return nList;
  }
  





  public static LinkedHashMap<String, Object> getDataFromXML(String xmlLocation, String tagName)
    throws Exception
  {
    LinkedHashMap<String, Object> dataToReturn = new LinkedHashMap();
    NodeList nList = getNodeListFromXML(xmlLocation, tagName);
    
    for (int index = 0; index < nList.getLength(); index++)
    {
      dataToReturn.put(getTagNameFromNode(nList.item(index)) + "-" + (index + 1), getNodeData(nList.item(index)));
    }
    
    return dataToReturn;
  }
  





  public static LinkedHashMap<String, String> getDataFromXMLAsString(String xmlLocation, String tagName)
    throws Exception
  {
    LinkedHashMap<String, String> dataToReturn = new LinkedHashMap();
    NodeList nList = getNodeListFromXML(xmlLocation, tagName);
    Node node = nList.item(0);
    if (node.getChildNodes().getLength() > 1) {
      NodeList nList1 = node.getChildNodes();
      for (int index = 0; index < nList1.getLength(); index++) {
        if (!nList1.item(index).toString().contains("text")) {
          dataToReturn = MapUtils.mergeHashMaps(getNodeData(nList1.item(index)), dataToReturn);
        }
      }
    } else if (!node.toString().contains("text")) {
      dataToReturn.put(getTagNameFromNode(node), getTagContentFromNode(node));
    }
    
    return dataToReturn;
  }
  





  public static LinkedList<LinkedHashMap<String, String>> getDataFromXMLLL(String xmlLocation, String tagName)
    throws Exception
  {
    LinkedList<LinkedHashMap<String, String>> dataToReturn = new LinkedList();
    LinkedHashMap<String, String> hashMap = new LinkedHashMap();
    
    NodeList nList = getNodeListFromXML(xmlLocation, tagName);
    for (int i = 0; i < nList.getLength(); i++) {
      Node node = nList.item(0);
      if (node.getChildNodes().getLength() > 1) {
        NodeList nList1 = node.getChildNodes();
        for (int index = 0; index < nList1.getLength(); index++) {
          if (!nList1.item(index).toString().contains("text")) {
            hashMap = MapUtils.mergeHashMaps(getNodeData(nList1.item(index)), hashMap);
          }
        }
      } else if (!node.toString().contains("text")) {
        hashMap.put(getTagNameFromNode(node), getTagContentFromNode(node));
      }
      dataToReturn.add(hashMap);
    }
    return dataToReturn;
  }
  





  public static String getTagNameFromNode(Node node)
    throws Exception
  {
    Element element = (Element)node;
    return element.getTagName();
  }
  




  public static String getTagContentFromNode(Node node)
    throws Exception
  {
    Element element = (Element)node;
    return element.getTextContent();
  }
  




  public static LinkedHashMap<String, String> getNodeData(Node node)
    throws Exception
  {
    LinkedHashMap<String, String> dataToReturn = new LinkedHashMap();
    
    if (node.getChildNodes().getLength() > 1) {
      NodeList nList = node.getChildNodes();
      for (int index = 0; index < nList.getLength(); index++) {
        if (!nList.item(index).toString().contains("text")) {
          dataToReturn = MapUtils.mergeHashMaps(getNodeData(nList.item(index)), dataToReturn);
        }
      }
    } else if (!node.toString().contains("text")) {
      dataToReturn.put(getTagNameFromNode(node), getTagContentFromNode(node));
    }
    

    return dataToReturn;
  }
  






  public static LinkedHashMap<String, String> getNodeData(String xmlLocation, String parentNode, String childNode)
    throws Exception
  {
    LinkedHashMap<String, String> dataToReturn = new LinkedHashMap();
    NodeList nList = getNodeListFromXML(xmlLocation, parentNode);
    Element parent = (Element)nList.item(0);
    Node child = parent.getElementsByTagName(childNode).item(0);
    
    dataToReturn.put(getTagNameFromNode(child), getTagContentFromNode(child));
    return dataToReturn;
  }
  






  public static LinkedList<LinkedHashMap<String, String>> getNodeDataAsLL(String xmlLocation, String parentNode, String childNode)
    throws Exception
  {
    LinkedList<LinkedHashMap<String, String>> dataToReturn = new LinkedList();
    LinkedHashMap<String, String> hashMap = new LinkedHashMap();
    
    NodeList nList = getNodeListFromXML(xmlLocation, parentNode);
    for (int i = 0; i < nList.getLength(); i++) {
      Element parent = (Element)nList.item(0);
      Node child = parent.getElementsByTagName(childNode).item(0);
      hashMap.put(getTagNameFromNode(child), getTagContentFromNode(child));
      dataToReturn.add(hashMap);
    }
    
    return dataToReturn;
  }
  
  public static String getAttributeValue(Node node, String attribute) throws Exception {
    return node.getAttributes().getNamedItem(attribute).getNodeValue();
  }
  
  public static String testmethod(String xml, String childTag, String childTagText, String parentTag, String attribute, String searchTag, String requiredTag) throws Exception {
    NodeList nList = getNodeListFromXML(xml, childTag);
    

    Node node = null;
    for (int i = 0; i < nList.getLength(); i++) {
      if (getTagContentFromNode(nList.item(i)).equals(childTagText)) {
        node = nList.item(i);
      }
    }
    

    Node parent = null;
    do {
      parent = node.getParentNode();
    } while (!parent.getNodeValue().equals(parentTag));
    
    String attributeValue = getAttributeValue(parent, attribute);
    NodeList searchList = getNodeListFromXML(xml, searchTag);
    

    Node searchNode = null;
    for (int i = 0; i < searchList.getLength(); i++) {
      if (getAttributeValue(searchList.item(i), attribute).equals(attributeValue)) {
        searchNode = searchList.item(i);
      }
    }
    
    NodeList childNodes = getNodeListFromNode(searchNode);
    Node requiredNode = null;
    for (int i = 0; i < nList.getLength(); i++) {
      if (getTagNameFromNode(childNodes.item(i)).equals(requiredTag)) {
        requiredNode = nList.item(i);
      }
    }
    
    return getTagContentFromNode(requiredNode);
  }
}
