package com.dwre.support;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.xml.XmlTest;

public class DataProviderUtils
{
  private static EnvironmentPropertiesReader configProperty = ;
  
  public DataProviderUtils() {}
  
  @DataProvider(parallel=true)
  public static Iterator<Object[]> parallelTestDataProvider(ITestContext context) throws IOException { List<Object[]> dataToBeReturned = new java.util.ArrayList();
    List<String> browsers = null;
    List<String> platforms = null;
    List<String> browserVersions = null;
    List<String> deviceNames = null;
    String driverInitilizeInfo = null;
    String browser = null;
    String platform = null;
    String browserVersion = null;
    String deviceName = null;
    Iterator<String> browsersIt = null;
    Iterator<String> browserVersionsIt = null;
    Iterator<String> platformsIt = null;
    Iterator<String> deviceNameIt = null;
    

    if ((configProperty.hasProperty("runSauceLabFromLocal")) && 
      (configProperty.getProperty("runSauceLabFromLocal").equalsIgnoreCase("true"))) {
      browser = configProperty.hasProperty("browserName") ? configProperty.getProperty("browserName") : null;
      browserVersion = configProperty.hasProperty("browserVersion") ? configProperty.getProperty("browserVersion") : 
        null;
      platform = configProperty.hasProperty("platform") ? configProperty.getProperty("platform") : null;
      
      browsers = Arrays.asList(browser.split("\\|"));
      browserVersions = Arrays.asList(browserVersion.split("\\|"));
      platforms = Arrays.asList(platform.split("\\|"));
      
      browsersIt = browsers.iterator();
      browserVersionsIt = browserVersions.iterator();
      platformsIt = platforms.iterator();
      

      if ((configProperty.hasProperty("runUserAgentDeviceTest")) && 
        (configProperty.getProperty("runUserAgentDeviceTest").equalsIgnoreCase("true")))
      {

        deviceName = configProperty.hasProperty("deviceName") ? configProperty.getProperty("deviceName") : null;
        deviceNames = Arrays.asList(deviceName.split("\\|"));
        deviceNameIt = deviceNames.iterator();
        
        do
        {
          browser = (String)browsersIt.next();
          browserVersion = (String)browserVersionsIt.next();
          platform = (String)platformsIt.next();
          deviceName = (String)deviceNameIt.next();
          driverInitilizeInfo = browser + "&" + browserVersion + "&" + platform + "&" + deviceName;
          dataToBeReturned.add(new Object[] { driverInitilizeInfo });
          if ((!browsersIt.hasNext()) || (!browserVersionsIt.hasNext()) || (!platformsIt.hasNext())) break;
        } while (deviceNameIt.hasNext());


      }
      else
      {


        do
        {

          browser = (String)browsersIt.next();
          browserVersion = (String)browserVersionsIt.next();
          platform = (String)platformsIt.next();
          driverInitilizeInfo = browser + "&" + browserVersion + "&" + platform;
          dataToBeReturned.add(new Object[] { driverInitilizeInfo });
          if ((!browsersIt.hasNext()) || (!browserVersionsIt.hasNext())) break; } while (platformsIt.hasNext());

      }
      


    }
    else
    {


      browsers = Arrays.asList(context.getCurrentXmlTest().getParameter("browserName").split(","));
      for (String b : browsers) {
        dataToBeReturned.add(new Object[] { b });
      }
    }
    return dataToBeReturned.iterator();
  }
}
