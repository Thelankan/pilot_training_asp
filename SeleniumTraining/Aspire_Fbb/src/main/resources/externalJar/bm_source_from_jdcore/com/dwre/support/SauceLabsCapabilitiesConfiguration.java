package com.dwre.support;

import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;











public class SauceLabsCapabilitiesConfiguration
{
  private String browserName;
  private String platform;
  private String screenResolution;
  private String browserVersion;
  private String testName;
  private String build;
  private String deviceOrientation;
  private String preRun;
  private String seleniumVersion;
  private String iedriverVersion;
  private String chromedriverVersion;
  private String maxTestDuration;
  private String commandTimeout;
  private String idleTimeout;
  private String deviceName;
  private boolean recordVideo = true;
  private boolean recordNetwork = true;
  private boolean recordSnapshot = true;
  private boolean videoUploadOnPass = true;
  private boolean autoAcceptAlerts = true;
  
  private static MobileEmulationUserAgentConfiguration mobEmuUA = new MobileEmulationUserAgentConfiguration();
  
  public SauceLabsCapabilitiesConfiguration(String testName, String build, String browserName, String browserVersion, String platform, String screenResolution)
  {
    this.testName = testName;
    this.build = build;
    this.browserName = browserName;
    this.platform = platform;
    this.browserVersion = browserVersion;
    this.screenResolution = screenResolution;
  }
  
  public SauceLabsCapabilitiesConfiguration(String testName, String build)
  {
    this.testName = testName;
    this.build = build;
  }
  
  public String getTestName() {
    return testName;
  }
  
  public void setTestName(String testName) {
    this.testName = testName;
  }
  
  public String getBuild() {
    return build;
  }
  
  public void setBuild(String build) {
    this.build = build;
  }
  
  public String getBrowserName() {
    return browserName;
  }
  
  public void setBrowserName(String browserName) {
    this.browserName = browserName;
  }
  
  public String getBrowserVersion() {
    return browserVersion;
  }
  
  public void setBrowserVersion(String browserVersion) {
    this.browserVersion = browserVersion;
  }
  
  public void setSeleniumVersion(String seleniumVersion) {
    this.seleniumVersion = seleniumVersion;
  }
  
  public void setIeDriverVersion(String iedriverVersion) {
    this.iedriverVersion = iedriverVersion;
  }
  
  public void setChromeDriverVersion(String chromedriverVersion) {
    this.chromedriverVersion = chromedriverVersion;
  }
  
  public void setMaxTestDuration(String maxTestDuration) {
    this.maxTestDuration = maxTestDuration;
  }
  
  public String getPlatform() {
    return platform;
  }
  
  public void setPlatform(String platform) {
    this.platform = platform;
  }
  
  public String getScreenResolution() {
    return screenResolution;
  }
  
  public void setScreenResolution(String screenResolution) {
    this.screenResolution = screenResolution;
  }
  
  public boolean isRecordVideo() {
    return recordVideo;
  }
  
  public void setRecordVideo(boolean recordVideo) {
    this.recordVideo = recordVideo;
  }
  
  public boolean isRecordNetwork() {
    return recordNetwork;
  }
  
  public void setRecordNetwork(boolean recordNetwork) {
    this.recordNetwork = recordNetwork;
  }
  
  public boolean isRecordSnapshot() {
    return recordSnapshot;
  }
  
  public void setRecordSnapshot(boolean recordSnapshot) {
    this.recordSnapshot = recordSnapshot;
  }
  
  public boolean isVideoUploadOnPass() {
    return videoUploadOnPass;
  }
  
  public void setVideoUploadOnPass(boolean videoUploadOnPass) {
    this.videoUploadOnPass = videoUploadOnPass;
  }
  
  public void setautoAcceptAlerts(boolean autoAcceptAlerts)
  {
    this.autoAcceptAlerts = autoAcceptAlerts;
  }
  
  public String getDeviceOrientation() {
    return deviceOrientation;
  }
  
  public void setDeviceOrientation(String deviceOrientation) {
    this.deviceOrientation = deviceOrientation;
  }
  
  public String getPreRun() {
    return preRun;
  }
  
  public void setPreRun(String preRun) {
    this.preRun = preRun;
  }
  
  public String getCommandTimeout() {
    return commandTimeout;
  }
  
  public void setCommandTimeout(String commandTimeout) {
    this.commandTimeout = commandTimeout;
  }
  
  public String getIdleTimeout() {
    return idleTimeout;
  }
  
  public void setIdleTimeout(String idleTimeout) {
    this.idleTimeout = idleTimeout;
  }
  
  public String getDeviceName() {
    return deviceName;
  }
  
  public void setDeviceName(String deviceName) {
    this.deviceName = deviceName;
  }
  
  public DesiredCapabilities getDesiredCapabilities() {
    DesiredCapabilities caps = new DesiredCapabilities();
    
    if (((System.getProperty("SELENIUM_DRIVER") != null) && (System.getProperty("SELENIUM_DRIVER").contains("internet explorer"))) || ((System.getenv("SELENIUM_DRIVER") != null) && 
      (System.getenv("SELENIUM_DRIVER").contains("internet explorer")))) {
      caps = DesiredCapabilities.internetExplorer();
      caps.setCapability("enablePersistentHover", false);
      caps.setCapability("ignoreZoomSetting", true);
      caps.setCapability("nativeEvents", false);
      caps.setCapability("ignoreProtectedModeSettings", true);
      caps.setCapability("ie.ensureCleanSession", true);
      caps.setCapability("ignoreProtectedModeSettings", true);
    }
    

    if (((System.getProperty("SELENIUM_DRIVER") != null) && (System.getProperty("SELENIUM_DRIVER").contains("safari"))) || ((System.getenv("SELENIUM_DRIVER") != null) && 
      (System.getenv("SELENIUM_DRIVER").contains("safari")))) {
      Log.event(">>> INSIDE SAFARI CAPABILITIES <<<");
      caps = DesiredCapabilities.safari();
      caps.setCapability("prerun", "https://gist.githubusercontent.com/saucyallison/3a73a4e0736e556c990d/raw/d26b0195d48b404628fc12342cb97f1fc5ff58ec/disable_fraud.sh");
      screenResolution = null;
    }
    
    caps.setCapability("name", testName);
    caps.setCapability("build", build);
    caps.setCapability("recordScreenshots", recordSnapshot);
    caps.setCapability("recordVideo", recordVideo);
    caps.setCapability("videoUploadOnPass", videoUploadOnPass);
    caps.setCapability("autoAcceptAlerts", autoAcceptAlerts);
    
    if (browserName != null) {
      caps.setBrowserName(browserName);
    }
    
    if (platform != null) {
      caps.setCapability("platform", platform);
    }
    
    if (browserVersion != null) {
      caps.setVersion(browserVersion);
    }
    if (screenResolution != null) {
      caps.setCapability("screenResolution", screenResolution);
    }
    if (deviceOrientation != null) {
      caps.setCapability("deviceOrientation", deviceOrientation);
    }
    if (preRun != null) {
      caps.setCapability("prerun", preRun);
    }
    






    if (seleniumVersion != null) {
      caps.setCapability("seleniumVersion", seleniumVersion);
    }
    
    if (iedriverVersion != null) {
      caps.setCapability("iedriverVersion", iedriverVersion);
    }
    

    if (chromedriverVersion != null) {
      caps.setCapability("chromedriverVersion", chromedriverVersion);
    }
    

    if (maxTestDuration != null) {
      caps.setCapability("maxDuration", maxTestDuration);
    }
    
    if (commandTimeout != null) {
      caps.setCapability("commandTimeout", commandTimeout);
    }
    
    if (idleTimeout != null) {
      caps.setCapability("idleTimeout", idleTimeout);
    }
    
    if (deviceName != null) {
      caps.setCapability("deviceName", deviceName);
    }
    return caps;
  }
  


















  public DesiredCapabilities getUserAgentDesiredCapabilities(SauceLabsCapabilitiesConfiguration caps, String deviceName, String userAgent)
  {
    Map<String, Object> deviceMetrics = new HashMap();
    Map<String, Object> mobileEmulation = new HashMap();
    
    int width = 0;
    int height = 0;
    Double pixRatio = null;
    
    width = Integer.valueOf(mobEmuUA.getDeviceWidth(deviceName)).intValue();
    height = Integer.valueOf(mobEmuUA.getDeviceHeight(deviceName)).intValue();
    pixRatio = Double.valueOf(mobEmuUA.getDevicePixelRatio(deviceName));
    
    deviceMetrics.put("width", Integer.valueOf(width));
    deviceMetrics.put("height", Integer.valueOf(height));
    deviceMetrics.put("pixelRatio", pixRatio);
    mobileEmulation.put("deviceMetrics", deviceMetrics);
    mobileEmulation.put("userAgent", userAgent);
    Log.event("mobileEmulation settings::==> " + mobileEmulation);
    
    DesiredCapabilities uaCaps = DesiredCapabilities.chrome();
    ChromeOptions opt = new ChromeOptions();
    opt.setExperimentalOption("mobileEmulation", mobileEmulation);
    uaCaps = caps.setChromeOption(opt);
    return uaCaps;
  }
  








  public DesiredCapabilities setChromeOption(ChromeOptions options)
  {
    DesiredCapabilities sauceWithChromeCab = getDesiredCapabilities();
    sauceWithChromeCab.setCapability("goog:chromeOptions", options);
    return sauceWithChromeCab;
  }
}
