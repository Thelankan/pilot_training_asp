package com.dwre.support;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;













public class FolderZiper
{
  public FolderZiper() {}
  
  private static void addFileToZip(String path, String srcFile, ZipOutputStream zip)
    throws Exception
  {
    File folder = new File(srcFile);
    
    if (folder.isDirectory()) {
      addFolderToZip(path, srcFile, zip);
    } else {
      byte[] buf = new byte['Ѐ'];
      
      FileInputStream in = new FileInputStream(srcFile);
      zip.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
      int len; while ((len = in.read(buf)) > 0) { int len;
        zip.write(buf, 0, len);
      }
      in.close();
    }
  }
  









  private static void addFileToZipWithoutFolder(String path, String srcFile, ZipOutputStream zip)
    throws Exception
  {
    byte[] buf = new byte['Ѐ'];
    
    try
    {
      FileInputStream in = new FileInputStream(srcFile);
      
      zip.putNextEntry(new ZipEntry(new File(srcFile).getName()));
      int len;
      while ((len = in.read(buf)) > 0) { int len;
        zip.write(buf, 0, len);
      }
      
      in.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }
  








  private static void addFolderToZip(String path, String srcFolder, ZipOutputStream zip)
    throws Exception
  {
    File folder = new File(srcFolder);
    
    for (String fileName : folder.list()) {
      if (path.isEmpty()) {
        addFileToZip(folder.getName(), srcFolder + "/" + fileName, zip);
      } else {
        addFileToZip(path + "/" + folder.getName(), srcFolder + "/" + fileName, zip);
      }
    }
  }
  










  public void zipFolder(List<String> srcFolders, String destZipFile)
    throws Exception
  {
    ZipOutputStream zip = null;
    FileOutputStream fileWriter = null;
    
    fileWriter = new FileOutputStream(destZipFile);
    zip = new ZipOutputStream(fileWriter);
    
    for (String srcFolder : srcFolders) {
      if (new File(srcFolder).isDirectory()) {
        addFolderToZip("", srcFolder, zip);
      } else {
        addFileToZipWithoutFolder(new File(srcFolder).getName(), srcFolder, zip);
      }
      
      zip.flush();
    }
    
    zip.close();
  }
}
