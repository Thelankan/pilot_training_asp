package com.dwre.pages.orders;

import com.dwre.support.BrowserActions;
import com.dwre.support.EnvironmentPropertiesReader;
import com.dwre.support.Log;
import com.dwre.support.Utils;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;





public class OrderConfirmationPage
  extends LoadableComponent<OrderConfirmationPage>
{
  private static EnvironmentPropertiesReader orderProperty = EnvironmentPropertiesReader.getInstance("order");
  
  private WebDriver driver;
  private boolean isPageLoaded;
  
  public OrderConfirmationPage(WebDriver driver)
  {
    this.driver = driver;
    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
    PageFactory.initElements(finder, this);
  }
  

  protected void isLoaded()
  {
    Utils.waitForPageLoad(driver);
    
    if (!isPageLoaded) {
      Assert.fail();
    }
    
    Utils.waitForPageLoad(driver);
    WebElement orderNo = BrowserActions.checkLocator(driver, orderProperty.getProperty("orderNumber"));
    if ((isPageLoaded) && (!Utils.waitForElement(driver, orderNo))) {
      Log.fail("Order Confirmation Page did not open up. Site might be down.", driver);
    }
  }
  


  protected void load()
  {
    isPageLoaded = true;
    Utils.waitForPageLoad(driver);
    try
    {
      Utils.switchWindows(driver, "OrderConfirmation Page", "title", "false");
    }
    catch (Exception localException) {}
    
    Utils.waitForElement(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("orderNumber")));
  }
  




















  public LinkedList<LinkedHashMap<String, String>> getCustomerDetails()
    throws Exception
  {
    LinkedList<LinkedHashMap<String, String>> sFCustomerDetails = new LinkedList();
    LinkedHashMap<String, String> customerDetails = new LinkedHashMap();
    
    customerDetails.put("first-name", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("billerName")), "billefirstrName").split(" ")[0]);
    customerDetails.put("last-name", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("billerName")), "billerLastName").split(" ")[1]);
    customerDetails.put("address1", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("billerAddress")), "billerAddress"));
    customerDetails.put("city", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("billerCity")), "billerCity").split("\\,")[0]);
    customerDetails.put("country-code", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("billerCity")), "country-code").split("\\,")[1]);
    customerDetails.put("postal-code", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("billerZipCode")), "billerZipCode"));
    customerDetails.put("phone", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("billerPhoneNo")), "billerPhoneNo"));
    
    sFCustomerDetails.add(customerDetails);
    
    return sFCustomerDetails;
  }
  



  public LinkedList<LinkedHashMap<String, String>> getPaymentMethodDetails()
    throws Exception
  {
    LinkedList<LinkedHashMap<String, String>> sFPaymentDetails = new LinkedList();
    LinkedHashMap<String, String> paymentDetails = new LinkedHashMap();
    
    paymentDetails.put("card-type", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("cardType")), "cardTypes"));
    paymentDetails.put("card-holder", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("cardOwner")), "cardOwner"));
    paymentDetails.put("card-number", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("cardNumber")), "cardNumber"));
    paymentDetails.put("amount", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("cardPaidAmount")), "cardPaidAmount"));
    
    sFPaymentDetails.add(paymentDetails);
    
    return sFPaymentDetails;
  }
  




  public LinkedList<LinkedHashMap<String, String>> getShippingDetails()
    throws Exception
  {
    LinkedList<LinkedHashMap<String, String>> sFShippingDetails = new LinkedList();
    

    List<WebElement> shippingElement = BrowserActions.checkLocators(driver, orderProperty.getProperty("shippingMethod"));
    for (int i = 0; i < shippingElement.size(); i++) {
      LinkedHashMap<String, String> orderShippingDetails = new LinkedHashMap();
      orderShippingDetails.put("item-id", ((WebElement)shippingElement.get(i)).findElement(By.cssSelector("div[class='shipping-method'] span[class='value']")).getText());
      sFShippingDetails.add(orderShippingDetails);
    }
    
    return sFShippingDetails;
  }
  
  public LinkedList<LinkedHashMap<String, String>> getShippingMethodDetails() throws Exception
  {
    LinkedList<LinkedHashMap<String, String>> sFShippingDetails = new LinkedList();
    

    List<WebElement> shippingElement = BrowserActions.checkLocators(driver, orderProperty.getProperty("shippingDetails"));
    for (int i = 0; i < shippingElement.size(); i++) {
      LinkedHashMap<String, String> orderShippingDetails = new LinkedHashMap();
      orderShippingDetails.put("first-name", ((WebElement)shippingElement.get(i)).findElement(By.cssSelector("span.payment-name")).getText().split(" ")[0]);
      orderShippingDetails.put("last-name", ((WebElement)shippingElement.get(i)).findElement(By.cssSelector("span.payment-name")).getText().split(" ")[1]);
      orderShippingDetails.put("address1", ((WebElement)shippingElement.get(i)).findElement(By.cssSelector("span.payment-addr")).getText());
      orderShippingDetails.put("city", ((WebElement)shippingElement.get(i)).findElement(By.cssSelector("span.payment-city")).getText().split("\\,")[0]);
      orderShippingDetails.put("postal-code", ((WebElement)shippingElement.get(i)).findElement(By.cssSelector("span.payment-city")).getText().split("\\,")[1].split(" ")[1]);
      orderShippingDetails.put("country-code", ((WebElement)shippingElement.get(i)).findElement(By.cssSelector("span.payment-city")).getText().split("\\,")[1].split(" ")[0]);
      orderShippingDetails.put("phone", ((WebElement)shippingElement.get(i)).findElement(By.cssSelector("a[class*=formatphone]")).getText());
      sFShippingDetails.add(orderShippingDetails);
    }
    
    return sFShippingDetails;
  }
  




  public LinkedList<LinkedHashMap<String, String>> getPaymentDetails()
    throws Exception
  {
    LinkedList<LinkedHashMap<String, String>> sFPaymentDetails = new LinkedList();
    
    LinkedHashMap<String, String> paymentDetails = new LinkedHashMap();
    paymentDetails.put("merchandiseTotal", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("merchandiseTotal")), "merchandiseTotal"));
    paymentDetails.put("salexTax", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("salexTax")), "salexTax"));
    paymentDetails.put("shippingTax", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("shippingTax")), "shippingTax"));
    paymentDetails.put("orderTotal", BrowserActions.getText(driver, BrowserActions.checkLocator(driver, orderProperty.getProperty("orderTotal")), "orderTotal"));
    
    sFPaymentDetails.add(paymentDetails);
    return sFPaymentDetails;
  }
  





  public LinkedList<LinkedHashMap<String, String>> getProductDetails()
    throws Exception
  {
    LinkedList<LinkedHashMap<String, String>> sFProductDetails = new LinkedList();
    List<WebElement> lblProductList = BrowserActions.checkLocators(driver, orderProperty.getProperty("productDetails"));
    
    for (int i = 0; i < lblProductList.size(); i++) {
      LinkedHashMap<String, String> product = new LinkedHashMap();
      
      product.put("productName", ((WebElement)lblProductList.get(i)).findElement(By.cssSelector(".name > a")).getText());
      product.put("productUpc", ((WebElement)lblProductList.get(i)).findElement(By.cssSelector(".product-UPC")).getText());
      product.put("productColor", ((WebElement)lblProductList.get(i)).findElement(By.cssSelector("div[data-attribute='color'] .value")).getText());
      product.put("productSize", ((WebElement)lblProductList.get(i)).findElement(By.cssSelector("div[data-attribute='size'] .value")).getText());
      product.put("productQty", ((WebElement)lblProductList.get(i)).findElement(By.cssSelector(".value.qty-value")).getText());
      
      String price = new String();
      if (((WebElement)lblProductList.get(i)).getAttribute("innerHTML").contains("Clearance")) {
        price = 
          ((WebElement)lblProductList.get(i)).findElement(By.cssSelector(".now-price")).getText().replace(" ", "").trim().split("\\$")[1].trim();
      } else if (((WebElement)lblProductList.get(i)).getAttribute("innerHTML").contains("Now")) {
        price = ((WebElement)lblProductList.get(i)).findElement(By.cssSelector(".now-price")).getText().trim().split("\\$")[1].trim();
      } else if (((WebElement)lblProductList.get(i)).getAttribute("innerHTML").contains("Sale")) {
        price = ((WebElement)lblProductList.get(i)).findElement(By.cssSelector(".now-price")).getText().trim().split("\\$")[1].trim();
      } else if (Utils.waitForElement(driver, ((WebElement)lblProductList.get(i)).findElement(By.cssSelector(".standard-price")))) {
        price = ((WebElement)lblProductList.get(i)).findElement(By.cssSelector(".standard-price")).getText().trim().replace("$", "");
      }
      
      product.put("productPrice", price);
      product.put("productSubTotal", ((WebElement)lblProductList.get(i)).findElement(By.cssSelector("div[class='subtotal-price']")).getText());
      
      sFProductDetails.add(product);
    }
    

    return sFProductDetails;
  }
}
