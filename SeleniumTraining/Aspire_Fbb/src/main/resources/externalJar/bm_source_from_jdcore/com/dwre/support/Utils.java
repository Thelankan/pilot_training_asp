package com.dwre.support;

import java.io.PrintStream;
import java.text.Collator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utils
{
  private static EnvironmentPropertiesReader configProperty = ;
  public static int maxElementWait = Integer.parseInt(configProperty.getProperty("maxElementWait"));
  


  public Utils() {}
  

  public static void waitForPageLoad(WebDriver driver)
  {
    waitForPageLoad(driver, WebDriverFactory.maxPageLoadWait);
  }
  







  public static void waitForPageLoad(WebDriver driver, int maxWait)
  {
    long startTime = StopWatch.startTime();
    FluentWait<WebDriver> wait = new WebDriverWait(driver, maxWait).pollingEvery(500L, java.util.concurrent.TimeUnit.MILLISECONDS)
      .ignoring(org.openqa.selenium.StaleElementReferenceException.class).withMessage("Page Load Timed Out");
    try
    {
      if (configProperty.getProperty("documentLoad").equalsIgnoreCase("true")) {
        wait.until(WebDriverFactory.documentLoad);
      }
      if (configProperty.getProperty("imageLoad").equalsIgnoreCase("true")) {
        wait.until(WebDriverFactory.imagesLoad);
      }
      if (configProperty.getProperty("framesLoad").equalsIgnoreCase("true")) {
        wait.until(WebDriverFactory.framesLoad);
      }
      String title = driver.getTitle().toLowerCase();
      String url = driver.getCurrentUrl().toLowerCase();
      String pageSource = null;
      try {
        pageSource = driver.findElement(org.openqa.selenium.By.cssSelector("body")).getText().toLowerCase();
      } catch (Exception e) {
        BrowserActions.nap(2L);
        pageSource = driver.findElement(org.openqa.selenium.By.cssSelector("body")).getText().toLowerCase();
      }
      Log.event("Page URL:: " + url);
      if (pageSource.contains("this site can’t be reached")) {
        driver.navigate().refresh();
        try {
          org.openqa.selenium.Alert alert = driver.switchTo().alert();
          System.out.println(alert.getText());
          alert.accept();
        } catch (Exception e) {
          Log.event("Error :: " + e);
        }
      }
      
      if (("the page cannot be found".equalsIgnoreCase(title)) || (title.contains("is not available")) || 
        (url.contains("/error/")) || (url.toLowerCase().contains("/errorpage/"))) {
        org.testng.Assert.fail("Site is down. [Title: " + title + ", URL:" + url + "]");
      }
    }
    catch (org.openqa.selenium.TimeoutException e) {
      driver.navigate().refresh();
      wait.until(WebDriverFactory.documentLoad);
      wait.until(WebDriverFactory.imagesLoad);
      wait.until(WebDriverFactory.framesLoad);
    }
    Log.event("Page Load Wait: (Sync)", StopWatch.elapsedTime(startTime));
  }
  










  public static String getTestOrientation()
  {
    String dataToBeReturned = null;
    boolean checkExecutionOnSauce = false;
    boolean checkDeviceExecution = false;
    checkExecutionOnSauce = (System.getProperty("SELENIUM_DRIVER") != null) || 
      (System.getenv("SELENIUM_DRIVER") != null);
    
    if (checkExecutionOnSauce) {
      checkDeviceExecution = (System.getProperty("runUserAgentDeviceTest") != null) && 
        (System.getProperty("runUserAgentDeviceTest").equalsIgnoreCase("true"));
      if (checkDeviceExecution) {
        dataToBeReturned = System.getProperty("deviceOrientation") != null ? 
          System.getProperty("deviceOrientation") : "no sauce run system variable: deviceOrientation ";
      } else {
        dataToBeReturned = "sauce browser test: no orientation";
      }
    } else {
      checkDeviceExecution = (configProperty.hasProperty("runUserAgentDeviceTest")) && 
        (configProperty.getProperty("runUserAgentDeviceTest").equalsIgnoreCase("true"));
      if (checkDeviceExecution) {
        dataToBeReturned = configProperty.hasProperty("deviceOrientation") ? 
          configProperty.getProperty("deviceOrientation") : 
          "no local run config variable: deviceOrientation ";
      } else {
        dataToBeReturned = "local browser test: no orientation";
      }
    }
    return dataToBeReturned;
  }
  








  public static boolean waitForElement(WebDriver driver, WebElement element)
  {
    return waitForElement(driver, element, maxElementWait);
  }
  










  public static boolean waitForElement(WebDriver driver, WebElement element, int maxWait)
  {
    boolean statusOfElementToBeReturned = false;
    long startTime = StopWatch.startTime();
    WebDriverWait wait = new WebDriverWait(driver, maxWait);
    try {
      WebElement waitElement = (WebElement)wait.until(org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf(element));
      if ((waitElement.isDisplayed()) && (waitElement.isEnabled())) {
        statusOfElementToBeReturned = true;
        Log.event("Element is displayed:: " + element.toString());
      }
    } catch (Exception e) {
      statusOfElementToBeReturned = false;
      Log.event("Unable to find a element after " + StopWatch.elapsedTime(startTime) + " sec ==> " + 
        element.toString());
    }
    return statusOfElementToBeReturned;
  }
  
  public static WebDriver switchWindows(WebDriver driver, String windowToSwitch, String opt, String closeCurrentDriver)
    throws Exception
  {
    WebDriver currentWebDriver = driver;
    WebDriver assingedWebDriver = driver;
    boolean windowFound = false;
    ArrayList<String> multipleWindows = new ArrayList(assingedWebDriver.getWindowHandles());
    
    for (int i = 0; i < multipleWindows.size(); i++)
    {
      assingedWebDriver.switchTo().window((String)multipleWindows.get(i));
      
      if (opt.equals("title")) {
        if (assingedWebDriver.getTitle().trim().equals(windowToSwitch)) {
          windowFound = true;
          break;
        }
      } else if ((opt.equals("url")) && 
        (assingedWebDriver.getCurrentUrl().contains(windowToSwitch))) {
        windowFound = true;
        break;
      }
    }
    


    if (!windowFound) {
      throw new Exception("Window: " + windowToSwitch + ", not found!!");
    }
    if (closeCurrentDriver.equals("true")) {
      currentWebDriver.close();
    }
    
    return assingedWebDriver;
  }
  






  public static void switchToNewWindow(WebDriver driver)
  {
    String winHandle = driver.getWindowHandle();
    for (String index : driver.getWindowHandles()) {
      if (!index.equals(winHandle)) {
        driver.switchTo().window(index);
        break;
      }
    }
    if (!((RemoteWebDriver)driver).getCapabilities().getBrowserName().matches(".*safari.*")) {
      ((JavascriptExecutor)driver).executeScript(
        "if(window.screen){window.moveTo(0, 0); window.resizeTo(window.screen.availWidth, window.screen.availHeight);};", new Object[0]);
    }
  }
  





  public static void switchToWindow(String windowHandle, WebDriver driver)
  {
    for (String index : driver.getWindowHandles()) {
      if (!index.equals(windowHandle)) {
        driver.switchTo().window(index);
        break;
      }
    }
    if (!((RemoteWebDriver)driver).getCapabilities().getBrowserName().matches(".*safari.*")) {
      ((JavascriptExecutor)driver).executeScript(
        "if(window.screen){window.moveTo(0, 0); window.resizeTo(window.screen.availWidth, window.screen.availHeight);};", new Object[0]);
    }
  }
  










  public static boolean compareTwoHashMap(Map<String, String> expectedList, Map<String, String> actualList, String[]... noNeed)
  {
    List<String> missedkey = new ArrayList();
    HashMap<String, String> missedvalue = new HashMap();
    try {
      for (String k : expectedList.keySet()) {
        if (!((String)actualList.get(k)).equalsIgnoreCase((String)expectedList.get(k))) {
          missedvalue.put(k, (String)actualList.get(k));
          Log.failsoft("Missed Values:: Expected List:: " + missedvalue);
          return false;
        }
      }
      for (String y : actualList.keySet()) {
        if (!expectedList.containsKey(y)) {
          missedkey.add(y);
          Log.failsoft("Missed keys:: Actual List:: " + missedkey);
          return false;
        }
      }
    } catch (NullPointerException np) {
      return false;
    }
    return true;
  }
  











  public static boolean compareTwoLinkedListHashMap(LinkedList<LinkedHashMap<String, String>> expectedList, LinkedList<LinkedHashMap<String, String>> actualList, String[]... noNeed)
  {
    int size = expectedList.size();
    if (noNeed.length > 0) {
      for (int i = 0; i < noNeed.length; i++) {
        System.out.println("Removed Key from Expected:: " + noNeed[i][i]);
        expectedList.remove(noNeed[i][i]);
        System.out.println("Removed Key from Actual  :: " + noNeed[i][i]);
        actualList.remove(noNeed[i][i]);
      }
    }
    boolean flag = true;
    try {
      for (int i = 0; i < size; i++) {
        if (!compareTwoHashMap((Map)expectedList.get(i), (Map)actualList.get(i), new String[0][]))
          flag = false;
      }
    } catch (NullPointerException np) {
      return false;
    }
    return flag;
  }
  










  public static boolean compareTwoList(List<String> expectedElements, List<String> actualElements)
  {
    boolean statusToBeReturned = false;
    List<String> uniqueList = new ArrayList();
    List<String> missedList = new ArrayList();
    for (String item : expectedElements) {
      if (actualElements.contains(item)) {
        uniqueList.add(item);
      } else {
        missedList.add(item);
      }
    }
    


    if (expectedElements.equals(actualElements)) {
      Log.event("All elements checked on this page:: " + uniqueList);
      statusToBeReturned = true;
    } else {
      Log.failsoft("Missing element on this page:: " + missedList);
      statusToBeReturned = false;
    }
    return statusToBeReturned;
  }
  
  public static boolean compareTwoList1(List<String> expectedElements, List<String> actualElements) {
    boolean statusToBeReturned = false;
    List<String> missedList = new ArrayList();
    for (int i = 0; i < expectedElements.size(); i++) {
      if (!((String)expectedElements.get(i)).equals(actualElements.get(i))) {
        statusToBeReturned = false;
        missedList.add((String)expectedElements.get(i));
      }
    }
    Log.failsoft("Missing element on this page:: " + missedList);
    return statusToBeReturned;
  }
  






  public static LinkedList<LinkedHashMap<String, String>> sortLinkedListProduct(LinkedList<LinkedHashMap<String, String>> actualList)
    throws Exception
  {
    LinkedList<LinkedHashMap<String, String>> listToReturn = new LinkedList();
    
    actualList = makeUnique(actualList);
    
    LinkedList<String> list = new LinkedList();
    int size = actualList.size();
    
    for (int x = 0; x < size; x++) {
      list.add((String)((LinkedHashMap)actualList.get(x)).get("ProductName"));
    }
    java.util.Collections.sort(list, new java.util.Comparator()
    {
      public int compare(String o1, String o2) {
        return Collator.getInstance().compare(o1, o2);
      }
    });
    
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        if (((String)list.get(i)).equals(((LinkedHashMap)actualList.get(j)).get("ProductName"))) {
          listToReturn.add((LinkedHashMap)actualList.get(j));
        }
      }
    }
    






    return listToReturn;
  }
  






  public static LinkedList<LinkedHashMap<String, String>> sortLinkedListAddress(LinkedList<LinkedHashMap<String, String>> actualList)
    throws Exception
  {
    LinkedList<LinkedHashMap<String, String>> listToReturn = new LinkedList();
    actualList = makeUniqueAddress(actualList);
    LinkedList<String> list = new LinkedList();
    int size = actualList.size();
    for (int x = 0; x < size; x++) {
      list.add((String)((LinkedHashMap)actualList.get(x)).get("FirstName"));
    }
    java.util.Collections.sort(list, new java.util.Comparator()
    {
      public int compare(String o1, String o2) {
        return Collator.getInstance().compare(o1, o2);
      }
    });
    
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        if (actualList.size() == 1) {
          LinkedHashMap<String, String> listToAdd = new LinkedHashMap((Map)actualList.get(i));
          listToReturn.add(listToAdd);
        }
        else if (((String)list.get(i)).equals(((LinkedHashMap)actualList.get(j)).get("FirstName"))) {
          LinkedHashMap<String, String> listToAdd = new LinkedHashMap((Map)actualList.get(i));
          listToReturn.add(listToAdd);
        }
      }
    }
    







    return listToReturn;
  }
  








  public static LinkedList<LinkedHashMap<String, String>> makeUnique(LinkedList<LinkedHashMap<String, String>> hashMap)
    throws Exception
  {
    int nosProduct = hashMap.size();
    for (int i = 0; i < nosProduct; i++) {
      for (int j = i + 1; j < nosProduct; j++) {
        if ((((String)((LinkedHashMap)hashMap.get(i)).get("ProductName")).equals(((LinkedHashMap)hashMap.get(j)).get("ProductName"))) && 
          (((String)((LinkedHashMap)hashMap.get(i)).get("Color")).equals(((LinkedHashMap)hashMap.get(j)).get("Color"))) && 
          (((String)((LinkedHashMap)hashMap.get(i)).get("Size")).equals(((LinkedHashMap)hashMap.get(j)).get("Size")))) {
          int qty = Integer.parseInt((String)((LinkedHashMap)hashMap.get(i)).get("Quantity")) + 
            Integer.parseInt((String)((LinkedHashMap)hashMap.get(j)).get("Quantity"));
          ((LinkedHashMap)hashMap.get(i)).put("Quantity", Integer.toString(qty));
          hashMap.remove(j);
          nosProduct = hashMap.size();
          j--;
        }
      }
    }
    
    return hashMap;
  }
  
  public static LinkedList<LinkedHashMap<String, String>> makeUniqueAddress(LinkedList<LinkedHashMap<String, String>> hashMap) throws Exception
  {
    int nosProduct = hashMap.size();
    for (int i = 0; i < nosProduct; i++) {
      for (int j = i + 1; j < nosProduct; j++) {
        System.out.print("i value " + (String)((LinkedHashMap)hashMap.get(i)).get("FirstName"));
        System.out.print("j value " + (String)((LinkedHashMap)hashMap.get(j)).get("FirstName"));
        if (hashMap.toString().contains("FirstName")) {
          if ((((String)((LinkedHashMap)hashMap.get(i)).get("FirstName")).equals(((LinkedHashMap)hashMap.get(j)).get("FirstName"))) && 
            (((String)((LinkedHashMap)hashMap.get(i)).get("Address")).equals(((LinkedHashMap)hashMap.get(j)).get("Address")))) {
            hashMap.remove(j);
            nosProduct = hashMap.size();
            j--;
          }
        }
        else {
          hashMap.remove(j);
          nosProduct = hashMap.size();
          j--;
        }
      }
    }
    return hashMap;
  }
  










  public static boolean verifyCssPropertyForElement(WebElement element, String cssProperty, String actualValue)
  {
    boolean result = false;
    String actualClassProperty = element.getCssValue(cssProperty);
    if (actualClassProperty.trim().contains(actualValue)) {
      result = true;
    }
    return result;
  }
  








  public static String getValueOfInputField(WebElement element, WebDriver driver)
  {
    String sDataToBeReturned = null;
    if (waitForElement(driver, element)) {
      sDataToBeReturned = element.getAttribute("value");
    }
    return sDataToBeReturned;
  }
  











  public static boolean waitForDisabledElement(WebDriver driver, WebElement element, int maxWait)
  {
    boolean statusOfElementToBeReturned = false;
    long startTime = StopWatch.startTime();
    WebDriverWait wait = new WebDriverWait(driver, maxWait);
    try {
      WebElement waitElement = (WebElement)wait.until(org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf(element));
      if (!waitElement.isEnabled()) {
        statusOfElementToBeReturned = true;
        Log.event("Element is displayed and disabled:: " + element.toString());
      }
    } catch (Exception ex) {
      statusOfElementToBeReturned = false;
      Log.event("Unable to find disabled element after " + StopWatch.elapsedTime(startTime) + " sec ==> " + 
        element.toString());
    }
    return statusOfElementToBeReturned;
  }
  










  public static boolean waitUntilElementDisappear(WebDriver driver, WebElement element)
  {
    WebDriverWait wait = new WebDriverWait(driver, 60L);
    boolean isNotDisplayed = ((Boolean)wait.until(new org.openqa.selenium.support.ui.ExpectedCondition()
    {
      public Boolean apply(WebDriver webDriver) {
        boolean isPresent = false;
        try {
          if (isDisplayed()) {
            isPresent = false;
            Log.event("Element " + toString() + ", is still visible in page");
          }
        } catch (Exception ex) {
          isPresent = true;
          Log.event("Element " + toString() + ", is not displayed in page ");
          return Boolean.valueOf(isPresent);
        }
        return Boolean.valueOf(isPresent);
      }
    })).booleanValue();
    return isNotDisplayed;
  }
  










  public static boolean waitUntilElementDisappear(WebDriver driver, WebElement element, int maxWait)
  {
    WebDriverWait wait = new WebDriverWait(driver, maxWait);
    boolean isNotDisplayed = ((Boolean)wait.until(new org.openqa.selenium.support.ui.ExpectedCondition()
    {
      public Boolean apply(WebDriver webDriver) {
        boolean isPresent = false;
        try {
          if (isDisplayed()) {
            isPresent = false;
            Log.event("Element " + toString() + ", is still visible in page");
          }
        } catch (Exception ex) {
          isPresent = true;
          Log.event("Element " + toString() + ", is not displayed in page ");
          return Boolean.valueOf(isPresent);
        }
        return Boolean.valueOf(isPresent);
      }
    })).booleanValue();
    return isNotDisplayed;
  }
  




  public static String getRunPlatForm()
  {
    String dataToBeReturned = null;
    
    if (((configProperty.hasProperty("runMobile")) && 
      (configProperty.getProperty("runMobile").equalsIgnoreCase("true"))) || 
      ((configProperty.hasProperty("runUserAgentDeviceTest")) && 
      (configProperty.getProperty("runUserAgentDeviceTest").equalsIgnoreCase("true"))) || (
      (System.getenv("runUserAgentDeviceTest") != null) && 
      (System.getenv("runUserAgentDeviceTest").equalsIgnoreCase("true"))))
    {
      String device = 
        configProperty.hasProperty("deviceName") ? configProperty.getProperty("deviceName") : System.getenv("deviceName") != null ? System.getenv("deviceName") : null;
      
      dataToBeReturned = device.endsWith("tablet") ? "tablet" : device.endsWith("mobile") ? "mobile" : "desktop";
    } else {
      dataToBeReturned = "desktop";
    }
    Log.event("Running platform type:: " + dataToBeReturned);
    return dataToBeReturned;
  }
  









  public static int getRandom(int min, int max)
    throws Exception
  {
    Random random = new Random();
    int rand;
    int rand; if (min == 0) {
      rand = random.nextInt(max);
    } else {
      rand = java.util.concurrent.ThreadLocalRandom.current().nextInt(min, max);
    }
    return rand;
  }
  






  public static boolean verifyPageURLContains(WebDriver driver, String hostURL, String stringContains)
  {
    boolean status = false;
    String url = null;
    try {
      url = driver.getCurrentUrl();
      if (url == null) {
        url = ((JavascriptExecutor)driver).executeScript("return document.URL;", new Object[0]).toString();
      }
    } catch (Exception e) {
      url = ((JavascriptExecutor)driver).executeScript("return document.URL;", new Object[0]).toString();
    }
    
    if (url.contains("production")) {
      if ((url.contains(hostURL.split("https://storefront:dcp-preview@")[1])) && (url.contains(stringContains))) {
        status = true;
      }
    } else if (url.contains("wow")) {
      if ((url.contains(hostURL.split("https://storefront:almostthere@")[1])) && (url.contains(stringContains))) {
        status = true;
      }
    } else if (hostURL.contains("https://")) {
      if ((url.contains(hostURL.split("https://")[1])) && (url.contains(stringContains))) {
        status = true;
      }
    } else if ((hostURL.contains("http://")) && 
      (url.contains(hostURL.split("http://")[1])) && (url.contains(stringContains))) {
      status = true;
    }
    

    return status;
  }
  







  public static float round(double d, int decimalPlace)
  {
    return java.math.BigDecimal.valueOf(d).setScale(decimalPlace, 4).floatValue();
  }
  






  public static String checkPriceWithDollar(String price)
  {
    if (!price.startsWith("$")) {
      Log.failsoft(price + " does not contains '$' value");
    }
    
    return price;
  }
  







  public static LinkedHashMap<String, String> copyHashMap(LinkedHashMap<String, String> actual, String ignore)
    throws Exception
  {
    List<String> indexes = new ArrayList(actual.keySet());
    LinkedHashMap<String, String> expected = new LinkedHashMap();
    
    for (int i = 0; i < indexes.size(); i++) {
      if (!((String)indexes.get(i)).equals(ignore)) {
        expected.put((String)indexes.get(i), (String)actual.get(indexes.get(i)));
      }
    }
    return expected;
  }
  







  public static LinkedList<LinkedHashMap<String, String>> copyLinkedListHashMap(LinkedList<LinkedHashMap<String, String>> actual, String ignore)
    throws Exception
  {
    int size = actual.size();
    LinkedList<LinkedHashMap<String, String>> expected = new LinkedList();
    for (int j = 0; j < size; j++) {
      List<String> indexes = new ArrayList(((LinkedHashMap)actual.get(j)).keySet());
      LinkedHashMap<String, String> hashMap = new LinkedHashMap();
      for (int i = 0; i < indexes.size(); i++) {
        if (!((String)indexes.get(i)).equals(ignore))
          hashMap.put((String)indexes.get(i), (String)((LinkedHashMap)actual.get(j)).get(indexes.get(i)));
      }
      expected.add(hashMap);
    }
    return expected;
  }
  
  public static boolean sortCompartPrintPaymentDetails(LinkedHashMap<String, LinkedHashMap<String, String>> paymentDetails1, LinkedHashMap<String, LinkedHashMap<String, String>> paymentDetails2, String[]... ignore)
    throws Exception
  {
    boolean flag = true;
    List<String> outterIndexOfFirst = new ArrayList(paymentDetails1.keySet());
    List<String> outterIndexOfSecond = new ArrayList(paymentDetails2.keySet());
    
    if (outterIndexOfFirst.toString().contains("GiftCard")) {
      LinkedList<LinkedHashMap<String, String>> giftCardDetailsInFirst = new LinkedList();
      LinkedList<LinkedHashMap<String, String>> giftCardDetailsInSecond = new LinkedList();
      
      for (int i = 0; i < outterIndexOfFirst.size(); i++) {
        if (((String)outterIndexOfFirst.get(i)).contains("GiftCard")) {
          giftCardDetailsInFirst.add((LinkedHashMap)paymentDetails1.get(outterIndexOfFirst.get(i)));
        }
        
        if (((String)outterIndexOfSecond.get(i)).contains("GiftCard")) {
          giftCardDetailsInSecond.add((LinkedHashMap)paymentDetails2.get(outterIndexOfSecond.get(i)));
        }
      }
      
      flag = compareTwoLinkedListHashMap(giftCardDetailsInFirst, giftCardDetailsInSecond, ignore);
    }
    

    if (outterIndexOfFirst.toString().contains("BRD")) {
      LinkedList<LinkedHashMap<String, String>> giftCardDetailsInFirst = new LinkedList();
      LinkedList<LinkedHashMap<String, String>> giftCardDetailsInSecond = new LinkedList();
      
      for (int i = 0; i < outterIndexOfFirst.size(); i++) {
        if (((String)outterIndexOfFirst.get(i)).contains("BRD")) {
          giftCardDetailsInFirst.add((LinkedHashMap)paymentDetails1.get(outterIndexOfFirst.get(i)));
        }
        
        if (((String)outterIndexOfSecond.get(i)).contains("BRD")) {
          giftCardDetailsInSecond.add((LinkedHashMap)paymentDetails2.get(outterIndexOfSecond.get(i)));
        }
      }
      
      flag = compareTwoLinkedListHashMap(giftCardDetailsInFirst, giftCardDetailsInSecond, new String[0][]);
    }
    
    if (outterIndexOfFirst.toString().contains("CreditCard")) {
      LinkedList<LinkedHashMap<String, String>> giftCardDetailsInFirst = new LinkedList();
      LinkedList<LinkedHashMap<String, String>> giftCardDetailsInSecond = new LinkedList();
      
      for (int i = 0; i < outterIndexOfFirst.size(); i++) {
        if (((String)outterIndexOfFirst.get(i)).contains("CreditCard")) {
          giftCardDetailsInFirst.add((LinkedHashMap)paymentDetails1.get(outterIndexOfFirst.get(i)));
        }
        
        if (((String)outterIndexOfSecond.get(i)).contains("CreditCard")) {
          giftCardDetailsInSecond.add((LinkedHashMap)paymentDetails2.get(outterIndexOfSecond.get(i)));
        }
      }
      
      flag = compareTwoLinkedListHashMap(giftCardDetailsInFirst, giftCardDetailsInSecond, new String[0][]);
    }
    
    return flag;
  }
  






  public static String getBrowserName(WebDriver driver)
    throws Exception
  {
    String browserName = ((RemoteWebDriver)driver).getCapabilities().getBrowserName();
    return browserName;
  }
  




  public static boolean compareAndPrintTableLinkedListHashMap(String title, String col1Head, String col2Head, LinkedList<LinkedHashMap<String, String>> hashMap1, LinkedList<LinkedHashMap<String, String>> hashMap2, String[]... noNeed)
    throws Exception
  {
    int iteration2 = hashMap2.size();
    boolean flag = true;
    for (int j = 0; j < iteration2; j++) {
      if (noNeed.length > 0) {
        for (int i = 0; i < noNeed[0].length; i++) {
          System.out.println("Removed Key from Expected:: " + 
            noNeed[0][i]);
          ((LinkedHashMap)hashMap1.get(j)).remove(noNeed[0][i]);
          System.out.println("Removed Key from Actual  :: " + 
            noNeed[0][i]);
          ((LinkedHashMap)hashMap2.get(j)).remove(noNeed[0][i]);
        }
      }
      
      Log.message("<br>");
      Log.message1("<table><tr bgcolor='#BBBBBB' align='center'><td colspan=3><b><font color='black'>" + 
        title + "(" + (j + 1) + ")</font></b></td></tr>");
      Log.message1("<tr align='center' bgcolor='#BBBBBB' style='color:black;'><td>Contents</td><td>" + 
        col1Head + "</td><td>" + col2Head + "</td></tr>");
      List<String> indexes1 = new ArrayList(((LinkedHashMap)hashMap1.get(j))
        .keySet());
      List<String> indexes2 = new ArrayList(((LinkedHashMap)hashMap2.get(j))
        .keySet());
      List<String> maxIndex = indexes1.size() > indexes2.size() ? indexes1 : 
        indexes2;
      for (int i = 0; i < maxIndex.size(); i++) {
        String value1 = ((LinkedHashMap)hashMap1.get(j)).containsKey(maxIndex.get(i)) ? 
          (String)((LinkedHashMap)hashMap1.get(j)).get(maxIndex.get(i)) : 
          "<font color='red'>No Value</font>";
        String value2 = ((LinkedHashMap)hashMap2.get(j)).containsKey(maxIndex.get(i)) ? 
          (String)((LinkedHashMap)hashMap2.get(j)).get(maxIndex.get(i)) : 
          "<font color='red'>No Value</font>";
        if (value1.equals(value2)) {
          Log.message1(
          


            "<tr><td bgcolor='#BBBBBB' style='color:black;'>" + (String)maxIndex.get(i) + "</td><td>" + value1 + "</td><td>" + value2 + "</td></tr>");
        } else
          Log.message1(
          

            "<tr><td bgcolor='#BBBBBB' style='color:black;'>" + (String)maxIndex.get(i) + "</td><td bgcolor='red'>" + value1 + "</td><td>" + value2 + "</td></tr>");
      }
      Log.message1("</table>");
      
      flag = compareTwoHashMap((Map)hashMap1.get(j), (Map)hashMap2.get(j), new String[0][]);
    }
    
    return flag;
  }
  




  public static boolean compareAndPrintTableHashMap(String title, String col1Head, String col2Head, LinkedHashMap<String, String> hashMap1, LinkedHashMap<String, String> hashMap2, String[]... noNeed)
    throws Exception
  {
    if (noNeed.length > 0) {
      for (int i = 0; i < noNeed[0].length; i++) {
        System.out.println("Removed Key from <b>Expected:: " + 
          noNeed[0][i]);
        hashMap1.remove(noNeed[0][i]);
        System.out.println("Removed Key from Actual  :: " + 
          noNeed[0][i]);
        hashMap2.remove(noNeed[0][i]);
      }
    }
    
    Log.message("<br>");
    Log.message1("<table><tr bgcolor='#BBBBBB' align='center'><td colspan=3><b><font color='black'>" + 
      title + "</font></b></td></tr>");
    Log.message1("<tr align='center' bgcolor='#BBBBBB' style='color:black;'><td>Contents</td><td>" + 
      col1Head + "</td><td>" + col2Head + "</td></tr>");
    List<String> indexes1 = new ArrayList(hashMap1.keySet());
    List<String> indexes2 = new ArrayList(hashMap2.keySet());
    List<String> maxIndex = indexes1.size() > indexes2.size() ? indexes1 : 
      indexes2;
    
    for (int i = 0; i < maxIndex.size(); i++) {
      String value1 = hashMap1.containsKey(maxIndex.get(i)) ? 
        (String)hashMap1.get(maxIndex.get(i)) : "No Value";
      String value2 = hashMap2.containsKey(maxIndex.get(i)) ? 
        (String)hashMap2.get(maxIndex.get(i)) : "No Value";
      if (value1.equals(value2)) {
        Log.message1(
        
          "<tr><td bgcolor='#BBBBBB' style='color:black;'>" + (String)maxIndex.get(i) + "</td><td>" + value1 + "</td><td>" + value2 + "</td></tr>");
      } else
        Log.message1(
        
          "<tr><td bgcolor='#BBBBBB' style='color:black;'>" + (String)maxIndex.get(i) + "</td><td bgcolor='red'>" + value1 + "</td><td>" + value2 + "</td></tr>");
    }
    Log.message1("</table>");
    
    return compareTwoHashMap(hashMap1, hashMap2, new String[0][]);
  }
}
