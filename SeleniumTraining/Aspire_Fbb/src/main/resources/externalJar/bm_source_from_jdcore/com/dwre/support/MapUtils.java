package com.dwre.support;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MapUtils
{
  public MapUtils() {}
  
  public static LinkedHashMap<String, String> mergeHashMaps(LinkedHashMap<String, String> sourceMap, LinkedHashMap<String, String> destinationMap)
  {
    List<String> keySet = new ArrayList(sourceMap.keySet());
    for (String key : keySet) {
      destinationMap.put(key, (String)sourceMap.get(key));
    }
    return destinationMap;
  }
  
  public static LinkedHashMap<String, Object> mergeHashMapofObjects(LinkedHashMap<String, Object> sourceMap, LinkedHashMap<String, Object> destinationMap) {
    List<String> keySet = new ArrayList(sourceMap.keySet());
    for (String key : keySet) {
      destinationMap.put(key, sourceMap.get(key));
    }
    return destinationMap;
  }
}
