package com.dwre.support;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;




public class DateTimeUtility
{
  static String sampleDateFormat1 = "yyyy-MM-dd";
  static String sampleDateFormat2 = "MM/dd/yyyy";
  

  public DateTimeUtility() {}
  

  public static String getCurrentDateAndTime()
  {
    SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmmss");
    Date date = new Date();
    String time = sdf.format(date);
    return time;
  }
  





  public static String getCurrentDateAndTimeInLoggerFormat()
  {
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH-mm-ss");
    Date date = new Date();
    String time = sdf.format(date);
    return time;
  }
  




  public static String getCurrentDate()
  {
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String currentdate = sdf.format(date);
    return currentdate;
  }
  





  public static String getCurrentDateAndTimeDDMMYY()
  {
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy hh.mm.ss.SSSSSSSSS a");
    Date date = new Date();
    String time = sdf.format(date);
    return time;
  }
  




  public static String getCurrentYear()
  {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
    Date date = new Date();
    String time = sdf.format(date);
    return time;
  }
  




  public static String getToday()
  {
    Date date = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
    String today = dateFormat.format(Long.valueOf(date.getTime()));
    return today;
  }
  






  public static String getDate(int day)
  {
    int millsInDay = 86400000;
    Date date = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
    String expDate = dateFormat.format(Long.valueOf(date.getTime() + day * millsInDay));
    return expDate;
  }
  




  public static String getTime()
  {
    Date date = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("hhmm");
    String today = dateFormat.format(Long.valueOf(date.getTime()));
    return today;
  }
  




  public static String getYesterday()
  {
    int millsInDay = 86400000;
    Date date = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat(sampleDateFormat2);
    String yesterday = dateFormat.format(Long.valueOf(date.getTime() - 1 * millsInDay));
    return yesterday;
  }
  








  public static String getPrevday(String testDate)
  {
    int millsInDay = 86400000;
    Date date = new Date(testDate);
    SimpleDateFormat dateFormat = new SimpleDateFormat(sampleDateFormat2);
    String yesterday = dateFormat.format(Long.valueOf(date.getTime() - 1 * millsInDay));
    return yesterday;
  }
  








  public static String getNextday(String testDate)
  {
    int millsInDay = 86400000;
    Date date = new Date(testDate);
    SimpleDateFormat dateFormat = new SimpleDateFormat(sampleDateFormat2);
    String yesterday = dateFormat.format(Long.valueOf(date.getTime() + 1 * millsInDay));
    return yesterday;
  }
  







  public static String formatDateToddMMMyy(String dateToformat)
    throws ParseException
  {
    SimpleDateFormat format1 = new SimpleDateFormat(sampleDateFormat2);
    SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yy");
    Date date = format1.parse(dateToformat);
    return format2.format(date);
  }
  








  public static String formatDateToyyyyMMdd(String dateToformat)
    throws ParseException
  {
    SimpleDateFormat format1 = new SimpleDateFormat(sampleDateFormat2);
    SimpleDateFormat format2 = new SimpleDateFormat(sampleDateFormat1);
    Date date = format1.parse(dateToformat);
    return format2.format(date);
  }
  








  public static String formatDateToDDMMYY(String dateToformat)
    throws ParseException
  {
    SimpleDateFormat format1 = new SimpleDateFormat(sampleDateFormat2);
    SimpleDateFormat format2 = new SimpleDateFormat("dd-MM-yy");
    Date date = format1.parse(dateToformat);
    return format2.format(date);
  }
  








  public static String formatDateToDDMMYYYY(String dateToformat)
    throws ParseException
  {
    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-dd-mm");
    SimpleDateFormat format2 = new SimpleDateFormat("dd/mm/yyyy");
    Date date = format1.parse(dateToformat);
    return format2.format(date);
  }
  








  public static String formatDateToddMMYYYY(String dateToformat)
    throws ParseException
  {
    SimpleDateFormat format1 = new SimpleDateFormat(sampleDateFormat1);
    SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
    Date date = format1.parse(dateToformat);
    return format2.format(date);
  }
  







  public static String formatDateToMMDDYYYY(String dateToformat)
    throws ParseException
  {
    SimpleDateFormat format1 = new SimpleDateFormat(sampleDateFormat1);
    SimpleDateFormat format2 = new SimpleDateFormat(sampleDateFormat2);
    Date date = format1.parse(dateToformat);
    return format2.format(date);
  }
  







  public static String formatDateToddMMMYY(String dateToformat)
    throws ParseException
  {
    SimpleDateFormat format1 = new SimpleDateFormat(sampleDateFormat1);
    SimpleDateFormat format2 = new SimpleDateFormat("dd/MMM/yy");
    Date date = format1.parse(dateToformat);
    return format2.format(date);
  }
  




  public static String getTodayddMMyyyy()
  {
    Date date = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    String today = dateFormat.format(Long.valueOf(date.getTime()));
    return today;
  }
  







  public static String formatDateToddMMyyyy(String dateToformat)
    throws ParseException
  {
    SimpleDateFormat format1 = new SimpleDateFormat(sampleDateFormat1);
    SimpleDateFormat format2 = new SimpleDateFormat("dd.MM.yyyy");
    Date date = format1.parse(dateToformat);
    return format2.format(date);
  }
  







  public static String formatDateToMMddyyyy(String dateToformat)
    throws ParseException
  {
    SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yy");
    SimpleDateFormat format2 = new SimpleDateFormat(sampleDateFormat2);
    Date date = format1.parse(dateToformat);
    return format2.format(date);
  }
  







  public static boolean verifyDateTimeFormatddmmyy(String dateToVerify, String datetimeFormat)
  {
    try
    {
      String dateTimeFormatToVerify = datetimeFormat.substring(0, 16);
      SimpleDateFormat dateFormat = new SimpleDateFormat(dateTimeFormatToVerify);
      dateFormat.parse(dateToVerify);
      if (datetimeFormat.length() > 16) {
        String am = dateToVerify.substring(16, 17);
        return ("p".equals(am)) || ("a".equals(am));
      }
      return true;
    }
    catch (Exception e) {}
    return false;
  }
  









  public static ArrayList<String> getTimeForWebclock(String time)
  {
    String[] st1 = time.split(":");
    String st2 = st1[0];
    String st3 = st1[1];
    int j = Integer.parseInt(st3);
    int s = j - 1;
    int d = j + 1;
    String befTime = s;
    String afTime = d;
    if (j == 59) {
      afTime = "00";
    }
    if (j == 0) {
      befTime = "59";
    }
    ArrayList<String> data = new ArrayList();
    data.add(st2);
    data.add(st3);
    data.add(befTime);
    data.add(afTime);
    return data;
  }
  

  public static boolean verifyDateFormat(String dateToVerify, String datetimeFormat)
  {
    boolean status = false;
    try {
      SimpleDateFormat sdf = new SimpleDateFormat(datetimeFormat);
      Date date = sdf.parse(dateToVerify);
      if (dateToVerify.equals(sdf.format(date))) {
        status = true;
      }
    } catch (ParseException ex) {
      ex.printStackTrace();
    }
    return status;
  }
}
