package com.dwre.pages;

import com.dwre.pages.customers.Catalogs;
import com.dwre.pages.customers.CustomerImportExport;
import com.dwre.pages.customers.Customers;
import com.dwre.pages.products.PriceBooks;
import com.dwre.pages.products.ProductImportExport;
import com.dwre.pages.products.ProductSets;
import com.dwre.pages.products.Products;
import com.dwre.pages.products.PromoImportExport;
import com.dwre.pages.products.Promotions;
import com.dwre.pages.products.Stores;
import com.dwre.pages.promotions.Campaigns;
import com.dwre.pages.promotions.ContentSlots;
import com.dwre.pages.promotions.Coupons;
import com.dwre.support.BrowserActions;
import com.dwre.support.DWRE_Utils;
import com.dwre.support.EnvironmentPropertiesReader;
import com.dwre.support.Log;
import com.dwre.support.MerchantTools;
import com.dwre.support.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

public class Navigation
  extends LoadableComponent<Navigation>
{
  private WebDriver driver;
  private boolean isPageLoaded;
  public ElementLayer elementLayer;
  String runPltfrm = Utils.getRunPlatForm();
  @FindBy(css="header[class*='header']")
  WebElement readyElement;
  @FindBy(id="login")
  WebElement LoginPage;
  
  public Navigation(WebDriver driver) { this.driver = driver;
    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 
      Utils.maxElementWait);
    PageFactory.initElements(finder, this);
  }
  

  protected void isLoaded()
  {
    if (!isPageLoaded) {
      Assert.fail();
    }
    if ((isPageLoaded) && (!Utils.waitForElement(driver, readyElement))) {
      Log.fail("Navigation didn't display", driver);
    }
    
    elementLayer = new ElementLayer(driver);
  }
  

  protected void load()
  {
    isPageLoaded = true;
    Utils.waitForPageLoad(driver);
  }
  



  public Object navigateTo(MerchantTools page)
    throws Exception
  {
    Object obj = new Object();
    
    switch (page) {
    case ContentSlots: 
      obj = new Customers(driver).get();
      break;
    case CustomerGroup: 
      new CustomerImportExport(driver).get();
      break;
    
    case CustomerImportExport: 
      obj = new Products(driver).get();
      break;
    case Customers: 
      obj = new ProductSets(driver).get();
      break;
    case Home: 
      obj = new Catalogs(driver).get();
      break;
    case ImportExportOrder: 
      obj = new PriceBooks(driver).get();
      break;
    case ManageSites: 
      new ProductImportExport(driver).get();
      break;
    
    case None: 
      obj = new Promotions(driver).get();
      break;
    case Orders: 
      obj = new Campaigns(driver).get();
      break;
    case PriceBooks: 
      obj = new Coupons(driver).get();
      break;
    case ProductImportExport: 
      obj = new ContentSlots(driver).get();
      break;
    case ProductSets: 
      obj = new Stores(driver).get();
      break;
    case Products: 
      obj = new PromoImportExport(driver).get();
      break;
    case Stores: 
      obj = new ManageSites(driver).get();
      break;
    case Coupons: case PromoImportExport: case Promotions: default: 
      obj = new HomePage(driver);
    }
    
    return obj;
  }
  



  @FindBy(name="LoginForm_Login")
  WebElement txtUserName;
  

  @FindBy(name="LoginForm_Password")
  WebElement txtPassword;
  

  @FindBy(name="login")
  WebElement btnLogin;
  

  public void login(WebDriver driver)
    throws Exception
  {
    if (LoginPage.isDisplayed()) {
      BrowserActions.typeOnTextField(txtUserName, EnvironmentPropertiesReader.getInstance("env").getProperty("userName"), driver, "User Name Field");
      BrowserActions.typeOnTextField(txtPassword, EnvironmentPropertiesReader.getInstance("env").getProperty("password"), driver, "Password Field");
      BrowserActions.clickOnElement(btnLogin, driver, "Login Button");
      Utils.waitForPageLoad(driver);
    }
    
    if ((System.getProperty("ProductSession") != null) && (System.getProperty("ProductSession").equals("false")) && (System.getProperty("product") != null) && (System.getProperty("product") != "") && (!System.getProperty("product").isEmpty())) {
      driver.get(DWRE_Utils.generateProductURL());
    }
  }
}
