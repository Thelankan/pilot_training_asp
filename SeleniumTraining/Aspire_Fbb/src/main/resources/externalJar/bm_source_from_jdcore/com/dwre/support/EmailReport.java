package com.dwre.support;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
import org.testng.ISuite;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.reporters.EmailableReporter2;
import org.testng.reporters.EmailableReporter2.ClassResult;
import org.testng.reporters.EmailableReporter2.MethodResult;
import org.testng.reporters.EmailableReporter2.SuiteResult;
import org.testng.reporters.EmailableReporter2.TestResult;
import org.testng.xml.XmlSuite;

public class EmailReport extends EmailableReporter2
{
  PrintWriter pWriter;
  String fileName;
  static String unescapePattern = "\\<div\\sclass=\"messages\">(.*)\\<\\/div\\>";
  static String unescapeSauceLink = "\\<tr\\sclass=\"param\\sstripe\">(.*)\\<\\/tr\\>";
  static String startTestTitle = "<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">";
  static String endTestTitle = "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div><!-- Report -->";
  static Boolean ignoreMethodeName = Boolean.valueOf(false);
  public static List<ITestResult> allTests = new ArrayList();
  
  public EmailReport() {}
  
  public void generateReport(List<XmlSuite> xml, List<ISuite> suites, String outdir) {
    super.generateReport(xml, suites, outdir);
    File eScripts = new File("jsscripts.txt");
    File eCSS = new File("ReportCSS.txt");
    
    try
    {
      File eReport = new File(outdir + File.separator + "TestAutomationResults.html");
      File eReport1 = new File(outdir + File.separator + "emailable-report.html");
      
      FileUtils.copyFile(eReport, eReport1);
      String eContent = FileUtils.readFileToString(eReport, "UTF-8");
      
      Pattern p = Pattern.compile(unescapePattern, 32);
      Matcher matcher = p.matcher(eContent);
      int matchCount = 0;
      
      while (matcher.find()) {
        matchCount++;
      }
      
      matcher = p.matcher(eContent);
      
      for (int i = 0; i < matchCount; i++) {
        matcher.find();
        String unEscapePart = matcher.group(1);
        unEscapePart = unEscapePart.replace("&lt;", "<");
        unEscapePart = unEscapePart.replace("&gt;", ">");
        unEscapePart = unEscapePart.replace("&quot;", "\"");
        unEscapePart = unEscapePart.replace("&apos;", "'");
        unEscapePart = unEscapePart.replace("&amp;", "&");
        eContent = eContent.replace(matcher.group(1), unEscapePart);
      }
      
      p = Pattern.compile(unescapeSauceLink, 32);
      matcher = p.matcher(eContent);
      matchCount = 0;
      
      while (matcher.find()) {
        matchCount++;
      }
      
      matcher = p.matcher(eContent);
      
      for (int i = 0; i < matchCount; i++) {
        matcher.find();
        String unEscapePart = matcher.group(1);
        unEscapePart = unEscapePart.replace("\"", "");
        unEscapePart = unEscapePart.replace("&lt;", "<");
        unEscapePart = unEscapePart.replace("&gt;", ">");
        unEscapePart = unEscapePart.replace("&quot;", "\"");
        unEscapePart = unEscapePart.replace("&apos;", "'");
        unEscapePart = unEscapePart.replace("&amp;", "&");
        eContent = eContent.replace(matcher.group(1), unEscapePart);
      }
      

      long minStartTime = 0L;
      long maxEndTime = 0L;
      long temp = 0L;
      
      Iterator localIterator2;
      
      for (Iterator localIterator1 = suiteResults.iterator(); localIterator1.hasNext(); 
          








          localIterator2.hasNext())
      {
        EmailableReporter2.SuiteResult suiteResult = (EmailableReporter2.SuiteResult)localIterator1.next();
        
        String ignoreMethodeNameParameter = ((ISuite)suites.get(suiteResults.indexOf(suiteResult))).getParameter("ignoreMethodeName");
        
        if ((ignoreMethodeNameParameter != null) && (ignoreMethodeNameParameter.contains("true"))) {
          ignoreMethodeName = Boolean.valueOf(true);
        }
        


        localIterator2 = suiteResult.getTestResults().iterator(); continue;EmailableReporter2.TestResult testResult = (EmailableReporter2.TestResult)localIterator2.next();
        Iterator localIterator4;
        for (Iterator localIterator3 = testResult.getFailedTestResults().iterator(); localIterator3.hasNext(); 
            
            localIterator4.hasNext())
        {
          EmailableReporter2.ClassResult classResult = (EmailableReporter2.ClassResult)localIterator3.next();
          
          localIterator4 = classResult.getMethodResults().iterator(); continue;EmailableReporter2.MethodResult methodResult = (EmailableReporter2.MethodResult)localIterator4.next();
          allTests.addAll(methodResult.getResults());
          for (ITestResult tResult : methodResult.getResults())
          {
            temp = tResult.getStartMillis();
            
            String exceptionReplacement = tResult.getThrowable().getMessage();
            
            if ((!(tResult.getThrowable() instanceof AssertionError)) && (exceptionReplacement != null) && (!exceptionReplacement.isEmpty()))
            {
              if (exceptionReplacement.indexOf("(Session") > 0) {
                exceptionReplacement = exceptionReplacement.substring(0, exceptionReplacement.indexOf("(Session") - 1).trim();
              }
              
              String exceptionToReplace = exceptionReplacement;
              exceptionReplacement = exceptionReplacement.replace("&", "&amp;");
              exceptionReplacement = exceptionReplacement.replace("<", "&lt;");
              exceptionReplacement = exceptionReplacement.replace(">", "&gt;");
              exceptionReplacement = exceptionReplacement.replace("\"", "&quot;");
              exceptionReplacement = exceptionReplacement.replace("'", "&apos;");
              eContent = eContent.replace(exceptionToReplace, exceptionReplacement);
            }
            
            if ((minStartTime == 0L) || (temp < minStartTime)) {
              minStartTime = temp;
            }
            
            temp = tResult.getEndMillis();
            
            if ((maxEndTime == 0L) || (temp > maxEndTime)) {
              maxEndTime = temp;
            }
            
            if (tResult.getMethod().isTest())
            {


              String methodDescription = getTestTitle(Reporter.getOutput(tResult).toString());
              String methodName = tResult.getMethod().getMethodName();
              
              if (methodDescription.isEmpty()) {
                methodDescription = tResult.getMethod().getDescription();
              }
              
              String toReplace = "<a href=\"#m([0-9]{1,4})\">" + methodName + "</a>";
              
              String toReplaceBy = "";
              
              if (ignoreMethodeName.booleanValue()) {
                toReplaceBy = "<a href=\"#m$1\">" + methodDescription + "</a>";
              } else {
                toReplaceBy = "<a href=\"#m$1\">" + methodName + ": " + methodDescription + "</a>";
              }
              
              eContent = eContent.replaceFirst(toReplace, toReplaceBy);
            }
          }
        }
        



        for (localIterator3 = testResult.getSkippedTestResults().iterator(); localIterator3.hasNext(); 
            
            localIterator4.hasNext())
        {
          EmailableReporter2.ClassResult classResult = (EmailableReporter2.ClassResult)localIterator3.next();
          
          localIterator4 = classResult.getMethodResults().iterator(); continue;EmailableReporter2.MethodResult methodResult = (EmailableReporter2.MethodResult)localIterator4.next();
          allTests.addAll(methodResult.getResults());
          for (ITestResult tResult : methodResult.getResults())
          {
            temp = tResult.getStartMillis();
            
            if ((minStartTime == 0L) || (temp < minStartTime)) {
              minStartTime = temp;
            }
            
            temp = tResult.getEndMillis();
            
            if ((maxEndTime == 0L) || (temp > maxEndTime)) {
              maxEndTime = temp;
            }
            
            if (tResult.getMethod().isTest())
            {


              String methodName = tResult.getMethod().getMethodName();
              String methodDescription = getTestTitle(Reporter.getOutput(tResult).toString());
              
              if (methodDescription.isEmpty()) {
                methodDescription = tResult.getMethod().getDescription();
              }
              
              String toReplace = "<a href=\"#m([0-9]{1,4})\">" + methodName + "</a>";
              
              String toReplaceBy = "";
              
              if (ignoreMethodeName.booleanValue()) {
                toReplaceBy = "<a href=\"#m$1\">" + methodDescription + "</a>";
              } else {
                toReplaceBy = "<a href=\"#m$1\">" + methodName + ": " + methodDescription + "</a>";
              }
              
              eContent = eContent.replaceFirst(toReplace, toReplaceBy);
            }
          }
        }
        


        for (localIterator3 = testResult.getPassedTestResults().iterator(); localIterator3.hasNext(); 
            
            localIterator4.hasNext())
        {
          EmailableReporter2.ClassResult classResult = (EmailableReporter2.ClassResult)localIterator3.next();
          
          localIterator4 = classResult.getMethodResults().iterator(); continue;EmailableReporter2.MethodResult methodResult = (EmailableReporter2.MethodResult)localIterator4.next();
          allTests.addAll(methodResult.getResults());
          for (ITestResult tResult : methodResult.getResults())
          {
            temp = tResult.getStartMillis();
            
            if ((minStartTime == 0L) || (temp < minStartTime)) {
              minStartTime = temp;
            }
            
            temp = tResult.getEndMillis();
            
            if ((maxEndTime == 0L) || (temp > maxEndTime)) {
              maxEndTime = temp;
            }
            
            if (tResult.getMethod().isTest())
            {


              String methodName = tResult.getMethod().getMethodName();
              String methodDescription = getTestTitle(Reporter.getOutput(tResult).toString());
              
              if (methodDescription.isEmpty()) {
                methodDescription = tResult.getMethod().getDescription();
              }
              
              String toReplace = "<a href=\"#m([0-9]{1,4})\">" + methodName + "</a>";
              
              String toReplaceBy = "";
              
              if (ignoreMethodeName.booleanValue()) {
                toReplaceBy = "<a href=\"#m$1\">" + methodDescription + "</a>";
              } else {
                toReplaceBy = "<a href=\"#m$1\">" + methodName + ": " + methodDescription + "</a>";
              }
              
              eContent = eContent.replaceFirst(toReplace, toReplaceBy);
            }
          }
        }
      }
      

      eContent = eContent.replace("</head>", "\r</head>\r");
      eContent = eContent.replace("<table", "\r\t<table");
      eContent = eContent.replace("</table>", "\r\t</table>\r");
      eContent = eContent.replaceFirst("<table>", "<table id='suitesummary' title=\"Filters results based on cell clicked/Shows all result on double-click\">");
      eContent = eContent.replaceFirst("<table>", "<table id='summary'>");
      
      eContent = eContent.replace("<thead>", "\r\t<thead>\r");
      eContent = eContent.replace("</thead>", "\r\t</thead>\r");
      eContent = eContent.replace("<tbody>", "\r\t<tbody>\r");
      eContent = eContent.replace("</tbody>", "\r\t</tbody>\r");
      
      eContent = eContent.replace("<h2", "\r\t\t<h2");
      eContent = eContent.replace("<tr", "\r\t\t<tr");
      eContent = eContent.replace("</tr>", "\r\t\t</tr>\r");
      eContent = eContent.replace("<td>", "\r\t\t\t<td>");
      eContent = eContent.replace("</td>", "\r\t\t\t</td>\r");
      eContent = eContent.replace("<th", "\r\t\t\t<th");
      eContent = eContent.replace("</th>", "\r\t\t\t</th>");
      eContent = eContent.replace("<br/>", "");
      eContent = eContent.replaceAll("<style(.*)</style>", "\r" + FileUtils.readFileToString(eCSS) + "\r");
      eContent = eContent.replace("<head>", "<head>\r" + FileUtils.readFileToString(eScripts) + "\r");
      eContent = eContent.replace("<head>", "<head>\r<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" />\r");
      
      eContent = eContent.replaceFirst("<table id='suitesummary' title=\"Filters results based on cell clicked/Shows all result on double-click\">", "<table id='suitesummary' title=\"Filters results based on cell clicked/Shows all result on double-click\" duration=\"" + (maxEndTime - minStartTime) + "\">");
      
      FileUtils.writeStringToFile(eReport, eContent);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    
    try
    {
      FileReader fr = null;
      fr = new FileReader(outdir + File.separator + "TestAutomationResults.html");
      BufferedReader br = new BufferedReader(fr);
      StringBuilder content = new StringBuilder(10000);
      
      int tableCount = 0;
      
      String hub = "localhost";
      try
      {
        hub = ((ISuite)suites.get(0)).getHost() == null ? Inet4Address.getLocalHost().getHostName() : ((ISuite)suites.get(0)).getHost();
      }
      catch (UnknownHostException e) {
        Log.event("Getting some host issue..");
      }
      String s;
      while ((s = br.readLine()) != null) {
        String s;
        content.append(s + "\n");
        if (s.trim().contains("</table>")) {
          tableCount++;
        }
        
        if (s.startsWith("<body"))
        {
          content.append("<p> Hi, </p>\n<p> Test automation scripts execution completed. Find the results summary below. </p>\n<p> <font color=\"blue\"><b>Note:</b> Please find the link to detailed results at the bottom of the email; Recommend to use <b>Chrome or Firefox </b> to view the results. </font></p>\n<p> <u><h3> Test Run Details: </h3> </u>\r<table  bordercolor=\"#FFFFF\"> </u></h3> </p>\r\r<pre style=\"font-size: 1.2em;\">\r   <b>Test Name</b> : " + 
          
            System.getProperty("testname") + "\r" + "   <b>Suite Name</b>: " + System.getProperty("suiteFile") + "\r" + "   <b>Run Date</b>  : " + new Date().toString() + "\r" + "   <b>Test Name</b> : " + System.getProperty("name") + "\r" + "   <b>Run By</b>    : " + hub + "\r" + "</pre>" + "<br><br>\n");
        }
        
        if (tableCount == 1) {
          content.append("<p><br></p><p> Thanks </p>\n</body>\n</html>");
          break;
        }
      }
      





      br.close();
      fr.close();
      try {
        ExtentReporter.closeReport(allTests, outdir);
      } catch (Exception e) {
        e.printStackTrace();
      }
      

      List<String> filename = new ArrayList();
      filename.add(outdir + File.separator + "TestAutomationResults.html");
      filename.add(outdir + File.separator + "ScreenShot");
      filename.add(outdir + File.separator + "ExtentReport.html");
      
      String ouputFile = outdir + File.separator + "AutomationTestSummaryReport.zip";
      FolderZiper folderZiper = new FolderZiper();
      folderZiper.zipFolder(filename, ouputFile);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  






  public static String getTestTitle(String content)
  {
    Pattern p = Pattern.compile(startTestTitle + "(.*)" + endTestTitle, 32);
    Matcher matcher = p.matcher(content);
    try
    {
      if (matcher.find()) {
        return matcher.group(1);
      }
      return "";
    }
    catch (IllegalStateException e) {}
    return "";
  }
  







  protected PrintWriter createWriter(String outdir)
    throws IOException
  {
    new File(outdir).mkdirs();
    pWriter = new PrintWriter(new java.io.BufferedWriter(new FileWriter(new File(outdir, "TestAutomationResults.html"))));
    return pWriter;
  }
}
