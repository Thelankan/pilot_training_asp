package com.dwre.support;


public class DWRE_Utils
{
  public DWRE_Utils() {}
  

  private static EnvironmentPropertiesReader redirectProperty = EnvironmentPropertiesReader.getInstance("redirect");
  
  public static String generateURL(MerchantTools page) {
    String url = redirectProperty.getProperty("home").replace("regex", System.getProperty("dwre_id"));
    
    switch (page)
    {
    case ContentSlots: 
      url = url + redirectProperty.getProperty("customers");
      break;
    case CustomerGroup: 
      url = url + redirectProperty.getProperty("customer_import_export");
      break;
    
    case CustomerImportExport: 
      url = url + redirectProperty.getProperty("products");
      break;
    case Customers: 
      url = url + redirectProperty.getProperty("productsets");
      break;
    case Home: 
      url = url + redirectProperty.getProperty("catalogs");
      break;
    case ImportExportOrder: 
      url = url + redirectProperty.getProperty("pricebooks");
      break;
    case ManageSites: 
      url = url + redirectProperty.getProperty("prd_import_export");
      break;
    
    case None: 
      url = url + redirectProperty.getProperty("promotions");
      break;
    case Orders: 
      url = url + redirectProperty.getProperty("campaigns");
      break;
    case PriceBooks: 
      url = url + redirectProperty.getProperty("coupons");
      break;
    case ProductImportExport: 
      url = url + redirectProperty.getProperty("contentslots");
      break;
    case ProductSets: 
      url = url + redirectProperty.getProperty("stores");
      break;
    case Products: 
      url = url + redirectProperty.getProperty("promo_import_export");
      break;
    case PromoImportExport: 
      url = url + redirectProperty.getProperty("orders");
      break;
    case Promotions: 
      url = url + redirectProperty.getProperty("export_order");
      break;
    case Stores: 
      url = url + redirectProperty.getProperty("managesites");
      break;
    }
    
    


    return url;
  }
  
  public static String generateProductURL() {
    String url = redirectProperty.getProperty("home").replace("regex", System.getProperty("dwre_id")) + redirectProperty.getProperty("home_product");
    url = url.replace("regex", redirectProperty.getProperty(System.getProperty("product")));
    
    return url;
  }
  
  public static String getWebSiteURL() {
    String url = redirectProperty.getProperty("home").replace("regex", System.getProperty("dwre_id"));
    return url;
  }
}
