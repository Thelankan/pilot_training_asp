package com.dwre.pages.customers;

import com.dwre.pages.ElementLayer;
import com.dwre.pages.Navigation;
import com.dwre.support.BrowserActions;
import com.dwre.support.DWRE_TestDataExtractor;
import com.dwre.support.DWRE_Utils;
import com.dwre.support.EnvironmentPropertiesReader;
import com.dwre.support.Log;
import com.dwre.support.MerchantTools;
import com.dwre.support.Utils;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

public class Customers
  extends LoadableComponent<Customers>
{
  private WebDriver driver;
  private boolean isPageLoaded;
  public ElementLayer elementLayer;
  String runPltfrm = Utils.getRunPlatForm();
  String workBook = EnvironmentPropertiesReader.getInstance("dataConfig").getProperty("workBookName");
  String workSheet = EnvironmentPropertiesReader.getInstance("dataConfig").getProperty("workSheet_Customer");
  

  @FindBy(css="form[name='customerListTop']")
  WebElement readyElement;
  

  @FindBy(id="login")
  WebElement LoginPage;
  
  @FindBy(name="WFCustomerSimpleSearch_SearchTerm")
  WebElement txtNameInSimpleSearch;
  
  @FindBy(id="searchFocus")
  WebElement btnFindInSimSearch;
  
  @FindBy(css="button[name='create']")
  WebElement btnNewInSimpleSearch;
  
  @FindBy(css="input[name='CredentialsLogin']")
  WebElement txtLoginID;
  
  @FindBy(css="button[name='generateNumber']")
  WebElement btnGenerateCustNum;
  
  @FindBy(css="input[name='CredentialsPassword']")
  WebElement txtPassword;
  
  @FindBy(css="input[name='CredentialsPasswordConfirm']")
  WebElement txtConfirmPassword;
  
  @FindBy(xpath="(//input[@type='text'])[10]")
  WebElement txtFirstName;
  
  @FindBy(xpath="(//input[@type='text'])[11]")
  WebElement txtLastName;
  
  @FindBy(css="button[name='update']")
  WebElement btnApply;
  
  @FindBy(css="input[name='CredentialsEnabled']")
  WebElement chkEnable;
  
  @FindBy(css="td.table_detail > select")
  WebElement selectGender;
  
  @FindBy(xpath="(//input[@value=''])[16]")
  WebElement txtEmail;
  
  @FindBy(css="input[name='CustomerNumber']")
  WebElement txtCustomerID;
  
  @FindBy(css="td[class='table_title w e s']")
  WebElement lblPageTitle;
  
  @FindBy(css="input[name='WFCustomerAdvancedSearch_Login']")
  WebElement adv_Login;
  
  @FindBy(css="#D button[name='parametricSearch']")
  WebElement btnAdvSearchFind;
  
  @FindBy(css=".table_title_description.aldi")
  WebElement lblEmptyResultInSearch;
  
  @FindBy(xpath="//a[@class='table_tabs_dis'][contains(text(),'Customer Groups')]")
  WebElement tabCustomerGroup;
  
  @FindBy(css="td[class='table_detail e s'][nowrap='nowrap']")
  List<WebElement> lstCustomerGroupAssigned;
  
  @FindBy(css="button[name='assign']")
  WebElement btnAssignInCustGroup;
  
  @FindBy(css="button[name='PageSize'][value='All']")
  WebElement btnAll;
  
  @FindBy(css="button[name='assignCustomers']")
  WebElement btnAssignCustomerGroups;
  
  @FindBy(css="button[name='confirmDelete']")
  WebElement btnDeleteCustomer;
  
  @FindBy(css="button[name='delete']")
  WebElement btnConfirmDeleteCustomer;
  

  public Customers(WebDriver driver)
  {
    this.driver = driver;
    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 
      Utils.maxElementWait);
    PageFactory.initElements(finder, this);
  }
  

  protected void isLoaded()
  {
    if (!isPageLoaded) {
      Assert.fail();
    }
    try
    {
      if (LoginPage.isDisplayed())
        new Navigation(driver).login(driver);
      driver.get(DWRE_Utils.generateURL(MerchantTools.Customers));
      Utils.waitForPageLoad(driver);
    }
    catch (Exception localException) {}
    
    if ((isPageLoaded) && (!Utils.waitForElement(driver, readyElement))) {
      Log.fail("Customers Page didn't display", driver);
    }
    
    elementLayer = new ElementLayer(driver);
  }
  

  protected void load()
  {
    isPageLoaded = true;
    driver.get(DWRE_Utils.generateURL(MerchantTools.Customers));
    Utils.waitForPageLoad(driver);
  }
  



  public void doSimpleSearch(String name)
    throws Exception
  {
    BrowserActions.typeOnTextField(txtNameInSimpleSearch, name, driver, "Name Field");
    BrowserActions.clickOnElement(btnFindInSimSearch, driver, "Find Button");
    Utils.waitForPageLoad(driver);
  }
  
  public boolean doAdvancedSearch(HashMap<String, String> inputs) throws Exception {
    List<String> keySet = new ArrayList(inputs.keySet());
    LinkedHashMap<String, WebElement> instructions = new LinkedHashMap();
    
    for (String key : keySet) {
      instructions.put("type_" + key + "_" + (String)inputs.get(key), (WebElement)getClass().getDeclaredField("adv_" + key).get(new Customers(driver)));
    }
    
    instructions.put("click_Find Button_", btnAdvSearchFind);
    
    WebElement tabAdvance = driver.findElement(By.linkText("Advanced"));
    BrowserActions.clickOnElement(tabAdvance, driver, "Advanced");
    BrowserActions.fillFormDetails(instructions, driver);
    
    Utils.waitForPageLoad(driver);
    
    if (Utils.waitForElement(driver, lblEmptyResultInSearch)) {
      return false;
    }
    return true;
  }
  
  public void DeleteCustomer(String Email) throws Exception
  {
    WebElement tabAdvance = driver.findElement(By.linkText("Advanced"));
    BrowserActions.clickOnElement(tabAdvance, driver, "Advanced");
    BrowserActions.typeOnTextField(adv_Login, Email, driver, "Login text box");
    BrowserActions.clickOnElement(btnAdvSearchFind, driver, "Advance Search");
    
    Utils.waitForPageLoad(driver);
    
    WebElement link = driver.findElement(By.linkText(Email));
    BrowserActions.clickOnElement(link, driver, "Email Link");
    Utils.waitForPageLoad(driver);
    BrowserActions.clickOnElement(btnDeleteCustomer, driver, "Delete");
    Utils.waitForPageLoad(driver);
    BrowserActions.clickOnElement(btnConfirmDeleteCustomer, driver, "Delete");
    Utils.waitForPageLoad(driver);
    Log.message("User Account Deleted.", driver, new Boolean[0]);
  }
  
  public void CreateCustomer(String Email, String... Password) throws Exception {
    HashMap<String, String> testData = DWRE_TestDataExtractor.initTestData(workBook, workSheet, new String[] { Email });
    LinkedHashMap<String, WebElement> instructions = new LinkedHashMap();
    
    BrowserActions.clickOnElement(btnNewInSimpleSearch, driver, "New button in Simple Search Form");
    Utils.waitForPageLoad(driver);
    
    Log.event("--->>> TestData :: " + testData.toString());
    instructions.put("check_Enable User_" + (String)testData.get("Enable"), chkEnable);
    instructions.put("type_Login Email_" + (String)testData.get("ID"), txtLoginID);
    instructions.put("click_Generate Customer ID_", btnGenerateCustNum);
    
    if (Password.length > 0) {
      instructions.put("type_Password_" + (String)testData.get("Password"), txtPassword);
      instructions.put("type_Confirm Password_" + (String)testData.get("ConfirmPassword"), txtConfirmPassword);
    } else {
      instructions.put("type_Password_test123@", txtPassword);
      instructions.put("type_Confirm Password_test123@", txtConfirmPassword);
    }
    
    instructions.put("type_First Name_" + (String)testData.get("FirstName"), txtFirstName);
    instructions.put("type_Last Name_" + (String)testData.get("LastName"), txtLastName);
    instructions.put("select_Gender_" + (String)testData.get("Gender"), selectGender);
    instructions.put("type_Email_" + (String)testData.get("ID"), txtEmail);
    
    BrowserActions.fillFormDetails(instructions, driver);
    
    String custID = txtCustomerID.getText();
    
    BrowserActions.clickOnElement(btnApply, driver, "Apply Button");
    Utils.waitForPageLoad(driver);
    
    if (!lblPageTitle.getAttribute("innerHTML").contains(custID)) {
      Log.fail("Cannot Create customer. Please contact Administrator.");
    }
    Log.message("New Customer registered successfully.");
  }
  
  public void assignCustomerGroup(String loginID) throws Exception
  {
    HashMap<String, String> testData = DWRE_TestDataExtractor.initTestData(workBook, workSheet, new String[] { loginID });
    
    HashMap<String, String> inputs = new HashMap();
    inputs.put("Login", loginID);
    if (doAdvancedSearch(inputs)) {
      WebElement loginLink = driver.findElement(By.linkText(loginID));
      BrowserActions.clickOnElement(loginLink, driver, "Email ID Link in Search result");
      Utils.waitForPageLoad(driver);
      
      BrowserActions.clickOnElement(tabCustomerGroup, driver, "Customer Group tab");
      
      boolean flag = false;
      for (WebElement cust : lstCustomerGroupAssigned) {
        if (cust.getAttribute("innerHTML").toLowerCase().contains((CharSequence)testData.get("CustomerGroup"))) {
          flag = true;
          break;
        }
      }
      
      if (!flag) {
        BrowserActions.clickOnElement(btnAssignInCustGroup, driver, "Assign Button In Customer Group Tab");
        Utils.waitForPageLoad(driver);
        
        if (Utils.waitForElement(driver, btnAll)) {
          BrowserActions.clickOnElement(btnAll, driver, "All Button in Pagination");
          Utils.waitForPageLoad(driver);
        }
        
        WebElement chkBox = driver.findElement(By.xpath("//td[@class='table_detail e s'][contains(text(),'" + (String)testData.get("CustomerGroup") + "')]//preceding-sibling::td/input"));
        BrowserActions.clickOnElement(chkBox, driver, "Checkbox for given Customer Group");
        
        BrowserActions.clickOnElement(btnAssignCustomerGroups, driver, "Checkbox for given Customer Group");
        Utils.waitForPageLoad(driver);
        
        flag = false;
        for (WebElement cust : lstCustomerGroupAssigned) {
          if (cust.getAttribute("innerHTML").toLowerCase().contains((CharSequence)testData.get("CustomerGroup"))) {
            flag = true;
            break;
          }
        }
        
        if (!flag)
          Log.fail("Not able to assign Customer Group. Please contact Administrator.");
      }
    } else {
      Log.fail("Customer not registered yet.");
    }
    
    Log.message("Customer group applied successfully.");
  }
}
