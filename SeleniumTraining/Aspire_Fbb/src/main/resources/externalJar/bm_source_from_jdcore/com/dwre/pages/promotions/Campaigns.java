package com.dwre.pages.promotions;

import com.dwre.pages.ElementLayer;
import com.dwre.pages.Navigation;
import com.dwre.support.BrowserActions;
import com.dwre.support.DWRE_TestDataExtractor;
import com.dwre.support.DWRE_Utils;
import com.dwre.support.EnvironmentPropertiesReader;
import com.dwre.support.Log;
import com.dwre.support.MerchantTools;
import com.dwre.support.Utils;
import java.util.HashMap;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;




public class Campaigns
  extends LoadableComponent<Campaigns>
{
  private WebDriver driver;
  private boolean isPageLoaded;
  public ElementLayer elementLayer;
  String runPltfrm = Utils.getRunPlatForm();
  String workBook = EnvironmentPropertiesReader.getInstance("dataConfig").getProperty("workBookName");
  String workSheet = EnvironmentPropertiesReader.getInstance("dataConfig").getProperty("workSheet_Campaigns");
  

  @FindBy(css="div[class='main-content dw-campaigns']")
  WebElement readyElement;
  

  @FindBy(id="login")
  WebElement LoginPage;
  
  @FindBy(css="button.dw-primary.dw-button")
  WebElement btnNew;
  
  @FindBy(css="input[id='x-auto-2-input']")
  WebElement txtCampaingsId;
  
  @FindBy(css="textarea[Name='description']")
  WebElement txtCampaignsDescription;
  
  @FindBy(css="img[id='x-auto-78']")
  WebElement calendarStartDate;
  
  @FindBy(css="img[id='x-auto-90']")
  WebElement calendarEndDate;
  
  @FindBy(css="div.x-menu-list-item td.x-date-bottom .x-btn-text")
  WebElement btnToday;
  
  @FindBy(css="#x-auto-79-input")
  WebElement drpdownStartDateTime;
  
  @FindBy(css="#x-auto-91-input")
  WebElement drpdownEndDateTime;
  
  @FindBy(css="#x-auto-81 .x-combo-list-item")
  List<WebElement> drpdownStartDateTimeValue;
  
  @FindBy(css="#x-auto-93 .x-combo-list-item")
  List<WebElement> drpdownEndDateTimeValue;
  
  @FindBy(css="div[class*='customerGroups'] td.x-btn-mc button")
  WebElement btnCustomergroupEdit;
  
  @FindBy(css="div[class*='coupons'] td.x-btn-mc button")
  WebElement btnCouponsEdit;
  
  @FindBy(css="div[class*='customergroupsselectionwindow']")
  WebElement customerGroupSelectorWindow;
  
  @FindBy(css="div[class*='couponsselectionwindow']")
  WebElement couponsSelectorWindow;
  
  @FindBy(css="div[class*='promotion-addbutton']")
  WebElement btnAddPromotion;
  
  @FindBy(css="div[class*='promotionsselectionwindow']")
  WebElement promotionSelectorWindow;
  
  @FindBy(css="table[class*='objectselectionpanel-applybutton'] > tbody > tr:nth-child(2) > td.x-btn-mc > em > button")
  WebElement btnApplySelectorWindow;
  
  @FindBy(css="table[class*='detailspanel-applybutton'] > tbody > tr:nth-child(2) > td.x-btn-mc > em > button")
  WebElement btnApplyCampaigns;
  
  @FindBy(css=".dw-bm-widget-searchpanel-textfield input")
  WebElement txtSearchIdPanel;
  
  @FindBy(css=".dw-bm-widget-searchpanel-searchbutton button img")
  WebElement btnSearch;
  
  @FindBy(css=".main-toolbar [dw-search-filters='searchFilters'] input[placeholder*='Search by ID']")
  WebElement txtSearchId;
  
  @FindBy(css=".main-toolbar [dw-search-filters='searchFilters'] button")
  WebElement btnSearchId;
  

  public Campaigns(WebDriver driver)
  {
    this.driver = driver;
    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 
      Utils.maxElementWait);
    PageFactory.initElements(finder, this);
  }
  

  protected void isLoaded()
  {
    if (!isPageLoaded) {
      Assert.fail();
    }
    try
    {
      if (LoginPage.isDisplayed())
        new Navigation(driver).login(driver);
      driver.get(DWRE_Utils.generateURL(MerchantTools.Campaigns));
    }
    catch (Exception localException) {}
    

    if ((isPageLoaded) && (!Utils.waitForElement(driver, readyElement, 30))) {
      Log.fail("Campaigns Page didn't display", driver);
    }
    
    elementLayer = new ElementLayer(driver);
  }
  

  protected void load()
  {
    isPageLoaded = true;
    driver.get(DWRE_Utils.generateURL(MerchantTools.Campaigns));
    Utils.waitForPageLoad(driver);
  }
  





  public void selectDate(WebElement e, String date)
    throws Exception
  {
    BrowserActions.clickOnElement(e, driver, "Date Calendar");
    Utils.waitForPageLoad(driver);
    List<WebElement> Dates = driver.findElements(By.cssSelector("td.x-date-active >a"));
    for (WebElement Date : Dates)
    {
      if (Date.getText().trim().equals(date))
      {
        Date.click();
        break;
      }
    }
    Utils.waitForPageLoad(driver);
  }
  






  public void selectTime(WebElement drpdown, List<WebElement> e, String time)
    throws Exception
  {
    BrowserActions.clickOnElement(drpdown, driver, "Drop down");
    Utils.waitForPageLoad(driver);
    for (WebElement list : e)
    {
      if (list.getText().trim().equals(time))
      {
        list.click();
        Utils.waitForPageLoad(driver);
        break;
      }
    }
    Utils.waitForPageLoad(driver);
  }
  




  public void selectID(String idName)
    throws Exception
  {
    Utils.waitForPageLoad(driver);
    BrowserActions.typeOnTextField(txtSearchIdPanel, idName, driver, "Search Id text field In Panel");
    BrowserActions.clickOnElement(btnSearch, driver, "Search Button in Panel");
    Utils.waitForPageLoad(driver);
    
    List<WebElement> listId = driver.findElements(By.cssSelector("table.x-grid3-row-table div.x-grid3-col-ID"));
    for (WebElement Id : listId)
    {
      if (Id.getText().toString().trim().equals(idName))
      {
        Id.click();
        Utils.waitForPageLoad(driver);
        break;
      }
    }
    Utils.waitForPageLoad(driver);
    BrowserActions.clickOnElement(btnApplySelectorWindow, driver, "Selector Window Apply Button");
    Utils.waitForPageLoad(driver);
  }
  
  public void uncheckSelectedID(String idName) throws Exception
  {
    Utils.waitForPageLoad(driver);
    BrowserActions.typeOnTextField(txtSearchIdPanel, idName, driver, "Search Id text field In Panel");
    BrowserActions.clickOnElement(btnSearch, driver, "Search Button in Panel");
    Utils.waitForPageLoad(driver);
    List<WebElement> listId = driver.findElements(By.cssSelector(".x-grid3-row-selected table.x-grid3-row-table div.x-grid3-col-ID"));
    for (WebElement Id : listId)
    {
      if (Id.getText().toString().trim().equals(idName))
      {
        WebElement checkbox = driver.findElement(By.xpath("//td[contains(@class,'x-grid3-td-ID')]/div[@class='x-grid3-cell-inner x-grid3-col-ID'][contains(text(),'" + idName + "')]/../..//div[@class='x-grid3-row-checker']"));
        BrowserActions.clickOnElement(checkbox, driver, "Id check box");
        Utils.waitForPageLoad(driver);
        break;
      }
    }
    Utils.waitForPageLoad(driver);
    BrowserActions.clickOnElement(btnApplySelectorWindow, driver, "Selector Window Apply Button");
    Utils.waitForPageLoad(driver);
  }
  




  public String createCampaigns(String CampaignsId)
    throws Exception
  {
    Coupons coupons = (Coupons)new Coupons(driver).get();
    String CouponIDandName = coupons.createCoupons("Create Coupon");
    String[] CouponId = CouponIDandName.split("/");
    Log.message("Created Coupon Id: " + CouponId[0]);
    Log.message("Created Coupon Code: " + CouponId[1]);
    
    Promotions promotions = (Promotions)new Promotions(driver).get();
    String PromotionID = promotions.createPromotion("Create Promotion");
    Log.message("Created Promotion Id: " + PromotionID);
    
    driver.get(DWRE_Utils.generateURL(MerchantTools.Campaigns));
    
    HashMap<String, String> testData = DWRE_TestDataExtractor.initTestData(workBook, workSheet, new String[] { CampaignsId });
    
    BrowserActions.clickOnElement(btnNew, driver, "New button");
    Utils.waitForPageLoad(driver);
    
    BrowserActions.typeOnTextField(txtCampaingsId, (String)testData.get("ID_Name"), driver, "Campaings Id Name");
    BrowserActions.typeOnTextField(txtCampaignsDescription, (String)testData.get("Description"), driver, "Campaigns Description");
    

    selectDate(calendarStartDate, (String)testData.get("StartDate"));
    selectTime(drpdownStartDateTime, drpdownStartDateTimeValue, (String)testData.get("StartTime"));
    

    selectDate(calendarEndDate, (String)testData.get("EndDate"));
    selectTime(drpdownEndDateTime, drpdownEndDateTimeValue, (String)testData.get("EndTime"));
    

    BrowserActions.clickOnElement(btnCustomergroupEdit, driver, "Customer Group Edit button");
    BrowserActions.nap(2L);
    if (Utils.waitForElement(driver, customerGroupSelectorWindow))
    {
      selectID((String)testData.get("Customer Group ID"));
    }
    

    BrowserActions.clickOnElement(btnCouponsEdit, driver, "Coupons Edit button");
    BrowserActions.nap(2L);
    if (Utils.waitForElement(driver, couponsSelectorWindow))
    {
      selectID(CouponId[0]);
    }
    

    BrowserActions.clickOnElement(btnAddPromotion, driver, "Add promotion Icon");
    BrowserActions.nap(2L);
    if (Utils.waitForElement(driver, promotionSelectorWindow))
    {
      selectID(PromotionID);
    }
    

    BrowserActions.clickOnElement(btnApplyCampaigns, driver, "Campaigns Apply button");
    Utils.waitForPageLoad(driver);
    BrowserActions.nap(2L);
    
    return CouponId[1];
  }
  




  public void updateCampaignDateandTime(String CampaignId)
    throws Exception
  {
    HashMap<String, String> testData = DWRE_TestDataExtractor.initTestData(workBook, workSheet, new String[] { CampaignId });
    Utils.waitForPageLoad(driver);
    BrowserActions.typeOnTextField(txtSearchId, CampaignId, driver, "Search Id in Search Panel");
    BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
    Utils.waitForPageLoad(driver);
    WebElement campaignLink = driver.findElement(By.xpath("//div/a[@class='link-style dw-nc-display-block'][contains(text(),'" + CampaignId + "')]"));
    BrowserActions.clickOnElement(campaignLink, driver, "Campaign link");
    Utils.waitForPageLoad(driver);
    


    selectDate(calendarStartDate, (String)testData.get("StartDate"));
    selectTime(drpdownStartDateTime, drpdownStartDateTimeValue, (String)testData.get("StartTime"));
    

    selectDate(calendarEndDate, (String)testData.get("EndDate"));
    selectTime(drpdownEndDateTime, drpdownEndDateTimeValue, (String)testData.get("EndTime"));
    

    BrowserActions.clickOnElement(btnApplyCampaigns, driver, "Campaigns Apply button");
    Utils.waitForPageLoad(driver);
    BrowserActions.nap(2L);
  }
  






  public void AddCustomerGroup(String CampaignId, String CustomerGroup)
    throws Exception
  {
    Utils.waitForPageLoad(driver);
    BrowserActions.typeOnTextField(txtSearchId, CampaignId, driver, "Search Id in Search Panel");
    BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
    Utils.waitForPageLoad(driver);
    WebElement campaignLink = driver.findElement(By.xpath("//div/a[@class='link-style dw-nc-display-block'][contains(text(),'" + CampaignId + "')]"));
    BrowserActions.clickOnElement(campaignLink, driver, "Campaign link");
    Utils.waitForPageLoad(driver);
    

    BrowserActions.clickOnElement(btnCustomergroupEdit, driver, "Customer Group Edit button");
    BrowserActions.nap(2L);
    if (Utils.waitForElement(driver, customerGroupSelectorWindow))
    {
      selectID(CustomerGroup);
    }
    

    BrowserActions.clickOnElement(btnApplyCampaigns, driver, "Campaigns Apply button");
    Utils.waitForPageLoad(driver);
    BrowserActions.nap(2L);
  }
  






  public void RemoveCustomerGroup(String CampaignId, String CustomerGroup)
    throws Exception
  {
    Utils.waitForPageLoad(driver);
    BrowserActions.typeOnTextField(txtSearchId, CampaignId, driver, "Search Id in Search Panel");
    BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
    Utils.waitForPageLoad(driver);
    WebElement campaignLink = driver.findElement(By.xpath("//div/a[@class='link-style dw-nc-display-block'][contains(text(),'" + CampaignId + "')]"));
    BrowserActions.clickOnElement(campaignLink, driver, "Campaign link");
    Utils.waitForPageLoad(driver);
    

    BrowserActions.clickOnElement(btnCustomergroupEdit, driver, "Customer Group Edit button");
    BrowserActions.nap(2L);
    if (Utils.waitForElement(driver, customerGroupSelectorWindow))
    {
      uncheckSelectedID(CustomerGroup);
    }
    
    BrowserActions.clickOnElement(btnApplyCampaigns, driver, "Campaigns Apply button");
    Utils.waitForPageLoad(driver);
    BrowserActions.nap(2L);
  }
  






  public void AddCoupon(String CampaignId, String CouponId)
    throws Exception
  {
    Utils.waitForPageLoad(driver);
    BrowserActions.typeOnTextField(txtSearchId, CampaignId, driver, "Search Id in Search Panel");
    BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
    Utils.waitForPageLoad(driver);
    WebElement campaignLink = driver.findElement(By.xpath("//div/a[@class='link-style dw-nc-display-block'][contains(text(),'" + CampaignId + "')]"));
    BrowserActions.clickOnElement(campaignLink, driver, "Campaign link");
    Utils.waitForPageLoad(driver);
    

    BrowserActions.clickOnElement(btnCouponsEdit, driver, "Coupons Edit button");
    BrowserActions.nap(2L);
    if (Utils.waitForElement(driver, couponsSelectorWindow))
    {
      selectID(CouponId);
    }
    
    BrowserActions.clickOnElement(btnApplyCampaigns, driver, "Campaigns Apply button");
    Utils.waitForPageLoad(driver);
    BrowserActions.nap(2L);
  }
  






  public void RemoveCoupon(String CampaignId, String CouponId)
    throws Exception
  {
    Utils.waitForPageLoad(driver);
    BrowserActions.typeOnTextField(txtSearchId, CampaignId, driver, "Search Id in Search Panel");
    BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
    Utils.waitForPageLoad(driver);
    WebElement campaignLink = driver.findElement(By.xpath("//div/a[@class='link-style dw-nc-display-block'][contains(text(),'" + CampaignId + "')]"));
    BrowserActions.clickOnElement(campaignLink, driver, "Campaign link");
    Utils.waitForPageLoad(driver);
    

    BrowserActions.clickOnElement(btnCouponsEdit, driver, "Coupons Edit button");
    BrowserActions.nap(2L);
    if (Utils.waitForElement(driver, couponsSelectorWindow))
    {
      uncheckSelectedID(CouponId);
    }
    
    BrowserActions.clickOnElement(btnApplyCampaigns, driver, "Campaigns Apply button");
    Utils.waitForPageLoad(driver);
    BrowserActions.nap(2L);
  }
  






  public void AddPromotion(String CampaignId, String PromotionId)
    throws Exception
  {
    Utils.waitForPageLoad(driver);
    BrowserActions.typeOnTextField(txtSearchId, CampaignId, driver, "Search Id in Search Panel");
    BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
    Utils.waitForPageLoad(driver);
    WebElement campaignLink = driver.findElement(By.xpath("//div/a[@class='link-style dw-nc-display-block'][contains(text(),'" + CampaignId + "')]"));
    BrowserActions.clickOnElement(campaignLink, driver, "Campaign link");
    Utils.waitForPageLoad(driver);
    

    BrowserActions.clickOnElement(btnAddPromotion, driver, "Add promotion Icon");
    BrowserActions.nap(2L);
    if (Utils.waitForElement(driver, promotionSelectorWindow))
    {
      selectID(PromotionId);
    }
    
    BrowserActions.clickOnElement(btnApplyCampaigns, driver, "Campaigns Apply button");
    Utils.waitForPageLoad(driver);
    BrowserActions.nap(2L);
  }
  






  public void RemovePromotion(String CampaignId, String PromotionId)
    throws Exception
  {
    Utils.waitForPageLoad(driver);
    BrowserActions.typeOnTextField(txtSearchId, CampaignId, driver, "Search Id in Search Panel");
    BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
    Utils.waitForPageLoad(driver);
    WebElement campaignLink = driver.findElement(By.xpath("//div/a[@class='link-style dw-nc-display-block'][contains(text(),'" + CampaignId + "')]"));
    BrowserActions.clickOnElement(campaignLink, driver, "Campaign link");
    Utils.waitForPageLoad(driver);
    

    BrowserActions.clickOnElement(btnAddPromotion, driver, "Add promotion Icon");
    BrowserActions.nap(2L);
    if (Utils.waitForElement(driver, promotionSelectorWindow))
    {
      uncheckSelectedID(PromotionId);
    }
    
    BrowserActions.clickOnElement(btnApplyCampaigns, driver, "Campaigns Apply button");
    Utils.waitForPageLoad(driver);
    BrowserActions.nap(2L);
  }
  




  public void enableCampaign(String CampaignId)
  {
    try
    {
      Utils.waitForPageLoad(driver);
      BrowserActions.typeOnTextField(txtSearchId, CampaignId, driver, "Search Id in Search Panel");
      BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
      Utils.waitForPageLoad(driver);
      WebElement campaignEnableLink = driver.findElement(By.xpath("//div/a[@class='link-style dw-nc-display-block'][contains(text(),'" + CampaignId + "')]/../../..//following-sibling::td//div/span[contains(@class,'enabled')][contains(text(),'No')]"));
      if (Utils.waitForElement(driver, campaignEnableLink))
      {
        WebElement actiondrop = driver.findElement(By.xpath("//div/a[@class='link-style dw-nc-display-block'][contains(text(),'" + CampaignId + "')]/../../..//following-sibling::td[contains(@class,'dw-grid-actions')]//div/../*/button"));
        BrowserActions.clickOnElement(actiondrop, driver, "Action Drop down");
        Utils.waitForPageLoad(driver);
        List<WebElement> drpmenus = driver.findElements(By.cssSelector(".dw-grid-actions [class='dw-menu'] .dw-menu-item"));
        for (WebElement e : drpmenus)
        {
          if (e.getText().toString().equals("Enable"))
          {
            e.click();
            BrowserActions.nap(3L);
            break;
          }
        }
      }
    }
    catch (Exception e)
    {
      Log.message("Selected Campaign is already in Enabled Status");
    }
  }
  



  public void disableCampaign(String CampaignId)
  {
    try
    {
      Utils.waitForPageLoad(driver);
      BrowserActions.typeOnTextField(txtSearchId, CampaignId, driver, "Search Id in Search Panel");
      BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
      Utils.waitForPageLoad(driver);
      WebElement campaignEnableLink = driver.findElement(By.xpath("//div/a[@class='link-style dw-nc-display-block'][contains(text(),'" + CampaignId + "')]/../../..//following-sibling::td//div/span[contains(@class,'enabled')][contains(text(),'Yes')]"));
      if (Utils.waitForElement(driver, campaignEnableLink))
      {
        WebElement actiondrop = driver.findElement(By.xpath("//div/a[@class='link-style dw-nc-display-block'][contains(text(),'" + CampaignId + "')]/../../..//following-sibling::td[contains(@class,'dw-grid-actions')]//div/../*/button"));
        BrowserActions.clickOnElement(actiondrop, driver, "Action Drop down");
        Utils.waitForPageLoad(driver);
        List<WebElement> drpmenus = driver.findElements(By.cssSelector(".dw-grid-actions [class='dw-menu'] .dw-menu-item"));
        for (WebElement e : drpmenus)
        {
          if (e.getText().toString().equals("Disable"))
          {
            e.click();
            BrowserActions.nap(3L);
            break;
          }
          
        }
      }
    }
    catch (Exception e)
    {
      Log.message("Selected Campaign is already in Disabled Status");
    }
  }
  



  public void deleteCampaign(String CampaignId)
  {
    try
    {
      Utils.waitForPageLoad(driver);
      BrowserActions.typeOnTextField(txtSearchId, CampaignId, driver, "Search Id in Search Panel");
      BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
      Utils.waitForPageLoad(driver);
      WebElement actiondrop = driver.findElement(By.xpath("//div/a[@class='link-style dw-nc-display-block'][contains(text(),'" + CampaignId + "')]/../../..//following-sibling::td[contains(@class,'dw-grid-actions')]//div/../*/button"));
      if (Utils.waitForElement(driver, actiondrop))
      {
        BrowserActions.clickOnElement(actiondrop, driver, "Action Drop down");
        Utils.waitForPageLoad(driver);
        List<WebElement> drpmenus = driver.findElements(By.cssSelector(".dw-grid-actions [class='dw-menu'] .dw-menu-item"));
        for (WebElement e : drpmenus)
        {
          if (e.getText().toString().equals("Delete"))
          {
            e.click();
            BrowserActions.nap(3L);
            break;
          }
        }
      }
    }
    catch (Exception e)
    {
      Log.message("Selected Campaign is already in Enabled Status");
    }
  }
}
