package com.dwre.support;

import com.saucelabs.selenium.client.factory.SeleniumFactory;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.core.har.HarLog;
import net.lightbody.bmp.core.har.HarResponse;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.xml.XmlTest;





public class WebDriverFactory
{
  private static Logger logger = LoggerFactory.getLogger(WebDriverFactory.class);
  private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
  private static MobileEmulationUserAgentConfiguration mobEmuUA = new MobileEmulationUserAgentConfiguration();
  
  static String driverHost;
  static String driverPort;
  static String browserName;
  static String deviceName;
  static URL hubURL;
  static Proxy zapProxy = new Proxy();
  
  static DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
  static DesiredCapabilities firefoxCapabilities = DesiredCapabilities.firefox();
  static DesiredCapabilities chromeCapabilities = DesiredCapabilities.chrome();
  static DesiredCapabilities safariCapabilities = DesiredCapabilities.safari();
  static DesiredCapabilities edgeCapabilities = DesiredCapabilities.edge();
  static DesiredCapabilities iOS_SimulatorCapabilities = new DesiredCapabilities();
  static DesiredCapabilities androidSimulatorCapabilities = new DesiredCapabilities();
  static DesiredCapabilities iOSDeviceCapabilities = DesiredCapabilities.iphone();
  static ChromeOptions opt = new ChromeOptions();
  static FirefoxProfile fp = new FirefoxProfile();
  public static ExpectedCondition<Boolean> documentLoad;
  public static ExpectedCondition<Boolean> framesLoad;
  public static ExpectedCondition<Boolean> imagesLoad;
  public static int maxPageLoadWait = 90;
  
  static {
    try {
      documentLoad = new ExpectedCondition()
      {
        public final Boolean apply(WebDriver driver) {
          JavascriptExecutor js = (JavascriptExecutor)driver;
          boolean docReadyState = false;
          try {
            docReadyState = ((Boolean)js.executeScript(
              "return (function() { if (document.readyState != 'complete') {  return false; } if (window.jQuery != null && window.jQuery != undefined && window.jQuery.active) { return false;} if (window.jQuery != null && window.jQuery != undefined && window.jQuery.ajax != null && window.jQuery.ajax != undefined && window.jQuery.ajax.active) {return false;}  if (window.angular != null && angular.element(document).injector() != null && angular.element(document).injector().get('$http').pendingRequests.length) return false; return true;})();", new Object[0])).booleanValue();
          }
          catch (WebDriverException e) {
            docReadyState = true;
          }
          return Boolean.valueOf(docReadyState);
        }
        

      };
      imagesLoad = new ExpectedCondition()
      {
        public final Boolean apply(WebDriver driver) {
          boolean docReadyState = true;
          try
          {
            List<WebElement> images = driver.findElements(By.cssSelector("img[src]"));
            for (int i = 0; i < images.size();) {
              try {
                JavascriptExecutor js = (JavascriptExecutor)driver;
                if (docReadyState) {} docReadyState = 
                
                  ((Boolean)js.executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", new Object[] { images.get(i) })).booleanValue();
                if (!docReadyState) {
                  break;
                }
                i++;



              }
              catch (StaleElementReferenceException e)
              {



                images = driver.findElements(By.cssSelector("img[src]"));
                i--;


              }
              catch (WebDriverException e)
              {


                docReadyState = true;
              }
            }
          } catch (WebDriverException e) {
            docReadyState = true;
          }
          return Boolean.valueOf(docReadyState);
        }
        
      };
      framesLoad = new ExpectedCondition()
      {
        public final Boolean apply(WebDriver driver) {
          boolean docReadyState = true;
          try
          {
            List<WebElement> frames = driver.findElements(By.cssSelector("iframe[style*='hidden']"));
            localIterator = frames.iterator(); break label124; for (;;) { WebElement frame = (WebElement)localIterator.next();
              try {
                driver.switchTo().defaultContent();
                driver.switchTo().frame(frame);
                JavascriptExecutor js = (JavascriptExecutor)driver;
                docReadyState = (docReadyState) && 
                  (((Boolean)js.executeScript("return (document.readyState==\"complete\")", new Object[0])).booleanValue());
                driver.switchTo().defaultContent();
                if (docReadyState) {
                  if (localIterator.hasNext())
                  {
                    continue;
                  }
                  
                }
                

              }
              catch (WebDriverException e)
              {

                docReadyState = true;
              }
            } } catch (WebDriverException e) { Iterator localIterator;
            label124:
            docReadyState = true;
          } finally {
            driver.switchTo().defaultContent();
          }
          return Boolean.valueOf(docReadyState);
        }
        
      };
      XmlTest test = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest();
      driverHost = System.getProperty("hubHost") != null ? System.getProperty("hubHost") : 
        test.getParameter("deviceHost");
      driverPort = System.getProperty("hubPort") != null ? System.getProperty("hubPort") : 
        test.getParameter("devicePort");
      
      maxPageLoadWait = configProperty.getProperty("maxPageLoadWait") != null ? 
        Integer.valueOf(configProperty.getProperty("maxPageLoadWait")).intValue() : maxPageLoadWait;
      
      opt.addArguments(new String[] { "--ignore-certificate-errors" });
      opt.addArguments(new String[] { "--disable-bundled-ppapi-flash" });
      opt.addArguments(new String[] { "--disable-extensions" });
      opt.addArguments(new String[] { "--disable-web-security" });
      opt.addArguments(new String[] { "--always-authorize-plugins" });
      opt.addArguments(new String[] { "--allow-running-insecure-content" });
      opt.addArguments(new String[] { "--test-type" });
      opt.addArguments(new String[] { "--enable-npapi" });
      chromeCapabilities.setCapability("takesScreenshot", true);
      try
      {
        hubURL = new URL("http://" + driverHost + ":" + driverPort + "/wd/hub");
      }
      catch (MalformedURLException localMalformedURLException) {}
      




      return;
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  


  public WebDriverFactory() {}
  

  public static WebDriver get()
    throws MalformedURLException
  {
    browserName = System.getProperty("browserName") != null ? System.getProperty("browserName") : 
      Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("browserName")
      .toLowerCase();
    return get(browserName, null);
  }
  







  public static WebDriver get(String browserSetup)
    throws MalformedURLException
  {
    return get(browserSetup, null);
  }
  







  public static WebDriver get(String browserWithPlatform, Proxy proxy)
    throws MalformedURLException
  {
    String browser = null;
    String platform = null;
    String browserVersion = null;
    String appiumVersion = null;
    String deviceOrientation = null;
    
    String sauceUserName = null;
    String sauceAuthKey = null;
    WebDriver driver = null;
    String userAgent = null;
    long startTime = StopWatch.startTime();
    

    String callerMethodName = new Exception().getStackTrace()[2].getMethodName();
    Log.event("TestCaseID:: " + callerMethodName);
    
    String[] driverInitializeInfo = null;
    

    synchronized (System.class)
    {
      if ((configProperty.hasProperty("runSauceLabFromLocal")) && 
        (configProperty.getProperty("runSauceLabFromLocal").trim().equalsIgnoreCase("true")))
      {
        sauceUserName = configProperty.hasProperty("sauceUserName") ? 
          configProperty.getProperty("sauceUserName") : null;
        sauceAuthKey = configProperty.hasProperty("sauceAuthKey") ? configProperty.getProperty("sauceAuthKey") : 
          null;
        

        if ((configProperty.hasProperty("runDesktop")) && 
          (configProperty.getProperty("runDesktop").equalsIgnoreCase("true")))
        {
          if (browserWithPlatform.contains("&")) {
            driverInitializeInfo = browserWithPlatform.split("&");
            browser = driverInitializeInfo[0];
            



            browser = BrowserType.fromConfiguration(browser).getConfiguration();
            browserVersion = driverInitializeInfo[1];
            platform = driverInitializeInfo[2];
          }
          
          System.setProperty("SELENIUM_DRIVER", 
            "sauce-ondemand:?os=" + platform + "&browser=" + browser + "&browser-version=" + 
            browserVersion + "&username=" + sauceUserName + "&access-key=" + sauceAuthKey);
          System.setProperty("SAUCE_USER_NAME", sauceUserName);
          System.setProperty("SAUCE_API_KEY", sauceAuthKey);
          

          if ((configProperty.hasProperty("runUserAgentDeviceTest")) && 
            (configProperty.getProperty("runUserAgentDeviceTest").trim().equalsIgnoreCase("true"))) {
            deviceName = driverInitializeInfo[3];
            System.setProperty("runUserAgentDeviceTest", "true");
            System.setProperty("deviceName", deviceName);
          }
        }
        
        if ((configProperty.hasProperty("runMobile")) && 
          (configProperty.getProperty("runMobile").equalsIgnoreCase("true")))
        {
          browser = configProperty.getProperty("mobileBrowserName");
          platform = configProperty.getProperty("mobilePlatformName");
          deviceName = configProperty.getProperty("mobileDeviceName");
          browserVersion = configProperty.getProperty("mobilePlatformVersion");
          appiumVersion = configProperty.getProperty("appiumVersion");
          deviceOrientation = configProperty.getProperty("deviceOrientation");
          

          iOSDeviceCapabilities.setCapability("name", callerMethodName);
          
          iOSDeviceCapabilities.setCapability("browserName", browser);
          iOSDeviceCapabilities.setCapability("platformVersion", browserVersion);
          iOSDeviceCapabilities.setCapability("platformName", platform);
          iOSDeviceCapabilities.setCapability("deviceName", deviceName);
          iOSDeviceCapabilities.setCapability("deviceOrientation", deviceOrientation);
          iOSDeviceCapabilities.setCapability("appiumVersion", appiumVersion);
          
          driver = new RemoteWebDriver(new URL(
            "http://" + sauceUserName + ":" + sauceAuthKey + "@ondemand.saucelabs.com:80/wd/hub"), 
            iOSDeviceCapabilities);
          
          String saucelabsSessionId = ((RemoteWebDriver)driver).getSessionId().toString();
          sauceLink = "http://saucelabs.com/jobs/" + saucelabsSessionId + "?auth=" + newHMACMD5Digest(
            new StringBuilder(String.valueOf(sauceUserName)).append(":").append(sauceAuthKey).toString(), saucelabsSessionId);
          logger.debug("Saucelab link for " + callerMethodName + ":: " + sauceLink);
          Log.addSauceJobUrlToReport(driver, sauceLink);
          
          return driver;
        }
      }
      


      if ((System.getProperty("SELENIUM_DRIVER") != null) || (System.getenv("SELENIUM_DRIVER") != null)) {
        return newWebDriverInstanceFromEnvironment(callerMethodName);
      }
    }
    


    if (browserWithPlatform.contains("_")) {
      browser = browserWithPlatform.split("_")[0].toLowerCase().trim();
      platform = browserWithPlatform.split("_")[1].toUpperCase().trim();
    } else {
      platform = "ANY";
    }
    Field f;
    try {
      if ("chrome".equalsIgnoreCase(browser))
      {
        if ((configProperty.hasProperty("runUserAgentDeviceTest")) && 
          (configProperty.getProperty("runUserAgentDeviceTest").equalsIgnoreCase("true"))) {
          deviceName = configProperty.hasProperty("deviceName") ? configProperty.getProperty("deviceName") : 
            null;
          userAgent = mobEmuUA.getUserAgent(deviceName);
          if ((userAgent != null) && (deviceName != null)) {
            driver = new RemoteWebDriver(hubURL, setChromeUserAgent(deviceName, userAgent));
          } else {
            logger.error(
              "Given user agent configuration not yet implemented (or) check the parameters(deviceName) value in config.properties: " + 
              deviceName);
          }
        } else {
          opt.addArguments(new String[] { "--start-maximized" });
          opt.addArguments(new String[] { "--disable-web-security" });
          Map<String, Object> prefs = new HashMap();
          prefs.put("credentials_enable_service", Boolean.valueOf(false));
          prefs.put("profile.password_manager_enabled", Boolean.valueOf(false));
          opt.setExperimentalOption("prefs", prefs);
          
          chromeCapabilities.setCapability("goog:chromeOptions", opt);
          chromeCapabilities.setPlatform(Platform.fromString(platform));
          if (proxy != null) {
            chromeCapabilities.setCapability("proxy", proxy);
          }
          driver = new RemoteWebDriver(hubURL, chromeCapabilities);
        }
      } else if ("iexplorer".equalsIgnoreCase(browser)) {
        ieCapabilities.setCapability("enablePersistentHover", false);
        ieCapabilities.setCapability("ignoreZoomSetting", true);
        ieCapabilities.setCapability("nativeEvents", false);
        ieCapabilities.setCapability("ignoreProtectedModeSettings", 
          true);
        ieCapabilities.setPlatform(Platform.fromString(platform));
        
        if (proxy != null) {
          ieCapabilities.setCapability("proxy", proxy);
        }
        driver = new RemoteWebDriver(hubURL, ieCapabilities);
      } else if ("edge".equalsIgnoreCase(browser)) {
        edgeCapabilities.setPlatform(Platform.fromString(platform));
        driver = new RemoteWebDriver(hubURL, edgeCapabilities);
      } else if ("safari".contains(browser.split("\\&")[0])) {
        if ((configProperty.hasProperty("runUserAgentDeviceTest")) && 
          (configProperty.getProperty("runUserAgentDeviceTest").equalsIgnoreCase("true"))) {
          deviceName = configProperty.hasProperty("deviceName") ? configProperty.getProperty("deviceName") : 
            null;
          userAgent = mobEmuUA.getUserAgent(deviceName);
          if ((userAgent != null) && (deviceName != null)) {
            driver = new RemoteWebDriver(hubURL, setChromeUserAgent(deviceName, userAgent));
          } else {
            logger.error(
              "Given user agent configuration not yet implemented (or) check the parameters(deviceName) value in config.properties: " + 
              deviceName);
          }
        } else {
          safariCapabilities.setCapability("prerun", "https://gist.githubusercontent.com/saucyallison/3a73a4e0736e556c990d/raw/d26b0195d48b404628fc12342cb97f1fc5ff58ec/disable_fraud.sh");
          driver = new RemoteWebDriver(hubURL, safariCapabilities);
        }
      } else if ("zap".equalsIgnoreCase(browser))
      {

        Proxy zapChromeProxy = new Proxy();
        zapChromeProxy.setHttpProxy("localhost:8080");
        zapChromeProxy.setFtpProxy("localhost:8080");
        zapChromeProxy.setSslProxy("localhost:8080");
        chromeCapabilities.setCapability("goog:chromeOptions", opt);
        chromeCapabilities.setCapability("proxy", zapChromeProxy);
        chromeCapabilities.setPlatform(Platform.fromString(platform));
        driver = new RemoteWebDriver(hubURL, chromeCapabilities);
      } else {
        synchronized (WebDriverFactory.class) {
          firefoxCapabilities.setCapability("unexpectedAlertBehaviour", "ignore");
          firefoxCapabilities.setPlatform(Platform.fromString(platform));
          driver = new RemoteWebDriver(hubURL, firefoxCapabilities);
        }
        driver.manage().timeouts().pageLoadTimeout(maxPageLoadWait, TimeUnit.SECONDS);
      }
      Assert.assertNotNull(driver, 
        "Driver did not intialize...\n Please check if hub is running / configuration settings are corect.");
      
      if (!"ANDROID".equalsIgnoreCase(platform)) {
        driver.manage().window().maximize();
      }
    } catch (UnreachableBrowserException e) {
      e.printStackTrace();
      throw new SkipException("Hub is not started or down.");
    }
    catch (WebDriverException e) {
      try {
        if (driver != null) {
          driver.quit();
        }
      } catch (Exception e1) {
        e.printStackTrace();
      }
      
      if (e.getMessage().toLowerCase().contains("error forwarding the new session empty pool of vm for setup")) {
        throw new SkipException("Node is not started or down.");
      }
      if ((e.getMessage().toLowerCase().contains("error forwarding the new session empty pool of vm for setup")) || 
        (e.getMessage().toLowerCase().contains("cannot get automation extension")) || 
        (e.getMessage().toLowerCase().contains("chrome not reachable"))) {
        Log.message("&emsp;<b> --- Re-tried as browser crashed </b>");
        try {
          driver.quit();
        } catch (WebDriverException e1) {
          e.printStackTrace();
        }
        driver = get();
      } else {
        throw e;
      }
      






      try
      {
        Field f = Reporter.getCurrentTestResult().getClass().getDeclaredField("m_startMillis");
        f.setAccessible(true);
        f.setLong(Reporter.getCurrentTestResult(), Calendar.getInstance().getTime().getTime());
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      Assert.fail("Exception encountered in getDriver Method." + e.getMessage().toString());
      



      try
      {
        Field f = Reporter.getCurrentTestResult().getClass().getDeclaredField("m_startMillis");
        f.setAccessible(true);
        f.setLong(Reporter.getCurrentTestResult(), Calendar.getInstance().getTime().getTime());
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    finally
    {
      try
      {
        Field f = Reporter.getCurrentTestResult().getClass().getDeclaredField("m_startMillis");
        f.setAccessible(true);
        f.setLong(Reporter.getCurrentTestResult(), Calendar.getInstance().getTime().getTime());
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    
    driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
    Log.event("Driver::initialize::Get", StopWatch.elapsedTime(startTime));
    Log.addTestRunMachineInfo(driver);
    return driver;
  }
  

















  public static WebDriver newWebDriverInstanceFromEnvironment(String testName)
  {
    if ((System.getenv("SAUCE_USER_NAME") != null) || (System.getProperty("SAUCE_USER_NAME") != null)) {
      SauceLabsCapabilitiesConfiguration caps = new SauceLabsCapabilitiesConfiguration(testName, 
        System.getenv("BUILD_ID"));
      String screenResolution = configProperty.hasProperty("screenResolution") ? 
        configProperty.getProperty("screenResolution") : null;
      String seleniumVersion = configProperty.hasProperty("seleniumVersion") ? 
        configProperty.getProperty("seleniumVersion") : null;
      String iedriverVersion = configProperty.hasProperty("iedriverVersion") ? 
        configProperty.getProperty("iedriverVersion") : null;
      String chromedriverVersion = configProperty.hasProperty("chromedriverVersion") ? 
        configProperty.getProperty("chromedriverVersion") : null;
      configProperty.hasProperty("chromedriverVersion");
      configProperty.getProperty("chromedriverVersion");
      String maxTestDuration = configProperty.hasProperty("maxTestDuration") ? 
        configProperty.getProperty("maxTestDuration") : null;
      String commandTimeout = configProperty.hasProperty("commandTimeout") ? 
        configProperty.getProperty("commandTimeout") : null;
      String idleTimeout = configProperty.hasProperty("idleTimeout") ? configProperty.getProperty("idleTimeout") : 
        null;
      
      caps.setRecordSnapshot(false);
      

      if (screenResolution != null) {
        caps.setScreenResolution(screenResolution);
      }
      


      if (seleniumVersion != null)
        caps.setSeleniumVersion(seleniumVersion);
      if (iedriverVersion != null)
        caps.setIeDriverVersion(iedriverVersion);
      if (chromedriverVersion != null) {
        caps.setChromeDriverVersion(chromedriverVersion);
      }
      

      if (maxTestDuration != null)
        caps.setMaxTestDuration(maxTestDuration);
      if (commandTimeout != null)
        caps.setCommandTimeout(commandTimeout);
      if (idleTimeout != null) {
        caps.setIdleTimeout(idleTimeout);
      }
      WebDriver driver = null;
      String userAgent = null;
      
      if (System.getProperty("runUserAgentDeviceTest") != null) {
        if (System.getProperty("runUserAgentDeviceTest").equalsIgnoreCase("true")) {
          deviceName = System.getProperty("deviceName") != null ? System.getProperty("deviceName") : null;
          userAgent = mobEmuUA.getUserAgent(deviceName) != null ? mobEmuUA.getUserAgent(deviceName) : null;
          
          if ((deviceName != null) && (userAgent != null)) {
            driver = 
              SeleniumFactory.createWebDriver(caps.getUserAgentDesiredCapabilities(caps, deviceName, userAgent));
          } else {
            logger.error("Invalid mobile emulation configuration, check the parameters(deviceName) value: " + 
              deviceName);
          }
        } else {
          logger.error("runUserAgentDeviceTest value has been set as false");
        }
      } else {
        driver = SeleniumFactory.createWebDriver(caps.getDesiredCapabilities());
      }
      String saucelabsSessionId = ((RemoteWebDriver)driver).getSessionId().toString();
      String sauceLink = "http://saucelabs.com/jobs/" + saucelabsSessionId + "?auth=" + newHMACMD5Digest(
        new StringBuilder(String.valueOf(System.getenv("SAUCE_USER_NAME"))).append(":").append(System.getenv("SAUCE_API_KEY")).toString(), saucelabsSessionId);
      logger.debug("Saucelab link for " + testName + ":: " + sauceLink);
      Log.addSauceJobUrlToReport(driver, sauceLink);
      return driver;
    }
    return null;
  }
  










  public static String newHMACMD5Digest(String keyString, String msg)
  {
    String sEncodedString = null;
    try {
      SecretKeySpec key = new SecretKeySpec(keyString.getBytes("UTF-8"), "HmacMD5");
      Mac mac = Mac.getInstance("HmacMD5");
      mac.init(key);
      
      byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));
      
      StringBuffer hash = new StringBuffer();
      
      for (int i = 0; i < bytes.length; i++) {
        String hex = Integer.toHexString(0xFF & bytes[i]);
        if (hex.length() == 1) {
          hash.append('0');
        }
        hash.append(hex);
      }
      sEncodedString = hash.toString();
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException) {}catch (InvalidKeyException localInvalidKeyException) {}catch (NoSuchAlgorithmException localNoSuchAlgorithmException) {}
    

    return sEncodedString;
  }
  








  public static final String getTestSessionNodeIP(WebDriver driver)
    throws Exception
  {
    XmlTest test = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest();
    driverHost = System.getProperty("hubHost") != null ? System.getProperty("hubHost") : 
      test.getParameter("deviceHost");
    driverPort = test.getParameter("devicePort");
    HttpHost host = new HttpHost(driverHost, Integer.parseInt(driverPort));
    HttpClient client = HttpClientBuilder.create().build();
    URL testSessionApi = new URL("http://" + driverHost + ":" + driverPort + "/grid/api/testsession?session=" + 
      ((RemoteWebDriver)driver).getSessionId());
    BasicHttpEntityEnclosingRequest r = new BasicHttpEntityEnclosingRequest("POST", 
      testSessionApi.toExternalForm());
    HttpResponse response = client.execute(host, r);
    JSONObject object = new JSONObject(EntityUtils.toString(response.getEntity()));
    String nodeIP = object.getString("proxyId").toLowerCase();
    nodeIP = nodeIP.replace("http://", "");
    nodeIP = nodeIP.replaceAll(":[0-9]{1,5}", "").trim();
    return nodeIP;
  }
  








  public static final String getHubSession(WebDriver driver)
    throws Exception
  {
    XmlTest test = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest();
    driverHost = System.getProperty("hubHost") != null ? System.getProperty("hubHost") : 
      test.getParameter("deviceHost");
    driverPort = test.getParameter("devicePort");
    HttpHost host = new HttpHost(driverHost, Integer.parseInt(driverPort));
    HttpClient client = HttpClientBuilder.create().build();
    URL testSessionApi = new URL("http://" + driverHost + ":" + driverPort + "/grid/api/testsession?session=" + 
      ((RemoteWebDriver)driver).getSessionId());
    BasicHttpEntityEnclosingRequest r = new BasicHttpEntityEnclosingRequest("POST", 
      testSessionApi.toExternalForm());
    HttpResponse response = client.execute(host, r);
    JSONObject object = new JSONObject(EntityUtils.toString(response.getEntity()));
    String nodeIP = object.getString("proxyId").toLowerCase();
    nodeIP = nodeIP.replace("http://", "");
    nodeIP = nodeIP.replaceAll(":[0-9]{1,5}", "").trim();
    return nodeIP;
  }
  











  public static DesiredCapabilities setChromeUserAgent(String deviceName, String userAgent)
  {
    Map<String, Object> deviceMetrics = new HashMap();
    Map<String, Object> mobileEmulation = new HashMap();
    
    int width = 0;
    int height = 0;
    Double pixRatio = null;
    
    width = Integer.valueOf(mobEmuUA.getDeviceWidth(deviceName)).intValue();
    height = Integer.valueOf(mobEmuUA.getDeviceHeight(deviceName)).intValue();
    pixRatio = Double.valueOf(mobEmuUA.getDevicePixelRatio(deviceName));
    
    deviceMetrics.put("width", Integer.valueOf(width));
    deviceMetrics.put("height", Integer.valueOf(height));
    deviceMetrics.put("pixelRatio", pixRatio);
    mobileEmulation.put("deviceMetrics", deviceMetrics);
    mobileEmulation.put("userAgent", userAgent);
    Log.event("mobileEmulation settings::==> " + mobileEmulation);
    opt.setExperimentalOption("mobileEmulation", mobileEmulation);
    chromeCapabilities.setCapability("goog:chromeOptions", opt);
    return chromeCapabilities;
  }
  




  public static void printHarData(Har har)
  {
    HarLog log = har.getLog();
    List<HarEntry> harEntries = log.getEntries();
    Long totalSize = Long.valueOf(0L);
    int callCount = 0;
    long totalTime = 0L;
    for (HarEntry entry : harEntries) {
      callCount++;
      if (entry.getResponse() != null)
      {


        totalSize = Long.valueOf(totalSize.longValue() + entry.getResponse().getBodySize());
        totalTime += entry.getTime(TimeUnit.MILLISECONDS);
      } }
    HarSummary summary = new HarSummary(totalSize.longValue() / 1024.0D, callCount, totalTime);
    Log.message("#################<b>PERF DATA</b>###################");
    Log.message("<br>");
    Log.message("Call count : " + summary.getCallCount());
    Log.message("Size : " + summary.getTotalPayloadSize() / 1024.0D + " MB");
    Log.message("Total load time : " + summary.getTotalLoadTime() / 1000L + " seconds");
  }
}
