package com.dwre.support;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.xml.XmlTest;


public class Log
{
  public static boolean printconsoleoutput;
  private static String screenShotFolderPath;
  private static AtomicInteger screenShotIndex = new AtomicInteger(0);
  private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
  
  static final String TEST_TITLE_HTML_BEGIN = "&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">";
  
  static final String TEST_TITLE_HTML_END = "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div>";
  
  static final String TEST_COND_HTML_BEGIN = "&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#0000FF\">";
  
  static final String TEST_COND_HTML_END = "</font> </strong> </div>&emsp;";
  
  static final String MESSAGE_HTML_BEGIN = "<div class=\"test-message\">&emsp;";
  
  static final String MESSAGE_HTML_END = "</div>";
  
  static final String PASS_HTML_BEGIN = "<div class=\"test-result\"><br><font color=\"green\"><strong> ";
  
  static final String PASS_HTML_END1 = " </strong></font> ";
  
  static final String PASS_HTML_END2 = "</div>&emsp;";
  
  static final String FAIL_HTML_BEGIN = "<div class=\"test-result\"><br><font color=\"red\"><strong> ";
  
  static final String FAIL_HTML_END1 = " </strong></font> ";
  static final String FAIL_HTML_END2 = "</div>&emsp;";
  static final String SKIP_EXCEPTION_HTML_BEGIN = "<div class=\"test-result\"><br><font color=\"orange\"><strong> ";
  static final String SKIP_HTML_END1 = " </strong></font> ";
  static final String SKIP_HTML_END2 = " </strong></font> ";
  static final String EVENT_HTML_BEGIN = "<div class=\"test-event\"> <font color=\"maroon\"> <small> &emsp; &emsp;--- ";
  static final String EVENT_HTML_END = "</small> </font> </div>";
  static final String TRACE_HTML_BEGIN = "<div class=\"test-event\"> <font color=\"brown\"> <small> &emsp; &emsp;--- ";
  static final String TRACE_HTML_END = "</small> </font> </div>";
  
  static
  {
    try
    {
      Properties props = new Properties();
      InputStream cpr = Log.class.getResourceAsStream("/log4j.properties");
      props.load(cpr);
      PropertyConfigurator.configure(props);
    } catch (IOException e) {
      e.printStackTrace();
    }
    

    File screenShotFolder = new File(Reporter.getCurrentTestResult().getTestContext().getOutputDirectory());
    screenShotFolderPath = screenShotFolder.getParent() + File.separator + "ScreenShot" + File.separator;
    screenShotFolder = new File(screenShotFolderPath);
    
    if (!screenShotFolder.exists()) {
      screenShotFolder.mkdir();
    }
    
    File[] screenShots = screenShotFolder.listFiles();
    

    if ((screenShots != null) && (screenShots.length > 0)) {
      for (File screenShot : screenShots) {
        screenShot.delete();
      }
    }
    
    Map<String, String> params = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest()
      .getAllParameters();
    
    if (params.containsKey("printconsoleoutput")) {
      printconsoleoutput = Boolean.parseBoolean((String)params.get("printconsoleoutput"));
    }
  }
  




  public Log() {}
  




  public static String takeScreenShot(WebDriver driver)
  {
    String inputFile = "";
    inputFile = Reporter.getCurrentTestResult().getName() + "_" + screenShotIndex.incrementAndGet() + ".png";
    ScreenshotManager.takeScreenshot(driver, screenShotFolderPath + inputFile);
    return inputFile;
  }
  








  public static String getScreenShotHyperLink(String inputFile)
  {
    String screenShotLink = "";
    screenShotLink = "<a href=\"." + File.separator + "ScreenShot" + File.separator + inputFile + 
      "\" target=\"_blank\" >[ScreenShot]</a>";
    return screenShotLink;
  }
  






  public static void addTestRunMachineInfo(WebDriver driver)
  {
    Object[] params = Reporter.getCurrentTestResult().getParameters();
    String testMachine = "";
    String hub = "localhost";
    try
    {
      hub = Reporter.getCurrentTestResult().getHost() == null ? Inet4Address.getLocalHost().getHostName() : 
        Reporter.getCurrentTestResult().getHost();
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }
    try {
      testMachine = 
        "(Browser: " + ((RemoteWebDriver)driver).getCapabilities().getBrowserName() + ", Hub: " + hub + ", Node: " + WebDriverFactory.getTestSessionNodeIP(driver).toUpperCase() + ")";
    } catch (Exception e) {
      e.printStackTrace();
    }
    if ((params == null) || (params.length == 0)) {
      params = new Object[1];
    }
    params[0] = 
      (testMachine + ", " + ((RemoteWebDriver)driver).getCapabilities().getBrowserName() + "_" + ((RemoteWebDriver)driver).getCapabilities().getPlatform());
    Reporter.getCurrentTestResult().setParameters(params);
    ExtentReporter.addAttribute(new String[] {params[0].toString() });
  }
  


  public static Logger lsLog4j()
  {
    return Logger.getLogger(Thread.currentThread().getName());
  }
  


  public static String callerClass()
  {
    return Thread.currentThread().getStackTrace()[2].getClassName();
  }
  







  public static void testCaseInfo(String description)
  {
    lsLog4j().setLevel(Level.ALL);
    lsLog4j().info("");
    lsLog4j().log(callerClass(), Level.INFO, "****             " + description + "             *****", null);
    lsLog4j().info("");
    
    if (Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("<div class=\"test-title\">")) {
      Reporter.log("&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">" + description + "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div>" + "&emsp;");
    } else {
      Reporter.log("&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">" + description + "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div>" + "<!-- Report -->&emsp;");
    }
    ExtentReporter.testCaseInfo(description);
  }
  
  public static void testCaseInfo(HashMap<String, String> testdata) {
    String description = testdata.containsKey("Description") ? ((String)testdata.get("Description")).trim() : null;
    String bugId = testdata.containsKey("BugId") ? (String)testdata.get("BugId") : null;
    String comments = testdata.containsKey("Comments") ? (String)testdata.get("Comments") : null;
    
    lsLog4j().setLevel(Level.ALL);
    lsLog4j().info("");
    lsLog4j().log(callerClass(), Level.INFO, "****             " + description + "             *****", null);
    lsLog4j().info("");
    
    if (Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("<div class=\"test-title\">")) {
      if ((bugId != null) && (comments != null)) {
        Reporter.log(
          "&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">" + description + " BUGID:" + bugId + " COMMENTS:" + comments + "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div>" + "&emsp;");
      } else if ((bugId != null) && (comments == null)) {
        Reporter.log("&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">" + description + " BUGID:" + bugId + "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div>" + "&emsp;");
      } else if ((bugId == null) && (comments != null)) {
        Reporter.log(
          "&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">" + description + " COMMENTS:" + comments + "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div>" + "&emsp;");
      } else {
        Reporter.log("&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">" + description + "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div>" + "&emsp;");
      }
    } else if ((bugId != null) && (comments != null)) {
      Reporter.log(
        "&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">" + description + " BUGID:" + bugId + " COMMENTS:" + comments + "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div>" + "<!-- Report -->&emsp;");
    } else if ((bugId != null) && (comments == null)) {
      Reporter.log(
        "&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">" + description + " BUGID:" + bugId + "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div>" + "<!-- Report -->&emsp;");
    } else if ((bugId == null) && (comments != null)) {
      Reporter.log(
        "&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">" + description + " COMMENTS:" + comments + "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div>" + "<!-- Report -->&emsp;");
    } else {
      Reporter.log("&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">" + description + "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div>" + "<!-- Report -->&emsp;");
    }
    ExtentReporter.testCaseInfo(description);
  }
  





  public static void testConditionInfo(String description)
  {
    Reporter.log("&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#0000FF\">" + description + "</font> </strong> </div>&emsp;");
  }
  


  public static void testCaseResult()
  {
    if (Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("FAILSOFT")) {
      fail("Test Failed. Check the steps above in red color.");
    } else if (Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("FAIL")) {
      fail("Test Failed. Check the steps above in red color.");
    } else {
      pass("Test Passed.");
    }
  }
  


  public static void endTestCase()
  {
    lsLog4j().info("****             -E---N---D-             *****");
    ExtentReporter.endTest();
  }
  






  public static void message(String description)
  {
    Reporter.log("<div class=\"test-message\">&emsp;" + description + "</div>");
    
    lsLog4j().log(callerClass(), Level.INFO, description, null);
    
    ExtentReporter.info(description);
  }
  

  public static void message1(String description)
  {
    Reporter.log(description);
    
    lsLog4j().log(callerClass(), Level.INFO, description, null);
    
    ExtentReporter.info(description);
  }
  












  public static void message(String description, WebDriver driver, Boolean... takeScreenShot)
  {
    boolean finalDecision = true;
    if ((takeScreenShot.length > 0) && (takeScreenShot[0].equals(Boolean.FALSE))) {
      finalDecision = false;
    }
    if ((configProperty.getProperty("isTakeScreenShot") != null) && 
      (configProperty.getProperty("isTakeScreenShot").equalsIgnoreCase("false"))) {
      finalDecision = false;
    }
    
    if (finalDecision) {
      String inputFile = takeScreenShot(driver);
      Reporter.log(
        "<div class=\"test-message\">&emsp;" + description + "&emsp;" + getScreenShotHyperLink(inputFile) + "</div>");
      ExtentReporter.info(description + " " + getScreenShotHyperLink(inputFile));
    } else {
      Reporter.log("<div class=\"test-message\">&emsp;" + description + "</div>");
      ExtentReporter.info(description);
    }
    lsLog4j().log(callerClass(), Level.INFO, description, null);
  }
  










  public static void message(WebDriver driver, String description, String screenshotfolderpath, String screenshotfileName)
    throws IOException
  {
    String inputFile = screenshotfileName + ".png";
    WebDriver augmented = new Augmenter().augment(driver);
    File screenshot = (File)((TakesScreenshot)augmented).getScreenshotAs(OutputType.FILE);
    FileUtils.copyFile(screenshot, new File(screenshotfolderpath + File.separator + inputFile));
    FileUtils.copyFile(screenshot, new File(screenShotFolderPath + File.separator + inputFile));
    Reporter.log("<div class=\"test-message\">&emsp;" + String.format("%-75s", new Object[] { description }).replace(" ", "&nbsp;") + "---&emsp;" + 
      getScreenShotHyperLink(inputFile) + "</div>");
    ExtentReporter.info(String.format("%-75s", new Object[] { description }).replace(" ", "&nbsp;") + "---&emsp;" + 
      getScreenShotHyperLink(inputFile));
    lsLog4j().log(callerClass(), Level.INFO, description, null);
  }
  











  public static String buildLogMessage(WebDriver driver, String description, String screenshotfolderpath, String screenshotfileName)
    throws IOException
  {
    String inputFile = screenshotfileName + ".png";
    WebDriver augmented = new Augmenter().augment(driver);
    File screenshot = (File)((TakesScreenshot)augmented).getScreenshotAs(OutputType.FILE);
    FileUtils.copyFile(screenshot, new File(screenshotfolderpath + File.separator + inputFile));
    FileUtils.copyFile(screenshot, new File(screenShotFolderPath + inputFile));
    return "<div class=\"test-message\">&emsp;" + String.format("%-75s", new Object[] { description }).replace(" ", "&nbsp;") + "---&emsp;" + 
      getScreenShotHyperLink(inputFile) + "</div>";
  }
  









  public static void message(String description, String screenshot)
    throws IOException
  {
    try
    {
      String inputFile = Reporter.getCurrentTestResult().getName() + "_" + screenShotIndex.incrementAndGet() + 
        ".png";
      FileUtils.copyFile(new File(screenshot), new File(screenShotFolderPath + inputFile));
      Reporter.log("<div class=\"test-message\">&emsp;" + description + getScreenShotHyperLink(inputFile) + "</div>");
      ExtentReporter.info(description + " " + getScreenShotHyperLink(inputFile));
      lsLog4j().log(callerClass(), Level.INFO, description, null);
    }
    catch (FileNotFoundException e) {
      Reporter.log("<div class=\"test-message\">&emsp;" + description + "</div>");
      ExtentReporter.info(description);
    }
  }
  







  public static void event(String description)
  {
    String currDate = new SimpleDateFormat("dd MMM HH:mm:ss SSS").format(Calendar.getInstance().getTime());
    Reporter.log("<div class=\"test-event\"> <font color=\"maroon\"> <small> &emsp; &emsp;--- " + currDate + " - " + description + "</small> </font> </div>");
    lsLog4j().log(callerClass(), Level.DEBUG, description, null);
    ExtentReporter.debug(description);
  }
  










  public static void event(String description, long duration)
  {
    String currDate = new SimpleDateFormat("dd MMM HH:mm:ss SSS").format(Calendar.getInstance().getTime());
    Reporter.log("<div class=\"test-event\"> <font color=\"maroon\"> <small> &emsp; &emsp;--- " + currDate + " - <b>" + duration + "</b> - " + description + " - " + 
      Thread.currentThread().getStackTrace()[2].toString() + "</small> </font> </div>");
    lsLog4j().log(callerClass(), Level.DEBUG, description, null);
    ExtentReporter.debug(currDate + " - <b>" + duration + "</b> - " + description + " - " + 
      Thread.currentThread().getStackTrace()[2].toString());
  }
  







  public static void trace(String description)
  {
    String currDate = new SimpleDateFormat("dd MMM HH:mm:ss SSS").format(Calendar.getInstance().getTime());
    Reporter.log("<div class=\"test-event\"> <font color=\"brown\"> <small> &emsp; &emsp;--- " + currDate + " - " + description + "</small> </font> </div>");
    lsLog4j().log(callerClass(), Level.TRACE, description, null);
  }
  











  public static void trace(String description, long duration)
  {
    String currDate = new SimpleDateFormat("dd MMM HH:mm:ss SSS").format(Calendar.getInstance().getTime());
    Reporter.log("<div class=\"test-event\"> <font color=\"brown\"> <small> &emsp; &emsp;--- " + currDate + " - <b>" + duration + "</b> - " + description + " - " + 
      Thread.currentThread().getStackTrace()[2].toString() + "</small> </font> </div>");
    lsLog4j().log(callerClass(), Level.TRACE, description, null);
  }
  







  public static void pass(String description)
  {
    Reporter.log("<div class=\"test-result\"><br><font color=\"green\"><strong> " + description + " </strong></font> " + "</div>&emsp;");
    lsLog4j().log(callerClass(), Level.INFO, description, null);
    ExtentReporter.pass(description);
  }
  








  public static void pass(String description, WebDriver driver)
  {
    if ((configProperty.getProperty("isTakeScreenShot") != null) && 
      (configProperty.getProperty("isTakeScreenShot").equalsIgnoreCase("true"))) {
      String inputFile = takeScreenShot(driver);
      Reporter.log("<div class=\"test-result\"><br><font color=\"green\"><strong> " + description + " </strong></font> " + getScreenShotHyperLink(inputFile) + 
        "</div>&emsp;");
      ExtentReporter.pass(description + " " + getScreenShotHyperLink(inputFile));
    } else {
      Reporter.log("<div class=\"test-result\"><br><font color=\"green\"><strong> " + description + " </strong></font> " + "</div>&emsp;");
      ExtentReporter.pass(description);
    }
    lsLog4j().log(callerClass(), Level.INFO, description, null);
  }
  








  public static void fail(String description, WebDriver driver)
  {
    String inputFile = takeScreenShot(driver);
    Reporter.log("<!--FAIL-->");
    Reporter.log(
      "<div class=\"test-result\"><br><font color=\"red\"><strong> " + description + " </strong></font> " + getScreenShotHyperLink(inputFile) + "</div>&emsp;");
    ExtentReporter.fail(description + " " + getScreenShotHyperLink(inputFile));
    ExtentReporter.logStackTrace(new AssertionError(description));
    lsLog4j().log(callerClass(), Level.ERROR, description, null);
    Assert.fail(description);
  }
  





  public static void fail(String description)
  {
    lsLog4j().log(callerClass(), Level.ERROR, description, null);
    Reporter.log("<!--FAIL-->");
    Reporter.log("<div class=\"test-result\"><br><font color=\"red\"><strong> " + description + " </strong></font> " + "</div>&emsp;");
    ExtentReporter.fail(description);
    ExtentReporter.logStackTrace(new AssertionError(description));
    Assert.fail(description);
  }
  




  public static boolean hasFailSofts()
  {
    return Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("FAILSOFT");
  }
  









  public static void failsoft(String description, WebDriver driver)
  {
    String inputFile = takeScreenShot(driver);
    Reporter.log("<div class=\"test-result\"><font color=\"red\">&emsp;" + description + "</font>" + 
      getScreenShotHyperLink(inputFile) + "</div>");
    Reporter.log("<!--FAILSOFT-->");
    ExtentReporter.fail(description + " " + getScreenShotHyperLink(inputFile));
    lsLog4j().log(callerClass(), Level.ERROR, description, null);
  }
  







  public static void failsoft(String description)
  {
    Reporter.log("<!--FAILSOFT-->");
    Reporter.log("<div class=\"test-result\"><font color=\"red\">&emsp;" + description + "</font></div>");
    ExtentReporter.fail(description);
    lsLog4j().log(callerClass(), Level.ERROR, description, null);
  }
  









  public static String buildfailsoftMessage(String description, WebDriver driver)
  {
    String inputFile = takeScreenShot(driver);
    return "<div class=\"test-result\">&emsp; <font color=\"red\"><strong>" + description + " </strong> </font>" + 
      getScreenShotHyperLink(inputFile) + "</div>&emsp;";
  }
  









  public static void exception(Exception e, WebDriver driver)
    throws Exception
  {
    String screenShotLink = "";
    try {
      String inputFile = takeScreenShot(driver);
      screenShotLink = getScreenShotHyperLink(inputFile);
    }
    catch (Exception localException) {}
    
    String eMessage = e.getMessage();
    if ((eMessage != null) && (eMessage.contains("\n"))) {
      eMessage = eMessage.substring(0, eMessage.indexOf("\n"));
    }
    lsLog4j().log(callerClass(), Level.FATAL, eMessage, e);
    if ((e instanceof SkipException)) {
      Reporter.log("<div class=\"test-result\"><br><font color=\"orange\"><strong> " + eMessage + " </strong></font> " + screenShotLink + " </strong></font> ");
      ExtentReporter.skip(eMessage + " " + screenShotLink);
      ExtentReporter.logStackTrace(e);
    } else {
      Reporter.log("<!--FAIL-->");
      Reporter.log("<div class=\"test-result\"><br><font color=\"red\"><strong> " + eMessage + " </strong></font> " + screenShotLink + "</div>&emsp;");
      ExtentReporter.fail(eMessage + " " + screenShotLink);
      ExtentReporter.logStackTrace(e);
    }
    throw e;
  }
  







  public static void exception(Exception e)
    throws Exception
  {
    String eMessage = e.getMessage();
    
    if ((eMessage != null) && (eMessage.contains("\n"))) {
      eMessage = eMessage.substring(0, eMessage.indexOf("\n"));
    }
    lsLog4j().log(callerClass(), Level.FATAL, eMessage, e);
    if ((e instanceof SkipException)) {
      Reporter.log("<div class=\"test-result\"><br><font color=\"orange\"><strong> " + eMessage + " </strong></font> " + " </strong></font> ");
      ExtentReporter.skip(eMessage);
      ExtentReporter.logStackTrace(e);
    } else {
      Reporter.log("<!--FAIL-->");
      Reporter.log("<div class=\"test-result\"><br><font color=\"red\"><strong> " + eMessage + " </strong></font> " + "</div>&emsp;");
      ExtentReporter.fail(eMessage);
      ExtentReporter.logStackTrace(e);
    }
    throw e;
  }
  











  public static void assertThat(boolean status, String passmsg, String failmsg)
  {
    if (!status) {
      fail(failmsg);
    } else {
      message(passmsg);
    }
  }
  














  public static void assertThat(boolean status, String passmsg, String failmsg, WebDriver driver)
  {
    if (!status) {
      fail(failmsg, driver);
    } else {
      message(passmsg, driver, new Boolean[0]);
    }
  }
  












  public static void softAssertThat(boolean status, String passmsg, String failmsg)
  {
    if (!status) {
      failsoft(failmsg);
    } else {
      message(passmsg);
    }
  }
  















  public static void softAssertThat(boolean status, String passmsg, String failmsg, WebDriver driver)
  {
    if (!status) {
      failsoft(failmsg, driver);
    } else {
      message(passmsg, driver, new Boolean[0]);
    }
  }
  










  public static void addSauceJobUrlToReport(WebDriver driver, String sauceLink)
  {
    Object[] params = Reporter.getCurrentTestResult().getParameters();
    if ((params == null) || (params.length == 0)) {
      params = new Object[1];
    }
    params[0] = 
    
      (params[0] + "<br>SauceLab link: <a href='" + sauceLink + "' target='_blank'>" + sauceLink + "</a>");
    Reporter.getCurrentTestResult().setParameters(params);
    ExtentReporter.addAttribute(new String[] {"SauceLab link: <a href='" + sauceLink + "' target='_blank'>" + sauceLink + "</a>" });
  }
}
