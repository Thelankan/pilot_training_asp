package com.dwre.pages.products;

import com.dwre.pages.ElementLayer;
import com.dwre.pages.Navigation;
import com.dwre.support.BrowserActions;
import com.dwre.support.DWRE_Utils;
import com.dwre.support.Log;
import com.dwre.support.MerchantTools;
import com.dwre.support.Utils;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;


public class Products
  extends LoadableComponent<Products>
{
  private WebDriver driver;
  private boolean isPageLoaded;
  public ElementLayer elementLayer;
  String runPltfrm = Utils.getRunPlatForm();
  

  @FindBy(id="SimpleDiv")
  WebElement readyElement;
  

  @FindBy(id="login")
  WebElement LoginPage;
  

  @FindBy(linkText="Lock")
  WebElement lockSession;
  

  @FindBy(linkText="Unlock")
  WebElement unLockSession;
  

  @FindBy(name="WFSimpleSearch_NameOrID")
  WebElement txtProductIDinSimpleSearch;
  

  @FindBy(name="WFSimpleSearch_IDList")
  WebElement txtProductIdinIDSearch;
  
  @FindBy(name="findSimple")
  WebElement btnFindInSimpleSearch;
  
  @FindBy(name="findIDList")
  WebElement btnFindInIdSearch;
  
  @FindBy(id="allocation")
  WebElement txtInventoryAllocation;
  
  @FindBy(name="update")
  WebElement btnApply;
  
  @FindBy(name="create")
  WebElement btnCreate;
  
  @FindBy(xpath="//button[contains(text(),'Yes')]")
  WebElement btnYes;
  
  @FindBy(xpath="//span[text()='In-Stock']")
  WebElement lblInStock;
  
  @FindBy(xpath="//span[@data-dw-tooltip='Product.ID']/../../../../..//following-sibling::td//td[@class='table_detail']/input[@type='text']")
  WebElement editProductId;
  
  @FindBy(xpath="//span[@data-dw-tooltip='Product.searchable']/../../../../..//following-sibling::td//td[@class='table_detail']/select[@class='inputfield_en']")
  WebElement editdropdownSearchable;
  
  @FindBy(xpath="//span[@data-dw-tooltip='Product.searchableIfUnavailable']/../../../../..//following-sibling::td//td[@class='table_detail']/select[@class='inputfield_en']")
  WebElement editdropdownSearchableifUnavailable;
  
  @FindBy(xpath="//span[@data-dw-tooltip='Product.name']/../../../../..//following-sibling::td//td[@class='table_detail']/input[@type='text']")
  WebElement editProductName;
  
  @FindBy(xpath="//span[@data-dw-tooltip='Product.onlineFlag']/../../../../..//following-sibling::td//td[@class='table_detail']/select[@class='inputfield_en']")
  WebElement editdropdownOnline;
  
  @FindBy(xpath="//span[@data-dw-tooltip='Product.searchPlacement']/../../../../..//following-sibling::td//td[@class='table_detail']/select[@class='inputfield_en']")
  WebElement editdropdownSearchPlacement;
  
  @FindBy(xpath="//span[@data-dw-tooltip='Product.searchRank']/../../../../..//following-sibling::td//td[@class='table_detail']/select[@class='inputfield_en']")
  WebElement editdropdownSearchRank;
  
  @FindBy(xpath="//span[@data-dw-tooltip='Product.pinterestEnabled']/../../../../..//following-sibling::td//td[@class='table_detail']/select[@class='inputfield_en']")
  WebElement editdropdownPinterestEnabled;
  
  @FindBy(xpath="//span[@data-dw-tooltip='Product.facebookEnabled']/../../../../..//following-sibling::td//td[@class='table_detail']/select[@class='inputfield_en']")
  WebElement editdropdownFacebookEnabled;
  
  @FindBy(css="button[class='button apply']")
  WebElement btnApplyAfterEdit;
  
  @FindBy(css="button[name='new']")
  WebElement btnEditCategories;
  
  @FindBy(css="#CatalogSelectionPanel img")
  WebElement dropDownCategory;
  
  @FindBy(css="button[name='finish']")
  WebElement btnApplyCategory;
  

  public Products(WebDriver driver)
  {
    this.driver = driver;
    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 
      Utils.maxElementWait);
    PageFactory.initElements(finder, this);
  }
  

  protected void isLoaded()
  {
    if (!isPageLoaded) {
      Assert.fail();
    }
    try
    {
      if (LoginPage.isDisplayed())
        new Navigation(driver).login(driver);
      driver.get(DWRE_Utils.generateURL(MerchantTools.Products));
    }
    catch (Exception localException) {}
    

    if ((isPageLoaded) && (!Utils.waitForElement(driver, readyElement, 30))) {
      Log.fail("Products Page didn't display", driver);
    }
    
    elementLayer = new ElementLayer(driver);
  }
  

  protected void load()
  {
    isPageLoaded = true;
    driver.get(DWRE_Utils.generateURL(MerchantTools.Products));
    Utils.waitForPageLoad(driver);
  }
  


  public void lockSession()
    throws Exception
  {
    if (Utils.waitForElement(driver, lockSession)) {
      BrowserActions.clickOnElement(lockSession, driver, "Lock Link");
      Utils.waitForPageLoad(driver);
    }
  }
  


  public void unLockSession()
    throws Exception
  {
    if (Utils.waitForElement(driver, unLockSession)) {
      BrowserActions.clickOnElement(unLockSession, driver, "Lock Link");
      Utils.waitForPageLoad(driver);
    }
  }
  



  public void navigateTo(String TabName)
    throws Exception
  {
    WebElement currentTab = driver.findElement(
      By.cssSelector(".table_tabs_en_background"));
    if (!currentTab.getAttribute("innerHTML").equalsIgnoreCase(TabName)) {
      WebElement tabToNav = driver
        .findElement(
        By.xpath("//td[contains(@class,'table_tabs_dis_background')]/a[contains(text(),'" + 
        TabName + "')]"));
      BrowserActions.clickOnElement(tabToNav, driver, "Tab");
      Utils.waitForPageLoad(driver);
    }
  }
  






  public void setInventory(String productID, String inventoryName, String value)
    throws Exception
  {
    LinkedHashMap<String, WebElement> instructions = new LinkedHashMap();
    



    instructions.put("type_Product Name / ID_" + productID, 
      txtProductIDinSimpleSearch);
    instructions.put("click_Find Button", btnFindInSimpleSearch);
    
    BrowserActions.fillFormDetails(instructions, driver);
    Utils.waitForPageLoad(driver);
    
    WebElement idLink = driver.findElement(By.linkText(productID));
    BrowserActions.clickOnElement(idLink, driver, "Product ID Link");
    
    navigateTo("Inventory");
    

    WebElement btnEditInventory = driver
      .findElement(By.xpath("//a[contains(text(),'" + inventoryName + 
      "')]/../following-sibling::td/button[@name='reset']"));
    BrowserActions.clickOnElement(btnEditInventory, driver, "Edit Button");
    Utils.waitForPageLoad(driver);
    
    BrowserActions.typeOnTextField(txtInventoryAllocation, value, driver, 
      "Inventory Value textbox");
    
    if (Utils.waitForElement(driver, btnCreate)) {
      BrowserActions.clickOnElement(btnCreate, driver, "Create");
      Utils.waitForPageLoad(driver);
    }
    
    BrowserActions.clickOnElement(btnApply, driver, "Apply");
    
    if (!Utils.waitForElement(driver, btnYes)) {
      Log.fail("Reset Allocation Confirmation dialog not opended. Please contact administrator.");
    }
    BrowserActions.clickOnElement(btnYes, driver, 
      "Yes Button in Allocaiton Confirmation Dialog");
    Utils.waitForPageLoad(driver);
    
    if (!Utils.waitForElement(driver, lblInStock)) {
      Log.fail("Something went wrong. Please contact administrator.");
    }
  }
  











  public void editProduct(String productID, LinkedHashMap EditAttribute_Value)
    throws Exception
  {
    LinkedHashMap<String, WebElement> instructions = new LinkedHashMap();
    LinkedHashMap<String, WebElement> executeInstructions = new LinkedHashMap();
    

    instructions.put("type_Product Name / ID_" + productID, 
      txtProductIDinSimpleSearch);
    instructions.put("click_Find Button", btnFindInSimpleSearch);
    
    BrowserActions.fillFormDetails(instructions, driver);
    Utils.waitForPageLoad(driver);
    
    WebElement idLink = driver.findElement(By.linkText(productID));
    BrowserActions.clickOnElement(idLink, driver, "Product ID Link");
    
    lockSession();
    
    Set EditDetailsSet = EditAttribute_Value.entrySet();
    Iterator EditDetailsIterator = EditDetailsSet.iterator();
    
    while (EditDetailsIterator.hasNext())
    {
      Map.Entry mapEntry = (Map.Entry)EditDetailsIterator.next();
      String attributeName = mapEntry.getKey().toString();
      String data = (String)mapEntry.getValue();
      String str1;
      switch ((str1 = attributeName.toLowerCase()).hashCode()) {case -1224285429:  if (str1.equals("searchable_if_unavailable")) {} break; case -1012222381:  if (str1.equals("online")) {} break; case -710088958:  if (str1.equals("searchable")) {} break; case -539301789:  if (str1.equals("search_rank")) {} break; case -252401976:  if (str1.equals("facebook_enabled")) {} break; case 3355:  if (str1.equals("id")) break; break; case 3373707:  if (str1.equals("name")) {} break; case 473202958:  if (str1.equals("search_placement")) {} break; case 563294108:  if (!str1.equals("pinterest_enabled"))
        {

          continue;executeInstructions.put("type_ProductId_" + data, 
            editProductId);
          Log.message("Edited Product Id: " + data);
          
          continue;
          
          executeInstructions.put("select_SearchableDropdown_" + data, editdropdownSearchable);
          Log.message("Edited Searchable: " + data);
          continue;
          
          executeInstructions
            .put("select_SearchableIfUnavailableDropdown_" + 
            data, editdropdownSearchableifUnavailable);
          Log.message("Edited searchable if unavailable: " + data);
          continue;
          
          executeInstructions.put("type_ProductName_" + data, 
            editProductName);
          Log.message("Edited Product Name: " + data);
          continue;
          
          executeInstructions.put("select_OnlineDropdown_" + data, 
            editdropdownOnline);
          Log.message("Edited Online Status: " + data);
          continue;
          
          executeInstructions.put("select_SearchPlacementDropdown_" + 
            data, editdropdownSearchPlacement);
          Log.message("Edited Search Placement: " + data);
          continue;
          
          executeInstructions.put("select_SearchRankDropdown_" + data, editdropdownSearchRank);
          Log.message("Edited Search Rank: " + data);
        }
        else {
          executeInstructions.put("select_PinterestEnabledDropdown_" + 
            data, editdropdownPinterestEnabled);
          Log.message("Edited Pinterest enabled: " + data);
          continue;
          
          executeInstructions.put("select_FacebookEnabledDropdown_" + 
            data, editdropdownFacebookEnabled);
          Log.message("Edited Facebook enabled: " + data);
        }
        break;
      }
    }
    BrowserActions.fillFormDetails(executeInstructions, driver);
    BrowserActions.clickOnElement(btnApplyAfterEdit, driver, "Apply Button");
    Utils.waitForPageLoad(driver);
    
    unLockSession();
  }
  








  public void editCategory(String productID, String brand, String[] levels)
    throws Exception
  {
    LinkedHashMap<String, WebElement> instructions = new LinkedHashMap();
    
    instructions.put("type_Product Name / ID_" + productID, 
      txtProductIDinSimpleSearch);
    instructions.put("click_Find Button", btnFindInSimpleSearch);
    
    BrowserActions.fillFormDetails(instructions, driver);
    Utils.waitForPageLoad(driver);
    
    WebElement idLink = driver.findElement(By.linkText(productID));
    BrowserActions.clickOnElement(idLink, driver, "Product ID Link");
    
    navigateTo("Categories");
    
    lockSession();
    
    if (Utils.waitForElement(driver, btnEditCategories)) {
      BrowserActions.clickOnElement(btnEditCategories, driver, 
        "Edit Category Button");
      Utils.waitForPageLoad(driver);
    }
    
    if (Utils.waitForElement(driver, dropDownCategory)) {
      BrowserActions.clickOnElement(dropDownCategory, driver, 
        "Edit Category Button");
      Utils.waitForPageLoad(driver);
      WebElement brandName = driver
        .findElement(
        By.xpath("//div[@class='x-combo-list-inner']/div[contains(text(),'" + 
        brand + "')]"));
      BrowserActions.clickOnElement(brandName, driver, 
        "Selecting Brand from drop down");
      Utils.waitForPageLoad(driver);
    }
    
    for (int i = 0; i < levels.length; i++)
    {
      Utils.waitForPageLoad(driver);
      BrowserActions.nap(2L);
      WebElement level = driver.findElement(
        By.xpath("//div//span[contains(text(),\"" + levels[i] + 
        "\")]/../../input[@type='checkbox']"));
      
      Utils.waitForElement(driver, level);
      BrowserActions.selectRadioOrCheckbox(level, "Yes");
      Log.message("Levels " + i + ":" + levels[i]);
      Utils.waitForPageLoad(driver);
      try
      {
        WebElement Expand = driver.findElement(
          By.xpath("//div//span[contains(text(),\"" + levels[i] + 
          "\")]/../../img[contains(@class,'plus')]"));
        
        if (Utils.waitForElement(driver, Expand))
        {
          BrowserActions.clickOnElement(Expand, driver, "Expand Icon");
          BrowserActions.nap(2L);
          Utils.waitForPageLoad(driver);
        }
      } catch (NoSuchElementException e) {
        Log.message("element is not found in Page" + e);
      }
      Utils.waitForPageLoad(driver);
    }
    
    if (Utils.waitForElement(driver, btnApplyCategory)) {
      BrowserActions.clickOnElement(btnApplyCategory, driver, 
        "Apply Category Button");
      Utils.waitForPageLoad(driver);
    }
    
    unLockSession();
  }
}
