package com.dwre.support;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.testng.IClass;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.SkipException;





public class ExtentReporter
{
  private static ExtentReports report = null;
  private static HashMap<String, ExtentTest> tests = new HashMap();
  private static File configFile = new File(System.getProperty("user.dir") + File.separator + "ReportConfig.xml");
  


  public ExtentReporter() {}
  


  private static String getTestName(ITestResult iTestResult)
  {
    String testClassName = iTestResult.getTestClass().getRealClass().getName();
    String testMethodName = iTestResult.getName();
    return testClassName + "#" + testMethodName;
  }
  





  private static Date getTime(long millis)
  {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(millis);
    return calendar.getTime();
  }
  





  private static void setInterruptedTestStatus(ITestResult iTestResult, ExtentTest extentTest)
  {
    if (!extentTest.getRunStatus().equals(LogStatus.UNKNOWN)) {
      return;
    }
    switch (iTestResult.getStatus()) {
    case 1: 
      extentTest.log(LogStatus.PASS, "<font color=\"green\">Test Passed</font>");
      break;
    case 2: 
      if (iTestResult.getThrowable() == null) {
        extentTest.log(LogStatus.FAIL, "<font color=\"red\">Test Failed</font>");
      } else
        extentTest.log(LogStatus.FAIL, "<div class=\"stacktrace\">" + ExceptionUtils.getStackTrace(iTestResult.getThrowable()) + "</div>");
      break;
    case 3: 
      if (iTestResult.getThrowable() == null) {
        extentTest.log(LogStatus.SKIP, "<font color=\"orange\">Test Skipped</font>");
      } else {
        extentTest.log(LogStatus.SKIP, "<div class=\"stacktrace\">" + ExceptionUtils.getStackTrace(iTestResult.getThrowable()) + "</div>");
      }
      

      break;
    }
    
  }
  


  private static synchronized ExtentReports getReportInstance(ITestResult iTestResult)
  {
    if (report == null) {
      String reportFilePath = new File(iTestResult.getTestContext().getOutputDirectory()).getParent() + File.separator + "ExtentReport.html";
      report = new ExtentReports(reportFilePath, Boolean.valueOf(true));
      if (configFile.exists()) {
        report.loadConfig(configFile);
      }
    }
    return report;
  }
  








  private static ExtentTest startTest(String description)
  {
    ExtentTest test = null;
    String testName = getTestName(Reporter.getCurrentTestResult());
    if (tests.containsKey(testName)) {
      test = (ExtentTest)tests.get(testName);
      if ((description != null) && (!description.isEmpty())) {
        test.setDescription(description);
      }
    } else {
      test = getReportInstance(Reporter.getCurrentTestResult()).startTest(testName, description);
      test.assignCategory(Reporter.getCurrentTestResult().getMethod().getGroups());
      tests.put(testName, test);
    }
    return test;
  }
  





  private static ExtentTest getTest()
  {
    return startTest("");
  }
  




  public static void testCaseInfo(String testCaseInfo)
  {
    startTest(testCaseInfo);
  }
  




  public static void info(String message)
  {
    getTest().log(LogStatus.INFO, message);
  }
  




  public static void debug(String event)
  {
    getTest().log(LogStatus.UNKNOWN, event);
  }
  




  public static void pass(String passMessage)
  {
    getTest().log(LogStatus.PASS, "<font color=\"green\">" + passMessage + "</font>");
  }
  




  public static void fail(String failMessage)
  {
    getTest().log(LogStatus.FAIL, "<font color=\"red\">" + failMessage + "</font>");
  }
  




  public static void skip(String message)
  {
    getTest().log(LogStatus.SKIP, "<font color=\"orange\">" + message + "</font>");
  }
  




  public static void logStackTrace(Throwable t)
  {
    if ((t instanceof Error)) {
      getTest().log(LogStatus.ERROR, "<div class=\"stacktrace\">" + ExceptionUtils.getStackTrace(t) + "</div>");
    } else if ((t instanceof SkipException)) {
      getTest().log(LogStatus.SKIP, "<div class=\"stacktrace\">" + ExceptionUtils.getStackTrace(t) + "</div>");
    } else {
      getTest().log(LogStatus.FATAL, "<div class=\"stacktrace\">" + ExceptionUtils.getStackTrace(t) + "</div>");
    }
  }
  




  public static void addAttribute(String... attribs)
  {
    getTest().assignAuthor(attribs);
  }
  


  public static void endTest()
  {
    getReportInstance(Reporter.getCurrentTestResult()).endTest(getTest());
  }
  





  public static void closeReport(List<ITestResult> allTestCaseResults, String outdir)
  {
    if ((report == null) && (allTestCaseResults.size() > 0)) {
      getReportInstance((ITestResult)allTestCaseResults.get(0));
    } else if ((report == null) && (allTestCaseResults.size() == 0)) {
      report = new ExtentReports(outdir + File.separator + "ExtentReport.html", Boolean.valueOf(true));
      if (configFile.exists()) {
        report.loadConfig(configFile);
      }
      report.endTest(report.startTest("Empty TestNG Suite", "To run tests, please add '@Test' annotation to your test methods"));
    }
    if (report != null) {
      String testName = null;
      ExtentTest extentTest = null;
      for (ITestResult iTestResult : allTestCaseResults) {
        testName = getTestName(iTestResult);
        if (!tests.containsKey(testName)) {
          extentTest = report.startTest(testName, iTestResult.getMethod().getDescription() == null ? "" : iTestResult.getMethod().getDescription());
          extentTest.setStartedTime(getTime(iTestResult.getStartMillis()));
          extentTest.assignCategory(iTestResult.getMethod().getGroups());
          setInterruptedTestStatus(iTestResult, extentTest);
          extentTest.setEndedTime(getTime(iTestResult.getEndMillis()));
          tests.put(testName, extentTest);
        } else {
          extentTest = (ExtentTest)tests.get(testName);
          if (extentTest.getEndedTime() == null) {
            setInterruptedTestStatus(iTestResult, extentTest);
            extentTest.setEndedTime(getTime(iTestResult.getEndMillis()));
          }
        }
      }
      for (ExtentTest eTest : tests.values()) {
        report.endTest(eTest);
      }
      try {
        report.flush();
        report.close();
      } catch (Exception e) {
        Log.event("Handling IO exception");
      }
    }
  }
}
