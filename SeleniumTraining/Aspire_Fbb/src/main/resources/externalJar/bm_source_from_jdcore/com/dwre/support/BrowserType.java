package com.dwre.support;




public enum BrowserType
{
  None("default"),  Chrome("Chrome"),  Firefox("firefox"),  IE("internet explorer"),  Safari("safari"),  PhantomJS(
    "phantomJS"),  Edge("MicrosoftEdge");
  
  private String _configuration;
  
  private BrowserType(String configuration) {
    _configuration = configuration;
  }
  
  public String getConfiguration() {
    return _configuration;
  }
  
  public static BrowserType fromConfiguration(String configuration) {
    if ((configuration == null) || (configuration.equalsIgnoreCase("chrome")) || 
      (configuration.equalsIgnoreCase("default")))
      return Chrome;
    if (configuration.equalsIgnoreCase("firefox"))
      return Firefox;
    if (configuration.equalsIgnoreCase("ie"))
      return IE;
    if (configuration.equalsIgnoreCase("safari"))
      return Safari;
    if (configuration.equalsIgnoreCase("phantomJS"))
      return PhantomJS;
    if (configuration.equalsIgnoreCase("edge")) {
      return Edge;
    }
    throw new IllegalArgumentException(
      "There is no value '" + configuration + "' in Enum '" + BrowserType.class.getName() + "'");
  }
}
