package com.dwre.pages.products;

import com.dwre.pages.ElementLayer;
import com.dwre.pages.Navigation;
import com.dwre.support.BrowserActions;
import com.dwre.support.DWRE_Utils;
import com.dwre.support.Log;
import com.dwre.support.MerchantTools;
import com.dwre.support.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

public class PromoImportExport
  extends LoadableComponent<PromoImportExport>
{
  private WebDriver driver;
  private boolean isPageLoaded;
  public ElementLayer elementLayer;
  String runPltfrm = Utils.getRunPlatForm();
  

  @FindBy(css="div[class='main-content dw-Promotiongrid']")
  WebElement readyElement;
  
  @FindBy(id="login")
  WebElement LoginPage;
  
  @FindBy(css="[dw-label='common.button.new']")
  WebElement btnNew;
  

  public PromoImportExport(WebDriver driver)
  {
    this.driver = driver;
    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 
      Utils.maxElementWait);
    PageFactory.initElements(finder, this);
  }
  

  protected void isLoaded()
  {
    if (!isPageLoaded) {
      Assert.fail();
    }
    try
    {
      if (LoginPage.isDisplayed())
        new Navigation(driver).login(driver);
      driver.get(DWRE_Utils.generateURL(MerchantTools.Promotions));
    }
    catch (Exception localException) {}
    

    if ((isPageLoaded) && (!Utils.waitForElement(driver, readyElement, 30))) {
      Log.fail("Promotions Page didn't display", driver);
    }
    
    elementLayer = new ElementLayer(driver);
  }
  

  protected void load()
  {
    isPageLoaded = true;
    driver.get(DWRE_Utils.generateURL(MerchantTools.Promotions));
    Utils.waitForPageLoad(driver);
  }
  


  public void createPromotion()
    throws Exception
  {
    BrowserActions.clickOnElement(btnNew, driver, "New button");
    Utils.waitForPageLoad(driver);
  }
}
