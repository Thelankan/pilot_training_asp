package com.dwre.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.testng.Assert;


public class DWRE_TestDataExtractor
{
  private String workBookName;
  private String workSheet;
  private String testCaseId;
  private boolean doFilePathMapping;
  private HashMap<String, String> data;
  
  public DWRE_TestDataExtractor() {}
  
  public DWRE_TestDataExtractor(String xlWorkBook, String xlWorkSheet)
  {
    workBookName = xlWorkBook;
    workSheet = xlWorkSheet;
  }
  
  public DWRE_TestDataExtractor(String xlWorkBook, String xlWorkSheet, String tcID) {
    workBookName = xlWorkBook;
    workSheet = xlWorkSheet;
    testCaseId = tcID;
  }
  
  public String getWorkBookName() {
    return workBookName;
  }
  
  public void setWorkBookName(String workBookName) {
    this.workBookName = workBookName;
  }
  
  public void setFilePathMapping(boolean doFilePathMapping) {
    this.doFilePathMapping = doFilePathMapping;
  }
  
  public String getWorkSheet() {
    return workSheet;
  }
  
  public void setWorkSheet(String workSheet) {
    this.workSheet = workSheet;
  }
  
  public String getTestCaseId() {
    return testCaseId;
  }
  
  public void setTestCaseId(String testCaseId) {
    this.testCaseId = testCaseId;
  }
  
  public String get(String key)
  {
    if (data.isEmpty())
      readData();
    return (String)data.get(key);
  }
  

  private Hashtable<String, Integer> excelHeaders = new Hashtable();
  private Hashtable<String, Integer> excelrRowColumnCount = new Hashtable();
  





  public HashMap<String, String> readData()
  {
    HashMap<String, String> testData = new HashMap();
    ReadFromExcel readTestData = new ReadFromExcel();
    boolean isDataFound = false;
    testCaseId = (testCaseId != null ? testCaseId.trim() : "");
    HSSFSheet sheet = null;
    HSSFRow row = null;
    HSSFCell cell = null;
    
    try
    {
      sheet = readTestData.initiateExcelConnection(workSheet, workBookName, doFilePathMapping);
      excelrRowColumnCount = readTestData.findRowColumnCount(sheet, excelrRowColumnCount);
      excelHeaders = readTestData.readExcelHeaders(sheet, excelHeaders, excelrRowColumnCount);
      
      for (int r = 0; r < ((Integer)excelrRowColumnCount.get("RowCount")).intValue(); r++)
      {
        row = sheet.getRow(r);
        if (row != null)
        {

          for (int c = 0; c < ((Integer)excelrRowColumnCount.get("ColumnCount")).intValue(); c++)
          {
            if (row.getCell(((Integer)excelHeaders.get("ID")).intValue()) == null) {
              break;
            }
            cell = row.getCell(((Integer)excelHeaders.get("ID")).intValue());
            
            if (readTestData.convertHSSFCellToString(cell).toString().equalsIgnoreCase(testCaseId))
            {

              isDataFound = true;
              
              for (String key : excelHeaders.keySet()) {
                testData.put(key, readTestData.convertHSSFCellToString(row.getCell(((Integer)excelHeaders.get(key)).intValue())));
              }
              
              break;
            }
          }
          
          if (isDataFound) {
            break;
          }
        }
      }
      if (!isDataFound) {
        Assert.fail("\nTest Data not found in test data sheet for Test Case Id  : " + testCaseId);
      }
    } catch (RuntimeException e) {
      Assert.fail("Error During Execution; Execution Failed More details " + e);
      e.printStackTrace();
    }
    
    data = testData;
    return testData;
  }
  
  public List<HashMap<String, String>> readAllData()
  {
    List<HashMap<String, String>> dataList = new ArrayList();
    ReadFromExcel readTestData = new ReadFromExcel();
    HSSFSheet sheet = null;
    HSSFRow row = null;
    
    try
    {
      sheet = readTestData.initiateExcelConnection(workSheet, workBookName, doFilePathMapping);
      excelrRowColumnCount = readTestData.findRowColumnCount(sheet, excelrRowColumnCount);
      excelHeaders = readTestData.readExcelHeaders(sheet, excelHeaders, excelrRowColumnCount);
      
      for (int r = 1; r < ((Integer)excelrRowColumnCount.get("RowCount")).intValue(); r++)
      {
        row = sheet.getRow(r);
        if (row != null)
        {
          HashMap<String, String> testData = new HashMap();
          for (int c = 0; c < ((Integer)excelrRowColumnCount.get("ColumnCount")).intValue(); c++)
          {
            for (String key : excelHeaders.keySet()) {
              testData.put(key, readTestData.convertHSSFCellToString(row.getCell(((Integer)excelHeaders.get(key)).intValue())));
            }
          }
          

          dataList.add(testData);
        }
      }
    } catch (RuntimeException e) { Assert.fail("Error During Execution; Execution Failed More details " + e);
      e.printStackTrace();
    }
    
    return dataList;
  }
  






  public static HashMap<String, String> initTestData(String workbook, String sheetName, String... value)
  {
    DWRE_TestDataExtractor testData = new DWRE_TestDataExtractor();
    testData.setWorkBookName(workbook);
    testData.setWorkSheet(sheetName);
    testData.setFilePathMapping(true);
    
    String[] testCaseId = value;
    testData.setTestCaseId(testCaseId[0]);
    return testData.readData();
  }
}
