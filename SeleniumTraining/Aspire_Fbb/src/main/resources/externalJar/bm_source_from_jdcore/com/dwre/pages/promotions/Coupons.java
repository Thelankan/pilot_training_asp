package com.dwre.pages.promotions;

import com.dwre.pages.ElementLayer;
import com.dwre.pages.Navigation;
import com.dwre.support.BrowserActions;
import com.dwre.support.DWRE_TestDataExtractor;
import com.dwre.support.DWRE_Utils;
import com.dwre.support.EnvironmentPropertiesReader;
import com.dwre.support.Log;
import com.dwre.support.MerchantTools;
import com.dwre.support.Utils;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;



public class Coupons
  extends LoadableComponent<Coupons>
{
  private WebDriver driver;
  private boolean isPageLoaded;
  public ElementLayer elementLayer;
  String runPltfrm = Utils.getRunPlatForm();
  String workBook = EnvironmentPropertiesReader.getInstance("dataConfig").getProperty("workBookName");
  String workSheet = EnvironmentPropertiesReader.getInstance("dataConfig").getProperty("workSheet_Coupons");
  

  @FindBy(css=".main-content.dw-couponsgrid.coupons")
  WebElement readyElement;
  

  @FindBy(id="login")
  WebElement LoginPage;
  
  @FindBy(css="[dw-handler='newCoupon'] button.dw-button-primary span")
  WebElement btnNewCoupon;
  
  @FindBy(css="[id='new-coupon-window-window']")
  WebElement newCouponWindow;
  
  @FindBy(css="[dw-model='Coupon.data.coupon_id'] input[class*='form.coupon_id']")
  WebElement txtCouponId;
  
  @FindBy(css="#new-coupon-window-window [dw-model='Coupon.data.case_insensitive'] select")
  WebElement drpdownCaseSensitive;
  
  @FindBy(css="[dw-model='Coupon.data.type'] select")
  WebElement drpdownCouponType;
  
  @FindBy(css="#new-coupon-window-window [dw-model='Coupon.data.single_code'] input")
  WebElement txtCouponCode;
  
  @FindBy(css="#new-coupon-window-window [dw-label-text='saveText'] button>span")
  WebElement btnSaveCoupon;
  
  @FindBy(css="div.dw-coupondetail")
  WebElement couponDetails;
  
  @FindBy(css="[dw-model='Coupon.data.description'] textarea")
  WebElement txtCouponDescription;
  
  @FindBy(css="[dw-model='Coupon.data.enabled'] select")
  WebElement drpdownCouponEnabled;
  
  @FindBy(css="[dw-model='Coupon.data.perCode'] input")
  WebElement txtRedemptionPerCode;
  
  @FindBy(css="[dw-model='Coupon.data.perCustomer'] input")
  WebElement txtRedemptionPerCustomer;
  
  @FindBy(css="[dw-model='Coupon.data.numberOfDays'] input")
  WebElement txtRedemptionTimePeriod;
  
  @FindBy(css="[dw-model='Coupon.data.perTimePeriod'] input")
  WebElement txtRedemptionPerTimePeriod;
  
  @FindBy(css="[name='multiple_codes_per_basket'] select")
  WebElement drpRedemptionPerOrder;
  
  @FindBy(css=".bm-screen-header [dw-label-text='saveText'] button")
  WebElement btnSave;
  
  @FindBy(css=".main-toolbar [dw-search-filters='searchFilters'] input[placeholder*='Search by ID']")
  WebElement txtSearchId;
  
  @FindBy(css=".main-toolbar [dw-search-filters='searchFilters'] button")
  WebElement btnSearchId;
  
  @FindBy(css="[dw-tooltip='Coupon.tooltip.assigments.button.assign'] button span")
  WebElement btnAssign;
  
  @FindBy(css="#assign-coupon-window-window")
  WebElement assignWindow;
  
  @FindBy(css="#assign-coupon-window-window [dw-model='searchModel'] input.dw-text")
  WebElement txtAssignSearchField;
  
  @FindBy(css="#assign-coupon-window-window [dw-model='searchModel'] button")
  WebElement btnAssignSearch;
  
  @FindBy(css="#assign-coupon-window-window [dw-label-text='saveText'] button span")
  WebElement btnAssignSucess;
  

  public Coupons(WebDriver driver)
  {
    this.driver = driver;
    ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 
      Utils.maxElementWait);
    PageFactory.initElements(finder, this);
  }
  

  protected void isLoaded()
  {
    if (!isPageLoaded) {
      Assert.fail();
    }
    try
    {
      if (LoginPage.isDisplayed())
        new Navigation(driver).login(driver);
      driver.get(DWRE_Utils.generateURL(MerchantTools.Coupons));
    }
    catch (Exception localException) {}
    

    if ((isPageLoaded) && (!Utils.waitForElement(driver, readyElement, 30))) {
      Log.fail("Coupons Page didn't display", driver);
    }
    
    elementLayer = new ElementLayer(driver);
  }
  

  protected void load()
  {
    isPageLoaded = true;
    driver.get(DWRE_Utils.generateURL(MerchantTools.Coupons));
    Utils.waitForPageLoad(driver);
  }
  



  public String createCoupons(String CouponId)
    throws Exception
  {
    String couponId = null;
    String couponCode = null;
    HashMap<String, String> testData = DWRE_TestDataExtractor.initTestData(workBook, workSheet, new String[] { CouponId });
    LinkedHashMap<String, WebElement> instructions = new LinkedHashMap();
    
    BrowserActions.clickOnElement(btnNewCoupon, driver, "New Coupon button");
    Utils.waitForPageLoad(driver);
    
    couponId = (String)testData.get("CouponId");
    couponCode = (String)testData.get("CouponCode");
    

    if (Utils.waitForElement(driver, newCouponWindow))
    {
      instructions.put("type_CouponId_" + (String)testData.get("CouponId"), txtCouponId);
      instructions.put("select_CaseSensitive_" + (String)testData.get("CaseSensitive"), drpdownCaseSensitive);
      instructions.put("select_CouponType_" + (String)testData.get("CouponType"), drpdownCouponType);
      if (((String)testData.get("CouponType")).toString().equals("Single Code"))
      {
        instructions.put("type_CouponCode_" + (String)testData.get("CouponCode"), txtCouponCode);
      }
      instructions.put("click_Save button_", btnSaveCoupon);
      BrowserActions.fillFormDetails(instructions, driver);
      Utils.waitForPageLoad(driver);
    }
    String str1;
    switch ((str1 = ((String)testData.get("CouponType")).toLowerCase()).hashCode()) {case -1790409019:  if (str1.equals("single code")) break; break; case -502661610:  if (!str1.equals("multiple codes"))
      {
        break label929;
        if (Utils.waitForElement(driver, couponDetails))
        {
          instructions.clear();
          instructions.put("type_Coupon Description_" + (String)testData.get("Description"), txtCouponDescription);
          instructions.put("select_Coupon Enable_" + (String)testData.get("Enabled"), drpdownCouponEnabled);
          instructions.put("type_Redemption per Coupon code_" + (String)testData.get("RedemptionPerCode"), txtRedemptionPerCode);
          instructions.put("type_Redemption per Customer_" + (String)testData.get("RedemptionPerCustomer"), txtRedemptionPerCustomer);
          instructions.put("type_Redemption Time Period (No Of Days)_" + (String)testData.get("TimePeriod"), txtRedemptionTimePeriod);
          instructions.put("type_Redemption Per Time Period_" + (String)testData.get("RedemptionsPerTimePeriod"), txtRedemptionPerTimePeriod);
          instructions.put("click_Save Button", btnSave);
          BrowserActions.fillFormDetails(instructions, driver);
        }
        

      }
      else if (Utils.waitForElement(driver, couponDetails))
      {
        instructions.clear();
        instructions.put("type_Coupon Description_" + (String)testData.get("Description"), txtCouponDescription);
        instructions.put("select_Coupon Enable_" + (String)testData.get("Enabled"), drpdownCouponEnabled);
        instructions.put("type_Redemption per coupon code_" + (String)testData.get("RedemptionPerCode"), txtRedemptionPerCode);
        instructions.put("type_Redemption per Customer_" + (String)testData.get("RedemptionPerCustomer"), txtRedemptionPerCustomer);
        instructions.put("type_Redemption Time Period (No Of Days)_" + (String)testData.get("TimePeriod"), txtRedemptionTimePeriod);
        instructions.put("type_Redemption Per Time Period_" + (String)testData.get("RedemptionsPerTimePeriod"), txtRedemptionPerTimePeriod);
        instructions.put("select_Redemption Per Ordre_" + (String)testData.get("RedemptionPerOrder"), drpRedemptionPerOrder);
        instructions.put("click_Save Button", btnSave);
        BrowserActions.fillFormDetails(instructions, driver);
      }
      break;
    }
    
    
    label929:
    return couponId + "/" + couponCode;
  }
  




  public void enableCoupon(String CouponId)
  {
    LinkedHashMap<String, WebElement> instructions = new LinkedHashMap();
    try
    {
      Utils.waitForPageLoad(driver);
      BrowserActions.typeOnTextField(txtSearchId, CouponId, driver, "Search Id in Search Panel");
      BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
      Utils.waitForPageLoad(driver);
      WebElement couponEnableLink = driver.findElement(By.xpath("//div/a[contains(@class,'link-style dw-nc-display-block')][contains(text(),'" + CouponId + "')]/../../..//following-sibling::td//div/span[contains(@class,'enabled')][contains(text(),'No')]"));
      if (Utils.waitForElement(driver, couponEnableLink))
      {
        WebElement couponIdlink = driver.findElement(By.xpath("//div/a[contains(@class,'link-style dw-nc-display-block')][contains(text(),'" + CouponId + "')]"));
        BrowserActions.clickOnElement(couponIdlink, driver, "Action Drop down");
        Utils.waitForPageLoad(driver);
        instructions.put("select_Coupon Enable_Yes", drpdownCouponEnabled);
        instructions.put("click_Save Button", btnSave);
        BrowserActions.fillFormDetails(instructions, driver);
      }
    }
    catch (Exception e)
    {
      Log.message("Selected Coupon is already in Enabled Status");
    }
  }
  




  public void disableCoupon(String CouponId)
  {
    LinkedHashMap<String, WebElement> instructions = new LinkedHashMap();
    try {
      Utils.waitForPageLoad(driver);
      BrowserActions.typeOnTextField(txtSearchId, CouponId, driver, "Search Id in Search Panel");
      BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
      Utils.waitForPageLoad(driver);
      WebElement coupondisableLink = driver.findElement(By.xpath("//div/a[contains(@class,'link-style dw-nc-display-block')][contains(text(),'" + CouponId + "')]/../../..//following-sibling::td//div/span[contains(@class,'enabled')][contains(text(),'Yes')]"));
      if (Utils.waitForElement(driver, coupondisableLink))
      {
        WebElement couponIdlink = driver.findElement(By.xpath("//div/a[contains(@class,'link-style dw-nc-display-block')][contains(text(),'" + CouponId + "')]"));
        BrowserActions.clickOnElement(couponIdlink, driver, "Coupon Id link");
        Utils.waitForPageLoad(driver);
        instructions.put("select_Coupon Enable_No", drpdownCouponEnabled);
        instructions.put("click_Save Button", btnSave);
        BrowserActions.fillFormDetails(instructions, driver);
      }
    }
    catch (Exception e)
    {
      Log.message("Selected Coupon is already in Disabled Status");
    }
  }
  







  public String assignCampaign(String CouponId, String CampaignId)
    throws Exception
  {
    String campId = null;
    Utils.waitForPageLoad(driver);
    BrowserActions.typeOnTextField(txtSearchId, CouponId, driver, "Search Id in Search Panel");
    BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
    Utils.waitForPageLoad(driver);
    
    WebElement couponLink = driver.findElement(By.xpath("//div/a[contains(@class,'link-style dw-nc-display-block')][contains(text(),'" + CouponId + "')]"));
    BrowserActions.clickOnElement(couponLink, driver, "Coupon link");
    Utils.waitForPageLoad(driver);
    
    NavigatetoTab("Assignment");
    BrowserActions.clickOnElement(btnAssign, driver, "Assign Button");
    
    if (Utils.waitForElement(driver, assignWindow))
    {
      BrowserActions.typeOnTextField(txtAssignSearchField, CampaignId, driver, "Assign Campaign Id Search");
      BrowserActions.clickOnElement(btnAssignSearch, driver, "Assign Search button");
      Utils.waitForPageLoad(driver);
      WebElement campaignIdCheckbox = driver.findElement(By.xpath("//td[@class='dw-grid-column']//div[contains(text(),'" + CampaignId + "')]/../..//preceding-sibling::td//input"));
      BrowserActions.clickOnElement(campaignIdCheckbox, driver, "Campaign Id Checkbox to Select");
      BrowserActions.clickOnElement(btnAssignSucess, driver, "Assign button after selected Campaing Id");
      Utils.waitForPageLoad(driver);
      campId = CampaignId;
    }
    return campId;
  }
  




  private void NavigatetoTab(String tabName)
    throws Exception
  {
    WebElement tablink = driver.findElement(By.xpath("//div[@class='dw-tab']//span[@class='dw-label'][contains(text(),'" + tabName + "')]"));
    BrowserActions.clickOnElement(tablink, driver, tabName + " Tab");
    Utils.waitForPageLoad(driver);
  }
  




  public void deleteCoupon(String CouponId)
  {
    try
    {
      Utils.waitForPageLoad(driver);
      BrowserActions.typeOnTextField(txtSearchId, CouponId, driver, "Search Id in Search Panel");
      BrowserActions.clickOnElement(btnSearchId, driver, "Button Search Id");
      Utils.waitForPageLoad(driver);
      WebElement actiondrop = driver.findElement(By.xpath("//div/a[contains(@class,'link-style dw-nc-display-block')][contains(text(),'" + CouponId + "')]/../../..//following-sibling::td[contains(@class,'dw-grid-actions')]//div/../*/button"));
      if (Utils.waitForElement(driver, actiondrop))
      {
        BrowserActions.clickOnElement(actiondrop, driver, "Action Drop down");
        Utils.waitForPageLoad(driver);
        List<WebElement> drpmenus = driver.findElements(By.cssSelector(".dw-grid-actions [class='dw-menu'] .dw-menu-item"));
        for (WebElement e : drpmenus)
        {
          if (e.getText().toString().equals("Delete"))
          {
            e.click();
            BrowserActions.nap(3L);
            break;
          }
        }
      }
    }
    catch (Exception e)
    {
      Log.message("Selected Promotion is already in Enabled Status");
    }
  }
}
