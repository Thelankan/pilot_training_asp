package com.fbb.support;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.*;

public class SheetsQuickstart {
    private static final String APPLICATION_NAME = "GSheetAPITest";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String CREDENTIALS_FOLDER = "credentials"; // Directory to store user credentials.

    private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();

    private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.DRIVE);
    private static final String CLIENT_SECRET_DIR = "../../../client_secret.json";

    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = SheetsQuickstart.class.getResourceAsStream(CLIENT_SECRET_DIR);
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(CREDENTIALS_FOLDER)))
                .setAccessType("offline")
                .build();
        return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
    }

    synchronized public static void updateTCStatus(String tcID, String status) {
        try {
            Log.event("[GAPI]Initiating result upload to Google Sheets...");
            // Build a new authorized API client service.
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            final String spreadsheetId = configProperty.get("g-sheetID");
            String today = DateTimeUtility.getCurrentDateAs("dd-MM-yyyy");
            final String range = "report_"+today+"!B1:B";
            String range1 = "report_"+today+"!";
            String unavailablerange = "report_"+today+"!";
            switch (Utils.getCurrentBrand()) {
                case rm:
                    if (Utils.isDesktop()) {
                        range1 += "C";
                    } else if (Utils.isMobile()) {
                        range1 += "D";
                    } else {
                        range1 += "E";
                    }
                    break;
                case ww:
                    if (Utils.isDesktop()) {
                        range1 += "F";
                    } else if (Utils.isMobile()) {
                        range1 += "G";
                    } else {
                        range1 += "H";
                    }
                    break;
                case ks:
                    if (Utils.isDesktop()) {
                        range1 += "I";
                    } else if (Utils.isMobile()) {
                        range1 += "J";
                    } else {
                        range1 += "K";
                    }
                    break;
                default:
                    Log.event("[GAPI]I can't find appropriate brand.");
                    break;
            }
            Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
            ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
            List<List<Object>> values = response.getValues();
            if (values == null || values.isEmpty()) {
                System.out.println("[GAPI]No data found.");
            } else {
                boolean flag = false;
                for (int i = 0; i < values.size(); i++) {
                    List<Object> row = values.get(i);
                    if (row.size() > 0 && row.get(0).equals(tcID)) {
                        range1 += Integer.toString(i + 1);
                        System.out.println("[GAPI]Row ID :: " + range1);
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    Log.event("[GAPI]Current method name == " + tcID + " == not available in current sheet.");
                    String rowNumber = Integer.toString(values.size() + 1);
                    unavailablerange += "A" + rowNumber;
                    String tcId = "TC" + new Throwable().getStackTrace()[2].getClassName().split("TC")[1];
                    List<List<Object>> values1 = Arrays.asList(Arrays.asList(tcId));
                    ValueRange body = new ValueRange().setValues(values1);
                    UpdateValuesResponse result = service.spreadsheets().values().update(spreadsheetId, unavailablerange, body).setValueInputOption("RAW").execute();
                    System.out.printf("[GAPI]%d cells appended.", result.getUpdatedCells());

                    unavailablerange = "";
                    unavailablerange += "B" + rowNumber;
                    tcId = "TC" + new Throwable().getStackTrace()[2].getClassName().split("TC")[1];
                    //values1.clear();
                    values1 = Arrays.asList(Arrays.asList(tcID));
                    body = new ValueRange().setValues(values1);
                    result = service.spreadsheets().values().update(spreadsheetId, unavailablerange, body).setValueInputOption("RAW").execute();
                    Log.event("[GAPI]New Row created. %d cells appended.", result.getUpdatedCells());

                    range1 += rowNumber;
                }
            }

            //Writing value
            List<List<Object>> values1 = Arrays.asList(Arrays.asList(status));
            ValueRange body = new ValueRange().setValues(values1);
            UpdateValuesResponse result = service.spreadsheets().values().update(spreadsheetId, range1, body).setValueInputOption("RAW").execute();
            Log.event("[GAPI]Result Updated. %d cells appended.", (int) result.getUpdatedCells());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean validateReportDate()throws Exception{
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        final String spreadsheetId = configProperty.get("g-sheetID");
        final String range = "SheetsList!A1:A";
        String range1 = "SheetsList!B";
        Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
        ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
        List<List<Object>> values = response.getValues();

        if (values == null || values.isEmpty()) {
            Log.event("[GAPI]No data found.");
            return false;
        } else {
            boolean flag = false;

            if(values.get(values.size()-1).get(0).toString().equals("current")){
                range1 += Integer.toString(values.size());
                flag=true;
            }else{
                for (int i = 0; i < values.size(); i++) {
                    List<Object> row = values.get(i);
                    if (row.size() > 0 && row.get(0).equals("current")) {
                        range1 += Integer.toString(i + 1);
                        Log.event("[GAPI]Row ID :: " + range1);
                        flag = true;
                        break;
                    }
                }
            }

            if(flag){
                response = service.spreadsheets().values().get(spreadsheetId, range1).execute();
                values = response.getValues();
                String lastDated = values.get(0).get(0).toString().split("\\_")[1];
                String today = DateTimeUtility.getCurrentDateAs("dd-MM-yyyy");
                return (today.equals(lastDated)) ? true : false;
            }else{
                return false;
            }
        }
    }

    public static void duplicateSheetFromTemplate()throws Exception{
        System.out.println("[GAPI]Creating New Sheet for today report...");

        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        final String spreadsheetId = configProperty.get("g-sheetID");
        Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();

        String today = DateTimeUtility.getCurrentDateAs("dd-MM-yyyy");
        BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();

        List<Request> requests = new ArrayList<>();

        DuplicateSheetRequest requestBody = new DuplicateSheetRequest();
        String reportName = "report_" + today;
        requestBody.setNewSheetName(reportName);
        requestBody.setSourceSheetId(Integer.parseInt(configProperty.get("g-sheetTemplate")));

        requests.add(new Request().setDuplicateSheet(requestBody));


        batchUpdateSpreadsheetRequest.setRequests(requests);
        Sheets.Spreadsheets.BatchUpdate request =
                service.spreadsheets().batchUpdate(spreadsheetId, batchUpdateSpreadsheetRequest);

        BatchUpdateSpreadsheetResponse updateResponse = request.execute();
        System.out.println("New Sheet Properties :: " + updateResponse);

        final String range = "SheetsList!A1:B";
        String range1 = "SheetsList!A";
        String range2 = "SheetsList!";

        int rowID = 0;

        ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
        List<List<Object>> values = response.getValues();

        for (int i = 0; i < values.size(); i++) {
            List<Object> row = values.get(i);
            if (row.size() > 0 && row.get(0).equals("current")) {
                range1 += Integer.toString(i + 1);
                rowID = i;
                range2 += "A" + Integer.toString(i + 2) + ":B" + Integer.toString(i + 2);
                break;
            }
        }

        String val = values.get(rowID).get(1).toString().split("_")[1];
        List<List<Object>> values1 = Arrays.asList(Arrays.asList(val));
        ValueRange body = new ValueRange().setValues(values1);
        UpdateValuesResponse result = service.spreadsheets().values().update(spreadsheetId, range1, body).setValueInputOption("RAW").execute();
        Log.event("[GAPI]Result Updated. %d cells appended.", (int) result.getUpdatedCells());

        values1 = Arrays.asList(Arrays.asList("current",reportName));
        body = new ValueRange().setValues(values1);
        result = service.spreadsheets().values().update(spreadsheetId, range2, body).setValueInputOption("RAW").execute();
        Log.event("[GAPI]Result Updated. %d cells appended.", (int) result.getUpdatedCells());
    }
    
    public static void LogException(String tc_name, Exception e)throws Exception{
    	try {
    		if(System.getProperty("updateGSheet").equals("true")) {
    			SheetsQuickstart.updateTCStatus(tc_name, e.getClass().getSimpleName());
    		}
    	}catch(Exception e1) {
    		e.printStackTrace();
    	}
    }
}