package com.fbb.support;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.WordUtils;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

/**
 * Util class consists wait for page load,page load with user defined max time
 * and is used globally in all classes and methods
 * 
 */
public class Utils {
	private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
	public static int maxElementWait = Integer.parseInt(configProperty.getProperty("maxElementWait"));

	/**
	 * waitForPageLoad waits for the page load with default page load wait time
	 * 
	 * @param driver
	 *            : Webdriver
	 */
	public static void waitForPageLoad(final WebDriver driver) {
		waitForPageLoad(driver, WebDriverFactory.maxPageLoadWait);
	}

	/**
	 * waitForPageLoad waits for the page load with custom page load wait time
	 * 
	 * @param driver
	 *            : Webdriver
	 * @param maxWait
	 *            : Max wait duration
	 */
	public static void waitForPageLoad(final WebDriver driver, int maxWait) {
		long startTime = StopWatch.startTime();
		FluentWait<WebDriver> wait = new WebDriverWait(driver, maxWait).pollingEvery(500, TimeUnit.MILLISECONDS)
				.ignoring(StaleElementReferenceException.class).withMessage("Page Load Timed Out");
		try {

			if (configProperty.getProperty("documentLoad").equalsIgnoreCase("true"))
				wait.until(WebDriverFactory.documentLoad);

			if (configProperty.getProperty("imageLoad").equalsIgnoreCase("true"))
				wait.until(WebDriverFactory.imagesLoad);

			if (configProperty.getProperty("framesLoad").equalsIgnoreCase("true"))
				wait.until(WebDriverFactory.framesLoad);

			String title = driver.getTitle().toLowerCase();
			String url = driver.getCurrentUrl().toLowerCase();
			Log.event("Page URL:: " + url);

			if ("the page cannot be found".equalsIgnoreCase(title) || title.contains("is not available")
					|| url.contains("/error/") || url.toLowerCase().contains("/errorpage/")) {
				Assert.fail("Site is down. [Title: " + title + ", URL:" + url + "]");
			}

		} catch (TimeoutException e) {
			/*driver.navigate().refresh();
			wait.until(WebDriverFactory.documentLoad);
			wait.until(WebDriverFactory.imagesLoad);
			wait.until(WebDriverFactory.framesLoad);*/
		}
		Log.event("Page Load Completed.", StopWatch.elapsedTime(startTime));

	} // waitForPageLoad

	/**
	 * To get the test orientation
	 * 
	 * <p>
	 * if test run on sauce lab device return landscape or portrait or valid
	 * message, otherwise check local device execution and return landscape or
	 * portrait or valid message
	 * 
	 * @return dataToBeReturned - portrait or landscape or valid message
	 */
	public static String getTestOrientation() {
		String dataToBeReturned = null;
		boolean checkExecutionOnSauce = false;
		boolean checkDeviceExecution = false;
		checkExecutionOnSauce = (System.getProperty("SELENIUM_DRIVER") != null
				|| System.getenv("SELENIUM_DRIVER") != null) ? true : false;

		if (checkExecutionOnSauce) {
			checkDeviceExecution = ((System.getProperty("runUserAgentDeviceTest") != null)
					&& (System.getProperty("runUserAgentDeviceTest").equalsIgnoreCase("true"))) ? true : false;
			if (checkDeviceExecution) {
				dataToBeReturned = (System.getProperty("deviceOrientation") != null)
						? System.getProperty("deviceOrientation")
								: "no sauce run system variable: deviceOrientation ";
			} else {
				dataToBeReturned = "sauce browser test: no orientation";
			}
		} else {
			checkDeviceExecution = (configProperty.hasProperty("runUserAgentDeviceTest")
					&& (configProperty.getProperty("runUserAgentDeviceTest").equalsIgnoreCase("true"))) ? true : false;
			if (checkDeviceExecution) {
				dataToBeReturned = configProperty.hasProperty("deviceOrientation")
						? configProperty.getProperty("deviceOrientation")
								: "no local run config variable: deviceOrientation ";
			} else {
				dataToBeReturned = "local browser test: no orientation";
			}
		}
		return dataToBeReturned;
	}

	public static boolean compareTwoArraysAreEqualBySorting(String[] Array1, String[] Array2) throws Exception {
		Arrays.sort(Array1);
		Arrays.sort(Array2);
		for (int intProd = 0; intProd < Array1.length; intProd++) {
			if (!(Array1[intProd].equals(Array2[intProd]))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * To wait for the specific element on the page
	 * 
	 * @param driver
	 *            : Webdriver
	 * @param element
	 *            : Webelement to wait for
	 * @return boolean - return true if element is present else return false
	 */
	public static boolean waitForElement(WebDriver driver, WebElement element) {
		return waitForElement(driver, element, maxElementWait);
	}

	/**
	 * To wait for the specific element on the page
	 * 
	 * @param driver
	 *            : Webdriver
	 * @param element
	 *            : Webelement to wait for
	 * @param maxWait
	 *            : Max wait duration
	 * @return boolean - return true if element is present else return false
	 */
	public static boolean waitForElement(WebDriver driver, WebElement element, int maxWait) {
		boolean statusOfElementToBeReturned = false;
		WebDriverWait wait = new WebDriverWait(driver, maxWait);
		long startTime = StopWatch.startTime();
		try {
			WebElement waitElement = wait.until(ExpectedConditions.visibilityOf(element));
			if (waitElement.isDisplayed() && waitElement.isEnabled()) {
				statusOfElementToBeReturned = true;
				Log.event("Element is displayed:: " + element.toString());
			}
		} catch (Exception e) {
			statusOfElementToBeReturned = false;
			Log.event("Unable to find a element after " + StopWatch.elapsedTime(startTime) + " sec ==> " + element.toString());
		}
		return statusOfElementToBeReturned;
	}

	public static boolean waitForElementFromDOM(WebDriver driver, WebElement element) {
		return waitForElementFromDOM(driver, element, maxElementWait);
	}
	
	public static boolean waitForElementFromDOM(WebDriver driver, WebElement element, int maxWait) {
		boolean statusOfElementToBeReturned = false;
		WebDriverWait wait = new WebDriverWait(driver, maxWait);
		long startTime = StopWatch.startTime();
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
			statusOfElementToBeReturned = true;
			Log.event("Element Available:: " + element.toString());
		} catch (Exception e) {
			statusOfElementToBeReturned = false;
			Log.event("Unable to find a element after " + StopWatch.elapsedTime(startTime) + " sec ==> " + element.toString());
		}
		return statusOfElementToBeReturned;
	}

	/**
	 * To switch between windows
	 * @param driver -
	 * @param windowToSwitch -
	 * @param opt - title/url
	 * @param closeCurrentDriver -
	 * @return WebDriver of current window
	 * @throws Exception - Exception
	 */
	public static WebDriver switchWindows(WebDriver driver, String windowToSwitch, String opt,
			String closeCurrentDriver) throws Exception {

		WebDriver currentWebDriver = driver;
		WebDriver assingedWebDriver = driver;
		boolean windowFound = false;
		ArrayList<String> multipleWindows = new ArrayList<String>(assingedWebDriver.getWindowHandles());

		for (int i = 0; i < multipleWindows.size(); i++) {

			assingedWebDriver.switchTo().window(multipleWindows.get(i));


			if (opt.equals("title")) {
				if (assingedWebDriver.getTitle().trim().equals(windowToSwitch)) {
					windowFound = true;
					break;
				}
			} else if (opt.equals("url")) {
				if (assingedWebDriver.getCurrentUrl().contains(windowToSwitch)) {
					windowFound = true;
					break;
				}
			} // if

		} // for

		if (!windowFound)
			throw new Exception("Window: " + windowToSwitch + ", not found!!");
		else {
			if (closeCurrentDriver.equals("true"))
				currentWebDriver.close();
		}

		return assingedWebDriver;

	}// switchWindows

	/**
	 * Switching between tabs or windows in a browser
	 * 
	 * @param driver
	 *            -
	 */
	public static void switchToNewWindow(WebDriver driver) {
		String winHandle = driver.getWindowHandle();
		for (String index : driver.getWindowHandles()) {
			if (!index.equals(winHandle)) {
				driver.switchTo().window(index);
				break;
			}
		}
		if (!((RemoteWebDriver) driver).getCapabilities().getBrowserName().matches(".*safari.*")) {
			((JavascriptExecutor) driver).executeScript(
					"if(window.screen){window.moveTo(0, 0); window.resizeTo(window.screen.availWidth, window.screen.availHeight);};");
		}
	}
	
	public static void openNewTab(WebDriver driver) {
		((JavascriptExecutor) driver).executeScript("window.open('about:blank', '_blank');");
	}
	

	/**
	 * To compare two HashMap values,then print unique list value and print
	 * missed list value
	 * 
	 * @param expectedList
	 *            - expected element list
	 * @param actualList
	 *            - actual element list
	 * @return statusToBeReturned - returns true if both the lists are equal,
	 *         else returns false
	 */
	public static boolean compareTwoHashMap(Map<String, String> expectedList, Map<String, String> actualList) {
		List<String> missedkey = new ArrayList<String>();
		HashMap<String, String> missedvalue = new HashMap<String, String>();
		try {
			for (String k : expectedList.keySet()) {
				if (!(actualList.get(k).equalsIgnoreCase(expectedList.get(k)))) {
					missedvalue.put(k, actualList.get(k));
					Log.failsoft("Missed Values:: Expected List:: " + missedvalue);
					return false;
				}
			}
			for (String y : actualList.keySet()) {
				if (!expectedList.containsKey(y)) {
					missedkey.add(y);
					Log.failsoft("Missed keys:: Actual List:: " + missedkey);
					return false;
				}
			}
		} catch (NullPointerException np) {
			return false;
		}
		return true;
	}

	/**
	 * To compare two HashMap values,then print unique list value and print
	 * missed list value
	 * 
	 * @param expectedList
	 *            - expected element list
	 * @param actualList
	 *            - actual element list
	 * @return statusToBeReturned - returns true if both the lists are equal,
	 *         else returns false
	 */
	public static boolean compareTwoHashMapNotEqual(Map<String, String> expectedList, Map<String, String> actualList) {
		List<String> expectedKey = new ArrayList<>(expectedList.keySet());
		List<String> actualKey = new ArrayList<>(actualList.keySet());
		boolean state = false;
		try {
			if(!expectedKey.equals(actualKey))
				return true;

			for(String key : expectedList.keySet()){
				if(!expectedList.get(key).equals(actualList.get(key)))
					return true;
			}

		} catch (NullPointerException np) {
			state = false;
		}
		return state;
	}

	/**
	 * To compare two hashmaps
	 * @param expectedList -
	 * @param actualList -
	 * @param onlyThis -
	 * @return true - if both hashmaps are equal
	 */
	@SafeVarargs
	public static boolean compareTwoHashMap(Map<String, String> expectedList, Map<String, String> actualList,
			List<String>... onlyThis) {
		List<String> missedkey = new ArrayList<String>();
		HashMap<String, String> missedvalue = new HashMap<String, String>();
		try {
			for (String k : expectedList.keySet()) {
				if (onlyThis[0].contains(k)) {
					if (!(actualList.get(k).equalsIgnoreCase(expectedList.get(k)))) {
						missedvalue.put(k, actualList.get(k));
						Log.failsoft("Missed Values:: Expected List:: " + missedvalue);
						return false;
					}
				}
			}
			for (String y : actualList.keySet()) {
				if (onlyThis[0].contains(y)) {
					if (!expectedList.containsKey(y)) {
						missedkey.add(y);
						Log.failsoft("Missed keys:: Actual List:: " + missedkey);
						return false;
					}
				}
			}
		} catch (NullPointerException np) {
			return false;
		}
		return true;
	}


	/**
	 * To compare two array list values,then print unique list value and print
	 * @param List<String> - List of element to verify
	 * @param List<String> - List of actual elements
	 * @param disableMissedLog - if missing elements are logged
	 * @return boolean - true if list completely conatains list
	 */
	public static boolean compareTwoList(List<String> expectedElements, List<String> actualElements, boolean... disableMissedLog) {
		boolean statusToBeReturned = false;
		List<String> uniqueList = new ArrayList<String>();
		List<String> missedList = new ArrayList<String>();
		for (String item : expectedElements) {
			if (actualElements.contains(item)) {
				uniqueList.add(item);
			} else {
				missedList.add(item);
			}
		}
		
		Collections.sort(expectedElements); Collections.sort(actualElements);
		
		if (expectedElements.equals(actualElements)) {
			Log.event("All elements checked on this page:: " + uniqueList);
			statusToBeReturned = true;
		} else {
			if(disableMissedLog.length == 0 || (disableMissedLog.length > 0 ? disableMissedLog[0] == false : false))
				Log.event("Missing element on this page:: " + missedList);
			statusToBeReturned = false;
		}
		return statusToBeReturned;
	}

	/**
	 * To compare two list of strings
	 * @param List<String> - List of element to verify
	 * @param List<String> - List of actual elements
	 * @return boolean - if elements are missing
	 */
	public static boolean compareTwoList1(List<String> expectedElements, List<String> actualElements) {
		boolean statusToBeReturned = true;
		List<String> missedList = new ArrayList<String>();
		for (int i = 0; i < expectedElements.size(); i++) {
			if (!(expectedElements.get(i).equals(actualElements.get(i)))) {
				statusToBeReturned = false;
				missedList.add(expectedElements.get(i));
			}
		}
		if(statusToBeReturned == false)
			Log.failsoft("Missing element on this page:: " + missedList);
		return statusToBeReturned;
	}

	/**
	 * To verify actual list contains expected list values
	 * @param List<String> - List of actual elements
	 * @param List<String> - List of element to verify
	 * @return boolean - true/false
	 */
	public static boolean ListsContains(List<String> actualElements, List<String> expectedElements) {
		boolean status = false;
		boolean flag=false;
		List<String> missedList = new ArrayList<String>();

		for(int i = 0 ; i < expectedElements.size(); i++){
			if(actualElements.contains((expectedElements.get(i)))){
				status = true;
			} 
			else
			{
				flag = true;
				missedList.add(expectedElements.get(i));
			}
		}
		if(flag) {status=true;}
		else
		{
			Log.message("Missing options in the dropdown:: " + missedList);
		}
		return status;

	}


	/**
	 * To sort LinkedList of Product
	 * @param actualList -
	 * @return Sorted list of linked list
	 * @throws Exception - Exception
	 */
	public static LinkedList<LinkedHashMap<String, String>> sortLinkedListProduct(
			LinkedList<LinkedHashMap<String, String>> actualList) throws Exception {
		LinkedList<LinkedHashMap<String, String>> listToReturn = new LinkedList<LinkedHashMap<String, String>>();

		actualList = makeUnique(actualList);

		LinkedList<String> list = new LinkedList<String>();
		int size = actualList.size();

		for (int x = 0; x < size; x++)
			list.add(actualList.get(x).get("ProductName"));

		Collections.sort(list, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Collator.getInstance().compare(o1, o2);
			}
		});

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (list.get(i).equals(actualList.get(j).get("ProductName"))) {
					listToReturn.add(actualList.get(j));
				}
			}
		}

		// printing sorted list - For debugging
		/*
		 * System.out.println("-----------------------------------------");
		 * for(int y = 0 ;y < size; y++) System.out.println(list.get(y));
		 * System.out.println("-----------------------------------------");
		 */
		return listToReturn;
	}

	/**
	 * To sort LinkedList of address
	 * 
	 * @param actualList -
	 * @return LinkedList -
	 * @throws Exception - Exception
	 */
	public static LinkedList<LinkedHashMap<String, String>> sortLinkedListAddress(
			LinkedList<LinkedHashMap<String, String>> actualList) throws Exception {
		LinkedList<LinkedHashMap<String, String>> listToReturn = new LinkedList<LinkedHashMap<String, String>>();
		actualList = makeUniqueAddress(actualList);
		LinkedList<String> list = new LinkedList<String>();
		int size = actualList.size();

		for (int x = 0; x < size; x++)
			list.add(actualList.get(x).get("FirstName"));

		Collections.sort(list, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Collator.getInstance().compare(o1, o2);
			}
		});

		for (int i = 0; i < size; i++) {
			if (list.get(i).equals(actualList.get(i).get("FirstName"))) {
				listToReturn.add(actualList.get(i));
			}
		}

		// printing sorted list
		/*
		 * System.out.println("-----------------------------------------");
		 * for(int y = 0 ;y < size; y++) System.out.println(list.get(y));
		 * System.out.println("-----------------------------------------");
		 */
		return listToReturn;
	}

	/**
	 * To make the linked list of linkedHash map unique
	 * @param hashMap -
	 * @return -
	 * @throws Exception -
	 */
	public static LinkedList<LinkedHashMap<String, String>> makeUnique(
			LinkedList<LinkedHashMap<String, String>> hashMap) throws Exception {
		int nosProduct = hashMap.size();
		for (int i = 0; i < nosProduct; i++) {
			for (int j = i + 1; j < nosProduct; j++) {
				if (hashMap.get(i).get("ProductName").equals(hashMap.get(j).get("ProductName")))
					if (hashMap.get(i).get("Color").equals(hashMap.get(j).get("Color")))
						if (hashMap.get(i).get("Size").equals(hashMap.get(j).get("Size"))) {
							int qty = Integer.parseInt(hashMap.get(i).get("Quantity"))
									+ Integer.parseInt(hashMap.get(j).get("Quantity"));
							hashMap.get(i).put("Quantity", Integer.toString(qty));
							hashMap.remove(j);
							nosProduct = hashMap.size();
							j--;
						}
			}
		}

		return hashMap;
	}

	/**
	 * To remove repeated address from list and make list unique
	 * @param hashMap - address list of hashMap
	 * @return LinkedList of Linked hashmap of address
	 * @throws Exception - Exception
	 */
	public static LinkedList<LinkedHashMap<String, String>> makeUniqueAddress(
			LinkedList<LinkedHashMap<String, String>> hashMap) throws Exception {
		int nosProduct = hashMap.size();
		for (int i = 0; i < nosProduct; i++) {
			for (int j = i + 1; j < nosProduct; j++) {
				if (hashMap.get(i).get("FirstName").equals(hashMap.get(j).get("FirstName")))
					if (hashMap.get(i).get("Address").equals(hashMap.get(j).get("Address"))) {
						hashMap.remove(j);
						j--;
					}
			}
		}

		return hashMap;
	}

	/**
	 * Verify the css property for an element
	 * 
	 * @param element
	 *            - WebElement for which to verify the css property
	 * @param cssProperty
	 *            - the css property name to verify
	 * @param actualValue
	 *            - the actual css value of the element
	 * @return boolean
	 */
	public static boolean verifyCssPropertyForElement(WebElement element, String cssProperty, String actualValue) {
		boolean result = false;

		String actualClassProperty = element.getCssValue(cssProperty);
		Log.event("Value from DOM :: " + actualClassProperty);
		Log.event("Value from USer :: " + actualValue);
		if (actualClassProperty.contains(actualValue)) {
			result = true;
		}
		return result;
	}

	/**
	 * To get the value of an input field.
	 * 
	 * @param element
	 *            - the input field you need the value/text of
	 * @param driver
	 *            -
	 * @return text of the input's value
	 */
	public static String getValueOfInputField(WebElement element, WebDriver driver) {
		String sDataToBeReturned = null;
		if (Utils.waitForElement(driver, element)) {
			sDataToBeReturned = element.getAttribute("value");
		}
		return sDataToBeReturned;
	}

	/**
	 * To wait for the specific element which is in disabled state on the page
	 * 
	 * @param driver
	 *            - current driver object
	 * @param element
	 *            - disabled webelement
	 * @param maxWait
	 *            - duration of wait in seconds
	 * @return boolean - return true if disabled element is present else return
	 *         false
	 */
	public static boolean waitForDisabledElement(WebDriver driver, WebElement element, int maxWait) {
		boolean statusOfElementToBeReturned = false;
		long startTime = StopWatch.startTime();
		WebDriverWait wait = new WebDriverWait(driver, maxWait);
		try {
			WebElement waitElement = wait.until(ExpectedConditions.visibilityOf(element));
			if ((!waitElement.isEnabled()) || waitElement.getAttribute("disabled").equals("true")) {
				statusOfElementToBeReturned = true;
				Log.event("Element is displayed and disabled:: " + element.toString());
			}
		} catch (Exception ex) {
			statusOfElementToBeReturned = false;
			Log.event("Unable to find disabled element after " + StopWatch.elapsedTime(startTime) + " sec ==> "
					+ element.toString());
		}
		return statusOfElementToBeReturned;
	}

	/**
	 * Wait until element disappears in the page
	 * 
	 * @param driver
	 *            - driver instance
	 * @param element
	 *            - webelement to wait to have disaapear
	 * @return true if element is not appearing in the page
	 */
	public static boolean waitUntilElementDisappear(WebDriver driver, final WebElement element) {
		final boolean isNotDisplayed;

		WebDriverWait wait = new WebDriverWait(driver, 60);
		isNotDisplayed = wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				boolean isPresent = false;
				try {
					if (Utils.waitForElement(driver, element)) {
						isPresent = false;
						Log.event("Element " + element.toString() + ", is still visible in page");
					}else{
						isPresent = true;
						Log.event("Element " + element.toString() + ", is not displayed in page ");
					}
				} catch (Exception ex) {
					isPresent = true;
					Log.event("Element " + element.toString() + ", is not displayed in page ");
					return isPresent;
				}
				return isPresent;
			}
		});
		return isNotDisplayed;
	}

	/**
	 * Wait until element disappears in the page
	 * @param driver -
	 * @param element -
	 * @param maxWait -
	 * @return boolean -
	 */
	public static boolean waitUntilElementDisappear(WebDriver driver, final WebElement element, int maxWait) {
		final boolean isNotDisplayed;

		WebDriverWait wait = new WebDriverWait(driver, maxWait);
		isNotDisplayed = wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				boolean isPresent = false;
				try {
					if (element.isDisplayed()) {
						isPresent = false;
						Log.event("Element " + element.toString() + ", is still visible in page");
					}
				} catch (Exception ex) {
					isPresent = true;
					Log.event("Element " + element.toString() + ", is not displayed in page ");
					return isPresent;
				}
				return isPresent;
			}
		});
		return isNotDisplayed;
	}

	/**
	 * To get screen resolution from the config.Property files
	 * 
	 * @return String - return screen resolution value
	 */
	public static String getScreenResolution() {
		String dataToBeReturned = null;

		if (configProperty.hasProperty("screenResolution")) {
			dataToBeReturned = configProperty.getProperty("screenResolution");
		}

		return dataToBeReturned;
	}// getScreenResolution

	/**
	 * To get run platform from the config.Property files
	 * 
	 * @return String - return mobile/desktop value
	 */
	public static String getRunPlatForm() {
		String dataToBeReturned = null;

		if (System.getProperty("deviceType") == null) {
			if (System.getProperty("runUserAgentDeviceTest") == null) {
				if (configProperty.getProperty("runUserAgentDeviceTest").equals("true")) {
					if (configProperty.getProperty("deviceName") != null)
						System.setProperty("deviceType",
								configProperty.getProperty("deviceName").contains("mobile") ? "mobile" : "tablet");
				} else if (configProperty.getProperty("realDeviceTest").equals("true")) {
					System.setProperty("deviceType",
							configProperty.getProperty("deviceType").contains("mobile") ? "mobile" : "tablet");
				} else if (configProperty.getProperty("runMobile").equals("true")) {
					System.setProperty("deviceType", "mobile");
				} else if (configProperty.getProperty("runTablet").equals("true")) {
					System.setProperty("deviceType", "tablet");
				} else {
					System.setProperty("deviceType", "desktop");
				}
			} else if (System.getProperty("deviceName").contains("mobile")) {
				System.setProperty("deviceType", "mobile");
			} else {
				System.setProperty("deviceType", "desktop");
			}
		}
			dataToBeReturned = System.getProperty("deviceType");
		// System.out.println("Running platform type:: " + dataToBeReturned);
		return dataToBeReturned;
	}// getRunPlatForm

	/**
	 * To get current device Platform
	 * @param driver -
	 * @return String -
	 */
	public static String getRunDevicePlatform(WebDriver driver) {
		
		String OS_Platform;
		String Device_Platform = Utils.getRunPlatForm();
		
		if(Device_Platform.equals("tablet") || Device_Platform.equals("mobile")){
			Log.message("DeviceName       :: " + getRunningDeviceName());
			//if(System.getProperty("deviceName" + Utils.getCurrentTestCode()) != null){ - For Debugging
			if(getRunningDeviceName() != null){
				//if(System.getProperty("deviceName" + Utils.getCurrentTestCode()).contains("android")) - For Debugging
				if(getRunningDeviceName().contains("android"))
					OS_Platform = "android";
				else
					OS_Platform = "mac";
			}else{
				//For Debugging
				/*if(System.getProperty("MobileOS").contains("android"))	
					OS_Platform = "android";
				else
					OS_Platform = "mac";*/
				
				OS_Platform = System.getProperty(Device_Platform + Utils.getCurrentTestCode());
				Log.event("OS_Platform :: " + OS_Platform);
			}
		}else{
			if(System.getProperty("SELENIUM_DRIVER") != null)
				OS_Platform = System.getProperty("SELENIUM_DRIVER").split("os=")[1].split("&")[0].trim();
			else if(System.getenv("SELENIUM_DRIVER") != null)
				OS_Platform = System.getenv("SELENIUM_DRIVER").split("os=")[1].split("&")[0].trim();
			else
				OS_Platform = System.getProperty("os.name").trim();
		}
		
		return OS_Platform;
	}

	/**
	 * To get browser name
	 * @param driver - Web Driver 
	 * @return String - Browser Name
	 */
	public static String getRunBrowser(WebDriver driver) {
		String dataToBeReturned = null;

		if(System.getProperty("runUserAgentDeviceTest")!=null){
			if(System.getProperty("runUserAgentDeviceTest").equals("true"))
				return getRunMobileBrowser();
		}

		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		dataToBeReturned = cap.getBrowserName();

		return dataToBeReturned.toLowerCase();
	}

	/**
	 * To get device name
	 * @return String - device name
	 */
	public static String getRunningDeviceName(){
		return System.getProperty("deviceName" + Utils.getCurrentTestCode()) != null ? System.getProperty("deviceName" + Utils.getCurrentTestCode()) : System.getProperty("deviceName");
	}
	
	/**
	 * To get mobile browser name
	 * @return String - Browser Name
	 */
	public static String getRunMobileBrowser(){
		String deviceString = System.getProperty("deviceName" + Utils.getCurrentTestCode()) != null ? System.getProperty("deviceName" + Utils.getCurrentTestCode()) : System.getProperty("deviceName");
		return deviceString.split("\\_")[1];
	}
	
	/**
	 * To get browser version 
	 * @param driver - Web Driver
	 * @return String - Browser version
	 */
	public static String getRunBrowserVersion(WebDriver driver) {
		String dataToBeReturned = null;
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		dataToBeReturned = cap.getVersion();
		return dataToBeReturned;
	}

	/**
	 * To generate random number from '0 to Maximum Value' or 'Minimum Value
	 * ---- Maximum Value'
	 * 
	 * @param max
	 *            - maximum bound
	 * @param min
	 *            - origin bound
	 * @return - random number between 'min to max' or '0 to max'
	 * @throws Exception - Exception
	 */
	public static int getRandom(int min, int max) throws Exception {
		Random random = new Random();
		int rand;
		if (min == 0)
			rand = random.nextInt(max);
		else
			rand = ThreadLocalRandom.current().nextInt(min, max);

		return rand;
	}

/**
 * To verify the page url contains the given word
 * @param driver -
 * @param hostURL -
 * @param stringContains -
 * @return boolean -
 */
	public static boolean verifyPageURLContains(final WebDriver driver, String hostURL, String stringContains) {
		boolean status = false;
		String url = null;
		try {
			url = driver.getCurrentUrl();
			if (url == null) {
				url = ((JavascriptExecutor) driver).executeScript("return document.URL;").toString();
			}
		} catch (Exception e) {
			url = ((JavascriptExecutor) driver).executeScript("return document.URL;").toString();
		}
		if (url.contains(hostURL.split("https://")[1]) && url.contains(stringContains)) {
			status = true;
		}

		return status;
	}

	/**
	 * Round to certain number of decimals
	 * 
	 * @param d -
	 * @param decimalPlace
	 *            the numbers of decimals
	 * @return float -
	 */
	public static float round(double d, int decimalPlace) {
		return BigDecimal.valueOf(d).setScale(decimalPlace, BigDecimal.ROUND_HALF_UP).floatValue();
	}

	/**
	 * checkPriceWithDollar
	 * 
	 * @param price -
	 * @return String -
	 */
	public static String checkPriceWithDollar(String price) {

		if (!price.startsWith("$")) {
			Log.failsoft(price + " does not contains '$' value");
		}

		return price;
	}

	/**
	 * copyHashMap
	 * 
	 * @param actual -
	 * @param ignore -
	 * @return LinkedList -
	 * @throws Exception - Exception
	 */
	public static LinkedHashMap<String, String> copyHashMap(LinkedHashMap<String, String> actual, String ignore)
			throws Exception {
		List<String> indexes = new ArrayList<String>(actual.keySet());
		LinkedHashMap<String, String> expected = new LinkedHashMap<String, String>();

		for (int i = 0; i < indexes.size(); i++) {
			if (!indexes.get(i).equals(ignore))
				expected.put(indexes.get(i), actual.get(indexes.get(i)));
		}

		return expected;
	}

	/**
	 * copyLinkedListHashMap
	 * 
	 * @param actual -
	 * @param ignore  -
	 * @return LinkedList - 
	 * @throws Exception - Exception
	 */
	public static LinkedList<LinkedHashMap<String, String>> copyLinkedListHashMap(
			LinkedList<LinkedHashMap<String, String>> actual, String ignore) throws Exception {
		int size = actual.size();
		LinkedList<LinkedHashMap<String, String>> expected = new LinkedList<LinkedHashMap<String, String>>();
		for (int j = 0; j < size; j++) {
			List<String> indexes = new ArrayList<String>(actual.get(j).keySet());
			LinkedHashMap<String, String> hashMap = new LinkedHashMap<String, String>();
			for (int i = 0; i < indexes.size(); i++) {
				if (!indexes.get(i).equals(ignore))
					hashMap.put(indexes.get(i), actual.get(j).get(indexes.get(i)));
			}
			expected.add(hashMap);
		}
		return expected;
	}


	/**
	 * To return the Title case format of given word or sentence
	 * 
	 * @param data string
	 * @return title case string
	 */
	public static String toCamelCase(String data) {
		String dataToReturn = new String();
		try {
			dataToReturn = WordUtils.capitalize(data);
		} catch (Exception e) {
			return data;
		}
		return dataToReturn;
	}

	/**
	 * To get the current brand.
	 * 
	 * @return text of the brand's name
	 */
	public static Brand getCurrentBrand() {
		String brand = new String();
		if(System.getProperties().toString().contains(Utils.getCurrentTestCode() + "_brand"))
			brand = System.getProperty(Utils.getCurrentTestCode() + "_brand").toLowerCase();
		else
			brand = System.getProperty("brand").toLowerCase();
		
		return Brand.fromConfiguration(brand);

	}
	
	/**
	 * To get current environment where the test cases being executed
	 * @return String environment short form
	 */
	public static String getCurrentEnv() {
		String env = new String();
		
		if(System.getProperties().toString().contains(Utils.getCurrentTestCode() + "_env"))
			env = System.getProperty(Utils.getCurrentTestCode() + "_env").toLowerCase();
		else
			env = System.getProperty("env").toLowerCase();
		
		return env;
	}
	
	/**
	 * To get the current brand Short.
	 * 
	 * @return text of the brand's name
	 */
	public static String getCurrentBrandShort() {
		String url = new String();
		if(System.getProperties().toString().contains(Utils.getCurrentTestCode() + "_brand"))
			url = System.getProperty(Utils.getCurrentTestCode() + "_brand").toLowerCase();
		else
			url = System.getProperty("brand").toLowerCase();
		return url;
	}

	/**
	 * To get the Name of the Test that is running currently
	 * 
	 * @return String - Test Name
	 */
	public static String getCurrentTestName() {
		return Reporter.getCurrentTestResult().getName();
	}

	/**
	 * To get the Name of the Test that is running currently
	 * 
	 * @return int
	 */
	public static int getCurrentTestCode() {
		return Reporter.getCurrentTestResult().hashCode();
	}

	public static Dimension getWindowSize(WebDriver driver) {
		return driver.manage().window().getSize();
	}

	/**
	 * Get expected page WebElement's location
	 * 
	 * @param ele
	 *            - expected WebElement value
	 * @param obj
	 *            - the page object the element is on
	 * @return String element width and height in the form "width|height"
	 * @throws Exception - Exception
	 */
	public static String getElementPosition(String ele, Object obj) throws Exception {
		Field f = null;
		try {
			f = obj.getClass().getDeclaredField(ele);
			f.setAccessible(true);
		} catch (NoSuchFieldException | SecurityException e1) {
			throw new Exception("No such a field present on this page, Please check the value:: " + ele);
		}
		WebElement element = null;
		try {
			element = ((WebElement) f.get(obj));
		} catch (IllegalArgumentException | IllegalAccessException e1) {
			Log.exception(e1);
		}
		return element.getLocation().getX() + "|" + element.getLocation().getY();
	}

	/**
	 * To wait until element to be clickable
	 * @param driver -
	 * @param element -
	 * @param maxWait -
	 * @return true - if element is clickable
	 */
	public static boolean waituntilElementClickable(WebDriver driver, WebElement element, int maxWait) {
		boolean statusOfElementToBeReturned = false;
		long startTime = StopWatch.startTime();
		try {
			WebElement waitElement=(new WebDriverWait(driver, 3).pollingEvery(maxWait, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)).until(ExpectedConditions.elementToBeClickable(element));
			//WebElement waitElement = wait.until(ExpectedConditions.elementToBeClickable(element));
			if (waitElement!=null) {
				statusOfElementToBeReturned = true;
				Log.event("Element is displayed and disabled:: " + element.toString());
			}

		} catch (Exception ex) {
			statusOfElementToBeReturned = false;
			Log.event("Unable to find disabled element after " + StopWatch.elapsedTime(startTime) + " sec ==> "
					+ element.toString());
		}
		return statusOfElementToBeReturned;
	}

	/**
	 * To get mapped webSite URL for current test method name
	 * @return String - webSite URL
	 * @throws Exception - Exception
	 */
	public static String getWebSite()throws Exception{
		String url = new String();
		if(System.getProperties().toString().contains(Utils.getCurrentTestCode() + "_webSite"))
			url = System.getProperty(Utils.getCurrentTestCode() + "_webSite");
		else
			url = System.getProperty("webSite");
		
		/*EnvironmentPropertiesReader envProperty = EnvironmentPropertiesReader.getInstance("env");
		if(EnvironmentPropertiesReader.getInstance("env").getProperty("siteProtection").equals("true")){
			url = url.replace("http://", "https://" + envProperty.getProperty("appUserName") + ":" + envProperty.getProperty("appPassword") + "@");
		}*/
		
		return url;
	}

	/**
	 * Is run device platform is Desktop?
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public static boolean isDesktop()throws Exception{
		return (Utils.getRunPlatForm().equals("desktop")) ? true : false;
	}
	
	/**
	 * Is run device platform is Table?
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public static boolean isTablet()throws Exception{
		return (Utils.getRunPlatForm().equals("tablet")) ? true : false;
	}
	
	/**
	 * Is run device platform is Mobile?
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public static boolean isMobile(){
		return (Utils.getRunPlatForm().equals("mobile")) ? true : false;
	}
	
	/**
	 * To compare print hash map as Table
	 * @param title - Table title
	 * @param col1Head - column-1 header
	 * @param col2Head - column-2 header
	 * @param hashMap1 - hashMap-1
	 * @param hashMap2 - hashMap-2
	 * @param noNeed - Ignore list
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public static boolean compareAndPrintTableHashMap(String title,
			String col1Head, String col2Head,
			LinkedHashMap<String, String> hashMap1,
			LinkedHashMap<String, String> hashMap2, String[]... noNeed)
					throws Exception {

		if (noNeed.length > 0) {
			for (int i = 0; i < noNeed[0].length; i++) {
				System.out.println("Removed Key from <b>Expected:: "
						+ noNeed[0][i]);
				hashMap1.remove(noNeed[0][i]);
				System.out.println("Removed Key from Actual  :: "
						+ noNeed[0][i]);
				hashMap2.remove(noNeed[0][i]);
			}
		}

		Log.message("<br>");
		Log.message1("<table><tr bgcolor='#BBBBBB'><td colspan=3><b><font color='black'>"
				+ title + "</font></b></td></tr>");
		Log.message1("<tr align='center' bgcolor='#BBBBBB' style='color:black;'><td>Contents</td><td>"
				+ col1Head + "</td><td>" + col2Head + "</td></tr>");
		List<String> indexes1 = new ArrayList<String>(hashMap1.keySet());
		List<String> indexes2 = new ArrayList<String>(hashMap2.keySet());
		List<String> maxIndex = indexes1.size() > indexes2.size() ? indexes1
				: indexes2;

		for (int i = 0; i < maxIndex.size(); i++) {
			String value1 = hashMap1.containsKey(maxIndex.get(i)) ? hashMap1
					.get(maxIndex.get(i)) : "No Value";
					String value2 = hashMap2.containsKey(maxIndex.get(i)) ? hashMap2
							.get(maxIndex.get(i)) : "No Value";
							if (value1.equalsIgnoreCase(value2))
								Log.message1("<tr><td bgcolor='#BBBBBB' style='color:black;'>"
										+ maxIndex.get(i) + "</td><td>" + value1 + "</td><td>"
										+ value2 + "</td></tr>");
							else
								Log.message1("<tr><td bgcolor='#BBBBBB' style='color:black;'>"
										+ maxIndex.get(i) + "</td><td bgcolor='red'>" + value1
										+ "</td><td>" + value2 + "</td></tr>");
		}
		Log.message1("</table>");

		return Utils.compareTwoHashMap(hashMap1, hashMap2);
	}
	
	/**
	 * To compare and print linked list of hashmap in table format
	 * @param title - Table title
	 * @param col1Head - column-1 header
	 * @param col2Head - column-2 header
	 * @param hashMap1 - hashMap-1
	 * @param hashMap2 - hashMap-2
	 * @param noNeed - Ignore list
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public static boolean compareAndPrintTableLinkedListHashMap(String title,
			String col1Head, String col2Head,
			LinkedList<LinkedHashMap<String, String>> hashMap1,
			LinkedList<LinkedHashMap<String, String>> hashMap2,
			String[]... noNeed) throws Exception {
		// int iteration1 = hashMap1.size();
		int iteration2 = hashMap2.size();
		boolean flag = true;
		for (int j = 0; j < iteration2; j++) {
			if (noNeed.length > 0) {
				for (int i = 0; i < noNeed[0].length; i++) {
					System.out.println("Removed Key from Expected:: "
							+ noNeed[0][i]);
					hashMap1.get(j).remove(noNeed[0][i]);
					System.out.println("Removed Key from Actual  :: "
							+ noNeed[0][i]);
					hashMap2.get(j).remove(noNeed[0][i]);
				}
			}

			Log.message("<br>");
			Log.message1("<table><tr bgcolor='#BBBBBB' align='center'><td colspan=3><b><font color='black'>"
					+ title + "(" + (j + 1) + ")</font></b></td></tr>");
			Log.message1("<tr align='center' bgcolor='#BBBBBB' style='color:black;'><td>Contents</td><td>"
					+ col1Head + "</td><td>" + col2Head + "</td></tr>");
			List<String> indexes1 = new ArrayList<String>(hashMap1.get(j)
					.keySet());
			List<String> indexes2 = new ArrayList<String>(hashMap2.get(j)
					.keySet());
			List<String> maxIndex = indexes1.size() > indexes2.size() ? indexes1
					: indexes2;
			for (int i = 0; i < maxIndex.size(); i++) {
				String value1 = hashMap1.get(j).containsKey(maxIndex.get(i)) ? hashMap1.get(j).get(maxIndex.get(i)) : "<font color='red'>No Value</font>";
				String value2 = hashMap2.get(j).containsKey(maxIndex.get(i)) ? hashMap2.get(j).get(maxIndex.get(i)) : "<font color='red'>No Value</font>";
				if (value1.equals(value2))
									Log.message1("<tr><td bgcolor='#BBBBBB' style='color:black;'>"
											+ maxIndex.get(i)
											+ "</td><td>"
											+ value1
											+ "</td><td>" + value2 + "</td></tr>");
								else
									Log.message1("<tr><td bgcolor='#BBBBBB' style='color:black;'>"
											+ maxIndex.get(i)
											+ "</td><td bgcolor='red'>"
											+ value1 + "</td><td>" + value2 + "</td></tr>");
			}
			Log.message1("</table>");

			flag = Utils.compareTwoHashMap(hashMap1.get(j), hashMap2.get(j));
		}

		return flag;
	}
	
	/**
	 To compare and print hashMap of hashMap in table format
	 * @param title - Table title
	 * @param col1Head - column-1 header
	 * @param col2Head - column-2 header
	 * @param hashMap1 - hashMap-1
	 * @param hashMap2 - hashMap-2
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public static boolean compareLinkedHashMapOfHashMap(String title,
			String col1Head, String col2Head,
			LinkedHashMap<String, LinkedHashMap<String, String>> hashMap1,
			LinkedHashMap<String, LinkedHashMap<String, String>> hashMap2) throws Exception {
		boolean flag = true;
		
		List<String> expPrdNames = new ArrayList<String>(hashMap1.keySet());
		List<String> actPrdNames = new ArrayList<String>(hashMap2.keySet());
		
		Collections.sort(expPrdNames, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Collator.getInstance().compare(o1, o2);
			}
		});
		
		Collections.sort(actPrdNames, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Collator.getInstance().compare(o1, o2);
			}
		});
		
		if(expPrdNames.equals(actPrdNames)){
			for(String key : expPrdNames){
				if(!compareAndPrintTableHashMap(title, col1Head, col2Head, hashMap1.get(key), hashMap2.get(key)))
					flag=false;
			}
		}else
			return false;
		
		
		return flag;
	}
	
	/**
	 * To get the brandName except the current brand
	 * @param brand -
	 * @return brandToReturn -
	 * @throws Exception -
	 */
	public static Brand[] returnBrandExcept(Brand brand)throws Exception{
		List<Brand> lstBrand = Arrays.asList(Brand.values()); 
		Brand[] brandToReturn = new Brand[lstBrand.size()];
		lstBrand.remove(brand);
		brandToReturn = lstBrand.toArray(brandToReturn);
		return brandToReturn;
	}
	
	/**
	 * To wait for Ajax to load 
	 * @param WebDriver - driver
	 * @throws Exception - Exception
	 */
	public static void WaitForAjax(WebDriver driver)throws Exception
	{
		boolean ajaxIsComplete = false;
	    while (!ajaxIsComplete) // Handle timeout somewhere 
	    {
	        ajaxIsComplete = (boolean)((JavascriptExecutor) driver).executeScript("return jQuery.active == 0");
	    }
	}
	
	
	/**
	 * To extract number from a string
	 * @param String - String to extract number
	 * @return int - Number extracted from given string 
	 */
	public static int getNumberInString(String stringWithNumer) {
		return Integer.parseInt(stringWithNumer.replaceAll("[^0-9]", ""));
	}
	
	/**
	 * To get all details of current date as a map
	 * @return Map<String, String> - details of date as map
	 * @throws Exception - Exception
	 */
	public static Map<String, String> getCurrentDateDetails() throws Exception{
		Map<String, String> today = new HashMap<String,String>();
		Calendar now = Calendar.getInstance();
		String dateCurrent = ""+now.get(Calendar.DATE);
		String dayCurrent = ""+now.get(Calendar.DAY_OF_WEEK);
		String weekNoCurrent = ""+now.get(Calendar.WEEK_OF_YEAR);
		String monthCurrent = ""+now.get(Calendar.MONTH);
		String yearCurrent = ""+now.get(Calendar.YEAR);
		today.put("date", dateCurrent);
		today.put("day", dayCurrent);
		today.put("week", weekNoCurrent);
		today.put("month", monthCurrent);
		today.put("year", yearCurrent);
		return today;
	}
	
	/**
	 * To get specific detail of current date
	 * @param String - The part of date to get
	 * @return String - specific detail of current date
	 * @throws Exception - Exception
	 */
	public static int getCurrentDateDetails(String partOfDate) throws Exception{
		Map<String, String> today = getCurrentDateDetails();
		try {
			return Integer.parseInt(today.get(partOfDate));
		}catch(Exception e) {
			Log.message("Invalid argument. Verify part of date parameter.");
			return 0;
		}
	}
	
	/**
	 * To alternate merge two lists
	 * @param List<E> - First list
	 * @param List<E> - Second list
	 * @return List<E> - Merged list
	 * @throws Exception - Exception
	 */
	public static <E>List<E> listAlternateMerge(List<E> list1, List<E> list2) throws Exception{
		Iterator<E> itLst1 = list1.iterator();
		Iterator<E> itLst2 = list2.iterator();
		List<E> mergedList = new ArrayList<E>();
		
		while(itLst1.hasNext() || itLst2.hasNext()) {
			if(itLst1.hasNext()) mergedList.add(itLst1.next());
			if(itLst2.hasNext()) mergedList.add(itLst2.next());
		}
		return mergedList;
	}
}
