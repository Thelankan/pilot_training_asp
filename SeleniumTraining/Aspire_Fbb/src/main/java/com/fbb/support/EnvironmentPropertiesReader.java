package com.fbb.support;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.testng.log4testng.Logger;


/**
 * EnvironmentPropertiesReader is to set the environment variable declaration
 * mapping for config properties in the UI test
 */
public class EnvironmentPropertiesReader {

	private static final Logger log = Logger.getLogger(EnvironmentPropertiesReader.class);
	private static EnvironmentPropertiesReader envProperties;

	private Properties properties;

	/**
	 * Default Constructor to load Config.properties
	 */
	private EnvironmentPropertiesReader() {
		properties = loadProperties();
	}
	
	/**
	 * Parameterized constructor to load given property file
	 * @param propertyFile - Property file name without extension
	 */
	private EnvironmentPropertiesReader(String propertyFile) {
		properties = loadProperties(propertyFile);
	}

	/**
	 * To default property file config.properties
	 * @return Properties -
	 */
	private Properties loadProperties() {
		File file = new File("./src/main/resources/config.properties");
		FileInputStream fileInput = null;
		Properties props = new Properties();

		try {
			fileInput = new FileInputStream(file);
			props.load(fileInput);
			fileInput.close();
		} catch (FileNotFoundException e) {
			log.error("config.properties is missing or corrupt : " + e.getMessage());
		} catch (IOException e) {
			log.error("read failed due to: " + e.getMessage());
		}

		return props;
	}
	
	/**
	 * To load properties from given property file name without extension
	 * @param propertyFile - Property File Name without Extension
	 * @return Properties - All properties available in property file
	 */
	private Properties loadProperties(String propertyFile) {
		File file = new File("./src/main/resources/"+propertyFile+".properties");
		FileInputStream fileInput = null;
		Properties props = new Properties();

		try {
			fileInput = new FileInputStream(file);
			props.load(fileInput);
			fileInput.close();
		} catch (FileNotFoundException e) {
			log.error(""+propertyFile+".properties is missing or corrupt : " + e.getMessage());
		} catch (IOException e) {
			log.error("read failed due to: " + e.getMessage());
		}

		return props;
	}

	/**
	 * To get current instance of EnvironmentPropertiesReader
	 * @return EnvironmentPropertiesReader - current instance
	 */
	public static EnvironmentPropertiesReader getInstance() {
		if (envProperties == null) {
			envProperties = new EnvironmentPropertiesReader();
		}
		return envProperties;
	}
	
	/**
	 * To get given property file instance of EnvirionmentPropertyReader
	 * @param propertyFile - Property File name without
	 * @return EnvironmentPropertiesReader - Property file instance
	 */
	public static EnvironmentPropertiesReader getInstance(String propertyFile) {
		EnvironmentPropertiesReader envProperties=null;
		if (envProperties == null) {
			envProperties = new EnvironmentPropertiesReader(propertyFile);
		}
		return envProperties;
	}

	/**
	 * To get property value for given key
	 * @param key - Key String
	 * @return String - value String
	 */
	public String getProperty(String key) {
		return properties.getProperty(key);
	}
	
	/**
	 * Special method used to get only data.properties values.
	 * @param key - Product Definition Syntax
	 * @return - Product ID
	 */
	public String get(String key) {
		String value = properties.getProperty(key);
		if(value.isEmpty() || value.equals(null) || value.equals("")) 
			Log.fail("Product ID missing in property file / Product definition(" + key + ") syntax is not valid.");

		return value.trim();
	}
	
	/**
	 * To set property with given key and value instance
	 * @param key - Key String
	 * @param value - Value String
	 */
	public void setProperty(String key, String value){
		properties.setProperty(key, value);
	}
	
	/**
	 * To verify given key is available in properties
	 * @param key - Key String
	 * @return boolean - true/false
	 */
	public boolean hasProperty(String key) {		
		return StringUtils.isNotBlank(properties.getProperty(key));
	}
}
