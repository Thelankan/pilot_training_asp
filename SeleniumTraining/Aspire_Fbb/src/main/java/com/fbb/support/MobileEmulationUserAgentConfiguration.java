package com.fbb.support;

import java.util.HashMap;

/**
 * Class to set the Mobile emulation configuration on chrome browser
 * 
 * <a href=
 *      "http://www.webapps-online.com/online-tools/user-agent-strings/dv">User
 *      Agent String</a> for more info on the available user agent string.
 * <a href=
 *      "https://sites.google.com/a/chromium.org/chromedriver/mobile-emulation">
 *      Mobile emulation on chrome</a> for more info on chrome mobile emulation.
 * 
 */
public class MobileEmulationUserAgentConfiguration {

	public static final String CHROME_IPHONE_IOS10 = "iphone6plus_chrome_mobile";
	public static final String CHROME_IPAD_IOS10 = "ipad4_chrome_tablet";
	public static final String CHROME_ANDROID6_TABLET = "android_chrome_tablet";
	public static final String CHROME_ANDROID6_MOBILE = "android_chrome_mobile";
	public static final String SAFARI_IPHONE_IOS10 = "iphone6plus_safari_mobile";
	public static final String SAFARI_IPAD_IOS10 = "ipad4_safari_tablet";
	
	
	public static final String ANDROID_GALAXY_TAB_A5_1_LANDSCAPE = "androidgalaxy_a5_1_tablet_landscape";

	@SuppressWarnings("serial")
	private final HashMap<String, String> chrome_iphone6plus_ios10 = new HashMap<String, String>() {
		{
			put("userAgent",
					"Mozilla/6.0 (iPhone; CPU iPhone OS 10_9_3 like Mac OS X) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/10.0 Mobile/10A5376e Chrome/57.0");
			put("width", "414");
			put("height", "736");
			put("pixelRatio", "2");			
		}
	};
	
	@SuppressWarnings("serial")
	private final HashMap<String, String> chrome_ipad4_ios10 = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/6.0 (iPad; CPU iPhone OS 10_9_3 like Mac OS X) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/10.0 Mobile/10A5376e Chrome/57.0");
			put("width", "768");
			put("height", "1024");
			put("pixelRatio", "2");
		}
	};
	
	@SuppressWarnings("serial")
	private final HashMap<String, String> safari_iphone6plus_ios10 = new HashMap<String, String>() {
		{
			put("userAgent",
					"Mozilla/6.0 (iPhone; CPU iPhone OS 10_9_3 like Mac OS X) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/10.0 Mobile/10A5376e Safari/7046A194A");
			put("width", "414");
			put("height", "736");
			put("pixelRatio", "2");			
		}
	};
	
	@SuppressWarnings("serial")
	private final HashMap<String, String> safari_ipad4_ios10 = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/6.0 (iPad; CPU iPhone OS 10_9_3 like Mac OS X) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/10.0 Mobile/10A5376e Safari/7046A194A");
			put("width", "768");
			put("height", "1024");
			put("pixelRatio", "2");
		}
	};
	
	/*@SuppressWarnings("serial")
	private final HashMap<String, String> chrome_android_mobile = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/5.0 (Linux; Android 5.1.1; SM-T335 Build/LMY47X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Safari/537.36");
			put("width", "800");
			put("height", "1280");
			put("pixelRatio", "2");
		}
	}; */
	
	@SuppressWarnings("serial")
	private final HashMap<String, String> chrome_android_tablet = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/5.0 (Linux; Android 6.0; SM-T335 Build/LMY47X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0");
			put("width", "768");
			put("height", "1024");
			put("pixelRatio", "2");
		}
	}; 
	
	@SuppressWarnings("serial")
	private final HashMap<String, String> chrome_android_mobile = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/5.0 (Linux; Android 6.0; SM-T335 Build/LMY47X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0");
			put("width", "414");
			put("height", "736");
			put("pixelRatio", "2");		
		}
	}; 

	/**
	 * To storing the all the devices configurations
	 *
	 * @return userAgentData - have mobile emulation configuration data(user
	 *         agent, width, height and pixelRatio)
	 */
	public HashMap<String, HashMap<String, String>> setUserAgentConfigurationValue() {

		HashMap<String, HashMap<String, String>> userAgentData = new HashMap<String, HashMap<String, String>>();

		userAgentData.put(CHROME_IPHONE_IOS10, chrome_iphone6plus_ios10);	
		userAgentData.put(CHROME_IPAD_IOS10, chrome_ipad4_ios10);
		userAgentData.put(SAFARI_IPHONE_IOS10, safari_iphone6plus_ios10);
		userAgentData.put(SAFARI_IPAD_IOS10, safari_ipad4_ios10);
		userAgentData.put(CHROME_ANDROID6_MOBILE, chrome_android_mobile);
		userAgentData.put(CHROME_ANDROID6_TABLET, chrome_android_tablet);
		
		return userAgentData;
	}

	/**
	 * To get the user agent string from device name
	 * 
	 * @param deviceName
	 *            - device name which going to perform a mobile emulation on
	 *            chrome
	 * @return dataToBeReturned- device width
	 */
	public String getUserAgent(String deviceName) {
		String dataToBeReturned = null;
		HashMap<String, HashMap<String, String>> getUserAgent = setUserAgentConfigurationValue();
		dataToBeReturned = hasDeviceName(deviceName) ? (String) getUserAgent.get(deviceName).get("userAgent") : null;
		String device = deviceName.split("\\_")[2];
		String platform = deviceName.split("\\_")[0].equals("android") ? "android" : "mac";
		System.setProperty(device + Utils.getCurrentTestCode(), platform);
		return dataToBeReturned;
	}

	/**
	 * To get the device width string from device name
	 * 
	 * @param deviceName
	 *            - device name which going to perform a mobile emulation on
	 *            chrome
	 * @return String -
	 */
	public String getDeviceWidth(String deviceName) {
		String dataToBeReturned = null;
		HashMap<String, HashMap<String, String>> getDeviceWidth = setUserAgentConfigurationValue();
		dataToBeReturned = hasDeviceName(deviceName) ? (String) getDeviceWidth.get(deviceName).get("width") : null;
		return dataToBeReturned;
	}

	/**
	 * To get the device height string from device name
	 * 
	 * @param deviceName
	 *            - device name which going to perform a mobile emulation on
	 *            chrome
	 * @return dataToBeReturned- device height
	 */
	public String getDeviceHeight(String deviceName) {
		String dataToBeReturned = null;
		HashMap<String, HashMap<String, String>> getDeviceHeight = setUserAgentConfigurationValue();
		dataToBeReturned = hasDeviceName(deviceName) ? (String) getDeviceHeight.get(deviceName).get("height") : null;
		return dataToBeReturned;
	}

	/**
	 * To get the device pixel ratio string from device name
	 * 
	 * @param deviceName
	 *            - device name which going to perform a mobile emulation on
	 *            chrome
	 * @return dataToBeReturned - device pixel ratio
	 */
	public String getDevicePixelRatio(String deviceName) {
		String dataToBeReturned = null;
		HashMap<String, HashMap<String, String>> getDevicePixelRatio = setUserAgentConfigurationValue();
		dataToBeReturned = hasDeviceName(deviceName) ? (String) getDevicePixelRatio.get(deviceName).get("pixelRatio")
				: null;
		return dataToBeReturned;
	}

	/**
	 * To check the device name present in the set up key hash map
	 * 
	 * @param deviceName
	 *            - device name which going to perform a mobile emulation on
	 *            chrome
	 * @return boolean value - if device name in the set up key will return
	 *         true, otherwise false
	 */
	private boolean hasDeviceName(String deviceName) {
		HashMap<String, HashMap<String, String>> hasDeviceName = setUserAgentConfigurationValue();
		return hasDeviceName.containsKey(deviceName);
	}

	/**
	 * To get the device name from mobile emulation attributes
	 * 
	 * @param userAgent
	 *            - mapped user agent string with device name
	 * @param pixelRatio
	 *            - mapped pixel ratio with device name
	 * @param width
	 *            - mapped width with device name
	 * @param height
	 *            - mapped height with device name
	 * @return dataToBeReturned - device name mapped with user agent, pixel
	 *         ratio, width and height
	 */
	public String getDeviceNameFromMobileEmulation(String userAgent, String pixelRatio, String width, String height) {
		String dataToBeReturned = null;
		boolean found = false;
		HashMap<String, HashMap<String, String>> getDeviceData = setUserAgentConfigurationValue();
		for (Object usKey : getDeviceData.keySet()) {
			if (getDeviceData.get(usKey).get("userAgent").equals(userAgent)
					&& getDeviceData.get(usKey).get("pixelRatio").equals(pixelRatio)
					&& getDeviceData.get(usKey).get("width").equals(width)
					&& getDeviceData.get(usKey).get("height").equals(height)) {
				dataToBeReturned = (String) usKey;
				found = true;
			}
		}
		if (!found) {
			dataToBeReturned = null;
		}
		return dataToBeReturned;
	}

	
}
