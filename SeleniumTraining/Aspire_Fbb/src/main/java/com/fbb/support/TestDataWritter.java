package com.fbb.support;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;

public class TestDataWritter {

	private String workBookName;
	private String workSheet;
	private String testCaseId;
	private boolean doFilePathMapping;
	private Hashtable<String, Integer> excelHeaders = new Hashtable<String, Integer>();
	private Hashtable<String, Integer> excelrRowColumnCount = new Hashtable<String, Integer>();

	public TestDataWritter() {
	}

	public TestDataWritter(String xlWorkBook, String xlWorkSheet) {
		this.workBookName = xlWorkBook;
		this.workSheet = xlWorkSheet;
	}

	public TestDataWritter(String xlWorkBook, String xlWorkSheet, String tcID) {
		this.workBookName = xlWorkBook;
		this.workSheet = xlWorkSheet;
		this.testCaseId = tcID;
	}

	public String getWorkBookName() {
		return workBookName;
	}

	public void setWorkBookName(String workBookName) {
		this.workBookName = workBookName;
	}

	public void setFilePathMapping(boolean doFilePathMapping) {
		this.doFilePathMapping = doFilePathMapping;
	}

	public String getWorkSheet() {
		return workSheet;
	}

	public void setWorkSheet(String workSheet) {
		this.workSheet = workSheet;
	}

	public String getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}

	public synchronized static boolean initWrite(String workBookName, String workSheetName,String keyString, String key,String valueString,String statusString, String valueToOverRide, String status){
		/** Loading the test data from excel using the test case id */
		TestDataWritter testData = new TestDataWritter();
		testData.setWorkBookName(workBookName);
		testData.setWorkSheet(workSheetName);
		testData.setFilePathMapping(true);

		Throwable t = new Throwable();;
		String testCaseId = t.getStackTrace()[1].getMethodName();
		if(!testCaseId.contains("TC_")){
			for(int i = 0 ; i < t.getStackTrace().length; i++){
				if(t.getStackTrace()[i].getMethodName().contains("TC_")){
					testCaseId = t.getStackTrace()[i].getMethodName();
					break;
				}
			}
		}
		testData.setTestCaseId(testCaseId);
		
		return testData.writeDataToExcel(keyString, key, valueString, valueToOverRide,statusString, status);
	}

	public synchronized static boolean initWrite(String workBookName, String workSheetName,String keyString, String key, HashMap<String, String> valuesToWrite){
		/** Loading the test data from excel using the test case id */
		TestDataWritter testData = new TestDataWritter();
		testData.setWorkBookName(workBookName);
		testData.setWorkSheet(workSheetName);
		testData.setFilePathMapping(true);

		Throwable t = new Throwable();;
		String testCaseId = t.getStackTrace()[1].getMethodName();
		if(!testCaseId.contains("_FBB_")){
			for(int i = 0 ; i < t.getStackTrace().length; i++){
				if(t.getStackTrace()[i].getMethodName().contains("_FBB_")){
					testCaseId = t.getStackTrace()[i].getMethodName();
					break;
				}
			}
		}
		testData.setTestCaseId(testCaseId);
		
		return testData.writeDataToExcel(keyString, key, valuesToWrite);
	}

	public synchronized Boolean writeDataToExcel(String keyString, String key, HashMap<String, String> valuesToWrite){
		com.fbb.support.ReadFromExcel readTestData = new com.fbb.support.ReadFromExcel();

		try {
			String filePath = "";
			if (doFilePathMapping)
				filePath = ".\\src\\main\\resources\\" + workBookName;
			else
				filePath = workBookName;

			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(filePath));
			HSSFWorkbook workbook = new HSSFWorkbook(fs);
			HSSFSheet sheet = workbook.getSheet(workSheet);
			HSSFRow row = null;
			int rowNumber = -1;
			HSSFCell cell = null;
			excelrRowColumnCount = readTestData.findRowColumnCount(sheet, excelrRowColumnCount);
			excelHeaders = readTestData.readExcelHeaders(sheet, excelHeaders, excelrRowColumnCount); 

			for (int r = 0; r < excelrRowColumnCount.get("RowCount"); r++) {
				row = sheet.getRow(r);  
				if(readTestData.convertHSSFCellToString(row.getCell(excelHeaders.get(keyString))).equals(key)){
					rowNumber = row.getRowNum();
					break;
				}
			} 
			if(rowNumber == (-1)){
				rowNumber = sheet.getLastRowNum() + 1;
				row = sheet.createRow(rowNumber);
				
				rowNumber = excelrRowColumnCount.get("RowCount");
				cell = sheet.getRow(rowNumber).getCell(excelHeaders.get("TestID"), Row.CREATE_NULL_AS_BLANK);
				try {
					cell.setCellValue(testCaseId);
				}catch(NullPointerException e){
					sheet.getRow(rowNumber).createCell(excelHeaders.get("TestID")).setCellValue(testCaseId);
				}
			}
			List<String> keySet = new ArrayList<String>(valuesToWrite.keySet());
			for(String keyS : keySet) {
				cell = sheet.getRow(rowNumber).getCell(excelHeaders.get(keyS));
				try{
					cell.setCellValue(valuesToWrite.get(keyS));
				}catch(NullPointerException e){
					sheet.getRow(rowNumber).createCell(excelHeaders.get(keyS)).setCellValue(valuesToWrite.get(keyS));
				}
			}
			FileOutputStream outputStream = new FileOutputStream(filePath);
			workbook.write(outputStream);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}

	public synchronized Boolean writeDataToExcel(String keyString, String key,String valueString,  String valueToOverRide,String statusString, String status){
		com.fbb.support.ReadFromExcel readTestData = new com.fbb.support.ReadFromExcel();

		try {
			String filePath = "";
			if (doFilePathMapping)
				filePath = ".\\src\\main\\resources\\" + workBookName;
			else
				filePath = workBookName;

			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(filePath));
			HSSFWorkbook workbook = new HSSFWorkbook(fs);
			HSSFSheet sheet = workbook.getSheet(workSheet);
			HSSFRow row = null;
			int rowNumber = -1;
			HSSFCell cell = null;
			excelrRowColumnCount = readTestData.findRowColumnCount(sheet, excelrRowColumnCount);
			excelHeaders = readTestData.readExcelHeaders(sheet, excelHeaders, excelrRowColumnCount); 

			for (int r = 0; r < excelrRowColumnCount.get("RowCount"); r++) {
				row = sheet.getRow(r);  
				if(readTestData.convertHSSFCellToString(row.getCell(excelHeaders.get(keyString))).equals(key)){
					rowNumber = row.getRowNum();
					break;
				}
			} 
			if(rowNumber != (-1)){
				cell = sheet.getRow(rowNumber).getCell(excelHeaders.get(valueString));
				String oldID = new String();
				try{
					oldID = cell.getStringCellValue();
					valueToOverRide = oldID + "|" + valueToOverRide;
					cell.setCellValue(valueToOverRide);
				}catch(NullPointerException e){
					sheet.getRow(rowNumber).createCell(excelHeaders.get(valueString)).setCellValue(valueToOverRide);
					sheet.getRow(rowNumber).createCell(excelHeaders.get(statusString)).setCellValue(status);
				}


			}
			FileOutputStream outputStream = new FileOutputStream(filePath);
			workbook.write(outputStream);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}

	public synchronized static boolean initWriteBug(String workBookName, String workSheetName,HashMap<String, String> testInfo){
		/** Loading the test data from excel using the test case id */
		TestDataWritter testData = new TestDataWritter();
		testData.setWorkBookName(workBookName);
		testData.setWorkSheet(workSheetName);
		testData.setFilePathMapping(true);

		return testData.writeBugDataToExcel(testInfo);
	}

	public synchronized Boolean writeBugDataToExcel(HashMap<String, String> testInfo){
		com.fbb.support.ReadFromExcel readTestData = new com.fbb.support.ReadFromExcel();

		try {
			String filePath = "";
			if (doFilePathMapping)
				filePath = ".\\src\\main\\resources\\" + workBookName;
			else
				filePath = workBookName;

			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(filePath));
			HSSFWorkbook workbook = new HSSFWorkbook(fs);
			HSSFSheet sheet = workbook.getSheet(workSheet);
			HSSFRow row = null;
			int rowNumber = -1;
			HSSFCell cell = null;
			excelrRowColumnCount = readTestData.findRowColumnCount(sheet, excelrRowColumnCount);
			excelHeaders = readTestData.readExcelHeaders(sheet, excelHeaders, excelrRowColumnCount); 

			List<String> keySet = new ArrayList<String>(testInfo.keySet());
			for (int r = 0; r < excelrRowColumnCount.get("RowCount"); r++) {
				row = sheet.getRow(r);  //row.getCell(excelHeaders.get(bugInfo.get(keySet.get(0)))).equals(bugInfo.get(keySet.get(0))
				String excelData = readTestData.convertHSSFCellToString(row.getCell(excelHeaders.get(keySet.get(0))));
				if(excelData.equals(testInfo.get(keySet.get(0)))){
					rowNumber = row.getRowNum();
					break;
				}
			} 
			if(rowNumber == (-1)){
				rowNumber = sheet.getLastRowNum() + 1;
				row = sheet.createRow(rowNumber);
				for(int i=0; i<keySet.size();i++)
					row.createCell(excelHeaders.get(keySet.get(i))).setCellValue(testInfo.get(keySet.get(i)));

			}else{
				cell = sheet.getRow(rowNumber).getCell(excelHeaders.get("Status")); 
				if(cell.getStringCellValue().equals("NEW"))
					cell.setCellValue("CREATE");
				else if(cell.getStringCellValue().equals("LOCKED"))
					cell.setCellValue("VALIDATE");
				else if(cell.getStringCellValue().equals("CREATE")){
					sheet.getRow(rowNumber).createCell(excelHeaders.get("Status")).setCellValue("LOCKED");
					cell = sheet.getRow(rowNumber).getCell(excelHeaders.get("BugId"));
					String oldID = new String();
					String valueToOverRide = new String();
					try{
						oldID = cell.getStringCellValue();
						valueToOverRide = oldID.equals("")? testInfo.get("BugId"): oldID + "|" + testInfo.get("BugId");
						cell.setCellValue(valueToOverRide);
					}catch(NullPointerException e){
						sheet.getRow(rowNumber).createCell(excelHeaders.get("BugId")).setCellValue(testInfo.get("BugId"));
					}
					//sheet.getRow(rowNumber).createCell(excelHeaders.get("BugId")).setCellValue(testInfo.get("BugId"));
				}
			}

			FileOutputStream outputStream = new FileOutputStream(filePath);
			workbook.write(outputStream);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public void googleSheetAPITest()throws Exception{
		
	}
}
