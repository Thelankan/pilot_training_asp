package com.fbb.support;

import java.util.List;
import java.util.Arrays;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

public class DataProviderUtils {
	private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();

	/**
	 * To provide parallel list of instance to System class to start threads
	 * @param context -
	 * @return Iterator for list of Objects with Browser combos specifications
	 * @throws IOException - IOException
	 */
	@DataProvider(parallel = true)
	public static Iterator<Object[]> parallelTestDataProvider(ITestContext context) throws IOException {

		List<Object[]> dataToBeReturned = new ArrayList<Object[]>();
		List<String> browsers = null;
		List<String> platforms = null;
		List<String> browserVersions = null;
		List<String> deviceNames = null;
		String driverInitilizeInfo = null;
		String browser = null;
		String platform = null;
		String browserVersion = null;
		String deviceName = null;
		Iterator<String> browsersIt = null;
		Iterator<String> browserVersionsIt = null;
		Iterator<String> platformsIt = null;
		Iterator<String> deviceNameIt = null;

		boolean isUAEnabled = false;
		if((System.getProperties().contains("runUserAgentDeviceTest") && System.getProperty("runUserAgentDeviceTest").equals("true"))
				|| configProperty.getProperty("runUserAgentDeviceTest").equals("true")){
			isUAEnabled = true;
		}
		// From local to sauce lab for browser test
		if (configProperty.hasProperty("runSauceLabFromLocal")
				&& configProperty.getProperty("runSauceLabFromLocal").equalsIgnoreCase("true")) {
			if((configProperty.hasProperty("runTablet") ? configProperty.getProperty("runTablet") : "false").equals("true")){
				browser = configProperty.hasProperty("tabletBrowserName") ? configProperty.getProperty("tabletBrowserName") : null;
			}else if((configProperty.hasProperty("runMobile") ? configProperty.getProperty("runMobile") : "false").equals("true")){
				browser = configProperty.hasProperty("mobileBrowserName") ? configProperty.getProperty("mobileBrowserName") : null;
			}else{
				browser = configProperty.hasProperty("browserName") ? configProperty.getProperty("browserName") : null;
				browserVersion = configProperty.hasProperty("browserVersion") ? configProperty.getProperty("browserVersion") : null;
				platform = configProperty.hasProperty("platform") ? configProperty.getProperty("platform") : null;
			}

			browsers = Arrays.asList(browser.split("\\|"));
			browsersIt = browsers.iterator();

			if(isUAEnabled){
				deviceNames = (System.getProperty("deviceName") != null) ? Arrays.asList(System.getProperty("deviceName").split("\\|")) : Arrays.asList(configProperty.getProperty("deviceName").split("\\|"));
				deviceNameIt = deviceNames.iterator();
				while(deviceNameIt.hasNext()){
					deviceName = deviceNameIt.next();
					driverInitilizeInfo = configProperty.getProperty("deviceBrowser").replace("|", "&") + "&" + deviceName;
					dataToBeReturned.add(new Object[] { driverInitilizeInfo });
				}
			}else{
				if((configProperty.hasProperty("runTablet") ? configProperty.getProperty("runTablet") : "false").equals("true")){
					while (browsersIt.hasNext()) {
						browser = browsersIt.next();
						driverInitilizeInfo = browser;
						dataToBeReturned.add(new Object[] { driverInitilizeInfo });
					}
				}else if((configProperty.hasProperty("runMobile") ? configProperty.getProperty("runMobile") : "false").equals("true")){
					while (browsersIt.hasNext()) {
						browser = browsersIt.next();
						driverInitilizeInfo = browser;
						dataToBeReturned.add(new Object[] { driverInitilizeInfo });
					}
				}else{
					browserVersions = Arrays.asList(browserVersion.split("\\|"));
					platforms = Arrays.asList(platform.split("\\|"));
					
					browserVersionsIt = browserVersions.iterator();
					platformsIt = platforms.iterator();
					
					while (browsersIt.hasNext() && browserVersionsIt.hasNext() && platformsIt.hasNext()) {
						browser = browsersIt.next();
						browserVersion = browserVersionsIt.next();
						platform = platformsIt.next();
						driverInitilizeInfo = browser + "&" + browserVersion + "&" + platform;
						dataToBeReturned.add(new Object[] { driverInitilizeInfo });
					}
				}
			}


		} else {
			// local to local test execution and also handling parallel support
			browsers = Arrays.asList(context.getCurrentXmlTest().getParameter("browserName").split(","));

			if(isUAEnabled){
				deviceNames = (System.getProperty("deviceName") != null) ? Arrays.asList(System.getProperty("deviceName").split("\\|")) : Arrays.asList(configProperty.getProperty("deviceName").split("\\|"));
				deviceNameIt = deviceNames.iterator();
			}
			browsersIt = browsers.iterator();

			while(browsersIt.hasNext()){
				browser = browsersIt.next();

				/*if(browser.contains("_"))
					browser = browser.split("\\_")[0];*/
				if(isUAEnabled && configProperty.getProperty("deviceBrowser").contains(browser.split("\\-")[0])){
					while(deviceNameIt.hasNext()){
						deviceName = deviceNameIt.next();
						driverInitilizeInfo = browser + "&" + deviceName;
						dataToBeReturned.add(new Object[] { driverInitilizeInfo });
					}
				}else				
					dataToBeReturned.add(new Object[] { browser });
			}

		}
		return dataToBeReturned.iterator();
	}

	/**
	 * To provide list of instance to System class to start threads from browser data provider file
	 * @param context - ITestContext
	 * @return Iterator for list of Objects with Browser combos specifications
	 * @throws IOException - IOException
	 */
	@DataProvider(parallel = true)
	public static Iterator<Object[]> multiDataIterator(ITestContext context) throws IOException {

		File dir1 = new File(".");
		String strBasePath = null;
		String browserInputFile = null;
		String paymentTypeInputFile = null;
		strBasePath = dir1.getCanonicalPath();

		List<String> websites = Arrays.asList(context.getCurrentXmlTest().getParameter("webSite").split(","));
		browserInputFile = strBasePath + File.separator + "src" + File.separator + "main" + File.separator + "resources"
				+ File.separator + "DataProviders" + File.separator
				+ context.getCurrentXmlTest().getParameter("browserDataProvider");
		paymentTypeInputFile = strBasePath + File.separator + "src" + File.separator + "main" + File.separator
				+ "resources" + File.separator + "DataProviders" + File.separator
				+ context.getCurrentXmlTest().getParameter("paymentDataProvider");

		// Get a list of String file content (line items) from the test file.
		List<String> browserList = getFileContentList(browserInputFile);
		List<String> paymentTypes = getFileContentList(paymentTypeInputFile);

		// We will be returning an iterator of Object arrays so create that
		// first.
		List<Object[]> dataToBeReturned = new ArrayList<Object[]>();

		// Populate our List of Object arrays with the file content.
		for (String website : websites) {
			for (String browser : browserList) {
				for (String payment : paymentTypes)
					dataToBeReturned.add(new Object[] { browser, website, payment });
			}
		}

		// return the iterator - testng will initialize the test class and calls
		// the
		// test method with each of the content of this iterator.
		return dataToBeReturned.iterator();

	}// multiDataIterator

	/**
	 * Utility method to get the file content in UTF8
	 * 
	 * @param filenamePath
	 * @return
	 */
	private static List<String> getFileContentList(String filenamePath) {
		List<String> lines = new ArrayList<String>();
		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filenamePath), "UTF8"));

			String strLine;
			while ((strLine = br.readLine()) != null) {
				lines.add(strLine);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lines;
	}

}