package com.fbb.support;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;




/**
 * Wrapper for Selenium WebDriver actions which will be performed on browser
 * 
 * Wrappers are provided with exception handling which throws Skip Exception on
 * occurrence of NoSuchElementException
 * 
 */

public class BrowserActions {



	public static String MOUSE_HOVER_JS = "var evObj = document.createEvent('MouseEvents');"
			+ "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
			+ "arguments[0].dispatchEvent(evObj);";
	public static String runPltfrm = Utils.getRunPlatForm();

	/**
	 * Wrapper to type a text in browser text field
	 * 
	 * @param txt
	 *            : WebElement of the Text Field
	 * @param txtToType
	 *            : Text to type [String]
	 * @param driver
	 *            : WebDriver Instances
	 * @param elementDescription
	 *            : Description about the WebElement
	 * @return String -
	 * @throws Exception - Exception
	 */
	public static String typeOnTextField(WebElement txt, String txtToType,
			WebDriver driver, String elementDescription) throws Exception {
		clearTextField(txt, elementDescription);
		txt.sendKeys(txtToType);
		txt.sendKeys(Keys.TAB);
		Log.event("Typed text '" + txtToType + "'" + " into " + elementDescription);
		return txtToType;
	}// typeOnTextField

	/**
	 * Wrapper to type a text in browser text field
	 * 
	 * @param txt
	 *            : WebElement of the Text Field
	 * @param txtToType
	 *            : Text to type [String]
	 * @param driver
	 *            : WebDriver Instances
	 * @param elementDescription
	 *            : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static void updateOnTextField(WebElement txt, String txtToType,
			WebDriver driver, String elementDescription) throws Exception {

		/*if (!Utils.waitForElement(driver, txt))
			throw new Exception(elementDescription
					+ " field not found in page!!");*/

		try {
			txt.click();
			txt.sendKeys(txtToType);
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " field not found in page!!");

		}

	}// typeOnTextField


	/**
	 * To clear text from text field
	 * @param field - Web Element for text field
	 * @param elementDescription - Web Element description
	 * @throws Exception - Exception
	 */
	public static void clearTextField(WebElement field, String elementDescription)throws Exception{
		try{
			field.clear();
		} catch(NoSuchElementException e){
			throw new Exception(elementDescription + " field not found in page!!");
		}
	}

	/**
	 * Wrapper to type a text in browser text field
	 * 
	 * @param txt
	 *            : String Input (CSS Locator)
	 * @param txtToType
	 *            : Text to type [String]
	 * @param driver
	 *            : WebDriver Instances
	 * @param elementDescription
	 *            : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static void typeOnTextField(String txt, String txtToType,
			WebDriver driver, String elementDescription) throws Exception {

		WebElement element = checkLocator(driver, txt);
		/*if (!Utils.waitForElement(driver, element, 1))
			throw new Exception(elementDescription
					+ " field not found in page!!");*/

		try {
			clearTextField(element, elementDescription);
			element.click();
			element.sendKeys(txtToType);
			element.sendKeys(Keys.TAB);
			Log.event("Typed(" + txtToType + ") in " + elementDescription);
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription
					+ " field not found in page!!");
		} catch (WebDriverException e){
			element.sendKeys(txtToType);
		}

	}// typeOnTextField
	/**
	 * To scroll into particular element
	 * 
	 * @param driver -
	 * @param element - the element to scroll to
	 * @throws InterruptedException -
	 */
	public static void scrollToView(WebElement element, WebDriver driver) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
	}

	/**
	 * To scroll to given element in page
	 * @param element - Web Element
	 * @param driver - Web Driver
	 * @throws InterruptedException - Exception
	 */
	public static void scrollInToView(WebElement element, WebDriver driver)
			throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
	}

	/**
	 * To scroll to top of the page
	 * @param driver - Web Driver
	 * @throws InterruptedException - Exception
	 */
	public static void scrollToTopOfPage(WebDriver driver)
			throws InterruptedException {
		WebElement element = driver.findElement(By.cssSelector("#header"));
		scrollToViewElement(element, driver);
	}

	/**
	 * To scroll to top of the page
	 * @param driver - Web Driver
	 * @throws InterruptedException - Exception
	 */
	public static void scrollUp(WebDriver driver)
			throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0, -100)", "");
	}

	/**
	 * To scroll to bottom of the page
	 * @param driver - Web Driver
	 * @throws InterruptedException - Exception
	 */
	public static void scrollToBottomOfPage(WebDriver driver)
			throws InterruptedException {
		//((JavascriptExecutor) driver).executeScript("window.scrollTo(0,document.body.scrollHeight)", "");
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	/**
	 * To scroll into particular element
	 * 
	 * @param driver
	 *            -
	 * @param element
	 *            - the element to scroll to
	 */
	public static void scrollToViewElement(WebElement element, WebDriver driver) {
		try {
			String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
					+ "var elementTop = arguments[0].getBoundingClientRect().top;"
					+ "window.scrollBy(0, elementTop-(viewPortHeight/2));";
			((JavascriptExecutor) driver).executeScript(scrollElementIntoMiddle, element);
			Log.event("Scrolled element to view.");
		} catch (Exception ex) {
		}
	}

	/**
	 * count elements
	 * @param xpath -
	 * @param driver -
	 * @return int -
	 */
	public static int countElements(String xpath, WebDriver driver) {
		return driver.findElements(By.xpath(xpath)).size();
	}

	/**
	 * To click on element
	 * @param btn - button
	 * @param driver - driver
	 * @param elementDescription - element description
	 * @throws Exception - Exception
	 */
	public static void clickOnElementX(WebElement btn, WebDriver driver, String elementDescription) throws Exception {
		try {
			BrowserActions.scrollToViewElement(btn, driver);
			String browser = Utils.getRunBrowser(driver);
			if((browser.equals("chrome")) && (Utils.isDesktop() || Utils.isTablet()))
				chromeClick(btn, elementDescription);
			else if(browser.equals("safari")){
				safariClick(btn, driver, elementDescription);
			}else 
				javascriptClick(btn, driver, elementDescription);
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException(elementDescription + " button/link not found in page!!");
		} catch(ElementNotVisibleException e2){
			javascriptClick(btn, driver, elementDescription);
		} catch (WebDriverException e1){
			if(e1.getMessage().contains("Other element would receive the click"))
				javascriptClick(btn, driver, elementDescription);
			else
				throw e1;
		} 

		Log.event("Clicked on " + elementDescription);

	}// clickOnButton
	
	/**
	 * To click on an element from script
	 * @param driver - driver
	 * @param element - element
	 * @param obj - page object where element instance is created in
	 * @throws Exception - Exception 
	 */
	public static void clickOnElementX(WebDriver driver, String element, Object obj, String elementDescription)throws Exception{
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		clickOnElementX(webElement, driver, elementDescription);
		Utils.waitForPageLoad(driver);
	}

	/**
	 * Dedicated method for web element click
	 * @param element - 
	 * @param elementDescription - 
	 * @throws Exception - 
	 */
	public static void chromeClick(WebElement element, String elementDescription)throws Exception{
		element.click();
	}

	/**
	 * Dedicated method for Safari click
	 * @param element - 
	 * @param driver -
	 * @param elementDescription -
	 * @throws Exception -
	 */
	public static void safariClick(WebElement element, WebDriver driver, String elementDescription)throws Exception{
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	/**
	 * Wrapper to click on button/text/radio/checkbox in browser
	 * 
	 * @param btn
	 *            : String Input (CSS Locator) [of the Button Field]
	 * @param driver
	 *            : WebDriver Instances
	 * @param elementDescription
	 *            : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static void clickOnElement(String btn, WebDriver driver,
			String elementDescription) throws Exception {

		WebElement element = checkLocator(driver, btn);
		/*if (!Utils.waitForElement(driver, element, 1))
			throw new Exception(elementDescription + " not found in page!!");*/

		try {
			element.click();
			Log.event("Clicked on " + elementDescription);
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}

	}// clickOnButton

	/**
	 * Wrapper to click on button/text/radio/checkbox in browser
	 * 
	 * @param btn
	 *            : String Input (CSS Locator) [of the Button Field]
	 * @param driver
	 *            : WebDriver Instances
	 * @param elementDescription
	 *            : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static void clickOnElement(WebElement btn, WebDriver driver,
			String elementDescription) throws Exception {

		/*if (!Utils.waitForElement(driver, btn, 1))
			throw new Exception(elementDescription + " not found in page!!");*/

		try {
			scrollToViewElement(btn, driver);
			btn.click();
			Log.event("Clicked on " + elementDescription);
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}

	}// clickOnButton

	/**
	 * To click on given element suing actions
	 * @param element -
	 * @param driver -
	 * @param elementDescription -
	 * @throws Exception - Exception
	 */
	public static void actionClick(WebElement element, WebDriver driver,
			String elementDescription) throws Exception {
		/*if (!Utils.waitForElement(driver, element, 5))
			throw new Exception(elementDescription + " not found in page!!");*/

		try {
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click(element).build().perform();
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}
	}

	/**
	 * To click and hold the click on given element
	 * @param element -
	 * @param driver -
	 * @param elementDescription -
	 * @throws Exception - Exception
	 */
	public static void clickAndHoldOnElement(WebElement element, WebDriver driver,
			String elementDescription) throws Exception {
		/*if (!Utils.waitForElement(driver, element, 5))
			throw new Exception(elementDescription + " not found in page!!");*/

		try {
			Actions actions = new Actions(driver);
			actions.clickAndHold(element).build().perform();
			//javascriptClick(element, driver, elementDescription);
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}
	}

	/**
	 * To click on an element using Javascript
	 * @param element -
	 * @param driver -
	 * @param elementDescription -
	 * @throws Exception - Exception
	 */
	public static void javascriptClick(WebElement element, WebDriver driver,
			String elementDescription) throws Exception {

		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);

			String pltFrm = Utils.getRunPlatForm();
			String browser = Utils.getRunBrowser(driver);
			Log.event("Clicked on "+elementDescription);
			if(pltFrm.equals("desktop") && !(browser.equals("safari") || browser.equals("firefox"))){
				Actions action = new Actions(driver);
				action.moveByOffset(10, 10).build().perform();
			}
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}
	}

	/**
	 * Wrapper to get a text from the provided WebElement
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @param fromWhichTxtShldExtract
	 *            : WebElement from which text to be extract in String format
	 * @return: String - text from web element
	 * @param elementDescription
	 *            : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static String getText(WebDriver driver,
			WebElement fromWhichTxtShldExtract, String elementDescription)
					throws Exception {
		String textFromHTMLAttribute = new String();
		try {
			textFromHTMLAttribute = fromWhichTxtShldExtract.getText().trim();

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = fromWhichTxtShldExtract.getAttribute("textContent");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = fromWhichTxtShldExtract.getAttribute("value");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = fromWhichTxtShldExtract.getAttribute("innerHTML");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = fromWhichTxtShldExtract.getText();

		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}

		Log.event("Value Extracted from Element :: " + textFromHTMLAttribute);
		return textFromHTMLAttribute;

	}// getText

	/**
	 * Wrapper to get a text from the provided WebElement
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @param fromWhichTxtShldExtract
	 *            : WebElement from which text to be extract in String format
	 * @return: String - text from web element
	 * @param elementDescription
	 *            : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static String getText(WebDriver driver,
			List<WebElement> fromWhichTxtShldExtract, String elementDescription)
					throws Exception {

		String textFromHTMLAttribute = " ";

		try {
			for(int i = 0; i < fromWhichTxtShldExtract.size(); i++){
				String textFromHTML = fromWhichTxtShldExtract.get(i).getText().trim() + System.lineSeparator();

				if (textFromHTML.isEmpty())
					textFromHTML += fromWhichTxtShldExtract.get(i).getAttribute("textContent") + System.lineSeparator();

				if (textFromHTML.isEmpty())
					textFromHTML += fromWhichTxtShldExtract.get(i).getAttribute("innerHTML") + System.lineSeparator();
				textFromHTMLAttribute += textFromHTML;
			}

		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}

		return textFromHTMLAttribute;

	}// getText

	/**
	 * Wrapper to get a text from the provided WebElement
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @param fromWhichTxtShldExtract
	 *            : WebElement from which text to be extract in String format
	 * @return: String - text from web element
	 * @param elementDescription
	 *            : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static List<String> getText(
			List<WebElement> fromWhichTxtShldExtract, String elementDescription, WebDriver driver)
					throws Exception {

		List<String> textFromHTMLAttribute = new ArrayList<String>();

		try {
			for(int i = 0; i < fromWhichTxtShldExtract.size(); i++){
				String textFromHTML = fromWhichTxtShldExtract.get(i).getText().trim() + System.lineSeparator();

				if (textFromHTML.isEmpty())
					textFromHTML += fromWhichTxtShldExtract.get(i).getAttribute("textContent") + System.lineSeparator();

				if (textFromHTML.isEmpty())
					textFromHTML += fromWhichTxtShldExtract.get(i).getAttribute("innerHTML") + System.lineSeparator();
				//textFromHTMLAttribute += textFromHTML;
				textFromHTMLAttribute.add(textFromHTML);
			}

		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}

		return textFromHTMLAttribute;

	}// getText

	/**
	 * Wrapper to get a text from the provided WebElement
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @param fromWhichTxtShldExtract
	 *            : String Input (CSS Locator) [from which text to be extract in
	 *            String format]
	 * @return: String - text from web element
	 * @param elementDescription
	 *            : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static String getText(WebDriver driver,
			String fromWhichTxtShldExtract, String elementDescription)
					throws Exception {

		String textFromHTMLAttribute = "";

		WebElement element = checkLocator(driver, fromWhichTxtShldExtract);

		try {
			textFromHTMLAttribute = element.getText().trim();

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = element.getAttribute("textContent");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = element.getAttribute("value");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = element.getAttribute("innerHTML");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = element.getText();

		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}

		return textFromHTMLAttribute;

	}// getText

	/**
	 * Wrapper to get a text from the provided WebElement's Attribute
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @param fromWhichTxtShldExtract
	 *            : WebElement from which text to be extract in String format
	 * @param attributeName
	 *            : Attribute Name from which text should be extracted like
	 *            "style, class, value,..."
	 * @return: String - text from web element
	 * @param elementDescription
	 *            : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static String getTextFromAttribute(WebDriver driver,
			WebElement fromWhichTxtShldExtract, String attributeName,
			String elementDescription) throws Exception {

		String textFromHTMLAttribute = "";

		try {
			textFromHTMLAttribute = fromWhichTxtShldExtract.getAttribute(
					attributeName).trim();
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}

		return textFromHTMLAttribute;

	}// getTextFromAttribute

	/**
	 * Wrapper to get a text from the provided WebElement's Attribute
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @param fromWhichTxtShldExtract
	 *            : String Input (CSS Locator) [from which text to be extract in
	 *            String format]
	 * @param attributeName
	 *            : Attribute Name from which text should be extracted like
	 *            "style, class, value,..."
	 * @return: String - text from web element
	 * @param elementDescription
	 *            : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static String getTextFromAttribute(WebDriver driver,
			String fromWhichTxtShldExtract, String attributeName,
			String elementDescription) throws Exception {

		String textFromHTMLAttribute = "";
		WebElement element = checkLocator(driver, fromWhichTxtShldExtract);

		try {
			textFromHTMLAttribute = element.getAttribute(attributeName).trim();
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}

		return textFromHTMLAttribute;

	}// getTextFromAttribute

	/**
	 * Wrapper to select option from combobox in browser and doesn't wait for
	 * spinner to disappear
	 * 
	 * @param btn
	 *            : String Input (CSS Locator) [of the ComboBox Field]
	 * 
	 * @param optToSelect
	 *            : option to select from combobox
	 * 
	 * @param driver
	 *            : WebDriver Instances
	 * @param elementDescription
	 *            : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static void selectFromComboBox(String btn, String optToSelect,
			WebDriver driver, String elementDescription) throws Exception {

		WebElement element = checkLocator(driver, btn);
		/*if (!Utils.waitForElement(driver, element, 1))
			throw new Exception(elementDescription + " not found in page!!");*/

		try {
			Select selectBox = new Select(element);
			selectBox.selectByValue(optToSelect);
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}

	}// selectFromComboBox

	/**
	 * Wrapper to select option from combobox in browser
	 * 
	 * @param btn
	 *            : WebElement of the combobox Field
	 * 
	 * @param optToSelect
	 *            : option to select from combobox
	 * 
	 * @param driver
	 *            : WebDriver Instances
	 * @param elementDescription
	 *            : Description about the WebElement
	 */
	public static void selectFromComboBox(WebElement btn, String optToSelect,
			WebDriver driver, String elementDescription) {

		/*if (!Utils.waitForElement(driver, btn, 1))
			throw new SkipException(elementDescription + " not found in page!!");*/

		try {
			Select selectBox = new Select(btn);
			selectBox.selectByValue(optToSelect);
		} catch (NoSuchElementException e) {
			throw new SkipException(elementDescription + " not found in page!!");
		}

	}// selectFromComboBox

	/**
	 * Select drop down value and doesn't wait for spinner.
	 * @param driver -
	 * @param dropDown -
	 * @param valueToBeSelected -
	 */
	public static void selectDropDownValue(WebDriver driver,
			WebElement dropDown, String valueToBeSelected) {
		dropDown.click();
		By valueBy = By.xpath("//li[@data-label='" + valueToBeSelected + "']");
		for (WebElement element : driver.findElements(valueBy)) {
			if (valueToBeSelected.equals(element.getText())
					&& element.isDisplayed()) {
				element.click();
				break;
			}
		}
	}

	/**
	 * To select the option which matches by visible text in dropdown
	 * @param element -
	 * @param value -
	 * @throws Exception - Exception
	 */
	public static void selectDropdownByValue(WebElement element, String value)throws Exception{
		Select select = new Select(element);
		select.selectByVisibleText(value);
	}

	/**
	 * To mouse hover on the given element
	 * @param driver -
	 * @param element -
	 * @param description -
	 * @throws Exception - Exception
	 */
	public static void mouseHover(WebDriver driver, WebElement element, String description)throws Exception {
		scrollToTopOfPage(driver);
		scrollToViewElement(element, driver);
		if((Utils.getRunBrowser(driver).equals("safari") && runPltfrm.equals("desktop")) || Utils.getRunBrowser(driver).equals("internet explorer") || Utils.getRunDevicePlatform(driver).toLowerCase().contains("mac")){
			if(System.getProperty(Utils.getCurrentTestName()).contains("1") || System.getProperty(Utils.getCurrentTestName()).contains("2"))
				moveToElementJS(driver, element);
		}else{
			Actions action = new Actions(driver);
			action.moveToElement(element).build().perform();
		}
		Log.event("Tried to Mouse hover on " + description);
	}

	/**
	 * To perform mouse hover on an element using javascript
	 * 
	 * @param driver -
	 * @param element -
	 */
	public static void moveToElementJS(WebDriver driver, WebElement element) {
		String mouseHoverJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');"
				+ "evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);}"
				+ " else if(document.createEventObject){ arguments[0].fireEvent('onmouseover');}";
		((JavascriptExecutor) driver).executeScript(mouseHoverJS, element);
	}

	/**
	 * To perform Mouse hover in IE browser
	 * @param driver -
	 * @param element -
	 */
	public static void javaScriptMouseHoverForIE(WebDriver driver, WebElement element) {
		String code = "var fireOnThis = arguments[0];" + "var evObj = document.createEvent('MouseEvents');"
				+ "evObj.initEvent( 'mouseover', true, true );" + "fireOnThis.dispatchEvent(evObj);";
		((JavascriptExecutor) driver).executeScript(code, element);
	}


	/**
	 * To choose/check a radio/check box
	 * @param element - Web Element of radio/check box
	 * @param enableOrDisable - To check - YES
	 * 							To Uncheck - NO
	 */
	public static void selectRadioOrCheckbox(WebElement element, String enableOrDisable) {

		if ("YES".equalsIgnoreCase(enableOrDisable)) {
			if (!(isRadioOrCheckBoxSelected(element))) {
				element.click();
			}
		}
		if ("NO".equalsIgnoreCase(enableOrDisable)) {
			if (isRadioOrCheckBoxSelected(element)) {
				element.click();
			}
		}
	}

	/**
	 * To select the radio / checkbox
	 * @param element -
	 * @param driver -
	 * @param enableOrDisable -
	 * @throws Exception - Exception
	 */
	public static void selectRadioOrCheckbox(WebElement element,WebDriver driver, String enableOrDisable) throws Exception{

		if ("YES".equalsIgnoreCase(enableOrDisable)) {
			if (!(isRadioOrCheckBoxSelected(element))) {
				clickOnElementX(element, driver, "Checkbox/Radio Button");;
			}
		}
		if ("NO".equalsIgnoreCase(enableOrDisable)) {
			if (isRadioOrCheckBoxSelected(element)) {
				clickOnElementX(element, driver, "Checkbox/Radio Button");
			}
		}
	}

	/**
	 * To select the radio / checkbox
	 * @param element -
	 * @param enableOrDisable -
	 * @param driver -
	 */
	public static void selectRadioOrCheckbox(WebElement element, String enableOrDisable, WebDriver driver)throws Exception {

		if(Utils.getRunBrowser(driver).toLowerCase().contains("edge")){
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].style.opacity=1", element); 
		}else{
			BrowserActions.moveToElementJS(driver, element);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].style.opacity=1", element); 

			if ("YES".equalsIgnoreCase(enableOrDisable)) {
				if (!(isRadioOrCheckBoxSelected(element))) {
					javascriptClick(element, driver, "element");
				}
			}
			if ("NO".equalsIgnoreCase(enableOrDisable)) {
				if (isRadioOrCheckBoxSelected(element)) {
					javascriptClick(element, driver, "element");
				}
			}
		}
	}

	/**
	 * To verify the radio / checkbox is selected or not
	 * @param element -
	 * @return true - if selected
	 */
	public static boolean isRadioOrCheckBoxSelected(WebElement element) {
		if (element.getCssValue("background").contains("checkmark")) {
			return true;
		}

		if (element.getCssValue("background").contains("checkbox")) {
			return false;
		}

		if (element.getAttribute("class").contains("active")) {
			return true;
		}

		if (null != element.getAttribute("checked")) {
			return true;
		}

		if(null != element.getAttribute("value"))
			if(element.getAttribute("value").equals("true"))
				return true;

		for (WebElement childElement : element.findElements(By.xpath(".//*"))) {
			if (childElement.getAttribute("class").contains("active")) {
				return true;
			}
		}

		return false;
	}

	/**
	 * To check whether locator string is xpath or css
	 * 
	 * @param driver -
	 * @param locator - 
	 * @return elements -
	 */
	public static List<WebElement> checkLocators(WebDriver driver,
			String locator) {
		List<WebElement> elements = null;
		if (locator.startsWith("//")) {
			elements = (new WebDriverWait(driver, 10).pollingEvery(500,
					TimeUnit.MILLISECONDS).ignoring(
							NoSuchElementException.class,
							StaleElementReferenceException.class)
					.withMessage("Couldn't find " + locator))
					.until(ExpectedConditions
							.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
		} else {
			elements = (new WebDriverWait(driver, 10).pollingEvery(500,
					TimeUnit.MILLISECONDS).ignoring(
							NoSuchElementException.class,
							StaleElementReferenceException.class)
					.withMessage("Couldn't find " + locator))
					.until(ExpectedConditions
							.visibilityOfAllElementsLocatedBy(By
									.cssSelector(locator)));
		}
		return elements;
	}

	/**
	 * To check whether locator string is xpath or css
	 * 
	 * @param driver -
	 * @param locator -
	 * @return element -
	 */
	public static WebElement checkLocator(WebDriver driver, String locator) {
		WebElement element = null;
		if (locator.startsWith("//")) {
			element = (new WebDriverWait(driver, 10).pollingEvery(500,
					TimeUnit.MILLISECONDS).ignoring(
							NoSuchElementException.class,
							StaleElementReferenceException.class)
					.withMessage("Couldn't find " + locator))
					.until(ExpectedConditions.visibilityOfElementLocated(By
							.xpath(locator)));
		} else {
			element = (new WebDriverWait(driver, 10).pollingEvery(500,
					TimeUnit.MILLISECONDS).ignoring(
							NoSuchElementException.class,
							StaleElementReferenceException.class)
					.withMessage("Couldn't find " + locator))
					.until(ExpectedConditions.visibilityOfElementLocated(By
							.cssSelector(locator)));
		}
		return element;
	}

	/**
	 * To get matching text element from List of web elements
	 * 
	 * @param elements
	 *            -
	 * @param contenttext
	 *            - text to match
	 * @param condition -
	 * @return elementToBeReturned as WebElement
	 * @throws Exception - Exception
	 */
	public static WebElement getMachingTextElementFromList(
			List<WebElement> elements, String contenttext, String condition)
					throws Exception {
		WebElement elementToBeReturned = null;
		boolean found = false;
		if (elements.size() > 0) {
			for (WebElement element : elements) {
				if (condition.toLowerCase().equals("equals")
						&& element.getText().trim().replaceAll("\\s+", " ")
						.equalsIgnoreCase(contenttext)) {
					elementToBeReturned = element;
					found = true;
					break;
				}

				if (condition.toLowerCase().equals("contains")
						&& element.getText().trim().replaceAll("\\s+", " ")
						.contains(contenttext)) {
					elementToBeReturned = element;
					found = true;
					break;
				}
			}
			if (!found) {
				throw new Exception("Didn't find the correct text("
						+ contenttext + ")..! on the page");
			}
		} else {
			throw new Exception("Unable to find list element...!");
		}
		return elementToBeReturned;
	}

	/**
	 * Open a new tab on the browser
	 * 
	 * @param driver -
	 */
	public static void openNewTab(WebDriver driver) {
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
	}

	/**
	 * To navigate to back to previous page
	 * @param driver -
	 */
	public static void navigateToBack(WebDriver driver) {
		driver.navigate().back();
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To select dropdown by index
	 * @param element -
	 * @param index -
	 */
	public static void selectDropdownByIndex(WebElement element, int index) {
		Select selectByIndex = new Select(element);
		selectByIndex.selectByIndex(index);

	}

	/**
	 * To get select option from dropdown
	 * @param element -
	 * @return String -
	 */
	public static String getSelectedOption(WebElement element) {
		Select selectByIndex = new Select(element);
		WebElement ele = selectByIndex.getFirstSelectedOption();
		return ele.getText().trim();
	}

	/**
	 * To verify given elements are same
	 * @param driver -
	 * @param element1 -
	 * @param element2 -
	 * @return Boolean -
	 * @throws Exception - Exception
	 */
	public static Boolean verifyElementsAreInSameRow(WebDriver driver,
			WebElement element1, WebElement element2)
					throws Exception {
		
		Point ele1 = element1.getLocation();
		Point ele2 = element2.getLocation();
		
		Log.event("Element1 Position :: " + ele1);
		Log.event("Element2 Position :: " + ele2);
		
		if(ele1.y == ele2.y)
			return true;
		else
			return false;
	}

	/**
	 * Wrapper to verify if the first element is above the second element in the UI
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @param elementAbove
	 *            : WebElement which is present above in the UI
	 * @return: Boolean
	 * @param elementBelow
	 *            : WebElement which is present below in the UI
	 * @throws Exception - Exception
	 */
	public static Boolean verifyVerticalAllignmentOfElements(WebDriver driver,
			WebElement elementAbove, WebElement elementBelow)
					throws Exception {
		scrollToTopOfPage(driver);
		Point eleAbove = elementAbove.getLocation();
		Point eleBelow = elementBelow.getLocation();
		if(eleAbove.y < eleBelow.y)
			return true;
		else
			return false;
	}

	/**
	 * Wrapper to verify if the first element is right to the second element in the UI
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @param elementRight
	 *            : WebElement which is present right side in the UI
	 * @return: Boolean
	 * @param elementLeft
	 *            : WebElement which is present left side in the UI
	 * @throws Exception - Exception
	 */
	public static Boolean verifyHorizontalAllignmentOfElements(WebDriver driver,
			WebElement elementRight, WebElement elementLeft)throws Exception {
		Point eleRight = elementRight.getLocation();
		Point eleLeft = elementLeft.getLocation();

		Log.event(eleLeft.x + "-->" + eleRight.x);
		if(eleLeft.x < eleRight.x)
			return true;
		else
			return false;
	}

	/**
	 * To refresh page
	 * @param driver -
	 * @throws Exception - Exception
	 */
	public static void refreshPage(WebDriver driver)throws Exception{
		driver.navigate().refresh();
		BrowserActions.scrollToTopOfPage(driver);
	}

	/**
	 * To vertically click on outside of a modal/popup
	 * @param driver - driver
	 * @param element - element
	 * @param obj - obj
	 * @throws Exception - Exception
	 */
	public static void clickOnEmptySpaceVertical(WebDriver driver, String element, Object obj)throws Exception{
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		
		if(Utils.getRunBrowser(driver).equalsIgnoreCase("Safari")) {
			try {
				int clickLocationX= webElement.getLocation().x + webElement.getSize().getWidth()/2;
				int clickLocationY= webElement.getLocation().y + webElement.getSize().getHeight() + 20;
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("document.elementFromPoint(arguments[0], arguments[1]).click();", clickLocationX, clickLocationY);
			}catch(Exception e) {
				int x = webElement.getLocation().getX();
				int y = webElement.getLocation().getY() + 10;
				String code = "document.elementFromPoint(" + x + "," + y + ").click();";
				((JavascriptExecutor) driver).executeScript(code);
			}
        }
		else {
			int clickLocationX= webElement.getLocation().x + webElement.getSize().getWidth()/2;
			int clickLocationY= webElement.getLocation().y + webElement.getSize().getHeight() + 20;
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("document.elementFromPoint(arguments[0], arguments[1]).click();", clickLocationX, clickLocationY);
		}
     }
	
	/**
	 * To horizontally click on outside of a modal/popup
	 * @param driver - driver
	 * @param element - element
	 * @param obj - obj
	 * @throws Exception - Exception
	 */
	public static void clickOnEmptySpaceHorizontal(WebDriver driver, String element, Object obj)throws Exception{
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		
		if(Utils.getRunBrowser(driver).equalsIgnoreCase("Safari")) {
			try {
				int clickLocationX= webElement.getLocation().x + webElement.getSize().getWidth() + 20;
				int clickLocationY= webElement.getLocation().y + webElement.getSize().getHeight()/2;
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("document.elementFromPoint(arguments[0], arguments[1]).click();", clickLocationX, clickLocationY);
			}catch(Exception e) {
				int x = webElement.getLocation().getX() - 10;
				int y = webElement.getLocation().getY();
				String code = "document.elementFromPoint(" + x + "," + y + ").click();";
				((JavascriptExecutor) driver).executeScript(code);
			}
        }
		else{
        	int clickLocationX= webElement.getLocation().x + webElement.getSize().getWidth() + 20;
    		int clickLocationY= webElement.getLocation().y + webElement.getSize().getHeight()/2;
    		JavascriptExecutor executor = (JavascriptExecutor) driver;
    		executor.executeScript("document.elementFromPoint(arguments[0], arguments[1]).click();", clickLocationX, clickLocationY);
        }
     }


	/**
	 * To get the location of an  element
	 * @param element -
	 * @param obj -
	 * @param coordinate -
	 * @return int -
	 * @throws Exception - Exception
	 */
	public static int getLocation(String element,Object obj, String coordinate)throws Exception{

		Field f1 = obj.getClass().getDeclaredField(element);
		f1.setAccessible(true);
		WebElement innerElement = ((WebElement) f1.get(obj));

		Point point = innerElement.getLocation();
		if(coordinate.toLowerCase().equals("x"))
			return point.getX();
		else
			return point.getY();	

	}

	/**
	 * To get the RGB value of an element
	 * @param element -
	 * @param obj -
	 * @return Test Color 
	 * @throws Exception - Exception
	 */
	public static String getRGBvalue(WebElement element)throws Exception {
		String textcolor=element.getCssValue("color");
		return textcolor;
	}

	/**
	 * To delete cookies
	 * @param driver -
	 * @throws Exception -
	 */
	public static void clearCookies(WebDriver driver) throws Exception{
		driver.manage().deleteAllCookies();
	}

	/**
	 * To mouse hover on element using actions
	 * @param driver -
	 * @param element -
	 */
	public static void mouseHover(WebDriver driver, WebElement element) {
		Actions actions = new Actions(driver);
		actions.moveToElement(element).build().perform();
	}

	/**
	 * To mouse hover on element using java script
	 * @param driver -
	 * @param element -
	 */
	public static void javaScriptMouseHover(WebDriver driver, WebElement element) {
		String javaScript =
				"var evObj = document.createEvent('MouseEvents');" +
						"evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
						+ "arguments[0].dispatchEvent(evObj);"; 
		((JavascriptExecutor) driver).executeScript(javaScript, element);
	}
	
	/**
	 * To verify if element(s) are displayed in page
	 * @param driver - driver
	 * @param expectedElements - expectedElements
	 * @return boolean - true/false if all elements are displayed
	 */
	public static boolean VerifyElementDisplayed(WebDriver driver, List<WebElement> expectedElements) {
		List<WebElement> actualElements = new ArrayList<WebElement>();
		for (WebElement expEle : expectedElements) {
			try {
				if (expEle.isDisplayed()) {
					actualElements.add(expEle);
				}
			} catch (NoSuchElementException e){
				Log.event(e.getMessage());
				Log.event("Element not present in Page :: " + expEle);
			}
		}
		return actualElements.equals(expectedElements);
	}

}// BrowserActions