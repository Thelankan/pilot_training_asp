package com.fbb.support;

import java.util.HashMap;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import com.utils.testrail.TestRailAPICalls;

public class TestRailSupportUtils {

	//private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();

	public static HashMap<String, String> TCSUMMARY_MAPPING = new HashMap<String, String>();
	public static HashMap<String, String> TRCASEID_MAPPING = new HashMap<String, String>();
	public static HashMap<String, Integer> STATUS_MAPPING = new HashMap<String, Integer>();
	public static HashMap<String, String> COMMENT_MAPPING = new HashMap<String, String>();
	public static HashMap<String, String> DEFECT_MAPPING = new HashMap<String, String>();
	public static HashMap<String, String> DEFECT_COMMENT_MAPPING = new HashMap<String, String>();
	public static HashMap<Integer, String> RUNID_MAPPING = new HashMap<Integer, String>();
	public static HashMap<String, Integer> HASHCODE_MAPPING = new HashMap<String, Integer>();
	public static HashMap<Integer, String> SAUCE_SESSION_MAPPING = new HashMap<Integer, String>();

	@SuppressWarnings("rawtypes")
	public static HashMap<String, HashMap<Integer, HashMap>> STEP_RESULT_MAPPING = new HashMap<String, HashMap<Integer, HashMap>>();

	/**
	 * To add test case details into test rail
	 */
	/*public static void addTestRailResultForCase(){
		try{
			HashMap<String, String> bugInfo = TestDataExtractor.initTestData("jira\\jiraTestData.xls", "defects");
			String defectID = bugInfo.get("BugId").contains("|") ? bugInfo.get("BugId").split("\\|")[bugInfo.get("BugId").split("\\|").length - 1] :
				bugInfo.get("BugId");
			int status = TestRailSupportUtils.STATUS_MAPPING.get(Integer.toString(Utils.getCurrentTestCode()));
			new TestRailAPICalls().addResultForCase(bugInfo.get("TRCaseID"), status, bugInfo.get("Comments"), defectID);
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}*/

	/**
	 * To Add Step wise result in TestRail
	 * @param stepNo - TestRail Case - Step Number
	 * @param actualResult - Result To Update
	 * @param status - TestRail Status code
	 * @throws Exception - Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void addStepResult(String stepNo, String actualResult, String status)throws Exception{
		HashMap<Integer, HashMap> result = new HashMap<Integer, HashMap>();
		if(STEP_RESULT_MAPPING.containsKey(Reporter.getCurrentTestResult().getName()))
			result = STEP_RESULT_MAPPING.get(Reporter.getCurrentTestResult().getName());

		HashMap stepResult = new HashMap();
		int status_id = 4;
		if(status.equalsIgnoreCase("pass"))
			status_id = 1;
		else if(status.equalsIgnoreCase("hold"))
			status_id = 3;
		else if(status.equalsIgnoreCase("fail"))
			status_id = 5;

		//stepResult.put("content","Step " + stepNo);
		stepResult.put("actual", actualResult);
		stepResult.put("status_id", status_id);
		result.put(Integer.parseInt(stepNo), stepResult);
		STEP_RESULT_MAPPING.put(Reporter.getCurrentTestResult().getName(), result);
	}

	/**
	 * To Map Sauce URL With Test Case Result
	 * @param sauceURL -
	 */
	public static void mapSauceSessionID(String sauceURL){
		try{
			SAUCE_SESSION_MAPPING.put(Utils.getCurrentTestCode(),sauceURL);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * To Map RUN ID from RUNID.xls file with Running Combo to Update result in TestRail
	 * @param driver -
	 * @return String -
	 */
	@SuppressWarnings("unchecked")
	public static String mapRunID(WebDriver driver){
		String OS_Platform;// = System.getProperty("SELENIUM_DRIVER").split("os=")[1].split("&")[0].replace(" ", "");
		String Device_Platform = Utils.getRunPlatForm();
		String Browser_Name = Utils.getRunBrowser(driver);
		String Brand = Utils.getCurrentBrandShort();

		if(Browser_Name.toLowerCase().contains("internet"))
			Browser_Name = "IE";
		if(Browser_Name.toLowerCase().contains("edge"))
			Browser_Name = "Edge";

		Log.event("Platform :: " + Device_Platform);
		
		if(Device_Platform.equals("tablet") || Device_Platform.equals("mobile")){
			Log.event("DeviceName :: " + Utils.getRunningDeviceName());
			//if(System.getProperty("deviceName" + Utils.getCurrentTestCode()) != null){
			if(Utils.getRunningDeviceName() != null){
				//if(System.getProperty("deviceName" + Utils.getCurrentTestCode()).contains("android"))
				if(Utils.getRunningDeviceName().contains("android"))
					OS_Platform = "android";
				else
					OS_Platform = "mac";
			}else{
				/*if(System.getProperty("MobileOS").contains("android"))	
					OS_Platform = "android";
				else
					OS_Platform = "mac";*/
				
				OS_Platform = System.getProperty(Device_Platform + Utils.getCurrentTestCode());
				Log.event("OS_Platform :: " + OS_Platform);
			}
		}else{
			if(System.getProperty("SELENIUM_DRIVER") != null)
				OS_Platform = System.getProperty("SELENIUM_DRIVER").split("os=")[1].split("&")[0].replace(" ", "");
			else if(System.getenv("SELENIUM_DRIVER") != null)
				OS_Platform = System.getenv("SELENIUM_DRIVER").split("os=")[1].split("&")[0].replace(" ", "");
			else
				OS_Platform = System.getProperty("os.name").replace(" ", "");
		}

		if(OS_Platform.toLowerCase().contains("mac"))
			OS_Platform = "mac";
		
		if(OS_Platform.toLowerCase().contains("windows2008"))
			OS_Platform = "windows7";

		String combo_name = Device_Platform.toUpperCase() + "-" + OS_Platform.toUpperCase() + "-" + Browser_Name.toUpperCase() + "-" + Brand.toUpperCase();
		//System.out.println("--->>>System passed here.................."+Browser_Name+".........."+OS_Platform);
		Log.event("Combo Name :: " + combo_name);
		
		String dropID = System.getProperty(Utils.getCurrentTestName());
		HashMap<String, String> combos_info = TestDataExtractor.initRunIDMapping("testdata\\RunID.xls", dropID,"Combination","RunID");
		String run_id = combos_info.get(combo_name);
		Log.event("RUN-ID :: " + run_id);

		RUNID_MAPPING.put(Utils.getCurrentTestCode(),run_id);
		HASHCODE_MAPPING.put(combo_name,Utils.getCurrentTestCode());

		return combo_name;
	}

	/**
	 * To Submit Result into TestRail
	 */
	public static void submitResult(){
		try{
			String tc_id = Utils.getCurrentTestName().split("\\_C")[1].toString().trim();
			int tc_status = STATUS_MAPPING.get(Integer.toString(Utils.getCurrentTestCode()));
			if(tc_status == 4){
				Log.message("Test Case failed due to Script Exception which might be caused by Application Issue (or) Some technical Issue.");
				Log.message("So Testrail Result will be updated as Re-Test.");
			}
			String tc_comment = COMMENT_MAPPING.get(Integer.toString(Utils.getCurrentTestCode()));
			//Log.message("TR Comments :: "+tc_comment);
			if(System.getProperty("embedSauceSession").equals("true"))
				tc_comment = "Sauce Session ID: " + SAUCE_SESSION_MAPPING.get(Utils.getCurrentTestCode()) + "  " + tc_comment; 
			
			String tc_bug = "";
			if(System.getProperty("runID").contains("[0-9]") || !System.getProperty("runID").isEmpty()){
				Log.message("Submitting Results to Testrail for Testcase :: " + tc_id + "  & RunID :: " + System.getProperty("runID"));
				new TestRailAPICalls().addResultForCase(tc_id, tc_status, tc_comment, tc_bug);
			}
			else{
				String run_id = RUNID_MAPPING.get(Utils.getCurrentTestCode());
				Log.message("Submitting Results to Testrail for Testcase :: " + tc_id + "  & RunID :: " + run_id);
				new TestRailAPICalls().addResultForCase(run_id,tc_id, tc_status, tc_comment, tc_bug);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * To Map user give description with Result like notes.
	 * @param hashCode - Running Test Hashcode
	 * @param description - Note to add with result
	 * @throws Exception - Exception
	 */
	public void mapComment(String hashCode, String description)throws Exception{
		if (System.getProperty("enableTestRailAPI").equals("true")) {
			TestRailSupportUtils.COMMENT_MAPPING.putIfAbsent(hashCode, "Test Initialization Success.");
			TestRailSupportUtils.COMMENT_MAPPING.put(hashCode, 
					TestRailSupportUtils.COMMENT_MAPPING.get(hashCode) + System.lineSeparator() +description);
		}
	}


}
