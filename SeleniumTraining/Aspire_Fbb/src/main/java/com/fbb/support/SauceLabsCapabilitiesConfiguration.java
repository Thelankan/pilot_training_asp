package com.fbb.support;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Class to set the DesiredCapabilties when working with SauceLabs.
 *
 *  https://docs.saucelabs.com/reference/test-configuration/
 *  for more info on the available properties.
 * 
 */
public class SauceLabsCapabilitiesConfiguration {
	private String browserName;
	private String platform;
	private String screenResolution;
	private String browserVersion;
	private String testName;
	private String build;
	private String deviceOrientation;
	private String preRun;
	private String seleniumVersion;
	private String iedriverVersion;
	private String chromedriverVersion;
	private String maxTestDuration;
	private String commandTimeout;
	private String idleTimeout;
	private String deviceName;

	private boolean recordVideo = true;
	private boolean recordNetwork = true;
	private boolean recordSnapshot = true;
	private boolean videoUploadOnPass = true;
	private boolean autoAcceptAlerts = true;

	private static MobileEmulationUserAgentConfiguration mobEmuUA = new MobileEmulationUserAgentConfiguration();

	/**
	 * Parameterized constructor to initialize object with specific configuration
	 * @param testName - Test Name
	 * @param build - Build ID
	 * @param browserName - Browser name
	 * @param browserVersion - Browser version
	 * @param platform - OS Platform
	 * @param screenResolution - Device Screen Resolution
	 */
	public SauceLabsCapabilitiesConfiguration(String testName, String build, String browserName, String browserVersion,
			String platform, String screenResolution) {
		this.testName = testName;
		this.build = build;
		this.browserName = browserName;
		this.platform = platform;
		this.browserVersion = browserVersion;
		this.screenResolution = screenResolution;

	}

	/**
	 * Parameterized constructor to initialize object with test parameters
	 * @param testName - Test Name
	 * @param build - Build ID
	 */
	public SauceLabsCapabilitiesConfiguration(String testName, String build) {
		this.testName = testName;
		this.build = build;
	}

	/**
	 * To get Test name
	 * @return String - Test Name
	 */
	public String getTestName() {
		return testName;
	}

	/**
	 * To set Test Name
	 * @param testName - Test Name
	 */
	public void setTestName(String testName) {
		this.testName = testName;
	}

	/**
	 * To get build information
	 * @return String - build info
	 */
	public String getBuild() {
		return build;
	}

	/**
	 * To set build information
	 * @param build - build info
	 */
	public void setBuild(String build) {
		this.build = build;
	}

	/**
	 * To get browser name
	 * @return String - browser name
	 */
	public String getBrowserName() {
		return browserName;
	}

	/**
	 * To set browser name
	 * @param browserName - Browser Name
	 */
	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}

	/**
	 * To get browser version
	 * @return String - Browser version
	 */
	public String getBrowserVersion() {
		return browserVersion;
	}

	/**
	 * To set browser version
	 * @param browserVersion - Browser Version
	 */
	public void setBrowserVersion(String browserVersion) {
		this.browserVersion = browserVersion;
	}

	/**
	 * To set selenium version
	 * @param seleniumVersion - Selenium version
	 */
	public void setSeleniumVersion(String seleniumVersion) {
		this.seleniumVersion = seleniumVersion;
	}

	/**
	 * To Set IE Driver version
	 * @param iedriverVersion - IE Driver version
	 */
	public void setIeDriverVersion(String iedriverVersion) {
		this.iedriverVersion = iedriverVersion;
	}

	/**
	 * To set chrome driver version
	 * @param chromedriverVersion - Chrome driver version
	 */
	public void setChromeDriverVersion(String chromedriverVersion) {
		this.chromedriverVersion = chromedriverVersion;
	}

	/**
	 * To set maximum test duration in minutes
	 * @param maxTestDuration - Maximum test duration in minutes
	 */
	public void setMaxTestDuration(String maxTestDuration) {
		this.maxTestDuration = maxTestDuration;
	}

	/**
	 * To get OS platform name
	 * @return String - Operating System platform name
	 */
	public String getPlatform() {
		return platform;
	}

	/**
	 * To set OS Platform name
	 * @param platform - OS Platform Name
	 */
	public void setPlatform(String platform) {
		this.platform = platform;
	}

	/**
	 * To get Screen Resolution from configuration
	 * @return String - screen resolution
	 */
	public String getScreenResolution() {
		return screenResolution;
	}

	/**
	 * To set screen resolution
	 * @param screenResolution - Screen Resolution
	 */
	public void setScreenResolution(String screenResolution) {
		this.screenResolution = screenResolution;
	}

	/**
	 * To verify video record is enabled/disabled
	 * @return boolean - true/false
	 */
	public boolean isRecordVideo() {
		return recordVideo;
	}

	/**
	 * To enable/disable video recoding option
	 * @param recordVideo - true/false
	 */
	public void setRecordVideo(boolean recordVideo) {
		this.recordVideo = recordVideo;
	}

	/**
	 * To verify network record is enabled/disabled
	 * @return boolean - true/false
	 */
	public boolean isRecordNetwork() {
		return recordNetwork;
	}

	/**
	 * To enable/disable network recording
	 * @param recordNetwork - true/false
	 */
	public void setRecordNetwork(boolean recordNetwork) {
		this.recordNetwork = recordNetwork;
	}

	/**
	 * To verify snapshot enable/disabled
	 * @return boolean - true/false
	 */
	public boolean isRecordSnapshot() {
		return recordSnapshot;
	}

	/**
	 * To enable/disable snapshot option
	 * @param recordSnapshot - true/false
	 */
	public void setRecordSnapshot(boolean recordSnapshot) {
		this.recordSnapshot = recordSnapshot;
	}

	/**
	 * To verify video upload on pass enabled/disabled
	 * @return boolean - true/false 
	 */
	public boolean isVideoUploadOnPass() {
		return videoUploadOnPass;
	}

	/**
	 * To enable/disable video upload on pass
	 * @param videoUploadOnPass - true/false
	 */
	public void setVideoUploadOnPass(boolean videoUploadOnPass) {
		this.videoUploadOnPass = videoUploadOnPass;
	}

	/* To handle and avoid unexpected alerts in iOS device */
	public void setautoAcceptAlerts(boolean autoAcceptAlerts) {
		this.autoAcceptAlerts = autoAcceptAlerts;
	}

	/**
	 * To get device orientation
	 * @return String - portrait/landscape
	 */
	public String getDeviceOrientation() {
		return deviceOrientation;
	}

	/**
	 * To set device orientation
	 * @param deviceOrientation - portrait/landscape
	 */
	public void setDeviceOrientation(String deviceOrientation) {
		this.deviceOrientation = deviceOrientation;
	}

	/**
	 * To get pre-run option
	 * @return String -
	 */
	public String getPreRun() {
		return preRun;
	}

	/**
	 * To set Pre-run option
	 * @param preRun -
	 */
	public void setPreRun(String preRun) {
		this.preRun = preRun;
	}

	/**
	 * To get timeout unit for command execution
	 * @return String - timeout value
	 */
	public String getCommandTimeout() {
		return commandTimeout;
	}

	/**
	 * To set command timeout value
	 * @param commandTimeout - 
	 */
	public void setCommandTimeout(String commandTimeout) {
		this.commandTimeout = commandTimeout;
	}

	/**
	 * To get idle timeout
	 * @return String - idle time out
	 */
	public String getIdleTimeout() {
		return idleTimeout;
	}

	/**
	 * To set idle time out
	 * @param idleTimeout - idle time out
	 */
	public void setIdleTimeout(String idleTimeout) {
		this.idleTimeout = idleTimeout;
	}

	/**
	 * To get device name
	 * @return String - device name
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * To set device name
	 * @param deviceName - device name
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	/**
	 * To get desired capabilities set to object
	 * @return DesiredCapabilities - 
	 */
	@SuppressWarnings("deprecation")
	public DesiredCapabilities getDesiredCapabilities() {
		DesiredCapabilities caps = new DesiredCapabilities();
		// Adding IE browser specific capabilities to sauce labs
		if (System.getProperty("SELENIUM_DRIVER") != null
				&& System.getProperty("SELENIUM_DRIVER").contains("internet explorer")) {
			caps = DesiredCapabilities.internetExplorer();
			caps.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
			caps.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			caps.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
			caps.setCapability("ignoreProtectedModeSettings", true);
			caps.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			caps.setCapability("options.EnablePersistentHover", false);
		}

		if (System.getProperty("SELENIUM_DRIVER") != null
				&& System.getProperty("SELENIUM_DRIVER").contains("firefox")) {
			caps = DesiredCapabilities.firefox();
			caps.setCapability("unexpectedAlertBehaviour", "ignore");
			caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			caps.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, "ignore");
			caps.setCapability("ACCEPT_UNTRUSTED_CERTIFICATES", "true");
			
		}
				
		caps.setCapability("name", testName);
		caps.setCapability("build", build);
		caps.setCapability("recordScreenshots", recordSnapshot);
		caps.setCapability("recordVideo", recordVideo);
		caps.setCapability("videoUploadOnPass", videoUploadOnPass);
		caps.setCapability("autoAcceptAlerts", autoAcceptAlerts);

		if (browserName != null) {
			caps.setBrowserName(browserName);
		}

		if (platform != null) {
			caps.setCapability(CapabilityType.PLATFORM, platform);
		}

		if (browserVersion != null) {
			caps.setVersion(browserVersion);
		}
		if (screenResolution != null) {
			caps.setCapability("screenResolution", screenResolution);
		}
		if (deviceOrientation != null) {
			caps.setCapability("deviceOrientation", deviceOrientation);
		}
		if (preRun != null) {
			caps.setCapability("prerun", preRun);
		}

		// To overriding the desired capabilities values depends upon the
		// version values, if we didn't mention then sauce labs will
		// take default values depends on the sauce lab settings like lower
		// version

		/* updating the selenium version */
		if (seleniumVersion != null) {
			caps.setCapability("seleniumVersion", seleniumVersion);
		}
		/* updating the ie server driver version */
		if (iedriverVersion != null) {
			caps.setCapability("iedriverVersion", iedriverVersion);
		}

		/* updating the chrome server driver version */
		if (chromedriverVersion != null) {
			caps.setCapability("chromedriverVersion", chromedriverVersion);
		}

		/* updating the test duration */
		if (maxTestDuration != null) {
			caps.setCapability("maxDuration", maxTestDuration);
		}

		if (commandTimeout != null) {
			caps.setCapability("commandTimeout", commandTimeout);
		}

		if (idleTimeout != null) {
			caps.setCapability("idleTimeout", idleTimeout);
		}

		if (deviceName != null) {
			caps.setCapability("deviceName", deviceName);
		}
		return caps;
	}

	/**
	 * To storing chrome mobile emulation configurations(width, height,
	 * pixelRatio and with sauce lab capabilities) and returning the
	 * capabilities
	 * 
	 * <p>
	 * if required feasible result then set relevant parameter from
	 * MobileEmulationUserAgentConfiguration()
	 * 
	 * @param caps
	 *            with sauce lab capabilities
	 * @param deviceName
	 *            name of the device needs to set user agent configuration
	 * @param userAgent
	 *            to passing the user agent string value depends on device
	 * @return uaCaps consist of sauce lab and user agent capabilities
	 */
	public DesiredCapabilities getUserAgentDesiredCapabilities(SauceLabsCapabilitiesConfiguration caps,
			String deviceName, String userAgent) {

		Map<String, Object> deviceMetrics = new HashMap<String, Object>();
		Map<String, Object> mobileEmulation = new HashMap<String, Object>();

		int width = 0;
		int height = 0;
		Double pixRatio = null;

		width = Integer.valueOf(mobEmuUA.getDeviceWidth(deviceName));
		height = Integer.valueOf(mobEmuUA.getDeviceHeight(deviceName));
		pixRatio = Double.valueOf(mobEmuUA.getDevicePixelRatio(deviceName));

		deviceMetrics.put("width", width);
		deviceMetrics.put("height", height);
		deviceMetrics.put("pixelRatio", pixRatio);
		mobileEmulation.put("deviceMetrics", deviceMetrics);
		mobileEmulation.put("userAgent", userAgent);
		Log.event("mobileEmulation settings::==> " + mobileEmulation);

		DesiredCapabilities uaCaps = DesiredCapabilities.chrome();
		ChromeOptions opt = new ChromeOptions();
		opt.setExperimentalOption("mobileEmulation", mobileEmulation);
		uaCaps = caps.setChromeOption(opt);
		return uaCaps;
	}

	/**
	 * Adding sauce lab capabilities to user agent configuration and returning
	 * the both capabilities
	 * 
	 * @param options
	 *            ChromeOptions
	 * @return sauceWithChromeCab returning the sauce lab capabilities with
	 *         chrome mobile emulation capabilities
	 */
	public DesiredCapabilities setChromeOption(ChromeOptions options) {
		DesiredCapabilities sauceWithChromeCab = this.getDesiredCapabilities();
		sauceWithChromeCab.setCapability(ChromeOptions.CAPABILITY, options);
		return sauceWithChromeCab;
	}
}
