package com.fbb.support;

import java.io.File;

import org.testng.ITestContext;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.utils.testrail.TestRailAPICalls;


public class BaseTest {

	private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
	protected static EnvironmentPropertiesReader prdData;

	{
		File outputDirectory = new File(new File("").getAbsolutePath() + File.separator + "test-output");
		if(outputDirectory.exists())
			FileUtils.deleteAllFilesFromFolder(outputDirectory);
	}

	/**
	 * To load pre-required parameters for suite level
	 */
	@BeforeSuite(alwaysRun = true)
	public static void setDefaultsSuite(){
		System.out.println("[BeforeSuite] Initializing System Parameters for Execution...");

		//To enable/disable User Agent mode
		if(System.getProperty("updateGSheet")==null) {
			System.setProperty("updateGSheet", configProperty.get("updateGSheet"));
		}else {
			System.setProperty("updateGSheet", System.getProperty("updateGSheet"));
		}
        System.out.println("[" + (System.getProperty("updateGSheet").equals("true") ? "enabled" : "disabled") + "]Update Google Sheet" );
		if(System.getProperty("updateGSheet").equals("true")){
			try {
				if (!SheetsQuickstart.validateReportDate()) {
					SheetsQuickstart.duplicateSheetFromTemplate();
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}

		//To enable/disable User Agent mode
		if(System.getProperty("runUserAgentDeviceTest")==null) {
			System.setProperty("runUserAgentDeviceTest", configProperty.getProperty("runUserAgentDeviceTest"));
		}else {
			System.out.println("[enabled]User-Agent Device-Test" );
		}

		//To enable/disable TestRail
		if(System.getProperty("enableTestRailAPI")==null) 
			System.setProperty("enableTestRailAPI", configProperty.getProperty("enableTestRailAPI"));
		else
			System.out.println("[enabled]TestRail" );

		//To Initiate TestRail Services
		new TestRailAPICalls();
		new TestDataWritter();

		//To enable Jira Integration
		//if(System.getProperty("enableJiraAPI")==null) System.setProperty("enableJiraAPI", configProperty.getProperty("enableJiraAPI"));

		//To enable embedding sauce url with testrail results.
		if(System.getProperty("embedSauceSession") == null) 
			System.setProperty("embedSauceSession", configProperty.getProperty("embedSauceSession"));
		else
			System.out.println("[enabled]Embedding Sauce Session into TestRail result" );

		//To Set Mobile OS property in System variable during execution from Jenkins
		if(System.getenv("SELENIUM_DRIVER") != null){
			if(System.getenv("SELENIUM_DRIVER").contains("os=ipad") || System.getenv("SELENIUM_DRIVER").contains("os=iphone")){
				System.setProperty("MobileOS", "mac");
			}
		}

	}

	/**
	 * To Load pre-required parameters for test level
	 * @param context -
	 * @return String - WebSite URL
	 */
	@BeforeTest(alwaysRun = true)
	public static String setDefaults(ITestContext context){
		System.out.println("[BeforeTest] Initializing test parameters...");
		//To set Testrail Run ID
		System.setProperty("runID", (System.getProperty("runID") != null ? System.getProperty("runID") : context.getCurrentXmlTest().getParameter("runID")));

		//To Initiate webSite URL with brand & environment
		String webSite = (System.getProperty("webSite") != null ? 
				System.getProperty("webSite") : context.getCurrentXmlTest().getParameter("webSite"));

		String brand = (System.getProperty("brand") != null ? 
				System.getProperty("brand") : context.getCurrentXmlTest().getParameter("brand"));

		String env = (System.getProperty("env") != null ? 
				System.getProperty("env") : context.getCurrentXmlTest().getParameter("env"));

		boolean isParallel = context.getSuite().getAllMethods().size() > 1 || brand.contains("|");

		//To make system properties in lower case
		brand = Brand.fromConfiguration(brand.toLowerCase()).name().toLowerCase();
		env = env.toLowerCase();
		webSite = webSite.toLowerCase();

		webSite = webSite.replace("!brand!", brand).replace("!env!", env);
		if(!isParallel){
			System.setProperty("webSite", webSite);
			System.setProperty("brand", brand);
			System.setProperty("env", env);
		}

		prdData = EnvironmentPropertiesReader.getInstance("data_" + brand);

		System.out.println("Total Number of Test :: "+context.getAllTestMethods().length);
		String dwre_id = new String();
		String product = new String();
		try{
			dwre_id = (System.getProperty("dwre_id") != null ? System
					.getProperty("dwre_id") : context.getCurrentXmlTest()
					.getParameter("dwre_id"));

			product = (System.getProperty("product") != null ? System
					.getProperty("product") : context.getCurrentXmlTest()
					.getParameter("product"));

		}catch(Exception e){

		}

		if(dwre_id !=null ) 
			System.setProperty("dwre_id", dwre_id);
		if(product !=null ){ 
			System.setProperty("product", product);
			System.setProperty("ProductSession", "false");
		}
		return webSite;
	}

	/**
	 * To load site brand if running multiple test in a single test suite
	 * @param context -
	 */
	//To Set defaults for WebSite & Brand when running brands in parallel from a single testng suite
	public static void setSiteBrand(ITestContext context){

		//To Initiate webSite URL with brand & environment
		String webSite = (System.getProperty("webSite") != null ? 
				System.getProperty("webSite") : context.getCurrentXmlTest().getParameter("webSite"));

		String brand = (System.getProperty("brand") != null ? 
				System.getProperty("brand") : context.getCurrentXmlTest().getParameter("brand"));

		String env = (System.getProperty("env") != null ? 
				System.getProperty("env") : context.getCurrentXmlTest().getParameter("env"));

		//To make system properties in lower case
		brand = Brand.fromConfiguration(brand.toLowerCase()).name().toLowerCase();
		env = env.toLowerCase();
		webSite = webSite.toLowerCase();

		//To construct webSite URL
		webSite = webSite.replace("!brand!", brand).replace("!env!", env);

		boolean isParallel = context.getSuite().getAllMethods().size() > 1 || brand.contains("|");

		if(isParallel){
			System.setProperty(Utils.getCurrentTestCode() + "_brand", brand);
			System.setProperty(Utils.getCurrentTestCode() + "_env", env);
			System.setProperty(Utils.getCurrentTestCode() + "_webSite", webSite);
		}

	}

}
