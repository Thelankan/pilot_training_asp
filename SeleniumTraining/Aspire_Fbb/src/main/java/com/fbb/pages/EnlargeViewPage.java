package com.fbb.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class EnlargeViewPage extends LoadableComponent<EnlargeViewPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public HomePage homePage;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	public MiniCartPage minicart;
	String runPltfrm = Utils.getRunPlatForm();

	/**********************************************************************************************
	 ********************************* WebElements of EnlargeViewPage Page ***********************************
	 **********************************************************************************************/


	@FindBy(css = "div[class='dialog-content ui-dialog-content ui-widget-content']")
	WebElement mdlEnlargeWindow;

	@FindBy(css = ".enlarge-container .product-name")
	WebElement txtProductName;

	@FindBy(css = ".ui-button-icon-primary.ui-icon.ui-icon-closethick")
	WebElement btnEnlargeViewClose_tablet;
	
	@FindBy(css = ".ui-button-icon.ui-icon.ui-icon-closethick")
	WebElement btnEnlargeViewClose;

	@FindBy(css = ".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.enlarge-dialog.ui-draggable .swatches.color")
	WebElement colorSwatches;

	@FindBy(css = ".enlarge-thumb-image .productthumbnail")
	WebElement alternateImages;

	@FindBy(css = ".enlarge-container .swatches.color li a img")
	List<WebElement> lstcolorSwatches;
	
	@FindBy(css = ".enlarge-container .swatches.color li")
	List<WebElement> lstcolorSwatchesLi;
	
	@FindBy(css = ".enlarge-container .swatches.color .selectable")
	List<WebElement> lstcolorSwatchesSelectable;

	@FindBy(css = ".enlarge-thumb-image.slick-initialized.slick-slider .slick-list.draggable div a[class='thumbnail-link'] img")
	List<WebElement> lstAlternateImages;
	
	@FindBy(css = ".enlarge-thumb-image .slick-current a.thumbnail-link img")
	WebElement imgCurrentAlternateImage;

	@FindBy(css = "div[id='enlargecontainer']>div[id='pdpMain'] .float-left.enlarge-hero-image div div div .thumb.slick-slide.slick-current.slick-active>a>img")
	WebElement imgMainProduct;
	
	@FindBy(css = ".enlarge-hero-image .slick-active img")
	WebElement imgMainProduct_Tablet;
	
	@FindBy(css = ".enlarge-thumb-image.slick-initialized.slick-slider .slick-list.draggable div .thumb.slick-slide.slick-current.slick-active")
	WebElement imgSelectedAlternateImage;

	@FindBy(css = ".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close.ui-state-focus")
	WebElement btnClose;

	@FindBy(css = ".enlarge-hero-image .productthumbnail")
	WebElement imgProductPrimaryImage;

	@FindBy(css = ".zoomWindow")
	WebElement zoomedImage;
	
	@FindBy(css = ".enlarge-hero-image .productthumbnail")
	WebElement zoomedImageTab;

	@FindBy(css = ".ui-icon-closethick")
	WebElement enlargeImageCloseIcon;
	
	@FindBy(css = ".enlarge-hero-container.slick-initialized.slick-slider .slick-prev")
	WebElement prevImageArrowEnlarge;
	
	@FindBy(css = ".enlarge-hero-container.slick-initialized.slick-slider .slick-next")
	WebElement nextImageArrowEnlarge;
	
	@FindBy(css = ".enlarge-thumb-image .slick-current")
	WebElement imgEnlargeThumbNail;

	@FindBy(css = ".enlarge-right-sec")
	WebElement attributeSection;
	
	@FindBy(css = ".enlarge-container .float-left .slick-current .productthumbnail")
	WebElement imgPrimaryImage;
	
	@FindBy(css = ".enlarge-container .float-right .slick-current .productthumbnail")
	WebElement imgSelectColorSwatch;
	/**********************************************************************************************
	 ********************************* WebElements of EnlargeViewPage Page - Ends ****************************
	 **********************************************************************************************/

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public EnlargeViewPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, mdlEnlargeWindow))) {
			Log.fail("Enlarge View Page did not open up. Site might be down.", driver);
		}

		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	/**
	 * To get Y co-ordinate of color swatch
	 * @param index -
	 * @return Integer -
	 * @throws Exception -
	 */
	public int getCurrentLocationOfPdtByIndex(int index)throws Exception{		
		WebElement image = lstcolorSwatches.get(index);
		Point point = image.getLocation();		
		return point.getY();
	}

	/**
	 * To get color swatches count
	 * @throws Exception - Exception
	 * @return int
	 */
	public int getcolorSwatchesCount()throws Exception{
		return lstcolorSwatches.size();
	}

	/**
	 * To close the Enlarge View
	 * @throws Exception - Exception
	 */
	public void closeTheEnlargeView()throws Exception{
		BrowserActions.clickOnElementX(btnEnlargeViewClose, driver, "Enlarge view close button");
	}
	
	/**
	 * To get Alternate Images count
	 * @throws Exception - Exception
	 * 
	 * @return int
	 */
	public int getAlternateImagesCount()throws Exception{
		return lstAlternateImages.size();
	}

	/**
	 * To click on Shopping Cart tool tip
	 * @param index -
	 * @throws Exception - Exception
	 * @return string
	 */
	public String getSelectedImageName(int index) throws Exception {

		String altImages = "";
		if(lstAlternateImages.size()>0){
			altImages=  lstAlternateImages.get(index - 1).getAttribute("src").split("&crop")[0];
		}
		return altImages;
	}
	
	/**
	 * To click on Shopping Cart tool tip
	 * @param index -
	 * @throws Exception - Exception
	 * @return string
	 */
	public String getSelectedImageName() throws Exception {

		String[] altImages = imgCurrentAlternateImage.getAttribute("src").split("\\/");
		return altImages[altImages.length-1];
	}
	

	/**
	 * To get main product image source 
	 * @return image source
	 * @throws Exception - Exception
	 */
	public String getMainProductImage() throws Exception {		
		if(Utils.isTablet()) {
			String[] value = imgMainProduct_Tablet.getAttribute("src").split("\\/");
			return value[value.length - 1];
		} else {
			return imgMainProduct.getAttribute("src").split("&crop")[0];
		}
	}
	
	/**
	 * To click on the previous image arrow
	 * @throws Exception -
	 */
	public void clickOnPrevImageArrow() throws Exception {		
		BrowserActions.clickOnElementX(prevImageArrowEnlarge, driver, "Previous Arrow");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on the next image arrow
	 * @throws Exception -
	 */
	public void clickOnNextImageArrow() throws Exception {		
		BrowserActions.clickOnElementX(nextImageArrowEnlarge, driver, "Next Arrow");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To check the next arrow in the enlarge page changes the image
	 * @return true if the image changes, else false
	 * @throws Exception -
	 */
	public boolean checkThumbnailImageSelectedBasedOnNextButton() throws Exception {		
		String beforeNavigating = BrowserActions.getTextFromAttribute(driver, imgEnlargeThumbNail, "aria-describedby", "");
		clickOnNextImageArrow();
		String afterNavigating = BrowserActions.getTextFromAttribute(driver, imgEnlargeThumbNail, "aria-describedby", "");
		if (beforeNavigating.equals(afterNavigating)) {
			return false;
		}
		return true;
	}
	
	/**
	 * To check the previous arrow in the enlarge page changes the image
	 * @return true if the image changes, else false
	 * @throws Exception -
	 */
	public boolean checkThumbnailImageSelectedBasedOnPrevButton() throws Exception {		
		String beforeNavigating = BrowserActions.getTextFromAttribute(driver, imgEnlargeThumbNail, "aria-describedby", "");
		clickOnPrevImageArrow();
		String afterNavigating = BrowserActions.getTextFromAttribute(driver, imgEnlargeThumbNail, "aria-describedby", "");
		if (beforeNavigating.equals(afterNavigating)) {
			return false;
		}
		return true;
	}

	/**
	 * To verify main and alternate image equals
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyMainAndAlternateImage()throws Exception{
		boolean status = false;

		Log.event("getMainProductImage==="+getMainProductImage());
		Log.event("getSelectedImageName=="+getSelectedImageName());

		if(getMainProductImage().contains(getSelectedImageName())){
			status = true;
		}
		return status;
	}

	/**
	 * To click on alternate image 
	 * @param index -
	 * @throws Exception - Exception
	 */
	public void clickOnAlternateImage(int index) throws Exception {

		if(lstAlternateImages.size()>1){		
			BrowserActions.clickOnElementX(lstAlternateImages.get(index - 1), driver,
					"Select Color swatches");
		}
		else{
			Log.message("Alternate Image is 1");
		}
	}

	/**
	 * To get color swatch
	 * @param index -
	 * @return swatch as string
	 * @throws Exception - Exception
	 */
	public String getSwatchColor(int index)throws Exception {
		String[] imgColor = lstcolorSwatches.get(index).getAttribute("src").split("\\|");
		String[] colorCodes = imgColor[imgColor.length-1].replace(".jpg", "").split("\\_");
		String colorCode = colorCodes[colorCodes.length-1];
		
		Log.event("Color Code :: " + colorCode);
		return colorCode;
	}
	
	/**
	 * To get color swatch
	 * @param index -
	 * @return swatch as string
	 * @throws Exception - Exception
	 */
	public String getSelectedSwatchColor()throws Exception {
		WebElement selectedColorSwatch = driver.findElement(By.cssSelector(".enlarge-container .swatches.color li.selected a img"));
		String[] imgColor = selectedColorSwatch.getAttribute("src").split("\\|");
		String[] colorCodes = imgColor[imgColor.length-1].replace(".jpg", "").split("\\_");
		String colorCode = colorCodes[colorCodes.length-1];
		
		Log.event("Color Code :: " + colorCode);
		return colorCode;
	}
	
	/**
	 * To select color swatch
	 * @param index - index
	 * @throws Exception - Exception
	 */
	public void selectColor(int index)throws Exception {
		WebElement color = lstcolorSwatchesSelectable.get(index);
		if (!(BrowserActions.getTextFromAttribute(driver, color, "class", "").toLowerCase().contains("selected"))) {
			WebElement colorAnchor = color.findElement(By.cssSelector("a"));
			BrowserActions.javascriptClick(colorAnchor, driver, "color");
		}
	}

	/**
	 * To get alternate image color text
	 * @return text
	 * @throws Exception - Exception
	 */
	public String getAlternateImageColor(int index) throws Exception {
		String[] imgColor = lstAlternateImages.get(index).getAttribute("src").split("\\/");
		String[] colorCodes = imgColor[imgColor.length-1].replace(".jpg", "").split("\\_");
		String colorCode = colorCodes[colorCodes.length-1];
		
		Log.event("Color Code :: " + colorCode);
		return colorCode;
	}
	
	/**
	 * To get selected alternate image color text
	 * @return text - Color code
	 * @throws Exception - Exception
	 */
	public String getSelectedAlternateImageColor() throws Exception {
		String[] imgColor = imgCurrentAlternateImage.getAttribute("src").split("\\/");
		String[] colorCodes = imgColor[imgColor.length-1].replace(".jpg", "").split("\\_");
		String colorCode = colorCodes[colorCodes.length-1];
		
		Log.event("Color Code :: " + colorCode);
		return colorCode;
	}

	/**
	 * To close Enlarge View 
	 * @throws Exception - Exception
	 */
	public void closeEnlargeViewModal() throws Exception{		
		BrowserActions.clickOnElementX(enlargeImageCloseIcon, driver,
				"Enlarge modal close");
	}

	/**
	 * To get the current Y co-ordinate position of Alt image of given index
	 * @param index -
	 * @return int - Y Point of Image
	 * @throws Exception - Exception
	 */
	public int getCurrentLocationOfAltImgByIndex(int index)throws Exception{		
		WebElement image = lstAlternateImages.get(index);
		Point point = image.getLocation();		
		return point.getY();
	}

	/**
	 * To mouse hover on primary image
	 * @throws Exception - Exception
	 */
	public void mouseHoverOnPrimaryImage() throws Exception{	

		if(runPltfrm.equals("desktop")){
			BrowserActions.mouseHover(driver, imgProductPrimaryImage);
			BrowserActions.mouseHover(driver, imgProductPrimaryImage);
		}
		else{
			BrowserActions.moveToElementJS(driver, imgProductPrimaryImage);
			Actions action = new Actions(driver);
			action.moveToElement(imgProductPrimaryImage).doubleClick().build();
		}
	}

	public int getAltImgSlickIndex()throws Exception{
		int getSlickSlideIndex = Integer.parseInt(imgSelectedAlternateImage.getAttribute("data-slick-index"));
		Log.event("Slick Slider Index :: " + getSlickSlideIndex);
		return getSlickSlideIndex;
	}
	
	public void mouseHoverOnAltImage(int index)throws Exception{
		BrowserActions.mouseHover(driver, lstAlternateImages.get(index-1), index + "th Alt Image");
	}
	
	/**
	 * To get color code of product primary image
	 * @return String - 
	 * @throws Exception -
	 */
	public String getPrimaryImgColorCode() throws Exception {
		String imgPath[] = imgPrimaryImage.getAttribute("src").trim().split("\\/");
		String imgCode = imgPath[imgPath.length - 1].split(".jpg")[0].replace(".jpg", "");
		return imgCode;
	}
	
	/**
	 * To get color code of selected color swatch image
	 * @return String - 
	 * @throws Exception -
	 */
	public String getSelectedColorCode() throws Exception {
		String imgPath[] = imgSelectColorSwatch.getAttribute("src").trim().split("\\/");
		String imgCode = imgPath[imgPath.length - 1].split("\\_")[1].replace(".jpg", "");
		return imgCode;
	}

}