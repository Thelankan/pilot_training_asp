package com.fbb.pages.customerservice;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class FaqPage extends LoadableComponent<FaqPage> {

	private WebDriver driver;
	public boolean isPageLoaded;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();
	public CSNavigation csNav;



	/**********************************************************************************************
	 ********************************* WebElements of FaqPage page ****************************
	 **********************************************************************************************/
	@FindBy(css = ".pt_article-page")
	WebElement readyElement;
	
	@FindBy(css = ".secondary-left-section")
	WebElement navLeftNavigation;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement breadcumbMobile;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement breadcumbTablet;
	
	@FindBy(css = ".secondary-aticle-content .mobile-nav")
	WebElement navLeftNavigationMobile;
	
	@FindBy(css = ".secondary-article-heading.mobile-nav")
	WebElement navMobileCurrentSelection;
	
	@FindBy(css = ".customerservice-name.hide-tablet.hide-mobile")
	WebElement csLabelDesktop;
	
	@FindBy(css = ".customerservice-name.hide-desktop")
	WebElement csLabelTablet;
	
	@FindBy(css = ".article-heading")
	WebElement csCurrentHeading;
	
	@FindBy(css = ".faq-content")
	WebElement faqContent;
	
	@FindBy(css = ".currentpage")
	WebElement lnkCurrentPage;
	
	@FindBy(css="a[title='FAQs']")
	WebElement tileLeftNavFaqww;

	@FindBy(css="a[title='FAQ']")
	WebElement tileLeftNavFaq;

	@FindBy(css =".pt_article-page")
	WebElement lnknavFaq;

	@FindBy(css=".faq-content h1")
	WebElement articleHeadingFAQjl;

	@FindBy(css=".article-heading")
	WebElement articleHeadingFAQ;

	@FindBy(css =".pt_article-page" )
	WebElement lnkNavContactUS;

	@FindBy(css=".inner-service-landing-contacts h2")
	WebElement tileContactModule;

	@FindBy(css=".mail-contact .mail-title")
	WebElement tilemailtitle;

	@FindBy(css = ".qa-content .question")
	List<WebElement> lstQAContent;  

	@FindBy(css = ".faq-content .qa-section .qa-content .question a")
	List<WebElement> lstQAContentOthers;

	@FindBy(css = ".inner-service-landing-contacts .ways-contacts .contact-title")
	WebElement  lnkCallUs;
	
	@FindBy(css = ".contact-text")
	WebElement lblContactNumber;
	
	@FindBy(css = ".chat-text-available")
	WebElement lblChatAvailable;
	
	@FindBy(css = ".chat-text-unavailable")
	WebElement lblChatUnavailable;
	
	@FindBy(css = ".chat-contact a div")
	WebElement lblChatText;
	
	@FindBy(css = ".available-image")
	WebElement imgChatAvailable;
	
	@FindBy(css = ".chat-unavailable")
	WebElement imgChatNotAvailable;
	
	@FindBy(css = ".secondary-aticle-content .secondary-article-heading.mobile-nav")
	WebElement MenuMobile;
	
	@FindBy(css = ".inner-service-landing-contacts")
	WebElement contactUsSection;
	
	@FindBy(css = ".secondary-left-section")
	WebElement leftNavigationSection;


	/**********************************************************************************************
	 ********************************* WebElements of FaqPage page ****************************
	 **********************************************************************************************/	


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public FaqPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
		csNav = new CSNavigation(driver).get();
	}




	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Contact Us Page did not open up.", driver);
		}
	}
	
	/**
	 * To navigate to FAQ Page
	 * @return FaqPage
	 * @throws Exception - Exception
	 */
	public FaqPage navigateToFaqPage()throws Exception{
		BrowserActions.clickOnElementX(lnknavFaq, driver, "FAQ Page Link");
		Utils.waitForPageLoad(driver);
		return new FaqPage(driver).get();
	}
	
	/**
	 * To navigate to Contact Us Page through contact module
	 * @return ContactUsPage
	 * @throws Exception - Exception
	 */
	public ContactUsPage navigateToContactUsPagethroughContactModule()throws Exception{
		BrowserActions.clickOnElementX(tilemailtitle, driver, "ContactUS Page Link");
		Utils.waitForPageLoad(driver);
		return new ContactUsPage(driver).get();
	}

	/**
	 * To verify ALL QA conent State
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean verifyAllQAContentState()throws Exception{

		for(WebElement ele : lstQAContent){
			if(ele.getAttribute("class").contains("active"))
				return false;
		}
		return true;
	}


	/**
	 * To verify Arrow Symbol in QA Content Place
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean verifyArrowSymbol()throws Exception{

		if (Utils.getRunBrowser(driver).equals("chrome"))
		{
			for(WebElement ele : lstQAContent){
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				String value = (String) executor.executeScript("return window.getComputedStyle(arguments[0], '::after').getPropertyValue('background-image')", ele);
				System.out.println(" -- "+value);
				if(!value.contains("closed-arrow-FAQ.png"))
					return false;
			}
			return true;
		}
		else
		{
			for(WebElement ele : lstQAContentOthers){
				String value = ele.getCssValue("background");
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				value = (String) executor.executeScript("return window.getComputedStyle(arguments[0], '::after').getPropertyValue('background-image')", ele);
				System.out.println(" -- "+value);
				if(!value.contains("closed-arrow-FAQ.png"))
					return false;
			}
			return true;
		}



	} 

	/**
	 * To verify Expanded State of QA Content by default
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean verifyExpandedState()throws Exception{
		for(WebElement ele : lstQAContent){
			BrowserActions.clickOnElementX(ele, driver, "Expanded status of the Elements");
			if(ele.getAttribute("class").contains("active"))
				return true;
		}

		return false;
	}

	/**
	 * To verify Expanded To Collaped State of QA Content
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean verifyExpandedtoCollapsedState()throws Exception{

		for(WebElement ele : lstQAContent){
			BrowserActions.clickOnElementX(ele, driver, "Expanded status of the Elements");
			if(ele.getAttribute("class").contains("active"))

				return true;

		}
		return false;
	}


}






