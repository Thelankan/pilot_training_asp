package com.fbb.pages;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;


public class ShoppingBagPage extends LoadableComponent<ShoppingBagPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;
	String runPltfrm = Utils.getRunPlatForm();

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	private static final String Recently_Viewed_Section = ".product-listing.last-visited";
	private static final String Cart_Recommendation=".you-may-like.loaded ";

	@FindBy(css = ".pt_cart")
	WebElement miniCartContent;

	@FindBy(css = ".name>a")
	WebElement txtProductNameDesktop;
	
	@FindBy(css = ".cart-top-banner h1")
	WebElement headerShoppingBag;

	@FindBy(css = ".cart-top-banner .button-fancy-large")
	WebElement btnCheckoutNow;

	@FindBy(css = ".cart-promo.cart-promo-approaching")
	WebElement divDiscountApproach;
	
	@FindBy(css = ".button-fancy-large.add-to-cart")
	WebElement btnUpdateInEditOverlay;

	@FindBy(css = ".item-edit-details.btm-space a")
	WebElement btnEditDetails;

	@FindBy(css = ".hide-desktop.btm-space a")
	WebElement btnEditDetails_Mobile_Tablet;

	@FindBy(css = ".button-text.btm-spac.remove-btn")
	List<WebElement> btnDeleteErrProd;

	@FindBy(css = "#Quantity")
	WebElement btnEditOverlayQty;

	@FindBy(css = ".specialproductsetcomponents.hide-mobile .cart-row.specialproductsetchild")
	WebElement specialProductSetChild;

	@FindBy(css = ".specialproductsetcomponents.hide-mobile .cart-row.specialproductsetchild .sku .value")
	WebElement specialProductSetChildID;

	@FindBy(css = ".specialproductsetcomponents.hide-mobile .cart-row.specialproductsetchild div[data-attribute='size'] .value")
	WebElement specialProductSetChildSize;

	@FindBy(css = ".specialproductsetcomponents.hide-mobile .cart-row.specialproductsetchild div[data-attribute='color'] .value")
	WebElement specialProductSetChildColor;

	@FindBy(css = "button[name*='deleteProduct']")
	List<WebElement> lstBtnRemove;

	@FindBy(css = "button[name*='deleteProduct']")
	WebElement btnRemove;

	@FindBy(css = ".item-quantity")
	WebElement 	divItemQuantity;

	@FindBy(css = ".cart-top-banner")
	WebElement 	divCartTopBanner;

	@FindBy(css = ".gift-Card img:not(.cart-brand-logo)")
	WebElement 	imgGCBrandImage;

	@FindBy(css = ".cart-top-banner .cart-action-checkout .button-fancy-large")
	WebElement 	divCartTopBannerCheckOutButton;

	@FindBy(css = ".bonus-item-actions")
	WebElement 	bonusItemAction;

	@FindBy(css = ".cart-promo.bonus-product-calloutMsg")
	WebElement 	bonusCartPromo;

	@FindBy(css = ".custom-cart-update .input-text")
	List<WebElement> valueItemQuantity;

	@FindBy(name = "dwfrm_cart_shipments_i0_items_i0_quantity")
	WebElement 	txtItemQuantity;

	@FindBy(name = "dwfrm_cart_shipments_i0_items_i1_quantity")
	WebElement 	txtItemSecondQuantity;

	@FindBy(css = ".qty-error-msg")
	WebElement 	divQuantityMessage;

	@FindBy(css = ".cart-top-banner")
	WebElement 	divHeadSection;

	@FindBy(css = ".cart-promo.cart-promo-approaching")
	WebElement 	divAlertSection;

	@FindBy(css = ".add-to-wishlist")
	WebElement 	lnkAddToWishlist;

	@FindBy(css = ".in-wishlist")
	WebElement 	txtInWishList;

	@FindBy(css = ".button-text.btm-spac.remove-btn")
	List<WebElement> listRemoveProduct;

	@FindBy(css = ".email input.email.required.error")
	WebElement receipentMailError;

	@FindBy(css = ".confirmemail input.email.required.error")
	WebElement receipentConfirmMailError;

	@FindBy(css = ".fields-address input.from.error")
	WebElement fromMailError;

	@FindBy(css = ".message.error textarea")
	WebElement msgAreaError;

	@FindBy(css = ".please-select.error")
	WebElement mainEGiftError;

	@FindBy(css = ".item-image img:not([alt='cart brand'])")
	WebElement 	imgProductSection;

	@FindBy (css =".specialproductsetcomponents.hide-mobile .cart-row.specialproductsetchild")
	WebElement prdSetChildProduct;

	@FindBy(css = ".specialproductsetcomponents.hide-desktop.hide-tablet .cart-row.specialproductsetchild")
	WebElement prdSetChildProductMobile;

	@FindBy(css = ".product-list-item .name>a")
	WebElement 	productNameSection;

	@FindBy(css = ".product-list-item div[data-attribute='color'] .value")
	WebElement 	productColor;

	@FindBy(css = ".product-list-item div[data-attribute='size'] .value")
	WebElement 	productSize;

	@FindBy(css = "#cart-items-form .product-availability-list span:not([class*='label'])")
	WebElement 	productAvailability;

	@FindBy(css = ".cart-brand-logo")
	WebElement 	imgBrandLogo;

	@FindBy(css = ".cart-unit-price")
	WebElement 	productUnitPrice;

	@FindBy(css = ".cart-unit-price .price-standard")
	WebElement 	productMarkDownPrice;

	@FindBy(css = ".cart-unit-price .price-sales ")
	List<WebElement> productSalesPrice;

	@FindBy(css = ".item-details")
	WebElement productDetails;

	@FindBy(css = ".item-total")
	WebElement productTotalPrice;

	@FindBy(css = ".custom-cart-update")
	WebElement productQuantityColumn;

	@FindBy(css = ".item-total .price-total-strikeoff")
	WebElement productItemTotalMarkDown;

	@FindBy(css = ".item-total .price-total")
	WebElement productItemTotalRegular;

	@FindBy(css = ".item-edit-details.btm-space a")
	WebElement lnkEdit;

	@FindBy(css = ".hide-desktop.btm-space")
	WebElement lnkEdit_Tablet_Mobile;

	@FindBy(css = ".sku span")
	WebElement 	txtproductId;

	@FindBy(css = ".sku .value")
	List<WebElement> lstProductSku;

	@FindBy(xpath = ".//*[@class='column col-1']/div[@class='item-details']/div[@class='product-list-item']/div[@data-attribute='size']/span[2]")
	WebElement 	spanProductSize;

	@FindBy(xpath = ".//*[@class='column col-1']/div[@class='item-details']/div[@class='product-list-item']/div[@data-attribute='color']/span[2]")
	WebElement 	spanProductColor;

	@FindBy(xpath = ".//*[@class='column col-1']/div[@class='item-details']/div[@class='product-availability-list attribute']/span[2]")
	WebElement 	spanProductAvailability;

	@FindBy(css = ".cart-unit-price.attribute .price-sales")
	WebElement 	spanProductPrice;

	@FindBy(css = ".cart-unit-price.attribute")
	WebElement productPriceAttribute;

	@FindBy(css = ".qty-decrease.qty-arrows.disabled")
	WebElement 	divDecreaseArrow;

	@FindBy(css = ".qty-increase.qty-arrows.disabled")
	WebElement 	divIncreaseArrow;

	@FindBy(css = ".qty-decrease.qty-arrows")
	WebElement 	divDecreaseArrowEnabled;

	@FindBy(css = ".qty-increase.qty-arrows")
	WebElement 	divIncreaseArrowEnabled;

	@FindBy(xpath = ".//*[@class='cart-columns']/div[@class='column col-1']/div[@class='item-details']/div[@class='cart-unit-price attribute']/span[2]")
	List<WebElement> productPrice;

	@FindBy(css = ".price-unadjusted")
	List<WebElement> totalProductPriceMarkdown;

	@FindBy(css = ".item-total .price-total")
	List<WebElement> totalProductPrice;

	@FindBy(css = ".item-quantity .input-text")
	List<WebElement> listProductQuantity;

	@FindBy(css = ".item-edit-details.btm-space a")
	List<WebElement> listEditlnk;

	@FindBy(css = ".hide-desktop.btm-space a")
	List<WebElement> listEditlnkMobileTablet;

	@FindBy(css = ".add-to-wishlist")
	List<WebElement> listWishlistLink;

	@FindBy(css = Recently_Viewed_Section)
	WebElement recentlyViewedSection;

	@FindBy(css = Recently_Viewed_Section + " .p-image")
	List<WebElement> recentlyViewedProdImage;

	@FindBy(css = Recently_Viewed_Section + " .product-name")
	List<WebElement> recentlyViewedProdName;

	@FindBy(css = Recently_Viewed_Section + " .product-tile")
	WebElement recentlyViewedProdTile;

	@FindBy(css = Recently_Viewed_Section + " .product-pricing")
	List<WebElement> recentlyViewedProdPricing;

	@FindBy(css = Recently_Viewed_Section + " .slick-prev.slick-arrow")
	WebElement recentlyViewSectionPrevArrow;

	@FindBy(css = Recently_Viewed_Section + " .slick-next.slick-arrow")
	WebElement recentlyViewSectionNextArrow;

	@FindBy(css = Recently_Viewed_Section + " .slick-prev.slick-arrow.slick-disabled")
	WebElement recentlyViewSectionPrevDisabledArrow;

	@FindBy(css = Recently_Viewed_Section + " .slick-next.slick-arrow.slick-disabled")
	WebElement recentlyViewSectionNextDisabledArrow;

	@FindBy(css = ".slick-list.draggable .grid-tile.slick-active")
	List<WebElement> recentlyViewedCurrentProd;

	@FindBy(css = ".slick-list.draggable .grid-tile.slick-active .product-name")
	List<WebElement> recentlyViewedCurrentProdName;

	@FindBy(css = ".in-wishlist")
	List<WebElement> listWishlistLinkAdded;

	@FindBy(css = ".inner-block .login-box.login-account")
	WebElement divLoginPanel;

	@FindBy(css = ".cart-coupon-code button")
	WebElement btnPromoCouponApply;

	@FindBy(css = ".cart-row")
	List<WebElement> divCartRow;

	@FindBy(css = ".cart-row")
	WebElement divSingleCartRow;

	@FindBy(css = ".cart-row")
	WebElement divCartEmpty;

	@FindBy(css = ".account-logout a")
	WebElement 	lnkSignOut; 

	@FindBy(css = ".account-links .Wishlist")
	WebElement 	lnkWishlist; 

	@FindBy(css = ".option-update button")
	WebElement 	removeWishlist; 

	@FindBy(css = ".price-unadjusted")
	WebElement totalProductPriceUnAdjusted;

	@FindBy(css = ".cart-unit-price.attribute")
	List<WebElement> divPriceAttribute;

	@FindBy(css = ".personalized-message")
	List<WebElement> divPersonalizedMessage;

	@FindBy(css = ".personalized-message")
	WebElement txtPersonalizedMessage;

	@FindBy(css = ".giftcard.from")
	List<WebElement> divGiftcartFrom;

	@FindBy(css = ".gift-Card")
	WebElement prdRowGiftCard;

	@FindBy(css = ".cart-empty")
	WebElement emptyCart;

	@FindBy(css = ".gift-Card .giftcard.from")
	WebElement divProductGiftcartFrom;

	@FindBy(css = ".gift-Card .giftcard.from .value")
	WebElement divProductGiftcartFromValue;

	@FindBy(css = ".gift-Card .giftmessage")
	WebElement txtProductGiftcartMessage;

	@FindBy(css = ".giftcard.to")
	List<WebElement> divGiftcartTo;

	@FindBy(css = ".gift-Card .giftcard.to")
	WebElement txtGiftcartTo;

	@FindBy(css = ".gift-Card .giftcard.to .value")
	WebElement txtGiftcartToValue;

	@FindBy(css = "select[id='Quantity']")
	WebElement drpQty;

	@FindBy(css = "h2.pid")
	WebElement lblCheckoutID;

	@FindBy(css = ".item-image img[alt='cart brand']")
	WebElement productBrandImg;

	@FindBy(css = ".name>a")
	List<WebElement> productNameDesktop;

	@FindBy(css = ".shippingoverlay-link")
	WebElement txtSeeDetails;

	@FindBy(css = ".discount-item-details .see-details")
	WebElement txtCouponSeeDetails;

	@FindBy(css = ".price-adjusted-total")
	WebElement txtUpdatedSubTotal;

	@FindBy(css = ".price-total-strikeoff")
	WebElement txtSubTotal;

	@FindBy(css = ".price-total")
	WebElement txtUnitPrice;

	@FindBy(css = ".promo-adjustment")
	WebElement promotionCalloutMessage;

	@FindBy(css = ".button-fancy-large.all-set-button.add-all-to-cart")
	WebElement btnUpdateCartEditOverlay;

	@FindBy(css = ".qty-increase.qty-arrows.disabled")
	WebElement btnQtyArrowUpDisabled;

	@FindBy(css = ".qty-decrease.qty-arrows.disabled")
	WebElement btnQtyArrowDownDisabled;

	@FindBy(css = ".column.col-1 .product-list-item .sku .value")
	WebElement txtProductSetID;

	@FindBy(css = ".cart-unit-price.attribute .price-sales")
	WebElement txtProductPrice;

	@FindBy(css = "button[value='Remove'] span")
	WebElement btnRemoveInCartPage;

	@FindBy(css = ".custom-cart-update input")
	WebElement qtyDropDownInCart;

	@FindBy(css = ".item-quantity")
	WebElement sectionQtyDropDown;

	@FindBy(css = "#primary .login-box.login-account ")
	WebElement readyElementSignInPage;

	@FindBy(css = "a[id*='paypalExpressButton']")
	WebElement btnPaypal;

	@FindBy(css = ".pt_checkout")
	WebElement readyElementCheckoutPage;

	@FindBy(css = ".cart-plcc-right>h1")
	WebElement headingPLCCcontentslot;	

	@FindBy(css = ".cart-plcc-section.active")
	WebElement divPLCCcontentslotExpanded;

	@FindBy(css = ".cart-plcc-section")
	WebElement divPLCCcontentslotMobileTablet;

	@FindBy(css = ".expand-close")
	WebElement expandclosePLCCcontentslot;

	@FindBy(css = ".cart-plcc-right .learn-more")
	WebElement learnmorelinkPlcc;	

	@FindBy(css = ".cart-plcc-right b")
	WebElement creditlimitPlcc;	

	@FindBy(css = ".cart-plcc-left.hide-mobile")
	WebElement logoPlccBrand;	

	@FindBy(css = ".cart-plcc-right>span.text")
	WebElement textPLCCcontentslot;

	@FindBy(css = ".cart-plcc-right .hide")
	WebElement textPLCCcontentslotExpanded;	

	@FindBy(css = ".bonus-item-actions .select-bonus")
	WebElement btnBonusProductAdd;	

	@FindBy(css = ".bonus-product-list")
	WebElement bonusProductOverlay;	


	@FindBy(css = ".cart-promo.bonus-product-calloutMsg .bonus-item-details")
	WebElement bonusCalloutMessageDisplay;	

	@FindBy(css = ".bonus-check")
	List<WebElement> chkBonusProductSelection;


	@FindBy(css = ".add-to-cart-bonus.button-fancy-medium")
	WebElement btnBonusProductAddToBag;	

	@FindBy(css = ".bonus-item")
	WebElement bonusItemPrice;

	@FindBy(css = ".qty-increase.qty-arrows.disabled")
	WebElement qtyIncreaseArrowDisabled;

	@FindBy(css = ".qty-decrease.qty-arrows.disabled")
	WebElement qtyDecreaseArrowDisabled;

	@FindBy(css = ".bonus-item-actions.item-user-actions .btm-space .add-to-wishlist")
	WebElement btnBonusAddToWishlist;

	@FindBy(css = ".bonus-item-actions.item-user-actions .item-edit-details.btm-space a")
	WebElement btnBonusEdit;	

	@FindBy(css = ".item-user-actions")
	WebElement divUserActionItems;

	@FindBy(css = ".cart-message")
	WebElement txtPromoHeading;

	@FindBy(css = "input[id='dwfrm_cart_couponCode']")
	WebElement txtPromoCode;

	@FindBy(css = "button[id='add-coupon']")
	WebElement btnApplycoupon;

	@FindBy(css = ".discount-item-details .discount")
	WebElement txtpromotionCalloutMsg;

	@FindBy(css = ".cart-right-section .rw-error.error.hide")
	WebElement txtRewardCertificateError;

	@FindBy(css = ".cart-left-section .rw-error.error.hide")
	WebElement txtRewardCertificateErrorMobile;

	@FindBy(css = ".discount-details.hide")
	WebElement txtPromotionDescription;

	@FindBy(css = ".cart-coupon-code a")
	WebElement couponToolTip;

	@FindBy(css = ".tips")
	WebElement couponToolTipContent;

	@FindBy(css = ".cart-coupon-code input")
	WebElement promoInputfield;

	@FindBy(css = ".textbutton")
	WebElement btnRemoveButton;

	@FindBy(css = ".textbutton")
	List<WebElement> btnCouponRemoveButton;

	@FindBy(css = ".cartcoupon.clearfix .value")
	WebElement appliedCouponRow;

	@FindBy(css =".cart-right-section .rowcoupons")
	List<WebElement> coupounAppliedCount;

	@FindBy(css = ".cart-right-section div[class='error']")
	WebElement txtcouponError;

	@FindBy(css = ".error-messages.hide-desktop.hide-tablet .error:not(.rw-error)")
	WebElement txtcouponErrorMobile;

	@FindBy(css = ".ui-button-icon.ui-icon.ui-icon-closethick")
	WebElement btnCouponToolTipClose;

	@FindBy(css = ".shippingoverlay-link")
	WebElement spanCostToolTip;

	@FindBy(css = ".order-shipping-discount.discount.order-detail")
	WebElement divShippingDiscount;

	@FindBy(css = ".order-sales-tax.order-detail .value")
	WebElement txtSalesTaxValue;

	@FindBy(css = ".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.shipping-overlay.ui-draggable")
	WebElement divShoppingCostToolTip;

	@FindBy(css = ".order-total.order-detail .label")
	WebElement divRemainingTotal;

	@FindBy(css = ".order-discount.discount.order-detail .value")
	WebElement divDicountTotal;

	@FindBy(css = ".order-shipping-discount.discount.order-detail .value")
	WebElement divShippingDicountTotal;

	@FindBy(css = ".order-saving.discount.order-detail .order-saving.value")
	WebElement divTotalSavings;

	@FindBy(css = ".quick-order-badge.hide-mobile .quick-order-badge-link")
	WebElement quickOrderCatalogBagde;

	@FindBy(css = ".quick-order-badge.hide-desktop.hide-tablet .quick-order-badge-link")
	WebElement quickOrderCatalogBagdeMobile;

	@FindBy(css = ".item-image img:nth-child(2)")
	List<WebElement> quickOrderbrandCount;

	@FindBy(css = ".cart-promo.bonus-product-calloutMsg")
	WebElement lblBonusProductCallout;

	@FindBy(css = ".select-bonus")
	WebElement lnkAddBonusProduct;

	@FindBy(css = "#QuickViewDialog")
	WebElement popUpBonusProduct;
	
	@FindBy(css = ".quick-view .ui-dialog-titlebar-close .ui-icon")
	WebElement btnEditOverlayClose;

	@FindBy(css = ".bonus-product-item")
	List<WebElement> bonusProducts;

	@FindBy(css = ".bonus-check")
	List<WebElement> checkbonusProducts;

	@FindBy(css = ".add-to-cart-bonus.button-fancy-medium")
	WebElement btnAddtoCartBonus;

	@FindBy(css = ".plcc-form-section")
	WebElement divplccLearnmorePage;

	@FindBy(css = ".selecting.attr-color span")
	List<WebElement> selectcolortBonus;

	@FindBy(css = ".cart-top-banner")
	WebElement contentBaner;

	@FindBy(css = ".spc-guest-btn.filled-wineberry")
	WebElement btnContinueGuest;

	@FindBy(css = "#dwfrm_billing_billingAddress_email_emailAddress")
	WebElement emailInputField;

	@FindBy(css =  ".shipping-overlay .ui-dialog-content")
	WebElement shippingCostOverlay;

	@FindBy(css =  ".ui-button.ui-corner-all.ui-widget.ui-button-icon-only.ui-dialog-titlebar-close")
	WebElement shippingCostOverlayClose;

	@FindBy(css =  "div[class='dialog-content ui-dialog-content ui-widget-content']  .promo.discount.order-detail")
	WebElement divShippingPromotionDetail;

	@FindBy(css =  "#primary > div.cart-actions.cart-actions-top > div.cart-content > div.cart-left-content > div.cart-footer > div.cart-order-totals > div > div > div.order-shipping-discount.discount.order-detail > span.label")
	WebElement shippingDiscountSection;

	@FindBy(css =  "div.cart-footer")
	WebElement orderSummarySection; 

	@FindBy(css = ".error-form")
	WebElement qtyError;

	@FindBy(css = ".name a")
	List<WebElement> lstCartProductNames;

	@FindBy(css = ".quick-view")
	WebElement viewEditModal;

	@FindBy(css = ".selected .color")
	WebElement modalSelectedColor;

	@FindBy(css = ".selected .size")
	WebElement modalSelectedSize;

	@FindBy(css = ".ui-dialog-titlebar-close")
	WebElement btnCloseEditModal;

	@FindBy (css =".cart-empty button")
	WebElement btnShopNewArriaval;

	@FindBy(css = ".slick-track .product-name-image-container .name-link")
	WebElement trendNowImage;

	@FindBy(css = ".slick-list.draggable")
	WebElement trendNowSlickContainer;

	@FindBy(css = ".recommendation-heading h2")
	WebElement trendNowHeading;

	@FindBy(css = Cart_Recommendation)
	WebElement sectionRecommendation;

	@FindBy(css = Cart_Recommendation + " img")
	WebElement prodImageRecommendation;

	@FindBy(css = Cart_Recommendation + " .product-name")
	WebElement prodNameRecommendation;

	@FindBy(css = Cart_Recommendation + " .product-pricing")
	WebElement prodPriceRecommendation;

	@FindBy(css = Cart_Recommendation + " .product-tile")
	List<WebElement> tileListRecommendationProd;

	@FindBy(css = Cart_Recommendation + " .product-tile")
	WebElement tileRecommendationProd;

	@FindBy(css = Cart_Recommendation + " .product-standard-price")
	WebElement prodStdPriceRecommendation;

	@FindBy(css = Cart_Recommendation + " .product-sales-price")
	WebElement prodSalePriceRecommendation;

	@FindBy(css = Cart_Recommendation + " img")
	List<WebElement> prodImageListRecommendation;

	@FindBy(css = Cart_Recommendation + " .product-name")
	List<WebElement> prodNameListRecommendation;

	@FindBy(css = ".quick-view")
	WebElement divQuickShop;
	
	@FindBy(css = ".optioninfo .hemmable")
	WebElement divSpecialMessageHemming;
	
	@FindBy(css = ".optioninfo .hemmable .optionvalue")
	WebElement divSpecialMessageHemmingValue;
	
	@FindBy(css = ".show-basket-id")
	WebElement lnkShowBagID;
	
	@FindBy(css = ".shopping-bag-id .pid")
	WebElement spanBagID;


	//================================================================================
	//			WebElements Declaration End
	//================================================================================

	/**
	 * constructor of the class
	 * @param driver - WebDriver
	 */
	public ShoppingBagPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, miniCartContent, 25);
		if (isPageLoaded && !(Utils.waitForElement(driver, miniCartContent))) {
			Log.fail("Shopping Bag Page didn't open", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}


	/**
	 * To Get Product Name
	 * @return String of Product Name
	 * @throws Exception - Exception
	 */

	public String getProductName()throws Exception{
		String dataToReturn = new String();
		if(Utils.getRunPlatForm().equals("desktop"))
			dataToReturn = txtProductNameDesktop.getText();
		return dataToReturn;
	}


	/**
	 * To Click on Checkout button in cart page and return checkout page
	 * @return CheckoutPage Page object
	 * @throws Exception - Exception
	 */
	public CheckoutPage clickOnCheckoutNowBtn() throws Exception{
		Utils.waitForElement(driver, btnCheckoutNow);
		BrowserActions.clickOnElementX(btnCheckoutNow, driver, "Checkout Now");
		Utils.waitForPageLoad(driver);
		return new CheckoutPage(driver).get();
	}


	/**
	 * To click on Special product main Image and return pdp page
	 * @param splPrdSetName -
	 * @return PdpPage object
	 * @throws Exception - Exception
	 * 
	 */
	public PdpPage clickOnSplProductMainImg(String splPrdSetName) throws Exception{
		WebElement splProductSetMainImage = 
				driver.findElement(By.cssSelector(".item-image "
						+ "a[title='Go to Product: "+ splPrdSetName +"'] img:not([alt='cart brand'])"));
		BrowserActions.clickOnElementX(splProductSetMainImage, driver, "Update Information");
		return new PdpPage(driver).get();
	}


	/**
	 * To click on Special product Name and return pdpPage
	 * @param splPrdSetName -
	 * @return PdpPage page object
	 * @throws Exception - Exception
	 */

	public PdpPage clickOnSplProductName(String splPrdSetName) throws Exception{
		WebElement splProductSetMainImage = 
				driver.findElement(By.cssSelector(".name a[title='Go to Product: "+ splPrdSetName +"']"));
		BrowserActions.clickOnElementX(splProductSetMainImage, driver, "Update Information");
		return new PdpPage(driver).get();
	}




	/**
	 * To apply promo coupon code
	 * @param coupon - coupon to apply
	 * @throws Exception - Exception
	 */
	public void applyPromoCouponCode(String coupon) throws Exception{
		Utils.waitForElement(driver, promoInputfield);
		BrowserActions.typeOnTextField(promoInputfield, coupon, driver, "Promotion field");
		BrowserActions.clickOnElementX(btnPromoCouponApply, driver, "Apply");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To Remove specific product form cart page
	 * @param productIndex -
	 * @throws Exception - Exception
	 */

	public void removeCartProduct(int productIndex )throws Exception{
		BrowserActions.clickOnElementX(lstBtnRemove.get(productIndex), driver, productIndex + "th Item");
		Log.event(productIndex + "th Item removed from Cart");
		Utils.waitForPageLoad(driver);

	}

	/**
	 * To Remove all items from cart page
	 * @throws Exception - Exception
	 */
	public void removeAllItemsFromCart()throws Exception{
		for(int i = lstBtnRemove.size()-1; i >= 0 ; i--){
			BrowserActions.clickOnElementX(lstBtnRemove.get(i), driver, (i+1) + "th Item's Remove Button");
			Log.event((i+1) + "th Item removed from Cart");
			Utils.waitForPageLoad(driver);
		}
	}

	/**
	 * To remove all item from cart page except some product
	 * @param except - 
	 * @return true - if all items are removed from cart
	 * @throws Exception -
	 */
	public boolean removeAllItemsFromCartExcept(String... except)throws Exception{
		List<String> productSku = getProductSkuList();
		boolean statusToReturn = false;
		int l = 0;
		
		if(btnDeleteErrProd.size() > 0) {
			int size = btnDeleteErrProd.size();
			for(int i=size-1; i>=0; i--) {
				BrowserActions.clickOnElementX(btnDeleteErrProd.get(i), driver, "Error Item's Remove Button");
			}
		}
		
		for(int i = lstBtnRemove.size()-1, j = 0, k = 0; i >= j ; i--,k++){
			WebElement deleteBtn = driver.findElement(By.xpath("//div[@class='sku']//span[contains(text(),'" + productSku.get(k) + "')]//ancestor::div[contains(@class,'cart-columns')]//button[contains(@name,'delete')]"));
			if(l < except.length){
				if(!except[0].contains(productSku.get(k))){
					BrowserActions.clickOnElementX(deleteBtn, driver, productSku.get(k) + " Item's Remove Button");
					Utils.waitForPageLoad(driver);
				}else
					l++;
			}else{
				BrowserActions.clickOnElementX(deleteBtn, driver, productSku.get(k) + " Item's Remove Button");
				Utils.waitForPageLoad(driver);
			}
		}

		if(l != except.length)
			statusToReturn = true;

		return statusToReturn;
	}

	/**
	 * To Verify product quantity
	 * @param proQuantity - quantity needed to verify
	 * @param index - product index
	 * @return true - if product quantity is equal
	 * @throws Exception - Exception
	 */
	public boolean verifyProductQuantity(int proQuantity,int index )throws Exception{
		boolean flag=false;
		Utils.waitForElement(driver, divItemQuantity);
		int productQuantity=Integer.parseInt(valueItemQuantity.get(index).getAttribute("value").trim());
		if(productQuantity==proQuantity){
			flag=true;
		}else{
			flag=false;
		}
		return flag;
	}


	/**
	 * To click on Edit link for special product
	 * @return PdpPage page object
	 * @throws Exception - Exception
	 */
	public PdpPage clickOnSplProductEdit() throws Exception{
		Utils.waitForPageLoad(driver);
		if(!(runPltfrm.equals("desktop"))) {
			BrowserActions.clickOnElementX(btnEditDetails_Mobile_Tablet, driver, "Edit qty");
			return new PdpPage(driver);
		} else {
			BrowserActions.clickOnElementX(btnEditDetails, driver, "Edit qty");
		}
		return null;
	}


	/**
	 * To click special product remove button
	 * @throws Exception - Exception
	 */
	public void clickOnSplProductRemove() throws Exception{
		BrowserActions.clickOnElementX(btnRemoveInCartPage, driver, "Update Information");
	}


	/**
	 * To click on Update cart special product edit overlay
	 * @throws Exception - Exception
	 */
	public void clickOnUpdateCartSplProdEditOverlay() throws Exception{
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, btnUpdateCartEditOverlay)) {
			Actions action = new Actions(driver);
			action.moveToElement(btnUpdateCartEditOverlay);
			BrowserActions.clickOnElementX(btnUpdateCartEditOverlay, driver, "Update Information");
		}
		Utils.waitForPageLoad(driver);
	}


	/**
	 * To get product quantity form cart
	 * @return int - value of quantity
	 * @throws Exception - Exception
	 */
	public int getQtyInCart() throws Exception{
		Utils.waitForPageLoad(driver);
		String value = BrowserActions.getTextFromAttribute(driver, qtyDropDownInCart, "value", "Qty in Cart"); 
		int qty = Integer.parseInt(value);
		return qty;
	}

	/**
	 * To select quantity in Edit overlay
	 * @param qty - Quantity
	 * @throws Exception - Exception
	 */

	public void selectQtyInEditOverlay(int qty) throws Exception{
		BrowserActions.scrollToViewElement(btnEditOverlayQty, driver);
		Select qtySelect = new Select(btnEditOverlayQty);
		if (qty > 0) {
			String value = Integer.toString(qty);
			qtySelect.selectByValue(value);
		} else {
			qtySelect.selectByIndex(0);
		}
	}



	/**
	 * To Verify cart for merged product
	 * @param qnt1 -
	 * @param qnt2 -
	 * @return true - if quantity is equl
	 * @throws Exception - Exception
	 */
	public boolean verifyMergedProduct(int qnt1,int qnt2)throws Exception{
		boolean flag=false;
		if(qnt1==Integer.parseInt(txtItemQuantity.getAttribute("value")) && qnt2==Integer.parseInt(txtItemSecondQuantity.getAttribute("value"))){
			flag=true;
		}else{
			flag=false;
		}

		return flag;


	}
	/**
	 * To verifyElementDisplayBelow
	 * @param firstElement -
	 * @param secondElement -
	 * @param obj -
	 * @return true - if element displayed below
	 * @throws Exception - Exception
	 */
	public boolean verifyElementDisplayedBelow(String firstElement,String secondElement, Object obj )throws Exception{

		boolean flag=false;
		Field f1 = obj.getClass().getDeclaredField(firstElement);
		Field f2 = obj.getClass().getDeclaredField(secondElement);
		f1.setAccessible(true);
		f2.setAccessible(true);
		WebElement firstEle = ((WebElement) f1.get(obj));
		WebElement secondEle = ((WebElement) f2.get(obj));

		if(firstEle.getLocation().getY() <secondEle.getLocation().getY())
			flag = true;


		return flag; 
	}

	/**
	 * To Verify Alert message
	 * @param message -
	 * @return true - if alert message present 
	 * @throws Exception - Exception
	 */
	public boolean verifyAlertMessage(String message)throws Exception{
		boolean flag=false;
		if(message==divAlertSection.getText())
			flag=true;

		return flag;
	}
	/**
	 * To verify product Added In cart page
	 * @param prdAddedInQuickOrder -
	 * @return true - if product added to cart
	 * @throws Exception - Exception
	 */
	public boolean verifyProductAddedInCart(String prdAddedInQuickOrder)throws Exception {
		String prdName;
		boolean status = false;

		Log.event("Prd name from User :: '" + prdAddedInQuickOrder + "'");
		for(int i=0; i< productNameDesktop.size(); i++){
			prdName = BrowserActions.getText(driver, productNameDesktop.get(i), "Product Name").trim();

			Log.event("Prd name from Site :: '" + prdName + "'");
			if(prdAddedInQuickOrder.equalsIgnoreCase(prdName.trim())){
				status = true;
				break;
			}				
		}
		return status;

	}

	/**
	 * To click on Shipping cost tool tip
	 * @throws Exception - Exception
	 */
	public void clickOnShippingCostTool() throws Exception{
		BrowserActions.clickOnElementX(txtSeeDetails, driver, "Shipping cost tool");
	}

	/**
	 * To click on Coupon Details 
	 * @throws Exception - Exception
	 */
	public void clickOnCouponSeeDetails() throws Exception{
		BrowserActions.clickOnElementX(txtCouponSeeDetails, driver, "Shipping cost tool");
	}

	/**
	 * To click on Updated Subtotal
	 * @throws Exception - Exception
	 */
	public void clickOnUpdatedSubtotal() throws Exception{
		BrowserActions.clickOnElementX(txtUpdatedSubTotal, driver, "Updated Subtotal");
	}


	/**
	 * To click on product Name
	 * @throws Exception - Exception
	 */


	public void clickOnProductName() throws Exception{
		BrowserActions.clickOnElementX(productNameDesktop.get(0), driver, "Updated Subtotal");
	}


	/**
	 * To click on get updted Sub Total
	 * @return double - price value
	 * @throws Exception - Exception
	 */
	public double getUpdatedSubtotalPrice() throws Exception{	
		String subtotal = null;
		if(Utils.waitForElement(driver, txtSubTotal)) {
			subtotal = BrowserActions.getText(driver, txtSubTotal, "Subtotal");
		} else {
			subtotal = BrowserActions.getText(driver, txtUpdatedSubTotal, "Updated Subtotal");
		}
		subtotal = subtotal.replace("$","");
		return Double.parseDouble(subtotal);
	}


	/**
	 * To Get Unit price
	 * @return double
	 * @throws Exception - Exception
	 */

	public double getUnitPrice() throws Exception{		
		String subtotal = BrowserActions.getText(driver, txtUnitPrice, "Updated Subtotal");
		subtotal = subtotal.replace("$","");
		return Double.parseDouble(subtotal);
	}


	/**
	 * To Navigete to Checkout page
	 * @return Object ob Instance
	 * @throws Exception - Exception
	 */
	public Object navigateToCheckout()throws Exception{
		BrowserActions.clickOnElementX(btnCheckoutNow, driver, "Checkout Now Button");
		Utils.waitForPageLoad(driver);

		if(Utils.waitForElement(driver, readyElementSignInPage))
			return new SignIn(driver).get();
		else if(Utils.waitForElement(driver, readyElementCheckoutPage))
			return new CheckoutPage(driver).get();
		else{
			if(Utils.waitForElement(driver, miniCartContent)){
				Log.fail("Checkout Now issue Persists - PXSFCC-1970",driver);
				return new CheckoutPage(driver).get();
			}
			Log.fail("Cannot navigate to desired page.");
		}

		return null;

	}

	/**
	 * To click on Image in cart
	 * @return PdpPage page object
	 * @throws Exception - Exception
	 */
	public PdpPage clickonImageInCart()throws Exception{
		BrowserActions.clickOnElementX(imgProductSection, driver, "Click the Product");
		return new PdpPage(driver).get();

	}

	/**
	 * To Remove all item from cart 
	 * @throws Exception - Exception
	 */
	public void removeAllCartProduct()throws Exception{
		Log.event("The product count in the Cart is :" + listRemoveProduct.size());
		while(listRemoveProduct.size() > 0){
			WebElement remove = driver.findElement(By.name("dwfrm_cart_shipments_i0_items_i0_deleteProduct"));
			if(Utils.getRunPlatForm().equals("mobile")){
				BrowserActions.scrollToViewElement(remove, driver);
			}
			BrowserActions.clickOnElementX(remove, driver, "Remove the products");
			Utils.waitForPageLoad(driver);
		}
		Log.event("All the Products in Cart page is Removed successfully");
	}

	/**
	 * To Vefity product color
	 * @param color -
	 * @return true - if product color equal
	 * @throws Exception - Exception
	 */
	public boolean verifyProductColor(String color)throws Exception{

		boolean flag=false;
		String imgColor=imgProductSection.getAttribute("alt").toLowerCase();
		if(imgColor.contains(color.toLowerCase())){
			flag=true;
		}
		return flag;

	}

	/**
	 * To click on Product Name In cart
	 * @return PdpPage page object
	 * @throws Exception - Exception
	 */
	public PdpPage clickonProductNameInCart()throws Exception{

		BrowserActions.clickOnElementX(productNameSection, driver, "Click the Product");
		return new PdpPage(driver).get();

	}


	/**
	 * To Verify product price
	 * @param price -  
	 * @return true - if product price equals
	 * @throws Exception - Exception
	 */
	public boolean verifyProductPrice(String price) throws Exception{
		BrowserActions.scrollInToView(spanProductPrice, driver);
		String actualPrice = spanProductPrice.getAttribute("innerHML").trim();
		if(price.contains(actualPrice) || price.equals(actualPrice) || price.equalsIgnoreCase(actualPrice)){
			return true;
		}else
			return false;
	}

	/**
	 * To Verify the Arrow diable in cart page
	 * @param direction -
	 * @return true -  if arrow disabled
	 * @throws Exception - Exception
	 */
	public boolean verifyArrowDisabled(String direction) throws Exception{
		boolean flag=false;

		if(direction.equals("down")){
			flag=Utils.waitForElement(driver, divDecreaseArrow, 10);	
		}else if(direction.equals("up")){
			flag=Utils.waitForElement(driver, divIncreaseArrow, 10);
		}

		return flag;
	}

	/**
	 * To click on Quantity Arrow on Cart page
	 * @param direction -
	 * @throws Exception - Exception
	 */
	public void clickOnQuantityArrow(String direction) throws Exception{
		if(direction.equals("down")){
			BrowserActions.scrollToView(divDecreaseArrowEnabled, driver);
			BrowserActions.clickOnElementX(divDecreaseArrowEnabled,driver,"Quantity Down arrow");	
		}else if(direction.equals("up")){
			BrowserActions.scrollToView(divIncreaseArrowEnabled, driver);
			BrowserActions.clickOnElementX(divIncreaseArrowEnabled,driver,"Quantity Up arrow");

		}
		Utils.waitForPageLoad(driver);

	}


	/**
	 * To Verify Error message in cart page
	 * @param message -  
	 * @return true -  if error message is equal
	 * @throws Exception - Exception
	 */
	public boolean verifyErrorMessage(String message)throws Exception{
		boolean flag=false;
		if(message.equals(divQuantityMessage.getText()))
			flag=true;

		return flag;
	}


	/**
	 * To Varify total price in cart page 
	 * @param index -
	 * @return true - if total price equals
	 * @throws Exception - Exception
	 */
	public boolean verifyTotalPrice(int index) throws Exception{

		boolean flag=false;
		String total_Value="";
		double totalProductText;
		String className=productPrice.get(index).getAttribute("class");
		if(className.equals("price-promotion")){
			String css=".price-sales";
			double productPriceText=Double.parseDouble(productPrice.get(index).findElement(By.cssSelector(css)).getText().replace("$","").trim());
			if(totalProductPriceMarkdown.get(index).getText().contains(",")){
				total_Value=totalProductPriceMarkdown.get(index).getText().replace(",","").trim();
				totalProductText=Double.parseDouble(total_Value.replace("$","").trim());
			}
			else{
				totalProductText=Double.parseDouble(totalProductPriceMarkdown.get(index).getText().replace("$","").trim());
			}

			int productqty=Integer.parseInt(listProductQuantity.get(index).getAttribute("value"));
			if(productPriceText * productqty ==totalProductText){
				flag=true;
			}
			else{
				flag=false;
			}


		}else{
			double productPriceText=Double.parseDouble(productPrice.get(index).getText().replace("$","").trim());

			if(totalProductPrice.get(index).getText().contains(",")){
				total_Value=totalProductPrice.get(index).getText().replace(",","").trim();
				totalProductText=Double.parseDouble(total_Value.replace("$","").trim());
			}
			else{
				totalProductText=Double.parseDouble(totalProductPrice.get(index).getText().replace("$","").trim());
			}

			int productqty=Integer.parseInt(listProductQuantity.get(index).getAttribute("Value"));
			if(Math.round(productPriceText * productqty) ==Math.round(totalProductText)){
				flag=true;
			}
			else{
				flag=false;
			}

		}

		return flag;

	}


	/**
	 * To Click on lEdit link 
	 * @param index - 
	 * @return Object of Instance
	 * @throws Exception - Exception
	 */
	public Object clickOnEditLink(int index)throws Exception{

		if(Utils.getRunPlatForm().equals("desktop")){
			BrowserActions.clickOnElementX(listEditlnk.get(index), driver, "Click the Edit link");
			Utils.waitForPageLoad(driver);
			if(Utils.waitForElement(driver, popUpBonusProduct)){
				return new QuickShop(driver).get();
			}
			else{
				Log.failsoft("Clicking on Edit link does not open Edit model.", driver);
			}

		}else{
			BrowserActions.clickOnElementX(listEditlnkMobileTablet.get(index), driver, "Click the Edit link");
			Utils.waitForPageLoad(driver);
			if(driver.findElement(By.id("wrapper")).getAttribute("class").contains("pt_product")){
				return new PdpPage(driver).get();
			}
			else{
				Log.failsoft("Clicking on Edit does not redirect to PDP page.", driver);
			}
		}

		return null;

	}
	
	/**
	 * To close edit overlay 
	 * @throws Exception - Exception
	 */
	public void closeEditOverlay() throws Exception{
		BrowserActions.clickOnElementX(btnEditOverlayClose, driver, "Click the Edit link");
		Utils.waitUntilElementDisappear(driver, btnEditOverlayClose);
	}
	
	


	/**
	 * To check quickshop is displayed in recently viewed
	 * @return boolean - true if Quickshop is displayed, false if not
	 * @throws Exception - Exception
	 */
	public boolean checkQuickShopIsDisplayedInRecentlyViewed()throws Exception{
		BrowserActions.scrollInToView(recentlyViewedProdTile, driver);
		BrowserActions.mouseHover(driver, recentlyViewedProdTile, "Product Tile");
		WebElement qcButton = recentlyViewedProdTile.findElement(By.cssSelector(".quickview"));
		if (Utils.waitForElement(driver, qcButton)) {
			return true;
		}
		return false;
	}

	/**
	 * To click product image in recently viewed
	 * @return PdpPage - PdpPage object
	 * @throws Exception - Exception
	 */
	public PdpPage clickOnRecentlyViewedProductName(int index)throws Exception{
		WebElement elem = recentlyViewedProdName.get(index);
		BrowserActions.clickOnElementX(elem, driver, "Recently Viewed Product Name");
		return new PdpPage(driver).get();
	}
	
	/**
	 * To click update in edit overlay
	 * @throws Exception - Exception
	 */
	public void clickOnUpdateInEditOverlay()throws Exception{
		BrowserActions.clickOnElementX(btnUpdateInEditOverlay, driver, "Recently Viewed Product Name");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click product image in recently viewed
	 * @return PdpPage - PdpPage object
	 * @throws Exception - Exception
	 */
	public PdpPage clickOnRecentlyViewedProductImage(int index)throws Exception{
		WebElement elem = recentlyViewedProdImage.get(index);
		BrowserActions.clickOnElementX(elem, driver, "Recently Viewed Product Image");
		return new PdpPage(driver).get();
	}


	/**
	 * To click product image in recently viewed
	 * @return PdpPage - PdpPage object
	 * @throws Exception - Exception
	 */
	public List<String> getCurrentRecentViewProdName()throws Exception{
		List<String> names = new ArrayList<String>();;
		int size = recentlyViewedCurrentProdName.size();
		for(int i=0; i<size; i++) {
			names.add(BrowserActions.getText(driver, recentlyViewedCurrentProdName.get(i), "Product Name"));
		}
		return names;
	}

	/**
	 * To click product image in recently viewed
	 * @return PdpPage - PdpPage object
	 * @throws Exception - Exception
	 */
	public boolean checkNextAndPrevArrowFunction()throws Exception{
		if(getNoOfProdDisplayInRecentView() > 4) {
			List<String> beforeNames = getCurrentRecentViewProdName();
			while (!Utils.waitForElement(driver, recentlyViewSectionNextDisabledArrow)) {
				BrowserActions.clickOnElementX(recentlyViewSectionNextArrow, driver, "Next arrow");
			}
			List<String> afterNames = getCurrentRecentViewProdName();

			boolean state1 =  Utils.compareTwoList(beforeNames, afterNames);

			beforeNames = getCurrentRecentViewProdName();
			while (!Utils.waitForElement(driver, recentlyViewSectionPrevDisabledArrow)) {
				BrowserActions.clickOnElementX(recentlyViewSectionPrevArrow, driver, "Prev arrow");
			}
			afterNames = getCurrentRecentViewProdName();

			boolean state2 =  Utils.compareTwoList(beforeNames, afterNames);

			if(!state1 && !state2) {
				return true;
			} else {
				return false;
			}

		} else {
			Log.event("Cannot be verified the next and Prev arrow");
			return false;
		}
	}


	/**
	 * To click on Wishlist on cart page
	 * @param productIndex -
	 * @return String of Page
	 * @throws Exception - Exception
	 */
	public String clickOnWishlist(int productIndex)throws Exception{

		String returnString="";
		BrowserActions.clickOnElementX(listWishlistLink.get(productIndex), driver, "Click the Wishlist link");
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, divCartTopBanner)){
			if(Utils.waitForElement(driver,listWishlistLinkAdded.get(productIndex))){

				returnString=listWishlistLinkAdded.get(productIndex).getText();
				Log.message("The product Add to wishlist has been changed to " +returnString);
			}

		}else if(Utils.waitForElement(driver, divLoginPanel)){

			returnString="SignUp_Page";
		}

		return returnString;

	}


	/**
	 * To get product count in cart page
	 * @return int -
	 * @throws Exception - Exception
	 */
	public int getProductCountInCart()throws Exception{
		int count=0;

		if(Utils.waitForElement(driver, divSingleCartRow)){
			count=divCartRow.size();
		}
		else{
			count=0;
		}

		return count;
	}


	/**
	 * To verify Empty cart 
	 * @return true - if cart it empty
	 * @throws Exception - Exception
	 */
	public boolean verifyEmptyCart()throws Exception{
		boolean flag=false;
		if(Utils.waitForElement(driver, divCartEmpty)){
			flag=true;
		}
		return flag;
	}

	/**
	 * To Sign out the User Accont
	 * @return SignIn Page object
	 * @throws Exception - Exception
	 */
	public SignIn signOutAccount() throws Exception{

		BrowserActions.scrollToTopOfPage(driver);
		headers.mouseOverMyAccount();
		BrowserActions.clickOnElementX(lnkSignOut, driver, "Serach icon");
		Utils.waitForPageLoad(driver);

		return new SignIn(driver).get();
	}


	/**
	 * To get Checkout Id in cart page
	 * @return String of Checkout ID
	 * @throws Exception - Exception
	 */
	public String getCheckoutID()throws Exception{
		return lblCheckoutID.getText().trim();
	}

	/**
	 * To click on paypal Button
	 * @return PaypagePage page object
	 * @throws Exception - Exception
	 */
	public PaypalPage clickOnPaypalButton()throws Exception{
		String currentWindowHandle=driver.getWindowHandle();
		System.out.println(currentWindowHandle);
		BrowserActions.clickOnElementX(btnPaypal, driver, "Paypal Button");
		Set<String> openWindowsList=driver.getWindowHandles();        
		String popUpWindowHandle=null;
		for(String windowHandle:openWindowsList)
		{
			if (!windowHandle.equals(currentWindowHandle))
				popUpWindowHandle=windowHandle;
		}
		driver.switchTo().window(popUpWindowHandle);  
		System.out.println(popUpWindowHandle);
		Utils.waitForPageLoad(driver);
		return new PaypalPage(driver).get();
	}


	/**
	 * To click on Bonus product for Adding
	 * @throws Exception - Exception
	 */
	public void clickOnBonusProductAdd()throws Exception{
		BrowserActions.clickOnElementX(btnBonusProductAdd, driver, "Bonus Product Add");		
	}
	//learnmorelinkPlcc

	/**
	 * To click on Add To Wishlist
	 * @throws Exception - Exception
	 */
	public void clickOnAddToWishlist()throws Exception{
		if(Utils.waitForElement(driver, lnkAddToWishlist)) {
			BrowserActions.clickOnElementX(lnkAddToWishlist, driver, "Add To Wishlist");
			Utils.waitForPageLoad(driver);
		}
	}



	/**
	 * To click on Learnmore linkplcc
	 * @throws Exception - Exception
	 */
	public void clickOnLearnmorelinkPlcc()throws Exception{
		Utils.waitForElement(driver, learnmorelinkPlcc);
		BrowserActions.clickOnElementX(learnmorelinkPlcc, driver, "Learn More Link");		
	}

	/**
	 * To select Bonus product In modal
	 * @param index - 
	 * @return String - 
	 * @throws Exception - Exception
	 */
	public String selectBonusProductInModal(int index)throws Exception{		
		BrowserActions.clickOnElementX(chkBonusProductSelection.get(index-1), driver, "Bonus Product Selection");
		return BrowserActions.getText(driver, chkBonusProductSelection.get(index-1), "Bonus Product Selection");
	}


	/**
	 * To click on Bonus product to Add ot Bag
	 * @throws Exception - Exception
	 */
	public void clickOnBonusProductAddToBag()throws Exception{
		BrowserActions.clickOnElementX(btnBonusProductAddToBag, driver, "Bonus Product Add to bag");	
	}


	/**
	 * To get Bonus item price
	 * @return String - 
	 * @throws Exception - Exception
	 */
	public String getBonusItemPrice()throws Exception{
		return BrowserActions.getText(driver, bonusItemPrice, "Bonus Item Price");		
	}


	/**
	 * To click on bonus productEdit link
	 * @throws Exception - Exception
	 */
	public void clickOnBonusProductEdit()throws Exception{
		BrowserActions.scrollToViewElement(btnBonusEdit, driver);
		BrowserActions.clickOnElementX(btnBonusEdit, driver, "Bonus Product Edit");		
	}


	/**
	 * To Enter promo code in checkout page
	 * @param promoCode - 
	 * @throws Exception - Exception
	 */
	public void enterPromoCode(String promoCode)throws Exception{
		txtPromoCode.clear();
		txtPromoCode.sendKeys(promoCode);
	}

	/**
	 * To click on Apply coupon button
	 * @throws Exception - Exception
	 */
	public void clickOnApplycouponButton()throws Exception{
		BrowserActions.clickOnElementX(btnApplycoupon, driver, "Apply");		
	}


	/**
	 * To click on click on coupon Tooltip
	 * @throws Exception - Exception
	 */
	public void clickOnCouponToolTip()throws Exception{
		BrowserActions.clickOnElementX(couponToolTip, driver, "Coupon Tool Tip");	
	}


	/**
	 * To click on Shipping cost tool tip
	 * @throws Exception - Exception
	 */
	public void clickOnCouponRemoveButton()throws Exception{
		BrowserActions.clickOnElementX(btnRemoveButton, driver, "Coupon Tool Tip");	
	}

	/**
	 * To remove all applied coupon codes
	 * @throws Exception - Exception
	 */
	public void removeAllAppliedCoupons() throws Exception{
		int size = btnCouponRemoveButton.size();
		for(int index=size-1; index>=0; index--) {
			BrowserActions.clickOnElementX(btnCouponRemoveButton.get(index), driver, "Coupon Tool Tip");
		}
	}


	/**
	 * To click on coupon tool tip close
	 * @throws Exception - Exception
	 */
	public void clickOnCouponToolTipclose()throws Exception{
		Utils.waitForElement(driver, couponToolTipContent);
		Utils.waitForElement(driver, btnCouponToolTipClose);
		BrowserActions.clickOnElementX(btnCouponToolTipClose, driver, "Coupon Tool Tip Close");
	}


	/**
	 * To click on clickPlcc Expand
	 * @throws Exception - Exception
	 */
	public void clickplccExpand()throws Exception{
		BrowserActions.clickOnElementX(expandclosePLCCcontentslot, driver, "Expand PLCC content slot");

	}


	/**
	 * To click on Shopping Cart tool tip
	 * @return true - if Shopping Cart Tool Tip opened
	 * @throws Exception - Exception
	 */
	public boolean clickOnShoppingCartToolTip()throws Exception{
		boolean flag=false;

		BrowserActions.clickOnElementX(spanCostToolTip, driver, "Shopping cost Tool Tip");
		if(Utils.waitForElement(driver, spanCostToolTip)){
			flag=true;
		}
		return flag;
	}

	/**
	 * To Verify sales Tax
	 * @param salesTax -
	 * @return true -  if sales tax equals
	 * @throws Exception - Exception
	 */
	public boolean verifySalesTax(int salesTax )throws Exception{
		boolean flag=false;
		String sales= txtSalesTaxValue.getText();
		int ExactValue=Integer.parseInt(sales.split("\\$")[1]);

		if(ExactValue==salesTax){
			flag=true;
		}

		return flag;

	}

	/**
	 * To Verify To total savings
	 * @return true - if total savings equals
	 * @throws Exception - Exception
	 */
	public boolean verifyTotalSavings()throws Exception{
		boolean flag=false;
		double discount=Double.parseDouble(divDicountTotal.getText().split("\\$")[1]);
		double discountShipping=Double.parseDouble(divShippingDicountTotal.getText().split("\\$")[1]);

		String divTotalSavings=(divShippingDicountTotal.getText().split("\\$")[1]);
		double divTotalSavingsDouble=Double.parseDouble(divTotalSavings.split("\\)")[0]);

		if(divTotalSavingsDouble==(discount+discountShipping)){
			flag=true;
		}

		return flag;
	}
	/**
	 * To remove Wishlist All product
	 * @throws Exception - Exception
	 */
	public void removeWishlishAllProduct()throws Exception{

		BrowserActions.scrollToTopOfPage(driver);
		headers.mouseOverMyAccount();
		BrowserActions.clickOnElementX(lnkWishlist, driver, "WishList icon");
		Utils.waitForPageLoad(driver);

		BrowserActions.clickOnElementX(removeWishlist, driver, "WishList item");

	}
	/**
	 * To get product Brand
	 * @param index -
	 * @return String of Current Brand name
	 * @throws Exception - Exception
	 */
	public String getProductBrand(int index)throws Exception{

		String brandNameExtend="";
		String brandName=quickOrderbrandCount.get(index).getAttribute("src");
		if(brandName.contains("jl")){
			brandNameExtend="Jessica";
		}else if(brandName.contains("rm")){
			brandNameExtend="Roman";
		}else if (brandName.contains("ww"))
			brandNameExtend="Women";
		Log.message("Brand full name is: "+brandNameExtend);

		return brandName;
	}

	/**
	 * To add choice bonus product
	 * @param index -
	 * @param productColor -
	 * @param productSize -
	 * @throws Exception - Exception
	 */
	public void addChoiceBonusProduct(int index, String productColor, String productSize) throws Exception{

		if(Utils.waitForElement(driver,lblBonusProductCallout)){
			Utils.waitForElement(driver, lnkAddBonusProduct);
			BrowserActions.clickOnElementX(lnkAddBonusProduct, driver, "Add Bonus product button");
			if(Utils.waitForElement(driver, popUpBonusProduct)){

				BrowserActions.clickOnElementX(checkbonusProducts.get(index), driver, "Bonus CheckBox");
				WebElement elementSize1= bonusProducts.get(index).findElement(By.cssSelector(".selecting.attr-size:nth-child(1)"));
				BrowserActions.clickOnElementX(elementSize1, driver, "size dropdown");
				List<WebElement> elementSize= bonusProducts.get(index).findElements(By.cssSelector(".value.attributeinfo .swatches.size .selectable a"));
				for(int in=0;in<elementSize.size();in++){
					if(elementSize.get(in).getAttribute("title").contains(productSize)){
						BrowserActions.clickOnElementX(elementSize.get(in), driver, "size dropdown");
						break;
					}
					else
						Log.message(productColor + " . Color is not displayed in the dropdown  ");
				}

				WebElement elementColor1= bonusProducts.get(index).findElement(By.cssSelector(".selecting.attr-color:nth-child(1)"));
				BrowserActions.clickOnElementX(elementColor1, driver, "size dropdown");
				List<WebElement> elementColor= bonusProducts.get(index).findElements(By.cssSelector(".value.attributeinfo .swatches.color .selectable a"));
				for(int in=0;in<elementColor.size();in++){
					if(elementColor.get(in).getAttribute("title").contains(productColor)){
						BrowserActions.clickOnElementX(elementColor.get(in), driver, "size dropdown");
						break;
					}
					else
						Log.message(productColor + " . Color is not displayed in the dropdown  ");
				}

			}
			else{
				Log.message("Bonus product Pop up is not open");
			}
			if(Utils.waitForElement(driver,btnAddtoCartBonus)){
				BrowserActions.clickOnElementX(btnAddtoCartBonus, driver, "Add to cart button");	
				Utils.waitForPageLoad(driver);
			}else{
				Log.message("Add to Cart button is not displayed in the bonus product pop up layout");
			}


		}else{
			Log.message("Choose Bonus product layout is not displayed");
		}


	}

	/**
	 * To number of products displayed in recently view section
	 * @return Integer - 
	 * @throws Exception -
	 */
	public int getNoOfProdDisplayInRecentView() throws Exception {
		Log.event("Number of Products Recently Viewed :: " + recentlyViewedProdName.size());
		return recentlyViewedProdName.size();
	}

	/**
	 * To Edit choice bonus product
	 * @param index -
	 * @param productColor -
	 * @param productSize -
	 * @throws Exception -
	 */
	public void EditChoiceBonusProduct(int index, String productColor, String productSize) throws Exception{


		if(Utils.waitForElement(driver, popUpBonusProduct)){
			if(!checkbonusProducts.get(index).getAttribute("checked").equals("true")){
				BrowserActions.clickOnElementX(checkbonusProducts.get(index), driver, "Bonus CheckBox");
			}

			WebElement elementSize1= bonusProducts.get(index).findElement(By.cssSelector(".selecting.attr-size:nth-child(1)"));
			BrowserActions.clickOnElementX(elementSize1, driver, "size dropdown");
			List<WebElement> elementSize= bonusProducts.get(index).findElements(By.cssSelector(".value.attributeinfo .swatches.size .selectable a"));
			for(int in=0;in<elementSize.size();in++){
				if(elementSize.get(in).getAttribute("title").contains(productSize)){
					BrowserActions.clickOnElementX(elementSize.get(in), driver, "size dropdown");
					break;
				}	
			}
			WebElement elementColor1= bonusProducts.get(index).findElement(By.cssSelector(".selecting.attr-color:nth-child(1)"));
			BrowserActions.clickOnElementX(elementColor1, driver, "size dropdown");

			List<WebElement> elementColor= bonusProducts.get(index).findElements(By.cssSelector(".value.attributeinfo .swatches.color .selectable a"));
			for(int in=0;in<elementColor.size();in++){
				if(elementColor.get(in).getAttribute("title").contains(productColor)){
					BrowserActions.clickOnElementX(elementColor.get(in), driver, "size dropdown");
					break;
				}

			}
		}
		else{
			Log.message("Bonus product Pop up is not open");
		}
		if(Utils.waitForElement(driver,btnAddtoCartBonus)){
			BrowserActions.clickOnElementX(btnAddtoCartBonus, driver, "Add to cart button");	
		}else{
			Log.message("Add to Cart button is not displayed in the bonus product pop up layout");
		}
		Utils.waitForPageLoad(driver);
	}
	public CheckoutPage clickOnContinueButton(String guestEmailAddress) throws Exception{
		Utils.waitForPageLoad(driver);
		BrowserActions.typeOnTextField(emailInputField,guestEmailAddress, driver, "order Number Field");
		BrowserActions.clickOnElementX(btnContinueGuest, driver, "Checkout as Guest");
		Utils.waitForPageLoad(driver);
		return new CheckoutPage(driver).get();	
	}  


	/**
	 * To get product sku list
	 * @return List of String
	 * @throws Exception - Exception
	 */
	public List<String> getProductSkuList()throws Exception{
		List<String> productSku = new ArrayList<String>();
		for(WebElement sku : lstProductSku){
			productSku.add(sku.getText().replace("#", ""));
		}
		return productSku;
	}

	/**
	 * To click on continuous arrow in cart page
	 * @param direction -
	 * @param count -
	 * @throws Exception -
	 */
	public void clickOnArrowContious(String direction,int count)throws Exception{

		if(direction.equals("down")){

			for(int i=0;i<count;i++){
				BrowserActions.clickOnElementX(divDecreaseArrowEnabled,driver,"Quantity Down arrow");	
				Utils.waitForPageLoad(driver);
			}

		}else if(direction.equals("up")){
			for(int i=0;i<count;i++){
				BrowserActions.clickOnElementX(divIncreaseArrowEnabled,driver,"Quantity Up arrow");	
				Utils.waitForPageLoad(driver);
			}



		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on close button in shipping cost tool in shopping cart page
	 * @throws Exception -
	 */
	public void clickOnShippingCostToolClose() throws Exception{
		BrowserActions.clickOnElementX(shippingCostOverlayClose, driver, "Shipping cost tool close");
	}

	/**
	 * To get product color variation value by index
	 * @param index -
	 * @return String -
	 * @throws Exception -
	 */
	public String getProductColorVariation(int index)throws Exception{
		WebElement color = driver.findElements(By.cssSelector("#cart-items-form div.cart-row")).get(index).findElement(By.cssSelector("div[data-attribute='color'] .value"));
		return color.getText();
	}

	/**
	 * To get product Suze variation value by index
	 * @param index -
	 * @return String -
	 * @throws Exception -
	 */
	public String getProductSizeVariation(int index)throws Exception{
		WebElement size = driver.findElements(By.cssSelector("#cart-items-form div.cart-row")).get(index).findElement(By.cssSelector("div[data-attribute='size'] .value"));
		return size.getText().trim();
	}

	/**
	 * To get product name value by index
	 * @param index -
	 * @return String -
	 * @throws Exception -
	 */
	public String getProductName(int index)throws Exception{
		WebElement name = driver.findElements(By.cssSelector("#cart-items-form div.cart-row")).get(index).findElement(By.cssSelector(".name a"));
		return name.getText();
	}

	/**
	 * To get product quantity value by index
	 * @param index -
	 * @return String -
	 * @throws Exception -
	 */
	public String getProductQuantity(int index)throws Exception{
		WebElement qty = driver.findElements(By.cssSelector("#cart-items-form div.cart-row")).get(index).findElement(By.cssSelector(".item-quantity input"));
		return qty.getAttribute("value").trim();
	}

	/**
	 * To get product price value by index
	 * @param index -
	 * @return String -
	 * @throws Exception -
	 */
	public String getProductPrice(int index)throws Exception{
		WebElement qty = driver.findElements(By.cssSelector("#cart-items-form div.cart-row")).get(index).findElement(By.cssSelector(".item-total span[class*='price-total']"));
		return qty.getText().trim();
	}

	/**
	 * To get product details as list
	 * @return LinkedList -
	 * @throws Exception -
	 */
	public LinkedList<LinkedHashMap<String, String>> getProductDetails()throws Exception{
		LinkedList<LinkedHashMap<String, String>> dataToReturn = new LinkedList<LinkedHashMap<String, String>>();

		for(int i = 0; i < divCartRow.size(); i++) {
			LinkedHashMap<String, String> product = new LinkedHashMap<String, String>();
			try { product.put("Name", getProductName(i)); }catch(NoSuchElementException e) {product.put("Name", "");}
			try { product.put("Color", getProductColorVariation(i)); }catch(NoSuchElementException e) {product.put("Color", "");}
			try { product.put("Size", getProductSizeVariation(i)); }catch(NoSuchElementException e) {product.put("Size", "");}
			try { product.put("Quantity", getProductQuantity(i)); }catch(NoSuchElementException e) {product.put("Quantity", "");}
			try { product.put("Price", getProductPrice(i)); }catch(NoSuchElementException e) {product.put("Price", "");}
			dataToReturn.add(product);
		}

		return dataToReturn;
	}

	/**
	 * To update size of the product based on product index	
	 * @param index - index
	 * @return String - updated size
	 * @throws Exception - Exception
	 */
	public String updateSizeByPrdIndex(int index, int sizeIndex)throws Exception{
		Object obj = clickOnEditLink(index);
		String size;
		if(Utils.isDesktop()) {
			QuickShop qsModal = (QuickShop) obj;
			size = qsModal.selectSize("random").trim();
			BrowserActions.clickOnElement(qsModal.btnAddToBag, driver, "Update button");

		}else {
			PdpPage pdpPage = (PdpPage) obj;
			size = pdpPage.selectSizeBasedOnIndex(sizeIndex);
			pdpPage.clickOnUpdate();
		}

		Utils.waitForPageLoad(driver);
		return size;
	}

	/**
	 * To update quantity of the product based on product index	
	 * @param index - index
	 * @return String - updated quantity
	 * @throws Exception - Exception
	 */
	public String updateQuantityByPrdIndex(int index, String qtyToUpdate)throws Exception{
		Object obj = clickOnEditLink(index);
		String qty;
		if(Utils.isDesktop()) {
			QuickShop qsModal = (QuickShop) obj;
			qty = qsModal.selectQty(qtyToUpdate).trim();
			BrowserActions.clickOnElement(qsModal.btnAddToBag, driver, "Update button");
		}else {
			PdpPage pdpPage = (PdpPage) obj;
			qty = pdpPage.selectQty(qtyToUpdate).trim();
			pdpPage.clickOnUpdate();
		}
		Utils.waitUntilElementDisappear(driver, popUpBonusProduct);
		return qty;
	}

	/**
	 * To get name list of products in shopping bag.	
	 * @return List<String> - Names of products
	 * @throws Exception - Exception
	 */
	public List<String> getCartItemNameList() throws Exception{
		List<String> listCartItemNames = new ArrayList<String>();
		for(WebElement cartItem: lstCartProductNames) {
			listCartItemNames.add(cartItem.getText().trim().toLowerCase());
		}
		return listCartItemNames;
	}

	/**
	 * To verify that availability is not shown for OOS item	
	 * @return boolean- 
	 * @throws Exception - Exception
	 */
	public boolean verifyAvailabilityMessageForOOS() throws Exception{
		for(WebElement cartItem: divCartRow) {
			WebElement quantity = cartItem.findElement(By.cssSelector(".item-quantity"));
			WebElement availability = cartItem.findElement(By.cssSelector(".product-availability-list"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			String quantityText = (String) executor.executeScript("return window.getComputedStyle(arguments[0], '::before').getPropertyValue('content')", quantity);
			if(quantityText.contains("OUT OF STOCK") && availability.isDisplayed()) {
				return false;
			}
		}
		return true;
	}

	public void clickCloseModal() throws Exception{
		BrowserActions.clickOnElementX(btnCloseEditModal, driver, "Close edit modal");
	}

	public PlpPage clickShopNewArrivalButton() throws Exception{
		BrowserActions.clickOnElement(btnShopNewArriaval, driver, "Shop new Arrivals");
		return new PlpPage(driver).get();
	}

	public PdpPage clickTrendNowImage() throws Exception{
		BrowserActions.clickOnElement(trendNowImage, driver, "Trend now Image");
		return new PdpPage(driver).get();
	}
	
	/**
	 * To verify quantity modification arrow position
	 * @param index - index(optional)
	 * @return boolean - true/false if arrow are positioned correctly
	 * @throws Exception - Exception
	 */
	public boolean verifyQuanityEditArrowposition(int  ...index)throws Exception{
		WebElement itemQty = null;
		if(index.length > 0) {
			itemQty = listProductQuantity.get(index[0]);
		}else {
			itemQty = listProductQuantity.get(Utils.getRandom(0, getProductCountInCart()-1));
		}
		WebElement arrowIncrease = itemQty.findElement(By.xpath("..")).findElement(By.cssSelector(".qty-increase"));
		WebElement arrowDecrease = itemQty.findElement(By.xpath("..")).findElement(By.cssSelector(".qty-decrease"));
		if(Utils.isMobile()) {
			return (BrowserActions.verifyHorizontalAllignmentOfElements(driver, arrowIncrease, itemQty)
					&& BrowserActions.verifyHorizontalAllignmentOfElements(driver, itemQty, arrowDecrease));
		}else {
			return (BrowserActions.verifyVerticalAllignmentOfElements(driver, arrowIncrease, arrowDecrease)
					&& BrowserActions.verifyHorizontalAllignmentOfElements(driver, arrowIncrease, itemQty)
					&& BrowserActions.verifyHorizontalAllignmentOfElements(driver, arrowDecrease, itemQty));
		}
	}


	/**
	 * To check quickshop is displayed in recently viewed
	 * @return boolean - true if Quickshop is displayed, false if not
	 * @throws Exception - Exception
	 */
	public boolean checkQuickShopIsDisplayedInRecommendation()throws Exception{
		BrowserActions.scrollInToView(tileRecommendationProd, driver);
		BrowserActions.mouseHover(driver, tileRecommendationProd, "Product Tile");
		WebElement qcButton = tileRecommendationProd.findElement(By.cssSelector("#quickviewbutton"));
		if (Utils.waitForElement(driver, qcButton)) {
			return true;
		}
		return false;
	}

	/**
	 * To click on quickshop button
	 * @return QuickShop -
	 * @throws Exception -
	 */
	public QuickShop clickOnQuickShop()throws Exception{		
		WebElement prdTile = null;
		prdTile = driver.findElements(By.cssSelector(".you-may-like.loaded .product-tile")).get(0);
		BrowserActions.scrollInToView(prdTile, driver);
		BrowserActions.mouseHover(driver, prdTile, "Product Tile");
		WebElement qcButton = prdTile.findElement(By.cssSelector(".quickview"));
		if (Utils.waitForElement(driver, qcButton)) {
			BrowserActions.javascriptClick(qcButton, driver, "Quick Shop Button for First Product");
		}
		return new QuickShop(driver).get();
	}

	/**
	 * To click on the product image in the recommendation section 
	 * @param index - index of the product
	 * @throws Exception - Exception
	 */
	public String clickRecommendationPrdImageByIndex(int index) throws Exception {
		String name;
		name = BrowserActions.getText(driver, prodNameListRecommendation.get(index), "Recommendation Prod Name");
		BrowserActions.clickOnElementX(prodImageListRecommendation.get(index), driver, "Recommedation prduct image");

		Utils.waitForPageLoad(driver);
		return name.trim().replace("amp;", "");
	}
	
	/**
	 * To get Shopping bag ID
	 * @return String - bagID
	 * @throws Exception - Exception
	 */
	public String getShoppingBagID()throws Exception{
		if(Utils.waitForElement(driver, lnkShowBagID)) {
			BrowserActions.clickOnElementX(lnkShowBagID, driver, "Show bag ID");
			Utils.waitForPageLoad(driver);
		}
		return spanBagID.getText();
	}
}