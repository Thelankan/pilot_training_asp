package com.fbb.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class CustomerGalleryModalPage extends LoadableComponent<CustomerGalleryModalPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public HomePage homePage;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	public MiniCartPage minicart;
	String runPltfrm = Utils.getRunPlatForm();

	/**********************************************************************************************
	 ********************************* WebElements of CustomerGalleryModal Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = "#TT-vc-modal-container")
	WebElement customerGalleryModal;
	
	@FindBy(css = ".TT-icon-thumbs-up")
	WebElement thumpsUpIcon;
	
	@FindBy(css = ".TT-icon-flag")
	WebElement flagIcon;
	
	@FindBy(css = ".TT-icon-pinterest")
	WebElement pintrestShareIcon;
	
	@FindBy(css = ".TT-icon-facebook")
	WebElement facebookShareIcon;
	
	@FindBy(css = ".TT-icon-twitter")
	WebElement twitterShareIcon;
	
	@FindBy(css = ".TT-close-icon")
	WebElement btnCloseModal;
	
	/**********************************************************************************************
	 ********************************* WebElements of CustomerGalleryModal Page - Ends ****************************
	 **********************************************************************************************/

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public CustomerGalleryModalPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, customerGalleryModal))) {
			Log.fail("Customer Gallery Modal Page did not open up. Site might be down.", driver);
		}

		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	/**
	 * To get number of products in product set
	 * @return Integer - 
	 * @throws Exception -
	 */
	public void closeCustomerGalleryModal() throws Exception {
		BrowserActions.clickOnElementX(btnCloseModal, driver, "Close Button");
	}

}