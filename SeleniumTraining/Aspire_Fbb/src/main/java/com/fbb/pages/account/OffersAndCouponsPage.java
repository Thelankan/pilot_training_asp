package com.fbb.pages.account;

import java.util.LinkedHashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class OffersAndCouponsPage extends LoadableComponent<OffersAndCouponsPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;
	
	public final LinkedHashMap<String, String> accountDetails = new LinkedHashMap<String, String>();

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(css = ".pt_offerscoupons")
	WebElement readyElement;
	
	@FindBy(xpath = "//h2[contains(text(),'Offers & Coupons')]/parent::div//div[contains(@class, 'oc-offer-content')]")
	List<WebElement> offerContent;
	
	@FindBy(css = ".slick-dots li")
	List<WebElement> offerContentInOverview;
	
	//================================================================================
	//			WebElements Declaration End
	//================================================================================




	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public OffersAndCouponsPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Offer and Coupons page is not loaded", driver);
		}
		
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}
	
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}
	
	/**
	 * To get Number of offers
	 * @return offer content size
	 * @throws Exception - Exception
	 */
	public int getNoOfOffers() throws Exception{
		return offerContent.size();
	}
	
	/**
	 * To get Number of offers in overview
	 * @return offer content size
	 * @throws Exception - Exception
	 */
	public int getNoOfOffersInOverview() throws Exception{
		return offerContentInOverview.size();
	}
	
}
