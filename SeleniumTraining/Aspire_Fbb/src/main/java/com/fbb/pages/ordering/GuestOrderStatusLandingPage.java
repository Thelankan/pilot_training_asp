package com.fbb.pages.ordering;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class GuestOrderStatusLandingPage extends LoadableComponent <GuestOrderStatusLandingPage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;
	
	/**********************************************************************************************
	 ********************************* WebElements of Guest Order Status Landing Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = ".pt_order")
	WebElement readyElement;
	
	@FindBy(css = ".pt_account.null")
	WebElement Lnk;
	
	@FindBy(css = ".pt_order")
	WebElement lnkGuestorderStatusPage;
	
	@FindBy(css = ".order-details-section .orderdetails [class='actions'] a")
	WebElement btnReturn;
	
	@FindBy(css = ".promo-banner")
	WebElement cntPromoBanner;
	
	@FindBy(css = "div.breadcrumb")
	WebElement cntBreadCrumb;
	
	@FindBy(css = ".breadcrumb .hide-mobile")
	List<WebElement> lblBreadCrumb;
	
	@FindBy(xpath = "//a[contains(@class,'breadcrumb-element')][contains(text(),'Check An Order')]")
	WebElement lnkBreadCrumbCheckAnOrder;
	
	@FindBy(css = "a[title='Go to Home']")
	WebElement lnkBreadCrumbHome;
	
	@FindBy(css = ".breadcrumb-desktop-label")
	WebElement lnkBreadCrumbHomeMobile;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement lnkBreakCrumbMobile;
	
	@FindBy(css = ".return-line-items")
	WebElement sectionReturnItems;
	
	@FindBy(css = ".return-sub-header")
	WebElement lblReturnSubHeader;
	
	@FindBy(css = ".return-message")
	WebElement lblReturnMessage;
	
	@FindBy(css = ".return-sub-header")
	WebElement lblSpecialCalloutCopy;
	
	@FindBy(css = ".return-cancel")
	WebElement btnCancel;
	
	@FindBy(name = "dwfrm_ordertrack_returnorder")
	WebElement btnReturnSelectedItems;
	
	@FindBy(css = ".eligible-list .header-msg.heading")
	WebElement lblEligibleListHeader;
	
	@FindBy(css = ".returns-check")
	List<WebElement> chkItemListCheckBox;
	
	@FindBy(css = ".list-tems .cart-columns")
	List<WebElement> lstProductDetail;
	
	
	/**********************************************************************************************
	 ********************************* WebElements of EasyReturnsPage - Ends ****************************
	 **********************************************************************************************/

	

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public GuestOrderStatusLandingPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Page did not open up. Site might be down.", driver);
		}
		
		elementLayer = new ElementLayer(driver);
		headers = new Headers(driver);
		footers = new Footers(driver);

	}// isLoaded
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Return 
	 * @throws Exception -
	 */
	public void clickOnReturn()throws Exception{
		BrowserActions.clickOnElementX(btnReturn, driver, "Return button");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To verify breadcrumb value
	 * @param checkString -
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyBreadCrumb(String checkString)throws Exception{
		String compareString = new String();
		for(WebElement ele : lblBreadCrumb) {
			compareString += ele.getAttribute("innerHTML").trim();
		}
		
		Log.event("String from Element :: " + compareString);
		Log.event("Text From User :: " + checkString);
		
		return (compareString.equals(checkString))? true : false;
	}

	/**
	 * To click on Check And Order link in Breadcrumb
	 * @return SignIn 
	 * @throws Exception - Exception
	 */
	public SignIn clickOnCheckAnOrderInBC()throws Exception{
		BrowserActions.clickOnElementX(lnkBreadCrumbCheckAnOrder, driver, "Check An Order in Breadcrumb");
		return new SignIn(driver).get();
	}
	
	/**
	 * To click on Home link in Breadcrumb
	 * @return SignIn 
	 * @throws Exception - Exception
	 */
	public HomePage clickOnHomeInBC()throws Exception{
		if(!Utils.isMobile()) {
			BrowserActions.clickOnElementX(lnkBreadCrumbHome, driver, "Home in Breadcrumb");
		}else {
			BrowserActions.clickOnElementX(lnkBreadCrumbHomeMobile, driver, "Home in Breadcrumb");
		}
		return new HomePage(driver).get();
	}
}
