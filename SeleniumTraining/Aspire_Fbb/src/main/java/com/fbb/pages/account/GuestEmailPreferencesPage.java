package com.fbb.pages.account;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class GuestEmailPreferencesPage extends LoadableComponent <GuestEmailPreferencesPage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;
	
	@FindBy(css = ".pt_account.null")
	WebElement readyElement;

	@FindBy(name = "emailsubscribeguest-emailmarketing")
	WebElement btnSubmit;
	
	@FindBy(name = "emailsubscribeguest-unsubscribe")
	WebElement btnGuestEmailUnsubscribeSubmit;
	
	@FindBy(css = ".solid-wine-berry")
	WebElement emailSubmit;

	@FindBy(css = ".mainheader")
	WebElement lblEmailPreferencesTitle;

	@FindBy(css = ".sub-head1")
	WebElement lblEmailPreferencesSubHeading;

	@FindBy(css = ".email-unsubscribe-guest .email.required .label-text")
	WebElement txtEmailPlaceHolder;

	@FindBy(css = ".email-unsubscribe-guest .email.required span[id$='error']")
	WebElement txtEmailPlaceHolderError;

	@FindBy(css = "input[id^='dwfrm_emailpreferences_email_']")
	WebElement txtEmail;
	
	@FindBy(css = "div.qa-section")
	WebElement divQuestionAnswerSection;
	
	@FindBy(css = ".qa-content")
	List<WebElement> divQuestionAnswers;
	
	@FindBy(css = ".qa-content > a")
	List<WebElement> lnkQuestion;
	
	@FindBy(css = ".answer")
	List<WebElement> txtAnswer;
	
	@FindBy(css = ".breadcrumb-element.hide-mobile")
	List<WebElement> lstBreadCrumbElementsDesktop;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement txtBreadCrumbElementMobile;
	
	@FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
	WebElement imgBreadCrumbBackArrowMobile;
	
	@FindBy(css = "[title='Go to Home']")
	WebElement homeBreadcrum;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop")
	WebElement homeBreadcrumbMobile;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement emailPreferenceBreadcrum;
	
	@FindBy(css = ".unsubscribe-confirmation-email-thankyou h1.mainheader")
	WebElement headerText;
	
	@FindBy(css = ".unsubscribe-confirmation-email-thankyou .email-unauthenticatedunsubscribe-subhead")
	WebElement subheadText;
	
	@FindBy(css = ".field-wrapper .form-row.email.required input")
	WebElement emailAddressTxtField;
	
	@FindBy(css = ".email-unsubscribe-guest.subscribe-emailid.make-label-absolute .form-row.form-row-button button")
	WebElement submitButton;
	
	@FindBy(css = ".field-wrapper .form-row.email.required .label-text")
	WebElement emailAddressPlaceHolder;
	
	@FindBy(css = ".form-row.email.required.error-handle .input-focus")
	WebElement emailAddressErrorPlaceHolder;
	
	@FindBy(css = ".form-row.email.required.error-handle .error ")
	WebElement emailAddressBlankErrorPlaceHolder;
	
	@FindBy(css = ".qa-section .qa-desc .qa-content>a")
	List <WebElement> questionContent;
	
	@FindBy(css = ".custom-radio-wrapper.fewercontactsrequested-radio input")
	List <WebElement> preferenceRadioBtn;

	@FindBy(css = "div > p.disclaimer")
	WebElement txtDisclaimer;

	@FindBy(css = ".email-preference-row.currentsite-brand")
	WebElement divCurrentBrand;
	
	@FindBy (css = ".otherbrands-divider")
	WebElement otherBrandDivider;
	
	@FindBy(css= ".fewercontactsrequested .custom-radio-icon")
	WebElement radioFewerEmailRequested;
	
	@FindBy(css= ".unsubscribe-email-radio label .custom-radio-icon")
	WebElement radioUnsubscribe;
	
	@FindBy(css = ".select-unsubscribe-reason .custom-radio-icon")
	List<WebElement> radioUnsubscribeReasons;
	
	@FindBy(css = ".unsubscribe-email.select-unsubscribe-reason")
	WebElement divUnsubscibeReasons;
	
	@FindBy(css = "button[name='emailsubscribe-overlay-submit']")
	WebElement btnOverlaySubmit;
	
	@FindBy(css = ".primary-content .unsubscribeconfirmation")
	WebElement divUnSubscribeConfirmation;
	
	@FindBy(css = ".promo-banner")
	WebElement divPromoBanner;
	
	@FindBy(css = ".modal-msg")
	WebElement modalHeader;
	
	@FindBy(css = "button[title='Close']")
	WebElement modalCloseIcon;
	
	@FindBy(css = ".fewercontactsrequested-radio .custom-radio-icon")
	WebElement radioModalLessFrequent;

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public GuestEmailPreferencesPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, lblEmailPreferencesTitle))) {
			Log.fail("Page did not open up. Site might be down.", driver);
		}
		
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}// isLoaded
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver,btnSubmit );
	}

	enum placeholders{
		normal,
		blank,
		invalid
	}
	/**
	 * Verify type email
	 * @param email - 
	 * @throws Exception - Exception
	 */
	public void typeEmail(String email) throws Exception{
		BrowserActions.typeOnTextField(txtEmail, email, driver, "Email");
	}
	/**
	 * Verify clicking the submit
	 * @return GuestEmail page object
	 * @throws Exception - Exception
	 */
	public GuestEmailPreferencesPage clickSubmit() throws Exception{
		BrowserActions.clickOnElementX(btnSubmit, driver, "Submit");
		Utils.waitForPageLoad(driver);
		return new GuestEmailPreferencesPage(driver).get();
	}
	
	/**
	 * Verify clicking the submit
	 * @return GuestEmail page object
	 * @throws Exception - Exception
	 */
	public GuestEmailPreferencesPage clickUnsubscribeSubmit() throws Exception{
		BrowserActions.clickOnElementX(btnGuestEmailUnsubscribeSubmit, driver, "Submit");
		Utils.waitForPageLoad(driver);
		return new GuestEmailPreferencesPage(driver).get();
	}
	
	/**
	 * Verify clicking the submit on modal
	 * @return GuestEmail page object
	 * @throws Exception - Exception
	 */
	public GuestEmailPreferencesPage clickModalSubmit() throws Exception{
		BrowserActions.clickOnElementX(btnOverlaySubmit, driver, "Submit");
		Utils.waitForPageLoad(driver);
		return new GuestEmailPreferencesPage(driver).get();
	}
	
	/**
	 * Verify clicking the close on modal
	 * @throws Exception - Exception
	 */
	public void clickModalClose() throws Exception{
		BrowserActions.clickOnElementX(modalCloseIcon, driver, "Modal Close");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * Verify clicking the submit
	 * @return GuestEmail page object
	 * @throws Exception - Exception
	 */
	public GuestEmailPreferencesPage clickEmailSubmit() throws Exception{
		BrowserActions.clickOnElementX(emailSubmit, driver, "Guest Email Submit");
		Utils.waitForPageLoad(driver);
		return new GuestEmailPreferencesPage(driver).get();
	}
	/**
	 * VErify the place holder
	 * @param placeHolderType - 
	 * @return Boolean values
	 * @throws Exception - Exception
	 */
	public Boolean verifyPlaceHolder(String placeHolderType) throws Exception{
		if(placeHolderType.equals(placeholders.normal.toString()))
		{
			if(txtEmailPlaceHolder.getText().trim().equalsIgnoreCase("Enter your email address"))
				return true;
			else
				return false;
		}
		else 	if(placeHolderType.equals(placeholders.blank.toString()))
		{
			if(txtEmailPlaceHolderError.getText().trim().equalsIgnoreCase("Enter your email address") && (txtEmailPlaceHolderError.getCssValue("color").contains("rgba(230, 0, 60, 1)") || txtEmailPlaceHolderError.getCssValue("color").contains("rgba(189, 3, 4, 1)")))
				return true;
			else
				return false;
		}
		else
		{
			if(txtEmailPlaceHolderError.getText().trim().toLowerCase().contains("please enter a valid email") && (txtEmailPlaceHolderError.getCssValue("color").contains("rgba(230, 0, 60, 1)") || txtEmailPlaceHolderError.getCssValue("color").contains("rgba(189, 3, 4, 1)")))
				return true;
			else
				return false;
		}
	}
	/**
	 * Verify the togglequestion
	 * @param index - 
	 * @param expand - 
	 * @throws Exception - Exception
	 */
	public void toggleQuestion(int index, Boolean expand) throws Exception{
		if(expand == true)
		{
			if(lnkQuestion.get(index).getAttribute("class").contains("active"))
				Log.message("Already expanded");
			else
				BrowserActions.clickOnElementX(lnkQuestion.get(index), driver, "expanded");
		}
		else
		{
			if(lnkQuestion.get(index).getAttribute("class").contains("active") == false)
				Log.message("Already collapsed");
			else
				BrowserActions.clickOnElementX(lnkQuestion.get(index), driver, "collapsed");
		}
	}
	/**
	 * VErify the answer is displayed
	 * @param index - 
	 * @return Boolean values
	 * @throws Exception - Exception
	 */
	public Boolean verifyAnswerDisplayed(int index) throws Exception{
		if(txtAnswer.get(index).getAttribute("class").contains("active"))
			return true;
		else
			return false;
	}
	/**
	 * Verify the breadcrum
	 * @return Boolean values
	 * @throws Exception - Exception
	 */
	public Boolean verifyBreadCrumb() throws Exception{
		List<String> actualValue = new ArrayList<String>();
		for (WebElement breadcrumb : lstBreadCrumbElementsDesktop) {
			actualValue.add(breadcrumb.getText().trim());
		}
		
		if(actualValue.equals(Arrays.asList("HOME", "EMAIL PREFERENCES")))
			return true;
		else
			return false;	
	}
	/**
	 * Verify clicking the breadcrum
	 * @return Homepage object
	 * @throws Exception - Exception
	 */
	public HomePage clickBreadCrumb() throws Exception{
			BrowserActions.clickOnElementX(lstBreadCrumbElementsDesktop.get(0), driver, "Home in breadcrumb");
			return new HomePage(driver).get();
	}
	/**
	 * Verify getting mobile breadcrum values 
	 * @return bread string
	 * @throws Exception - Exception
	 */
	public String getMobileBreadCrumb() throws Exception{
		return BrowserActions.getText(driver, txtBreadCrumbElementMobile, "Mobile Breadcrumb").trim();
	}
	/**
	 * VErify clicking back arrow of breadcrum
	 * @return Homepage object
	 * @throws Exception - Exception
	 */
	public HomePage clickBackArrowInBreadcrumb() throws Exception{
		BrowserActions.clickOnElementX(imgBreadCrumbBackArrowMobile, driver, "Back Arrow");
		return new HomePage(driver).get();
	}
	/**
	 * Verify email field is editable
	 * @return status boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailFieldEditable()throws Exception {
		String testEmailAddress= "test@yopmail.com";
		emailAddressTxtField.clear();
		emailAddressTxtField.sendKeys(testEmailAddress);
		String text = BrowserActions.getText(driver, emailAddressTxtField, "Get field value");
		if(text.isEmpty())
		{
			Log.message("Email field is not editable.");
			return false;
		}
		else if(text.equals(testEmailAddress))
			return true;
		else {
			Log.message("Email eneterd does not match source text.");
			return false;
		}
	}
	/**
	 * Verify clear email fields
	 * @throws Exception - Exception
	 */
	public void clearEmailField()throws Exception {
		emailAddressTxtField.clear();
		Log.message("clear");
		}
	/**
	 * Verify email blank error
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailFieldBlankError()throws Exception {
		return Utils.verifyCssPropertyForElement(emailAddressBlankErrorPlaceHolder, "color", "rgba(189, 3, 4, 1)");

	}
	/**
	 * Verify the email error message with incorrect values
	 * @return - 
	 * @throws Exception - Exception
	 */
	public String verifyEmailFieldErrorIncorrectValue() throws Exception{
		emailAddressTxtField.clear();
		emailAddressTxtField.sendKeys("emailgmail@com");
		BrowserActions.clickOnElementX(emailSubmit, driver, "Entered incorrect email address and Clicked on signup");
		Utils.waitForPageLoad(driver);
		return BrowserActions.getText(driver, emailAddressErrorPlaceHolder, "Error Message");

	}
	
	/**
	 * Verify the email error message with incorrect values
	 * @return - 
	 * @throws Exception - Exception
	 */
	public String verifyEmailFieldErrorIncorrectValue(String txtEmail) throws Exception{
		emailAddressTxtField.clear();
		emailAddressTxtField.sendKeys(txtEmail);
		BrowserActions.clickOnElementX(emailSubmit, driver, "Entered given value on email address field and Clicked on submit");
		Utils.waitForPageLoad(driver);
		return BrowserActions.getText(driver, txtEmailPlaceHolderError, "Error Message");

	}
	/**
	 * Verify the email placeholder moves up
	 * @return Status Boolean 
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailPlaceholderMovestotheTop()throws Exception {
		Boolean flag = true;
		emailAddressTxtField.clear();
		float y1=emailAddressPlaceHolder.getLocation().getY();
		emailAddressTxtField.sendKeys("email@gmail.com");
		float y2=emailAddressPlaceHolder.getLocation().getY();
		if(y2>y1)
			flag=true;
		return flag;
	}
	/**
	 * Verify the state drop is expand
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyExpandedState()throws Exception{
		Boolean flag=false;
		for(int i=0;i<questionContent.size();i++)
		{
			BrowserActions.clickOnElementX(questionContent.get(i), driver, "Expanded status of the Elements");
			if(questionContent.get(i).getAttribute("class").contains("active"))
			{
				Log.message("Question " + (i+1) +" is expandable");
				flag = true;
			}
			else
			{
				Log.message("Question " + (i+1) +" is not expandable");
				i=questionContent.size();
				flag = false;
			}
			
		}
			
		return flag;
	}
	/**
	 * Verify the state is collapsed
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyCollapsedState()throws Exception{
		Boolean flag=false;
		for(int i=0;i<questionContent.size();i++)
		{
			BrowserActions.clickOnElementX(questionContent.get(i), driver, "Expanded status of the Elements");
			BrowserActions.clickOnElementX(questionContent.get(i), driver, "Collapsed status of the Elements");
			if(questionContent.get(i).getAttribute("class").contains("active"))
			{
				Log.message("Question " + (i+1) +" is not collapsed");
				i=questionContent.size();
				flag = false;
			}
			else
			{
				Log.message("Question " + (i+1) +" is collapsed");
				flag = true;
			}
			
		}
			
		return flag;
	}
	
	/**
	 * Select Please email me less frequently radio button
	 * @throws Exception - Exception
	 */
	public void selectEmailLessFrequently() throws Exception{
		BrowserActions.clickOnElementX(radioFewerEmailRequested, driver, "Please email me less frequently radio");
	}
	
	/**
	 * Select Unsubscribe radio button
	 * @throws Exception - Exception
	 */
	public void selectUnsubscribe() throws Exception{
		BrowserActions.clickOnElementX(radioUnsubscribe, driver, "Unsubscribe radio");
	}
	
	/**
	 * Verify unsubscribe reason radio buttons
	 * @throws Exception - Exception
	 */
	public boolean verifyUnscribeReasonRadioButtons() throws Exception{
		selectUnsubscribe();
		for(WebElement unSubReason: radioUnsubscribeReasons) {
			BrowserActions.clickOnElementX(unSubReason, driver, "Unsubscribe reason");
			if(!unSubReason.getCssValue("background-image").contains("circle-selected"))
				return false;
		}
		return true;
	}
	
	/**
	 * Click Submit on confirm pop-up overlay
	 * @throws Exception - Exception
	 */
	public void clickOverlaySubmit() throws Exception{
		BrowserActions.clickOnElementX(btnOverlaySubmit, driver, "Submit button on overlay");
	}
	
	/**
	 * Click Unsub radio
	 * @throws Exception - Exception
	 */
	public void clickRadioUnSub() throws Exception{
		BrowserActions.clickOnElementX(radioUnsubscribe, driver, "Unsubscribe");
	}
	
	/**
	 * Click Fewer Email option
	 * @throws Exception - Exception
	 */
	public void clickRadioFewerEmail() throws Exception{
		BrowserActions.clickOnElementX(radioModalLessFrequent, driver, "Please email ne less frequently");
	}
	
	/**
	 * To verify open-close state of QA section 
	 * @throws Exception - Exception
	 */
	public boolean verifyQuestionAnswersClosed() throws Exception{
		for(WebElement qaElement: divQuestionAnswers) {
			if(qaElement.getAttribute("class").contains("active"))
				return false;
		}
		return true;
	}
	
	/**
	 * To verify Caret symbol in QA section 
	 * @throws Exception - Exception
	 */
	public boolean verifyQASectionCaretSymbol(Object obj) throws Exception{
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		for(WebElement Element: divQuestionAnswers) {
			WebElement qaElement = Element.findElement(By.className("question"));
			String actualValue = (String) executor.executeScript("return window.getComputedStyle(arguments[0], ':after').getPropertyValue('background-image')", qaElement);
			if(!actualValue.contains("carat"))
				return false;
		}
		return true;
	}
	
}
