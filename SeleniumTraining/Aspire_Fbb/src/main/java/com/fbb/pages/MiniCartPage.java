package com.fbb.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class MiniCartPage extends LoadableComponent<MiniCartPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	public static final String MINICART ="//*[@class='top-menu-utility']/div[@class='menu-utility']/div[@class='menu-utility-user']/div[@class='right-section']/div[@id='mini-cart']/div[@class='mini-cart-total']/div[@class='mini-cart-content']";
	
	public static final String MINICART_PRODUCT =MINICART +"/div[@class='mini-cart-products']/div[@class='mini-cart-product  ']";
	
	public static final String MINICART_PRODUCT_RIGHT=MINICART_PRODUCT +"/div[@class='mini-cart-products']/div[@class='mini-cart-product  ']/div[@class='right-content']/div[@class='mini-cart-product-info']";
	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(id = "mini-cart")
	WebElement miniCartContent;

	@FindBy(css = ".top-banner #mini-cart")
	WebElement miniCartIconMobile;
	
	@FindBy(css = ".top-banner #mini-cart .mini-cart-total .mini-cart-link .minicart-quantity")
	WebElement miniCartBagQuanity;
	

	@FindBy(css = "#mini-cart .mini-cart-content .mini-cart-totals-addtocartoverlay .mini-cart-subtotals .value")
	WebElement lblTotalCostInMinicartOverlay;
	
	@FindBy(xpath = ".//*[@class='top-menu-utility']/div[@class='menu-utility']/div[@class='menu-utility-user']/div[@class='right-section']/div[@id='mini-cart']/div[@class='mini-cart-total']/div[@class='mini-cart-content']")
	WebElement miniCartOverlay;
	
	@FindBy(xpath =MINICART+"/div[@class='mini-cart-products']/div[@class='mini-cart-product  ']/div[@class='left-content']/div[@class='mini-cart-image']/a/img")
	WebElement miniCartImage;
	
	@FindBy(xpath =MINICART+"/div[@class='mini-cart-products']/div[@class='mini-cart-product  ']/div[@class='right-content']/div[@class='mini-cart-product-info']/div[@class='mini-cart-name']/a")
	WebElement miniCartName;
	
	
	@FindBy(xpath =MINICART_PRODUCT_RIGHT+"/div[@class='mini-cart-attributes']/div[@data-attribute='size']/span[@class='value']")
	WebElement miniCartSizeVariation;
	
	@FindBy(xpath =MINICART_PRODUCT_RIGHT+"/div[@class='mini-cart-attributes']/div[@data-attribute='color']/span[@class='value']")
	WebElement miniCartColorVariation;
	
	@FindBy(xpath =MINICART_PRODUCT_RIGHT+"/div[@class='mini-cart-pricing']/span[@class='value']")
	WebElement miniCartProductQuantity;
	
	@FindBy(xpath =MINICART_PRODUCT_RIGHT+"/div[@class='mini-cart-pricing']//div[@class='total-price']/span[@class='value']")
	WebElement miniCartProductTotalPrice;
	
	@FindBy(xpath =MINICART_PRODUCT_RIGHT+"/div[@class='mini-cart-pricing']//div[@class='product-price']/span[@class='price-sales ']")
	WebElement miniCartProductPrice;
	
	@FindBy(xpath =MINICART_PRODUCT_RIGHT+"/div[@class='mini-cart-pricing']/span[@class='value']")
	List<WebElement> miniCartProducts;
	
	@FindBy(xpath =".mini-cart-content .mini-cart-subtotals .value")
	WebElement miniCartProductSubTotalPrice;
	
	@FindBy(css =".top-menu-utility .menu-utility-user .right-section #mini-cart .mini-cart-content .mini-cart-products .mini-cart-product .right-content")
	List<WebElement> miniCartProductinfo;
	
	@FindBy(css =".top-menu-utility .menu-utility-user .right-section #mini-cart .mini-cart-content .mini-cart-products .mini-cart-product .mini-cart-product-info")
	WebElement miniCartProductinfoFirst;
	
	@FindBy(css =".top-menu-utility .menu-utility-user .right-section #mini-cart .mini-cart-content .mini-cart-products .mini-cart-product .right-content .mini-cart-product-info .mini-cart-name>a")
	List<WebElement> miniCartProductName;
	
	@FindBy(css =".top-menu-utility .menu-utility-user .right-section #mini-cart .mini-cart-content .mini-cart-products .mini-cart-product .left-content .mini-cart-image > a")
	List<WebElement> miniCartProductImage;
	
	@FindBy(css = "#mini-cart .mini-cart-products.overlay-product-slick")
	WebElement miniCartInnerOverLay;
	
	@FindBy(css = ".top-menu-utility #mini-cart .mini-cart-content")
	WebElement miniCartOuterOverLay;
	
	@FindBy(css = ".primary-logo svg")
	WebElement miniCartPrimayLogo;
	
	@FindBy(css = ".right-section .total-qty")
	WebElement qtyMiniCartBagIndicator;

	@FindBy(css = ".mini-cart-content .slimScrollDiv .slimScrollBar")
	WebElement divscrollerMinicart;
	
	@FindBy(xpath = "//div[@class='mini-cart-totals']/div[@class='mini-cart-subtotals']/span[contains(text(),'Sub Total:')]/following-sibling::*[1]")
	WebElement txtSubTotalmoretotal;
	
	@FindBy(css = ".button.mini-cart-link-cart")
	WebElement divCheckOutButton;
	
	@FindBy(xpath = "//div[@class='mini-cart-totals']/div[@class='mini-cart-totals-addtocartoverlay']/div[@class='mini-cart-subtotals']/span[contains(text(),'Order Subtotal:')]/following-sibling::*[1]")
	WebElement txtSubTotal;
	
	@FindBy(css = "#mini-cart .mini-cart-image a>img")
	WebElement imgProductImage;
	
	@FindBy(css = "#mini-cart .mini-cart-image .brand-logo img")
	WebElement imgBrandLogoUnderPrdImage;
	
	@FindBy(css = "#mini-cart .mini-cart-name a")
	WebElement lnkProductName;
	
	@FindBy(css = "#mini-cart .mini-cart-attributes")
	WebElement divProductAttributes;
	
	@FindBy(css = "#mini-cart .mini-cart-product-info")
	WebElement divProductAttributesGC;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing>span[class='label']")
	WebElement lblQuantity;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing>span[class='value']")
	WebElement divQuantityValue;
	
	@FindBy(css = "#mini-cart .product-availability-list span.label")
	WebElement lblAvailability;
	
	@FindBy(xpath = "//div[@id='mini-cart']//div[@class='product-availability-list']//span[2]")
	WebElement divAvailabilityValue;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing span.price")
	WebElement lblPrice;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing span.mini-cart-price.value")
	WebElement lblPriceGC;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .product-price")
	WebElement divPriceValue;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .product-price .price-standard")
	WebElement divPriceSingle;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .product-price .price-sales.price-standard-exist")
	WebElement divPriceSales;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .mini-cart-price.value")
	WebElement lblGiftCertPrice;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .total-price span.label")
	WebElement lblTotalLabel;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .total-price span.value")
	WebElement lblTotal;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .total-price .value")
	WebElement lblGiftCertTotal;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .product-price")
	WebElement divTotalValue;
	
	@FindBy(css = "#mini-cart .mini-cart-product")
	List<WebElement> divProductSection;
	
	@FindBy(css = "#mini-cart .mini-cart-products")
	WebElement miniCartContentSlot;
	
	@FindBy(css = "#mini-cart .mini-cart-subtotals")
	WebElement divSubTotal;
	
	@FindBy(css = "#mini-cart .mini-cart-subtotals .value")
	WebElement lblSubTotalValue;
	
	@FindBy(css = "#mini-cart .giftcard.from")
	WebElement divGiftCardFrom;
	
	@FindBy(css = "#mini-cart .giftcard.to")
	WebElement divGiftCardTo;
	
	@FindBy(css = "#mini-cart .special-productset-child")
	List<WebElement> spsChildPrds;
	
	@FindBy(css = "#mini-cart .special-productset-child")
	WebElement spsFirstChildPrd;
	
	@FindBy(css = "#mini-cart .special-productset-child [data-attribute='color']")
	List<WebElement> spsChildPrdColor;
	
	@FindBy(css = "#mini-cart .special-productset-child [data-attribute='size']")
	List<WebElement> spsChildPrdSize;
	
	@FindBy(css = "#mini-cart .special-productset-child .product-availability-list")
	List<WebElement> spsChildPrdAvailability;
	
	@FindBy(css = "#mini-cart .slimScrollDiv")
	WebElement scrollSection;
	
	@FindBy(css = "#mini-cart .mini-cart-products")
	WebElement divProducts;
	
	@FindBy(css = "#mini-cart .slimScrollBar")
	WebElement scrollBar;
	//================================================================================
	//			WebElements Declaration End
	//================================================================================




	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public MiniCartPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, miniCartContent) || Utils.waitForElement(driver, miniCartIconMobile))) {
			Log.fail("Mini cart page didn't open up", driver);
		}
		
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}
	
	public String getTotalCost()throws Exception{
		return lblTotalCostInMinicartOverlay.getAttribute("innerHTML").trim().replace("$", "");
	}
	
	/**
	 * To click on product image of 
	 * @param index -
	 * @return PdpPage 
	 * @throws Exception - Exception
	 */
	public PdpPage ClickOnProductImage(int index)throws Exception{
		Utils.waitForDisabledElement(driver, miniCartOverlay , 10);
		BrowserActions.mouseHover(driver, miniCartImage, "Mouse Hover the miniCart content");
		Utils.waitForDisabledElement(driver, miniCartProductImage.get(index) , 10);
		BrowserActions.clickOnElementX(miniCartProductImage.get(index), driver, "mini cart image");
		return new PdpPage(driver).get();
	}
	
	/**
	 * To click on First Product Image in Mini cart Overlay
	 * @return PdpPage - PdpPage page object
	 * @throws Exception - Exception
	 */
	public PdpPage clickOnProductImage()throws Exception{
		
		BrowserActions.clickOnElementX(imgProductImage, driver, "First Product Image");
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, imgProductImage))
		{
			BrowserActions.clickOnElementX(imgProductImage, driver, "First Product Image");
		}
		return new PdpPage(driver).get();
	}
	
	/**
	 * To click on the product name in Minicard Flyout
	 * @param index -
	 * @return PdpPage of Product
	 * @throws Exception - Exception
	 */
	public PdpPage ClickOnProductName(int index)throws Exception{
		Utils.waitForElement(driver, miniCartOverlay);
		Utils.waitForElement(driver, miniCartProductName.get(index));
		BrowserActions.clickOnElementX(miniCartProductName.get(index), driver, "Image icon");
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}
	
	/**
	 * To verify minicart flyout is open / collapsed
	 * @return true - if flyout is visible
	 * @throws Exception - Exception
	 */
	public boolean verifyMiniCartFlyout()throws Exception{
		boolean flag=false;
		
		if(Utils.getRunPlatForm().equals("desktop")){
			BrowserActions.mouseHover(driver, miniCartContent, "Mouse Hover the miniCart content");
			
			if(Utils.waitForElement(driver, miniCartOverlay , 10)){
				flag=true;
			}
			
		}
		else if(Utils.getRunDevicePlatform(driver).equals("tablet")){
			Actions action = new Actions(driver);
			action.clickAndHold(miniCartContent).build().perform();
		}
		
		return flag;
	}
	
	/**
	 * To calculate total value of given variation 
	 * @param quantity -
	 * @param price -
	 * @param total -
	 * @return String - Total value 
	 * @throws Exception - Exception
	 */
	public String verifyMiniCartTotal(String quantity,String price,String total)throws Exception{

		double productQuantity = Double.parseDouble(quantity);
		double productPrice = Double.parseDouble(price.split("\\$")[1]);

		Log.message("Total is verified in minicart , Total =" +productQuantity*productPrice);
		return ("$"+String.valueOf(productQuantity*productPrice));
	}
	
	/**
	 * To click on checkout in mini cart flyout
	 * @return ShoppingBagPage - Cart page
	 * @throws Exception - Exception
	 */
	public ShoppingBagPage clickOnCheckOut()throws Exception{
		if(Utils.waitForElement(driver, divCheckOutButton)){
			BrowserActions.scrollInToView(divCheckOutButton, driver);
			BrowserActions.clickOnElementX(divCheckOutButton, driver, "mini cart image");
			Utils.waitForPageLoad(driver);
			
		}
		
		return new ShoppingBagPage(driver).get();
		
		
	}
	
	/**
	 * To get Quantity of products in minicart flyout
	 * @return int  - number of products
	 * @throws Exception - Exception
	 */
	public int verifyMiniCartBagQuantity()throws Exception{

		return Integer.parseInt(miniCartBagQuanity.getText());
	}
	
	/**
	 * To verify minicart flyout is opened
	 * @return true - if minicart flyout is visible
	 * @throws Exception - Exception
	 */
	public boolean verifyMinicartCloseAction()throws Exception{
		BrowserActions.scrollToBottomOfPage(driver);
		if(miniCartOuterOverLay.isDisplayed())
			return true;
		else
			return false;
	}
	
	/**
	 * To get Mini cart count near bag icon in headers
	 * @return int - Total no.of products in bag
	 * @throws Exception - Exception
	 */
	public int getMiniCartCount()throws Exception{
		//boolean flag=false;
		int qty=0;
		BrowserActions.moveToElementJS(driver, qtyMiniCartBagIndicator);
		
		if(qtyMiniCartBagIndicator.getText().equals("")){
			qty=0;
		}else{
			qty=Integer.parseInt(qtyMiniCartBagIndicator.getText());
		}
		
		return qty;
		
	}
	
	/**
	 * To get color code of first product in list
	 * @return String - Color code
	 * @throws Exception - Exception
	 */
	public String getProductColorCode()throws Exception{
		String imgPath[] = imgProductImage.getAttribute("src").trim().split("\\/");
		Log.event("Image Path :: " + imgPath);
		String imgCode = imgPath[imgPath.length - 1].split("\\_")[3].split("\\?")[0].replace(".jpg", "");
		return imgCode;
	}
	
	/**
	 * To click on First product name link
	 * @return PdpPage - PdpPage page object
	 * @throws Exception - Exception
	 */
	public PdpPage clickOnProductName()throws Exception{
		BrowserActions.clickOnElementX(lnkProductName, driver, "First Product Name Link");
		return new PdpPage(driver).get();
	}
	
	/**
	 * To calculate sub total from each product total
	 * @return float - Sub total of all products
	 * @throws Exception - Exception
	 */
	public float calculateSubTotal()throws Exception{
		float price = 0;
		for(WebElement prdSection : divProductSection) {
			price += Float.parseFloat(prdSection.findElement(By.cssSelector(".mini-cart-pricing .total-price span.value")).getText().replace("$", "").trim());
		}
		return price;
	}
	
	/**
	 * To get float value from sub total label
	 * @return float - sub total
	 * @throws Exception - Exception
	 */
	public float getSubTotal()throws Exception{
		return Float.parseFloat(lblSubTotalValue.getText().replace("$", "").trim());
	}
	
	/**
	 * To get size value by product index
	 * @param index - index
	 * @return String - size
	 * @throws Exception - Exception
	 */
	public String getSizeByPrdIndex(int index)throws Exception{
		return divProductSection.get(index).findElement(By.cssSelector(".attribute[data-attribute='size'] span.value")).getText().trim();
	}
	
	/**
	 * To get quantity value by product index
	 * @param index - index
	 * @return String - quantity
	 * @throws Exception - Exception
	 */
	public String getQtyByPrdIndex(int index)throws Exception{
		return divProductSection.get(index).findElement(By.cssSelector(".mini-cart-pricing>span[class='value']")).getText().trim();
	}
	
	/**
	 * To get total price by product index
	 * @param index - index
	 * @return float - total price
	 * @throws Exception - Exception
	 */
	public float getTotalByPrdIndex(int index)throws Exception{
		return Float.parseFloat(divProductSection.get(index).findElement(By.cssSelector(".total-price .value")).getText().replace("$", "").trim());
	}
}
