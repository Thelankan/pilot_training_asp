package com.fbb.pages.customerservice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class ContactConfirmationPage extends LoadableComponent<ContactConfirmationPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();
	String runBrowser;


	/**********************************************************************************************
	 ********************************* WebElements of Contact Confirmation Page page ****************************
	 **********************************************************************************************/

	private static EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader.getInstance("demandware");


	@FindBy(css = ".pt_article-page .thankyou-section")
	WebElement readyElement;

	@FindBy(css = ".thankyou-section>h1")
	WebElement customerConfirmText;

	@FindBy(css = ".form-wrapper")
	WebElement thankYouBody;

	@FindBy(css = ".contactpage-chat-module")
	WebElement contactModule;
	
	@FindBy(css = ".content-asset p a")
	WebElement mailToAddress;

	@FindBy(linkText = "here")
	WebElement clickHereButton;

	@FindBy(css = ".text-content .content-asset")
	WebElement thankYouBodyText;

	@FindBy(css = ".customerservice-name")
	WebElement ConfirmationHeadingText;

	@FindBy(css = ".primary-logo")
	WebElement logo;


	/**********************************************************************************************
	 ********************************* WebElements of Contact Confirmation Page page ****************************
	 **********************************************************************************************/	


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public ContactConfirmationPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);
		runBrowser  = Utils.getRunBrowser(driver);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Contact Confirmation Page did not open up.", driver);
		}
	}

	/**
	 * To Navigate to Home page By clicking on Click Here link
	 * @return HomPage
	 * @throws Exception - Exception
	 */
	public boolean navigateToHomePage()throws Exception{
		if(Utils.waitForElement(driver, clickHereButton)) {
			BrowserActions.clickOnElementX(clickHereButton, driver, "Clicked on Click here link");
			Utils.waitForPageLoad(driver);
			return true;
		}
		else {
			Log.failsoft("Click Here Link not available in Thank You content.");
			return false;
		}
		
	}

	/**
	 * To Navigate to Home Page by clicking on Logo
	 * @return HomePage
	 * @throws Exception - Exception
	 */
	public HomePage clickOnLogo()throws Exception{

		BrowserActions.clickOnElementX(logo, driver, "Clicked on logo");
		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();
	}

	/**
	 * To Verify Confirmation Heading displayed below Customer Service Option.
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyConfirmationHeadingBelowCustomerService() throws Exception {
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, ConfirmationHeadingText, thankYouBody);
	}

	/**
	 * To confirm thank you message displayed below customer confirmation text
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyThankYouBodyBelowConfirmationHeading() throws Exception {
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, customerConfirmText, thankYouBody);
	}

	/**
	 * To vertical allignment of Contact module and Thanks you message
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyContactModuleBelowThankYouBody() throws Exception {
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, thankYouBody, contactModule);
	}

	/**
	 * To verify Contact Information displayed text from DWRE property
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean verifyContactConfirmationTextProperty() throws Exception {

		String textToVerify = customerConfirmText.getText();
		String propertyText = demandWareProperty.getProperty("ContactConfirmation");

		Log.event("Element Text :: " + textToVerify);
		Log.event("Property Text :: " + propertyText);
		
		if (textToVerify.equalsIgnoreCase(propertyText)) {
			return true;
		}
		else
			return false;
	}

}