package com.fbb.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;


public class OrderConfirmationPage extends LoadableComponent<OrderConfirmationPage> {

	private WebDriver driver;
	public boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();

	@FindBy(css = ".pt_order-confirmation")
	WebElement readyElement;
	
	@FindBy(css = ".order-number .value")
	WebElement lblOrderNumber;
	
	@FindBy(css = ".order-date .value")
	WebElement lblOrderDate;
	
	@FindBy(css = ".total .value")
	WebElement lblOrderTotal;
	
	@FindBy(css = ".order-shipments")
	WebElement orderDetails;
	
	@FindBy(css =".order-payment-summary")
	WebElement orderSummary;
	
	@FindBy(css = ".order-shipment-address")
	WebElement orderShippingAddress;
	
	@FindBy(css =".content-asset .additional-images div")
	WebElement confrmCotentSlot;
	
	@FindBy(css =".content-asset .additional-offers h2")
	WebElement contentAssetHeader;
	
	@FindBy(css = ".qa-section")
	WebElement orderCnfQASection;
	
	@FindBy(css =".qa-desc .qa-content .answer.active")
	WebElement qaAnswerActive;
	
	@FindBy(css =".qa-desc .qa-content .question")
	WebElement qaQuestion;
	
	@FindBy(css =".order-total .order-value")
	WebElement orderSummaryTotal;
	
	@FindBy(css = ".order-totals-table > div > div:not([class*='order-total']):not([class='hide']):not([class='order-saving discount order-detail']) span.value")
	List<WebElement> orderSummaryDetails;
	// --------------- Header Section ---------------------- //
	/**
	 * Initiate the web driver
	 * @param driver -web driver
	 * 
	 */
	public OrderConfirmationPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver,  readyElement))) {
			Log.fail( "Order Confirmation Page didn't open", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	/**
	 * To get order number
	 * @return String - order number
	 * @throws Exception - Exception
	 */
	public String getOrderNumber()throws Exception{
		return lblOrderNumber.getText().trim();
	}
	
	/**
	 * To get order date
	 * @return String - Order Date
	 * @throws Exception - Exception
	 */
	public String getOrderDate()throws Exception{
		return lblOrderDate.getText().trim();
	}
	
	/**
	 * To get order total
	 * @return String - Order Total
	 * @throws Exception - Exception
	 */
	public String getOrderTotal() throws Exception{
		return lblOrderTotal.getText().trim();
	}
	
	/**
	 * To get subtotal in summary
	 * @return String
	 * @throws Exception - Exception
	 */
	public float getOrderTotalInSummary()throws Exception{
		String subTotalToReturn = orderSummaryTotal.getText().trim().replace("$", ""); 
		Log.event("Order Sub-Total :: " + subTotalToReturn);
		return Float.parseFloat(subTotalToReturn);
	}
	
	/**
	 * To click the question and answer
	 * @throws Exception
	 */
	public void clickonQuestion()throws Exception{
		BrowserActions.clickOnElement(qaQuestion, driver, "QA question");
	}
	
	
	/**
	 * To calculate the order summary total
	 * @return float
	 * @throws Exception - Exception
	 */
	public float calculateTotalInOrderSummaryList()throws Exception{
		float total = 0;
		float discount =0;
		for(int i = 0; i < orderSummaryDetails.size(); i++)
		{
			String tempString = orderSummaryDetails.get(i).getText();
			if(tempString.contains("-"))
				discount+=Float.parseFloat(tempString.replace("-", "").replace("$", "").trim());
			else
				total+=Float.parseFloat(tempString.replace("$", "").trim());
		}
		float amount = total-discount;
		return Float.parseFloat(String.format("%.2f", amount));
	}
}


