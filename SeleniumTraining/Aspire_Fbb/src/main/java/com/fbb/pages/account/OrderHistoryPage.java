package com.fbb.pages.account;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class OrderHistoryPage extends LoadableComponent <OrderHistoryPage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;
	
	private static final String MyAccountPageLinksNavigation = ".navigation-links-row";

	private static EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader
			.getInstance("demandware");

	String runPltfrm = Utils.getRunPlatForm();
	

	/**********************************************************************************************
	 ********************************* WebElements of OrderHistoryPage ***********************************
	 **********************************************************************************************/

	@FindBy(css = ".pt_order")
	WebElement readyElement;
	
	@FindBy(css = ".order-history .content-wrap h1")
	WebElement lblOrderHistory;
	
	@FindBy(css = MyAccountPageLinksNavigation + " .order-history a[style*='cursor']")
	WebElement lnkOrderHistoryAfterNavigation;

	@FindBy(css = ".secondary-navigation-links .order-history a")
	WebElement lnkOrderHistory;
	
	@FindBy(css = ".order-history h1.account-subpage-heading")
	WebElement lblHistoryHeading;
	
	@FindBy(css = ".order-history h2")
	WebElement lblHistorySubheading;
	
	@FindBy(css = ".order-history-table .order-history-header .col-first")
	WebElement txtHeaderOrderNumber;
	
	@FindBy(css = ".order-history-table .order-history-header .col.col-2")
	WebElement txtHeaderOrderDate;
	
	@FindBy(css = ".order-history-table .order-history-header .col.col-3")
	WebElement txtHeaderOrderOrderTotal;
	
	@FindBy(css = ".order-history-table .order-history-header .col.col-4")
	WebElement txtHeaderOrderOrderStatus;
	
	@FindBy(css = ".order-history-table .order-history-header .col.col-5")
	WebElement txtHeaderOrderOrderAction;
	
	@FindBy(css = ".order-list")
	List<WebElement> divOrderListEntries;
	
	@FindBy(css = ".order-list .order-number .value")
	List<WebElement> divOrderNumbers;
	
	@FindBy(css = ".order-list .order-date .value")
	List<WebElement> divOrderEntryDates;
	
	@FindBy(css = ".order-list .order-price")
	List<WebElement> divOrderEntryTotals;
	
	@FindBy(css = ".order-list-wrapper.search-result-items li:nth-child(1) .order-history-data .col.col-1 .value")
	WebElement txtOrderNumber;
	
	@FindBy(css = ".order-list-wrapper.search-result-items li:nth-child(1) .order-history-data .col.col-2 .value")
	WebElement txtOrderDate;
	
	@FindBy(css = ".order-list-wrapper.search-result-items li:nth-child(1) .order-history-data .col.col-3 .value")
	WebElement txtOrderTotal;
	
	@FindBy(css = ".order-list-wrapper.search-result-items li:nth-child(1) .order-history-data .col.col-4 .value")
	WebElement txtOrderStatus;
	
	@FindBy(css = ".order-list-wrapper.search-result-items li:nth-child(1) .order-history-data .col.col-5 button")
	WebElement txtOrderViewDetails;
	
	@FindBy(css = ".order-list-wrapper.search-result-items")
	WebElement orderHistoryTable;
	
	@FindBy(css = "#primary .order-details-section")
	WebElement orderDetailsPage;
	
	@FindBy(css = ".order-history .content-asset")
	WebElement orderDetailsContentAsset;
	
	@FindBy(css = ".order-history .content-asset .question-title")
	WebElement orderQuestionTitle;
	
	@FindBy(css = ".order-history .content-asset .question")
	List <WebElement> orderQuestionCaratSymbol;
	
	@FindBy(css = ".qa-content")
	List<WebElement> questionsSection;
	
	@FindBy(css = ".question")
	WebElement questionsExpandIcon;
	
	@FindBy(css = "a[title='Go to Home']")
	WebElement breadCrumbHome;
	
	@FindBy(css = "a[title='Go to MY ACCOUNT']")
	WebElement breadCrumbMyAccount;
	
	@FindBy(xpath = "//div[@class='breadcrumb']/a[contains(@class, 'breadcrumb-element current-element')]")
	WebElement breadCrumbOrderHistory;
	
	@FindBy(css = ".order-history .content-wrap .top-content")
	WebElement lblOrderHistoryTopContent;
	
	@FindBy(css = ".button-text.return")
	WebElement txtReturnItem;
	
	@FindBy(css = ".order-list-wrapper.search-result-items .value.processing.order_processed")
	WebElement txtProcessStatus;
	
	@FindBy(css = ".breadcrumb-element.hide-mobile")
	List<WebElement> lstbreadcrumbs;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement lstbreadcrumb;
	
	@FindBy(css = ".order-list.opened")
	List<WebElement> ordersummaryopened;
	
	@FindBy(css = "div[class='col col-1 col-first order-number']")
	List<WebElement> listOrdernumber;
	
	@FindBy(css = "div[class='col col-2 order-date']")
	List<WebElement> listOrderDate;
	
	@FindBy(css = "div[class='col col-3 order-price']")
	List<WebElement> listOrderTotal;
	
	@FindBy(css = ".order-list")
	List<WebElement> orderlist;
	
	@FindBy(css = ".order-list.opened .button-text.view-detials")
	WebElement viewdetails;
	
	@FindBy(css = ".order-list.opened .arr-img.only-for-mobile")
	WebElement closearrow;
	
	@FindBy(css = ".arr-img.only-for-mobile")
	List<WebElement> opencloseOrderSummary;
	
	@FindBy(css=".hide-desktop.hide-tablet.back-arrow")
	WebElement breadcrumBackArrow;
	
	@FindBy(css = ".order-number .value")
	List<WebElement> orderNumbers;
	
	//Gift Card
	private static final String GIFTCARD = ".gift-card";
	
	@FindBy(css = GIFTCARD)
	WebElement divGiftCard;
	
	@FindBy(css = GIFTCARD + " .shipmentnumber")
	WebElement gcShipmentNumber;
	
	@FindBy(css = GIFTCARD + " .method .heading")
	WebElement gcShimpmentHeading;
	
	@FindBy(css = GIFTCARD + " .method .value")
	WebElement gcShipmentMethod;
	
	@FindBy(css = GIFTCARD + " .address")
	WebElement gcShipmentAddress;
	
	@FindBy(css = GIFTCARD + " .address .default")
	WebElement gcShipmentName;
	
	@FindBy(css = GIFTCARD + " .address div:nth-child(2)")
	WebElement gcShipmentAddress1;
	
	@FindBy(css = GIFTCARD + " .address div:nth-child(3)")
	WebElement gcShipmentAddress2;
	
	@FindBy(css = GIFTCARD + " .shipping-status .value")
	WebElement gcShipmentStatus;
	
	@FindBy(css = GIFTCARD + " .name")
	WebElement gcName;
	
	@FindBy(css = GIFTCARD + " .item-image a")
	WebElement gcItemImage;
	
	@FindBy(css = GIFTCARD + " .col-2 .line-item-quantity")
	WebElement gcQtyDesktop;
	
	@FindBy(css = GIFTCARD + " .hide-desktop.line-item-quantity")
	WebElement gcQtyMobile;
	
	@FindBy(css = GIFTCARD + " .line-item-price")
	WebElement gcPrice;
	
	//Electronic Gift Card
	private static final String ELECTRONICGIFTCARD = ".e-gift-card";
	
	@FindBy(css = ELECTRONICGIFTCARD)
	WebElement divElectronicGiftCard;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .shipmentnumber")
	WebElement egcShipmentNumber;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .method .heading")
	WebElement egcShimpmentHeading;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .method .value")
	WebElement egcShipmentMethod;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .shipping-status .value")
	WebElement egcShipmentStatus;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .name")
	WebElement egcName;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .item-image a")
	WebElement egcItemImage;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .col-2 .line-item-quantity")
	WebElement egcQtyDesktop;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .hide-desktop.line-item-quantity")
	WebElement egcQtyMobile;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .line-item-price")
	WebElement egcPrice;
	
	/**********************************************************************************************
	 ********************************* WebElements of OrderHistoryPage - Ends ****************************
	 **********************************************************************************************/

	

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public OrderHistoryPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("OrderHistory Page did not open up. Site might be down.", driver);
		}
		
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}// isLoaded
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver,readyElement );
	}
	
	
	
	/**
	 * Verify order history lable from property
	 * @param option - 
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderHistoryLablesFromProperty(String option) throws Exception {
		
		boolean flag=false;
		
		String textToVerify =null;
		String propertyText =null;
		
		switch (option){
		case "History label":
			textToVerify = BrowserActions.getText(driver, lblOrderHistory, "My Account Label");
			propertyText = demandWareProperty.getProperty("orderHistoryHeader");
			
			if (propertyText.toUpperCase().equals(textToVerify.toUpperCase())) {
				flag=true;
			}
			
			break;
			
			
		case "Subhead":
			textToVerify = BrowserActions.getText(driver, lblHistorySubheading, "My Account Label");
			propertyText = demandWareProperty.getProperty("orderHistorySubhead");
			if (propertyText.toUpperCase().equals(textToVerify.toUpperCase())) {
				flag=true;
			}
			
			break;
			
		case "Header Order Number":
			textToVerify = BrowserActions.getText(driver, txtHeaderOrderNumber, "Header Order Number");
			propertyText = demandWareProperty.getProperty("orderHistoryOrdernumber");
			if (propertyText.toUpperCase().equals(textToVerify.toUpperCase())) {
				flag=true;
			}
			
			break;
		case "Date":
			textToVerify = BrowserActions.getText(driver, txtHeaderOrderDate, "Header Order Date");
			propertyText = demandWareProperty.getProperty("orderHistoryDateplaced");
			if (propertyText.toUpperCase().equals(textToVerify.toUpperCase())) {
				flag=true;
			}
			
			break;
			
		case "Order Total":
			textToVerify = BrowserActions.getText(driver, txtHeaderOrderOrderTotal, "My Account Label");
			propertyText = demandWareProperty.getProperty("orderHistoryOrdertotal");
			if (propertyText.toUpperCase().equals(textToVerify.toUpperCase())) {
				flag=true;
			}
			
			break;
			
		case "Status":
			textToVerify = BrowserActions.getText(driver, txtHeaderOrderOrderStatus, "My Account Label");
			propertyText = demandWareProperty.getProperty("orderHistoryStatus");
			if (propertyText.toUpperCase().equals(textToVerify.toUpperCase())) {
				flag=true;
			}
			
			break;	
			
		case "Action":
			textToVerify = BrowserActions.getText(driver, txtHeaderOrderOrderAction, "My Account Label");
			propertyText = demandWareProperty.getProperty("MyAccount");
			if (propertyText.toUpperCase().equals(textToVerify.toUpperCase())) {
				flag=true;
			}
			
			break;
		
			
			
		}
		Log.message("The Property Value is: "+ propertyText ,driver);
		Log.message("The Actual Value is from website: "+ textToVerify ,driver);
		
		return flag;
	}

	/**
	 * Verify latest order date is correct
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyLatestOrderDate()throws Exception{
		if(Utils.waitForElement(driver, txtOrderDate)){
			String time = new SimpleDateFormat("M/d/yy").format(new Date());
			String latestOrderDate=txtOrderDate.getText();
			
			Log.event("Order Date :: " + latestOrderDate);
			Log.event("System Date :: " + time);
			return(latestOrderDate.contains(time));
		}
		return false;
	}
	
	/**
	 * Verify order date format for all orders
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderDateFormat()throws Exception{
		SimpleDateFormat dateFormat = new SimpleDateFormat("M/dd/yy");
		for(WebElement orderDate: divOrderEntryDates) {
			try {
				dateFormat.parse(orderDate.getText().replace("DATE", "").trim());
			}
			catch(ParseException e){
				return false;
			}
		}
		return true;
	}
	/**
	 * Verify the order dollar symbol
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderTotalDollarSymbol()throws Exception{
		for(WebElement orderTotal: divOrderEntryTotals) {
			if(!orderTotal.getText().contains("$"))
				return false;
		}
		return true;
	}
	/**
	 * Verify click on view details
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean clickOnViewDetails()throws Exception{
		boolean flag=false;
		BrowserActions.clickOnElementX(txtOrderViewDetails, driver, "View Details Link");
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, orderDetailsPage)){
			flag=true;
		}
		return flag;
	}
	/**
	 * Verify get expand status question
	 * @param index - 
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getExpandStatusOfQuestion(int index)throws Exception{
		Utils.waitForElement(driver, questionsSection.get(index));
		BrowserActions.scrollToView(questionsSection.get(index), driver);
		return (questionsSection.get(index).getAttribute("class").contains("active"))? "expanded":"collapsed";
	}
	/**
	 * Verify Expance collapse question
	 * @param index - 
	 * @param state - 
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean expandCollapseQuestionOf(int index, String state)throws Exception{
		boolean flag=false;
			BrowserActions.scrollToView(questionsSection.get(index), driver);
		if(state.equals("expand") && getExpandStatusOfQuestion(index).equals("collapsed"))
			Log.event("Section collapsed");
			BrowserActions.clickOnElementX(questionsSection.get(index), driver, "Nth Question Section");
			if(!getExpandStatusOfQuestion(index).equals("collapsed")){
				flag=true;
			}
		else if(state.equals("collapse") && getExpandStatusOfQuestion(index).equals("expanded")){
			Log.event("Section expanded");
			BrowserActions.clickOnElementX(questionsSection.get(index), driver, "Nth Question Section");
			Log.event("Question Setion " + index + " " + state + "ed!!!");
			if(getExpandStatusOfQuestion(index).equals("expanded")){
				flag=true;
			}
		}
		return flag;
	}
	/**
	 * Verify the cart symbol  location
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyCaratSymbolLocation()throws Exception{
		boolean flag=false;
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, orderDetailsPage)){
			flag=true;
		}
		return flag;
	}
	/**
	 * Verify the breadcrum text
	 * @param TextToVerify - 
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyBreadcrumbText(String TextToVerify)throws Exception{
		boolean flag=false;
		
		String breadCrumbText=breadCrumbHome.getText()+" / "+ breadCrumbMyAccount.getText()+" / "+breadCrumbOrderHistory.getText();
		if(breadCrumbText.toUpperCase().equals(TextToVerify.toUpperCase())){
			flag=true;
		}
		
		return flag;
	}
	/**
	 * Verify the breadcrum navigation
	 * @param breadcrumb - 
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyBreadcrumbNavigation(String breadcrumb)throws Exception{
		boolean flag=false;
		switch (breadcrumb){
		case "Home":
			BrowserActions.clickOnElementX(breadCrumbHome, driver, "BreadCrumbHome");
			Utils.waitForPageLoad(driver);
			if(driver.getCurrentUrl().contains("home")
					|| driver.findElement(By.cssSelector(".pt_storefront")).isDisplayed()){
				flag=true;
			}
			
			break;
			
			
		case "My Account":
			BrowserActions.clickOnElementX(breadCrumbMyAccount, driver, "BreadCrumb my account");
			Utils.waitForPageLoad(driver);
			if(driver.getCurrentUrl().contains("account") || driver.getCurrentUrl().contains("register")){
				flag=true;
			}
			
			break;
		}
		
		
		return flag;
	}
	/**
	 * Verify order status
	 * @return flag - 
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderStatus()throws Exception{
		boolean flag=true;
		
		return flag;
	}
	
	/**
	 * to click the breadcrumb value
	 * @param breadcrumbValue - 
	 * @return HomePage for breadcrumbValue=Home
	 * @return MyAccountPage for breadcrumbValue=others
	 * @throws Exception - Exception
	 */
	public Object clickCategoryInBreadcrumb(String breadcrumbValue) throws Exception {
		for (WebElement ele : lstbreadcrumbs) {
			if (ele.getText().trim().equalsIgnoreCase(breadcrumbValue)) {
				if (breadcrumbValue.trim().equalsIgnoreCase("Home")) {
					BrowserActions.clickOnElementX(ele, driver, "Breadcrumb Category");
					Utils.waitForPageLoad(driver);
					return new HomePage(driver).get();
				} else {
					BrowserActions.clickOnElementX(ele, driver, "Breadcrumb Category");
					Utils.waitForPageLoad(driver);
					return new MyAccountPage(driver).get();
				}
			}
		}
		// if breadcrumb value is not found, return null
		return null;
	}
	/**
	 * Verify click on breadcrum mobile 
	 * @throws Exception - Exception
	 */
	public void clickOnBreakCrumbMobile() throws Exception{
		BrowserActions.clickOnElementX(breadcrumBackArrow, driver, "Breadcrumb");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * Verify get breadcrum count
	 * @return breadcrum count Integer
	 * @throws Exception - Exception
	 */
	public int getbreadcrumbCount() throws Exception{
		Utils.waitForPageLoad(driver);
		int count=lstbreadcrumbs.size();
		return count;
	}
	
	
	/**
	 * to verify Bredcrum is displayed or clickable
	 * @param str -
	 * @param validation - 
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public boolean validateBreadcrum(String str, String validation) throws Exception {
		boolean temp = false,flag=false;
		int count=getbreadcrumbCount();
		for (int i = 0; i < count; i++) 
		{
			if (validation == "clickable") 
			{
				if (lstbreadcrumbs.get(i).getText().equals(str)) 
				{
					String tag=lstbreadcrumbs.get(i).getTagName();
					temp = lstbreadcrumbs.get(i).isEnabled();
					List<String> clickableTags= Arrays.asList("input","button","a");
					String stringValue = lstbreadcrumbs.get(i).getAttribute("href");
					if ((clickableTags.contains(tag)&& stringValue != null)&&temp) 
					{
						flag=true;
						if(stringValue.contains("void(0)"))
							flag= false;
					}
				}
			}
			else if ((validation == "displayed")) 
			{	
				flag = false;
				if(runPltfrm.equals("mobile")) 
				{
					Log.message("Breadcrum Value" +lstbreadcrumb.getText());
					if (lstbreadcrumb.getText().equalsIgnoreCase(str))
					{
						flag = true;
					}	
				} 
				else 
				{
					if (lstbreadcrumbs.get(i).getText().equalsIgnoreCase(str))
					{
						flag = lstbreadcrumbs.get(i).isDisplayed();
					}
				}
			}
		}
		return flag;
	}
	
	/**
	 * Verify click order by index
	 * @param int - index 
	 * @throws Exception - Exception
	 */
	public void clickOrderByIndex(int index) throws Exception {
		try {
			if(Utils.isMobile()) {
				WebElement expand = orderlist.get(index).findElement(By.cssSelector(".arr-img.only-for-mobile"));
				BrowserActions.clickOnElementX(expand, driver, "Expand order card");
			}
			WebElement nthViewDetails = orderlist.get(index).findElement(By.cssSelector(".view-detials"));
			BrowserActions.clickOnElementX(nthViewDetails, driver, "Order number " + index+1);
			Utils.waitForPageLoad(driver);
		}catch(Exception e) {
			Log.event("No order found. " + e.getMessage());
		}
	}
	
	/**
	 * To click no order by order number
	 * @param String - Order Number
	 * @throws Exception - Exception
	 */
	public void clickOrderByNumber(String orderNumber) throws Exception{
		try {
			for(WebElement order: orderlist) {
				String orderNumberOnPage = order.findElement(By.cssSelector(".order-number .value")).getText().replaceAll("[^0-9]", "");
				if(orderNumberOnPage.equals(orderNumber)) {
					if(Utils.isMobile()) {
						WebElement expand = order.findElement(By.cssSelector(".arr-img.only-for-mobile"));
						BrowserActions.clickOnElementX(expand, driver, "Expand order card");
					}
					WebElement orderViewDetails = order.findElement(By.cssSelector(".view-detials"));
					BrowserActions.clickOnElementX(orderViewDetails, driver, "Order number: " + orderNumber);
					Utils.waitForPageLoad(driver);
				}
			}
			Log.event("Order number not found.");
		}catch(Exception e) {
			Log.event("No order found. " + e.getMessage());
		}
	}
	
	/**
	 * Verify close summary
	 * @throws Exception - Exception
	 */
	public void closesummary() throws Exception {
		
		BrowserActions.clickOnElementX(closearrow, driver, "Close order summary tray");
		Utils.waitForPageLoad(driver);
		
	}
	
	/**
	 * Verify open close summary tray
	 * @param index - 
	 * @throws Exception - Exception
	 */
	public void opencloseOrderSummarytray(int index) throws Exception {
		
		BrowserActions.clickOnElementX(opencloseOrderSummary.get(index), driver, "Order summary Tray/Arrow");
		Utils.waitForPageLoad(driver);
		
	}
	
	/**
	 * Verify order of elements in order entry
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderListElementsOrder()throws Exception{
		for(WebElement orderEntry: divOrderListEntries) {
			if(orderEntry.findElement(By.cssSelector(".col.col-1")).getLocation().x > orderEntry.findElement(By.cssSelector(".col.col-2")).getLocation().x
			|| orderEntry.findElement(By.cssSelector(".col.col-2")).getLocation().x > orderEntry.findElement(By.cssSelector(".col.col-3")).getLocation().x
			|| orderEntry.findElement(By.cssSelector(".col.col-3")).getLocation().x > orderEntry.findElement(By.cssSelector(".col.col-4")).getLocation().x
			|| orderEntry.findElement(By.cssSelector(".col.col-4")).getLocation().x > orderEntry.findElement(By.cssSelector(".col.col-5")).getLocation().x) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Verify order of elements in order entry for Mobile
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderListElementsOrderMobile()throws Exception{
		for(WebElement orderEntry: divOrderListEntries) {
			if(orderEntry.findElement(By.cssSelector(".order-date .only-for-mobile")).getLocation().x > orderEntry.findElement(By.cssSelector(".order-price .only-for-mobile")).getLocation().x) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Verify elements in order entry for mobile
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyMobileOrderListElements()throws Exception{
		boolean orderNumberViewable, orderDateViewable, orderTotalViewable;
		for(WebElement orderEntry: divOrderListEntries) {
			orderNumberViewable = Utils.waitForElement(driver, orderEntry.findElement(By.cssSelector(".col.col-1")));
			orderDateViewable = Utils.waitForElement(driver, orderEntry.findElement(By.cssSelector(".col.col-2")));
			orderTotalViewable =Utils.waitForElement(driver, orderEntry.findElement(By.cssSelector(".col.col-3")));
			if(!(orderNumberViewable && orderDateViewable && orderTotalViewable))
				return false;
		}
		return true;
	}
	
	/**
	 * Verify mobile order list is closed
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyMobileOrderListIsClised()throws Exception{
		boolean orderViewDetailsViewable;
		for(WebElement orderEntry: divOrderListEntries) {
			orderViewDetailsViewable = Utils.waitForElement(driver, orderEntry.findElement(By.cssSelector(".col.col-5")));
			if(orderViewDetailsViewable)
				return false;
		}
		return true;
	}
	
	/**
	 * Verify mobile order list is closed
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyMobileOrderListIsOpened()throws Exception{
		boolean orderViewDetailsViewable;
		for(WebElement orderEntry: divOrderListEntries) {
			WebElement viewDetails = orderEntry.findElement(By.cssSelector(".col.col-5"));
			orderViewDetailsViewable = Utils.waitForElement(driver, viewDetails);
			if(!orderViewDetailsViewable) {
				BrowserActions.javascriptClick(orderEntry, driver, "Order entry");
			}
			if(Utils.waitForElement(driver, viewDetails))
				return false;
		}
		return true;
	}

	/**
	 * To get list of order numbers in array format
	 * @return String[] - order numbers
	 * @throws Exception - Exception
	 */
	public List<String> getOrderNumersList()throws Exception{
		List<String> array = new ArrayList<String>();
		for(WebElement orderNumber : orderNumbers){
			array.add(orderNumber.getText().trim()); 
		}
		return array;
	}
	
	/**
	 * To get order date for given order number
	 * @param orderNumber - Order number
	 * @return String - Order Date
	 * @throws Exception - Exception
	 */
	public String getOrderDateForOrderNumber(String orderNumber)throws Exception{
		WebElement orderRow = driver.findElement(By.xpath("//span[contains(text(),'" + orderNumber + "')]//ancestor::li"));
		String orderDate = orderRow.findElement(By.cssSelector(".order-date .value")).getText().replace("Date", "").trim();
		System.out.println("Orderdata:: " + orderDate);
		return orderDate;
	}
	
	/**
	 * To get order date for given order number
	 * @param orderNumber - Order number
	 * @return String - Order Total
	 * @throws Exception - Exception
	 */
	public String getOrderTotalForOrderNumber(String orderNumber)throws Exception{
		WebElement orderRow = driver.findElement(By.xpath("//span[contains(text(),'" + orderNumber + "')]//ancestor::li"));
		String orderTotal = orderRow.findElement(By.cssSelector(".order-price .value")).getText().replace("Total", "").trim();
		return orderTotal;
	}
	
	/**
	 * To verify order numbers are unique
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderNumbersUnique() throws Exception{
		Set<String> lstOrderNumber = new HashSet<String>();
		for(WebElement orderNumberElement: divOrderNumbers) {
			lstOrderNumber.add(orderNumberElement.getText());
		}
		return lstOrderNumber.size() == divOrderNumbers.size();
	}
	
}
