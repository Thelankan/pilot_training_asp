package com.fbb.pages.customerservice;


import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class TrackOrderPage extends LoadableComponent<TrackOrderPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();
	String runBrowser;

	
	/**********************************************************************************************
	 ********************************* WebElements of Track My Order page ****************************
	 **********************************************************************************************/
	
	@FindBy(css = ".pt_order")
	WebElement readyElement;
	
	@FindBy(xpath = "//span[contains(text(),'Order status')]//ancestor::a")
	WebElement tileOrderStatus;

	@FindBy(css = ".article-heading")
	WebElement orderHeading;
	
	@FindBy(css = ".secondary-left-section>ul>li>a")
	List<WebElement> navigationOptions;
	
	/**********************************************************************************************
	 ********************************* WebElements of Track My Order page ****************************
	 **********************************************************************************************/	
	
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public TrackOrderPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);
		runBrowser  = Utils.getRunBrowser(driver);
	}

	
	
	
	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Track My Order Page did not open up.", driver);
		}
	}
	
	/**
	 * To click on order status 
	 * @throws Exception - Exception
	 */
	

	public void navigateToOrderStausPage()throws Exception{
		BrowserActions.clickOnElementX(tileOrderStatus, driver, "Order Status Tile link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To navigate to contact us page
	 * @return ContactUsPage
	 * @throws Exception - Exception
	 */
	
	public ContactUsPage navigateToContactUSPage()throws Exception{		
		BrowserActions.clickOnElementX(navigationOptions.get(8), driver, "Contact us link");
		Utils.waitForPageLoad(driver);
		return new ContactUsPage(driver).get();
	}



}

