package com.fbb.pages.customerservice;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;


public class OrderStatusPage extends LoadableComponent<OrderStatusPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public CSNavigation csNav;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();
	String runBrowser;



	/**********************************************************************************************
	 ********************************* WebElements of OrderStatus page ****************************
	 **********************************************************************************************/

	@FindBy(css = ".pt_article-page.rm-custservice-orderstatus")
	WebElement readyElement;
	
	@FindBy(css = ".secondary-left-section")
	WebElement navLeftNavigation;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement breadcumbMobile;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement breadcumbTablet;
	
	@FindBy(css = ".secondary-aticle-content .mobile-nav")
	WebElement navLeftNavigationMobile;
	
	@FindBy(css = ".secondary-article-heading.mobile-nav")
	WebElement navMobileCurrentSelection;
	
	@FindBy(css = ".customerservice-name.hide-tablet.hide-mobile")
	WebElement csLabelDesktop;
	
	@FindBy(css = ".customerservice-name")
	WebElement csLabelTablet;
	
	@FindBy(css = ".article-heading")
	WebElement csCurrentHeading;
	
	@FindBy(css = ".article-body")
	WebElement orderStatusContent;
	
	@FindBy(xpath = "//span[contains(text(),'Order status')]//ancestor::a")
	WebElement tileOrderStatus;

	@FindBy(css = ".currentpage")
	WebElement lnkCurrentPage;

	@FindBy(css = "nav")
	WebElement leftNavigationElements;

	@FindBy(css = ".secondary-left-section>ul>li>a")
	WebElement leftNavigationOption;

	@FindBy(css = ".secondary-left-section li a")
	List<WebElement> leftNavigationArticleList;
	
	@FindBy(css = ".secondary-left-section ul li a")
	List<WebElement> dropdownMenuList;

	@FindBy(css = ".secondary-aticle-content")
	WebElement LblCustomerServiceHeading;

	@FindBy(css = ".secondary-left-section")
	WebElement leftNavigationSection;
	
	@FindBy(css = ".inner-service-landing-contacts")
	WebElement contactUsSection;

	@FindBy(css = ".primary-content")
	WebElement primarycontent;

	@FindBy(css = ".currentpage")
	WebElement currentPage;

	@FindBy(css = ".article-heading")
	WebElement articleHeading;

	@FindBy(css = ".article-body")
	WebElement articleBody;

	@FindBy(css = ".secondary-left-section>ul>li>a")
	List<WebElement> navigationOptions;

	@FindBy(css =".pt_article-page" )
	WebElement lnkNavContactUS;


	@FindBy(xpath = "//span[contains(text(),'Order status')]//ancestor::a")
	WebElement OrderStatus;

	@FindBy(css = ".secondary-aticle-content")
	WebElement custServiceMenuMobile;

	@FindBy(css = ".secondary-aticle-content .secondary-article-heading.mobile-nav")
	WebElement MenuMobile;
	
	@FindBy(css = ".secondary-article-heading.mobile-nav.active")
	WebElement drpDownLeftNavActiveMobile;
	
	@FindBy(css = "a[title='Contact US']")
	WebElement lnkContactUs;

	/**********************************************************************************************
	 ********************************* WebElements of OrderStatus page ****************************
	 **********************************************************************************************/	


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public OrderStatusPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
		runBrowser  = Utils.getRunBrowser(driver);
		csNav = new CSNavigation(driver).get();
	}




	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Order Page did not open up.", driver);
		}
	}

	/**
	 * To click on tile order status button to navigate to Order Status Page
	 * @throws Exception - Exception
	 */
	public void navigateToOrderStatusPage()throws Exception{
		BrowserActions.clickOnElementX(tileOrderStatus, driver, "Order Status Tile link");
		Utils.waitForPageLoad(driver);

	}
	
	/**
	 * To get the color of current page elements
	 * @return string value
	 * @throws Exception - Exception
	 */
	public String getColorOfCurrentPage()throws Exception{
		return lnkCurrentPage.getCssValue("color");
	}
	
	/**
	 * To navigate to Customer Server Navigation Page
	 * @return CS navigation value
	 * @throws Exception - Exception
	 */
	public CSNavigation navigateToCsNavigationPage()throws Exception{
		BrowserActions.clickOnElementX(readyElement, driver, "Left Navigation link");
		Utils.waitForPageLoad(driver);
		return new CSNavigation(driver).get();
	}

	/**
	 * To get List of Left Navigation menus in Customer Service Page
	 * @return list value
	 * @throws Exception - Exception
	 */
	public List<String> LeftNavigationArticleList()throws Exception{

		List<String> navList = new ArrayList<String>();
		for(int i =0;i<leftNavigationArticleList.size();i++)
		{
			navList.add(leftNavigationArticleList.get(i).getText().toLowerCase());
		}
		System.out.println(navList);
		return navList;
	}
	
	/**
	 * To get List of DropDown menu items in Customer Service Page
	 * @return list value
	 * @throws Exception - Exception
	 */
	public List<String> DropDownArticleList()throws Exception{
		BrowserActions.clickOnElementX(navLeftNavigationMobile, driver, "Dropdown menu");
		List<String> menuList = new ArrayList<String>();
		for(int i =0;i<dropdownMenuList.size();i++)
		{
			menuList.add(dropdownMenuList.get(i).getText().toLowerCase());
		}
		BrowserActions.clickOnElementX(navLeftNavigationMobile, driver, "Dropdown menu");
		System.out.println("Dropdown menuList:: " + menuList);
		return menuList;
	}

	/**
	 * To navigate to Order Status Page
	 * @return OrderStatusPage
	 * @throws Exception - Exception
	 */
	public OrderStatusPage navigateToOrderStatus()throws Exception{

		BrowserActions.clickOnElementX(OrderStatus, driver, "Customer Service Link in Header");
		Utils.waitForPageLoad(driver);
		return new OrderStatusPage(driver).get();
	}

	/**
	 * To Navigate to Track Order Page
	 * @return TrackOrderPage
	 * @throws Exception - Exception
	 */
	public TrackOrderPage clickingLeftNaviagtion()throws Exception{
		BrowserActions.clickOnElementX(leftNavigationSection, driver, "LeftNavigation");
		Utils.waitForPageLoad(driver);
		return new TrackOrderPage(driver).get();
	}

	/**
	 * To Get the color of Left Navigation Color fonts
	 * @return string value
	 * @throws Exception - Exception
	 */
	public String getLeftNavigationColor()throws Exception{
		return currentPage.getCssValue("color");
	} 

	/**
	 * To Navigate to Contact Us Page in All platform
	 * @return ContactUsPage - Contact us page object
	 * @throws Exception - Exception
	 */
	public ContactUsPage navigateToContactUSPageAllPltfrm()throws Exception{
		if(!Utils.isDesktop()) {
			if(!(Utils.waitForElement(driver, drpDownLeftNavActiveMobile))) {
				BrowserActions.clickOnElementX(MenuMobile, driver, "Expanded status of the Elements");
			}
		}
		BrowserActions.clickOnElementX(lnkContactUs, driver, "Contact us link");
		Utils.waitForPageLoad(driver);
		return new ContactUsPage(driver).get();
	}

}

