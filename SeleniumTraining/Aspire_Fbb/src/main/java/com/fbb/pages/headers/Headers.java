package com.fbb.pages.headers;

import java.awt.Robot;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.CreateAccountPage;
import com.fbb.pages.ElementLayer;
import com.fbb.pages.Explore;
import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.MiniCartPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PasswordResetPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.customerservice.ContactUsPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.customerservice.TrackOrderPage;
import com.fbb.pages.ordering.OrderStatusPage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StopWatch;
import com.fbb.support.Utils;

public class Headers extends LoadableComponent<Headers> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public String brand = System.getProperty("brand");
	String runPltfrm = Utils.getRunPlatForm();
	public HamburgerMenu hamburgerMenu;
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");
	//================================================================================
	//			WebElements Declaration Start
	//================================================================================



	//--------------Header Brand Navigation ELements-------------------

	private static final String WRAPPER = "#wrapper ";
	private static final String HEADER = WRAPPER + "#header ";
	private static final String SLIDER_MENU_MOBILE = ".slider-menu ";
	private static final String HEADER_NAV_MOBILE = SLIDER_MENU_MOBILE + "#navigation-bar ";
	private static final String lISTOFSEARCHSUGGESTIONS = ".product-suggestions";

	private static final String MENU_UTIL = HEADER + ".top-menu-utility ";
	private static final String MENU_UTIL_BRAND_CONTENT_MOBILE = MENU_UTIL + ".header-brand-selector .content-asset .brand-logos.hide-desktop ";

	private static final String MENU_UTIL_USER_DESKTOP = MENU_UTIL + ".menu-utility-user .user-info ";
	private static final String MENU_UTIL_REG_USER_DESKTOP = MENU_UTIL + ".menu-utility-user .user-info .user-panel.registered-user ";
	private static final String MENU_UTIL_USER_MOBILE = HEADER_NAV_MOBILE + ".user-info ";

	private static final String MENU_UTIL_MINICART_DESKTOP = MENU_UTIL + ".menu-utility-user #mini-cart ";

	private static final String HEADER_NAV_LEVEL1 = "#navigation .menu-category.level-1 li ";


	private static final String HEADER_SUB_CATEGORY_l2 = ".hover~.level-2 .hide-mobile>ul>li>ul>li>a";
	private static final String HEADER_SUB_CATEGORY_l2_HAS_L3 = ".hover~.level-2 .hide-mobile ul>li>a~.level-3";
	private static final String HEADER_SUB_CATEGORY_l3 = "//following-sibling::ul[@class='level-3']/li/a";

	//-----------------------------------------------------------------

	//=====================================================================//
	//	------------------Desktop Elements-----------------------------    //
	//=====================================================================//

	@FindBy(css = HEADER)
	WebElement headerPanel;

	@FindBy(css = WRAPPER)
	WebElement wrapper;

	@FindBy(css = ".pt_cart")
	WebElement readyElementShoppingBagPage;

	@FindBy(css = MENU_UTIL + ".menu-utility-user span.customer-service")
	WebElement lnkCustomerServiceDesktop;

	@FindBy(css = MENU_UTIL + "span.email.icon")
	WebElement lnkEmailUsDesktop;

	@FindBy(css = MENU_UTIL + ".email-us-icon a img")
	WebElement imgEmailUsDesktop;

	@FindBy(css = "#profilemenu")
	WebElement lblProfileMenu;

	@FindBy(css = MENU_UTIL + ".email-us-icon")
	WebElement divEmailUsDesktop;

	@FindBy(css = MENU_UTIL + ".livechat-icon a>div")
	WebElement lnkLiveChatDesktop;

	@FindBy(css = MENU_UTIL + ".livechat-icon a img")
	WebElement imgLiveChatDesktop;

	@FindBy(css = MENU_UTIL + "a.Track-order")
	WebElement lnkTrackOrderDesktop;

	@FindBy(css = MENU_UTIL + "a.Return-items")
	WebElement lnkReturnItemsDesktop;

	@FindBy(css = MENU_UTIL + "a.Update-information")
	WebElement lnkUpdateMyInfoDesktop;

	@FindBy(css = ".primary-logo a[href*='ww']")
	WebElement lnkWomanWithin_desktop;

	@FindBy(css = ".primary-logo a[href*='jl']")
	WebElement lnkJessicaLondon_desktop;

	@FindBy(css = ".primary-logo a[href*='rm']")
	WebElement lnkRomans_desktop;
	
	@FindBy(css = ".primary-logo a[href*='ks']")
	WebElement lnkKingSize_desktop;
	
	@FindBy(css = ".primary-logo a[href*='el']")
	WebElement lnkEllos_desktop;

	@FindBy(css = ".customer-service-flyout")
	WebElement CustomerSerivceFlyOut;

	@FindBy(css = ".primary-logo a")
	WebElement lnkPrimaryLogo;

	@FindBy(css = "a.user-account")
	WebElement lnkSignInDesktop;

	@FindBy(css = ".top-menu-utility span.sign-in")
	WebElement lnkSignInDesktop1;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "a.user-account.login")
	WebElement lnkMyAccountDesktop;

	@FindBy(css = ".slider-menu a.user-account.login")
	WebElement lnkMyAccount_mobile;

	@FindBy(css = ".slider-menu .account-links a")
	WebElement lnkProfile_mobile;

	@FindBy(css = ".user-panel.registered-user.user-display .account-links .Profile")
	WebElement lnkMyAccountProfile_Mobile;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "input[id*='dwfrm_login_username_']")
	WebElement fldUserNameDesktop;

	@FindBy(css = ".input-text")
	List<WebElement> txtfldUserNameDesktop;

	@FindBy(css = ".input-text.required.error")
	WebElement txtfldPasswordDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "input[id*='dwfrm_login_password_']")
	WebElement fldPasswordDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "span.pwd-show")
	WebElement lnkShowHidePassword;

	@FindBy(css = "#dwfrm_login > fieldset > div > div.form-row.form-row-button.clickbutton > button")
	WebElement btnSignInDesktop;

	@FindBy(css = "#loginFlyout")
	WebElement btnSignInDesktop1;

	@FindBy(css = ".top-menu-utility .user-info.info>a")
	WebElement btnSignInDesktopClick;

	@FindBy(css =".user-account")
	List<WebElement> btnSignInDesktopNew;

	@FindBy(css = "//input[contains(@id,'dwfrm_login_username')]/following-sibling::span[contains(@id,'error')]")
	WebElement txtUserNameErrorDesktop;

	@FindBy(css = "span[id*='dwfrm_login_username'][class='error']")
	WebElement lblUserNameErrorDesktop;

	@FindBy(xpath = "//input[contains(@id,'dwfrm_login_password')]/following-sibling::span[contains(@id,'error')]")
	WebElement txtPasswordErrorDesktop;

	@FindBy(css = "span[id*='dwfrm_login_password'][class='error']")
	WebElement lblPasswordErrorDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "#password-reset")
	WebElement lnkForgetPasswordDesktop;

	@FindBy(css = "#main .login-box.login-account")
	WebElement divLoginBoxDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user")
	WebElement flytSignInUnregisteredDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "div.login-rememberme input.input-checkbox")
	WebElement chkRememberMeDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.registered-user")
	WebElement flytSignInRegisteredDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user .login-data .login-email")
	WebElement lblOtherBrandLoginMsgDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user .login-data .login-brands")
	WebElement lblOtherBrandLoginBrandsDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "#loggedInUserName")
	WebElement lblUserNameDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".my-account")
	WebElement lblMyAccDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP)
	WebElement lblUserAccDesktop;

	@FindBy(css = ".login-user .my-account")
	WebElement lblUserAccTablet;

	@FindBy(css = ".mobile-sign-in.hide-tablet .user-account >span:nth-child(2)")
	WebElement lblUserAccMobile;	

	@FindBy(css = ".user-info .user-account.login")
	WebElement lnkUserAccMobile;

	@FindBy(xpath = "//div[@class='account-links']/a[1]")
	WebElement lnkHamburgerOverview;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user #Facebook")
	WebElement btnFaceBookInSignInFlyoutDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".log-in")
	WebElement iconUserDesktop;

	@FindBy(css = "#primary>h1 .account-logout")
	WebElement lnkSignOutDesktop;

	@FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
	WebElement lnkArrowBreadCrumb_mobile;

	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement txtBreadCrumb_mobile;

	@FindBy(css = "div.breadcrumb")
	WebElement breadCrumb;
	
	@FindBy(css = ".breadcrumb .back-arrow")
	WebElement breadcrumbMobileArrow;

	@FindBy(css = ".breadcrumb-element")
	List<WebElement> breadcrumbElements;
	
	@FindBy(css = ".breadcrumb-element")
	WebElement breadcrumbElement;

	@FindBy(css = ".promo-banner")
	WebElement divPromoContent;

	@FindBy(css = MENU_UTIL_MINICART_DESKTOP + ".mini-cart-link")
	WebElement lnkViewBagDesktop;

	@FindBy(css = MENU_UTIL_MINICART_DESKTOP + ".mini-cart-total>a")
	WebElement lnkMiniCart;
	
	@FindBy(css = MENU_UTIL_MINICART_DESKTOP + ".mini-cart-total a span.cart-bag-qty")
	WebElement lnkMiniCartTablet;
	
	@FindBy(css = MENU_UTIL_MINICART_DESKTOP + ".mini-cart-total .minicart-quantity")
	WebElement lblMiniCartCountDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user .create-account-section a span")
	WebElement lnkCreateNewAccount;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user .create-account-section .new-customer")
	WebElement lblCreateNewAccount;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user .create-account-section .new-customer")
	WebElement lblNewCustomer;

	@FindBy(css = ".create-message")
	WebElement lblNewCustomerCreateMessage;

	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.Profile")
	WebElement lnkProfileMyAccFlytDesktop;

	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.Wishlist")
	WebElement lnkWishListMyAccFlytDesktop;

	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.Orderhistory")
	WebElement lnkOrderHistoryMyAccFlytDesktop;

	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.Rewards")
	WebElement lnkRewardPointsMyAccFlytDesktop;

	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a[title='sign out']")
	WebElement lnkSignOutMyAccFlytDesktop;

	@FindBy(css = ".login-user .my-account")
	WebElement lnkMyAccMobile;

	@FindBy(css = ".slider-menu a.Wishlist")
	WebElement lnkWishListMobile;

	@FindBy(css = MENU_UTIL_MINICART_DESKTOP + ".mini-cart-content")
	WebElement flytMyBag;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".serverSide-errorMessage.hide")
	WebElement lblServerSideLoginError;

	@FindBy(css = ".right-section #mini-cart .button.mini-cart-link-cart")
	WebElement btnCheckOut;

	@FindBy(css = ".customer-service-info.info span.customer-service")
	WebElement custService;

	@FindBy(css = "pt_account null")
	WebElement lnksigninpage;
	
	@FindBy(xpath = "//ul[@class='menu-category level-1']//a[contains(@class,'has-sub-menu nonBoldCat')]")
	List<WebElement> lnkGlobalNavCategories;
	
	@FindBy(css = ".selected .has-sub-menu")
	WebElement navGlobalNavigationSelected;
	
	@FindBy(css = "span.cqo_text")
	WebElement spanCatalogQuickOrder;
	
	@FindBy(css = ".quick-order div.iconHolder>a")
	WebElement lnkCatalogQuickOrder;


	//=====================================================================//
	//	-------------------Mobile Elements-----------------------------    //
	//=====================================================================//

	@FindBy(css = MENU_UTIL_BRAND_CONTENT_MOBILE + ".woman-within")
	WebElement lnkBrand1_mobile;

	@FindBy(css = MENU_UTIL_BRAND_CONTENT_MOBILE + ".jessica")
	WebElement lnkBrand2_mobile;

	@FindBy(css = "button.menu-toggle")
	WebElement lnkHamburger;

	@FindBy(css = ".hamburger-toggle.open")
	WebElement lnkHamburgerOpened;

	@FindBy(css = MENU_UTIL_BRAND_CONTENT_MOBILE + ".roman")
	WebElement lnkBrand3_mobile;

	@FindBy(css = ".user-info  a.user-account .sign-in")
	WebElement lnkSignInMobile;

	@FindBy(css = MENU_UTIL_USER_MOBILE + "a.user-account.login")
	WebElement lnkMyAccountMobile;

	@FindBy(css = ".user-info #loggedInUserName")
	WebElement lblUserNameMobile;

	@FindBy(css = ".login-user .my-account")
	WebElement lblMyAccMobile;

	@FindBy(css = ".account-logout a")
	WebElement lnkMyAccSignOutMobile;

	@FindBy(css = MENU_UTIL_USER_MOBILE + ".log-in")
	WebElement iconUserMobile;

	@FindBy(css = MENU_UTIL + ".mini-cart-link")
	WebElement lnkViewBagMobile;

	@FindBy(css = ".headermainsearch.hide-desktop.hide-tablet")
	WebElement iconSearch;

	@FindBy(css = "#mini-cart")
	WebElement iconMyBag;

	@FindBy(css = ".cart-bag-qty")			
	WebElement iconMyBagQty;
	
	@FindBy(css = "#mini-cart .my-bag")			
	WebElement minicartMyBag;

	@FindBy(xpath = "//a[contains(text(),'QUICK ORDER')]")
	WebElement iconQuickOrderMobile;

	@FindBy(css = ".mini-cart-link .minicart-quantity")
	WebElement lblMiniCartCountMobile;

	@FindBy(css= ".top-banner")
	WebElement stickyHeader;

	@FindBy(css= ".menu-category > li:nth-child(n)")
	List<WebElement> rootCategories;

	//=====================================================================//
	//=====================================================================//
	//=====================================================================//

	@FindBy(css = "#header input[name='q']")
	WebElement txtSearch;

	@FindBy(css=".hitgroup .hit")
	List<WebElement> lstBrandNameInSearchSuggestion;

	@FindBy(css = ".search-suggestion-wrapper")
	WebElement autoSuggestPopup;

	@FindBy(css = ".phrase-suggestions h4")
	WebElement lstPhraseTitleInSearchSuggestion;

	@FindBy(css = ".search-phrase a")
	WebElement lnksearch;

	@FindBy(css = ".search-phrase a span.original")
	WebElement lnkSearchOriginal;

	@FindBy(css = ".phrase-suggestions a.hit")
	List<WebElement> lstCategoriesInSearchSuggestion;

	@FindBy(css = ".phrase-suggestions .header")
	List<WebElement> lstPhraseSuggestionSubHeaders;

	@FindBy(css = ".product-suggestions .product-suggestion .product-link .product-details .product-name")
	List<WebElement> lstProductSuggestions;

	@FindBy(css = ".product-suggestion")
	List<WebElement> lstProductSuggestion;

	@FindBy(css = ".product-suggestion .product-image > img")
	WebElement lstProductImageInSuggestion;

	@FindBy(css = ".product-suggestion .product-name")
	List<WebElement> lstProductNameInSearchSuggestion;

	@FindBy(css = ".product-suggestion .product-name")
	List<WebElement> lstProductPriceInSuggestion;

	@FindBy(css = ".product-suggestion .product-image > img")
	List<WebElement> lstProductImage;

	@FindBy(css = ".product-suggestion .product-name")
	List<WebElement> lstProductName;

	@FindBy(css = ".product-suggestion .product-price")
	List<WebElement> lstProductPrice;

	@FindBy(css = ".simplesearch>form>fieldset>button")
	WebElement btnSearchIcon;

	@FindBy(css = ".phrase-suggestions .hitgroup")
	WebElement lstPhraseSuggestionGroups;

	@FindBy(css = ".product-suggestion:nth-child(2)")
	WebElement lstProductSuggestionSingle;

	@FindBy(css = ".phrase-suggestions .hitgroup:nth-child(1)")
	WebElement lstPhraseSuggestionGroupsSingle;

	@FindBy(css = lISTOFSEARCHSUGGESTIONS)
	WebElement divProductSuggestions;

	@FindBy(css = ".phrase-suggestions")
	WebElement divPhraseSuggestions;

	@FindBy(css= ".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable")
	WebElement mdlResetPassword;

	@FindBy(css = "#dialog-container #PasswordResetForm #dwfrm_requestpassword_email")
	WebElement txtEmailInResetPasswordMdl;

	@FindBy(css = "#dialog-container #PasswordResetForm #dwfrm_requestpassword_email-error")
	WebElement lblEmailErrorInResetPassword;

	@FindBy(css = ".primary-logo a")
	WebElement brandLogo;

	@FindBy(css = ".top-menu-utility .woman-within img")
	WebElement lnkWomanWithinBrandImage;

	@FindBy(css = ".top-menu-utility .jessica img")
	WebElement lnkJesiccaBrandImage;

	@FindBy(css = ".scroll-wrapper .brand-logos .woman-within img")
	WebElement lnkWomanWithinBrandImage_mobile;

	@FindBy(css = ".scroll-wrapper .brand-logos .jessica img")
	WebElement lnkJesiccaBrandImage_mobile;

	@FindBy(css = ".scroll-wrapper .brand-logos .roman img")
	WebElement lnkRoamansBrandImage_mobile;

	@FindBy(css = ".top-menu-utility .roman img")
	WebElement lnkRoamansBrandImage;

	@FindBy(css = ".product-suggestion .product-image img")
	List<WebElement> lstProductImageInSearchSuggestion;

	@FindBy(css = "input#q")
	WebElement txtSearchMobile;

	@FindBy(css = ".account-logout a")
	WebElement lnkSignOutMobile;
	
	@FindBy(css = ".menu-utility-user")
	WebElement divUtilityBar;
	
	@FindBy(css = "div.offer-flyout-main")
	WebElement divOfferFlyout;
	
	@FindBy(css = ".header-search")
	WebElement divSearchBox;
	
	@FindBy(css = ".header-promo-banner")
	WebElement divTopPromoBanner;
	
	@FindBy(css = ".menu-category.level-1")
	WebElement divGlobalNavigation;
	
	@FindBy(css = ".menu-category.level-1 li a[title='Explore']")
	WebElement lnkExplore;
	
	@FindBy(css = "//div[@class='slider-menu']//ul[@class='menu-category level-1']//li/child::a[contains(text(), 'Explore')]")
	WebElement lnkExploreMobile;

	//================================================================================
	//			Other Page Ready Elements for Verification
	//================================================================================

	@FindBy(css = ".pt_account")
	WebElement readyElementMyAccountPage;
	
	@FindBy(css = "div[class='pt_account overview']")
	WebElement readyElementMyAccountPageDiv;

	@FindBy(css = "#primary .login-box.login-account")
	WebElement readyElementSignInPage;

	@FindBy(css = ".pt_product-search-result")
	WebElement readyElementSLPPage;
	//================================================================================
	//			WebElements Declaration End
	//================================================================================

	@FindBy(css = ".headermainsearch.hide-desktop.hide-tablet")
	WebElement btnSearchIconMobile;

	@FindBy(xpath = ".//*[@id='navigation-bar']/ul/li[4]/a")
	WebElement lnkNavbarDresses;

	@FindBy(xpath = "//a[contains(text(),'QUICK ORDER')]")
	WebElement lnkQuickOrder;

	@FindBy(css = ".button.mini-cart-link-cart")
	WebElement lnkCheckOut;

	@FindBy(css = "div[class='mini-cart-content']")
	WebElement divMiniCartOverlay;


	@FindBy(xpath = ".//*[@id='profilemenu']/div/div/a[3]")
	WebElement lnkOrderStatus;

	@FindBy(xpath = ".//*[@id='profilesmenu']/div/div/a[3]")
	WebElement txtLoginErrorMsg;

	@FindBy(css = "#bc-chat-container")
	WebElement mdlLiveChat;

	@FindBy(css = ".x-panel-bwrap")
	WebElement btnCloseStorefrontKit;

	@FindBy(css = ".fixed-header")
	WebElement fixedHeader;
	
	@FindBy(css = ".footer-down")
	WebElement footerDown;

	//GiftcardPage

	@FindBy(xpath = "//footer//a[contains(text(),'Gift Card Balance')]")
	WebElement lnkGiftCardPage;

	@FindBy(xpath = "//a[contains(@class,'breadcrumb-element')][not(contains(@class,'current'))][last()]")
	WebElement lnkSubCategoryBC;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement lnkSubCategoryBC_Mobile;

	@FindBy(css = ".top-menu-utility")
	WebElement topMenuUtilityBar;

	@FindBy(css = "li.hoverIntent div.level-2")
	WebElement currentLevel2;
	
	@FindBy(css = "li.hoverIntent div.level-2 > .sub-cat-wrapper > .banner")
	WebElement currentLevel2Banner;
	
	@FindBy(css = "li.hoverIntent>a.has-sub-menu")
	WebElement hoveredGlobalNavigationLevel1;
	
	@FindBy(css = "div.promo-banner a.learn-more")
	WebElement lnkLearnMore;
	
	@FindBy(css = "div.promo-banner a[href*='/platinum']")
	WebElement lnkLearnMoreApproved;
	
	@FindBy(css = "div.promo-banner .promo-text a[href*='/apply']")
	WebElement lnkApplyNow;
	
	@FindBy(css = "div.promo-banner a[href*='/apply']")
	WebElement lnkAcceptNow;
	
	@FindBy(css = "div.promo-banner a")
	WebElement lnkRewardsHeader;

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public Headers(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, headerPanel))) {
			Log.fail("Header Panel didn't display", driver);
		}
		elementLayer = new ElementLayer(driver);
		hamburgerMenu = new HamburgerMenu(driver);

		//((JavascriptExecutor)driver).executeScript("document.getElementById('DW-SFToolkit').style.display='none'");
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		//Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the element which have exact variable name
	 * @param expectedEle - String "Expected Element"
	 * @return WebElement - Actual WebElement with expected Element variable name
	 * @throws Exception - Exception
	 */
	public static WebElement getElement(String expectedEle)throws Exception{
		WebElement element = null;
		try{
			Field elementField = Headers.class.getClass().getDeclaredField(expectedEle);
			elementField.setAccessible(true);
			element = ((WebElement) elementField.get(Headers.class.getClass()));
		}//try
		catch(Exception e){
			e.printStackTrace();

		}//catch
		return element;
	}

	/**
	 * To get the Y-location of an element 
	 * @param driver - driver instance
	 * @param heightElement - name of WebElement in String that you need Y-Location of
	 * @param obj - page object
	 * @return int - Y-location of the element 
	 * @throws Exception - Exception
	 */
	public int getElementY(WebDriver driver, String heightElement, Object obj)throws Exception{
		Field f = obj.getClass().getDeclaredField(heightElement);
		f.setAccessible(true);
		WebElement element = ((WebElement) f.get(obj));
		Point elementPoint = element.getLocation();

		Log.event("Sticky Header Position :: " + elementPoint.y);
		return elementPoint.y;
	}

	/**
	 * To choose between brands in header
	 * @param brandToNavigate - Which brand homepage to navigate
	 * @return HomePage - Home page of related brand
	 * @throws Exception - Exception
	 */
	public HomePage chooseBrandFromHeader(Brand brandToNavigate)throws Exception{
		switch(brandToNavigate){
		case jl:
			BrowserActions.clickOnElementX(lnkJessicaLondon_desktop, driver, "Brand In Header Brand Navigation Section");
			break;
		case ww:
			BrowserActions.clickOnElementX(lnkWomanWithin_desktop, driver, "Brand In Header Brand Navigation Section");
			break;
		case rm:
			BrowserActions.clickOnElementX(lnkRomans_desktop, driver, "Brand In Header Brand Navigation Section");
			break;
		case ks:
			BrowserActions.clickOnElementX(lnkKingSize_desktop, driver, "Brand In Header Brand Navigation Section");
			break;
		case el:
			BrowserActions.clickOnElementX(lnkEllos_desktop, driver, "Brand In Header Brand Navigation Section");
			break;
		default:
			Log.failsoft(brandToNavigate.getConfiguration() + " is not configured.");
			BrowserActions.clickOnElementX(brandLogo, driver, "Brand logo.");
		}

		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();
	}

	/**
	 * To click on brand logo
	 * @return HomePage
	 * @throws Exception - Exception
	 */

	public HomePage clickOnBrandFromHeader()throws Exception{
		BrowserActions.clickOnElementX(lnkPrimaryLogo, driver, "Brand In Header Brand");
		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();
	}
	

	/**
	 * To check Global Header Is Sticky When Scroll Down
	 * @return HomePage
	 * @throws Exception - Exception
	 */

	public boolean checkGlobalHeaderIsStickyWhenScrollDown()throws Exception{
		BrowserActions.scrollInToView(footerDown, driver);
		return Utils.verifyCssPropertyForElement(fixedHeader, "position", "fixed");
	}
	

	/**
	 * To search the provided keyword
	 * @param textToSearch - 
	 * @return SearchResultPage
	 * @throws Exception - Exception
	 * 
	 */
	public SearchResultPage searchProductKeyword(String textToSearch)throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		final long startTime = StopWatch.startTime();
		if(runPltfrm.equals("mobile")){
			BrowserActions.clickOnElementX(btnSearchIconMobile, driver, "Serach icon");
			BrowserActions.typeOnTextField(txtSearch, "", driver,"Search field");
			BrowserActions.typeOnTextField(txtSearch, textToSearch, driver,"Search field");
			txtSearch.sendKeys(Keys.ENTER);
		}
		else
		{
			BrowserActions.scrollToTopOfPage(driver);
			BrowserActions.typeOnTextField(txtSearch, "", driver,"Search field");
			BrowserActions.typeOnTextField(txtSearch, textToSearch, driver,"Search field");
			BrowserActions.javascriptClick(btnSearchIcon, driver, "Serach icon");
		}
		Log.event("Searched the provided product!",StopWatch.elapsedTime(startTime));
		BrowserActions.scrollToTopOfPage(driver);
		return new SearchResultPage(driver).get();
	}

	/**
	 * To get the text from primary search input box
	 * @return string 
	 * @throws Exception - Exception
	 */
	public String getTextFromPrimarySearchBox()throws Exception{
		return BrowserActions.getText(driver, txtSearch, "Primary Search Textbox");
	}

	/**
	 * To type given string into search input box
	 * @param txtToType - 
	 * @return string 
	 * @throws Exception - Exception
	 */
	public String typeTextInSearchField(String txtToType)throws Exception{
		Log.event("Typing text("+ txtToType +") into primary search box!");
		String txtToReturn = BrowserActions.typeOnTextField(txtSearch, txtToType, driver, "Primary Searchbox");
		Utils.waitForElement(driver, lstPhraseTitleInSearchSuggestion);
		Utils.waitForElement(driver, autoSuggestPopup);
		return txtToReturn;
	}

	/**
	 * to get the text from search input box
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getEnteredTextFromSearchTextBox() throws Exception {		
		String	txtSearchTextBox = txtSearch.getAttribute("value");
		return txtSearchTextBox;

	}

	/**
	 * To click on the brand name in search suggestion with the given index
	 * @param index - 
	 * @return SearchResultPage
	 * @throws Exception - Exception
	 */
	public SearchResultPage clickOnBrandNameInSearchSuggestionByIndex(int index) throws Exception{
		BrowserActions.clickOnElementX(lstBrandNameInSearchSuggestion.get(index-1), driver, "brand name");
		return new SearchResultPage(driver).get();
	}

	/**
	 * to get the text from search suggestion
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSearchTextFromSearchSuggestion()throws Exception{
		return BrowserActions.getText(driver, lnksearch, "Seach Suggestion");
	}

	/**
	 * to verify the text from search suggestion is violet color
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySearchTexColortFromSearchSuggestion()throws Exception{
		return Utils.verifyCssPropertyForElement(lnksearch, "color", "rgba(101, 30, 64, 1)");
	}

	/**
	 * to verify the flyout on the sticky header
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyRootCategoryHoverFlyout()throws Exception{
		Random rand = new Random();
		int randomRootIndex = rand.nextInt(5);
		BrowserActions.mouseHover(driver, rootCategories.get(randomRootIndex));
		Utils.waitForPageLoad(driver);
		WebElement flyOut = rootCategories.get(randomRootIndex).findElement(By.cssSelector(".level-2"));
		Log.event("Hovered Category Visibility :: " + flyOut.getCssValue("visibility"));
		if(flyOut.getCssValue("visibility").equals("visible"))
			return true;
		else
			return false;
	}

	/**
	 * To Mouse hover on category based on index
	 * @param index - index number of the category
	 * @throws Exception - Exception
	 */
	public void mouseHoverOnCategoryByIndex(int index)throws Exception{
		BrowserActions.scrollToBottomOfPage(driver);
		BrowserActions.scrollToTopOfPage(driver);
		BrowserActions.mouseHover(driver, rootCategories.get(index));
		BrowserActions.mouseHover(driver, rootCategories.get(index));
		Log.event("Mouse hovered on " + index + "th Category.");
		Utils.waitForElement(driver, currentLevel2);
	}

	/**
	 * To Mouse hover on category based on category name
	 * @param CategoryName - Category name to hover 
	 * @throws Exception - Exception
	 */
	public void mouseHoverOnCategoryByCategoryName(String CategoryName)throws Exception{
		BrowserActions.scrollToBottomOfPage(driver);
		BrowserActions.scrollToTopOfPage(driver);

		WebElement category = null;
		try {
			category = driver.findElement(By.cssSelector(".menu-category.level-1 a[title='"+CategoryName+"']"));
		} catch (NoSuchElementException ex) {
			Log.failsoft("The given Category "+CategoryName+" is not present");
		}

		BrowserActions.mouseHover(driver, category);
		BrowserActions.mouseHover(driver, category);

		Log.event("Mouse hovered on " + CategoryName + " Category.");
		Utils.waitForElement(driver, currentLevel2);
	}

	/**
	 * to verify the text from search suggestion is violet color
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySearchTextBoldFromSearchSuggestion()throws Exception{
		//font-weight is not set for the element. Hence using font-family
		return Utils.verifyCssPropertyForElement(lnksearch, "font-family", "Bold");
	}

	/**
	 * to click the text from search suggestion and navigate to Search Results page
	 * @return SearchResultPage
	 * @throws Exception - Exception
	 */
	public SearchResultPage clickSearchTextFromSearchSuggestion()throws Exception{
		BrowserActions.javascriptClick(lnksearch, driver, "Search Suggestion");
		return new SearchResultPage(driver).get();
	}

	/**
	 * to get the text from search phrase suggestion sub headers. ex. Categories and Pages that might be interesting
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getTextFromPhraseSuggestionSubHeaders()throws Exception{
		return BrowserActions.getText(driver, lstPhraseSuggestionSubHeaders, "Phrase Suggestion Sub Headers");
	}

	/**
	 * to get the text from search phrase suggestion
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getTextFromPhraseSuggestion()throws Exception{
		return BrowserActions.getText(driver, lstCategoriesInSearchSuggestion, "Seach Suggestion");
	}

	/**
	 * to get the text from list of products in search suggestion
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getTextFromProductSuggestion()throws Exception{
		return BrowserActions.getText(driver, lISTOFSEARCHSUGGESTIONS, "Seach Suggestion");
	}

	/**
	 * to get the text from list of products in search suggestion
	 * @return List
	 * @throws Exception - Exception
	 */
	public List<String> getTextFromProductSuggestions()throws Exception{
		return BrowserActions.getText(lstProductSuggestions, "Seach Suggestion", driver);
	}

	/**
	 * to get the text from list of products in search suggestion
	 * @return List
	 * @throws Exception - Exception
	 */
	public List<String> getPriceRangesFromProductSuggestions()throws Exception{
		List<String> productsuggestions = BrowserActions.getText(lstProductSuggestions, "Seach Suggestion", driver);
		List<String> prices = new ArrayList<String>();

		//Get the prices of all the suggested products
		for (String text : productsuggestions) 
		{
			String[] temp = text.split("\\$");
			prices.add(temp[1]);
		}

		return prices;
	}

	/**
	 * to get the text from list of products in search suggestion
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPriceRangesFromProductSuggestionsIsInAscendingOrder()throws Exception{
		List<String> prices = getPriceRangesFromProductSuggestions();
		Boolean flag = false;

		//Check if the price are in ascending order
		for (int i=1; i <= prices.size(); i++) 
		{
			double lower = Double.parseDouble(prices.get(i-1));
			double higher = Double.parseDouble(prices.get(i));
			if(higher >= lower)
				flag = true;
			else
				return false;
		}

		return flag;
	}

	/**
	 * to verify if the product price is displayed right to the product image in search product suggestion
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPricesDisplayedRightOfProductImage()throws Exception{
		Boolean flag = false;
		for (int i=0; i < lstProductSuggestion.size(); i++) 
		{
			if(BrowserActions.verifyHorizontalAllignmentOfElements(driver, lstProductPrice.get(i), lstProductImage.get(i)))
				flag = true;
			else
				return false;

		}
		return flag;
	}

	/**
	 * to verify if the product price is displayed below to the product name in search product suggestion
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPricesDisplayedBelowProductName()throws Exception{
		Boolean flag = false;
		for (int i=0; i < lstProductSuggestion.size(); i++) 
		{
			if(BrowserActions.verifyVerticalAllignmentOfElements(driver, lstProductName.get(i), lstProductPrice.get(i)))
				flag = true;
			else
				return false;

		}
		return flag;
	}

	/**
	 * To get the search phrase color in search suggestion
	 * @return String - color of search phrase
	 * @throws Exception - Exception
	 */
	public String getSearchPhraseColor()throws Exception{
		return lnksearch.getCssValue("color");
	}

	/**
	 * to get the font weight of phrase in search suggestion
	 * @return font-weight
	 * @throws Exception - Exception
	 */
	public String getSearchPhraseWeight()throws Exception{
		return lnksearch.getCssValue("font-weight");
	}

	/**
	 * To hover the mouse pointer on the sign in button
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean mouseOverSignIn() throws Exception {
		if(runPltfrm.equals("desktop")){
			BrowserActions.mouseHover(driver, lnkSignInDesktop1, "Sign In Menu Link ");
		}else if(runPltfrm.equals("tablet")){
			try{
				BrowserActions.actionClick(lnkSignInDesktop, driver, "Sign In Menu Link ");
			}catch(WebDriverException x){
				BrowserActions.javascriptClick(lnkSignInDesktop, driver, "Sign In Menu Link ");
			}

		}else{
			return false;
		}

		if(Utils.waitForElement(driver, flytSignInUnregisteredDesktop, 3)){
			Log.event("SignIn Flyout displayed");
			return true;
		}else{
			Log.event("SignIn Flyout not displayed.");
			return false;
		}
	}

	/**
	 * To get the value entered in the email field
	 * @return Value entered in the Email field
	 * @throws Exception -
	 */
	public String getValueEnteredInEmailAddress()throws Exception{
		return BrowserActions.getTextFromAttribute(driver, fldUserNameDesktop, "value", "Email Address");
	}


	/**
	 * To hover the mouse pointer on the sign in button
	 * @throws Exception - Exception
	 */
	public void mouseOverMyBag() throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		if(runPltfrm.equals("desktop")){
			BrowserActions.mouseHover(driver, lnkViewBagDesktop, "Cart page Link ");
		}
	}

	/**
	 * To hover the mouse pointer on the sign in button
	 * @throws Exception - Exception
	 */
	public void mouseOverMyAccount() throws Exception {
		if(Utils.isTablet()){
			BrowserActions.clickOnElement(lblUserAccTablet, driver, "Account click!!");
			if(!Utils.waitForElement(driver, lblProfileMenu)) {
				Log.event("Hover menu didn't open. Using workaround...");
				forceOpenSignInHover(driver);
			}
		}
		else{
			if(Utils.getRunBrowser(driver).contains("ie") || Utils.getRunBrowser(driver).contains("internet") || Utils.getRunBrowser(driver).contains("safari")){
				((JavascriptExecutor)driver).executeScript("document.querySelector('.menu-utility-user .user-info .user-panel.registered-user').style.display='block';");
			}else
				BrowserActions.mouseHover(driver, lblUserAccDesktop, "Sign In Menu Link ");

		}
		if(Utils.waitForElement(driver, flytSignInRegisteredDesktop, 3)){
			Log.event("SignIn Flyout displayed");
		}else{
			if(Utils.isTablet()){
				BrowserActions.clickAndHoldOnElement(lblUserAccTablet, driver, "Sign In Link ");
			}			
		}


	}

	/**
	 * To open/close the hamburger menu in mobile
	 * @param String - open or close state
	 * @return Object - page object
	 * @throws Exception - Exception
	 */
	public Object openCloseHamburgerMenu(String state) throws Exception {
		if (Utils.waitForElement(driver, lnkHamburger, 2) && (state.equals("open"))) {
			BrowserActions.javascriptClick(lnkHamburger, driver, "Hamburger menu ");
			Log.event("Hamburger menu opened");
			return new HamburgerMenu(driver).get();
		} else if(Utils.waitForElement(driver, lnkHamburgerOpened, 2) && (state.equals("close"))){
			BrowserActions.javascriptClick(lnkHamburger, driver, "Hamburger menu ");
			Log.event("Hamburger menu closed");
			return new Headers(driver).get();
		} else if (runPltfrm.equals("tablet")){
			Log.event("The tablet orientation is landscape");
			return null;
		} else {
			return new HamburgerMenu(driver).get();
		}

	}

	/**
	 * To type the username in username field in Sign in flyout
	 * @param String - username to type 
	 * @throws Exception - Exception
	 */
	public void typeUserNameInFlyout(String username)throws Exception{
		BrowserActions.typeOnTextField(fldUserNameDesktop, username, driver, "user name field");
		Log.event("--->>>Typed User name :: " + username);
	}

	/**
	 * To type the password in password field in Sign in flyout
	 * @param String - password to type 
	 * @throws Exception - Exception
	 */
	public void typePasswordInFlyout(String password)throws Exception{
		BrowserActions.typeOnTextField(fldPasswordDesktop, password, driver, "password field");
		Log.event("--->>>Typed Password :: " + password);
	}

	/**
	 * To click on the sign in button in the Sign in flyout
	 * @return Object - page object
	 * @throws Exception - Exception
	 */
	public Object clickOnSignInDesktop()throws Exception{

		BrowserActions.clickOnElementX(btnSignInDesktop1, driver, "Sign In Button");
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, readyElementMyAccountPage, 2)){
			Log.event("Logged in successfully with given account.");
			return new MyAccountPage(driver).get();
		}else if(Utils.waitForElement(driver, divLoginBoxDesktop, 2)){
			Log.event("Cannot login with given credentials.");
			return new SignIn(driver).get();
		}else if(Utils.waitForElement(driver, readyElementMyAccountPage)){
			Log.event("Logged in successfully with given account.");
			return new MyAccountPage(driver).get();
		}else 
			return new Headers(driver).get();
	}


	/**
	 * To Check/UnCheck the 'Remember Me' Checkbox 
	 * @param needCheck - True to check the Remember me Checkbox else false
	 * @throws Exception - Exception
	 */
	public void checkOnRememberMeDesktop(boolean needCheck)throws Exception{

		if(needCheck) {
			if(!(chkRememberMeDesktop.isSelected())) {
				BrowserActions.clickOnElementX(chkRememberMeDesktop, driver, "Sign In Button");
			}
		} else {
			if(chkRememberMeDesktop.isSelected()) {
				BrowserActions.clickOnElementX(chkRememberMeDesktop, driver, "Sign In Button");
			}
		}

		Utils.waitForPageLoad(driver);

	}

	/**
	 * To click on the sign in button in the Sign in flyout
	 * @return sign in
	 * @throws Exception - Exception
	 */
	public SignIn clickOnSignInMobile()throws Exception{
		BrowserActions.clickOnElementX(lnkSignInMobile, driver, "Sign In link in Mobile");
		Utils.waitForPageLoad(driver);
		return new SignIn(driver).get();
	}

	/**
	 * To navigate to my account page
	 * @return MyAccountPage
	 * @throws Exception - Exception
	 */

	public SignIn clickOnSignInLink()throws Exception{
		WebElement signInLink = driver.findElement(By.cssSelector("a.user-account"));
		BrowserActions.javascriptClick(signInLink, driver, "Sign In link in Desktop");
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, signInLink)) {
			BrowserActions.javascriptClick(signInLink, driver, "Sign In link in Desktop");
		}
		return new SignIn(driver).get();
	}

	/**
	 * To navigate to my account with valid credentials
	 * @param String - username 
	 * @param String - password 
	 * @param boolean - (Optional) createNewUser
	 * @return MyAccountPage - MyAccountPage page object
	 * @throws Exception - Exception
	 */
	public MyAccountPage navigateToMyAccount(String username, String password, boolean... createNewUser)throws Exception{
		boolean state = mouseOverSignIn();
		Log.event("--->>Flyout state :: " + state);
		if(state){
			typeUserNameInFlyout(username);
			typePasswordInFlyout(password);
			checkOnRememberMeDesktop(true);
			Object obj = clickOnSignInDesktop();
			Utils.waitForPageLoad(driver);
			String returnedClass = obj.getClass().getSimpleName();
			Log.event("Returned Class :: " + returnedClass);
			if(!returnedClass.equals(MyAccountPage.class.getSimpleName())){
				if(createNewUser.length > 0 && (createNewUser.length > 0 ? createNewUser[0] : false))
					obj = GlobalNavigation.registerNewUser(driver, username+"|"+password);
				else
					Log.fail("Invalid User credentials. Please check the spelling.", driver);
			}
			try{
				return (MyAccountPage)obj;
			}catch(ClassCastException e){
				Log.fail("Class Cast Exception Occured. Please verify the method logic.", driver);
				return null;
			}


		}else{

			SignIn signIn;
			if(runPltfrm.equals("mobile")){
				openCloseHamburgerMenu("open");
				signIn = clickOnSignInMobile();
			}else{
				signIn = clickOnSignInLink();
			}
			signIn.typeOnEmail(username);
			signIn.typeOnPassword(password);
			signIn.clickSignInButton();

			if(!Utils.waitForElement(driver, readyElementMyAccountPage)){
				if(createNewUser.length > 0 && (createNewUser.length > 0 ? createNewUser[0] : false))
					GlobalNavigation.registerNewUser(driver, username+"|"+password);
				else
					Log.fail("Invalid User credentials. Please check the spelling.", driver);
			}
			try{
				return new MyAccountPage(driver).get();
			}catch(ClassCastException e){
				Log.fail("Class Cast Exception Occured. Please verify the method logic.", driver);
				return null;
			}
		}
	}




	/**
	 * To navigate to my account with valid credentials
	 * @param username - 
	 * @param password - 
	 * @param chkRemMe -
	 * @return MyAccountPage
	 * @throws Exception - Exception
	 */
	public MyAccountPage navigateToMyAccountWithRememberChkBox(String username, String password, boolean chkRemMe, boolean... createNewUser)throws Exception{
		boolean state = mouseOverSignIn();
		Log.event("--->>Flyout state :: " + state);
		if(state){
			
			typeUserNameInFlyout(username);
			typePasswordInFlyout(password);
			
			checkOnRememberMeDesktop(chkRemMe);
			Object obj = clickOnSignInDesktop();
			
			Utils.waitForPageLoad(driver);
			
			String returnedClass = obj.getClass().getSimpleName();
			Log.event("Returned Class :: " + returnedClass);
			
			if(!returnedClass.equals(MyAccountPage.class.getSimpleName())){
				if(createNewUser.length > 0 && (createNewUser.length > 0 ? createNewUser[0] : false))
					obj = GlobalNavigation.registerNewUser(driver, username+"|"+password);
				else
					Log.fail("Invalid User credentials. Please check the spelling.", driver);
			}
			
			try{
				return (MyAccountPage)obj;
			}catch(ClassCastException e){
				Log.fail("Class Cast Exception Occured. Please verify the user account is active.", driver);
				return null;
			}


		}else{

			SignIn signIn;
			if(runPltfrm.equals("mobile")){
				openCloseHamburgerMenu("open");
				signIn = clickOnSignInMobile();
			}else{
				signIn = clickOnSignInLink();
			}
			
			signIn.typeOnEmail(username);
			signIn.typeOnPassword(password);
			signIn.checkOnRememberMeMobile(chkRemMe);
			signIn.clickSignInButton();
			
			if(!Utils.waitForElement(driver, readyElementMyAccountPage)){
				if(createNewUser.length > 0 && (createNewUser.length > 0 ? createNewUser[0] : false))
					GlobalNavigation.registerNewUser(driver, username+"|"+password);
				else
					Log.fail("Invalid User credentials. Please check the spelling.", driver);
			}
			
			try{
				return new MyAccountPage(driver).get();
			}catch(ClassCastException e){
				Log.fail("Class Cast Exception Occured. Please verify the method logic.", driver);
				return null;
			}
		}
	}




	/**
	 * To navigate to my accoutn page
	 * @return MyAccountPage
	 * @throws Exception - Exception
	 */

	public MyAccountPage navigateToMyAccount()throws Exception{
		Utils.waitForPageLoad(driver);

		if(Utils.isDesktop()){
			BrowserActions.clickOnElementX(lnkMyAccountDesktop, driver, "My Account link in header");
		}
		else if(Utils.isTablet()) {
			BrowserActions.clickOnElementX(lnkMyAccountDesktop, driver, "My Account link in header");
			Utils.waitForPageLoad(driver);
			BrowserActions.clickOnElementX(lnkMyAccountDesktop, driver, "My Account link in header");
		}
		else{
			BrowserActions.javascriptClick(lnkMyAccMobile, driver, "My Account link in hamburger");
			BrowserActions.javascriptClick(lnkHamburgerOverview, driver, "Overview link");
		}
		Utils.waitForPageLoad(driver);
		return new MyAccountPage(driver).get();
	}

	/**
	 * To navigate to profile page in mobile
	 * @return ProfilePage
	 * @throws Exception - Exception
	 */

	public ProfilePage navigateTolnkProfile_mobile()throws Exception{
		Utils.waitForPageLoad(driver);
		if(runPltfrm.equals("mobile")){
			BrowserActions.javascriptClick(lnkProfile_mobile, driver, "My Account link in header");
		}
		Utils.waitForPageLoad(driver);
		return new ProfilePage(driver).get();
	}

	/**
	 * To navigate to profile page
	 * @return ProfilePage
	 * @throws Exception - Exception
	 */

	public ProfilePage navigateToProfilePage()throws Exception{
		Utils.waitForPageLoad(driver);
		if (runPltfrm.equals("mobile")) {
			openCloseHamburgerMenu("open");
			BrowserActions.clickOnElementX(lnkMyAccount_mobile, driver, "My Account link in header");
			BrowserActions.clickOnElementX(lnkMyAccountProfile_Mobile, driver, "Profile link");
		}
		Utils.waitForPageLoad(driver);
		return new ProfilePage(driver).get();
	}

	/**
	 * To get the User name from header in desktop
	 * @return String - User name
	 * @throws Exception - Exception
	 */
	public String getUserName()throws Exception{
		String userName= null;
		if(Utils.isMobile()) {
			openCloseHamburgerMenu("open");
			userName = BrowserActions.getText(driver, lblUserNameMobile, "User name Lable in Header").replace("Hello", "").replace(",", "").trim();
			openCloseHamburgerMenu("close");
		}
		else
			userName = BrowserActions.getText(driver, lblUserNameDesktop, "User name Lable in Header").replace("Hello", "").replace(",", "").trim();
		
		return userName;
	}

	/**
	 * To get the text from MyAccount lable in header
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getMyAccountTxtDesktop()throws Exception{
		return BrowserActions.getText(driver, lblMyAccDesktop, "User name Lable in Header");
	}

	/**
	 * To select a product form search suggestion and navigate to Pdp Page 
	 * @param product(Optional) - An Integers represents nth item in list, 
	 * 							  or a string represents product name in list,
	 * 							  If empty, a random item will be choosen 
	 * @return PdpPage - PdpPage of exact product selected in search suggestion
	 * @throws Exception - Exception
	 */
	public PdpPage selectProductFromSearchSuggestion(String... product)throws Exception{
		if(product.length > 0){
			try{
				int num = Integer.parseInt(product[0]) - 1;
				BrowserActions.javascriptClick(lstProductNameInSearchSuggestion.get(num-1), driver, "nth Product in Product Suggestion ");
			}catch(NumberFormatException e){
				int i=0;
				while(i<lstProductSuggestion.size()){
					WebElement singleProduct = lstProductSuggestion.get(i).findElement(By.cssSelector(".product-name"));
					if(singleProduct.getText().equalsIgnoreCase(product[0])){
						BrowserActions.javascriptClick(singleProduct, driver, "Product(" + product[0] + ") in Product suggestion ");
						break;
					}
				}
			}
		}else{
			int rand = Utils.getRandom(0, lstProductSuggestion.size());
			BrowserActions.javascriptClick(lstProductSuggestion.get(rand), driver, "Random product in product suggesstion ");
		}
		Utils.waitForPageLoad(driver);

		return new PdpPage(driver).get();
	}

	/**
	 * To navigate to search result page for given search term
	 * @param searchKey - Search term
	 * @return SearchResultPage
	 * @throws Exception - Exception
	 */
	public SearchResultPage navigateToSLP(String searchKey)throws Exception{
		if(Utils.isMobile() && !Utils.waitForElement(driver, txtSearch)) {
			BrowserActions.javascriptClick(iconSearch, driver, "Search Icon ");
		}
		typeTextInSearchField(searchKey);
		if(Utils.isMobile())
			txtSearch.sendKeys(Keys.ENTER);
		else
			BrowserActions.javascriptClick(btnSearchIcon, driver, "Search Icon ");
		Utils.waitForPageLoad(driver);
		return new SearchResultPage(driver).get();
	}

	/**
	 * To verify the search phrase presence in left side enhanced search suggestion
	 * in auto suggest pop up
	 * @param String - searchKey 
	 * @return boolean - 'true', if displayed, 'false', if not displayed
	 * @throws Exception - Exception
	 */
	public boolean verifySearchPhraseInEnhancedSearch(String searchKey)throws Exception{
		List<WebElement> lstCategorySuggestions = lstPhraseSuggestionGroups.findElements(By.cssSelector("a.hit"));
		for(int i = 0; i < lstCategorySuggestions.size(); i++){
			if(!lstCategorySuggestions.get(i).getAttribute("innerHTML").toLowerCase().contains(searchKey.toLowerCase()))
				return false;
		}
		return true;
	}

	/**
	 * To get product name from search suggestions
	 * @return string - list of product name
	 * @throws Exception - Exception
	 */

	public List<String> getProductNameFromSearchSuggestion()throws Exception{
		List<String> prdName = new ArrayList<String>();
		prdName = BrowserActions.getText(lstProductNameInSearchSuggestion, "Product name in search suggestion", driver);
		return prdName;
	}

	/**
	 * To hover the mouse pointer on the sign in button
	 * @throws Exception - Exception
	 */
	public void mouseOverCustomerService() throws Exception {
		if(runPltfrm.equals("tablet")){
			BrowserActions.clickAndHoldOnElement(lnkCustomerServiceDesktop, driver, "Customer Service link");
		}else {
			BrowserActions.mouseHover(driver, lnkCustomerServiceDesktop, "Customer Service link");
			if(Utils.waitForElement(driver, lnkEmailUsDesktop, 3)){
				Log.event("Customer service Flyout displayed");
			}else{
				BrowserActions.scrollToBottomOfPage(driver);
				BrowserActions.mouseHover(driver, lnkCustomerServiceDesktop, "Customer Service link");
				if(!(Utils.waitForElement(driver, lnkEmailUsDesktop, 3))){
					Log.fail("Customer service Flyout not opened", driver);
				}
			}
		}
	}



	/**
	 * To hover the mouse pointer on the My Bag Icon
	 * @return Mini cart page
	 * @throws Exception - Exception
	 */
	public MiniCartPage mouseOverMiniCart() throws Exception {

		if(Utils.getRunDevicePlatform(driver).equals("desktop")){
			if(Utils.getRunBrowser(driver).equals("safari")||Utils.getRunBrowser(driver).equals("internet explorer")){

				Log.fail("Browser Limitations : Mouse over functionality is not working", driver);
			}
			else{
				BrowserActions.scrollToTopOfPage(driver);
				BrowserActions.mouseHover(driver, lnkMiniCart, "My Bag Link ");
				if(Utils.waitForElement(driver,divMiniCartOverlay)){
					Log.event("Mini Cart Flyout displayed");
				}
			}
		}else{
			if(runPltfrm.equals("tablet")){
				Actions action = new Actions(driver);
				action.clickAndHold(lnkMiniCart).build().perform();
			}
		}

		return new MiniCartPage(driver).get();
	}



	/**
	 * To navigate to Contact Us page
	 * @return ContactUsPage
	 * @throws Exception - Exception
	 */
	public ContactUsPage navigateToContactUs()throws Exception{
		BrowserActions.clickOnElementX(lnkEmailUsDesktop, driver, "Email Us ");
		Utils.waitForPageLoad(driver);
		return new ContactUsPage(driver).get();
	}

	/**
	 * To navigate to Live Chat
	 * @throws Exception - Exception
	 */
	public void navigateToLiveChat()throws Exception{
		BrowserActions.clickOnElementX(imgLiveChatDesktop, driver, "Live Chat Us ");
		Utils.waitForElement(driver, mdlLiveChat);
	}

	/**
	 * To navigate to Track My Order page
	 * @return TrackMyOrderPage
	 * @throws Exception - Exception
	 */
	public TrackOrderPage navigateToTrackMyOrder()throws Exception{
		BrowserActions.clickOnElementX(lnkTrackOrderDesktop, driver, "Track Order ");
		Utils.waitForPageLoad(driver);
		return new TrackOrderPage(driver).get();
	}

	/**
	 * To navigate to Return Items page
	 * @return ReturnItemsPage
	 * @throws Exception - Exception
	 */
	public TrackOrderPage navigateToReturnItems()throws Exception{
		BrowserActions.clickOnElementX(lnkReturnItemsDesktop, driver, "Return items ");
		Utils.waitForPageLoad(driver);
		return new TrackOrderPage(driver).get();
	}

	/**
	 * To navigate to Update My Info page
	 * @return UpdateMyInformationPage
	 * @throws Exception - Exception
	 */
	public ProfilePage navigateToUpdateMyInformation()throws Exception{
		BrowserActions.clickOnElementX(lnkUpdateMyInfoDesktop, driver, "Update My Info ");
		Utils.waitForPageLoad(driver);
		return new ProfilePage(driver).get();
	}

	/**
	 * To navigate to category level-1
	 * @param level1 -
	 * @return PlpPage -
	 * @throws Exception -
	 */
	public PlpPage navigateTo(String level1) throws Exception {
		Log.event("Run Platform = " + runPltfrm);
		if (runPltfrm.equals("desktop")) {
			try{
				String navEle = "#navigation a[title='"+level1+"']";
				Log.event("Constructed Locator :: " + navEle);
				WebElement element = driver.findElement(By.cssSelector(navEle));
				BrowserActions.clickOnElementX(element, driver, "Category 1 ");
			}catch(NoSuchElementException e){
				Log.fail("Given Category Not Present in this Site. Please check with Business Manager.", driver);
			}

		} else {
			openCloseHamburgerMenu("open");
			try{
				WebElement element = BrowserActions.checkLocator(driver,
						"//div[@class='slider-menu']//ul[@class='menu-category level-1']//li/child::a[contains(text(), '" + level1 + "')]");
				BrowserActions.clickOnElementX(element, driver,
						"Level 1 Header Menu Toogle(+)");
			}catch(NoSuchElementException e){
				Log.fail("Given Category Not Present in this Site. Please check with Business Manager.");
			}
		}
		Log.event(" -----> Clicked '" + level1 + "'");
		Utils.waitForPageLoad(driver);
		return new PlpPage(driver).get();
	}

	/**
	 * To navigate to level2 category item Ex: Mens - Men's Clothing
	 * Parameterized level1's level2 category will be clicked
	 * @param level1 - L1 category item
	 * @param level2 - L2 category item
	 * @return PLP page
	 * @throws Exception - Exception
	 */
	public PlpPage navigateTo(String level1, String level2) throws Exception {
		Log.event("Given Level :: " + level1 + " >> " + level2);
		if (runPltfrm.equals("desktop")) {
			WebElement level1Parent = driver.findElement(By.xpath("//ul[contains(@class,'level-1')]//a[contains(@title,'"+ level1 +"')]"));
			try {
				BrowserActions.mouseHover(driver, level1Parent, "Level-1 category ");
				if (level2.equals("random")) {
					List<WebElement> level2Parent = level1Parent
							.findElement(By.xpath(".."))
							.findElements(
									By.cssSelector("div[class='level-2'] ul[class*='nav-column')] div a"));
					int rand = ThreadLocalRandom.current().nextInt(1,
							level2Parent.size());
					WebElement randomLevel2 = level2Parent.get(rand - 1);
					Utils.waitForElement(driver, randomLevel2);
					level2 = BrowserActions.getText(driver, randomLevel2,
							"L2 category name");
					BrowserActions.clickOnElementX(randomLevel2, driver,
							"Level 2 category");
					Log.event(" -----> Clicked random L2 category: '"
							+ level2 + "'");
				} else {
					WebElement level2Parent = driver.findElement(By.xpath("//ul[contains(@class,'level-1')]//a[contains(@title,'"+ level1 +"')][contains(@class,'has-sub-menu')]/../div[@class='level-2']//ul[contains(@class,'nav-column')]//li//a[contains(text(),'"+ level2 +"')]"));
					BrowserActions.clickOnElementX(level2Parent, driver, "sub category(L2): " + level2);
					Log.event("Clicked on Level2Parent...");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else{
			openCloseHamburgerMenu("open");
			Utils.waitForPageLoad(driver);
			WebElement level1Parent = driver.findElement(By.xpath("//ul[contains(@class,'level-1')]//a[contains(@title,'"+ level1 +"')]"));
			BrowserActions.clickOnElementX(level1Parent, driver, "Level 1 Header Menu Toogle(+)");
			if (level2.equals("random")) {
				List<WebElement> level2Parent = level1Parent.findElement(By.xpath("..")).findElements(By.cssSelector(".level-2 ul[class*='nav-column')] div a"));
				int rand = ThreadLocalRandom.current().nextInt(1,
						level2Parent.size());
				WebElement randomLevel2 = level2Parent.get(rand - 1);
				Utils.waitForElement(driver, randomLevel2);
				level2 = BrowserActions.getText(driver, randomLevel2,
						"L2 category name");
				BrowserActions.clickOnElementX(randomLevel2, driver,
						"Level 2 category");
				Log.event(" -----> Clicked random L2 category: '" + level2
						+ "'");
			} else if(level2.equalsIgnoreCase("View All")){
				WebElement level2Parent = driver.findElement(By.cssSelector("ul.level-1.active li.active li.nav-viewAll a"));
				BrowserActions.javascriptClick(level2Parent, driver, "Level-2 category ");
				Log.event("Clicked on View All");
			} else {
				WebElement level2Parent = driver.findElement(By.cssSelector(".level-1.active .active .level-2 ul.nav-column.col-1"));
				WebElement level2Parent1 = level2Parent.findElement(By.xpath("//a[contains(text(), '"+level2+"')]"));
				BrowserActions.javascriptClick(level2Parent1, driver, "Level-2 category ");
				Log.event("Clicked on Level2Parent...");
			}
		}
		Utils.waitForPageLoad(driver);
		return new PlpPage(driver).get();
	}

	/**
	 * To get Level 2 category
	 * @param level2 - 
	 * @return WebElement
	 * @throws Exception - 
	 */
	public WebElement getLevel2Category(String level2)throws Exception{
		List<WebElement> subCat = driver.findElements(By.cssSelector("ul.level-1 li.active .level-2 div.nonBoldCat a"));
		for(int i = 0; i < subCat.size(); i++){
			if(subCat.get(i).getText().trim().toLowerCase().equals(level2.toLowerCase()))
				return subCat.get(i);
		}
		return null;
	}

	/**
	 * To navigate to level3 category item Ex: Men - Men's Clothing - Jeans
	 * 
	 * Parameterized level1 and level2 will be navigated then level3 will be
	 * clicked
	 * 
	 * @param level1
	 *            - L1 category item
	 * @param level2
	 *            - L2 category item
	 * @param level3
	 *            - L3 category item
	 * @return PlpPage - Plp Page
	 * @throws Exception - Exception
	 */
	public PlpPage navigateTo(String level1, String level2, String level3)
			throws Exception {

		if (Utils.isDesktop()) {
			WebElement element = BrowserActions.checkLocator(driver, HEADER_NAV_LEVEL1 + "a[title='" + level1 + "']");
			BrowserActions.mouseHover(driver, element, "Level-1 category ");
			if (level3.equals("random")) {

				List<WebElement> level2Parent = BrowserActions.checkLocators(driver, HEADER_SUB_CATEGORY_l2_HAS_L3);
				int rand = ThreadLocalRandom.current().nextInt(1,
						level2Parent.size());
				WebElement level2Text = level2Parent.get(rand - 1).findElement(By.xpath("../a"));
				level2 = BrowserActions.getText(driver, level2Text, "L2 category text");
				Log.event(" Clicked random L2 category: '" + level2 + "'");
				List<WebElement> level3Category = level2Text.findElement(By.xpath("..")).findElements(By.cssSelector("ul[class='level-3']>li>a"));
				if (level3Category.size() > 0) {
					rand = ThreadLocalRandom.current().nextInt(1, level3Category.size());
					WebElement randomLevel3 = level3Category.get(rand - 1);
					level3 = BrowserActions.getText(driver, randomLevel3, "L3 category text");
					BrowserActions.clickOnElementX(randomLevel3, driver, "Level 3 category");

				} else {
					throw new Exception("There is no L3 category for L1 - " + level1);
				}
				Log.event(" Clicked random L3 category: '" + level3 + "'");
			} else {
				WebElement level3catagory = driver.findElement(By.cssSelector("#navigation .menu-category.level-1 li a[title='"+ level1 +"'] + .level-2 .has-sub-menu-parent-level3")).findElement(By.xpath("//a[contains(text(),'"+ level2 +"')]//ancestor::li[@class='has-sub-menu-parent-level3']//ul[contains(@class,'level-3')]//a[contains(text(),'"+ level3 +"')]"));
				BrowserActions.clickOnElementX(level3catagory, driver, "sub category(L3): " + level3);
			}
		} else {
			openCloseHamburgerMenu("open");
			WebElement element = driver.findElement(By.xpath("//ul[contains(@class,'level-1')]//a[contains(@title,'"+ level1 +"')]"));
			BrowserActions.clickOnElementX(element, driver, "Level 1 Header Menu Toogle(+)");
			WebElement level2Parent = driver.findElement(By.cssSelector(".level-1.active .active .level-2 ul.nav-column.col-1"));
			WebElement level2Parent1 = level2Parent.findElement(By.xpath("//a[contains(text(), '"+level2+"')]"));
			BrowserActions.javascriptClick(level2Parent1, driver, "Level-2 category ");
			Log.event("Clicked on Level2Parent...");
			WebElement level3catagory = level2Parent.findElement(By.xpath("//a[contains(text(), '"+level3+"')]"));
			BrowserActions.javascriptClick(level3catagory, driver, "sub category(L3): " + level3);
		}
		Utils.waitForPageLoad(driver);
		return new PlpPage(driver).get();
	}

	/**
	 * To navigate to level3 category item Ex: Men - Men's Clothing - Jeans
	 * 
	 * Parameterized level1 and level2 will be navigated then level3 will be
	 * clicked
	 * 
	 * @param level1
	 *            - L1 category item
	 * @param level2
	 *            - L2 category item
	 * @param level3
	 *            - L3 category item
	 * @param level4
	 *            - L4 category item
	 * @throws Exception - Exception
	 */
	public void navigateTo(String level1, String level2, String level3, String level4)
			throws Exception {

		if (runPltfrm == "desktop"
				|| runPltfrm == "tablet") {
			WebElement element = BrowserActions
					.checkLocator(driver, HEADER_NAV_LEVEL1
							+ "a[data-displaytext*='" + level1 + "']");
			BrowserActions.mouseHover(driver, element, "Level-1 category ");
			if (level3.equals("random")) {

				List<WebElement> level2Parent = BrowserActions.checkLocators(
						driver, HEADER_SUB_CATEGORY_l2_HAS_L3);
				int rand = ThreadLocalRandom.current().nextInt(1,
						level2Parent.size());
				WebElement level2Text = level2Parent.get(rand - 1).findElement(
						By.xpath("../a"));
				level2 = BrowserActions.getText(driver, level2Text,
						"L2 category text");
				Log.event(" Clicked random L2 category: '" + level2 + "'");
				List<WebElement> level3Category = level2Text.findElement(
						By.xpath("..")).findElements(
								By.cssSelector("ul[class='level-3']>li>a"));
				if (level3Category.size() > 0) {
					rand = ThreadLocalRandom.current().nextInt(1,
							level3Category.size());
					WebElement randomLevel3 = level3Category.get(rand - 1);
					level3 = BrowserActions.getText(driver, randomLevel3,
							"L3 category text");
					BrowserActions.clickOnElementX(randomLevel3, driver,
							"Level 3 category");

				} else {
					throw new Exception("There is no L3 category for L1 - "
							+ level1);
				}
				Log.event(" Clicked random L3 category: '" + level3 + "'");
			} else {
				WebElement level2Parent = BrowserActions
						.getMachingTextElementFromList(BrowserActions
								.checkLocators(driver, HEADER_SUB_CATEGORY_l2),
								level2, "equals");
				WebElement level3catagory = BrowserActions
						.getMachingTextElementFromList(
								level2Parent.findElements(By
										.xpath(HEADER_SUB_CATEGORY_l3)),
								level3, "equals");
				BrowserActions.clickOnElementX(level3catagory, driver,
						"sub category(L3): " + level3);
			}
		} else if ((runPltfrm == "mobile")) {
			openCloseHamburgerMenu("open");
			WebElement element = BrowserActions.checkLocator(driver,
					"//a[@data-displaytext='" + level1
					+ "']/following-sibling::i");
			BrowserActions.javascriptClick(element, driver,
					"Level 1 Header Menu Toogle(+)");
			if (level3.equals("random")) {
				List<WebElement> level2Parent = element
						.findElement(By.xpath(".."))
						.findElements(
								By.cssSelector("div[class='level-2'] .hide-desktop ul>li>i"));
				int rand = ThreadLocalRandom.current().nextInt(1,
						level2Parent.size());

				WebElement level2Text = level2Parent.get(rand - 1);
				level2 = BrowserActions.getText(driver, level2Text,
						"L2 category text");
				BrowserActions.javascriptClick(level2Text, driver,
						"Level 2 category");

				List<WebElement> level3Category = level2Text.findElement(
						By.xpath("..")).findElements(
								By.cssSelector("ul[class='level-3']>li>a"));
				if (level3Category.size() > 0) {
					rand = ThreadLocalRandom.current().nextInt(1,
							level3Category.size());
					WebElement randomLevel3 = level3Category.get(rand - 1);
					level3 = BrowserActions.getText(driver, randomLevel3,
							"L3 category text");
					BrowserActions.javascriptClick(randomLevel3, driver,
							"Level 3 category");
				} else {
					throw new Exception("There is no L3 category for L1 - "
							+ level1);
				}
				Log.event(" Clicked random L3 category: '" + level3 + "'");
			} else {
				WebElement level2Parent = BrowserActions
						.getMachingTextElementFromList(
								BrowserActions
								.checkLocators(driver,
										"li[class='active'] div[class='level-2'] .hide-desktop>ul>li"),
								level2, "equals");
				WebElement level2ParentExpand = level2Parent.findElement(By
						.cssSelector("i"));
				BrowserActions.javascriptClick(level2ParentExpand, driver,
						"Level 1 Header Menu");
				WebElement level3catagory = BrowserActions
						.getMachingTextElementFromList(
								level2Parent.findElements(By
										.xpath(HEADER_SUB_CATEGORY_l3)),
								level3, "equals");
				BrowserActions.javascriptClick(level3catagory, driver,
						"sub category(L3): " + level3);
			}
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Forget Password Link in SignIn Flyout
	 * @param email - user email id
	 * @return - Headers(if email is given), Password Reset Page(if no email is given)
	 * @throws Exception - Exception
	 */
	public Object clickOnForgetPassword(String... email)throws Exception{
		mouseOverSignIn();
		if(email.length > 0){
			BrowserActions.typeOnTextField(fldUserNameDesktop, email[0], driver, "Email Address in SignIn Flyout ");
			BrowserActions.javascriptClick(lnkForgetPasswordDesktop, driver, "Forget password Link in Desktop");
			return new Headers(driver).get();
		}else{
			BrowserActions.javascriptClick(lnkForgetPasswordDesktop, driver, "Forget password Link in Desktop");
			Utils.waitForPageLoad(driver);
			return new PasswordResetPage(driver).get();
		}

	}

	/**
	 * To get the error message shown in Password Reset
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getPasswordResetError()throws Exception{
		return BrowserActions.getText(driver, lblEmailErrorInResetPassword, "Email Error in Password Reset ");
	}

	/**
	 * To type user's email id in email textbox in Password Reset model
	 * @param email - user's email id
	 * @throws Exception - Exception
	 */
	public void typeOnEmailInResetPasswordModel(String email)throws Exception{
		BrowserActions.typeOnTextField(txtEmailInResetPasswordMdl, email, driver, "Email Field in Reset Password Model ");
	}

	/**
	 * To verify the text mode of Password field
	 * @return 'text' - if text is visible. 'password' - if text is being protected
	 * @throws Exception - Exception
	 */
	public String verifyPasswordFieldTextMode()throws Exception{
		return fldPasswordDesktop.getAttribute("type");
	}

	/**
	 * To verify the password field and show link's parent are same
	 * @return - true(if both have same parent), false(if both doesn't have same parent)
	 * @throws Exception - Exception
	 */
	public boolean verifyPresenceOfShowInPassword()throws Exception{
		String class1 = fldPasswordDesktop.findElement(By.xpath("..")).getAttribute("class");
		String class2 = lnkShowHidePassword.findElement(By.xpath("..")).getAttribute("class");
		if(class1.equals(class2))
			return true;
		else
			return false;
	}

	/**
	 * To click on Show/Hide in Password field
	 * @return State of Password field - show(Not protected), hide(Protected)
	 * @throws Exception - Exception
	 */
	public String clickOnShowHideInPassword()throws Exception{
		BrowserActions.javascriptClick(lnkShowHidePassword, driver, "Show/Hide Link ");
		return fldPasswordDesktop.getAttribute("type").equals("password")? "hide" : "show";
	}

	/**
	 * To get the other brands Loin message in SignIn Flyout
	 * @return string 
	 * @throws Exception - Exception
	 */
	public String getLoginMessage()throws Exception{
		String loginMessage = BrowserActions.getText(driver, lblOtherBrandLoginMsgDesktop, "Login Message ") + " " +
				BrowserActions.getText(driver, lblOtherBrandLoginBrandsDesktop, "Login Brand ");

		return loginMessage;
	}

	/**
	 * To click on Facebook button in SignIn Flyout
	 * @throws Exception - Exception
	 */
	public void clickonFaceBookButton()throws Exception{
		BrowserActions.clickOnElementX(btnFaceBookInSignInFlyoutDesktop, driver, "Facebook Button ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on back arrow in mobile breadcrumb
	 * @throws Exception - Exception
	 */

	public void clickBackArrowOnBreadCrumbMobile()throws Exception{
		BrowserActions.clickOnElementX(lnkArrowBreadCrumb_mobile, driver, "Bread crumb arrow ");
	}

	/**
	 * To get bread crumb text
	 * @return the bread crumb text
	 * @throws Exception - Exception
	 */
	public String getBreadCrumbText()throws Exception{
		if(runPltfrm.equals("mobile")) {
			return BrowserActions.getText(driver, txtBreadCrumb_mobile, "Mobile BreadCrumb");
		} else {
			return BrowserActions.getText(driver, breadCrumb, "BreadCrumb");
		}
	}

	/**
	 * To navigate to Account Creation page by clicking on Create account in SignIn Flyout
	 * @return CreateAccountPage
	 * @throws Exception - Exception
	 */
	public CreateAccountPage navigateToCreateAccount()throws Exception{
		BrowserActions.mouseHover(driver, lnkSignInDesktop, "Sign In Link ");
		Utils.waitForElement(driver, lnkCreateNewAccount);
		BrowserActions.clickOnElementX(lnkCreateNewAccount, driver, "Create an Account Button ");
		Utils.waitForPageLoad(driver);
		return new CreateAccountPage(driver).get();
	}
	/**
	 * To click on Create account in SignIn Flyout
	 * @throws Exception - Exception
	 */
	public void clickOnCreateAccount()throws Exception{
		BrowserActions.javascriptClick(lnkCreateNewAccount, driver, "Create a New Account Link ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on SignOut link in MyAccount Flyout to signout
	 * @return SignIn page
	 * @throws Exception - Exception
	 */
	public SignIn clickOnSignOut()throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		BrowserActions.clickOnElementX(lnkSignOutMyAccFlytDesktop, driver, "SignOut Link in My Acc Flyout ");
		Utils.waitForPageLoad(driver);
		return new SignIn(driver).get();
	}

	/**
	 * To click on WishList link
	 * @return WishListPage - Wishlist page object
	 * @throws Exception - Exception
	 */
	public WishListPage clickOnWishList()throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		if(!(Utils.isMobile())) {
			mouseOverMyAccount();
			BrowserActions.clickOnElementX(lnkWishListMyAccFlytDesktop, driver, "WishList Link in My Acc Flyout ");
		} else {
			hamburgerMenu.openCloseHamburgerMenu("open");
			BrowserActions.clickOnElementX(lnkMyAccMobile, driver, "My account ");
			BrowserActions.clickOnElementX(lnkWishListMobile, driver, "WishList Link");
		}
		Utils.waitForPageLoad(driver);
		return new WishListPage(driver).get();
	}

	/**
	 * To get signout url
	 * @return string - url
	 * @throws Exception - Exception
	 */

	public String getSignOutUrl()throws Exception{
		String url = BrowserActions.getTextFromAttribute(driver, lnkSignOutMyAccFlytDesktop, "href", "sign out");
		Utils.waitForPageLoad(driver);
		return url;
	}



	/**
	 * To clear the text in user name and password field in SignIn Flyout
	 * @throws Exception - Exception
	 */
	public void clearUserNamePassword()throws Exception{
		fldUserNameDesktop.clear();
		fldPasswordDesktop.clear();
	}

	/**
	 * To Navigate to other Brand's Home page
	 * @return Brand's Homepage
	 * @throws Exception - Exception
	 */
	public HomePage navigateToHome()throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		BrowserActions.clickOnElementX(brandLogo, driver, "Brand Logo ");
		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();
	}

	/**
	 * To click on the Search Icon in the mobile viewport
	 * @throws Exception - Exception
	 */
	public void clickOnSearchIconMobile()throws Exception{
		BrowserActions.clickOnElementX(btnSearchIconMobile, driver, "Search Icon ");
	}

	/**
	 * To click on the Search Icon in the Desktop viewport
	 * @throws Exception - Exception
	 */
	public void clickOnSearchIconDesktop()throws Exception{
		BrowserActions.javascriptClick(btnSearchIcon, driver, "Search Icon ");
	}

	/**
	 * To click on the Mini cart
	 * @throws Exception - Exception
	 */
	public void clickAndHoldOnBag()throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		if(Utils.waitForElement(driver, iconMyBag)) {
			BrowserActions.clickAndHoldOnElement(iconMyBag, driver, "My Bag");
		}else {
			BrowserActions.clickAndHoldOnElement(iconMyBagQty, driver, "My Bag");
		}

		if(!driver.findElement(By.cssSelector(".mini-cart-product")).isDisplayed()) {
			Log.event("Mini Cart hover menu did not open up. Using workaround...");
			forceOpenMiniCartHover(driver);
		}
	}

	/**
	 * To click on the Mini cart
	 * @throws Exception - Exception
	 */
	public void tabMiniCartIcon()throws Exception{
		BrowserActions.clickAndHoldOnElement(iconMyBag, driver, "My Bag");
	}

	/**
	 * To click on the Mini cart to Navigate to Shopping bag page
	 * @return ShoppingBagPage - ShoppingBag Page object 
	 * @throws Exception - Exception
	 */
	public ShoppingBagPage clickOnBagLink()throws Exception{
		if(Utils.isMobile())
			if(Utils.waitForElement(driver, iconMyBagQty)) {
				BrowserActions.javascriptClick(iconMyBagQty, driver, "Bag Icon");
			}else {
				BrowserActions.javascriptClick(iconMyBag, driver, "Bag Icon");
			}
		else if(Utils.isDesktop()) {
			BrowserActions.javascriptClick(lnkMiniCart, driver, "View Bag Link");
		} else {
			BrowserActions.clickOnElementX(lnkMiniCartTablet, driver, "View Bag Link");
			Utils.waitForPageLoad(driver);
			BrowserActions.clickOnElementX(lnkMiniCartTablet, driver, "View Bag Link");
		}
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	/**
	 * To click on the Mini cart a tag to Navigate to Shopping bag page
	 * @return ShoppingBagPage
	 * @throws Exception - Exception
	 */
	public ShoppingBagPage navigateToShoppingBagPage()throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		if(Utils.isMobile())
			if(Utils.waitForElement(driver, iconMyBag)) {
				BrowserActions.clickOnElementX(iconMyBag, driver, "Bag Icon");
			}else {
				BrowserActions.javascriptClick(iconMyBagQty, driver, "Bag Icon");
			}
		else
			BrowserActions.clickOnElementX(lnkMiniCart, driver, "View Bag Link ");

		Utils.waitForPageLoad(driver);
		if(!Utils.waitForElement(driver, readyElementShoppingBagPage)){
			Log.reference("Failed to Navigate Shopping Bag via Element Click. Using work around...");
			driver.get(Utils.getWebSite() + "/cart");
			Utils.waitForPageLoad(driver);
		}
		return new ShoppingBagPage(driver).get();
	}

	/**
	 * To get the Login Error message for unknown user name and password
	 * @return String - Login Error message
	 * @throws Exception - Exception
	 */
	public String getLoginErrorInFlyt()throws Exception{
		return lblServerSideLoginError.getText();
	}

	/**
	 * To click outside of the page
	 * @param elem - Locator of an object to take coordinates
	 * @param obj - obj where locator is defined
	 * @throws Exception - Exception
	 */

	public void clickOutSide(String elem, Object obj)throws Exception{
		Field f = obj.getClass().getDeclaredField(elem);
		f.setAccessible(true);
		WebElement element = ((WebElement) f.get(obj));

		Point coordinates = element.getLocation();
		Robot robot = new Robot();
		robot.mouseMove(coordinates.getX(), coordinates.getY());
		Actions action = new Actions(driver);
		action.doubleClick();
		action.click().build().perform();
		action.moveByOffset(100, 100).build().perform();		
	}

	/**
	 * To click outside of the page
	 * @throws Exception - Exception
	 */


	public void clickOutSide()throws Exception{
		if(Utils.getRunBrowser(driver).equals("safari") || Utils.getRunBrowser(driver).equals("internet explorer"))
		{
			Log.fail("***Feature Not working in " + Utils.getRunBrowser(driver) + "***");
		}
		Actions action = new Actions(driver);
		action.moveByOffset(1, 1).click().build().perform();
		action.click().build().perform();
	}

	/**
	 * To navigate To PDP Page of Given product ID
	 * @param productID - 
	 * @return PDP Page
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPDP(String productID)throws Exception{
		final long startTime = StopWatch.startTime();

		if(Utils.isMobile())
		{
			// BrowserActions.scrollToTopOfPage(driver);
			BrowserActions.clickOnElementX(btnSearchIconMobile, driver, "Search icon");
			BrowserActions.typeOnTextField(txtSearch, productID, driver,"Entering category in the search field");
			txtSearch.submit();
			Log.event("Enter key sent to Search Text box!",StopWatch.elapsedTime(startTime));
		}
		else
		{
			try {
				BrowserActions.clickOnElementX(txtSearch, driver, "Text field");
				BrowserActions.typeOnTextField(txtSearch, productID, driver,"Entering category in the search field");
				BrowserActions.javascriptClick(btnSearchIcon, driver, "Search icon");
			}catch(InvalidElementStateException e) {
					redirectToPDP(productID, driver);
			}
		}
		Utils.waitForPageLoad(driver);
		Log.event("Searched the provided product!",StopWatch.elapsedTime(startTime));

		if(Utils.waitForElement(driver, readyElementSLPPage)){
			Log.event("Navigated to Search Result Page.");
			SearchResultPage slp = new SearchResultPage(driver);
			if(slp.isProductPresentInPage(productID)) {
				return slp.navigateToPdpByPrdID(productID);
			}
			else {
				String prdURL = Utils.getWebSite() + "/products/" + productID + ".html";
				driver.get(prdURL);
				PdpPage pdpPage = new PdpPage(driver).get();
				if(driver.getCurrentUrl().contains("/404")) {
					Log.fail("Given Product not found...", driver);
				}
				else {
					return pdpPage;
				}
			}
		}
		return new PdpPage(driver).get();
	}

	/**
	 * To navigate to pdp with direct hit
	 * @param prdID - 
	 * @param driver -
	 * @return PdpPage -
	 * @throws Exception -
	 */
	public PdpPage redirectToPDP(String prdID, WebDriver driver)throws Exception{
		String url = Utils.getWebSite() + "/products/" + prdID + ".html";
		driver.get(url);
		return new PdpPage(driver).get();
	}

	/**
	 * To navigate to variation sku
	 * @param variation -
	 * @param driver - 
	 * @return PdpPage -
	 * @throws Exception -
	 */
	public PdpPage redirectToVariationPDP(String variation, WebDriver driver)throws Exception{
		String url = Utils.getWebSite();
		String[] variation_value = variation.split("\\_");
		String variation_string = EnvironmentPropertiesReader.getInstance("redirect").getProperty("variation_pdp");
		variation_string = variation_string.replace("!pid!", variation_value[0]).replace("!colorcode!", variation_value[1]).replace("!sizecode!", variation_value[2]);
		url += variation_string;
		driver.get(url);
		return new PdpPage(driver).get();
	}

	/**
	 * To Sign In to My Account Page
	 * @param userName - 
	 * @param password - 
	 * @return MyAccountPage
	 * @throws Exception - Exception
	 */
	public MyAccountPage signInToMyAccountPage(String userName, String password)throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		if(runPltfrm.equals("mobile")){
			openCloseHamburgerMenu("open");
			BrowserActions.clickOnElementX(lnkSignInMobile, driver, "Mobile SignIn Link ");
			SignIn signInPage = new SignIn(driver).get();
			signInPage.enterUserName(userName);
			signInPage.enterPassword(password);
			signInPage.clickSignInButton();
		}
		else if(runPltfrm.equals("desktop")){
			if(Utils.getRunBrowser(driver).equals("safari")||Utils.getRunBrowser(driver).equals("internet explorer")){
				BrowserActions.clickOnElementX(lnkSignInDesktop, driver, "Mobile SignIn Link ");
				SignIn signin=new SignIn(driver).get();
				signin.typeOnEmail(userName);
				signin.typeOnPassword(password);
				signin.clickSignInButton();
			}
			else{
				mouseOverSignIn();
				typeUserNameInFlyout(userName);
				typePasswordInFlyout(password);
				if(Utils.waitForElement(driver, btnSignInDesktop1)) {
					BrowserActions.clickOnElementX(btnSignInDesktop1, driver, "Sign In Button");
				} else {
					BrowserActions.clickOnElementX(btnSignInDesktop, driver, "Sign In Button");
				}
				if(Utils.waitForElement(driver, divLoginBoxDesktop)) 
					Log.fail("Invalid User credentials. Please check the spelling.", driver);

				try{
					return new MyAccountPage(driver).get();
				}catch(ClassCastException e){
					Log.fail("Cannot navigate to My Account Page. Please verify the screenshot/sauce video for more information", driver);
				}

			}

		}
		else if(runPltfrm.equals("tablet")) {			
			driver.navigate().to(Utils.getWebSite()+"/register");
			SignIn signInPage = new SignIn(driver).get();
			signInPage.enterUserName(userName);
			signInPage.enterPassword(password);
			signInPage.clickSignInButton();
		}

		return new MyAccountPage(driver).get();

	}

	/**
	 * TO verify and Sign IN to myaccount
	 * @param userName - 
	 * @param password - 
	 * @return my account page
	 * @throws Exception - Exception
	 */
	public MyAccountPage verifyandSingInAccount(String userName, String password)throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		if(runPltfrm.equals("mobile")|(runPltfrm.equals("tablet"))){
			openCloseHamburgerMenu("open");
			BrowserActions.clickOnElementX(lnkSignInMobile, driver, "Mobile SignIn Link ");
			Utils.waitForPageLoad(driver);
			SignIn signin = new SignIn(driver).get();
			signin.enterUserName(userName);
			signin.enterPassword(password);
			signin.clickSignInButton();
			Utils.waitForPageLoad(driver);

			if(Utils.waitForElement(driver, divLoginBoxDesktop)){
				signin=new SignIn(driver).get();
				signin.clickOnCreateAccount();
				signin.enterTextOnField("txtFirstName","automation","First Name",signin);
				signin.enterTextOnField("txtLastName","Drops","secondName Name",signin);
				signin.enterTextOnField("txtEmail",userName,"Email Id",signin);
				signin.enterTextOnField("txtConfirmEmailId",userName,"Confirm Email",signin);
				signin.enterTextOnField("txtpasswordField",password,"Password",signin);
				signin.enterTextOnField("txtConfirmPasswordField",password,"Confirm Password",signin);
				signin.clickOnRegisterBtn();
				Utils.waitForPageLoad(driver);
			}

		}
		else if(runPltfrm.equals("desktop")){
			SignIn signin=null;
			if(Utils.getRunBrowser(driver).equals("safari")||Utils.getRunBrowser(driver).equals("internet explorer")){
				BrowserActions.clickOnElementX(lnkSignInDesktop, driver, "Mobile SignIn Link ");
				Utils.waitForPageLoad(driver);
				signin=new SignIn(driver).get();
				signin.typeOnEmail(userName);
				signin.typeOnPassword(password);
				signin.clickSignInButton();
				Utils.waitForPageLoad(driver);
				if(Utils.waitForElement(driver, divLoginBoxDesktop)){
					signin=new SignIn(driver).get();
					signin.clickOnCreateAccount();
					signin.enterTextOnField("txtFirstName","automation","First Name",signin);
					signin.enterTextOnField("txtLastName","Drops","secondName Name",signin);
					signin.enterTextOnField("txtEmail",userName,"Email Id",signin);
					signin.enterTextOnField("txtConfirmEmailId",userName,"Confirm Email",signin);
					signin.enterTextOnField("txtpasswordField",password,"Password",signin);
					signin.enterTextOnField("txtConfirmPasswordField",password,"Confirm Password",signin);
					signin.clickOnRegisterBtn();
					Utils.waitForPageLoad(driver);
				}

			}
			else{
				mouseOverSignIn();
				typeUserNameInFlyout(userName);
				typePasswordInFlyout(password);
				BrowserActions.clickOnElementX(btnSignInDesktop, driver, "Sign In Button");
				Utils.waitForPageLoad(driver);
				if(Utils.waitForElement(driver, divLoginBoxDesktop)){
					signin=new SignIn(driver).get();
					signin.clickOnCreateAccount();
					signin.enterTextOnField("txtFirstName","automation","First Name",signin);
					signin.enterTextOnField("txtLastName","Drops","secondName Name",signin);
					signin.enterTextOnField("txtEmail",userName,"Email Id",signin);
					signin.enterTextOnField("txtConfirmEmailId",userName,"Confirm Email",signin);
					signin.enterTextOnField("txtpasswordField",password,"Password",signin);
					signin.enterTextOnField("txtConfirmPasswordField",password,"Confirm Password",signin);
					signin.clickOnRegisterBtn();
					Utils.waitForPageLoad(driver);

				}
				try{
					return new MyAccountPage(driver).get();
				}catch(ClassCastException e){
					Log.fail("Cannot navigate to My Account Page. Please verify the screenshot/sauce video for more information", driver);
				}

			}

		}
		else if(runPltfrm.equals("tablet")) {
			BrowserActions.clickOnElementX(lnkSignInDesktop, driver, "Desktop SignIn Link ");
			Utils.waitForPageLoad(driver);
			SignIn signin = new SignIn(driver).get();
			signin.enterUserName(userName);
			signin.enterPassword(password);
			signin.clickSignInButton();
			Utils.waitForPageLoad(driver);

			if(Utils.waitForElement(driver, divLoginBoxDesktop)){
				signin=new SignIn(driver).get();
				signin.clickOnCreateAccount();
				signin.enterTextOnField("txtFirstName","automation","First Name",signin);
				signin.enterTextOnField("txtLastName","Drops","secondName Name",signin);
				signin.enterTextOnField("txtEmail",userName,"Email Id",signin);
				signin.enterTextOnField("txtConfirmEmailId",userName,"Confirm Email",signin);
				signin.enterTextOnField("txtpasswordField",password,"Password",signin);
				signin.enterTextOnField("txtConfirmPasswordField",password,"Confirm Password",signin);
				signin.clickOnRegisterBtn();
				Utils.waitForPageLoad(driver);
			}

			if(Utils.waitForElement(driver, divLoginBoxDesktop)){
				signin.clickOnCreateAccount();
				signin.enterTextOnField("txtFirstName","automation","First Name",signin);
				signin.enterTextOnField("txtLastName","Drops","secondName Name",signin);
				signin.enterTextOnField("txtEmail",userName,"Email Id",signin);
				signin.enterTextOnField("txtConfirmEmailId",userName,"Confirm Email",signin);
				signin.enterTextOnField("txtpasswordField",password,"Password",signin);
				signin.enterTextOnField("txtConfirmPasswordField",password,"Confirm Password",signin);
				signin.clickOnRegisterBtn();
				Utils.waitForPageLoad(driver);
			}

		}

		return new MyAccountPage(driver).get();

	}

	/**
	 * TO get mini cart count
	 * @return string - mini cart count
	 * @throws Exception - Exception
	 */
	public String getMiniCartCount()throws Exception{
		try{
			if(Utils.isMobile())
				return lblMiniCartCountMobile.getText().trim();
			else
				return lblMiniCartCountDesktop.getText().trim();
		}catch(NoSuchElementException e){
			return "0";
		}
	}

	/**
	 * To navigate to Quick Order Landing Page
	 * @return QuickOrderPage - page object
	 * @throws Exception - Exception
	 */
	public QuickOrderPage navigateToQuickOrder()throws Exception{
		if(Utils.isMobile()){
			openCloseHamburgerMenu("open");
			BrowserActions.clickOnElementX(lnkUserAccMobile, driver, "Mobile My Account Link");
			BrowserActions.clickOnElementX(iconQuickOrderMobile, driver, "Mobile SignIn Link ");
			Utils.waitForPageLoad(driver);
		} else {
			mouseOverMyAccount();
			BrowserActions.clickOnElementX(lnkQuickOrder, driver, "Quick Order Icon");
			Utils.waitForPageLoad(driver);
		}
		return new QuickOrderPage(driver).get();
	}

	/**
	 * To navigate to checkout page
	 * @return CheckoutPage - page object
	 * @throws Exception - Exception
	 */
	public CheckoutPage navigateToCheckOut()throws Exception{
		mouseOverMyAccount();
		BrowserActions.clickOnElementX(lnkCheckOut, driver, "Serach icon");
		Utils.waitForPageLoad(driver);

		ShoppingBagPage cartPage = new ShoppingBagPage(driver).get();
		cartPage.navigateToCheckout();
		Utils.waitForPageLoad(driver);

		return new CheckoutPage(driver).get();
	}

	/**
	 * To navigate to order status page
	 * @return OrderStatuPage - page object
	 * @throws Exception - Exception
	 */
	public OrderStatusPage navigateToOrderStatus()throws Exception{
		mouseOverMyAccount();
		BrowserActions.clickOnElementX(lnkOrderStatus, driver, "Order History Icon");
		Utils.waitForPageLoad(driver);
		return new OrderStatusPage(driver).get();
	}

	/**
	 * TO Close store front tool kit
	 * @throws Exception - Exception
	 */

	public void closeStoreFrontToolKit()throws Exception{
		BrowserActions.clickOnElementX(btnCloseStorefrontKit, driver, "Close storefront Kit");
	}

	/**
	 * TO Close live chat modal
	 * @throws Exception - Exception
	 */
	public void closeLiveChat()throws Exception{
		if(Utils.waitForElement(driver, mdlLiveChat)){
			WebElement btnChatMenu = driver.findElement(By.cssSelector(".bc-headbtn-menulist"));
			Log.event("Clicking on Menu button");
			BrowserActions.clickOnElementX(btnChatMenu, driver, "Chat Menu Icon");
			WebElement btnChatClose = driver.findElement(By.cssSelector(".bc-headbtn-close-icon"));
			Log.event("Clicking on Close button");
			BrowserActions.clickOnElementX(btnChatClose, driver, "Chat Close Icon");
			Utils.waitUntilElementDisappear(driver, mdlLiveChat);
		}else{
			Log.event("Chat Not Present");
		}
	}

	/**
	 * To get love status modal status as 
	 * @return true - opened, false - closed
	 * @throws Exception - 
	 */
	public boolean getLiveChatStatus()throws Exception{
		try{
			if(runPltfrm.equals("mobile")){
				HamburgerMenu hMenu = (HamburgerMenu)openCloseHamburgerMenu("open");
				if(Utils.waitForElement(driver, hMenu.lnkChat))
					return true;
				else
					return false;
			}else{
				if(imgLiveChatDesktop.getAttribute("border").equals("0"))
					return false;
				else
					return true;
			}
		}catch(NoSuchElementException e){
			return false;
		}
	}

	/**
	 * To navigate to CustomerService page
	 * @return CustomerService page
	 * @throws Exception - Exception
	 */
	public CustomerService navigateToCustomerService()throws Exception{
		if(!Utils.isMobile()) {
			BrowserActions.clickOnElementX(lnkCustomerServiceDesktop, driver, "Customer Serice Link in Header");
			BrowserActions.clickOnElementX(lnkCustomerServiceDesktop, driver, "Customer Serice Link in Header");
			Utils.waitForPageLoad(driver);
			return new CustomerService(driver).get();
		}else {
			HamburgerMenu hbMenu =  (HamburgerMenu) openCloseHamburgerMenu("open");
			return hbMenu.navigateToCustomerServiceHamburger();
		}
	}

	/**
	 * To navigate TO explore page
	 * @param String - level1 category name 
	 * @return Explore - explore pageobject
	 * @throws Exception - Exception
	 */
	public Explore navigateToExplore() throws Exception {
		try {
			String exploreURL = redirectData.get("explore_url_"+Utils.getCurrentBrandShort());
			Log.event("Run Platform = " + runPltfrm);
			if (Utils.isDesktop() || Utils.isTablet()) {
				if(BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(lnkExplore))){
					BrowserActions.clickOnElementX(lnkExplore, driver, "Category 1 ");
				} else
				{
					Log.event("Navigation element not available, using workaround to navigate to Explore page.");
					driver.navigate().to(driver.getCurrentUrl()+exploreURL);
				}
			} else {
				openCloseHamburgerMenu("open");
				if(BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(lnkExploreMobile))){
					BrowserActions.clickOnElementX(lnkExploreMobile, driver,
							"Level 1 Header Menu Toogle(+)");
				} else
				{
					Log.event("Navigation element not available, using workaround to navigate to Explore page.");
					driver.navigate().to(driver.getCurrentUrl()+exploreURL);
				}
			}
			Log.event(" -----> Clicked 'Explore'");
			Utils.waitForPageLoad(driver);
		}catch(Exception e) {
			Log.fail("Given Category navigation( Explore ) Not available.", driver);
		}
		return new Explore(driver).get();
	}


	/**
	 * To navigate to Sign In Page
	 * @return SignIn - SignIn Pageobject
	 * @throws Exception - Exception
	 */
	public SignIn navigateToSignInpage()throws Exception{
		if(Utils.isDesktop()){
			BrowserActions.clickOnElementX(lnkSignInDesktop, driver, "My Account link in header");
		}
		if(Utils.isTablet()){
			BrowserActions.actionClick(lnkSignInDesktop, driver, "My Account link in header");
			BrowserActions.actionClick(lnkSignInDesktop, driver, "My Account link in header");
		}
		if(Utils.isMobile()){
			openCloseHamburgerMenu("open");
			clickOnSignInMobile();
		}

		Utils.waitForPageLoad(driver);
		return new SignIn(driver).get();
	}

	/**
	 * To click on Sign out in mobile
	 * @return SignIn page
	 * @throws Exception - Exception
	 */


	public SignIn clickOnSignOutMobile()throws Exception{
		BrowserActions.clickOnElementX(lnkHamburger, driver, "Open hamburger menu");
		BrowserActions.clickOnElementX(lblMyAccMobile, driver, "SignOut Link in My Acc Flyout ");
		BrowserActions.clickOnElementX(lnkSignOutMobile, driver, "SignOut Link ");	

		return new SignIn(driver).get();
	}

	/**
	 * To navigate to Gift card Page
	 * @return GiftCardPage - GiftCardPage page object
	 * @throws Exception - Exception
	 */
	public GiftCardPage navigateToGiftCardPage()throws Exception{
		BrowserActions.clickOnElementX(lnkGiftCardPage, driver, "Gift Card");
		Utils.waitForPageLoad(driver);
		return new GiftCardPage(driver).get();
	}

	public GiftCardPage navigateToGiftCardLandingPage()throws Exception{
		Log.event("Run Platform = " + runPltfrm);
		if (runPltfrm.equals("desktop")) {
			try{
				String navEle = "#navigation a[title='Gift Card']";
				Log.event("Constructed Locator :: " + navEle);
				WebElement element = driver.findElement(By.cssSelector(navEle));
				BrowserActions.clickOnElementX(element, driver, "Category 1 ");
			}catch(NoSuchElementException e){
				Log.fail("Given Category Not Present in this Site. Please check with Business Manager.", driver);
			}

		} else {
			openCloseHamburgerMenu("open");
			try{
				WebElement element = BrowserActions.checkLocator(driver,
						"//div[@class='slider-menu']//ul[@class='menu-category level-1']//li/child::a[contains(text(), 'Gift Card')]");
				BrowserActions.clickOnElementX(element, driver,
						"Level 1 Header Menu Toogle(+)");
			}catch(NoSuchElementException e){
				Log.fail("Given Category Not Present in this Site. Please check with Business Manager.");
			}
		}
		Log.event(" -----> Clicked 'Gift Card'");
		Utils.waitForPageLoad(driver);
		return new GiftCardPage(driver).get();
	}
	/**
	 * To navigate to PLP from Last category in Breadcrumb
	 * @return PlpPage - PlpPage page object
	 * @throws Exception - Exception
	 */
	public PlpPage navigateToLastSubCategory()throws Exception{
		if(Utils.isMobile()) {
			if(lnkSubCategoryBC_Mobile.getText().equalsIgnoreCase("Home")) {
				Log.fail("Product does not have proper category configuration. Least Sub category will navigate to Home.", driver);
			}
			BrowserActions.clickOnElementX(lnkSubCategoryBC_Mobile, driver, "Last Category in BC");
		} else {
			if(lnkSubCategoryBC.getText().equalsIgnoreCase("Home")) {
				Log.fail("Product does not have proper category configuration. Least Sub category will navigate to Home.", driver);
			}
			BrowserActions.clickOnElementX(lnkSubCategoryBC, driver, "Last Category in BC");
		}
		return new PlpPage(driver).get();
	}

	/**
	 * To navigate to PLP page for given product id.
	 * If searching the product navigates to SLP, then we will go into PDP of the product and then we will click the least breadcrumb link
	 * and navigated to PLP.
	 * 
	 * If searching the product navigates to PDP, then we will click the least breadcrumb link and navigate to PDP.
	 * @param String - product ID to open
	 * @return PlpPage - PlpPage page object
	 * @throws Exception - Exception
	 */
	public PlpPage navigateToPLP(String prdID)throws Exception{
		typeTextInSearchField(prdID);
		clickOnSearchIconDesktop();
		WebElement wrapperElement = driver.findElement(By.id("wrapper"));
		if(wrapperElement.getAttribute("class").contains("product-search-page")){
			new SearchResultPage(driver).get().navigateToPdpByPrdID(prdID);
		}else if(wrapperElement.getAttribute("class").contains("pt_error")){
			Log.fail("Product configuration failure occured. cannot load PDP Page.", driver);
		}

		return new PdpPage(driver).get().headers.navigateToLastSubCategory();
	}

	public SignIn signOut()throws Exception{
		if(Utils.isMobile()) {
			HamburgerMenu hMenu = (HamburgerMenu)openCloseHamburgerMenu("open");
			hMenu.ClickOnMyAccount();
			hMenu.clickOnSignOut();
		}else {
			clickOnSignOut();
		}
		return new SignIn(driver).get();
	}

	/**
	 * To click n-th breadcrumb
	 * @int breadcrumbIndex - index of breadcrumb to be clicked
	 * @throws Exception - Exception
	 */
	public void clickNthBreadcrumb(int breadcrumbIndex)throws Exception{
		BrowserActions.clickOnElementX(breadcrumbElements.get(breadcrumbIndex), driver, "n-th Breadcrumb");
	}
	
	/**
	 * To get the last Element of the breadcumb
	 * @return String - Breadcrumb text of last element
	 * @throws Exception - Exception
	 */
	public String getLastBreadcrumb() throws Exception{
		if(breadcrumbElements.size() == 1)
			return breadcrumbElements.get(0).getText();
		else
			return breadcrumbElements.get(breadcrumbElements.size()-2).getText();
	}

	/**
	 * To navigate to Wishlist Page
	 * @return WishListPage - WishListPage page object
	 * @throws Exception - Exception
	 */
	public WishListPage navigateToWishListPage()throws Exception{
		mouseOverMyAccount();
		BrowserActions.clickOnElementX(lnkWishListMyAccFlytDesktop, driver, "WishList Link");
		return new WishListPage(driver).get();
	}

	/**
	 * To forcefully open sign in hover menu in Tablet
	 * @throws Exception - Exception
	 * 
	 */
	public void forceOpenSignInHover(WebDriver driver) throws Exception{
		String classAttribute = lnkSignInDesktop.getAttribute("class");
		classAttribute = classAttribute.trim() + " DelayHover";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.querySelector('.user-info').setAttribute('class', '"+classAttribute+"')");

	}

	/**
	 * To forcefully open mini cart hover menu in Tablet
	 * @throws Exception  - Exception
	 */
	public void forceOpenMiniCartHover(WebDriver driver) throws Exception{
		String classAttribute = lnkMiniCart.getAttribute("class");
		classAttribute = classAttribute.trim() + " DelayHover";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.querySelector('.mini-cart-total').setAttribute('class', '"+classAttribute+"')");
	}
	
	/**
	 * TO get list of Global Navigation categories on site
	 * @throws Exception - Exception
	 * @return List<String> - Global Navigation Categories
	 */
	public List<String> getGlobalNavCategoriesList() throws Exception{
		List<String> navGlobalList = new ArrayList<String>();
		for(int i =0;i<lnkGlobalNavCategories.size();i++)
		{
			navGlobalList.add(lnkGlobalNavCategories.get(i).getText().toLowerCase());
		}
		return navGlobalList;
	}
	
	 /**
	  * To mouse hover on given leve1 category  
	  * @param String - level1
	  * @Throws Exception - Exception
	  * @return WebElement - level1
	  */
	 public WebElement mouseHoverOnCategory(String level1) throws Exception{
		 WebElement level1Parent = null;
		 if(level1.equals("random")) {
			 int rand = ThreadLocalRandom.current().nextInt(0, lnkGlobalNavCategories.size()-1);
			 level1Parent = lnkGlobalNavCategories.get(rand);
		 }
		 else {
			 level1Parent = driver.findElement(By.xpath("//ul[contains(@class,'level-1')]//a[contains(@title,'"+level1+"')]"));
		 }
		 BrowserActions.mouseHover(driver, level1Parent, "Level-1 category ");
		 BrowserActions.mouseHover(driver, level1Parent, "Level-1 category ");
		 Log.event("Mouse hovered on " + level1Parent.getText());
		 return level1Parent;
	 }
	 
	 /**
	  * To get number of column1 items in submenu
	  * @param String - level1
	  * @return boolean - column item verification
	  * @throws Exception - Exception
	  */
	 public boolean verifySubMenuColumnSize(String level1) throws Exception{
		 WebElement level1Parent = mouseHoverOnCategory(level1);
		 List<WebElement> level2ColumnElements = level1Parent.findElement(By.xpath("..")).findElements(By.cssSelector(".level-2 > .sub-cat-wrapper > .sub-cat-nav ul.col-1>li>div"));
		 List<WebElement> level2ColumnElements2 = level1Parent.findElement(By.xpath("..")).findElements(By.cssSelector(".level-2 > .sub-cat-wrapper > .sub-cat-nav ul.col-1 ul.col-2>li>div"));
		 return level2ColumnElements2.size()<=(8-level2ColumnElements.size());
	 }
	 
	 /**
	  * To Verify banner image shown is correct
	  * @param String - level1 category name
	  * @return boolean - true if banner image if from correct week, else false 
	  * @throws Exceptio - Exception
	  */
	 public boolean verifyBannerImageDate(String level1) throws Exception{
		 mouseHoverOnCategory(level1);

		 if(currentLevel2Banner.isDisplayed()) {
			 String src = currentLevel2Banner.findElement(By.cssSelector("section img")).getAttribute("src");
			 Calendar now = Calendar.getInstance();
			 String yearCurrent = ""+now.get(Calendar.YEAR);
			 String weekCurrent = "wk"+now.get(Calendar.WEEK_OF_YEAR);
			 Log.event("Current Year: "+ yearCurrent+ ", Current Week: "+ weekCurrent);
			 return src.contains(yearCurrent) && src.contains(weekCurrent) && src.contains(Utils.getCurrentBrandShort()) && src.contains(level1);
		 }
		 else
			 return false;
	 }
	 
	 /**
	  * To verify search suggestions are shown with all components
	  * @return boolean - true/false for if all components are shown
	  * @throws Exception - Exception
	  */
	 public boolean verifySearchSuggestions() throws Exception{
		 Log.event("Number of search suggestions:: " + lstProductSuggestion.size());
		 if(Utils.isMobile()) {
			 for(int i=0; i<4 && i< lstProductSuggestion.size(); i++) {
				 WebElement suggestion = lstProductSuggestion.get(i);
				 
				 if(Utils.waitForElement(driver, suggestion.findElement(By.cssSelector(".product-image")))) {
					 Log.event("Search suggestion image is displayed");
				 }else {
					 Log.event("Search suggestion image is not displayed");
					 return false;
				 }
				 
				 if(Utils.waitForElement(driver, suggestion.findElement(By.cssSelector(".product-name")))) {
					 Log.event("Search suggestion name is displayed");
				 }else {
					 Log.event("Search suggestion name is not displayed");
					 return false;
				 }
				 
				 if(Utils.waitForElement(driver, suggestion.findElement(By.cssSelector(".product-price")))) {
					 Log.event("Search suggestion price is displayed");
				 }else {
					 Log.event("Search suggestion price is not displayed");
					 return false;
				 }
			 }
		 }
		 else {
			 for(WebElement suggestion: lstProductSuggestion) {
				 if(Utils.waitForElement(driver, suggestion.findElement(By.cssSelector(".product-image")))) {
					 Log.event("Search suggestion image is displayed");
				 }else {
					 Log.event("Search suggestion image is not displayed");
					 return false;
				 }
				 
				 if(Utils.waitForElement(driver, suggestion.findElement(By.cssSelector(".product-name")))) {
					 Log.event("Search suggestion name is displayed");
				 }else {
					 Log.event("Search suggestion name is not displayed");
					 return false;
				 }
				 
				 if(Utils.waitForElement(driver, suggestion.findElement(By.cssSelector(".product-price")))) {
					 Log.event("Search suggestion price is displayed");
				 }else {
					 Log.event("Search suggestion price is not displayed");
					 return false;
				 }
			 }
		 }
		 return true;
	 }

}
