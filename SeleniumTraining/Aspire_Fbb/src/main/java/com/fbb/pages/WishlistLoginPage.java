package com.fbb.pages;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.account.PasswordResetPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class WishlistLoginPage extends LoadableComponent<WishlistLoginPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();
	String runBrowser;
	private static EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader.getInstance("demandware");
	private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data_" + Utils.getCurrentBrandShort());

	private final static String MYACCOUTLOGIN = "#primary .login-box.login-account ";

	@FindBy(css = ".search-wishlist")
	WebElement divSearchWishlist;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement currentBreadCrumb;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop")
	WebElement currentBreadCrumbMobile;

	@FindBy(css = ".login-box.login-account>h1")
	WebElement accountHeading;

	@FindBy(css = ".login-box.login-account>h2")
	WebElement accountSubHeading;

	@FindBy(css = MYACCOUTLOGIN + "button[name='dwfrm_login_login']")
	WebElement btnSignIn;

	@FindBy(css = MYACCOUTLOGIN + "input[name*='dwfrm_login_username']")
	WebElement fldEmail;

	@FindBy(css = MYACCOUTLOGIN + "input[name*='dwfrm_login_password']")
	WebElement fldPassword;

	@FindBy(css = MYACCOUTLOGIN + ".password.required span.error")
	WebElement txtPasswordError;

	@FindBy(css = MYACCOUTLOGIN + ".username.required span.error")
	WebElement txtEmailError;

	@FindBy(css = ".primary-content .login-account .form-row.username .label-text")
	WebElement txtemail;

	@FindBy(css = ".primary-content .login-account .form-row.password .label-text")
	WebElement txtpassword;

	@FindBy(css = ".inner-block .returning-customers .pwd-show")
	WebElement showpassword;

	@FindBy(css = "#primary #password-reset")
	WebElement lnkForgetPasswordMobile;

	@FindBy(css = "#dwfrm_billing_billingAddress_email_emailAddress")
	WebElement txtGuestEmail;
	
	@FindBy(css = ".primary-content .login-account button[alt='Facebook login']")
	WebElement btnFacebook;
	
	@FindBy(css = ".primary-content .login-account .login-brands")
	WebElement lblLoginBrands;

	@FindBy(name = "dwfrm_login_unregistered")
	WebElement btnGuestLoginContinue;

	@FindBy(css = ".wrapper-forgot-password>h1")
	WebElement headingRequestPassword;

	@FindBy(css = "#dwfrm_requestpassword_email-error")
	WebElement emailTbErrorRequestPassword;

	@FindBy(css = "button[name='dwfrm_login_login']")
	WebElement tempButton;
	
	@FindBy(css = "button[name='dwfrm_requestpassword_send']")
	WebElement tempButton1;

	@FindBy(css = ".wrapper-forgot-password input")
	WebElement emailTbRequestPassword;

	@FindBy(css = "button[name='dwfrm_requestpassword_send']")
	WebElement btnSendRequestPassword;
	
	@FindBy(css = ".login-create button.create-account")
	WebElement btnCreateAccount_Desktop;
	
	@FindBy(css = ".create-account.hide-desktop")
	WebElement btnCreateAccount_Mobile;

	@FindBy(css = "button.ui-dialog-titlebar-close")
	WebElement closeForgotPasswordDialog;
	
	@FindBy(css = ".login-box-content .login-brands")
	WebElement loginMessage;
	
	@FindBy(css = ".login-create-account button[alt='Facebook login']")
	WebElement btnFacebookCreateAcc;
	
	@FindBy(css = ".view-more.hide-desktop")
	WebElement lnkViewMore;
	
	@FindBy(css = ".login-box.login-create-account.clearfix")
	WebElement sectionCreateAccFormField;

	@FindBy(css = "li.facebook a")
	WebElement facebookLinkUrl;
	
	@FindBy(css = ".icon.icon-facebook")
	WebElement faceboolLink;

	@FindBy(css = ".login-box-content.returning-customers.clearfix.make-label-absolute #password-reset")
	WebElement lnkForgotPwd;

	@FindBy(css = ".login-create>h1")
	WebElement headingCreateWishlist;
	
	@FindBy(css = ".login-box-content h1")
	WebElement headingCreateWishlistMobile;
	
	@FindBy(css = ".login-create .content-asset .para")
	WebElement subHeadingCreateWishlist;
	
	@FindBy(css = ".login-box-content .content-asset")
	WebElement subHeadingCreateWishlistMobile;

	@FindBy(css = ".step")
	List<WebElement> wishlistSteps;


	@FindBy(css = ".error-form")
	WebElement txtSignInError;

	@FindBy(css = "#dwfrm_login_password_d0jltfawfakj-error")
	WebElement txtinvalidpassword;

	@FindBy(css = "div[class$='clearfix make-label-absolute'] input[id='dwfrm_login_rememberme']")
	WebElement cbRememberme;
	
	@FindBy(css = ".step")
	WebElement wishlistStep;

	@FindBy(css = ".wrapper-forgot-password")
	WebElement divForgotPassword;

	@FindBy(css = "#PasswordResetForm>fieldset>button")
	WebElement btnPasswordResetSubmit;

	@FindBy(css = "#dwfrm_wishlist_search_firstname")
	WebElement tbSearchWishlistFirstName;

	@FindBy(css = "#dwfrm_wishlist_search_lastname")
	WebElement tbSearchWishlistLastName;

	@FindBy(css = "#dwfrm_wishlist_search_email")
	WebElement tbSearchWishlistemail;

	@FindBy(css = "button[name='dwfrm_wishlist_search_search']")
	WebElement btnSearchWishlist;

	@FindBy(css = "div[class$='clearfix make-label-absolute'] button[id='Facebook']")
	WebElement btnSigninWithfacebook;
	
	@FindBy(css = ".display-mobile .content-asset")
	WebElement divContentAssetMobile;


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public WishlistLoginPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);
		runBrowser  = Utils.getRunBrowser(driver);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, divSearchWishlist))) {
			Log.fail("Wishlist Login Page did not open up.", driver);
		}
	}

	/**
	 * To get the Load status of page
	 * @return boolean - 'true'(Page loaded) / 'false'(Page not loaded)
	 * @throws Exception - Exception
	 */
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}

	/**
	 * To clear text from email and password fields
	 * @throws Exception -
	 */
	public void clearEmailAndPassword()throws Exception{
		typeOnEmail("");
		typeOnPassword("");
		Utils.waitForPageLoad(driver);
	}
	
	
	/**
	 * To click on the signIn button
	 * @throws Exception - Exception
	 */
	public void clickSignInButton()throws Exception{
		BrowserActions.clickOnElementX(btnSignIn, driver, "SignIn Button In Sign in page ");
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, fldEmail);
	}

	/**
	 * To enter the given email id into email field
	 * @param email - User Email ID
	 * @throws Exception - Exception
	 */
	public void typeOnEmail(String email)throws Exception{
		fldEmail.clear();
		BrowserActions.typeOnTextField(fldEmail, email, driver, "Email Address ");
	}

	/**
	 * To enter the given password in Password field
	 * @param password -
	 * @throws Exception - Exception
	 */
	public void typeOnPassword(String password)throws Exception{
		fldPassword.clear();
		BrowserActions.typeOnTextField(fldPassword, password, driver, "Email Address ");
	}
	
	/**
	 * To click on the Create account button
	 * @throws Exception
	 */
	public void clickOnCreateAccount()throws Exception{
		if(Utils.isMobile()) {
			BrowserActions.clickOnElementX(btnCreateAccount_Mobile, driver, "Create Account");
		} else {
			BrowserActions.clickOnElementX(btnCreateAccount_Desktop, driver, "Create Account");
		}
		Utils.waitForElement(driver, sectionCreateAccFormField);
	}

	/**
	 * Verify password is masked or unmasked state
	 * @param type -
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPasswordisMaskedorUnmasked(String type)throws Exception {
		Boolean flag = false;
		if(type.equals("mask"))
		{if(fldPassword.getAttribute("type").equals("password"))
			flag=true;}
		if(type.equals("unmask"))
		{
			if(fldPassword.getAttribute("type").equals("text"))
				flag=true;
		}
		return flag;
	}

	/**
	 * Click show password
	 * @throws Exception - Exception
	 */
	public void clickShowPassword()throws Exception {
		Utils.waitForElement(driver, showpassword);
		BrowserActions.clickOnElementX(showpassword, driver, "SHOW");

	}

	/**
	 * Click forget password link
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public PasswordResetPage clickOnForgetPassword()throws Exception{
		BrowserActions.clickOnElementX(lnkForgetPasswordMobile, driver, "Forget Password Link ");
		return new PasswordResetPage(driver).get();
	}

	/**
	 * To type guest email in its field
	 * @param email -
	 * @throws Exception - Exception
	 */
	public void typeGuestEmail(String email)throws Exception{
		BrowserActions.typeOnTextField(txtGuestEmail, email, driver, "Guest Email");
	}

	/**
	 * To click on continue in the guest login
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public CheckoutPage clickOnContinueInGuestLogin()throws Exception{
		BrowserActions.clickOnElementX(btnGuestLoginContinue, driver, "Continue");
		return new CheckoutPage(driver).get();
	}	

	/**
	 * To type email address in request password field
	 * @param email -
	 * @throws Exception - Exception
	 */
	public void typeEmailaddrsinRequestPassword(String email)throws Exception {			

		if(Utils.waitForElement(driver, tempButton1))
		{
			BrowserActions.clickOnElementX(emailTbRequestPassword, driver, "Type Email Address in Request Password Page");
			emailTbRequestPassword.clear();
			emailTbRequestPassword.sendKeys(email);
		}
	}

	/**
	 * To click send request password button
	 * @throws Exception - Exception
	 */
	public void clickSendRequestPassword()throws Exception {
		Utils.waitForElement(driver, btnSendRequestPassword);
		BrowserActions.clickOnElementX(btnSendRequestPassword, driver, "Send password reset request");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To close forgot password dialog
	 * @throws Exception - Exception
	 */
	public void clickcloseForgotPasswordDialog()throws Exception {
		Utils.waitForElement(driver, closeForgotPasswordDialog);
		BrowserActions.clickOnElementX(closeForgotPasswordDialog, driver, "Close Forgot Password");
		

	}

	/**
	 * Click forgot password link 
	 * @throws Exception - Exception
	 */
	public void clickForgotPwdLink()throws Exception {
		Utils.waitForElement(driver, lnkForgotPwd);
		BrowserActions.clickOnElementX(lnkForgotPwd, driver, "Forgot Pwd link");

	}

	/**
	 * Click facebook link
	 * @throws Exception - Exception
	 */
	public void clickFacebooklink()throws Exception {
		Utils.waitForElement(driver, faceboolLink);
		BrowserActions.clickOnElementX(faceboolLink, driver, "facebook");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * Click facebook link
	 * @return url - 
	 * @throws Exception - Exception
	 */
	public String getFaceBookNavigationLink()throws Exception {
		Utils.waitForElement(driver, facebookLinkUrl);
		return BrowserActions.getTextFromAttribute(driver, facebookLinkUrl, "href", "Email");
	}
	
	/**
	 * To get login message
	 * @return String -
	 * @throws Exception -
	 */
	public String getloginMessage()throws Exception {
		Utils.waitForElement(driver, loginMessage);
		return BrowserActions.getText(driver, loginMessage, "Login message");
	}
	
	/**
	 * To verify login message match with text from property file
	 * @param String - login message from page
	 * @return boolean - true/false if message matches text
	 * @throws Exception - Exception
	 */
	public boolean verifyLoginMessageComingFromProperty(String message)throws Exception {
		Utils.waitForElement(driver, loginMessage);
		String propertyText = prdData.get("LoginMessage");
		return message.toLowerCase().trim().equals(propertyText.toLowerCase().trim());
	}
	
	/**
	 * To verify Header in CS match with text from property file
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyReturningCustH1ComingFromProperty()throws Exception {
		Utils.waitForElement(driver, accountHeading);
		String propertyText;
		String message = BrowserActions.getText(driver, accountHeading, "accountHeading ");
		propertyText = demandWareProperty.getProperty("ReturningCustH1"); 
		
		if(message.toLowerCase().trim().equals(propertyText.toLowerCase().trim())) {
			return true;
		}
		return false;
	}
	
	/**
	 * To verify Sub Header in CS match with text from property file
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyReturningCustH2ComingFromProperty()throws Exception {
		Utils.waitForElement(driver, accountSubHeading);
		String propertyText;
		String message = BrowserActions.getText(driver, accountSubHeading, "accountHeading ");
		propertyText = demandWareProperty.getProperty("ReturningCustH2"); 
		if(message.toLowerCase().trim().equals(propertyText.toLowerCase().trim())) {
			return true;
		}
		return false;
	}
	
	
	/**
	 * Click forgot password reset button
	 * @throws Exception - Exception
	 */
	public void clickForgotPwdSubmit()throws Exception {
		BrowserActions.clickOnElementX(btnPasswordResetSubmit, driver, "Forgot Pwd Submit");

	}

	/**
	 * To verify email and password fields are editable
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailandPasswordFieldsAreEditable()throws Exception {
		String value = BrowserActions.getTextFromAttribute(driver, fldEmail, "aria-required", "Email");
		
		String value1 = BrowserActions.getTextFromAttribute(driver, fldPassword, "aria-required", "Email");
		if (value.isEmpty()) {
			return false;
		}
		if (value1.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/**
	 * To verify email and password fields are mandatory
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailandPasswordareMandatory()throws Exception {
		String value = BrowserActions.getTextFromAttribute(driver, fldEmail, "class", "Email");
		
		String value1 = BrowserActions.getTextFromAttribute(driver, fldPassword, "class", "Email");
		if (!(value.toLowerCase().contains("error"))) {
			return false;
		}
		if (!(value1.toLowerCase().contains("error"))) {
			return false;
		}

		return true;
	}


	/**
	 * To enter wishlist details and search 
	 * @param firstname -
	 * @param lastname -
	 * @param email -
	 * @return WishListPage -
	 * @throws Exception - Exception
	 */
	public WishListPage searchWishlist(String firstname,String lastname,String email) throws Exception{
		Utils.waitForElement(driver, tbSearchWishlistemail);		
		tbSearchWishlistFirstName.sendKeys(firstname);			
		tbSearchWishlistLastName.sendKeys(lastname);
		tbSearchWishlistemail.sendKeys(email);		
		BrowserActions.clickOnElementX(btnSearchWishlist, driver, "Search Wishlist button");
		Utils.waitForPageLoad(driver);		
		return new WishListPage(driver).get();
	}

	/**
	 * Click signin with facebook button
	 * @return URL of the navigated page
	 * @throws Exception - Exception
	 */
	public String clickSigninWithFacebook() throws Exception{
		Utils.waitForPageLoad(driver);	
		Utils.waitForElement(driver, btnSigninWithfacebook);	
		BrowserActions.clickOnElementX(btnSigninWithfacebook, driver, "Signin With Facebook");	
		Utils.waitForPageLoad(driver);
		String url=driver.getCurrentUrl();
		return url;
	}

}
