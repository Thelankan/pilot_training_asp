package com.fbb.pages.account;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.PreApprovedAppPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class MyAccountPage extends LoadableComponent<MyAccountPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;
	public String runPltfrm = Utils.getRunPlatForm();

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	private static final String RewardTrackerSection = ".reard-tracker";

	private static final String OfferSection = ".plcc-ov-right-slider-section";

	private static final String MyAccountPageLinksNavigation = ".navigation-links-row";

	private static final String RewardTrackerSectionActive = ".reard-tracker.slick-slide.slick-current.slick-active";

	private static EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader.getInstance("demandware");
	private static EnvironmentPropertiesReader redirectProperty = EnvironmentPropertiesReader.getInstance("redirect");
	private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data_" + Utils.getCurrentBrandShort());

	@FindBy(css = ".pt_account")
	WebElement readyElement;

	@FindBy(css = ".account-logout a")
	WebElement 	lnkSignOut;

	@FindBy(css = ".addresses>a")
	WebElement lnkAddress;

	@FindBy(css = RewardTrackerSection)
	WebElement rewardTrackerSection;

	@FindBy(css = RewardTrackerSectionActive)
	WebElement rewardTrackerSectionActive;
	
	@FindBy(css = ".reard-tracker.slick-slide:not(.slick-cloned)")
	List<WebElement> lstRewardTrackerCards;

	@FindBy(css = RewardTrackerSection + " .plcc-card-name")
	WebElement rewardTrackerCardName;

	@FindBy(css = RewardTrackerSection + " .plcc-card-owner")
	WebElement rewardTrackerCardHolderName;

	@FindBy(css = ".reard-tracker.slick-slide.slick-current.slick-active .plcc-card-owner")
	WebElement rewardTrackerActiveCardHolderName;

	@FindBy(css = ".reard-tracker.slick-slide.slick-current.slick-active .plcc-card-name")
	WebElement rewardTrackerActiveCardName;

	@FindBy(css = ".reard-tracker.slick-slide.slick-current.slick-active .credit-amount")
	WebElement rewardTrackerActiveCardHolderAmount;

	@FindBy(css = ".reard-tracker.slick-slide.slick-current.slick-active .avaiable-rewards")
	WebElement rewardTrackerActiveCardRewards;

	@FindBy(css = ".reard-tracker.slick-slide.slick-current.slick-active .avaiable-rewards-msg")
	WebElement rewardTrackerActiveCardRewardsMsg;

	@FindBy(css = RewardTrackerSection + " .credit-amount")
	WebElement rewardTrackerCardHolderAmount;

	@FindBy(css = RewardTrackerSection + " .avaiable-rewards")
	WebElement rewardTrackerCardRewards;

	@FindBy(css = RewardTrackerSection + " .avaiable-rewards-msg")
	WebElement rewardTrackerCardRewardsMsg;

	@FindBy(css = ".slick-list.draggable")
	WebElement offersPanel;

	@FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
	WebElement backArrowIcon;
	
	@FindBy(css = ".promo-banner")
	WebElement promoContent;

	@FindBy(css = ".breadcrumb")
	WebElement breadCrumb;
	
	@FindBy(css = MyAccountPageLinksNavigation)
	WebElement divNavOverview;

	@FindBy(css = MyAccountPageLinksNavigation + " .overview a")
	WebElement lnkOverViewLink;

	@FindBy(css = MyAccountPageLinksNavigation + " .overview a[style*='cursor']")
	WebElement lnkOverViewLinkAfterNavigation;

	@FindBy(css = MyAccountPageLinksNavigation + " .profile>a")
	WebElement lnkProfile;

	@FindBy(css = MyAccountPageLinksNavigation + " .addresses a")
	WebElement lnkAddresses;

	@FindBy(css = MyAccountPageLinksNavigation + " .order-history a")
	WebElement lnkOrderHistory;

	@FindBy(css = MyAccountPageLinksNavigation + " .email-preferences a")
	WebElement lnkEmailPref;

	@FindBy(css = MyAccountPageLinksNavigation + " .catalog-preferences a")
	WebElement lnkCatalogPref;

	@FindBy(css = MyAccountPageLinksNavigation + " .wishlists a")
	WebElement lnkWishList;

	@FindBy(css = MyAccountPageLinksNavigation + " li.payment-methods a")
	WebElement lnkPaymentMethods;
	
	@FindBy(css = ".heading.update-value")
	WebElement navDropdownSelectedMobile;
	
	@FindBy(css = ".address-main-heading")
	WebElement headingAddressPageMobile;
	
	@FindBy(css = ".top-content h1")
	WebElement headingOrderHistoryMobile;
	
	@FindBy(css = ".top-content h1")
	WebElement headingPaymentMethodsMobile;

	@FindBy(css = ".plcc-ov-right-slider-section .slick-list")
	WebElement offerSection;
	
	@FindBy(css = ".plcc-ov-top-content")
	WebElement divPLCCSection;

	@FindBy(css = ".reward-trackers-main .link")
	WebElement lnkManageYourCard;

	@FindBy(css = ".reward-trackers-main .slick-dots li")
	List<WebElement> lstRewardSlickDots;

	@FindBy(css = ".reard-tracker.slick-slide")
	List<WebElement> lnkNoOfRewardCard;
	
	@FindBy(xpath = "//a[contains(text(),'QUICK ORDER')]")
	WebElement lnkQuickOrder;
	
	@FindBy(css = ".reward-trackers-main .slick-dots")
	WebElement lnkRewardSectionSlickDot;
	
	@FindBy(css = ".plcc-links > a:nth-child(1)")
	WebElement lnkPlatinumCreditCard;

	@FindBy(css = ".reward-trackers-main .slick-next.slick-arrow")
	WebElement lnkRewardSectionNextArrow;
	
	@FindBy(css = ".reward-trackers-main .slick-dots li")
	WebElement lstRewardSlickDot;

	@FindBy(css = ".reward-trackers-main .slick-prev.slick-arrow")
	WebElement lnkRewardSectionPrevArrow;

	@FindBy(css = ".plcc-ov-right-section .link")
	WebElement lnkViewOffersAndCoupons;

	@FindBy(css = ".plcc-ov-right-slider-section.slick-initialized.slick-slider.slick-dotted")
	WebElement lnkOfferSectionWithArrowsAndSlick;

	@FindBy(css = OfferSection + " .slick-prev.slick-arrow")
	WebElement lnkOfferPrevArrow;

	@FindBy(css = OfferSection + " .slick-next.slick-arrow")
	WebElement lnkOfferNextArrow;

	@FindBy(css = OfferSection + " .slick-dots li")
	List<WebElement> lnkOfferSlickDots;

	@FindBy(css = ".plcc-ov-left-section .plcc-credit-image img")
	WebElement imgCreditPLCCHolder;

	@FindBy(css = ".plcc-rewards-msg div")
	List<WebElement> txtRewardMsgs;

	@FindBy(css = ".plcc-rewards-msg")
	WebElement txtRewardMsgSection;

	@FindBy(css = ".plcc-rewards-msg div")
	WebElement txtRewardMsgNotApproved;
	
	@FindBy(css = ".plcc-rewards-msg div")
	WebElement txtRewardMsgApproved;

	@FindBy(css = ".plcc-ov-left-section .plcc-links a")
	WebElement lnkAcceptThisOfferPLCCHolder;

	@FindBy(css = ".account-overview-heading")
	WebElement lblOverViewHeading;

	@FindBy(css = ".profile-info h1")
	WebElement lblProfileHeading;

	@FindBy(css = ".address-main-heading")
	WebElement lblAddressesHeading;
	
	@FindBy(css = ".paragraph")
	WebElement lblAddressesSubHeading;

	@FindBy(css = "#secondary h1.heading.update-value.hide-desktop.hide-tablet")
	WebElement lblOverView;

	@FindBy(css = ".content-asset .main")
	WebElement lblMyAccount;

	@FindBy(css = "#profilemenu .account-links a")
	WebElement lstAccountMenu;
	
	@FindBy(css = ".navigation-links-row li")
	List<WebElement> navigationLinks;

	//Mobile
	@FindBy(css = ".secondary-navigation>h1")
	WebElement lnkOverView;

	@FindBy(css = ".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close")
	WebElement modalClose;

	@FindBy(css=".catalog-request-header")
	WebElement catalogRequestHeader;

	@FindBy(css = ".catalog-request img")
	WebElement copycatlogimage;

	@FindBy(css = ".catalog-request-step1")
	WebElement lblStep1;

	@FindBy(css = ".catalog-request-step2")
	WebElement lblStep2;

	@FindBy(css=".form-row.show-text")
	List <WebElement> catlogStep1;

	@FindBy(css = ".form-row.requestCatalog.show-text input")
	WebElement requestCatluge;
	
	@FindBy(css = ".international-modal")
	WebElement mdlInternationalForm;
	
	@FindBy(name = "dwfrm_internationalmodal_close")
	WebElement btnCloseInternationalModal;
	
	@FindBy(css = ".hamburger-toggle .menu-toggle")
	WebElement btnToggleHamburgerMenu;
	
	@FindBy(css = ".account-subpage-heading")
	WebElement headingAccountSubpage;
	//================================================================================
	//			WebElements Declaration End
	//================================================================================

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public MyAccountPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		
		if(Utils.waitForElement(driver, mdlInternationalForm, 2)) {
			try {
				BrowserActions.clickOnElementX(btnCloseInternationalModal, driver, "Close Button in International Modal");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement, 2))) {
			Log.fail("My Account page is not loaded", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}

	/**
	 * To navigate to Profile page
	 * @return ProfilePage -
	 * @throws Exception -
	 */
	public ProfilePage navigateToUpdateProfile() throws Exception{
		if(runPltfrm.equals("mobile")) {
			expandCollapseOverview("open");
		}
		BrowserActions.clickOnElementX(lnkProfile, driver, "Profile link");
		Utils.waitForPageLoad(driver);
		return new ProfilePage(driver).get();
	}
	
	
	/**
	 * To navigate to Quick Order Page
	 * @return QuickOrderPage - Quick order page object
	 * @throws Exception - Exception
	 */
	public QuickOrderPage navigateToQuickOrder() throws Exception{
		headers.mouseOverMyAccount();
		BrowserActions.clickOnElementX(lnkQuickOrder, driver, "Quick Order link");
		Utils.waitForPageLoad(driver);
		return new QuickOrderPage(driver).get();
	}

	/**
	 * To navigate to Address Page
	 * @return AddressesPage -
	 * @throws Exception -
	 */
	public AddressesPage navigateToAddressPage()throws Exception{
		if(runPltfrm.equals("mobile")){
			expandCollapseOverview("open");
		}
		BrowserActions.clickOnElementX(lnkAddresses, driver, "Address Page navigation Link");
		return new AddressesPage(driver).get();
	}

	/**
	 * To signout from account
	 * @return SignIn -
	 * @throws Exception -
	 */
	public SignIn signOutAccount() throws Exception{
		headers.mouseOverMyAccount();
		BrowserActions.clickOnElementX(lnkSignOut, driver, "Serach icon");
		Utils.waitForPageLoad(driver);
		return new SignIn(driver).get();
	}

	/**
	 * To click on Overview link
	 * @throws Exception -
	 */
	public void clickOnOverViewLink() throws Exception{
		if(runPltfrm.equals("mobile")) {
			expandCollapseOverview("open");
		}
		BrowserActions.clickOnElementX(lnkOverViewLink, driver, "Overview link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on address link
	 * @return AddressesPage -
	 * @throws Exception -
	 */
	public AddressesPage clickOnAddressLink() throws Exception{
		if(runPltfrm.equals("mobile")) {
			expandCollapseOverview("open");
		}
		BrowserActions.clickOnElementX(lnkAddresses, driver, "Addresses link");
		Utils.waitForPageLoad(driver);
		return new AddressesPage(driver).get();
	}

	/**
	 * To click on order history link
	 * @return OrderHistoryPage -
	 * @throws Exception -
	 */
	public OrderHistoryPage clickOnOrderHistoryLink() throws Exception{
		if(runPltfrm.equals("mobile")) {
			expandCollapseOverview("open");
		}
		BrowserActions.clickOnElementX(lnkOrderHistory, driver, "Order History link");
		Utils.waitForPageLoad(driver);
		return new OrderHistoryPage(driver).get();
	}

	/**
	 * To click on Email Preference Link
	 * @return EmailPreferencePage -
	 * @throws Exception -
	 */
	public EmailPreferencePage clickOnEmailPrefLink() throws Exception{
		if(runPltfrm.equals("mobile")) {
			expandCollapseOverview("open");
		}
		BrowserActions.clickOnElementX(lnkEmailPref, driver, "Email Preference link");
		Utils.waitForPageLoad(driver);
		return new EmailPreferencePage(driver).get();
	}

	/**
	 * To click on catalog preference link
	 * @return CatalogPreferencePage -
	 * @throws Exception -
	 */
	public CatalogPreferencePage clickOnCatalogPrefLink() throws Exception{
		if(runPltfrm.equals("mobile")) {
			expandCollapseOverview("open");
		}
		BrowserActions.clickOnElementX(lnkCatalogPref, driver, "Catalog Preference link");
		Utils.waitForPageLoad(driver);
		return new CatalogPreferencePage(driver).get();
	}

	/**
	 * To click on Wishlist link
	 * @return WishListPage -
	 * @throws Exception -
	 */
	public WishListPage clickOnWishListLink() throws Exception{
		if(runPltfrm.equals("mobile")) {
			expandCollapseOverview("open");
		}
		BrowserActions.clickOnElementX(lnkWishList, driver, "Wish List link");
		Utils.waitForPageLoad(driver);
		return new WishListPage(driver).get();
	}


	/**
	 * To click on the 'Manage Your Card' link 
	 * @throws Exception - Exception
	 */
	public void clickOnManageYourCardLink() throws Exception{
		BrowserActions.clickOnElementX(lnkManageYourCard, driver, "Manage Your Card link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To navigate to PLCC pre approved page
	 * @return PreApprovedPlatinumCardPage -
	 * @throws Exception -
	 */
	public PreApprovedPlatinumCardPage navigateToPreApprovedPlatinumCreditCard()throws Exception{
		BrowserActions.clickOnElementX(lnkPlatinumCreditCard, driver, "Navigate to Platinum Credit Card");
		Utils.waitForPageLoad(driver);
		return new PreApprovedPlatinumCardPage(driver).get();
	}

	/**
	 * To get the reference link of the 'Manage Your Card' link 
	 * @return return 'url' of the link
	 * @throws Exception - Exception
	 */
	public String getManageYourCardLink() throws Exception{
		String url = BrowserActions.getTextFromAttribute(driver, lnkManageYourCard, "href", "Manage your card");
		Utils.waitForPageLoad(driver);
		return url;
	}

	/**
	 * To get reward cards in my account page
	 * @return Number of reward cards
	 * @throws Exception - Exception - one of 4 possibilities
	 */
	public int getNoOfRewardCards() throws Exception{
		Utils.waitForPageLoad(driver);
		return lnkNoOfRewardCard.size() - 2;
	}

	/**
	 * To get Number of slick dots for rewards 
	 * @return Number of slick dot
	 * @throws Exception - Exception
	 */
	public int getNoOfRewardSlickDots() throws Exception{
		Utils.waitForPageLoad(driver);
		return lstRewardSlickDots.size();
	}


	/**
	 * To check rewards are selected when selecting different slick dot
	 * @return true when slick dot are able to select, else false
	 * @throws Exception - Exception
	 */
	public boolean checkRewardSelectedBasedOnSlickDot() throws Exception{
		Utils.waitForPageLoad(driver);
		int slick = getNoOfRewardSlickDots();
		for ( int i=0;i<slick;i++) {
			WebElement elem = lstRewardSlickDots.get(i).findElement(By.cssSelector(" button"));
			String str1 = BrowserActions.getTextFromAttribute(driver, elem, "type", "Slick dot");
			if(!str1.toLowerCase().trim().equals("button")) {
				return false;
			}
		}
		return true;
	}

	/**
	 * To check the slick dots are navigating in continuation loop 
	 * @return true if continuation loop, else false
	 * @throws Exception - Exception
	 */
	public boolean checkTheRewardTrackerIsSelectedInContinousLoop() throws Exception{
		Utils.waitForPageLoad(driver);
		int slick = getNoOfRewardSlickDots();
		for ( int i=0;i<(slick+1);i++) {
			BrowserActions.clickOnElementX(lnkRewardSectionPrevArrow, driver, "Prev Arrow");
			if(!Utils.waitForElement(driver, lnkRewardSectionNextArrow)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * To check the arrows of reward section is navigatable
	 * @return true if both the if navigatable, else returns false
	 * @throws Exception - Exception
	 */
	public boolean checkRewardSectionPrevAndNextArrowIsNavigatable() throws Exception{
		Utils.waitForPageLoad(driver);
		String str1 = BrowserActions.getTextFromAttribute(driver, lnkRewardSectionPrevArrow, "type", "Prev arrow");
		String str2 = BrowserActions.getTextFromAttribute(driver, lnkRewardSectionNextArrow, "type", "Next arrow");
		if(str1.toLowerCase().trim().equals("button") && str2.toLowerCase().trim().equals("button")) {
			return true;
		} else {  
			return false;
		}
	}

	/**
	 * To click offers and coupons
	 * @return Offers and Coupons page
	 * @throws Exception - Exception
	 */
	public OffersAndCouponsPage clickOnViewOffersAndCoupons() throws Exception{
		BrowserActions.clickOnElementX(lnkViewOffersAndCoupons, driver, "View Offers And Coupons link");
		Utils.waitForPageLoad(driver);
		return new OffersAndCouponsPage(driver).get();
	}

	/**
	 * To check when clicking 'Manage Your Card' link is navigating or not 
	 * @return boolean - true if navigating to correct URL, else false
	 * @throws Exception - Exception
	 */
	public boolean checkManageYourCardTab() throws Exception{
		Utils.waitForPageLoad(driver);
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		try {
			driver.switchTo().window(tabs.get(1));
		} catch (IndexOutOfBoundsException e) {
			Log.failsoft("Tab for Manage Your Card not opened." + e.getLocalizedMessage());
		}
		String actualURL = driver.getCurrentUrl();
		driver.switchTo().window(tabs.get(0));
		return checkManageYourCardTab(actualURL);
	}	

	/**
	 * To check the 'Manage Your Card' href is having the comenity url 
	 * @param String - actualURL
	 * @return boolean - true if URL is correct, else false
	 * @throws Exception - Exception
	 */
	public boolean checkManageYourCardTab(String actualURL) throws Exception{
		Utils.waitForPageLoad(driver);
		String expectedURL = redirectProperty.get("comenity_manage") + Utils.getCurrentBrand().getConfiguration().toLowerCase();
		return actualURL.contains(expectedURL);
	}

	/**
	 * To click on specific bread crumb
	 * @param int - elementPos 
	 * @throws Exception - Exception
	 */
	public void clickOnBreadCrumb(int elementPos) throws Exception {
		if (runPltfrm.equals("mobile")) {
			if (Utils.waitForElement(driver, backArrowIcon)) {
				BrowserActions.clickOnElementX(backArrowIcon, driver, "Breadcrumb on mobile");
				Utils.waitForPageLoad(driver);
			}
		}		
		else {
			List<WebElement> prodBreadCrumbText = breadCrumb.findElements(By.cssSelector("a.breadcrumb-element"));
			BrowserActions.javascriptClick(prodBreadCrumbText.get(elementPos - 1), driver, "Bread Crumb to PLP");
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get total no of bread crumbs displaying
	 * @return No of bread crumbs
	 * @throws Exception - Exception
	 */
	public int getTotalNoOfBreadCrumb() throws Exception {
		List<WebElement> prodBreadCrumbText = breadCrumb.findElements(By.cssSelector("a.breadcrumb-element.hide-mobile"));
		return prodBreadCrumbText.size();
	}

	/**
	 * To check my account label coming from property
	 * @return if property value matches, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyMyAccLabelComesFromProperty() throws Exception {
		String textToVerify = BrowserActions.getText(driver, lblMyAccount, "My Account Label");
		String propertyText = demandWareProperty.getProperty("MyAccountOverview");
		if (textToVerify.toUpperCase().trim().contains(propertyText.toUpperCase().trim())) {
			return true;
		}
		return false;
	}

	/**
	 * To click on particular offer slick dot
	 * @param index - 
	 * @throws Exception - Exception
	 */
	public void clickOnParticularOfferSlickDot(int index) throws Exception {
		WebElement slickDot = lnkOfferSlickDots.get(index-1).findElement(By.cssSelector(" button"));
		BrowserActions.clickOnElementX(slickDot, driver, "Slick Dot "+index );
	}


	/**
	 * To get Number of offers by slick dot
	 * @param index - 
	 * @return true if correct
	 * @throws Exception - Exception
	 */
	public boolean verifyOfferSelectedBySlickDot(int index) throws Exception {
		clickOnParticularOfferSlickDot(index);
		clickOnParticularOfferSlickDot(index);
		String status = lnkOfferSlickDots.get(index-1).getAttribute("aria-hidden");
		if(status.toLowerCase().equals("false")) {
			return true;
		}
		return false;
	}

	/**
	 * To verify each offer having slick dot
	 * @param NoOfOffers - 
	 * @return true/false
	 * @throws Exception - 
	 */
	public boolean verifyEachOfferHaveSlickDot(int NoOfOffers) throws Exception {
		int status = lnkOfferSlickDots.size();
		if(status == NoOfOffers) {
			return true;
		}
		return false;
	}

	/**
	 * To get slick dot count
	 * @return size of slick dot of offers
	 * @throws Exception - Exception
	 */
	public int getOfferSlickDotCount() throws Exception {
		return lnkOfferSlickDots.size();
	}

	/**
	 * To get Number of offers by previous arrows
	 * @return true if correct, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyOfferSelectedByPrevArrow() throws Exception {
		clickOnParticularOfferSlickDot(1);
		clickOnParticularOfferSlickDot(1);
		String status = lnkOfferSlickDots.get(0).getAttribute("aria-hidden");

		BrowserActions.clickOnElementX(lnkOfferPrevArrow, driver, "Offer Prev Arrow");
		String status1 = lnkOfferSlickDots.get(0).getAttribute("aria-hidden");
		BrowserActions.clickOnElementX(lnkOfferPrevArrow, driver, "Offer Prev Arrow");
		String status2 = lnkOfferSlickDots.get(0).getAttribute("aria-hidden");

		if(status.toLowerCase().equals(status1.toLowerCase()) || status.toLowerCase().equals(status2.toLowerCase())) {
			return false;
		}
		return true;
	}

	/**
	 * To get Number of offers by previous arrows
	 * @return true if correct, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyOfferSelectedByNextArrow() throws Exception {
		clickOnParticularOfferSlickDot(1);
		clickOnParticularOfferSlickDot(1);
		String status = lnkOfferSlickDots.get(0).getAttribute("aria-hidden");

		BrowserActions.clickOnElementX(lnkOfferNextArrow, driver, "Offer Next Arrow");
		String status1 = lnkOfferSlickDots.get(0).getAttribute("aria-hidden");
		BrowserActions.clickOnElementX(lnkOfferNextArrow, driver, "Offer Next Arrow");
		String status2 = lnkOfferSlickDots.get(0).getAttribute("aria-hidden");

		if(status.toLowerCase().equals(status1.toLowerCase()) || status.toLowerCase().equals(status2.toLowerCase())) {
			return false;
		}
		return true;
	}

	/**
	 * To check reward section card reward message
	 * @return when the reward message is correct
	 * @throws Exception - Exception
	 */
	public boolean checkRewardSectionCardRewardsMsg() throws Exception{
		String cardRewardMsg = BrowserActions.getText(driver, rewardTrackerActiveCardRewardsMsg, "Reward tracker card amount");
		Date dateTime = new Date();
		DateFormat df = new SimpleDateFormat("MM/dd/yy");
		String date = df.format(dateTime);
		if(cardRewardMsg.contains(date)) {
			return true;
		}
		return false;
	}

	/**
	 * To check reward section card holder name
	 * @return true when card holder name
	 * @throws Exception - Exception
	 */
	public boolean checkRewardSectionCardHolderName() throws Exception{
		String cardName = BrowserActions.getText(driver, rewardTrackerCardHolderName, "Reward tracker card name");
		if(!cardName.isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * To check current session brand is correct or not
	 * @return boolean - true when the session card brand is correct
	 * @throws Exception - Exception
	 */
	public boolean checkCurrentSessionBrand() throws Exception{
		List<String> cardBrands= new ArrayList<String>();
		for(WebElement card : lstRewardTrackerCards) {
			cardBrands.add(card.findElement(By.cssSelector(".plcc-card-name")).getText().split(" ")[0]);
		}
		if(Utils.getCurrentBrand().getConfiguration().startsWith(cardBrands.get(0))) {
			return true;
		}else {
			for(int i=1 ; i<cardBrands.size() ; i++) {
				if(Utils.getCurrentBrand().getConfiguration().startsWith(cardBrands.get(i)))
					return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify available credit and reward point relative location 
	 * @return boolean - true/false if available credit and reward points are correctly placed
	 */
	public boolean verifyCreditAndRewardPlacement()throws Exception{
		if(Utils.waitForElement(driver, rewardTrackerSectionActive)) {
			for(WebElement card : lstRewardTrackerCards) {
				WebElement availableCredit = card.findElement(By.cssSelector(".available-credit"));
				WebElement rewardPoint = card.findElement(By.cssSelector(".reward-points"));
				if(!BrowserActions.verifyHorizontalAllignmentOfElements(driver, rewardPoint, availableCredit)) {
					return false;
				}
			}
			return true;
		}else
			return true;
	}

	/**
	 * To verify PLCC content slot is aligned center
	 * @return status as true if center
	 * @throws Exception - Exception
	 */
	public boolean verifyPLCCContentSlotAlignedCenter()throws Exception {
		return Utils.verifyCssPropertyForElement(imgCreditPLCCHolder, "text-align", "center");
	}

	/**
	 * To verify PLCC content copy alignment
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyPLCCContentCopyAlignedCenter()throws Exception {
		return Utils.verifyCssPropertyForElement(txtRewardMsgSection, "text-align", "center");
	}

	/**
	 * To verify pre approved text presence for un approved user
	 * @return boolean - 
	 * @throws Exception -
	 */
	public boolean verifyPreApprovedTextPresentForUnApprovedUser()throws Exception {
		if(Utils.waitForElement(driver, txtRewardMsgNotApproved)) {
			if(BrowserActions.getText(driver, txtRewardMsgNotApproved, "Un-Approved reward message").toLowerCase().contains("Approved".toLowerCase())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * To verify pre approved text presence for approved user
	 * @return boolean - 
	 * @throws Exception -
	 */
	public boolean verifyPreApprovedTextPresentForApprovedUser()throws Exception {
		if(Utils.waitForElement(driver, txtRewardMsgApproved)) {
			if(BrowserActions.getText(driver, txtRewardMsgApproved, "Approved reward message").contains("Pre-Approved")) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * To verify Accept This Offer Link presence For Un Approved User
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyAcceptThisOfferLinkPresentForUnApprovedUser()throws Exception {
		if(BrowserActions.getText(driver, lnkAcceptThisOfferPLCCHolder, "PLCC Accpet Offer").equals("See details")) {
			return true;
		}
		else
			return false;
	}
	
	/**
	 * To verify Accept This Offer Link presence For approved User
	 * @return boolean - 
	 * @throws Exception -
	 */
	public boolean verifyAcceptThisOfferLinkPresentForApprovedUser()throws Exception {
		if(BrowserActions.getText(driver, lnkAcceptThisOfferPLCCHolder, "PLCC Accpet Offer").toLowerCase().contains("accept")) {
			return true;
		}
		else
			return false;
	}

	/**
	 * To click accept this offer link
	 * @return PreApprovedAppPage -
	 * @throws Exception -
	 */
	public PreApprovedAppPage clickAcceptThisOfferLink()throws Exception {
		BrowserActions.clickOnElementX(lnkAcceptThisOfferPLCCHolder, driver, "Accept This Offer Link");
		Utils.waitForPageLoad(driver);
		return new PreApprovedAppPage(driver).get();
	}

	/**
	 * To navigate to payments methdos page
	 * @return PaymentMethodsPage -
	 * @throws Exception -
	 */
	public PaymentMethodsPage navigateToPaymentMethods()throws Exception{
		if(runPltfrm.equals("mobile")){
			Utils.waitForPageLoad(driver);
			expandCollapseOverview("open");
			Utils.waitForPageLoad(driver);
			BrowserActions.clickOnElementX(lnkPaymentMethods, driver, "Payments Link in Menu");
		}else{
			BrowserActions.clickOnElementX(lnkPaymentMethods, driver, "Payment Methods Link");
		}
		Utils.waitForPageLoad(driver);
		return new PaymentMethodsPage(driver).get();
	}

	/**
	 * To expand an collapse overview
	 * @param state, value should be open when need to expand, else close
	 * @throws Exception - Exception
	 */
	public void expandCollapseOverview(String state)throws Exception{
		if(runPltfrm.equals("mobile")) {
			if(state.equals("open") && Utils.waitForElement(driver, lstAccountMenu)){
				Log.event("My Account Overview Menu " + state + "ed");
			} else {
				BrowserActions.clickOnElementX(lblOverView, driver, "My Account Overview Menu Link");
				Log.event("Clicked On Overview Link");
			}
		}
	}

	/**
	 * To navigate to given page from account menu
	 * @param page -
	 * @throws Exception -
	 */
	public void navigateTo(String page)throws Exception{
		if(Utils.isMobile()) {
			BrowserActions.clickOnElementX(lnkOverView, driver, "Account navigation menu");
			Log.event("Opened mobile account menu navigation.");
		}
		for(int i=0;i<navigationLinks.size();i++)
		{
			if(page.equals(navigationLinks.get(i).getAttribute("class")))
			{
				navigationLinks.get(i).findElement(By.cssSelector("a")).click();
				Log.event("Clicked On "+ navigationLinks.get(i));
				Utils.waitForPageLoad(driver);
			}
		}

	}

	/**
	 * To click on close modal
	 * @throws Exception -
	 */
	public void closeModal()throws Exception{		
		BrowserActions.clickOnElementX(modalClose, driver, "Offer Next Arrow");
	}

	/**
	 * To get catalog preference color
	 * @return String -
	 * @throws Exception -
	 */
	public String getCatlogPreferenceColor()throws Exception{
		return lnkCatalogPref.getCssValue("color");
	} 

	/**
	 * To validate highlighted navigation option color for given page
	 * @param String - page
	 * @return boolean - true/false if correctly highlighted
	 * @throws Exception - Exception
	 */
	public boolean validateHighlightedNavigationDrawerColor(String page)throws Exception{
		String color;
		String expectedColor = prdData.get("highlight_color");
		for(int i=0;i<navigationLinks.size();i++){
			if(page.equals(navigationLinks.get(i).getAttribute("class"))){
				color=navigationLinks.get(i).getAttribute("color");
				return(color.equals(expectedColor));
			}
		}
		Log.failsoft("Page navivation link not found in page.");
		return false;
	}
}
