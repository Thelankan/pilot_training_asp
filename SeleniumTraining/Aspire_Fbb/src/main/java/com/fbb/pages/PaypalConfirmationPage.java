package com.fbb.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;


public class PaypalConfirmationPage extends LoadableComponent<PaypalConfirmationPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(css = ".clearfix")
	WebElement readyElement;
	
	@FindBy(css = ".xo-checkout-wrapper.ng-scope")
	WebElement paypalConfimationContent;
	
	 @FindBy(css =".buttons.reviewButton input")
	 WebElement paypalContinueButton;
	 
	//================================================================================
	//			WebElements Declaration End
	//================================================================================

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public PaypalConfirmationPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, paypalConfimationContent))) {
			Log.fail("PaypalConfirmation page is not loaded", driver);
		}
		
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}
	
	public boolean getPageLoadStatus()throws Exception{
		
		return isPageLoaded;
	}
	
	/**
	 * To click continue confirmation button, popup handle and get CheckoutPage
	 * @return CheckoutPage
	 * @throws Exception - Exception 
	 */
	public CheckoutPage clickContinueConfirmation()throws Exception{
		
		//Utils.switchWindows(driver, "PayPal", "title", "false");
		Utils.waitForElement(driver, paypalContinueButton, 10);
		BrowserActions.clickOnElementX(paypalContinueButton, driver, "Paypal Continue Buttons");
		if(Utils.isDesktop()){
			Utils.switchWindows(driver, "plussizetech", "url", "false");
		}
		Utils.waitForPageLoad(driver);
		return new CheckoutPage(driver).get();
		
		/*String currentWindowHandle=driver.getWindowHandle();
		Utils.waitForElement(driver, paypalContinueButton, 10);
		BrowserActions.clickOnElementX(paypalContinueButton, driver, "Paypal Continue Buttons");
		Set<String> openWindowsList=driver.getWindowHandles();        
        String popUpWindowHandle=null;
        for(String windowHandle:openWindowsList)
        {
          if (!windowHandle.equals(currentWindowHandle))
        	  popUpWindowHandle=windowHandle;
        }
		driver.switchTo().window(popUpWindowHandle);  
		Utils.waitForPageLoad(driver);
		return new CheckoutPage(driver).get();*/
	}
	
}
