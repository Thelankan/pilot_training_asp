package com.fbb.pages.customerservice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class ShippingInformationPage extends LoadableComponent<ShippingInformationPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public CSNavigation csNav;
	String runPltfrm = Utils.getRunPlatForm();

	
	/**********************************************************************************************
	 ********************************* WebElements of Shipping Information Page ****************************
	 **********************************************************************************************/
	
	@FindBy(css = ".pt_article-page")
	WebElement readyElement;
	
	@FindBy(css = "div[class*='pt_article-page account-clock-']")
	WebElement lnkNavOrderStatus;
	
	@FindBy(css = "div[class*='pt_article-page account-shipping-']")
	WebElement lnkNavShippingInformation;
	
	@FindBy(css = ".article-heading")
	WebElement csCurrentHeading;
	
	
	/**********************************************************************************************
	 ********************************* WebElements of Shipping Information Page ****************************
	 **********************************************************************************************/	
	
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public ShippingInformationPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
		csNav = new CSNavigation(driver).get();
	}

	
	
	
	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Shipping Info Page did not open up.", driver);
		}
	}

	
	}
	
	
	
	
	
	
