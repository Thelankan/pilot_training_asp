package com.fbb.pages.account;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class PaymentMethodsPage extends LoadableComponent<PaymentMethodsPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;
	
	private static final String MyAccountPageLinksNavigation = ".navigation-links-row";
	
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	
	public final LinkedHashMap<String, String> accountDetails = new LinkedHashMap<String, String>();

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(css = ".payment-list")
	WebElement paymentList;
	
	@FindBy(css = ".cardinfo")
	List<WebElement> savedCardsList;
	
	@FindBy(css = ".pt_account.paymentinstruments")
	WebElement readyElement;
	
	@FindBy(css = "div.delete-creditcard-overlay")
	WebElement ovrlayDeleteCard;
	
	@FindBy(css = ".delete-creditcard-overlay h1")
	WebElement overlayDeleteConfHeading;
	
	@FindBy(css = ".delete-creditcard-overlay .waring-msg")
	WebElement overlayDeleteConfSubHeading;
	
	@FindBy(css = ".delete-card-number")
	WebElement spanDeleteCardNumber;
	
	@FindBy(css = MyAccountPageLinksNavigation + " li.payment-methods a[style*='cursor']")
	WebElement lnkPaymentMethodsAfterNavigate;
	
	@FindBy(css = "button.confirm-delete-creditcard")
	WebElement btnConfirmDelete;
	
	@FindBy(css = ".cancle-delete-creditcard")
	WebElement btnCancelDelete;
	
	@FindBy(css = ".ui-dialog-titlebar-close")
	WebElement btnOverlayClose;
	
	@FindBy(css= ".default-msg")
	WebElement lblDefaultPaymentIcon;
	
	@FindBy(css = ".default-msg .is-default")
	WebElement iconDefaultPayment;
	
	@FindBy(css = ".default-msg span:not(.is-default)")
	WebElement lblDefaultPayment;

	@FindBy(css = ".add-card")
	WebElement lnkAddNewCard;
	
	@FindBy(css= ".cancel.cancel-button.button-text.hide-mobile")
	WebElement lnkCancelAddCard;
	
	@FindBy(css = ".payment-methods a")
	WebElement lnkPaymentMethods;
	
	@FindBy(css = ".catalog-preferences a")
	WebElement lnkCatalogPreferences;
	
	@FindBy(css = ".paymentslist.make-label-absolute h1")
	WebElement lblPaymentMethodsTitle;
	
	@FindBy(css = ".no-card-msg")
	WebElement lblNoSavedCardMessage;
	
	@FindBy(css = ".type .label-text")
	WebElement txtCreditCardTypePlaceholder;
	
	@FindBy(css = ".owner .label-text")
	WebElement txtNamePlaceholder;
	
	@FindBy(id = "dwfrm_paymentinstruments_creditcards_newcreditcard_owner-error")
	WebElement txtInvalidNamePlaceholder;
	
	@FindBy(css = ".creditcardnumber .label-text")
	WebElement txtCardNumberPlaceholder;
	
	@FindBy(css = ".creditcardnumber > label > span.error")
	WebElement txtInvalidCardNumberPlaceholder;
	
	@FindBy(css = ".month .label-text")
	WebElement txtExpiryMonthPlaceholder;
	
	@FindBy(css = ".year .label-text")
	WebElement txtExpiryYearPlaceholder;
	
	@FindBy(css = "#dwfrm_paymentinstruments_creditcards_newcreditcard_type > option")
	List<WebElement> lstCardTypes;
	
	@FindBy(id = "dwfrm_paymentinstruments_creditcards_newcreditcard_type")
	WebElement drpSelectCardType;
	
	@FindBy(id = "dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_month")
	WebElement drpExpiryMonth;
	
	@FindBy(id = "dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_year")
	WebElement drpExpiryYear;
	
	@FindBy(id = "dwfrm_paymentinstruments_creditcards_newcreditcard_owner")
	WebElement txtNameOnCard;
	
	@FindBy(css = "input[id*='dwfrm_paymentinstruments_creditcards_newcreditcard_number']")
	WebElement txtCardNumber;
	
	@FindBy(name = "dwfrm_paymentinstruments_creditcards_create")
	WebElement btnSave;
	
	@FindBy(id = "dwfrm_paymentinstruments_creditcards_newcreditcard_makeDefault")
	WebElement chkMakeDefaultPayment;
	
	@FindBy(css = ".payment-methods-accepted")
	WebElement divPaymentMethodsAccepted;
	
	@FindBy(css = ".breadcrumb-element.hide-mobile")
	List<WebElement> lstBreadCrumbElementsDesktop;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement txtBreadCrumbElementMobile;
	
	@FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
	WebElement imgBreadCrumbBackArrowMobile;
	
	@FindBy(css = ".carddetails .delete")
	List<WebElement> lstSavedCardDelete;
	
	@FindBy(css = ".confirm-delete-creditcard")
	WebElement btnConfirm;
	
	@FindBy(css = ".carddetails")
	List<WebElement> lstSavedCards;
	
	@FindBy(css = ".plcc-card-message")
	WebElement txtPLCCMessage;
	
	@FindBy(css = ".cardinfo")
	WebElement rowFirstCard;

	@FindBy(css = ".plcc-tooltip a")
	WebElement tooltipPLCC;
	
	@FindBy(css = ".showall")
	WebElement btnShowAll;
	
	@FindBy(css = ".default-card .cardtype .img")
	WebElement cardImage;
	
	@FindBy(css = ".default-card .cardtype .cardname")
	WebElement cardName;
	
	@FindBy(css = ".default-card .cardholder")
	WebElement cardHolderName;
	
	@FindBy(css = ".default-card .cardnumber")
	WebElement cardNumber;
	
	@FindBy(css = ".default-card .expdate")
	WebElement cardExpDate;
	
	@FindBy(css = ".default-card .delete")
	WebElement deleteButton;
	
	@FindBy(css = ".expired .expdate")
	WebElement expireExpDate;
	
	@FindBy(css = ".expdate")
	List<WebElement> cardExpDates;
	
	@FindBy(css = ".payment-form.hide")
	WebElement divAddNewCardModule;
	
	@FindBy(css = ".type .selected-option.selected")
	WebElement selectedCard;
	
	@FindBy(css = ".month .selected-option.selected")
	WebElement selectedMonth;
	
	@FindBy(css = ".year .selected-option.selected")
	WebElement selectedYear;
	
	@FindBy(css = ".cardinfo:not(.Visa):not(.Mastercard):not(.Discover):not(.Amex):not(.Master) .carddetails")
	List<WebElement> savedCardPLCC;
	
	@FindBy(css = ".year .select-option:not([label='Expiry Year'])")
	List<WebElement> lstYearsAvailable;
	
	@FindBy(css = ".month .select-option:not([label='Expiry Month'])")
	List<WebElement> lstMonthsAvailable;
	
	//================================================================================
	//			WebElements Declaration End
	//================================================================================




	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public PaymentMethodsPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("My Account page is not loaded", driver);
		}
		
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}
	
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}
	/** 
	 * To delete the card details.
	 * @param cardNumber - 
	 * @throws Exception - Exception
	 */
	public void deleteCard(String cardNumber)throws Exception{
		if(!(driver.findElements(By.cssSelector(".no-card-msg")).size() > 0))
		{
			if(cardNumber.length() > 4)
				cardNumber = cardNumber.substring(cardNumber.length()-4, cardNumber.length());
			
			WebElement btnDelete = driver.findElement(By.xpath("//div[contains(text(),'"+ cardNumber +"')]//following-sibling::div[contains(@class,'delete')]//button"));
			BrowserActions.clickOnElementX(btnDelete, driver, "Delete Link for Card Number : " + cardNumber);
			if(Utils.waitForElement(driver, ovrlayDeleteCard)){
				BrowserActions.clickOnElementX(btnConfirmDelete, driver, "Delete Button in Overlay");
				Utils.waitForPageLoad(driver);
			}
			else
				Log.fail("Confirm Delete Overlay Not opended");
		}
	}
	
	@SuppressWarnings("unused")
	/**
	 * To verify the card is present
	 * @param cardNumber
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyCardPresent(String cardNumber)throws Exception{
		boolean cardState = true;
		
		if(cardNumber.length() > 4)
			cardNumber = cardNumber.substring(cardNumber.length()-4, cardNumber.length());
		
		try{
			WebElement card = driver.findElement(By.xpath("//div[contains(text(),'"+ cardNumber +"')]"));
		}catch(NoSuchElementException e){
			cardState = false;
		}
		
		return cardState;
	}
	/**
	 * To verify payment method title
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPaymentMethodsTitle() throws Exception{
		if(lblPaymentMethodsTitle.getText().trim().equals("Payment Methods"))
			return true;
		else
			return false;
	}
	/**
	 * To verify Save card message
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyNoSavedCardsMessage() throws Exception{
		if(lblNoSavedCardMessage.getText().trim().equals("No saved card found."))
			return true;
		else
			return false;
	}
	/**
	 * To verify placeholder text
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPlaceHolderText() throws Exception{
		if(!txtCreditCardTypePlaceholder.getText().trim().equalsIgnoreCase("Select Credit Card Type"))
			return false;
		
		if(!txtNamePlaceholder.getText().trim().equalsIgnoreCase("Name on Card"))
			return false;
		
		if(!txtCardNumberPlaceholder.getText().trim().equalsIgnoreCase("Card Number"))
			return false;
		
		if(!txtExpiryMonthPlaceholder.getText().trim().equalsIgnoreCase("Expiry Month"))
			return false;
		
		if(!txtExpiryYearPlaceholder.getText().trim().equalsIgnoreCase("Expiry Year"))
			return false;
		
		return true;
	}
	
	/**
	 *  To verify drop down should display plcc card name first 
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyOrderOfCardsDisplayed() throws Exception{
		@SuppressWarnings("unused")
		Boolean nonPLCC = false;
		Boolean PLCC = false;
		for (WebElement cardType : lstCardTypes) {
			if(!cardType.getText().trim().equals("") 
					&& !(cardType.getText().trim().equalsIgnoreCase("Select Credit Card type")))
			{
				if(cardType.getAttribute("plcctype").equals("false"))
				{
					if(PLCC == false)
						nonPLCC = true;
					else
						return false;
				}
				else
				{
					PLCC = true;
				}
			}
		}
		return true;
	}
	/**
	 * To select the credit card type
	 * @param cardType - 
	 * @throws Exception - Exception
	 */
	public void selectCardType(String cardType) throws Exception{
		BrowserActions.selectDropdownByValue(drpSelectCardType, cardType);
	}
	/**
	 * To click save payment method
	 * @throws Exception - Exception
	 */
	public void savePaymentMethod() throws Exception{
		BrowserActions.clickOnElementX(btnSave, driver, "Save");
	}
	/**
	 * To verify error message is displayed when field is empty
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
			
	public Boolean verifyErrorMessageDisplayedWhenFieldAreEmpty() throws Exception{
		Boolean flag = false;
		if(drpSelectCardType.getAttribute("class").contains("error"))
			flag = true;
				
		if(txtNameOnCard.getAttribute("class").contains("error"))
			flag = true;
		
		if(txtCardNumber.getAttribute("class").contains("error"))
			flag = true;
		
		if(drpExpiryMonth.getAttribute("class").contains("error"))
			flag = true;
		
		if(drpExpiryYear.getAttribute("class").contains("error"))
			flag = true;
		
		return flag;
	}
	/**
	 * To verify the invalid entry in card name field
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyInvalidEntryInNameOnCardField() throws Exception{
		String placeHolder = BrowserActions.getText(driver, txtInvalidNamePlaceholder, "Name placeholder");
		if(placeHolder.trim().equalsIgnoreCase("Please enter the valid card holder name"))
			return true;
		else
			return false;
	}
	/**
	 * To verify the invalid entry in card number field
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyInvalidEntryInCardNumberField() throws Exception{
		String placeHolder = BrowserActions.getText(driver, txtInvalidCardNumberPlaceholder, "Card Number placeholder");
		if(placeHolder.trim().equalsIgnoreCase("Please enter a valid number."))
			return true;
		else
			return false;	
	}
	/**
	 * To type the card name 
	 * @param name - 
	 * @throws Exception - Exception
	 */
	public void typeName(String name) throws Exception{
		BrowserActions.typeOnTextField(txtNameOnCard, name, driver, "Name on card");
	}
	/**
	 * To type the card number 
	 * @param card - 
	 * @throws Exception - Exception
	 */
	public void typeCardNumber(String card) throws Exception{
		BrowserActions.typeOnTextField(txtCardNumber, card, driver, "Card Number");
	}
	/**
	 * To get credit card number length
	 * @return Length as Integer
	 * @throws Exception - Exception
	 */
	public int getCardNumberLength() throws Exception{
		return BrowserActions.getText(driver, txtCardNumber, "Card Number").trim().length();
	}
	/**
	 * To Verify month dropdown values
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyExpiryMonthDropdownValues() throws Exception{
		List<String> actual = new ArrayList<String>();
		for (WebElement option : drpExpiryMonth.findElements(By.cssSelector("option"))) {
			actual.add(option.getText().trim());
		}
		if(actual.equals(Arrays.asList("Expiry Month", "January", "February", "March",
				"April", "May", "June", "July", "August", "September", "October", "November", "December")))
			return true;
		else
			return false;	
	}
	/**
	 * To verify expiry year drop down values
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyExpiryYearDropdownValues() throws Exception{
		int startYear = Calendar.getInstance().get(Calendar.YEAR);
		int endYear = startYear + 11;
		WebElement lastOption = drpExpiryYear.findElement(By.cssSelector("option:nth-of-type(13)"));
		int actualEndYear = Integer.parseInt(lastOption.getText().trim()); 
		if(actualEndYear == endYear)
			return true;
		else
			return false;	
	}
	/**
	 * To verify make default payment is checked
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean isMakeDefaultPaymentChecked() throws Exception{
		if(chkMakeDefaultPayment.isSelected())
			return true;
		else
			return false;	
	}
	/**
	 * To verify breadcrum value
	 * @return Status Boolean 
	 * @throws Exception - Exception
	 */
	public Boolean verifyBreadCrumb() throws Exception{
		List<String> actualValue = new ArrayList<String>();
		for (WebElement breadcrumb : lstBreadCrumbElementsDesktop) {
			actualValue.add(breadcrumb.getText().trim());
		}
		
		if(actualValue.equals(Arrays.asList("HOME", "MY ACCOUNT", "PAYMENT METHODS")))
			return true;
		else
			return false;	
	}
	/**
	 * To click the breadcrum 
	 * @param crumb - 
	 * @return Myaccount or homepage object
	 * @throws Exception - Exception
	 */
	public Object clickBreadCrumb(String crumb) throws Exception{
		if(crumb.equalsIgnoreCase("home"))
		{
			BrowserActions.clickOnElementX(lstBreadCrumbElementsDesktop.get(0), driver, "Home in breadcrumb");
			return new HomePage(driver);
		}
		else
		{
			BrowserActions.clickOnElementX(lstBreadCrumbElementsDesktop.get(1), driver, "My Account in breadcrumb");
			return new MyAccountPage(driver);
		}
	}
	/**
	 * To get mobile breadcrum value
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getMobileBreadCrumb() throws Exception{
		return BrowserActions.getText(driver, txtBreadCrumbElementMobile, "Mobile Breadcrumb").trim();
	}
	/**
	 * To click backarrow in breadcrum
	 * @return Myaccount page object
	 * @throws Exception - Exception
	 */
	public MyAccountPage clickBackArrowInBreadcrumb() throws Exception{
		BrowserActions.clickOnElementX(imgBreadCrumbBackArrowMobile, driver, "Back Arrow");
		return new MyAccountPage(driver);
	}
	/**
	 * To click new credit card link
	 * @throws Exception - Exception
	 */
	public void clickAddNewCardLink() throws Exception{
		BrowserActions.clickOnElementX(lnkAddNewCard, driver, "Add New Card");
	}
	
	/**
	 * To cancel adding new credit card
	 * @throws Exception - Exception
	 */
	public void clickCancelAddNewCard() throws Exception{
		BrowserActions.clickOnElementX(lnkCancelAddCard, driver, "Cancel Add Card");
	}
	
	/**
	 * To fill credit crad details
	 * @param cardDetails - 
	 * @throws Exception - Exception
	 */
	public void fillCardDetails(HashMap<String, String> cardDetails) throws Exception{
		if(cardDetails.get("IsPLCC").equalsIgnoreCase("Yes"))
		{
			BrowserActions.selectDropdownByValue(drpSelectCardType, cardDetails.get("CardType"));
			BrowserActions.typeOnTextField(txtCardNumber, cardDetails.get("Number"), driver, "Card Number");
		}
		else
		{
			BrowserActions.selectDropdownByValue(drpSelectCardType, cardDetails.get("CardType"));
			BrowserActions.typeOnTextField(txtNameOnCard, cardDetails.get("Name"), driver, "Name on card");
			BrowserActions.typeOnTextField(txtCardNumber, cardDetails.get("Number"), driver, "Card Number");
			BrowserActions.selectDropdownByValue(drpExpiryMonth, cardDetails.get("ExpMonth"));
			BrowserActions.selectDropdownByValue(drpExpiryYear, cardDetails.get("ExpYear"));
		}
		
		if(cardDetails.get("MakeDefaultPayment").equalsIgnoreCase("Yes"))
		{
			if(isMakeDefaultPaymentChecked() == false)
				BrowserActions.clickOnElementX(chkMakeDefaultPayment, driver, "Make Default Payment");
		}
		else
		{
			if(isMakeDefaultPaymentChecked() == true)
				BrowserActions.clickOnElementX(chkMakeDefaultPayment, driver, "Make Default Payment");
		}
	}
	/**
	 * To verify expire month drop down is displayed 
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyExpiryMonthDropdownDisplayed() throws Exception{
		if(driver.findElement(By.cssSelector(".month .custom-select")).isDisplayed())
			return true;
		else
			return false;	
	}
	/**
	 * To Verify Expire year drop down is dsiplayed
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyExpiryYearDropdownDisplayed() throws Exception{
		if(driver.findElement(By.cssSelector(".year .custom-select")).isDisplayed())
			return true;
		else
			return false;	
	}
	/**
	 * To get default card type.
	 * @return Card type as String
	 * @throws Exception - Exception
	 */
	public String getDefaultCardType() throws Exception{
		for (WebElement savedCard : lstSavedCards) {
			if(savedCard.findElement(By.cssSelector("div > div > span[class^='is-default']")).getAttribute("class").contains("yes"))
				return savedCard.findElement(By.cssSelector("div > div > span[class*='img']")).getAttribute("class").trim();
		}
		return null;
	}
	
	/**
	 * To get default card number.
	 * @return String - Card number
	 * @throws Exception - Exception
	 */
	public String getDefaultCardNumber() throws Exception{
		for (WebElement savedCard : lstSavedCards) {
			if(savedCard.findElement(By.cssSelector("div > div > span[class^='is-default']")).getAttribute("class").contains("yes")) {
				String cardNumber =  savedCard.findElement(By.cssSelector(".cardnumber")).getAttribute("innerHTML").trim().replaceAll("[^0-9]", "");
				Log.event("Default card number ends with:: " + cardNumber);
				return cardNumber;
			}
		}
		return null;
	}
	
	/**
	 * To get card stype
	 * @param index - 
	 * @return Card type as String
	 * @throws Exception - Exception
	 */
	public String getCardType(int index) throws Exception{
		WebElement card = lstSavedCards.get(index);
		return card.findElement(By.cssSelector("div > div > span[class*='img']")).getAttribute("class").trim();
	}
	/**
	 * To verify the brand plcc message
	 * @param cardType - 
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyBrandInPLCCMessage(String cardType) throws Exception{
		if(cardType.contains("rm"))
		{
			if(txtPLCCMessage.getText().trim().contains("Roaman's"))
				return true;
			else
				return false;
		}
		else if(cardType.contains("jl"))
		{
			if(txtPLCCMessage.getText().trim().contains("Jessica"))
				return true;
			else
				return false;
		}
		else
		{
			if(txtPLCCMessage.getText().trim().contains("Woman"))
				return true;
			else
				return false;
		}
	}
	/**
	 * To verify delete button is not available in plcc
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyDeleteNotAvailableForPLCC() throws Exception{
		List<WebElement> plccCards = driver.findElements(By.cssSelector(".payment-list>div[class='cardinfo']"));
		for (WebElement cards : plccCards) {
			if(cards.getAttribute("innerHTML").contains("button-text delete close-on-mobile"))
				return false;
		}
		return true;
	}
	/**
	 * To make card type as default.
	 * @param cardType - 
	 * @throws Exception - Exception
	 */
	public void makeCardTypeAsDefault(String cardType) throws Exception{
		for (WebElement savedCard : lstSavedCards) {
			if(savedCard.findElement(By.cssSelector("div > div > span[class='cardname']")).getText().trim().equalsIgnoreCase(cardType))
			{
				if(savedCard.findElements(By.cssSelector("div > div > span[class='is-default ']")).size() > 0)
				{
					BrowserActions.clickOnElementX(savedCard.findElement(By.cssSelector("div > div > span[class='is-default ']")), driver, "Make Default");
					BrowserActions.clickOnElementX(savedCard.findElement(By.cssSelector(".makedefault-payment")), driver, "Make Default checkbox");
					Utils.waitForPageLoad(driver);
					break;
				}
			}
		}
		Utils.waitForPageLoad(driver);
	}
	/**
	 * To verify order of save credit card
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyOrderOfSavedCards() throws Exception{
		Boolean nonPLCC = false;
		for (int i=1; i < lstSavedCards.size(); i++) {
			if(lstSavedCards.get(i-1).findElement(By.cssSelector("div > div > span[class*='img']")).getAttribute("class").trim().contains("plcc"))
			{
				if(nonPLCC == false)
					BrowserActions.verifyVerticalAllignmentOfElements(driver, lstSavedCards.get(i-1), lstSavedCards.get(i));
				else
					return false;
			}
			else
			{
				nonPLCC = true;
				BrowserActions.verifyVerticalAllignmentOfElements(driver, lstSavedCards.get(i-1), lstSavedCards.get(i));
			}
		}
		return true;
	}
	/**
	 * To verify position of default card in save list
	 * @param cardType - 
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPositionOfDefaultCardInTheSavedCardsList(String cardType) throws Exception{
			if(lstSavedCards.get(0).findElement(By.cssSelector("div > div > span[class='cardname']")).getText().trim().equalsIgnoreCase(cardType))
			{
				if(lstSavedCards.get(0).findElement(By.cssSelector("div > div > span[class*='img']")).getAttribute("class").trim().contains("plcc"))
				{
					if(lstSavedCards.get(0).findElement(By.cssSelector("div > div > span[class^='is-default']")).getAttribute("class").contains("yes"))
						return true;
					else
						return false;
				}
				return false;
			}
			else
			{
				WebElement ele = driver.findElements(By.cssSelector("div[class*='cardinfo ']")).get(0);
				if(ele.findElement(By.cssSelector("div > div > div > span[class^='is-default']")).getAttribute("class").contains("yes"))
					return true;
				else
					return false;
			}
	}
	/**
	 * To Verify make default link is displayed for mobile
	 * @return Boolean - true/false if default link is displayed on mobile
	 * @throws Exception - Exception
	 */
	public Boolean verifyMakeDefaultLinkDisplayedForMobile() throws Exception{
		for (WebElement savedCard : lstSavedCards) {
			if(savedCard.findElement(By.cssSelector("div > div > span[class^='is-default']")).getAttribute("class").contains("yes"))
			{
				if(savedCard.findElements(By.cssSelector(".make-default")).size() > 0)
					return false;
			}
		}
		return true;
	}
	/**
	 * To verify default payment method is not clickable
	 * @return Boolean - true/false if default payment method is clickable 
	 * @throws Exception - Exception
	 */
	public Boolean verifyDefaultPaymentIconNotClickable() throws Exception{
		String defaultCard = null;
		for (WebElement savedCard : lstSavedCards) {
			if(savedCard.findElement(By.cssSelector("div > div > span[class^='is-default']")).getAttribute("class").contains("yes"))
			{
				defaultCard = savedCard.findElement(By.cssSelector("div > div > span[class='cardname']")).getText().trim();
				BrowserActions.clickOnElementX(savedCard.findElement(By.cssSelector("div > div > span.is-default.yes")), driver, "Green indicator for default payment");
				break;
			}
		}
		
		//Verify the default card is still the same
		if(driver.findElement(By.cssSelector("div[class='cardinfo "+ defaultCard +"'] > div.carddetails > div > div > span.is-default.yes")).isDisplayed())
			return true;
		else
			return false;
	}
	
	/**
	 * To verify payment method link is not clickable
	 * @return String Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPaymentMethodsLinkNotClickable() throws Exception{
		if(lnkPaymentMethods.getAttribute("style").contains("cursor: text"))
			return true;
		else
			return false;
	}
	
	/**
	 * To add new credit card
	 * @param String - cardName 
	 * @throws Exception - Exception
	 */
	public void addNewCard(String cardName, boolean... makeDefault)throws Exception{
		String cardInfo[] = checkoutProperty.getProperty(cardName).split("\\|");
		
		String cardType = cardInfo[0];
		String nameOnCard = cardInfo[1];
		String cardNo = cardInfo[2];
		String expMonth = cardInfo[3];
		String expYear = cardInfo[4];
		
		if(!Utils.waitForElement(driver, txtNameOnCard))
			clickAddNewCardLink();
		Utils.waitForElement(driver, txtNameOnCard);
		BrowserActions.scrollToViewElement(drpSelectCardType, driver);
		BrowserActions.selectDropdownByValue(drpSelectCardType, cardType);
		BrowserActions.typeOnTextField(txtNameOnCard, nameOnCard, driver, "Card Name");;
		BrowserActions.typeOnTextField(txtCardNumber, cardNo, driver, "Card Number");
		BrowserActions.scrollToViewElement(drpExpiryMonth, driver);
		BrowserActions.selectDropdownByValue(drpExpiryMonth, expMonth);
		BrowserActions.scrollToViewElement(drpExpiryYear, driver);
		BrowserActions.selectDropdownByValue(drpExpiryYear, expYear);
		if(makeDefault.length > 0) {
			checkOrUnCheckMakeDefaultInCardsSection(makeDefault[0]);
		}
		BrowserActions.clickOnElementX(btnSave, driver, "Save Button");
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, paymentList);
	}
	
	/**
	 * To get number of saved cards count
	 * @return Integer - Number of saved cards
	 * @throws Exception - Exception
 	 */
	public int getNumberOfSavedCards()throws Exception{
		if(Utils.waitForElement(driver, paymentList))
			return savedCardsList.size();
		else
			return 0;
	}
	
	public void checkOrUnCheckMakeDefaultInCardsSection(boolean makeDefault) throws Exception {
		if(makeDefault) {
			if(!(chkMakeDefaultPayment.isSelected())) {
				BrowserActions.clickOnElement(chkMakeDefaultPayment, driver, "Make Default Payment");
			}
		} else {
			if(chkMakeDefaultPayment.isSelected()) {
				BrowserActions.clickOnElement(chkMakeDefaultPayment, driver, "Make Default Payment");
			}
		}
	}
	
	/**
	 * To get default card type.
	 * @return String - Card type as String
	 * @throws Exception - Exception
	 */
	public boolean verifyDefaultCardGreenIndicator() throws Exception{
		if(Utils.waitForElement(driver, btnShowAll))
			BrowserActions.clickOnElementX(btnShowAll, driver, "Show all button");
		for (WebElement savedCard : lstSavedCards) {
			if(savedCard.findElement(By.cssSelector("div > div > span[class^='is-default']")).getAttribute("class").contains("yes"))
				return (savedCard.findElement(By.cssSelector("div > div > span[class*='img']")).getLocation().x > savedCard.findElement(By.cssSelector("div > div > span[class^='is-default']")).getLocation().x);
		}
		return false;
	}
	
	/**
	 * To delete all saved cards
	 * @throws Exception - Exception
	 */
	public void deleteAllSavedCards() throws Exception{
		List<WebElement> deleteButtons = driver.findElements(By.cssSelector("button.delete"));
		int noCardsSaved = deleteButtons.size();
		for(int i=0 ; i<noCardsSaved ; i++) {
			BrowserActions.clickOnElementX(driver.findElement(By.cssSelector("button.delete")), driver, "DELETE");
			BrowserActions.clickOnElementX(btnConfirmDelete, driver, "Confirm Delete");
			Utils.waitForPageLoad(driver);
		}
	}
	
	/**
	 * To select month on add new card module
	 * @param String - Month
	 * @throws Exception - Exception
	 */
	public void selectMonth(String expMonth) throws Exception{
		BrowserActions.scrollToViewElement(drpExpiryMonth, driver);
		BrowserActions.selectDropdownByValue(drpExpiryMonth, expMonth);
		
	}
	
	/**
	 * To select year on add new card module
	 * @param String - Year
	 * @Exception - Exception
	 */
	public void selectYear(String expYear) throws Exception{
		BrowserActions.scrollToViewElement(drpExpiryYear, driver);
		BrowserActions.selectDropdownByValue(drpExpiryYear, expYear);
	}
	
	/**
	 * To get selected card on add new card module
	 * @return String - Card type
	 * @Exception - Exception
	 */
	public String getSelectedCard() throws Exception{
		return BrowserActions.getText(driver, selectedCard, "Selected card");
	}
	
	/**
	 * To get name entered on add new card module
	 * @return String - Card owner name
	 * @Exception - Exception
	 */
	public String getNameText() throws Exception{
		return BrowserActions.getText(driver, txtNameOnCard, "Name typed");
	}
	
	/**
	 * To get card number entered on add new card module
	 * @return String - Card number
	 * @Exception - Exception
	 */
	public String getCardNumber() throws Exception{
		return BrowserActions.getText(driver, txtCardNumber, "Card number typed");
	}
	
	/**
	 * To get month selected on add new card module
	 * @return String - Month selected
	 * @Exception - Exception
	 */
	public String getSelectedMonth() throws Exception{
		return BrowserActions.getText(driver, selectedMonth, "month selected");
	}
	
	/**
	 * To get year selected on add new card module
	 * @return String - Year selected
	 * @Exception - Exception
	 */
	public String getSelectedYear() throws Exception{
		return BrowserActions.getText(driver, selectedYear, "Year selected.");
	}
	
	/**
	 * To verify position of default card 
	 * @return Boolean - true/false if default card is placed correctly
	 * @throws Exception - Exception
	 */
	public Boolean verifyPositionOfDefaultCard() throws Exception{
		WebElement defaultCard = driver.findElement(By.cssSelector(".cardtype > .is-default.yes")).findElement(By.xpath("../../../.."));
		Log.message(defaultCard.getAttribute("class"));
		if(savedCardPLCC.size()!=0) {
			if(!BrowserActions.verifyVerticalAllignmentOfElements(driver, savedCardPLCC.get(savedCardPLCC.size()-1), defaultCard)) {
				return false;
			}
		}

		List<WebElement> lstNonPLCC = lstSavedCards;
		lstNonPLCC.removeAll(savedCardPLCC);
		if(savedCardPLCC.size()!=0) {
			if(!BrowserActions.verifyVerticalAllignmentOfElements(driver, defaultCard, lstNonPLCC.get(0))) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify months in dropdown are in correct order
	 * @return Boolean - true/false if months are in correct order
	 * @throws - Exception - Exception
	 */
	public Boolean verifyMonthList()throws Exception{
		if(lstMonthsAvailable.size()==12) {
			for(int i=0 ; i <12 ; i++) {
				if(!lstMonthsAvailable.get(i).getAttribute("value").trim().equals(i+1+"")) {
					return false;
				}
			}
		}
		else {
			Log.failsoft("12 months are not present.");
			return false;
		}
		return true;
	}
	
	/**
	 * To verify years are shown as spec
	 * @return Boolean - true/false if years are shown as current year+11 years top down.
	 * @throws Exception - Exception
	 */
	public Boolean verifyYearList()throws Exception{
		int currentYear = Utils.getCurrentDateDetails("year");
		if(lstYearsAvailable.size()==12) {
			for(int i=0 ; i <12 ; i++) {
				if(!lstYearsAvailable.get(i).getAttribute("value").trim().equals(currentYear+i+"")) {
					return false;
				}
			}
		}
		else {
			Log.failsoft("Total 12 years are not shown.");
			return false;
		}
		return true;
	}


	/**
	 * To click on delete button on index numbered saved card
	 * @param int - index of card
	 * @throws Exception - Exception
	 */
	public String clickDeleteOnSavedCard(int index)throws Exception{
		WebElement delButton = lstSavedCardDelete.get(index-1);
		WebElement cardNumber = delButton.findElement(By.xpath("../../../..")).findElement(By.cssSelector(".cardnumber"));
		BrowserActions.clickOnElementX(delButton, driver, "DELETE button on card #" + index);
		Utils.waitForElement(driver, ovrlayDeleteCard);
		return cardNumber.getText();
	}
	
	/**
	 * To cancel deleting a card 
	 * @throws Exception - Exception
	 */
	public void clickCancelOnDeleteConfirmation()throws Exception{
		BrowserActions.clickOnElementX(btnCancelDelete, driver, "Cancel button.");
	}
	
	/**
	 * To close delete cnfirmation 
	 * @throws Exception - Exception
	 */
	public void clickCloseConfirmationOverlay()throws Exception{
		BrowserActions.clickOnElementX(btnOverlayClose, driver, "Close button.");
	}
	
	/**
	 * To click on DELETE on Delete Confirmation
	 * @throws Exception - Exception
	 */
	public void clickDeleteOnDeleteConfirmation() throws Exception{
		BrowserActions.clickOnElementX(btnConfirmDelete, driver, "DELETE button.");
	}
		
	 /** To verify details of saved PLCC cards
	 * @return boolean - true/false if details have been verified
	 * @throws Exception - Exception
	 */
	public boolean verifyDetailsSavedPLCC()throws Exception{
		for(WebElement savedPLCC: savedCardPLCC) {
			WebElement cardName = savedPLCC.findElement(By.cssSelector(".cardname"));
			WebElement cardNameOnCard = savedPLCC.findElement(By.cssSelector(".cardholder"));
			WebElement cardAvailableCredit = savedPLCC.findElement(By.cssSelector(".cardnumber-along-text"));
			WebElement cardRewardPoints = savedPLCC.findElement(By.cssSelector(".rewardpoints-along-text"));
			WebElement cardToolTip = savedPLCC.findElement(By.cssSelector(".plcc-tooltip"));
			if(!Utils.isMobile()) {
				if(!BrowserActions.verifyHorizontalAllignmentOfElements(driver, cardNameOnCard, cardName) ||
					!BrowserActions.verifyHorizontalAllignmentOfElements(driver, cardAvailableCredit, cardNameOnCard) ||
					!BrowserActions.verifyHorizontalAllignmentOfElements(driver, cardRewardPoints, cardAvailableCredit) ||
					!BrowserActions.verifyHorizontalAllignmentOfElements(driver, cardToolTip, cardRewardPoints))
					return false;
			}else {
				if(BrowserActions.verifyVerticalAllignmentOfElements(driver, cardNameOnCard, cardName) ||
					BrowserActions.verifyVerticalAllignmentOfElements(driver, cardAvailableCredit, cardNameOnCard) ||
					BrowserActions.verifyVerticalAllignmentOfElements(driver, cardRewardPoints, cardAvailableCredit) ||
					BrowserActions.verifyVerticalAllignmentOfElements(driver, cardToolTip, cardRewardPoints))
					return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify saved card logo position
	 * @return boolean - true/false if log are positioned correctly
	 * @throws Exception - Exception
	 */
	public boolean verifySavedCardLogo()throws Exception{
		for(WebElement savedCard: lstSavedCards) {
			WebElement cardLogo = savedCard.findElement(By.cssSelector(".img"));
			WebElement cardName = savedCard.findElement(By.cssSelector(".cardname"));
			if(!BrowserActions.verifyHorizontalAllignmentOfElements(driver, cardName, cardLogo)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify card expiration date text color
	 * @return boolean - true/false if date is shown in corrrect clor
	 * @throws Exception - Exception
	 */
	public boolean verifyExpirationDateColor()throws Exception{
		String expiredDateColor = demandwareData.get("card_date_color_expired");
		String validDateColor = demandwareData.get("card_date_color_valid");
		for(WebElement cardDate : cardExpDates) {
			String date = cardDate.getText().split("\\s")[1];
			if((Utils.getNumberInString(date)+"").length() !=0) {
				int expMonth = Utils.getNumberInString(date.split("/")[0]);
				int expYear = Utils.getNumberInString(date.split("/")[1]);
				int currentMonth = Utils.getCurrentDateDetails("month");
				int currentYear = Utils.getCurrentDateDetails("year")%100;
				if(currentYear < expYear || 
					(currentYear == expYear && currentMonth < expMonth-1)) {
					if(!cardDate.getCssValue("color").equals(validDateColor))
						return false;
				}
				if(currentYear > expYear || 
					(currentYear == expYear && currentMonth >= expMonth-1)) {
					if(!cardDate.getCssValue("color").equals(expiredDateColor))
						return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * To verify PLCC cards are above other saved cards
	 * @return boolean - true/false if saved PLCC is shown correctly
	 * @throws Exception - Exception 
	 */
	public boolean verifySavedPLCCLocation()throws Exception{
		WebElement lastPLCC = null, firstNonPLCC = null;
		if(savedCardsList.size() != 0) {
			for(int i=0 ; i<savedCardsList.size() ; i++) {
				if(savedCardsList.get(i).getAttribute("class").length() > "cardinfo".length()) {
					lastPLCC = savedCardsList.get(i);
					if(i < savedCardsList.size()-1) {
						firstNonPLCC = savedCardsList.get(i+1);
					}else {
						Log.event("No non-PLCC card is saved to verify.");
						return true;
					}
				}
			}
			return BrowserActions.verifyVerticalAllignmentOfElements(driver, lastPLCC, firstNonPLCC);
		}else {
			Log.event("No PLCC card is saved to verify.");
			return true;
		}
	}
	
	/**
	 * To display saved card list fully
	 * @throws Exception - Exception 
	 */
	public void expandSavedCardList()throws Exception{
		if(Utils.waitForElement(driver, btnShowAll)) {
			BrowserActions.clickOnElementX(btnShowAll, driver, "Show All");
			Log.event("Clicked on Show All button");
		}
	}
}
