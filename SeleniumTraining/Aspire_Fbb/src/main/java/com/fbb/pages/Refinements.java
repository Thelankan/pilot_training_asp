package com.fbb.pages;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class Refinements extends LoadableComponent<Refinements> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public String brand = System.getProperty("brand");
	String runPltfrm = Utils.getRunPlatForm();

	//=====================================================================//
	//	-------------------Mobile Elements-----------------------------    //
	//=====================================================================//

	@FindBy(css = "#main .refinements")
	WebElement readyElementMobile;

	@FindBy(css = "#main .refinements")
	WebElement readyElementDesktop;

	@FindBy(css = ".refinements.ws-sticky")
	WebElement refinementSection;

	@FindBy(css = ".refinements .filters")
	WebElement drpFilterByCollapsed;
	
	@FindBy(css = ".filter.no-ajaxcall")
	WebElement drpFilterByArrow;

	@FindBy(css = ".filter>.fa-angle-down")
	WebElement btnFilterArrowDown;

	@FindBy(css = ".filters.refinement-mobile")
	WebElement drpFilterByExpanded;

	@FindBy(css = ".search-result-options.hide-tablet.hide-mobile .sort-by.custom-select")
	WebElement drpSortByCollapsedDesktop;

	@FindBy(css =".refinements .sort-by")
	WebElement drpSortByCollapseMobileTablet;

	@FindBy(css = ".hide-desktop .sort-by.custom-select")
	WebElement drpSortByCollapsedMobile;

	@FindBy(css = ".hide-desktop .sort-by .selected-option.selected")
	WebElement drpSortSelectedOption;

	@FindBy(css = ".sort-by.custom-select.current_item")
	WebElement drpSortByExpanded;

	@FindBy(css = ".refinement-heading-category")
	List<WebElement> lstRefinementFilterOptionsDesktop;

	@FindBy(css = ".mobile-refinement .refinement")
	List<WebElement> lstRefinementFilterOptionsMobile;

	@FindBy(css = ".mobile-refinement")
	WebElement refinementFilterOptionsDesktop;

	@FindBy(css = ".mobile-refinement .refinement")
	WebElement refinementFilterOptionsMobile;

	@FindBy(css = ".refinement h3")
	List<WebElement> lstRefinementFilterOptionsInMobile;

	@FindBy(css = ".refinement.active div[class='expended']")
	WebElement divExpandedRefinementSubList;

	@FindBy(css = ".refinement.active div.expended")
	WebElement divExpandedRefinementSubListTablet;

	@FindBy(css= ".refinement-heading-category.expanded.toggle")
	WebElement drpExpandedRefinement;

	@FindBy(css = "div[class='expended'][style*='block'] li")
	List<WebElement> lstExpandedRefinementAllSubList;

	@FindBy(css = "div[class*='expended'][style*='block'] li > div.available span[class^='check-square']")
	List<WebElement> lstExpandedRefinementAvailableSubList;

	@FindBy(css = ".refinement.active div[class='expended-main'] li > div.available span.check-square")
	List<WebElement> lstExpandedRefinementAvailableSubListTablet;

	@FindBy(css = "div[class='expended'][style*='none'] li > div.available")
	List<WebElement> lstCollapsedRefinementAvailableSubList;

	@FindBy(css = ".refinement i.fa.fa-angle-down.fa-2x")
	List<WebElement> lstRefinementDownArrow;

	@FindBy(css = ".refinement.active i.fa-angle-down")
	WebElement lnkRefinementMenuBackTablet;

	@FindBy(css = "a.backbutton")
	WebElement lnkRefinementMenuBackMobile;

	@FindBy(css = ".search-result-options.hide-tablet.hide-mobile .result-count span:nth-child(1)")
	WebElement lblRefinementCountDesktop;

	@FindBy(css = ".search-result-options.hide-tablet.hide-mobile .result-count")
	WebElement divResultCountDesktop;

	@FindBy(css = "#category-level-2 .expandable.active")
	WebElement level2Section;

	@FindBy(css = ".result-count.result-count-mobile.hide-desktop span:nth-child(1)")
	WebElement lblRefinementCountTablet;

	@FindBy(css= ".filters.refinement-mobile .active .toggle .fa.fa-angle-down.fa-2x")
	WebElement iconReturnToMainRefinementInMobile;

	@FindBy(css = ".filters.refinement-mobile .active .toggle-heading .count.hide-mobile")
	WebElement lblSelectedSubRefCount;

	@FindBy(css =".filters.refinement-mobile .filter-count")
	WebElement lblSelectedSubRefCountMobile;

	@FindBy(css =".refine-by-clearall.hidedesktop .backbutton")
	WebElement refinementBaackButtonMobile;

	@FindBy(css = ".filters.refinement-mobile .active")
	WebElement activeSubRefinement;

	@FindBy(css = ".refinement-breadcrumb a.device-clear-all")
	WebElement btnClearAllInTablet;

	@FindBy(css = ".refine-by-clearall.hidedesktop .device-clear-all.hide-desktop")
	WebElement btnClearAllInMobile;

	@FindBy(css = ".refinement-breadcrumb")
	WebElement divRefinementBar;
	
	@FindBy(css = ".refinement-breadcrumb .refine-by-clearall .device-clear-all")
	WebElement btnClearAllInDesktop;

	@FindBy(css = ".clear-refinement .clear")
	WebElement btnClearInActiveRefInTablet;

	@FindBy(css = ".sort-by.custom-select.current_item .selection-list > li")
	List<WebElement> lstSortingOptions;

	@FindBy(css = ".sort-by.custom-select.current_item .selection-list > li:not([class='selected'])")
	List<WebElement> lstSortOptionSelectable;

	@FindBy(css = ".sort-by.custom-select.current_item .selected-option.selected")
	WebElement selectedSortingOptionNearSortBy;

	@FindBy(css = ".sort-by.custom-select.current_item .selection-list > li.selected")
	WebElement selectedSortingOptionInDrp;

	@FindBy(css = ".sort-breadcrumb")
	WebElement txtSortBreadcrumb;

	@FindBy(id = "search-result-items")
	WebElement sectionSearchResults;
	//=====================================================================//
	//    -------------------Vertical Refinement Elements-----------------------------  //
	//=====================================================================//

	@FindBy(id = "category-level-1")
	WebElement verticalRefinement;

	@FindBy(css = "#category-level-1>li")
	List<WebElement> lstLevel1Category;

	@FindBy(css = ".level-1 .selected>a")
	WebElement activeLevel1;

	@FindBy(css = "#category-level-2>li")
	List<WebElement> lstLevel2Category;

	@FindBy(css = "#category-level-3>li")
	List<WebElement> lstLevel3Category;

	@FindBy(css = "#category-level-4>li")
	List<WebElement> lstLevel4Category;

	@FindBy(css = ".category-level-1.cr-section-pl")
	WebElement Level2;

	@FindBy(id = "category-level-2")
	WebElement Level3;

	@FindBy(css = ".refinements-count .refinement .toggle-heading")
	List<WebElement> lstFilterOptions;


	//================================================================================
	//			WebElements Declaration End
	//================================================================================

	@FindBy(css = ".loader[style*='block']")
	WebElement plpSpinner;

	@FindBy(css = ".promo-banner")
	WebElement divPromoBanner;

	@FindBy(css = ".hide-desktop .selection-list")
	WebElement divSortMenu;
	
	@FindBy(css = ".price .fa-angle-down")
	WebElement btnDownPriceDesktop;
	
	@FindBy(css = ".price .fa-angle-up")
	WebElement btnUpPriceDesktop;
	
	@FindBy(css = ".size .fa-angle-down")
	WebElement btnDownSizeDesktop;
	
	@FindBy(css = ".size .fa-angle-up")
	WebElement btnUpSizeDesktop;
	
	@FindBy(css = ".brand .fa-angle-down")
	WebElement btnDownBrandDesktop;
	
	@FindBy(css = ".brand .fa-angle-up")
	WebElement btnUpBrandDesktop;
	
	@FindBy(css = ".refinementColor .fa-angle-down")
	WebElement btnDownColorDesktop;
	
	@FindBy(css = ".refinementColor .fa-angle-up")
	WebElement btnUpColorDesktop;
	
	static final String colorOptions = "li.color-refinement-count li a";
	
	@FindBy(css = colorOptions)
	List<WebElement> lstAvailableColors;
	
	@FindBy(css = "li.color-refinement-count li")
	WebElement AvailableColors;
	
	static final String refinementColor = ".breadcrumb-refinement-value>span>a";
	
	@FindBy(css = refinementColor)
	List<WebElement> selectedColors;
	
	@FindBy(css = ".breadcrumb-refined-by")
	WebElement spanFilteredBy;
	
	@FindBy(css = ".price div.available>a")
	List<WebElement> lstAvailablePrices;
	
	@FindBy(css = ".brand div.available>a")
	List<WebElement> lstAvailableBrands;
	
	static final String refinementSize = ".size div.available>a";
	
	@FindBy(css = refinementSize)
	List<WebElement> lstAvailableSizes;
	
	@FindBy(css = ".breadcrumb-refinement-name")
	List<WebElement> lstSelectedRefinements;

	@FindBy(css = ".pt_product-search-noresult")
	WebElement noSearchResultPage;


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public Refinements(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded) {
			if(Utils.waitForElement(driver, noSearchResultPage)){
				Log.reference("Search Results not available for given Keywork.");
			}else{
				if(Utils.getRunPlatForm().equals("desktop")) {
					if(!Utils.waitForElement(driver, readyElementDesktop))
						Log.fail("Refinements menu didn't display", driver);
				}
				else{
					if(!Utils.waitForElement(driver, readyElementMobile))
						Log.fail("Refinements menu didn't display", driver);
				}
			}
		}
		elementLayer = new ElementLayer(driver);
		Log.event("Refinements Loaded successfully.");
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To wait for Search results spinner
	 */
	private void waitUntilPlpSpinnerDisappear() {
		Utils.waitUntilElementDisappear(driver, plpSpinner);
	}

	/**
	 * To get the element which have exact variable name
	 * @param expectedEle - String "Expected Element"
	 * @return WebElement - Actual WebElement with expected Element variable name
	 * @throws Exception - Exception
	 */
	public static WebElement getElement(String expectedEle)throws Exception{
		WebElement element = null;
		try{
			Field elementField = Refinements.class.getClass().getDeclaredField(expectedEle);
			elementField.setAccessible(true);
			element = ((WebElement) elementField.get(Refinements.class.getClass()));
		}//try
		catch(Exception e){
			e.printStackTrace();

		}//catch
		return element;
	}

	/**
	 * to get the name of the active Level1 Category name
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public String getActiveLevel1CategoryName()throws Exception{
		return activeLevel1.getText().trim();
	}

	/**
	 * to navigate to given level 2 Category.
	 * @level2 String - Name of level2 menu
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public void navigateLevel2Category(String level2)throws Exception{
		for (WebElement category : lstLevel2Category) {
			if(category.getText().trim().equalsIgnoreCase(level2))
			{
				System.out.println("Name of l2 category to be clicked on:: " + category.getText());
				BrowserActions.clickOnElementX(category, driver, "Level 2 category");
				break;
			}
		}
	}

	/**
	 * to get the name of the active Level2 Category name
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public String getActiveLevel2CategoryName()throws Exception{
		for (WebElement category : lstLevel2Category) {
			if(category.getAttribute("class").contains("active"))
			{
				return category.findElement(By.cssSelector("div>a")).getText().trim();
			}
		}
		return null;
	}

	/**
	 * to get the name of the active Level1 Category name
	 * @param categoryName - 
	 * @param expand -
	 * @throws Exception - Exception
	 */
	public void expandCollapseLevel1Category(String categoryName, Boolean expand)throws Exception{
		WebElement ele;
		for (WebElement category : lstLevel1Category) {
			if(category.findElement(By.cssSelector("div>a")).getText().trim().toLowerCase().contains(categoryName.toLowerCase()))
			{
				if(expand.equals(true))
				{
					if(category.getAttribute("class").equals("expandable active "))
					{
						Log.event("The category is already expanded");
						break;
					}
					else
					{
						ele = category.findElement(By.cssSelector("div>span"));
						BrowserActions.clickOnElementX(ele, driver, "");
						break;
					}
				}
				else
				{
					if(category.getAttribute("class").equals("expandable active  non-active"))
					{
						Log.event("The category is already collapsed");
						break;

					}
					else
					{
						ele = category.findElement(By.cssSelector("div>span"));
						BrowserActions.clickOnElementX(ele, driver, "");
						break;
					}
				}
			}
		}

	}


	/**
	 * to get the name of the active Level1 Category name
	 * @param categoryName - 
	 * @param expand -
	 * @throws Exception - Exception
	 */
	public void expandCollapseLevel2Category(String categoryName, Boolean expand)throws Exception{
		WebElement ele;
		List<WebElement> lstlevel2 = driver.findElements(By.cssSelector("#category-level-1>li>ul>li.expandable"));
		for (WebElement category : lstlevel2) {
			if(category.findElement(By.cssSelector("div>a")).getText().trim().equalsIgnoreCase(categoryName))
			{
				if(expand.equals(true))
				{
					if(category.getAttribute("class").equals("expandable active"))
					{
						Log.event("The category is already expanded");
						break;
					}
					else
					{
						BrowserActions.scrollInToView(category.findElement(By.cssSelector("div>span")), driver);
						ele = category.findElement(By.cssSelector("div>span"));
						BrowserActions.clickOnElementX(ele, driver, "");
						break;
					}
				}
				else
				{
					if(category.getAttribute("class").equals("expandable active non-active") == false)
					{
						BrowserActions.scrollInToView(category.findElement(By.cssSelector("div>span")), driver);
						ele = category.findElement(By.cssSelector("div>span"));
						BrowserActions.clickOnElementX(ele, driver, "");
						break;
					}
					else
					{
						Log.event("The category is already collapsed");
						break;
					}
				}
			}
		}

	}

	/**
	 * To enpand / collapse level3 category
	 * @param categoryName -
	 * @param expand -
	 * @throws Exception -
	 */
	public void expandCollapseLevel3Category(String categoryName, Boolean expand)throws Exception{
		WebElement ele;
		for (WebElement category : lstLevel3Category) {
			if(category.findElement(By.cssSelector("div>a")).getText().trim().equalsIgnoreCase(categoryName))
			{
				if(expand.equals(true))
				{
					if(category.getAttribute("class").contains("expandable active"))
					{
						Log.event("The category is already expanded");
						break;
					}
					else
					{
						BrowserActions.scrollInToView(category.findElement(By.cssSelector("div>span")), driver);
						ele = category.findElement(By.cssSelector("div>span"));
						BrowserActions.clickOnElementX(ele, driver, "");
						break;
					}
				}
				else
				{
					if(category.getAttribute("class").contains("non-active") == false)
					{
						BrowserActions.scrollInToView(category.findElement(By.cssSelector("div>span")), driver);
						ele = category.findElement(By.cssSelector("div>span"));
						BrowserActions.clickOnElementX(ele, driver, "");
						break;
					}
					else
					{
						Log.event("The category is already collapsed");
						break;
					}
				}
			}
		}

	}

	/**
	 * to verify if L3 is displayed below L2
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyLevel3CategoryDisplayedBelowLevel2Category()throws Exception{
		WebElement Level2 = null;
		for (WebElement category : lstLevel2Category) {
			if(category.getAttribute("class").contains("expandable active"))
			{
				Level2 = category.findElement(By.cssSelector("div>a"));
				break;
			}
		}
		WebElement Level3 = driver.findElement(By.cssSelector("#category-level-2 > li.expandable.active #category-level-3"));
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, Level2, Level3);
	}

	/**
	 * to verify if L3 is displayed below L2
	 * @param categoryName - 
	 * @return PlpPage
	 * @throws Exception - Exception
	 */
	public PlpPage clickLevel3Category(String categoryName)throws Exception{
		List<WebElement> lstLevel3Category = driver.findElements(By.cssSelector("#category-level-2 > li.expandable.active >ul > li"));
		for (WebElement category : lstLevel3Category) {
			if(category.findElement(By.cssSelector("a")).getText().trim().equalsIgnoreCase(categoryName))
			{
				BrowserActions.clickOnElementX(category.findElement(By.cssSelector("a")), driver, "");
				break;
			}
		}
		return new PlpPage(driver).get();
	}

	/**
	 * to verify if L4 is displayed below L3
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyLevel4CategoryDisplayedBelowLevel3Category()throws Exception{
		WebElement Level3 = null;
		for (WebElement category : lstLevel3Category) {
			if(category.getAttribute("class").contains("expandable active"))
			{
				Level3 = category.findElement(By.cssSelector("div>a"));
				break;
			}
		}
		WebElement Level4 = driver.findElement(By.cssSelector("#category-level-2 > li.expandable.active #category-level-3 #category-level-4 > li > a"));
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, Level3, Level4);
	}

	/**
	 * to verify if Provided L4 is displayed below provided L3
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyLevel4CategoryDisplayedBelowLevel3Category(String level3, String level4)throws Exception{
		WebElement elementLevel3 = null, elementLevel4= null;
		for (WebElement category : lstLevel3Category) {
			if(category.findElement(By.cssSelector("a")).getText().trim().equalsIgnoreCase(level3)){
				elementLevel3 = category;
				break;
			}
		}

		for (WebElement category : lstLevel4Category) {
			if(category.findElement(By.cssSelector("a")).getText().trim().equalsIgnoreCase(level4)){
				elementLevel4 = category;
				break;
			}
		}
		//WebElement Level4 = driver.findElement(By.cssSelector("#category-level-2 > li.expandable.active #category-level-3 #category-level-4 > li > a"));
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, elementLevel3, elementLevel4);
	}

	/**
	 * click L4 category
	 * @param categoryName - 
	 * @return PlpPage
	 * @throws Exception - Exception
	 */
	public PlpPage clickLevel4Category(String categoryName)throws Exception{
		for (WebElement category : lstLevel4Category) {
			if(category.findElement(By.cssSelector("a")).getText().trim().equalsIgnoreCase(categoryName))
			{
				BrowserActions.clickOnElementX(category.findElement(By.cssSelector("a")), driver, "");
				break;
			}
		}
		return new PlpPage(driver).get();
	}

	/**
	 * to verify if L3 is displayed below L2
	 * @param categoryName - 
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean isTheSelectedLevel3CategoryHighlighted(String categoryName)throws Exception{
		Boolean flag = false;
		List<WebElement> lstLevel3Category = driver.findElements(By.cssSelector("#category-level-2 > li.expandable.active >ul > li"));
		for (WebElement category : lstLevel3Category) {
			if(category.findElement(By.cssSelector("a")).getText().trim().equalsIgnoreCase(categoryName))
			{
				if(category.findElement(By.cssSelector("a")).getAttribute("class").equals("refinement-link  active"))
				{
					flag = true;
					break;
				}
				else
				{
					flag = false;
					break;
				}
			}
		}
		return flag;
	}

	/**
	 * to verify if L4 is highlighted
	 * @param categoryName - 
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean isTheSelectedLevel4CategoryHighlighted(String categoryName)throws Exception{
		for (WebElement category : lstLevel4Category) {
			if(category.findElement(By.cssSelector("a")).getText().trim().equalsIgnoreCase(categoryName)) {
				System.out.println("L4:: "+category.findElement(By.cssSelector("a")).getText());
				System.out.println("class attribute:: "+category.findElement(By.cssSelector("a")).getAttribute("class").toString());
				if(category.findElement(By.cssSelector("a")).getAttribute("class").equals("refinement-link  active"))
					return true;
			}
		}
		return false;
	}

	/**
	 * To get refinement element
	 * @param refinement -
	 * @return WebElement of given / random refinement
	 * @throws Exception -
	 */
	public WebElement getRefinementElement(String... refinement)throws Exception{
		WebElement element = null;
		if(Utils.isDesktop()) {
			if(refinement.length > 0){
				for(int i = 0; i < lstRefinementFilterOptionsDesktop.size(); i++){
					if(lstRefinementFilterOptionsDesktop.get(i).getText().toLowerCase().contains(refinement[0].toLowerCase())){
						element = lstRefinementFilterOptionsDesktop.get(i);
						break;
					}
				}
			}else{
				int rand = Utils.getRandom(0, lstRefinementFilterOptionsDesktop.size());
				element = lstRefinementFilterOptionsDesktop.get(rand);
			}
			return element;
		}else {
			if(refinement.length > 0){
				for(int i = 0; i < lstRefinementFilterOptionsMobile.size(); i++){
					if(lstRefinementFilterOptionsMobile.get(i).getText().toLowerCase().contains(refinement[0].toLowerCase())){
						element = lstRefinementFilterOptionsMobile.get(i).findElement(By.cssSelector(".toggle-heading"));
						Log.event(element.getText() + " selected.");
						BrowserActions.javascriptClick(element, driver, "Filter option");
						break;
					}
				}
			}else{
				int rand = Utils.getRandom(0, lstRefinementFilterOptionsMobile.size());
				element = lstRefinementFilterOptionsMobile.get(rand).findElement(By.cssSelector(".toggle-heading"));
				Log.event(element.getText() + " selected.");
				BrowserActions.clickOnElement(element, driver, "Filter option");
			}
			return element;
		}

	}

	/**
	 * To return the State of Refinement Section weather it is opened or closed
	 * @param refinement - Refinement Section Name
	 * @return String
	 * 			closed - Refinement is Collapsed
	 * 			opened - Refinement is Expanded
	 * @throws Exception - Exception
	 */
	public String verifyRefinementSection(String refinement)throws Exception{
		String state = new String();
		WebElement element = getRefinementElement(refinement).findElement(By.xpath("following-sibling::div"));
		if(element.getCssValue("display").equals("none"))
			state = "closed";
		else
			state = "opened";

		Log.event("Refinement State :: " + state);
		return state;
	}

	/**
	 * To get the refinement arrow mark State
	 * @param refinement - Refinement Section name
	 * @return List
	 * 			List[0] - Refinement Element
	 * 			List[1] - State - "upward" / "downward"
	 * @throws Exception - Exception
	 */
	public List<Object> getRefinementArrowState(String refinement)throws Exception{
		String state = new String();
		List<Object> obj = new ArrayList<Object>();
		WebElement element = null;
		for(int i = 0; i < lstRefinementFilterOptionsDesktop.size(); i++){
			if(lstRefinementFilterOptionsDesktop.get(i).findElement(By.cssSelector("span.toggle-heading")).getText().toLowerCase().contains(refinement.toLowerCase())){
				element = lstRefinementFilterOptionsDesktop.get(i);
				obj.add(element);
			}
		}

		if(element.findElement(By.cssSelector("i.fa.fa-angle-down.fa-2x")).isDisplayed())
			state = "downward";
		else if(element.findElement(By.cssSelector("i.fa.fa-angle-up.fa-2x")).isDisplayed())
			state = "upward";
		else
			state = "not-displayed";

		obj.add(state);
		return obj;
	}

	/**
	 * To select the Random sub refinement of given Refinement section name
	 * @param refinement - Refinement Section Name
	 * @return - String - Selected randrom Sub-refinement name
	 * @throws Exception - Exception
	 */
	public String selectRandomSubRefinementOf(String refinement)throws Exception{
		List<Object> obj = getRefinementArrowState(refinement);
		if(obj.get(1).equals("downward")){
			WebElement element = ((WebElement)obj.get(0));
			BrowserActions.clickOnElementX(element, driver, "Downward Toggle Mark of " + refinement + " ");
		}
		int rand = Utils.getRandom(0, lstExpandedRefinementAvailableSubList.size());
		BrowserActions.scrollToViewElement(lstExpandedRefinementAvailableSubList.get(rand), driver);
		BrowserActions.clickOnElementX(lstExpandedRefinementAvailableSubList.get(rand), driver, rand + "th item");
		waitUntilPlpSpinnerDisappear();
		return driver.findElements(By.cssSelector("div[class*='expended'][style*='block'] li > div.available")).get(rand).findElement(By.cssSelector("a")).getText();
	}

	/**
	 * To select the index'th sub refinement of given Refinement section name
	 * @param refinement - Refinement Section Name
	 * @param index - 
	 * @return - String - Selected randrom Sub-refinement name
	 * @throws Exception - Exception
	 */
	public String selectSubRefinementOf(String refinement, int index)throws Exception{
		List<Object> obj = getRefinementArrowState(refinement);
		if(obj.get(1).equals("downward")){
			WebElement element = ((WebElement)obj.get(0));//.findElement(By.xpath("..")).findElement(By.xpath("i.fa.fa-angle-down.fa-2x"));
			BrowserActions.clickOnElementX(element, driver, "Downward Toggle Mark of " + refinement + " ");
		}
		if(driver.findElements(By.cssSelector("div[class='expended right'][style*='block'] li > div.available span.check-square")).size() > 0)
		{
			BrowserActions.scrollToViewElement(lstExpandedRefinementAvailableSubList.get(index-1), driver);
			BrowserActions.clickOnElementX(lstExpandedRefinementAvailableSubList.get(index-1), driver, index-1 + "th item");
			waitUntilPlpSpinnerDisappear();
			return lstExpandedRefinementAvailableSubList.get(index-1).findElement(By.xpath("..")).getText().trim();
		}
		else
		{
			BrowserActions.scrollToViewElement(driver.findElements(By.cssSelector("div[class='expended-main'] li > div.available span.check-square")).get(index-1), driver);
			BrowserActions.clickOnElementX(driver.findElements(By.cssSelector("div[class='expended-main'] li > div.available span.check-square")).get(index-1), driver, index-1 + "th item");
			waitUntilPlpSpinnerDisappear();
			return driver.findElements(By.cssSelector("div[class='expended-main'] li > div.available")).get(index-1).findElement(By.cssSelector("a")).getText();
		}
	}

	/**
	 * To get the Refinement Status
	 * @param refinement - Refinement Section Name
	 * @return - String
	 * 			collapsed - Refinement is closed
	 * 			expanded  - Refinement is opened
	 * @throws Exception - Exception
	 */
	public String getRefinementStatus(String refinement)throws Exception{
		String state = new String();

		WebElement element = null;
		for(int i = 0; i < lstRefinementFilterOptionsDesktop.size(); i++){
			if(Utils.getRunBrowser(driver).equals("iexplorer")){

				if(lstRefinementFilterOptionsDesktop.get(i).getAttribute("innerHTML").toLowerCase().contains(refinement.toLowerCase())){
					element = lstRefinementFilterOptionsDesktop.get(i);
				}
			}
			else if(lstRefinementFilterOptionsDesktop.get(i).getText().toLowerCase().equals(refinement.toLowerCase()))
				element = lstRefinementFilterOptionsDesktop.get(i);
		}
		state = (element.getAttribute("class").equals("toggle"))? "collapsed" : "expanded";
		return state;
	}

	/**
	 * To Expand or Collapse the Refinement Toggle
	 * @param refinement - Refinement Name
	 * @param state - To Open or close --- 'expand'(To Open), 'collapse'(To Close)
	 * @throws Exception - Exception
	 */
	public void clickOnRefinementToggle(String refinement, String... state)throws Exception{
		WebElement element = getRefinementElement(refinement);
		WebElement toggle = element;
		String presentState = (element.getAttribute("class").contains("expanded"))? "expanded" : "collapsed";
		if(state.length > 0){
			if(presentState.contains(state[0]))
				Log.event("Refinement Already " + presentState);
			else{
				if(presentState.equals("collapsed"))
					toggle = element.findElement(By.cssSelector("i.fa.fa-angle-down.fa-2x"));
				else
					toggle = element.findElement(By.cssSelector("i.fa.fa-angle-up.fa-2x"));	
				BrowserActions.clickOnElementX(toggle, driver, refinement + " toogle icon ");
				presentState = (element.getAttribute("class").equals("toggle"))? "collapsed" : "expanded";
				Log.event("After clicking on toggle now State is :: " + presentState);
			}
		}else{
			Log.event("Present State of Toggle is :: " + presentState);
			if(presentState.equals("collapsed"))
				toggle = element.findElement(By.cssSelector("i.fa.fa-angle-down.fa-2x"));
			else
				toggle = element.findElement(By.cssSelector("i.fa.fa-angle-up.fa-2x"));	
			BrowserActions.clickOnElementX(toggle, driver, refinement + " toogle icon ");
			presentState = (element.getAttribute("class").equals("toggle"))? "collapsed" : "expanded";
			Log.event("After clicking on toggle now State is :: " + presentState);
		}

	}

	/**
	 * To get refinement list options
	 * @return list
	 * @throws Exception - Exception
	 */
	public List<String> getRefinementList() throws Exception {
		List<String> refinement = new ArrayList<>();

		for (int i = 0; i < lstRefinementFilterOptionsInMobile.size(); i++) {
			refinement.add(lstRefinementFilterOptionsInMobile.get(i).getText());
		}
		return refinement;
	}

	/**
	 * To collapse the expanded refinement status
	 * @throws Exception - Exception
	 */
	public void closeOpenedRefinement()throws Exception{
		if(Utils.waitForElement(driver, drpExpandedRefinement)) {
			WebElement element = drpExpandedRefinement.findElement(By.cssSelector("i.fa.fa-angle-up.fa-2x"));
			BrowserActions.clickOnElementX(element, driver, "Toggle mark of currently opened refinement ");
		}else {
			Log.event("There are no active expanded refinements");
		}
	}

	/**
	 * To get the Status of FilterBy menu
	 * @return String
	 * 			collapsed - FilterBy Menu is in its default State
	 * 			expanded - FilterBy Menu is opened
	 * @throws Exception - Exception
	 */
	public String getFilterByState()throws Exception{
		if(drpFilterByCollapsed.getAttribute("class").contains("refinement-mobile"))
			return "expanded";
		else
			return "collapsed";
	}

	/**
	 * To get the Status of FilterBy menu
	 * @return String
	 * 			collapsed - FilterBy Menu is in its default State
	 * 			expanded - FilterBy Menu is opened
	 * @throws Exception - Exception
	 */
	public String getSortByState()throws Exception{

		if(Utils.getRunPlatForm().equals("desktop")){
			if(drpSortByCollapsedDesktop.getAttribute("class").contains("current_item"))
				return "expanded";
			else
				return "collapsed";
		}else{
			if(drpSortByCollapsedMobile.getAttribute("class").contains("current_item"))
				return "expanded";
			else
				return "collapsed";
		}
	}

	/**
	 * To open or close the Filter By option
	 * @param state - String (expanded/collapsed)
	 * @return String
	 * 		expanded - Filter By is expanded
	 * 		collapsed - Filter By is collapsed
	 * @throws Exception - Exception
	 */
	public String openCloseFilterBy(String state)throws Exception{
		String cState = new String();

		if(getFilterByState().equals(state))
			cState = state;
		else{
			BrowserActions.javascriptClick(drpFilterByArrow, driver, "Filter By drp ");
			if(getFilterByState().equals(state))
				cState = state;
			else
				BrowserActions.javascriptClick(drpFilterByArrow, driver, "Filter By drp ");
			cState = getFilterByState();
		}
		return cState;
	}

	/**
	 * To open filter by option
	 * @throws Exception -
	 */
	public void openFilterBy()throws Exception{
		if(Utils.waitForElement(driver, drpFilterByCollapsed)){
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].setAttribute('class', 'filters refinement-mobile')", drpFilterByCollapsed);
		}

	}

	/**
	 * To Main refinement under Filter By in Table/Mobile
	 * @param refinement - Main refinement
	 * @return String
	 * 			- Selected Main refinement text
	 * @throws Exception - Exception
	 */
	public String selectRefinementInFilterBy(String... refinement)throws Exception{
		String selectedRefinement = new String();
		WebElement element = null;
		if(getFilterByState().equals("collapsed"))
			openCloseFilterBy("expanded");
		if(refinement.length > 0)
			element = getRefinementElement(refinement[0]);
		else
			element = getRefinementElement();

		BrowserActions.clickOnElementX(element, driver, "Main refinement in Filter by ");
		selectedRefinement = element.getText();
		return selectedRefinement;
	}

	/**
	 * To Main refinement under Filter By in Table/Mobile
	 * @param refinement - Main refinement
	 * @return String
	 * 			- Selected Main refinement text
	 * @throws Exception - Exception
	 */
	public String selectRefinementInFilterByWithoutBackButton(int index, String... refinement)throws Exception{
		String selectedRefinement = new String();
		WebElement element = null;
		if(getFilterByState().equals("collapsed"))
			openCloseFilterBy("expanded");
		if(refinement.length > 0)
			element = getRefinementElement(refinement[0]);
		else
			element = getRefinementElement();

		selectedRefinement = element.getText();
		WebElement elem = driver.findElement(By.cssSelector("a[data-prefn='"+refinement[0]+"']"));
		Utils.waitForElement(driver, elem);
		List<WebElement> options = driver.findElements(By.cssSelector("a[data-prefn='"+refinement[0]+"'] .check-square"));
		BrowserActions.clickOnElementX(options.get(index-1), driver, "Options in refinement");
		return selectedRefinement;
	}
	
	
	
	/**
	 * To select sub random or index'th sub refinement
	 * @param refinement - Main refinement
	 * @param index - nth Sub refinement
	 * @return - String Array
	 * 		[0] - Main refinement Selected
	 * 		[1] - Sub refinement selected
	 * @throws Exception - Exception
	 */
	public String[] selectSubRefinementInFilterBy(String refinement, int... index)throws Exception{
		String[] selectedRefinement = new String[2];
		int rand;
		WebElement element = null;
		if(getFilterByState().equals("collapsed"))
			openCloseFilterBy("expanded");
		element = getRefinementElement(refinement);

		if(getRefinementStatus(refinement).equals("collapsed")){
			BrowserActions.javascriptClick(element, driver, element + " refinement in Filter by ");
		}

		selectedRefinement[0] = element.getText();
		if(index.length > 0)
			rand = index[0]-1;
		else
			rand = Utils.getRandom(0, lstExpandedRefinementAvailableSubListTablet.size());

		BrowserActions.scrollToViewElement(lstExpandedRefinementAvailableSubListTablet.get(rand), driver);
		selectedRefinement[1] = driver.findElements(By.cssSelector(".refinement.active div[class='expended-main'] li > div.available a")).get(rand).getText().trim();
		BrowserActions.javascriptClick(lstExpandedRefinementAvailableSubListTablet.get(rand), driver, rand + "th item");
		waitUntilPlpSpinnerDisappear();

		/*if(Utils.isTablet()) {
			BrowserActions.clickOnElementX(lnkRefinementMenuBackTablet, driver, "Level back up");
		}*/
		/*if(Utils.isMobile()) {
			BrowserActions.clickOnElementX(lnkRefinementMenuBackMobile, driver, "Level back up");
		}*/

		return selectedRefinement;
	}

	/**
	 * To return to configured Main refinement list from sub refinement (Table/Mobile)
	 * @throws Exception - Exception
	 */
	public void returnToMainRefineMentInFilterBy()throws Exception{
		if(Utils.waitForElement(driver, iconReturnToMainRefinementInMobile))
			BrowserActions.clickOnElementX(iconReturnToMainRefinementInMobile, driver, "Left arror in Sub Refinement ");
	}

	/**
	 * To click on Clear all button to remove all refinements
	 * @throws Exception - Exception
	 */
	public void clickOnClearAllInRefinement()throws Exception{
		if(Utils.isDesktop())
			BrowserActions.clickOnElementX(btnClearAllInDesktop, driver, "Clear All ");
		else if(Utils.isTablet())
			BrowserActions.javascriptClick(btnClearAllInTablet, driver, "Clear All ");
		else
			BrowserActions.clickOnElementX(btnClearAllInMobile, driver, "Clear All ");
		waitUntilPlpSpinnerDisappear();
	}

	/**
	 * To get the product count in List page
	 * @return String - product count
	 * @throws Exception - Exception
	 */
	public String getProductCount()throws Exception{
		String prdCount = new String();

		if(Utils.getRunPlatForm().equals("desktop"))
			prdCount = lblRefinementCountDesktop.getAttribute("innerHTML");
		else 
			prdCount = lblRefinementCountTablet.getAttribute("innerHTML");

		Log.event("Count :: " + prdCount);
		return prdCount;
	}

	/**
	 * To click on clear in Sub Refinement menu Under Filter By
	 * @throws Exception - Exception
	 */
	public void clickOnClearInSubRefinement()throws Exception{
		if(Utils.getRunPlatForm().equals("desktop")){

		}else{
			BrowserActions.clickOnElementX(btnClearInActiveRefInTablet, driver, "Clear button ");
			waitUntilPlpSpinnerDisappear();
		}
	}

	/**
	 * To open or close the Sory By option
	 * @param state - String (expanded/collapsed)
	 * @return String
	 * 		expanded - Filter By is expanded
	 * 		collapsed - Filter By is collapsed
	 * @throws Exception - Exception
	 */
	public String openCloseSortBy(String state)throws Exception{
		String cState = new String();

		if(getSortByState().equals(state))
			cState = state;
		else{
			if(Utils.getRunPlatForm().equals("desktop"))
				BrowserActions.clickOnElementX(drpSortByCollapsedDesktop.findElement(By.cssSelector(".selected-option.selected")), driver, "Filter By drp ");
			else {
				BrowserActions.javascriptClick(drpSortSelectedOption, driver, "Filter By drp ");
			}
			cState = getSortByState();
		}
		return cState;
	}

	/**
	 * To get the element for given refinement section
	 * @param sortOption - Refinement name
	 * @return WebElement - WebElement of given Refinement Name
	 * @throws Exception - Exception
	 */
	public WebElement getSortElement(String... sortOption)throws Exception{
		WebElement element = null;
		if(sortOption.length > 0){
			for(int i = 0; i < lstSortOptionSelectable.size(); i++){
				if(lstSortOptionSelectable.get(i).getText().trim().toLowerCase().equals(sortOption[0].toLowerCase())){
					element = lstSortOptionSelectable.get(i);
					break;
				}
			}
		}else{
			int rand = Utils.getRandom(0, lstSortOptionSelectable.size());
			element = lstSortOptionSelectable.get(rand);
		}

		if(sortOption.length > 0 && element == null){
			Log.fail("Requested Sort option["+sortOption[0]+"] not available.");
		}
		return element;
	}

	/**
	 * To choose given sort option or random sort option
	 * @param sortOption - Sort Name
	 * @return - Selected sort Name
	 * @throws Exception - Exception
	 */
	public String selectSortBy(String... sortOption)throws Exception{
		String selectedSortOption = new String();

		openCloseSortBy("expanded");
		if(sortOption.length > 0){
			WebElement element = getSortElement(sortOption[0]);
			selectedSortOption = element.getAttribute("innerHTML");
			BrowserActions.javascriptClick(element, driver, "Sort Option ");			
		}
		else{
			int rand = Utils.getRandom(0, lstSortOptionSelectable.size());
			BrowserActions.javascriptClick(lstSortOptionSelectable.get(rand), driver, "Sort Option ");
			selectedSortOption = lstSortOptionSelectable.get(rand).getText();
		}
		waitUntilPlpSpinnerDisappear();
		return selectedSortOption;
	}

	/**
	 * To verify given sortby option applied
	 * @param sortedOrder -
	 * @param plpPage -
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifySortApplied(String sortedOrder, PlpPage plpPage)throws Exception{
		boolean flag = true;
		float price1,price2;
		price1=price2=0;
		List<HashMap<String, String>> prdList = plpPage.getAllProductDetails();
		switch(sortedOrder){
		case "Lowest Priced":{

			for(int i=0; i<prdList.size()-1; i++){
				if(prdList.get(i).get("Price") != null){
					if(prdList.get(i).get("Price").contains("-")){

					}else{
						try {
							price1 = Float.parseFloat(prdList.get(i).get("Price").replace("$", "").trim());
						} 
						catch (Exception e) 
						{
							if(i+2<prdList.size()-1)
								i=i+1;
							else { break;}
							try{
								price1 = Float.parseFloat(prdList.get(i).get("Price").replace("$", "").trim());
							}catch(NumberFormatException e2){
								if(prdList.get(i).get("Price").equals("N/A")){
									continue;
								}else
									price1 = Float.parseFloat(prdList.get(i).get("Price").replace("$", "").trim().split("\\-")[0]);
							}
						}
						try{
							price2 = Float.parseFloat(prdList.get(i+1).get("Price").replace("$", "").trim());
						}
						catch (Exception e) 
						{
							if(i+2<prdList.size()-1)
								i=i+2;
							else {break;}
							try{
								price2 = Float.parseFloat(prdList.get(i).get("Price").replace("$", "").trim());
							}catch(NumberFormatException e2){
								if(prdList.get(i).get("Price").equals("N/A")){
									continue;
								}else
									price2 = Float.parseFloat(prdList.get(i).get("Price").replace("$", "").trim().split("\\-")[0]);
							}
						}
						//If 1'st product price greater than 2'nd product price
						if(price1 > price2)
							flag = false;

					}
				}
			}
		}
		break;
		case "Highest Priced":{
			for(int i=0; i<prdList.size()-1; i++){
				if(prdList.get(i).get("Price") != null){
					if(prdList.get(i).get("Price").contains("-")){

					}else{

						{
							try {
								price1 = Float.parseFloat(prdList.get(i).get("Price").replace("$", "").trim());
							} 
							catch (Exception e) 
							{
								if(i+2<prdList.size()-1)
									i=i+1;
								else { break;}
								try{
									price1 = Float.parseFloat(prdList.get(i).get("Price").replace("$", "").trim());
								}catch(NumberFormatException e2){
									if(prdList.get(i).get("Price").equals("N/A")){
										continue;
									}else
										price1 = Float.parseFloat(prdList.get(i).get("Price").replace("$", "").trim().split("\\-")[0]);
								}
							}
							try{
								price2 = Float.parseFloat(prdList.get(i+1).get("Price").replace("$", "").trim());
							}
							catch (Exception e) 
							{
								if(i+2<prdList.size()-1)
									i=i+2;
								else {break;}
								try{
									price2 = Float.parseFloat(prdList.get(i).get("Price").replace("$", "").trim());
								}catch(NumberFormatException e2){
									if(prdList.get(i).get("Price").equals("N/A")){
										continue;
									}else
										price2 = Float.parseFloat(prdList.get(i).get("Price").replace("$", "").trim().split("\\-")[0]);
								}
							}
							//If 1'st product price lesser than 2'nd product price
							if(price1 < price2)
								flag = false;

						}
					}
				}
			}
		}
		break;
		default:
		}

		return flag;
	}

	/**
	 * To verify Sort Option in Breadcrumb
	 * @param sortedOrder - 
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifySortOptionInBreadCrumb(String sortedOrder)throws Exception{
		String actualSortedOrder = txtSortBreadcrumb.findElement(By.cssSelector(".breadcrumb-refinement")).getText().trim();
		if(actualSortedOrder.equalsIgnoreCase(sortedOrder))
			return true;
		else
			return false;
	}

	/**
	 * Verify that given level 2 item is on left side of page. 
	 * @param String - level2
	 * @throws Exception
	 */
	public boolean verifyLevel2PlacementOnLeft(String level2) throws Exception{
		for(WebElement ele: lstLevel2Category) {
			ele = ele.findElement(By.className("cr-section"));
			if(ele.getText().trim().equalsIgnoreCase(level2)) {
				return BrowserActions.verifyHorizontalAllignmentOfElements(driver, sectionSearchResults, ele);
			}
		}
		return false;
	}

	/**
	 * Verify that given level 3 item is on left side of page. 
	 * @param String - level3
	 * @throws Exception
	 */
	public boolean verifyLevel3PlacementOnLeft(String level3) throws Exception{
		for(WebElement ele: lstLevel3Category) {
			ele = ele.findElement(By.className("cr-section"));
			if(ele.getText().trim().equalsIgnoreCase(level3)) {
				return BrowserActions.verifyHorizontalAllignmentOfElements(driver, sectionSearchResults, ele);
			}
		}
		return false;
	}

	/**
	 * Verify that given level 3 item is on left side of page. 
	 * @param String - level3
	 * @throws Exception
	 */
	public boolean verifyLevel3Selected(String level3) throws Exception{
		for(WebElement ele: lstLevel3Category) {
			ele = ele.findElement(By.className("refinement-link"));
			if(ele.getText().trim().equalsIgnoreCase(level3)) {
				return ele.getAttribute("class").contains("active");
			}
		}
		return false;
	}

	/**
	 * Verify that given level 4 item is on left side of page. 
	 * @param String - level4
	 * @throws Exception
	 */
	public boolean verifyLevel4PlacementOnLeft(String level4) throws Exception{
		for(WebElement ele: lstLevel4Category) {
			ele = ele.findElement(By.className("refinement-link"));
			if(ele.getText().trim().equalsIgnoreCase(level4)) {
				return BrowserActions.verifyHorizontalAllignmentOfElements(driver, sectionSearchResults, ele);
			}
		}
		return false;
	}

	/**
	 * To click on arrow button on Filter refinement
	 * @throws Exception - Exception
	 */
	public void clickFilterArrowDown() throws Exception{
		if (Utils.getCurrentBrand().equals(Brand.ks)) {
			BrowserActions.javascriptClick(btnFilterArrowDown, driver, "Filter Arrow Down");
		} else {
			BrowserActions.actionClick(btnFilterArrowDown, driver, "Filter Arrow Down");
		}
	}

	/**
	 * To get list of available filter options
	 * @return List<String> - list of filter options
	 * @throws Exception - Exception
	 */
	public List<String> getListFilterOptions() throws Exception{
		List<String> filterOptions = new ArrayList<String>();
		for(WebElement filterOption: lstFilterOptions) {
			filterOptions.add(filterOption.getText().trim().toLowerCase());
		}
		return filterOptions;
	}
	
	/**
	 * To open or close color refinement options
	 * @param String - "open"/"close" to Open and Close refinement menu
	 * @throws Exception - Exception
	 */
	public void clickOpenCloseColorFilterDesktop(String state) throws Exception{
		if(state.equals("open")) {
			if(btnDownColorDesktop.isDisplayed()) {
				BrowserActions.clickOnElementX(btnDownColorDesktop, driver, "Color open arrow button");
			}else {
				Log.event("Color refinemnts already open.");
			}
		}
		if(state.equals("close")) {
			if(btnUpColorDesktop.isDisplayed()) {
				BrowserActions.clickOnElementX(btnUpColorDesktop, driver, "Color open arrow button");
			}else {
				Log.event("Color refinemnts already closed.");
			}
		}
	}
	
	/**
	 * To select a color on refinement 
	 * @param String - color to select(Optional)
	 * @throws Exception - Exception
	 */
	public void selectColorOnRefinement(String ...color) throws Exception{
		clickOpenCloseColorFilterDesktop("open");
		if(color.length == 0) {
			WebElement colorToPick = lstAvailableColors.get(Utils.getRandom(0, lstAvailableColors.size()));
			BrowserActions.clickOnElement(colorToPick, driver, "Color selected");
		}else {
			WebElement colorToPick = driver.findElement(By.cssSelector(".refinement.refinementColor li a[data-prefv='"+color[0]+"']"));
			if(Utils.waitForElement(driver, colorToPick)) {
				BrowserActions.javascriptClick(colorToPick, driver, color + " color selected");
			}else {
				selectColorOnRefinement();
			}
		}
		Utils.waitForPageLoad(driver);
		clickOpenCloseColorFilterDesktop("close");
	}
	
	/**
	 * To open or close size refinement options
	 * @param String - "open"/"close" to Open and Close refinement menu
	 * @throws Exception - Exception
	 */
	public void clickOpenCloseSizeFilterDesktop(String state) throws Exception{
		if(state.equals("open")) {
			if(btnDownSizeDesktop.isDisplayed()) {
				BrowserActions.clickOnElementX(btnDownSizeDesktop, driver, "Size open arrow button");
			}else {
				Log.event("Size refinemnts already open.");
			}
		}
		if(state.equals("close")) {
			if(btnUpSizeDesktop.isDisplayed()) {
				BrowserActions.clickOnElementX(btnUpSizeDesktop, driver, "Size open arrow button");
			}else {
				Log.event("Size refinemnts already closed.");
			}
		}
	}
	
	/**
	 * To select a size on refinement
	 * @param String - size to be selected(Optional)
	 * @throws Exception - Exception 
	 */
	public void selectSizeOnRefinement(String ...size) throws Exception{
		clickOpenCloseSizeFilterDesktop("open");
		if(size.length == 0) {
			WebElement sizeToPick = lstAvailableSizes.get(Utils.getRandom(0, lstAvailableSizes.size()));
			BrowserActions.clickOnElement(sizeToPick, driver, "Random size selected");
		}else {
			WebElement sizeToPick = driver.findElement(By.cssSelector(refinementSize+"#swatch-"+size[0].replace("/", "_").replace(" ", "_").toLowerCase()));
			BrowserActions.clickOnElement(sizeToPick, driver, size[0] + " selected");
		}
		Utils.waitForPageLoad(driver);
		clickOpenCloseSizeFilterDesktop("close");
	}
	
	/**
	 * To verify given color option is selected in refinement 
	 * @param String - color selection to verify
	 * @return boolean - true/false for if color is selected
	 * @throws Exception - Exception
	 */
	public boolean verifyColorRefinementSelected(String color) throws Exception{
		WebElement colorToVerify = driver.findElement(By.cssSelector(refinementColor+"#swatch-"+color.toLowerCase()));
		return colorToVerify.isDisplayed();
	}
	
	/**
	 * To verify given refinement is selected  
	 * @param String - refinement to verify
	 * @return boolean - true/false if given refinement is selected
	 * @throws Exception - Exception 
	 */
	public boolean verifyRefinementSelected(String refinement) throws Exception{
		for(WebElement ref: lstSelectedRefinements) {
			if(ref.getText().equalsIgnoreCase(refinement)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * To verify if given refinement is removed 
	 * @param String - refinement to verify
	 * @return boolean - true/false if given refinement is removed
	 * @throws Exception - Exception
	 */
	public boolean verifyRefinementRemoved(String refinement) throws Exception{
		for(WebElement ref: lstSelectedRefinements) {
			if(ref.getText().equalsIgnoreCase(refinement)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify remove(X) button present in all selected refinement
	 * @return boolean - true/false if X is present in all refinement
	 * @throws Exception - Exception
	 */
	public boolean verifyRefinementXButton() throws Exception{
		for(WebElement ref: lstSelectedRefinements) {
			if(!ref.findElement(By.xpath("..")).findElement(By.cssSelector("a.breadcrumb-relax")).isDisplayed()) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify the shape of remove button on refinements 
	 * @return boolean - true/false if remove button is shaped correctly.
	 * @throws Exception - Exception
	 */
	public boolean verifyRemoveButtonShape() throws Exception{
		for(WebElement ref: lstSelectedRefinements) {
			if(!ref.findElement(By.xpath("..")).findElement(By.cssSelector("a.breadcrumb-relax")).getText().trim().equalsIgnoreCase("x")) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To remove a given a refinemnent 
	 * String - refinement to remove
	 * @throws Exception - Exception
	 */
	public void removeRefinement(String refinement) throws Exception{
		for(WebElement ref: lstSelectedRefinements) {
			if(ref.getText().equalsIgnoreCase(refinement)) {
				WebElement closeRef = ref.findElement(By.xpath("..")).findElement(By.cssSelector("a.breadcrumb-relax"));
				BrowserActions.clickOnElement(closeRef, driver, "close on refinement");
				Utils.waitForPageLoad(driver);
				break;
			}
		}
	}
}
