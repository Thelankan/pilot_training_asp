package com.fbb.pages.account;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class ProfilePage extends LoadableComponent<ProfilePage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;

	String runPltfrm = Utils.getRunPlatForm();

	private static final String MyAccountPageLinksNavigation = ".navigation-links-row";

	@SuppressWarnings("unused")
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader
	.getInstance("checkout");
	// ================================================================================
	// WebElements Declaration Start
	// ================================================================================

	@FindBy(css = ".pt_account.profile")
	WebElement readyElement;
	
	@FindBy(css = ".main")
	WebElement mainHeading;

	@FindBy(css = "#dwfrm_profile_customer_firstname")
	WebElement txtFirstName;

	@FindBy(css = ".overview a")
	WebElement lnkOverview;
	
	@FindBy(css = ".account-overview-heading")
	WebElement divOverviewHeading;

	@FindBy(css = "div.secondary-navigation-links > ul.navigation-links-row")
	WebElement lstAccountMenu;

	@FindBy(css = "#secondary h1.heading.update-value.hide-desktop.hide-tablet")
	WebElement lblOverView;

	@FindBy(css = "#dwfrm_profile_customer_lastname")
	WebElement txtLastName;

	@FindBy(css = "#dwfrm_profile_customer_email")
	WebElement txtEmail;

	@FindBy(css = "#dwfrm_profile_customer_emailconfirm")
	WebElement txtConfirmEmail;

	@FindBy(css = "input[id*='dwfrm_profile_login_password_']")
	WebElement txtPassword;

	@FindBy(css = "input[id*='dwfrm_profile_login_currentpassword_']")
	WebElement txtChangePassword;
	
	@FindBy(css = "input[id*='dwfrm_profile_login_newpassword_']")
	WebElement txtChangeNewPassword;
	
	@FindBy(css = "input[id*='dwfrm_profile_login_newpasswordconfirm_']")
	WebElement txtChangeConfirmNewPassword;
	
	@FindBy(css = ".breadcrumb-element.current-element")
	WebElement breadcrumbCurrentElement;

	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop")
	WebElement breadcrumbMobile;

	@FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
	WebElement breadcrumbArrowMobile;

	@FindBy(css = "#dwfrm_profile_customer_phone")
	WebElement txtPhoneNo;

	@FindBy(css = ".birthMonth .field-wrapper .custom-select")
	WebElement drpBirthMonth;

	@FindBy(css = MyAccountPageLinksNavigation + " .profile>a[style*='cursor']")
	WebElement lnkProfileAfterNavigate;

	@FindBy(css = "div[class='selected-option selected']")
	WebElement selectedMonth;

	@FindBy(xpath = "//select[@id='dwfrm_profile_customer_birthMonth']//following-sibling::div[@class='selected-option selected']")
	WebElement selectedMonth1;

	@FindBy(css = ".form-row.birthMonth.show-text.field-valid .selection-list li:not([class='selected'])")
	List<WebElement> lstMonthSelectable;

	@FindBy(css = ".profile-info h1")
	WebElement lblProfileHeading;

	@FindBy(css = "button[name = 'dwfrm_profile_updateprofile']")
	WebElement btnUpdateInfo;

	@FindBy(css = ".profile-info>h1")
	WebElement profileHeading;

	@FindBy(css = ".profile-info>h2")
	WebElement profileSubHeading;

	@FindBy(css = "#RegistrationForm>fieldset>legend")
	WebElement PersonalInformationHeading;

	@FindBy(css = ".form-row.firstname.required.show-text .input-focus")
	WebElement firstNamePlaceHolderText;

	@FindBy(css = ".form-row.lastname.required.show-text .input-focus")
	WebElement lastNamePlaceHolderText;

	@FindBy(css = ".form-row.email.required.show-text .input-focus")
	WebElement emailPlaceHolderText;

	@FindBy(css = ".form-row.emailconfirm.required.show-text .input-focus")
	WebElement confirmEmailPlaceHolderText;

	@FindBy(css = "#dwfrm_profile_customer_email-error")
	WebElement emailAddressError;

	@FindBy(css = "#dwfrm_profile_customer_emailconfirm-error")
	WebElement confirmEmailAddressError;  
	
	@FindBy(css = "span[id*='dwfrm_profile_login_password_']")
	WebElement passwordError;

	@FindBy(css = ".form-row.password.required .input-focus")
	WebElement pwdPlaceHolderText;

	@FindBy(css = ".newpasswordconfirm .input-focus .label-text")
	WebElement confirmPwdPlaceHolderText;

	@FindBy(css = ".form-row.password.required .form-caption")
	WebElement passwordCaption;

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(css = ".form-row.phone.show-text .input-focus")
	WebElement phoneNoPlaceHolderText;

	@FindBy(css = "#dwfrm_profile_customer_phone-error")
	WebElement phoneNoError;  

	@FindBy(css = ".form-row.birthMonth.show-text .input-focus")
	WebElement monthPlaceHolderText;

	@FindBy(css = "#RegistrationForm > fieldset:nth-child(2) > div.form-row.form-row-button > button")
	WebElement btnUpdateInformation;


	// ================================================================================
	// WebElements Declaration End
	// ================================================================================

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public ProfilePage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("My Account page is not loaded", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	public boolean getPageLoadStatus() throws Exception {
		return isPageLoaded;
	}

	public String typeOnFirstName() throws Exception {
		BrowserActions.scrollToViewElement(txtFirstName, driver);
		BrowserActions.typeOnTextField(txtFirstName,
				RandomStringUtils.randomAlphabetic(5), driver, "First Name");
		return BrowserActions.getText(driver, txtFirstName, "First Name");
	}
	/**
	 * To verify the navigate to update overview
	 * @return Myaccount obejct 
	 * @throws Exception - Exception
	 */
	public MyAccountPage navigateToUpdateOverview() throws Exception{
		if(runPltfrm.equals("mobile")) {
			expandCollapseOverview("open");
		}
		BrowserActions.clickOnElementX(lnkOverview, driver, "Profile link");
		Utils.waitForPageLoad(driver);
		return new MyAccountPage(driver).get();
	}
	/**
	 * To verify the expand and collpase overview
	 * @param state - 
	 * @throws Exception - Exception
	 */
	public void expandCollapseOverview(String state)throws Exception{
		if(runPltfrm.equals("mobile")) {
			if(state.equals("open") && Utils.waitForElement(driver, lstAccountMenu)){
				Log.event("My Account Overview Menu " + state + "ed");
			} else {
				BrowserActions.clickOnElementX(lblOverView, driver, "My Account Overview Menu Link");
				Log.event("Clicked On Overview Link");
			}
		}
	}

	/**
	 * To verify type last name
	 * @return string - 
	 * @throws Exception - Exception
	 */
	public String typeOnLastName() throws Exception {
		BrowserActions.scrollToViewElement(txtLastName, driver);
		BrowserActions.typeOnTextField(txtLastName,
				RandomStringUtils.randomAlphabetic(3), driver, "Last Name");
		return BrowserActions.getText(driver, txtLastName, "Last Name");
	}
	/**
	 * To verify type email address
	 * @param email - 
	 * @throws Exception - 
	 */
	public void typeOnEmailAddress(String email) throws Exception {
		BrowserActions.scrollToViewElement(txtEmail, driver);
		txtEmail.clear();
		BrowserActions.typeOnTextField(txtEmail, email, driver, "Email address");
	}
	/**
	 * To verify type confirmation email address
	 * @param confirmEmail - 
	 * @throws Exception - 
	 */
	public void typeOnConfirmEmailAddress(String confirmEmail) throws Exception {
		BrowserActions.scrollToViewElement(txtConfirmEmail, driver);
		txtConfirmEmail.clear();
		BrowserActions.typeOnTextField(txtConfirmEmail, confirmEmail, driver, "Confirm Email address");
	}
	/**
	 * To verify type pasword
	 * @param password - 
	 * @throws Exception - 
	 */
	public void typeOnPassword(String password) throws Exception {
		BrowserActions.scrollToViewElement(txtPassword, driver);
		txtPassword.clear();
		BrowserActions.clickOnElementX(txtPassword, driver, "Password");
		BrowserActions.typeOnTextField(txtPassword, password, driver, "Password");

	}
	/**
	 * To verify type OnPhoneNo
	 * @throws Exception - Exception
	 */
	public void typeOnPhoneNo() throws Exception {
		BrowserActions.scrollToViewElement(txtPhoneNo, driver);
		BrowserActions.clickOnElementX(txtPhoneNo, driver, "Phone No");
		BrowserActions.typeOnTextField(txtPhoneNo,
				"22333" + RandomStringUtils.randomNumeric(5), driver,
				"Phone No");
	}

	/**
	 * To verify invalid phone number
	 * @param phoneNo - 
	 * @throws Exception - Exception
	 */
	public void enterInvalidPhoneNo(String phoneNo) throws Exception {
		BrowserActions.scrollToViewElement(txtPhoneNo, driver);
		BrowserActions.clickOnElementX(txtPhoneNo, driver, "Phone No");
		BrowserActions.typeOnTextField(txtPhoneNo,
				phoneNo, driver,
				"Phone No");
	}
	/**
	 * To verify the click update button
	 * @return MyAccountPage - Myaccount page object
	 * @throws Exception - Exception
	 */
	public MyAccountPage clickUpdateInfoBtn() throws Exception {
		BrowserActions.scrollToViewElement(btnUpdateInfo, driver);
		BrowserActions.clickOnElementX(btnUpdateInfo, driver, "Update Information");
		Utils.waitForPageLoad(driver);
		return new MyAccountPage(driver).get();
	}
	/**
	 * To verify the get firstname
	 * @return String - First name
	 * @throws Exception - Exception
	 */
	public String getFirstName() throws Exception {
		return BrowserActions.getText(driver, txtFirstName, "First Name");
	}

	/**
	 * To verify the get getLastName
	 * @return String value
	 * @throws Exception - Exception
	 */
	public String getLastName() throws Exception {
		return BrowserActions.getText(driver, txtLastName, "Last Name");
	}
	/**
	 * Verify get profile information
	 * @return Hashmap
	 * @throws Exception - Exception
	 */
	public HashMap<String, String> getProfileInformation() throws Exception {
		HashMap<String, String> profInfo = new HashMap<String, String>();

		profInfo.put("FirstName",
				BrowserActions.getText(driver, txtFirstName, "First Name"));
		profInfo.put("LastName",
				BrowserActions.getText(driver, txtLastName, "First Name"));
		profInfo.put("Email",
				BrowserActions.getText(driver, txtEmail, "First Name"));
		profInfo.put("ConfirmEmail",
				BrowserActions.getText(driver, txtConfirmEmail, "First Name"));
		return profInfo;
	}
	/**
	 * To fill profile information
	 * @throws Exception - Exception
	 */
	public void fillProfileInformation() throws Exception {
		typeOnFirstName();
		typeOnLastName();
	}
	/**
	 * To verify enter first name.
	 * @return status
	 * @throws Exception - Exception
	 */
	public boolean verifyEnteredFirstNameIsCorrect() throws Exception {

		boolean status = false;
		BrowserActions.scrollToViewElement(txtFirstName, driver);
		String firstNameToBeEntered = RandomStringUtils.randomAlphabetic(5);
		BrowserActions.typeOnTextField(txtFirstName, firstNameToBeEntered,
				driver, "First Name");
		String firstNameEntered = BrowserActions.getText(driver, txtFirstName,
				"First Name");

		if (firstNameToBeEntered.equals(firstNameEntered)) {
			status = true;
		}

		return status;
	}
	/**
	 * To verify firstname is correct
	 * @param firstName - 
	 * @return Boolean status
	 * @throws Exception - Exception
	 */
	public boolean verifyFirstNameIsCorrect(String firstName) throws Exception {

		boolean status = false;
		BrowserActions.scrollToViewElement(txtFirstName, driver);
		String firstNameEntered = BrowserActions.getText(driver, txtFirstName,
				"First Name");

		if (firstName.equals(firstNameEntered)) {
			status = true;
		}

		return status;
	}
	/**
	 * To verify firstname is correct
	 * @param lastName - 
	 * @return Boolean status
	 * @throws Exception - Exception
	 */
	public boolean verifyLastNameIsCorrect(String lastName) throws Exception {

		boolean status = false;
		BrowserActions.scrollToViewElement(txtLastName, driver);
		String firstNameEntered = BrowserActions.getText(driver, txtLastName,
				"Last Name");

		if (lastName.equals(firstNameEntered)) {
			status = true;
		}

		return status;
	}
	/**
	 * To verify verifyEntered LastName IsCorrect
	 * @return Boolean status
	 * @throws Exception - Exception
	 */
	public boolean verifyEnteredLastNameIsCorrect() throws Exception {

		boolean status = false;
		BrowserActions.scrollToViewElement(txtLastName, driver);
		String lastNameToBeEntered = RandomStringUtils.randomAlphabetic(5);
		BrowserActions.typeOnTextField(txtLastName, lastNameToBeEntered,
				driver, "Last Name");
		String lastNameEntered = BrowserActions.getText(driver, txtLastName,
				"Last Name");

		if (lastNameToBeEntered.equals(lastNameEntered)) {
			status = true;
		}

		return status;
	}
	/**
	 * To verify verify Entered address IsCorrect
	 * @param email - 
	 * @return Boolean status
	 * @throws Exception - Exception
	 */
	public boolean verifyEmailAddressIsCorrect(String email) throws Exception {
		boolean status = false;

		BrowserActions.scrollToViewElement(txtEmail, driver);
		BrowserActions.typeOnTextField(txtEmail, email, driver, "Email Address");

		String emailEntered = BrowserActions.getText(driver, txtEmail, "Email Address");

		if (email.equals(emailEntered)) {
			status = true;
		}

		return status;
	}/**
	 * To verify verify Entered confirm email IsCorrect
	 * @param confirmEmail - 
	 * @return Boolean status
	 * @throws Exception - Exception
	 */

	public boolean verifyConfirmEmailAddressIsCorrect(String confirmEmail)
			throws Exception {
		boolean status = false;

		BrowserActions.scrollToViewElement(txtConfirmEmail, driver);
		BrowserActions.typeOnTextField(txtConfirmEmail, confirmEmail, driver,
				"Confirm Email Address");

		String confirmEmailEntered = BrowserActions.getText(driver,
				txtConfirmEmail, "Confirm Email Address");

		if (confirmEmail.equals(confirmEmailEntered)) {
			status = true;
		}

		return status;
	}
	/**
	 * To verify verify Entered email IsCorrect
	 * @return Boolean status
	 * @throws Exception - Exception
	 */
	public boolean verifyEmailAndConfirmEmailSame() throws Exception {
		boolean status = false;

		String emailEntered = BrowserActions.getText(driver, txtEmail,
				"Email Address");
		String confirmEmailEntered = BrowserActions.getText(driver,
				txtConfirmEmail, "Confirm Email Address");

		if (emailEntered.equals(confirmEmailEntered)) {
			status = true;
		}

		return status;
	}
	/**
	 * To verify get email address
	 * @return String Value
	 * @throws Exception - Exception
	 */
	public String getEmailAddress() throws Exception {
		return BrowserActions.getText(driver, txtEmail, "Email Address");
	}

	/**
	 * To verify get email address
	 * @return String Value
	 * @throws Exception - Exception
	 */
	public String getConfirmEmailAddress() throws Exception {

		return BrowserActions.getText(driver, txtConfirmEmail,
				"Confirm Email Address");
	}
	/**
	 * To verify the password mask
	 * @param type - 
	 * @return status boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPasswordisMaskedorUnmasked(String type)throws Exception {
		Boolean flag = true;
		if(type.equals("mask"))
		{if(txtPassword.getAttribute("type").equals("password"))
			flag=true;}
		if(type.equals("unmask"))
		{
			if(txtPassword.getAttribute("type").equals("text"))
				flag=true;
		}
		return flag;
	}
	/**
	 * To verify the password unmask
	 * @param type - 
	 * @return status boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyConfirmPasswordisMaskedorUnmasked(String type)throws Exception {
		Boolean flag = true;
		if(type.equals("mask"))
		{if(txtChangeConfirmNewPassword.getAttribute("type").equals("password"))
			flag=true;}
		if(type.equals("unmask"))
		{
			if(txtChangeConfirmNewPassword.getAttribute("type").equals("text"))
				flag=true;
		}
		return flag;
	}


	/**
	 * Verify the confirm password is same 
	 * @return boolean value - 
	 * @throws Exception - Exception
	 */
	public Boolean verifyPwdConfirmPwdSame()throws Exception {
		String pwd =  BrowserActions.getText(driver, txtChangePassword, "Password");
		String confirmPwd =  BrowserActions.getText(driver, txtChangeConfirmNewPassword, "Confirm Password");
		Log.message("pwd:: "+pwd+" ::confirmPwd:: "+confirmPwd);
		if(pwd.equals(confirmPwd)){
			return true;
		}
		return false;
	}
	/**
	 * Verify get breadcrum text
	 * @param runMode - 
	 * @return List of string
	 * @throws InterruptedException - 
	 */
	public List<String> getTextInBreadcrumb(String... runMode)
			throws InterruptedException {
		List<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0),
				driver);
		if (runPltfrm == "desktop" || (runMode.length > 0) || runPltfrm == "tablet") {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;
	}
	/**
	 * To verify the click on breadcrum value
	 * @param index - 
	 * @throws Exception - Exception
	 */
	public void clickOnBreakCrumbValue(int index) throws Exception{
		BrowserActions.clickOnElementX(lstTxtProductInBreadcrumb.get(index), driver, "Breadcrumb");
	}
	/**
	 * To verify the phone number length
	 * @return Interger value
	 * @throws Exception - Exception
	 */
	public int getPhoneNoLength() throws Exception{
		String phoneNo =  BrowserActions.getText(driver, txtPhoneNo,
				"Phone No.");
		return phoneNo.length();
	}
	/**
	 * To verify the Enter phone number
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyEnteredPhoneNo()throws Exception {
		Boolean flag = false;

		String phoneNoToBeEntered = "22333" + RandomStringUtils.randomNumeric(5);

		BrowserActions.typeOnTextField(txtPhoneNo, phoneNoToBeEntered, driver,
				"Phone No");

		String phoneNoEntered =  BrowserActions.getText(driver, txtPhoneNo,
				"Phone No");

		if(phoneNoToBeEntered.equals(phoneNoEntered)){
			flag = true;
		}
		return flag;
	}
	/**
	 * To verify select birth month
	 * @param birthMonth - 
	 * @return String value
	 * @throws Exception - Exception
	 */
	public String selectBirthMonth(String... birthMonth) throws Exception {
		if (Utils.waitForElement(driver, drpBirthMonth)) {
			BrowserActions.scrollToViewElement(drpBirthMonth, driver);
			Select monthSelect = new Select(drpBirthMonth);
			if (birthMonth.length > 0) {
				if (monthSelect.getAllSelectedOptions().get(0).getText().equals(birthMonth[0]) || drpBirthMonth.getText().equals(birthMonth[0]))
					return birthMonth[0];
			}
			if (birthMonth.length > 0)
				monthSelect.selectByVisibleText(birthMonth[0].trim());
			else
				monthSelect.selectByIndex(4);

			return drpBirthMonth.getAttribute("value") == null ? drpBirthMonth.getText() : drpBirthMonth.getAttribute("value");
		}

		if (birthMonth.length > 0)
			if (selectedMonth.getText().trim().equals(birthMonth[0]))
				return selectedMonth.getText().trim();

		((JavascriptExecutor) driver).executeScript("arguments[0].style.opacity=1", selectedMonth);
		BrowserActions.clickOnElementX(selectedMonth, driver, "Birth Month dropdown");
		Log.event("Clicked on birth month Dropdown");
		BrowserActions.scrollToViewElement(selectedMonth, driver);


		if (birthMonth.length > 0) {
			if (Utils.waitForElement(driver,
					driver.findElement(
							By.xpath("//ul[@class='selection-list']//li[contains(text(),'"
									+ birthMonth[0] + "')]")))) {
				WebElement qtyToSelect = driver.findElement(
						By.xpath("//ul[@class='selection-list']//li[contains(text(),'" + birthMonth[0]
								+ "')]"));
				BrowserActions.clickOnElementX(qtyToSelect, driver, "Quantity ");
				Log.event("Clicked on Expected Quantity...");
				BrowserActions.scrollToViewElement(selectedMonth, driver);
				return birthMonth[0];
			} else {
				Log.failsoft("--->>>Given Quantity Not Available...", driver);
				return selectedMonth.getText();
			}
		} else {
			int rand = Utils.getRandom(0, lstMonthSelectable.size());
			BrowserActions.clickOnElementX(lstMonthSelectable.get(rand), driver, "Quantity menu ");
			Log.message("Clicked on random Quantity...");
			BrowserActions.scrollToViewElement(selectedMonth, driver);
			return selectedMonth1.getText();
		}
	}
	/**
	 * To verify the enter password
	 * @param password - 
	 * @throws Exception - Exception
	 */
	public void enterPassword(String password)throws Exception{
		BrowserActions.typeOnTextField(txtPassword, password, driver,
				"Password");
	}
	
	/**
	 * To enter new password
	 * @param password - 
	 * @throws Exception - Exception
	 */
	public void enterNewPassword(String password)throws Exception{
		BrowserActions.typeOnTextField(txtChangePassword, password, driver,
				"Password");
	}
	/**
	 * To verify the enter confirm password
	 * @param confirmPassword - 
	 * @throws Exception - Exception
	 */
	public void enterConfirmPassword(String confirmPassword)throws Exception{
		BrowserActions.typeOnTextField(txtChangeConfirmNewPassword, confirmPassword, driver,
				"Confirm Password");
	}
	/**
	 * To verify get breadcrum text
	 * @return string value
	 * @throws Exception - Exception
	 */
	public String getTextInBreadcrumbMobile()
			throws Exception {

		BrowserActions.scrollToViewElement(breadcrumbMobile, driver);
		String breadcrumbText = BrowserActions.getText(driver, breadcrumbMobile, "Breadcrumb in mobile");
		return breadcrumbText;
	}
	/**
	 * To verify the click breadcrum mobile
	 * @throws Exception - Exception
	 */
	public void clickOnBreadcrumbMobile() throws Exception{
		BrowserActions.clickOnElementX(breadcrumbArrowMobile, driver, "Breadcrumb");
	}
	/**
	 * To verify the click update info button
	 * @throws Exception - Exception
	 */
	public void clickOnUpdateInfoBtn() throws Exception{
		BrowserActions.clickOnElementX(btnUpdateInformation, driver, "Update Information button");
	}

	/**
	 * To verify the type confirm password
	 * @param password - 
	 * @throws Exception - Exception
	 */
	public void typeOnConfirmPassword(String password) throws Exception {		
		BrowserActions.scrollToViewElement(txtChangeConfirmNewPassword, driver);
		BrowserActions.clickOnElementX(txtChangeConfirmNewPassword, driver, "Confirm Password");
		BrowserActions.typeOnTextField(txtChangeConfirmNewPassword, password, driver, "Confirm Password");
	}
	
	/**
	 * To verify the type password error
	 * @param String - expected error message 
	 * @throws Exception - Exception
	 * @return boolean
	 */
	public boolean verifyPasswordError(String expectedErrorMessage) throws Exception{
		return(passwordError.getText().trim().equalsIgnoreCase(expectedErrorMessage));
	}

}
