package com.fbb.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class PlpPage extends LoadableComponent<PlpPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();
	public Refinements refinements;
	String runBrowser;

	/**********************************************************************************************
	 ********************************* WebElements of PLP Page ****************************
	 **********************************************************************************************/

	private static final String SIZE = "div.refinementSize";
	private static final String BRAND = "div[class='refinement brand ']";
	private static final String LNK_VIEW_MORE_LESS = ".view-more.hide-desktop";

	private static final String COLOR = "div.refinementColor";


	// --------------- Header Section ---------------------- //


	//---------WebElements Mobile------------//

	@FindBy(css = ".view-more.hide-desktop > a")
	List<WebElement> btnViewMore;

	@FindBy(css = ".view-more.hide-desktop > a")
	WebElement btnViewMoreButton;

	@FindBy(css = ".result-count.hide-desktop>span:nth-child(1)")
	WebElement lblResultCountMobile;
	
	@FindBy(css = ".result-count.result-count-mobile.hide-desktop")
	WebElement lblResultCountTablet;
	//---------------------------------------//

	//----------WebElements Desktop----------//

	@FindBy(css = "div[class='result-count']>span:nth-child(1)")
	WebElement lblResultCountDesktop;

	@FindBy(css = "div[class$='hide-tablet hide-mobile'] div[class='selected-option selected']")
	WebElement sortbydropdown;
	
	@FindBy(css = "div[class$='hide-desktop'] div[class='selected-option selected']")
	WebElement sortbydropdown_Mob_Tab;

	@FindBy(css = "div[class*='hide-tablet hide-mobile'] div.selection-list li")
	List<WebElement> listSortbyOptions;

	@FindBy(css = ".hide-mobile .sort-by .selection-list")
	WebElement sortBySelectionListDesktop;

	@FindBy(css = "div[class$='current_item']")
	static WebElement sortbyForm;

	@FindBy(xpath = "//div[contains(@class,'current_item')]//li[contains(text(),'Recommended')]")
	static WebElement sortbyRecommended;

	@FindBy(xpath = "//div[contains(@class,'current_item')]//li[contains(text(),'Lowest Priced')]")
	static WebElement sortbyLowestPriced;

	@FindBy(xpath = "//div[contains(@class,'current_item')]//li[contains(text(),'Highest Priced')]")
	static WebElement sortbyHighestPriced;

	@FindBy(xpath = "//div[contains(@class,'current_item')]//li[contains(text(),'Top Rated')]")
	static WebElement sortbyTopRated;

	@FindBy(xpath = "//div[contains(@class,'current_item')]//li[contains(text(),'Best Matches')]")
	static WebElement sortbyBestMatches;

	@FindBy(css = ".sort-by.custom-select.current_item")
	static WebElement currentSortOption;

	@FindBy(css = ".hide-mobile .sort-by.custom-select .selected-option.selected")
	static WebElement selectedSortOption;

	@FindBy(css = "div[class='search-result-options hide-tablet hide-mobile']")
	static WebElement searchResultHeader;

	@FindBy(css = ".toggle")
	List<WebElement> listtoggle;

	@FindBy(css = "#main")
	WebElement divmain;

	@FindBy(css = ".category-banner-text p")
	WebElement categoryBannerText;

	@FindBy(css=".sort-by.custom-select.current_item .selection-list")
	WebElement sortListDropDownExpand;

	//---------------------------------------//
	
	@FindBy(css = ".primary-content")
	WebElement divProductTileContainer;

	@FindBy(css = ".breadcrumb-element.last-element")
	WebElement bcLastCategoryName;

	@FindBy(css = ".refinement-link.bold>span")
	WebElement toogleActiveForPrice;

	@FindBy(css = "div[id='f684e1823420731b4c27d51fa0']>div[class='promotional-message']>a")
	WebElement promoMessageShirt;
	
	@FindBy(css = "div[class='promotional-message']")
	WebElement txtProdPromoMessage;
	
	@FindBy(css = ".clearance.promotional-message")
	WebElement txtClearencePromoMessage;
	
	@FindBy(css = ".product-feature-messages")
	WebElement txtProdFeatureMsg;
	
	@FindBy(css = ".swatch-list img")
	WebElement txtSwatchList;

	@FindBy(css = ".breadcrumb-refinement")
	WebElement lblBreadcrumbRefinement;

	@FindBy(css = "div[class='promotional-message']")
	List<WebElement> lstPromoProductMsg;

	@FindBy(css = "div[class='backtorefine']")
	WebElement btnBackarrowLeftNav;

	@FindBy(css = ".pt_product-search-result.product-list-page")
	WebElement readyElement;

	@FindBy(css = "label[for='q'] span[class='hide-mobile']")
	WebElement lblSearchText;

	@FindBy(css = "label[for='q'] span[class='hide-desktop']")
	WebElement lblSearchTextMobile;

	@FindBy(css = "h3.refinement-header+div.category-refinement>ul>li>h4>a")
	public List<WebElement> lstSubCategoryRefinement;

	@FindBy(css = ".refinement-header.toggle.expanded")
	public List<WebElement> subCategoryHeader;

	@FindBy(css = "div.product-swatches>ul>li")
	List<WebElement> listColor;

	@FindBy(css = "div.product-swatches>ul>li:not([class*='hide'])>a:not([class*='un'])")
	List<WebElement> lstColor;
	
	@FindBy(css = "li:not(.two-space-tile):not(.one-space-tile) .product-tile:not([class*='set'])")
	List<WebElement> productTile;
	
	@FindBy(css = "li:not(.two-space-tile):not(.one-space-tile) .product-tile:not([class*='set']) a.name-link")
	List<WebElement> productTileLink;

	@FindBy(css = ".product-tile .name-link")
	List<WebElement> lstProductName;
	
	@FindBy(css = ".product-tile .name-link .product-name")
	WebElement txtProductName;

	@FindBy(css = "a.page-next")
	WebElement lnkNextPage;
	// --------------- Breadcrumb Section ---------------------- //

	@FindBy(className = "breadcrumb-refinement-value")
	List<WebElement> txtBreadCrumbValue;

	@FindBy(css = "span[class='breadcrumb-element breadcrumb-result-text']")
	WebElement txtSearchResultInBreadcrumb;

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(css = ".breadcrumb")
	WebElement divBreadcrumb;

	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement divCurrentBreadCrumbDesktopTablet;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop")
	WebElement divCurrentBreadCrumbMobile;
	
	@FindBy(css = ".category-banner-text .category")
	WebElement lblCategoryNameMobile;

	// --------------- Left Navigation Section ----------------- //

	@FindBy(css = "div[class='refineby-attribute']")
	WebElement sectionFilterBy;

	@FindBy(css = ".device-clear-all")
	WebElement lnkClearAll;

	// ----------- Refinement - Refinement Size ------------ //

	@FindBy(css = SIZE)
	WebElement btnsize;

	@FindBy(css = SIZE + " ul[style*='none']")
	public WebElement btnSizeExpand;

	@FindBy(css = SIZE + " i[class='toggle-plus-minus']")
	WebElement btnSizeCollapse;

	@FindBy(css = SIZE + " ul li a")
	List<WebElement> listSelectSize;

	@FindBy(css = SIZE + " ul li a i[class='toggle-check toggle-check-active']")
	List<WebElement> lstSelectedFilter;

	// ----------- Refinement ------------ //

	@FindBy(css = BRAND)
	WebElement lblBrand;

	@FindBy(css = BRAND + " ul li ")
	List<WebElement> lstBrand;

	@FindBy(css = BRAND + " ul li ul li a")
	List<WebElement> lstSubBrand;

	@FindBy(css = ".refinement")
	List<WebElement> lstRefinementFilterOptions;

	@FindBy(css = ".refinement h3")
	List<WebElement> lstRefinementFilterOptionsInMobile;

	@FindBy(css = ".refinement-heading-category .toggle-heading")
	List<WebElement> lstRefinementFilterOptionsTitle;

	@FindBy(css = BRAND + " span[class='viewmore-refinement']")
	WebElement btnBrandViewMore;

	@FindBy(css= ".refinement-heading-category.expanded.toggle")
	WebElement drpExpandedRefinement;

	@FindBy(css = "div[class='expended'][style*='block'] li")
	List<WebElement> lstExpandedRefinementAllSubList;

	@FindBy(css = ".refinement.active div[class='expended']")
	WebElement divExpandedRefinementSubList;

	@FindBy(css = "div[class='expended'][style*='block'] li > div.available")
	List<WebElement> lstExpandedRefinementAvailableSubList;

	@FindBy(css = ".refinement i.fa.fa-angle-down.fa-2x")
	List<WebElement> lstRefinementDownArrow;

	@FindBy(css = ".search-result-options.hide-tablet.hide-mobile .result-count")
	List<WebElement> lblRefinementCountDesktop;

	// -------------- Result product section ------------------ //

	@FindBy(css = "#results-products")
	WebElement txtProductFound;

	// ---------------- Search Result Filter Section ------------- //

	@FindBy(css = "div[class='search-result-options']")
	WebElement sectionSearchResult;

	@FindBy(css = ".sort-by .custom-select")
	WebElement drpSortBy;

	@FindBy(css = ".sort-by .selection-list li")
	List<WebElement> lstSortByOptions;

	@FindBy(css = "i[data-option='column']")
	WebElement toggleGrid;

	@FindBy(css = "div[class='search-result-options'] .items-per-page .custom-select")
	WebElement drpItemsPerPage;

	@FindBy(css = "div[class='custom-select current_item'] select option")
	List<WebElement> optionsInItemsPerPge;

	@FindBy(css = "div[class='search-result-options last'] .items-per-page .custom-select")
	WebElement drpItemsPerPageBottom;

	@FindBy(css = "div[class='search-result-options'] .items-per-page .selection-list li:not([class*='selected'])")
	List<WebElement> lstItemsPerPageOptions;

	@FindBy(css = "div[class='search-result-options last'] .items-per-page .selection-list li:not([class*='selected'])")
	List<WebElement> lstItemsPerPageOptionsBottom;

	// ---------------- Result Hit Section ------------- //

	@FindBy(css = ".results-hits")
	WebElement txtResultHits;

	// --------------- Pagination Section ------------- //

	@FindBy(css = ".pagination li")
	List<WebElement> lstPageNos;

	@FindBy(css = ".page-next i")
	WebElement iconNextPage;

	@FindBy(css = ".page-last i")
	WebElement iconLastPage;

	@FindBy(css = ".page-previous i")
	WebElement iconPreviousPage;

	@FindBy(css = ".page-first i")
	WebElement iconFirstPage;

	@FindBy(css = ".breadcrumb-element.last-element")
	WebElement Style;

	// -------------- Product List Section ----------- //

	@FindBy(id = "search-result-items")
	WebElement sectionSearchResults;

	@FindBy(css = "ul[id='search-result-items']>li")
	List<WebElement> lstProducts;

	@FindBy(css = ".product-swatches")
	List<WebElement> lstProductsWithColorSwatch;

	@FindBy(className = "name-link")
	List<WebElement> lstProductNames;

	@FindBy(css = ".thumb-link>picture>img")
	List<WebElement> lstProductImages;
	
	@FindBy(css = ".product-image img")
	WebElement txtProductImages;

	@FindBy(id = "quickviewbutton")
	WebElement btnQuickView;

	// ---------- Spinner ------------ //


	@FindBy(css = ".loader[style*='block']")
	WebElement plpSpinner;

	@FindBy(css = ".filterby-refinement.hide-desktop>a")
	WebElement btnFilterByMobile;

	@FindBy(css = "#category-level-1 li a")
	List<WebElement> lstCatagory;

	@FindBy(css = "div[id='secondary'] div[class='close_menu hide-desktop'] i")
	WebElement iconCloseMenu;

	@FindBy(css = "div[id='secondary'] div[class*='refinement refinementSize'] span")
	WebElement txtSize;

	@FindBy(css = "div.size>h3>i")
	WebElement btnSizetoggle;

	@FindBy(css = ".selected>a>span:nth-child(2)")
	WebElement getBrandName;

	@FindBy(css = ".refinement-header")
	List<WebElement> lstCategoryRefinement;

	@FindBy(css = ".refinement-header")
	WebElement textCatagoryrefinement;

	@FindBy(css = ".breadcrum-device.hide-desktop>a")
	WebElement lnkBreadcrumbInmobile;

	@FindBy(css = "div[class='search-result-options last']>div[class='backtotop']>a")
	WebElement txtBacktoTop;

	@FindBy(css = "div[class*='last']>span i[data-option='column']")
	WebElement toggleListBottom;

	@FindBy(css = "div[class*='last']>span i[data-option='column']")
	WebElement toggleGridBottom;

	@FindBy(css = "ul[id='search-result-items']>li")
	public List<WebElement> totalProducts;

	@FindBy(css = ".name-link")
	List<WebElement> lstBrandName;

	@FindBy(xpath = ".//*[@id='d4b4cd10e98331ab08749eeeff']/div[1]/div[1]/a[1]/picture/img")
	WebElement imgProductPlp;

	@FindBy(css = "div[id='wrapper']>div[id='main']>div[class='refinements']")
	WebElement leftNavigationMobile;

	@FindBy(css = COLOR)
	WebElement btnColor;

	@FindBy(css = COLOR + " i[class='toggle-plus-minus']")
	WebElement btnColorCollapse;

	@FindBy(css = "div.refinementColor> ul li a>i")
	List<WebElement> listSelectColor;

	@FindBy(css = COLOR + " ul li a i[class='toggle-check toggle-check-active']")
	List<WebElement> lstSelectedFilterColor;

	@FindBy(css = "div.refinementColor> ul>.unselectable.hide")
	WebElement UnSelectable;

	@FindBy(css = ".refinementColor >ul>.view-more-less>.viewmore-refinement")
	WebElement viewMoreColors;

	@FindBy(css = ".product-name>a[title='Go to Product: Petite Juliana Rail Straight Jeans']")
	WebElement productBrand1;

	@FindBy(css = ".product-name>a[title='Go to Product: Petite Size Colored Denim Jeans']")
	WebElement productbrand2;

	@FindBy(css = "#secondary > div.refinement.brand > ul > li:nth-child(3)")
	WebElement Brand2;

	@FindBy(css = "#secondary > div.refinement.brand > ul > li:nth-child(4) > ul > li > a > i")
	WebElement Brand2Product;

	@FindBy(css = BRAND + " i[class='toggle-plus-minus']")
	WebElement btnBrandCollapse;

	@FindBy(css = "#secondary > div.refinement.brand > h3 > i")
	WebElement brandToggleMobile;

	@FindBy(css = ".page-3")
	WebElement page3;

	@FindBy(css = ".toggle-grid.hide-mobile >i[data-option='wide']")
	WebElement listView;

	@FindBy(css = SIZE + " ul li a i[class='toggle-check toggle-check-active']")
	WebElement FirstSelectedFilter;

	@FindBy(css = "div[id='secondary']")
	WebElement leftNavPanel;

	@FindBy(css = ".toggle-grid>i")
	WebElement fldToggleView;

	@FindBy(css = "#secondary .refinement,div[class='filterby-refinement hide-desktop']")
	List<WebElement> lstLeftNavRefinement;

	@FindBy(css = "a.thumb-link>picture>img")
	List<WebElement> lstImgPrimary;

	@FindBy(css = ".breadcrumb-refinement-value>span")
	WebElement filteredValue;

	@FindBy(css = "div.breadcrumb.hide-mobile > span.breadcrumb-refinement > span:nth-child(3) > span")
	WebElement filteredSecondValue;

	@FindBy(css = "#category-level-1>li:nth-child(1)>h4>a")
	WebElement pajamaSets;

	@FindBy(css = "#category-level-1>li:nth-child(1)>h4>a>i:nth-child(1)")
	WebElement arrowInPajamaSets;

	@FindBy(css = "#category-level-1>li[class='expandable toggle-down']")
	WebElement expandanbleToggle;

	@FindBy(css = ".refinement-header.toggle ")
	List<WebElement> lstCategoryRefinementToggle;

	@FindBy(css = "div ul li a i[class='toggle-check toggle-check-active']")
	List<WebElement> chkRefinMentActive;

	@FindBy(css = "div[id='secondary'] div[class='close_menu hide-desktop'] i")
	WebElement IconCloseMenu;

	@FindBy(css = ".loader[style*='block']")
	WebElement searchResultsSpinner;

	@FindBy(css = ".loader[style*='block']")
	WebElement Spinner;

	@FindBy(css = "#main")
	WebElement pageSource;

	@FindBy(css = ".grid-clm>p>a>img")
	WebElement ImgBanner;

	@FindBy(css = "#secondary > div.refinement.brand > ul > li:nth-child(1)")
	WebElement Brand1;

	@FindBy(css = "#secondary > div.refinement.brand > ul > li:nth-child(2) > ul > li:nth-child(1) > a > i")
	WebElement Brand1Product;

	@FindBy(css = ".toggle-grid.hide-mobile >i[data-option='wide']")
	WebElement toggleList;

	@FindBy(css = "span.toggle-grid:nth-child(6) > i[data-option='column']")
	WebElement girdView;

	@FindBy(css = ".shipping-promotion")
	WebElement txtFreeShipping;

	@FindBy(css = "div[id='0045a0589b561f73b470378f9e'] div[class='product-promo']")
	WebElement promoMessage;

	@FindBy(css = ".toggle-grid")
	WebElement toggleViewForMobile;

	@FindBy(css = ".toggle-grid.hide-mobile")
	WebElement toggleViewForDesktop;
	
	@FindBy(css = "div[class='b_product_badge'] img")
	WebElement imgProductBadge;

	@FindBy(css = ".product-swatches-all.product-swatches-all-desktop.product-swatches-all-mobile")
	List<WebElement> lnkViewAllcolors;

	@FindBy(css = ".swatch-list")
	List<WebElement> swatchRowColors;

	@FindBy(css = ".product-standard-price")
	List<WebElement> lstOriginalPrice_Standard_Price;

	@FindBy(css = ".product-sales-price")
	List<WebElement> lstNowPrice_Sale_Price;
	
	@FindBy(css = ".product-pricing")
	List<WebElement> txtProdPrice;
	
	@FindBy(css = ".product-pricing")
	WebElement txtSingleProdPrice;

	@FindBy(css = ".search-result-options .filterby-refinement.hide-desktop .toggle-grid>i[data-option='column']")
	WebElement gridViewMobile;

	@FindBy(css = ".search-result-options .filterby-refinement.hide-desktop .toggle-grid>i[data-option='wide']")
	WebElement listViewMobile;

	@FindBy(css = "div[class='search-result-options'] .current-page")
	WebElement lblCurrentPage;

	@FindBy(css = "div[class='product-badge']")
	List<WebElement> txtProductBadge;

	@FindBy(css = ".brand>ul>li")
	List<WebElement> btnBrandAlphabetToggle;

	@FindBy(css = ".refinement.brand>ul>li>ul>li>a>.toggle-check")
	List<WebElement> chkBrandAlphabet;

	// --------------- Pagination Section ------------- //
	@FindBy(css = ".toggle-grid.hide-mobile+div>ul>li.first-last>a[class='page-next']>i")
	WebElement iconNextPageHeader;

	@FindBy(css = ".toggle-grid.hide-mobile+div>ul>li.first-last>a[class='page-last']>i")
	WebElement iconLastPageHeader;

	@FindBy(css = ".toggle-grid.hide-mobile+div>ul>li.first-last>a[class='page-previous']>i")
	WebElement iconPreviousPageHeader;

	@FindBy(css = ".toggle-grid.hide-mobile+div>ul>li.first-last>a[class='page-first']>i")
	WebElement iconFirstPageHeader;

	@FindBy(css = ".category-seo-text")
	WebElement divCategorySEOtext;

	@FindBy(css = ".content-slot.slot-grid-header")
	WebElement divContentSlotHeader;

	@FindBy(css = ".refinements.ws-sticky")
	WebElement divHorizontalRefinement;

	@FindBy(css = ".search-result-options.hide-tablet.hide-mobile .sort-by .selected-option.selected")
	WebElement drpSortBySelectedDesktop;

	@FindBy(css = ".search-result-options.hide-tablet.hide-mobile .result-count")
	WebElement txtResultCount;

	@FindBy(id = "category-level-1")
	WebElement divVerticalRefinement;

	@FindBy(css = ".product-promo-tile")
	WebElement divCategoryBody1;

	@FindBy(css = ".offer-promo-tile")
	WebElement divCategoryBody2;

	@FindBy(css = ".onespace-tile-content")
	WebElement divCategoryBody3;

	@FindBy(css = ".back-to-top")
	WebElement btnBackToTop;

	@FindBy(css = "#search-result-items>li.grid-tile ")
	List<WebElement> lstProductTile;

	@FindBy(css = ".pagination")
	WebElement paginationSection;

	@FindBy(css = "a.page-next")
	WebElement btnPaginationNext;

	@FindBy(css = ".loader-bg")
	WebElement waitLoader;
	
	@FindBy(css = ".footer-container.footer-top")
	WebElement divFooterTop;

	/**
	 * To get the product image srs
	 * 
	 * @return String
	 */
	public String getProductImageUrl() {
		return imgProductPlp.getAttribute("src");

	}

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public PlpPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);

	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("PLP Page did not open up. Site might be down.", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);
		refinements = new Refinements(driver).get();
		runBrowser  = Utils.getRunBrowser(driver);
	}

	// --------------- Header Section ---------------------- //


	/**
	 * To Scroll into ViewMore button
	 * @throws Exception - Exception
	 */
	public void scrollToViewMore()throws Exception{
		BrowserActions.scrollToViewElement(btnViewMore.get(btnViewMore.size()-1), driver);
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the count of product tiles present in this page
	 * @return list of product size
	 * @throws Exception - Exception
	 */
	public int getProductTileCount()throws Exception{
		return lstProducts.size();
	}
	
	/**
	 * To get list of product id
	 * @param noOfIdNeeded - No of product needed
	 * @return String[] - Product id array
	 * @throws Exception - Exception
	 */
	public String[] getListOfProductId(int noOfIdNeeded)throws Exception{
		String[] prdId = new String[noOfIdNeeded];
		for (int i=0;i<noOfIdNeeded;i++) {
			WebElement prdLink = productTile.get(i);
			prdId[i] = BrowserActions.getTextFromAttribute(driver, prdLink, "data-itemid", "product");
		}
		return prdId;
	}
	
	/**
	 * To get product url
	 * @param prodId - No of product urls needed
	 * @return String[] - product url array
	 * @throws Exception - Exception
	 */
	public String[] getProductUrl(int prodId)throws Exception{
		String[] prdUrl = new String[prodId];
		for (int i=0; i<prodId; i++) {
			WebElement prdLink = productTileLink.get(i);
			prdUrl[i] = BrowserActions.getTextFromAttribute(driver, prdLink, "href", "product");
		}
		return prdUrl;
	}
	

	/**
	 * To get the integer value of search count found for the navigated category
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getSearchResultCount()throws Exception{
		Log.message("Run Platform is : "+ Utils.getRunPlatForm() );
		if(Utils.getRunPlatForm().equals("desktop"))
			return Integer.parseInt(lblResultCountDesktop.getText());
		else
			return Integer.parseInt(lblResultCountMobile.getText());
	}

	/**
	 * To get refinement list options
	 * @return list
	 * @throws Exception - Exception
	 */
	public List<String> getRefinementList() throws Exception {
		List<String> refinement = new ArrayList<>();

		for (int i = 0; i < lstRefinementFilterOptionsTitle.size(); i++) {
			refinement.add(lstRefinementFilterOptionsTitle.get(i).getText().trim());
		}
		return refinement;
	}

	/**
	 * To click the view more/less link
	 * 
	 * @throws Exception - Exception
	 */
	public void clickViewMoreOrLessLnk() throws Exception {
		if(Utils.waitForElement(driver, btnViewMore.get(btnViewMore.size()-1))){
			Log.event("clicking on view more button");
			List<WebElement> viewMoreBtn = driver.findElements(By.cssSelector(LNK_VIEW_MORE_LESS));
			BrowserActions.javascriptClick(viewMoreBtn.get(viewMoreBtn.size()-1), driver, "click on the Tab");
			waitUntilPlpSpinnerDisappear();	
			Utils.waitForPageLoad(driver);
		}else{
			Log.failsoft("No More Products to View!");
		}

	}

	/**
	 * To wait for Search results spinner
	 */
	private void waitUntilPlpSpinnerDisappear() {
		Utils.waitUntilElementDisappear(driver, plpSpinner);
	}

	/**
	 * To get list of options available in sort by 
	 * @return List -
	 * @throws Exception -
	 */
	public List<String> getSortbylist() throws Exception {
		List<String> options = new ArrayList<>();

		for (int i = 0; i < listSortbyOptions.size(); i++) {
			options.add(listSortbyOptions.get(i).getAttribute("innerHTML").trim().toUpperCase());
		}
		return options;
	}

	/**
	 * To get the name for all products in product tile or search results
	 * @return List of String - List of names of all products
	 * @throws Exception - Exception
	 */
	public List<String> getProductNames()throws Exception{
		List<String> lstPrice = new ArrayList<String>();
		for(int i=0; i < lstProductName.size(); i++)
			lstPrice.add(lstProductName.get(i).getText().trim());

		return lstPrice;
	}

	/**
	 * To get all product details as list of hashmap
	 * @return List -
	 * @throws Exception -
	 */
	public List<HashMap<String, String>> getAllProductDetails()throws Exception{
		List<HashMap<String, String>> prdDetails = new ArrayList<HashMap<String, String>>();

		String productName = new String();
		String price = new String();

		int x = lstProducts.size() > 10 ? 10 : lstProducts.size();
		for(int i = 0; i < x; i++){
			HashMap<String, String> product = new HashMap<String, String>();
			try{
				productName = lstProducts.get(i).findElement(By.cssSelector("a.name-link")).getText();
			}			
			catch(NoSuchElementException | StaleElementReferenceException e) {productName=null;}
			try {
				price = lstProducts.get(i).findElement(By.cssSelector("span.product-sales-price")).getText();
			}
			catch(NoSuchElementException | StaleElementReferenceException e) {price=null;}
			product.put("Name", productName);
			product.put("Price", price);
			prdDetails.add(product);
		}
		return prdDetails;
	}

	/**
	 * to verify that the vertical refinement is displayed to the left of search results
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyVerticalRefinementDisplayedToTheLeftOfSearchResults()throws Exception{
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, sectionSearchResults, divVerticalRefinement);
	}


	/**
	 * to get the number of product tiles displayed per row in the search results  
	 * @return Int
	 * @throws Exception - Exception
	 */
	public int getNumberOfProductTilesPerRow()throws Exception{
		List<WebElement> productTiles = driver.findElements(By.cssSelector("#search-result-items>li"));
		int rowCount = 1;
		int i = 1;
		for (; i <= productTiles.size(); i++) {
			if(productTiles.get(i).getAttribute("class").contains("two-space-tile"))
			{
				for (int j = i; j <= productTiles.size(); j++) {
					if(BrowserActions.verifyElementsAreInSameRow(driver, productTiles.get(j), productTiles.get(j+1)))
						continue;
					else
					{
						i = j+ 1;
						break;
					}
				}

			}
			else
			{
				for (int j = i-1; j <= productTiles.size(); j++) {
					if(BrowserActions.verifyElementsAreInSameRow(driver, productTiles.get(j), productTiles.get(j+1)))
						rowCount++;
					else
						break;
				}
				break;
			}
		}
		return rowCount;
	}
	/**
	 * to get the breadcrumb value
	 * @return List of String 
	 * @throws Exception - Exception
	 */
	public List<String>  getBreadcrumbText()throws Exception{
		return BrowserActions.getText(lstTxtProductInBreadcrumb, "Breadcrumb value", driver);
	}


	/**
	 * to get the breadcrumb Full text
	 * 
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getBreadcrumbFullText() throws Exception {
		String appendString = "";
		List<String> breadCrumbList = getBreadcrumbText();
		for (int index = 0; index < breadCrumbList.size(); index++) {
			if (index != (breadCrumbList.size() - 1)) {
				String temp = breadCrumbList.get(index).replace("\\", "\\\\").trim();
				appendString = appendString
						+ temp.split("\\\\")[0] + "/";
			} else {
				appendString = appendString + breadCrumbList.get(index).trim();
			}
		}
		return appendString;
	}
	/**
	 * To click on Back To Top Button
	 * @throws Exception - Exception
	 */
	public void clickBackToTopButton() throws Exception{
		if (Utils.waitForElement(driver, btnBackToTop)) {
			BrowserActions.clickOnElementX(btnBackToTop, driver, "Back to Top");
			Utils.waitForPageLoad(driver);
		} else {
			Log.reference("The Back to top icon is not displaying");
		}
	}
	
	/**
	 * To click on quickshop button
	 * @param prdID -
	 * @return QuickShop -
	 * @throws Exception -
	 */
	public QuickShop clickOnQuickShop(String... prdID)throws Exception{
		WebElement prdTile = null;
		if(prdID.length > 0) {
			while(prdTile.equals(null)){
				try{
					prdTile = driver.findElement(By.cssSelector("div[data-itemid='"+prdID[0]+"']"));
				}catch(NoSuchElementException e){
					if(Utils.waitForElement(driver, btnPaginationNext))
						clickNextButtonInPagination();
					else
						Log.fail("Product Not found.", driver);
				}
			}
		}else {
			prdTile = driver.findElements(By.cssSelector(".product-tile")).get(0);
		}
		BrowserActions.scrollInToView(prdTile, driver);
		BrowserActions.mouseHover(driver, prdTile, "Product Tile");
		WebElement qcButton = prdTile.findElement(By.cssSelector(".quickview"));
		if (Utils.waitForElement(driver, qcButton)) {
			BrowserActions.javascriptClick(qcButton, driver, "Quick Shop Button for First Product");
		}
		return new QuickShop(driver).get();
	}

	/**
	 * to click the NEXT link in the pagination section
	 * @throws Exception - 
	 */
	public void clickNextButtonInPagination()throws Exception{
		BrowserActions.scrollInToView(paginationSection, driver);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", btnPaginationNext);
		Utils.waitForElement(driver, paginationSection);
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}

	/**
	 * To click on Sort by arrow
	 * @param arrow -
	 * @throws Exception -
	 */
	public void clickSortbyArrow(String arrow) throws Exception {
		BrowserActions.clickOnElementX(sortbydropdown, driver, "click on the dropdown");
	}

	/**
	 * To navigate to PDP Page.
	 * @param index - Optional
	 * @return PDP Page
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPdp(int... index)throws Exception{
		if(index.length > 0){
			if(runBrowser.equalsIgnoreCase("chrome")&&runPltfrm.equalsIgnoreCase("desktop"))
			{
				BrowserActions.javascriptClick(lstProductName.get(index[0]-1), driver, index[0] + "th Product ");
			}
			else
			{
				BrowserActions.clickOnElementX(lstProductName.get(index[0]-1), driver, index[0] + "th Product ");
			}
		}else{
			BrowserActions.clickOnElementX(lstProductName.get(Utils.getRandom(0, lstProductName.size())), driver, "Random Product ");
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}


	/**
	 * To Navigate To PDP Page of the product ID
	 * @param productID - 
	 * @return PdpPage of given Product ID
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPdp(String prdID)throws Exception{
		Log.event("Trying to Click product name link for product id :: " + prdID);

		try {
			WebElement prdLink = driver.findElement(By.cssSelector("div[data-itemid='"+ prdID.trim() +"'] .name-link .product-name"));
			BrowserActions.clickOnElementX(prdLink, driver, "Name Link for Product " + prdID + " ");
			Utils.waitForPageLoad(driver);
		}catch(NoSuchElementException e) {
			if(Utils.waitForElement(driver, btnPaginationNext)) {
				BrowserActions.clickOnElementX(btnPaginationNext, driver, "Next Page ");
				Utils.waitUntilElementDisappear(driver, waitLoader);
				return navigateToPdp(prdID);
			}
			else
				Log.fail("Product ID("+prdID+") not found.");
		}

	return new PdpPage(driver).get();
	}

	/**
	 * To click view more button in the PLP page
	 * @throws Exception -
	 */
	public void clickOnViewMore()throws Exception{
		if (Utils.waitForElement(driver, btnViewMore.get(btnViewMore.size()-1))) {
			BrowserActions.scrollToViewElement(btnViewMore.get(btnViewMore.size()-1), driver);
			BrowserActions.javascriptClick(btnViewMore.get(btnViewMore.size()-1), driver, "View more button");
			Utils.waitForPageLoad(driver);
		} else {
			Log.fail("The view more button is not found in the page!");
		}
	}

	/**
	 * To select product color in plp page
	 * @param productID -
	 * @param selectColor -
	 * @return String color details and product name
	 * @throws Exception -
	 */
	public String selectProductColorInPLP(String productID, String selectColor)throws Exception{
		WebElement prdColorLink = null;
		while (prdColorLink == null) {
			try {
				prdColorLink = driver.findElement(By.cssSelector("div[data-itemid='"+ productID.trim() +"'] .swatch-list a[title*='"+ selectColor.trim() +"']"));
				BrowserActions.clickOnElementX(prdColorLink, driver, "Product color");
				Utils.waitForPageLoad(driver);
				WebElement colorImg = prdColorLink.findElement(By.cssSelector(" img"));
				String[] colorHref = BrowserActions.getTextFromAttribute(driver, colorImg, "data-thumb", "Product Color").split("\\?")[0].split("\\/");
				return colorHref[colorHref.length-1].split("\\.")[0];
			} catch (Exception ea) {
				if (Utils.waitForElement(driver, btnViewMore.get(btnViewMore.size()-1))) {
					clickOnViewMore();
				} else {
					Log.fail("The product "+productID+" is not found in page!");
				}
			}
		}
		return null;
	}

	/**
	 * To get the banner text for the current category
	 * @return String Banner text -
	 * @throws Exception -
	 */
	public String getCategoryName() throws Exception {
		return BrowserActions.getText(driver, categoryBannerText, "Category Banner");	
	}

	/**
	 * To verify Product Badge image in Particular Product
	 * @param productID - 
	 * 
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyProdBadgeImgInParticularProd(String productID, String badgeType)throws Exception{
		Utils.waitForPageLoad(driver);
		WebElement prdLink = null;
		boolean flag = true;
		while (flag) {
			try {
				prdLink = driver.findElement(By.cssSelector("div[data-itemid='"+ productID.trim() +"']"));
				flag = false;
			} catch (Exception ee) {
				if(ee.toString().contains("NoSuchElementException")) {
					try {
						BrowserActions.clickOnElementX(lnkNextPage, driver, "Next page");
						Utils.waitForPageLoad(driver);
					} catch (Exception ex) {
						if(ex.toString().contains("NoSuchElementException")) {
							return false;
						}
					}
				}
			}
		}
		WebElement productBadge = prdLink.findElement(By.cssSelector(".b_product_badge img"));
		
		String[] badgeTypeSrc = BrowserActions.getTextFromAttribute(driver, productBadge, "src", "Product badge").split("\\/");
		
		if(badgeTypeSrc[badgeTypeSrc.length - 1].toLowerCase().contains(badgeType.toLowerCase()))
			return true;
		else
			return false;
	}

	/**
	 * To click anywhere in PLP Page
	 * @throws Exception - Exception
	 */
	public void clickAnywhereInPlpPage() throws Exception {
		Utils.waitForElement(driver, divmain);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", divmain);	
	}

	/**
	 * To verify Level-4 Is diplayed
	 * @return true - if displayed
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfLevel4Displayed() throws Exception {
		WebElement ele = driver.findElement(By.cssSelector("#category-level-2 > li.expandable.active #category-level-3 #category-level-4"));
		if(ele.isDisplayed())
			return true;
		else
			return false;
	}

	/**
	 * To verify Level-3 Is diplayed
	 * @return true - if displayed
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfLevel3Displayed() throws Exception {
		WebElement ele = driver.findElement(By.cssSelector("#category-level-2 > li.expandable.active #category-level-3"));
		if(ele.isDisplayed())
			return true;
		else
			return false;
	}

	/**
	 * To verify Level-2 Is diplayed
	 * @return true - if displayed
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfLevel2Displayed() throws Exception {
		WebElement ele = driver.findElement(By.cssSelector("#category-level-1 > li.expandable.active #category-level-2"));
		if(ele.isDisplayed())
			return true;
		else
			return false;
	}

	/**
	 * To verify the click the product by index
	 * @param int - index of product to be clicked on
	 * @return PdpPage - pdp Page object
	 * @throws Exception - Exception
	 */
	public PdpPage clickProductByIndex(int index) throws Exception {
		WebElement prdTile = driver.findElements(By.cssSelector(".product-tile a.name-link")).get(index-1);		
		BrowserActions.clickOnElementX(prdTile, driver, "click on the tiles");
		return new PdpPage(driver).get();
	}


	/**
	 * To verify the mouse hover by index
	 * @param int - index of product to hover mouse on  
	 * @throws Exception -Exception
	 */
	public void mouseHoverProductByIndex(int index) throws Exception 
	{
		WebElement prdTile = driver.findElements(By.cssSelector(".product-tile")).get(index-1);		
		BrowserActions.mouseHover(driver, prdTile);
		Log.message("Mouse hover to element");


	}	

	/**
	 * To verify the quick shop button displayed in tiles
	 * @param int - index of product to verify quick shop button
	 * @return true - if displayed
	 * @throws Exception - Exception
	 */
	public boolean verifyQuickShopButtonDisplayed(int index) throws Exception
	{
		WebElement quickOrderButton = driver.findElements(By.cssSelector(".product-tile .quickview")).get(index-1);
		if(quickOrderButton.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * To verify sort by list is hiding or not.
	 * @return true -if Hiding element is displayed. 
	 * @throws Exception -Exception
	 */
	public boolean verifySortByListIsHiding() throws Exception
	{
		WebElement hideSrtList = driver.findElements(By.cssSelector(".sort-by.custom-select .selection-list")).get(1);
		if((hideSrtList.isDisplayed()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * To verify sort by list is hiding or not.
	 * @return true -if Hiding element is displayed. 
	 * @throws Exception -Exception
	 */
	public String getCurrentSortMode() throws Exception
	{
		return selectedSortOption.getAttribute("innerHTML");
	}

	/**
	 * To get the price for given product in search result page
	 * @param String - Product ID
	 * @return String - Price of Product with provided ID
	 * @throws Exception - Exception
	 */
	public String getPriceOf(String productID)throws Exception{
		String lstPrice = new String();
		lstPrice = driver.findElement(By.cssSelector("div[data-itemid='" + productID + "'] .product-sales-price")).getText().trim();
		return lstPrice;
	}

	/**
	 * To open quick shop modal for give product id.
	 * @param String - Product ID
	 * @return QuickShop - QuickShop page object
	 * @throws Exception - Exception
	 */
	public QuickShop clickQuickShop(String productID)throws Exception{
		Log.event("Given Item ID :: " + productID.trim());
		boolean flag = true;
		do {
			try {
				WebElement prdTile = driver.findElement(By.xpath("//div[@data-itemid='"+productID+"']//div[@class='product-image']"));
				BrowserActions.scrollInToView(prdTile, driver);
				BrowserActions.mouseHover(driver, prdTile);
				WebElement quickShopButton = driver.findElement(By.xpath("//div[@data-itemid='"+productID.trim()+"']//a[@id='quickviewbutton']"));

				if(quickShopButton.isDisplayed()) {
					BrowserActions.clickOnElementX(quickShopButton, driver, "Quick Shop Button");
				}else{
					BrowserActions.javascriptClick(quickShopButton, driver, "Quick Shop Button");
				}

				return new QuickShop(driver).get();
			}catch(NoSuchElementException e) {
				if(Utils.waitForElement(driver, btnPaginationNext)) {
					clickNextButtonInPagination();
				}else {
					flag = false;
				}
			}
		}while(flag);
		Log.fail("Given Product ID not present in current category...");
		return null;
	}
	
	/**
	 * To verify contents of a product tile
	 * @param int - Index of product of verify tile(Optional)
	 * @return boolean - if tile conatins all element - true if tile has missing element - false
	 * @throws Exception - Exception
	 */
	public boolean verifyProductTileContent(int ...prodNumber) throws Exception{
		int index;
		if(prodNumber.length == 1) {
			index = prodNumber[0];
		}else {		
			Random rand = new Random();
			index = rand.nextInt(getProductTileCount());
		}
		WebElement tileElement = lstProducts.get(index);
		BrowserActions.scrollInToView(tileElement, driver);
		if(tileElement.findElement(By.cssSelector(".p-image")).isDisplayed()) {
			Log.event("Product image displayed for tile number: " + index);
		}else {
			Log.event("Product image not displayed for tile number: " + index);
			return false;
		}
		if(tileElement.findElement(By.cssSelector(".product-name")).isDisplayed()) {
			Log.event("Product name available for tile number: " + index);
		}else {
			Log.event("Product name not available for tile number: " + index);
			return false;
		}
		if(tileElement.findElement(By.cssSelector(".product-pricing")).isDisplayed()) {
			Log.event("Product price available for tile number: " + index);
		}else {
			Log.event("Product price not available for tile number: " + index);
			return false;
		}
		if(tileElement.findElement(By.cssSelector(".product-swatches")).isDisplayed()) {
			Log.event("Color swatch not available for tile number: " + index);
		}else {
			Log.event("Color swatch not available for tile number: " + index);
			return false;
		}
				
		return true;
	}
	
	/**
	 * To verify image change by color swatch
	 * @param int - product number(Optional)
	 * @return boolean - image update verification
	 * @throws Exception - Exception
	 */
	public boolean verifyProductImageUpdate(int ...prodNumber) throws Exception{
		int index;
		Random rand = new Random();
		if(prodNumber.length == 1) {
			index = prodNumber[0];
		}else {		
			index = 0;
		}
		WebElement tileElement = lstProducts.get(index);
		BrowserActions.scrollInToView(tileElement, driver);
		
		WebElement elementImage = null;
		
		String beforeURL = null;
		boolean imageFound = false;
		while (lstProducts.size() > index && !imageFound) {
			elementImage = tileElement.findElement(By.cssSelector(".p-image"));
			beforeURL = elementImage.getAttribute("src");
			Log.event("beforeURL:: "+beforeURL);
			
			if(beforeURL.toLowerCase().contains("noimage")) {
				index = index + 1;
			} else {
				imageFound = true;
			}
			
			tileElement = lstProducts.get(index);
		}
		if(beforeURL.toLowerCase().contains("noimage")) {
			Log.reference("All product in this category din have an image");
			return false;
		}
		List<WebElement> colorSwatches = tileElement.findElements(By.cssSelector(".swatch-list .swatch:not(.selected)"));
		WebElement unselectedColor = colorSwatches.get(rand.nextInt(colorSwatches.size()));
		
		BrowserActions.clickOnElementX(unselectedColor, driver, "Unselected color option");
		elementImage = tileElement.findElement(By.cssSelector(".p-image"));
		String afterURL = elementImage.getAttribute("src");
		Log.event("afterURL:: "+afterURL);
		
		return (!beforeURL.equals(afterURL));
	}
}
