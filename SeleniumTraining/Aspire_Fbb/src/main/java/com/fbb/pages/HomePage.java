package com.fbb.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.HamburgerMenu;
import com.fbb.pages.headers.Headers;
import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class HomePage extends LoadableComponent<HomePage> {

	private String appURL;
	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public MiniCartPage minicart;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();
	
	private static EnvironmentPropertiesReader envProperty = EnvironmentPropertiesReader
			.getInstance("env");

	/**********************************************************************************************
	 ********************************* WebElements of Home Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = "#header input[name='q']")
	WebElement txtSearch;

	@FindBy(css = "div.pt_storefront")
	WebElement divMain;

	@FindBy(id = "ignore_this_warning")
	WebElement overRideLink;

	@FindBy(css = "div.promo-banner")
	WebElement txtPLCCPromoDesktop;
	
	@FindBy(css = "div.promo-banner")
	WebElement txtPLCCPromoMobile;
	
	@FindBy(css = ".footer-plcc-banner .right-section .text-section p")
	WebElement txtFooterMarketing;
	
	@FindBy(css = ".breadcrumb-element")
	WebElement lnkBreadcrumbHome;
	
	@FindBy(css = ".quick-order-breadcrumb.breadcrumb")
	WebElement lnkBreadcrumbCurrentPage;
	
	@FindBy(css = ".content-slot.slot-grid-header")
	WebElement divGridHeader;
	
	@FindBy(css = ".breadcrumb")
	WebElement divBreadCrumb;
	
	@FindBy(css = ".primary-logo svg")
	WebElement divPrimaryLogo;
	
	@FindBy(css = ".home-content-container.home-content-1")
	WebElement divHomeInnerWrapper;
	
	@FindBy(xpath = "//img[contains(@alt,'Home Banner')]")
	WebElement hompagebanner;
	
	@FindBy(css = ".rm-module.rm-module-01-02-01")
	WebElement hompagebanner1;
	
	
	@FindBy(css = ".customer-service-info.info")
	WebElement CustomerService;

	@FindBy(css = ".pt_storefront")
	WebElement readyElement;

	/**********************************************************************************************
	 ********************************* WebElements of Home Page - Ends ****************************
	 **********************************************************************************************/

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 * @param url
	 *            : UAT URL
	 */

	public HomePage(WebDriver driver, String url) {
		appURL = url;
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		
	}// HomePage
	
	

	/**
	 * 
	 * @param driver
	 *            : webdriver
	 */
	public HomePage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);
	}// HomePage

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, divMain))) {
			Log.fail("Home Page did not open up. Site might be down.", driver);
		}
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);
		
		Log.event("Home Page Loaded Successfully.");
	}// isLoaded

	@Override
	protected void load() {
		if(appURL != null){
			if(envProperty.getProperty("siteProtection").equals("true")){
				/*if(Utils.getRunBrowser(driver).equalsIgnoreCase("safari")) {
					Log.event("Turning of Phishing site warning.");
					driver.get("http://a");
				}*/
				appURL = appURL.replace("http://", "https://" + envProperty.getProperty("appUserName") + ":" + envProperty.getProperty("appPassword") + "@");
				Log.event("Updated URL :: " + appURL);
				//driver.get(url);
			}
			driver.get(appURL);
			
			Log.messageT("----------------------------------------------");
			Log.messageT("Platform        :: " + Utils.toCamelCase(runPltfrm));
			Log.messageT("Device Platform :: " + Utils.toCamelCase(Utils.getRunDevicePlatform(driver)));
			Log.messageT("Browser Name    :: " + Utils.toCamelCase(Utils.getRunBrowser(driver)));
			Log.messageT("Browser Version :: " + Utils.getRunBrowserVersion(driver));
			Log.messageT("Testing Brand   :: " + Utils.getCurrentBrand().getConfiguration());
			Log.messageT("Testing Environ.:: " + Utils.getCurrentEnv());
			Log.messageT("----------------------------------------------");
		}
		//Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}// load

	/**
	 * To get the page load status
	 * @return boolean 'true' or 'false'
	 * @throws Exception - Exception
	 */
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}
	
	/**
	 * To check the primary logo in the home page is loaded with correct brand
	 * @param brandName -
	 * @return true if loaded with correct brand else false
	 * @throws Exception -
	 */
	public boolean checkPrimaryLogoBrandName(Brand brandName) throws Exception {
		String src[] = BrowserActions.getTextFromAttribute(driver, divPrimaryLogo, "src", "Primary logo").split("\\/");
		String srcBrand = src[src.length - 2]; 
		
		Brand brand = Brand.fromValue(srcBrand);
		if(brand.equals(brandName))
			return true;
		else 
			return false;
		
	}
	

	/**
	 * To get the PLCC promo text
	 * @return String - Promo Text on screen
	 * @throws Exception - Exception
	 */
	public String getPLCCPromoText()throws Exception{
		if(Utils.isDesktop() || Utils.isTablet())
			return BrowserActions.getText(driver, txtPLCCPromoDesktop, "PLLC Promotion Desktop ");
		else
			return BrowserActions.getText(driver, txtPLCCPromoMobile, "PLLC Promotion Mobile ");
	}
	
	/**
	 * To get Footer Marketing PLCC text
	 * @return String - footer PLCC text
	 * @throws Exception - Exception
	 */
	public String getFooterPLCCText()throws Exception{
		return BrowserActions.getText(driver, txtFooterMarketing, "PLLC Promotion Footer");
		
	}

	/**
	 * Navigate to my account section
	 * @return MyAccountPage
	 * @throws Exception - Exception
	 */
	public MyAccountPage navigateToMyAccount()throws Exception{
		if(runPltfrm.equals("mobile")){
			try{
			HamburgerMenu hMenu = (HamburgerMenu)headers.openCloseHamburgerMenu("open");
			return hMenu.navigateToMyAccount();
			}catch(ClassCastException e){
				Log.fail("Cannot Open up Hamburger Menu.", driver);
			}
		}else{
			return headers.navigateToMyAccount();
		}
		
		return new MyAccountPage(driver).get();
	}
	

}// HomePage
