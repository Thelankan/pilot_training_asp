package com.fbb.pages.footers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.GiftCardPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.PlatinumCardLandingPage;
import com.fbb.pages.account.CatalogPreferencePage;
import com.fbb.pages.account.PreApprovedPlatinumCardPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.customerservice.EmailSubscriptionGuest;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class Footers extends LoadableComponent<Footers> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	private static final String FOOTER_DESKTOP = "footer ";
	private static final String FOOTER_MOBILE = "footer ";
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");
	String runPltfrm = Utils.getRunPlatForm();
	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(css = ".footer-container.footer-top")
	List<WebElement> footerPanel;
	
	@FindBy(css = ".footer-container.footer-top")
	WebElement footerPanel1;

	@FindBy(css = FOOTER_DESKTOP + "input[id*='dwfrm_emailsignup_email']")
	WebElement fldEmailSignUpFooter;

	@FindBy(css = FOOTER_DESKTOP + "button[name='dwfrm_emailsignup_signup']")
	WebElement btnEmailSignUpDesktopTablet;
	
	@FindBy(css = FOOTER_DESKTOP + ".email-signup-footer-success-heading")
	WebElement lblEmailSignUpSuccessMsgDesktop;
	
	@FindBy(css = FOOTER_MOBILE + ".email-signup-footer-success-heading")
	WebElement lblEmailSignUpSuccessMsgMobile;
	
	@FindBy(css = FOOTER_MOBILE + "button[name='dwfrm_emailsignup_signup']")
	WebElement btnEmailSignUpMobile;
	
	@FindBy(css = FOOTER_DESKTOP + "span[id*='dwfrm_emailsignup_']")
	WebElement txtEmailSignUpErrorDesktop;
	
	@FindBy(css = FOOTER_MOBILE + "span[id*='dwfrm_emailsignup_email_']")
	WebElement txtEmailSignUpErrorMobile;
	
	@FindBy(xpath = "//div[@class='footer-links']//a[contains(text(),'Gift Card Balance')]")
	WebElement lnkGiftCardBalance;
	
	@FindBy(xpath = "//div[@class='footer-links']//div[@class='footer_item'][3]/ul[2]/li[2]/a")
	WebElement lnkPhysicalGiftCard;
	
	@FindBy(xpath = "//div[@class='footer-links']//div[@class='footer_item'][3]/ul[2]/li[3]/a")
	WebElement lnkEGiftCard;
	
	@FindBy(css = ".instagram>a")
	WebElement imgInstagram;
	
	@FindBy(css = ".pinterest>a")
	WebElement imgPinterest;
	
	@FindBy(css = ".facebook>a")
	WebElement imgFacebook;
	
	@FindBy(css = ".twitter>a")
	WebElement imgTwitter;
	
	@FindBy(css = ".youtube>a")
	WebElement imgYoutube;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'FAQ')]")
	WebElement lnkTopQuestions;
	
	@FindBy(css = "#bc-chat-container")
	WebElement mdlLiveChat;

	@FindBy(xpath = "//footer//a[contains(text(),'Contact Us')]")
	WebElement lnkContactUs;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a//div[contains(text(),'Live Chat')]")
	WebElement lnkLiveChat;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'Return')]")
	WebElement lnkReturnCenter;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'shipping & handling')]")
	WebElement lnkShippingAndHandling;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'International')]")
	WebElement lnkInternationalShipping;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'easy returns')]")
	WebElement lnkEasyReturns;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'order status')]")
	WebElement lnkOrderStatus;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'My Account')]")
	WebElement lnkYouraccount;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'My Orders / Order Status')]")
	WebElement lnkOrderStatusUnderAccount;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'Wishlist')]")
	WebElement lnkWishLists;
	
	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'wish list')]")
	WebElement lnkWishListsJLWW;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'Rewards')]")
	WebElement lnkRewardPoints;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'about jesicca london')]")
	WebElement lnkAboutJesiccaLondon;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'About Us')]")
	WebElement lnkAboutWomansWithin;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'about us')]")
	WebElement lnkAboutRomans;
	
	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'About Us')]")
	WebElement lnkAboutKingSize;
	
	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'About Us")
	WebElement lnkAboutEllos;

	@FindBy(css = "footer a[href*='careers']")
	WebElement lnkFullBeautyBrandCareer;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'about fullbeauty brands')]")
	WebElement lnkAboutFullBeautyBrands;
	
	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'About Fullbeauty Brands')]")
	WebElement lnkAboutFullBeautyBrandsww;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'in the community')]")
	WebElement lnkInTheCommunity;

	@FindBy(css = "a>img[alt='fullbeauty']")
	WebElement imgFullBeauty;

	@FindBy(css = "a>img[alt='jessicalondon']")
	WebElement imgJessicaLondon;

	@FindBy(css = "a>img[alt='womanwithin']")
	WebElement imgWomanWithin;
	
	@FindBy(css = "a>img[alt='roamans']")
	WebElement imgRoamans;

	@FindBy(css = "a>img[alt='ellos']")
	WebElement imgEllos;

	@FindBy(css = "a>img[alt='swimsuits']")
	WebElement imgSwimSuits;

	@FindBy(css = "a>img[alt='kingsize']")
	WebElement imgKingSize;

	@FindBy(css = "a>img[alt='brylanehome']")
	WebElement imgBrylaneHome;

	@FindBy(css = "a>img[alt='bco']")
	WebElement imgBCO;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'Privacy Policy')]")
	List<WebElement> lnkPrivacyPolicy;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'Terms of Use')]")
	List<WebElement> lnkTermsOfUse;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'California Transparency in Supply Chains Act')]")
	List<WebElement> lnkSupplyChainsAct;
	
	@FindBy(css = ".brands-list li>a")
	List<WebElement> lnkBrands;
	
	@FindBy(css = ".footer-plcc .footer-plcc-banner .right-section.section .text-section p a[href*='apply']>span")
	WebElement lnkPlatinumCreditCard;
	
	@FindBy(linkText = "Learn More")
	WebElement lnkPlatinumCreditCardLanding;
	
	@FindBy(css = ".footer-plcc .footer-plcc-banner .right-section.section .text-section p a[href*='platinum']")
	WebElement lnkLearnMoreFooter;

	@FindBy(xpath = "//" + FOOTER_DESKTOP + "//a[contains(text(),'Email Subscription')]")
	WebElement lnkEmailSubscription;

	@FindBy(xpath = "//div[contains(@class,'footer-down-inner')]//a[contains(text(),'Customer Service')]")
	WebElement lnkCustomerService;

	@FindBy(xpath = "//footer//a[contains(text(),'Gift Card Balance')]")
	WebElement lnkGiftCardPage;
	
	@FindBy(css = ".footer-container.footer-down")
	WebElement footerContainer;
	
	@FindBy(css = ".text-section a[href$='/catalog-preferences']")
	WebElement lnkRequestCatalog;
	
	@FindBy(css = ".footer-plcc .footer-plcc-banner .right-section.section .text-section p")
	WebElement lnkRewardsFooter;
	
	//================================================================================
	//			WebElements Declaration End
	//================================================================================




	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public Footers(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if(footerPanel.size()>1){
			if (isPageLoaded && !(Utils.waitForElement(driver, footerPanel.get(0)) || Utils.waitForElement(driver, footerPanel.get(1)))) {
				Log.fail("Footer Panel didn't display", driver);
			}
		}
		else{
			if (isPageLoaded && !(Utils.waitForElement(driver, footerPanel1))){
				Log.fail("Footer Panel didn't display", driver);
			}
		}
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		//Utils.waitForPageLoad(driver);
	}
	
	/**
	 * TO scroll to footer page
	 * @throws InterruptedException - 
	 */
	public void scrollToFooter() throws InterruptedException {
		BrowserActions.scrollToViewElement(footerPanel1, driver);
	}
	
	/**
	 * To type the email address into email signup textbox
	 * @param txtToType - 
	 * @throws Exception - Exception
	 */
	public void typeInEmailSignUp(String txtToType)throws Exception{
		BrowserActions.typeOnTextField(fldEmailSignUpFooter, txtToType, driver, "Email SignUp in Footer ");
	}
	
	/**
	 * To check the sign up error message is displaying correct
	 * @return true if message is correct, else false
	 * @throws Exception -
	 */
	public boolean getErrorMessage()throws Exception{
		String error = null;
		if (runPltfrm.equals("mobile")) {
			error = BrowserActions.getText(driver, txtEmailSignUpErrorMobile, "Signup error");
		} else {
			error = BrowserActions.getText(driver, txtEmailSignUpErrorDesktop, "Signup error");
		}
		
		if (error.toLowerCase().contains("please enter a valid email")) {
			return true;
		}
		return false;
	}
	
	/**
	 * To click on the Email SignUp button
	 * @throws Exception - Exception
	 */
	public void clickOnEmailSignUp()throws Exception{
		if(Utils.getRunPlatForm().equals("desktop") || Utils.getRunPlatForm().equals("tablet"))
			BrowserActions.clickOnElementX(btnEmailSignUpDesktopTablet, driver, "Email Sign Up ");
		else
			BrowserActions.clickOnElementX(btnEmailSignUpMobile, driver, "Email Sign Up ");
		
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To get the error message from Email signup error text from footer
	 * @return String - Error Message
	 * @throws Exception - Exception
	 */
	public String getEmailSignUpErrMsgFooter()throws Exception{
		return Utils.waitForElement(driver, txtEmailSignUpErrorDesktop) == true ? 
				BrowserActions.getText(driver, txtEmailSignUpErrorDesktop, "Email SignUp Err ") :
					"Error Message not displayed!";
	}
	
	/**
	 * To get the error message from Email signup error text from hamburgermenu
	 * @return String - Error Message
	 * @throws Exception - Exception
	 */
	public String getEmailSignUpErrMsgHamburgerMenu()throws Exception{
			return BrowserActions.getText(driver, txtEmailSignUpErrorMobile, "Email SignUp Err ");
	}
	
	/**
	 * To navigate to Instagram
	 * @throws Exception - Exception
	 */
	public void navigateToInstagram()throws Exception{
		if(Utils.waitForElement(driver, imgInstagram)) {
			BrowserActions.clickOnElementX(imgInstagram, driver, "Instagram ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "instagram", "url", "");
		} else {
			Log.failsoft("Instagram link is not displayed", driver);
		}
	}

	/**
	 * To navigate to Pinterest
	 * @throws Exception - Exception
	 */
	public void navigateToPinterest()throws Exception{
		if(Utils.waitForElement(driver, imgPinterest)) {
			BrowserActions.clickOnElementX(imgPinterest, driver, "Pinterest ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "pinterest", "url", "");
		} else {
			Log.failsoft("Pinterest link is not displayed", driver);
		}
	}

	/**
	 * To navigate to FaceBook
	 * @throws Exception - Exception
	 */
	public void navigateToFaceBook()throws Exception{
		if(Utils.waitForElement(driver, imgFacebook)) {
			BrowserActions.clickOnElementX(imgFacebook, driver, "FaceBook ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "facebook", "url", "");
		} else {
			Log.failsoft("Facebook link is not displayed", driver);
		}
	}

	/**
	 * To navigate to Twitter
	 * @throws Exception - Exception
	 */
	public void navigateToTwitter()throws Exception{
		if (Utils.waitForElement(driver, imgTwitter)) {
		BrowserActions.clickOnElementX(imgTwitter, driver, "Twitter ");
		Utils.waitForPageLoad(driver);
		Utils.switchWindows(driver, "twitter", "url", "");
		} else {
			Log.failsoft("Twitter link is not displayed", driver);
		}
	}

	/**
	 * To navigate to Youtube
	 * @throws Exception - Exception
	 */
	public void navigateToYoutube()throws Exception{
		if(Utils.waitForElement(driver, imgYoutube)) {
		BrowserActions.clickOnElementX(imgYoutube, driver, "Youtube ");
		Utils.waitForPageLoad(driver);
		Utils.switchWindows(driver, "youtube", "url", "");
		} else {
			Log.failsoft("Youtube link is not displayed", driver);
		}
	}

	/**
	 * To navigate to Top Questions
	 * @throws Exception - Exception
	 */
	public void navigateToTopQuestions()throws Exception{
		if (Utils.waitForElement(driver, lnkTopQuestions)) {
			BrowserActions.clickOnElementX(lnkTopQuestions, driver, "Top Questions ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("Top Questions link is not displayed", driver);
		}
	}

	/**
	 * To navigate to Contact Us
	 * @throws Exception - Exception
	 */
	public void navigateToContactUs()throws Exception{
		if(Utils.waitForElement(driver, lnkContactUs)) {
			BrowserActions.clickOnElementX(lnkContactUs, driver, "Contact Us ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("Contact Us link is not displayed", driver);
		}
	}

	/**
	 * To navigate to Live chat
	 * @throws Exception - Exception
	 */
	public void navigateToLiveChat()throws Exception{
		if(Utils.waitForElement(driver, lnkLiveChat)) {
			BrowserActions.clickOnElementX(lnkLiveChat, driver, "Live Chat ");
			Utils.waitForElement(driver, mdlLiveChat);
		} else {
			Log.failsoft("Live chat link is not displayed", driver);
		}
	}
	
	/**
	 * To navigate to Return Center
	 * @throws Exception - Exception
	 */
	public void navigateToReturnCenter()throws Exception{
		if(Utils.waitForElement(driver, lnkReturnCenter)) {
			BrowserActions.clickOnElementX(lnkReturnCenter, driver, "Return Center ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("Return Center link is not displayed", driver);
		}
	}

	/**
	 * To navigate to Shipping and handling
	 * @throws Exception - Exception
	 */
	public void navigateToShippingAndHandling()throws Exception{
		BrowserActions.clickOnElementX(lnkShippingAndHandling, driver, "Shipping and handling ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to International Shipping 
	 * @throws Exception - Exception
	 */
	public void navigateToInternationalShipping()throws Exception{
		BrowserActions.clickOnElementX(lnkInternationalShipping, driver, "International Shipping ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to EasyReturns
	 * @throws Exception - Exception
	 */
	public void navigateToEasyReturns()throws Exception{
		BrowserActions.clickOnElementX(lnkEasyReturns, driver, "EasyReturns ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to Order Status
	 * @throws Exception - Exception
	 */
	public void navigateToOrderStatus()throws Exception{
		BrowserActions.clickOnElementX(lnkOrderStatus, driver, "Order Status ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to Your Account
	 * @throws Exception - Exception
	 */
	public void navigateToYourAccount()throws Exception{
		if(Utils.waitForElement(driver, lnkYouraccount)) {
		BrowserActions.clickOnElementX(lnkYouraccount, driver, "My Account");
		Utils.waitForPageLoad(driver); 
		} else {
			Log.failsoft("Your Account is not displayed in page", driver);
		}
	}

	/**
	 * To navigate to Order Status under Account section
	 * @throws Exception - Exception
	 */
	public void navigateToOrderStatusUnderAccountSection()throws Exception{
		BrowserActions.clickOnElementX(lnkOrderStatusUnderAccount, driver, "Order Status ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to Wish lists
	 * @throws Exception - Exception
	 */
	public void navigateToWishLists()throws Exception{
		if(Utils.waitForElement(driver, lnkWishLists)) {
			BrowserActions.clickOnElementX(lnkWishLists, driver, "Wish lists ");
		}
		else {
			BrowserActions.clickOnElementX(lnkWishListsJLWW, driver, "Wish lists ");
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to Reward Points
	 * @throws Exception - Exception
	 */
	public void navigateToRewardPoints()throws Exception{
		if(Utils.waitForElement(driver, lnkRewardPoints)) {
			BrowserActions.clickOnElementX(lnkRewardPoints, driver, "Reward Points ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("Reward Points is not displayed in page", driver);
		}
	}
	
	/**
	 * To navigate to PlatinumCreditCard
	 * @return PlatinumCreditCardPage
	 * @throws Exception - Exception
	 */
	public PlatinumCardApplication navigateToPlatinumCreditCard()throws Exception{
		BrowserActions.clickOnElementX(lnkPlatinumCreditCard, driver, "Navigate to Platinum Credit Card");
		Utils.waitForPageLoad(driver);
		return new PlatinumCardApplication(driver).get();
	}
	
	
	/**
	 * To navigate to PlatinumCreditCard Landing Page
	 * @return PlatinumCreditCardPage
	 * @throws Exception - Exception
	 */
	public PlatinumCardLandingPage navigateToPlatinumCreditCardLanding()throws Exception{
		try {
			BrowserActions.clickOnElementX(lnkPlatinumCreditCardLanding, driver, "Navigate to Platinum Credit Card Landing Page");
		}catch(Exception e) {
			Log.event(e.getLocalizedMessage());
			Log.event("Learn More link not found on page. Using workaround...");
			driver.navigate().to(Utils.getWebSite() + redirectData.get("platinumLanding"));
		}
		Utils.waitForPageLoad(driver);
		return new PlatinumCardLandingPage(driver).get();
	}
	
	
	/**
	 * To navigate to PlatinumCreditCard Apply page for Pre-Approved users
	 * @return PlatinumCreditCardPage
	 * @throws Exception - Exception
	 */
	public PreApprovedPlatinumCardPage navigateToPreApprovedPlatinumCreditCard()throws Exception{
		BrowserActions.clickOnElementX(lnkPlatinumCreditCard, driver, "Navigate to Platinum Credit Card");
		Utils.waitForPageLoad(driver);
		return new PreApprovedPlatinumCardPage(driver).get();
	}
	/**
	 * To navigate to PlatinumCreditCard
	 * @return PlatinumCreditCardPage
	 * @throws Exception - Exception
	 */
	
		
	public EmailSubscriptionGuest navigateToEmailSubscription()throws Exception{
		BrowserActions.clickOnElementX(lnkEmailSubscription, driver, "Navigate to Email Subscription");
		Utils.waitForPageLoad(driver);
		return new EmailSubscriptionGuest(driver).get();
	}
	
	/**
	 * To navigate to About Brand
	 * @param brandName - Brand name in two character
	 * @throws Exception - Exception
	 */
	@SuppressWarnings("incomplete-switch")
	public void navigateToAboutBrand(Brand brandName)throws Exception{
		switch(brandName)
		{
		case jl:
			if (Utils.waitForElement(driver, lnkAboutJesiccaLondon)) {
				BrowserActions.clickOnElementX(lnkAboutJesiccaLondon, driver, "About Jessica London ");
				Utils.waitForPageLoad(driver);
			} else {
				Log.failsoft("About Jesicca London is not displayed", driver);
			}
			break;

		case ww:
			if (Utils.waitForElement(driver, lnkAboutWomansWithin)) {
				BrowserActions.clickOnElementX(lnkAboutWomansWithin, driver, "About Womans Within ");
				Utils.waitForPageLoad(driver);
			} else {
				Log.failsoft("About Woman Within is not displayed", driver);
			}
			break;

		case rm:
			if (Utils.waitForElement(driver, lnkAboutRomans)) {
				BrowserActions.clickOnElementX(lnkAboutRomans, driver, "About Romans ");
				Utils.waitForPageLoad(driver);
			} else {
				Log.failsoft("About Roamans is not displayed", driver);
			}
			break;
		case ks:
			if (Utils.waitForElement(driver, lnkAboutKingSize)) {
				BrowserActions.clickOnElementX(lnkAboutKingSize, driver, "About KingSize ");
				Utils.waitForPageLoad(driver);
			} else {
				Log.failsoft("About KingSize is not displayed", driver);
			}
			break;
		case el:
			if (Utils.waitForElement(driver, lnkAboutEllos)) {
				BrowserActions.clickOnElementX(lnkAboutEllos, driver, "About Ellos ");
				Utils.waitForPageLoad(driver);
			} else {
				Log.failsoft("About Ellos is not displayed", driver);
			}
			break;
		}
		
	}

	/**
	 * To navigate to FBB brand career
	 * @throws Exception - Exception
	 */
	public void navigateToFullBeautyBrandCareer()throws Exception{
		if(Utils.waitForElement(driver, lnkFullBeautyBrandCareer)) {
			BrowserActions.clickOnElementX(lnkFullBeautyBrandCareer, driver, "FBB brand career ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("Full Beauty Brand career is not displayed", driver);
		}
	}

	/**
	 * To navigate to About FBB brand
	 * @throws Exception - Exception
	 */
	public void navigateToAboutFullBeautyBrands()throws Exception{
		if(Utils.waitForElement(driver, lnkAboutFullBeautyBrands)) {
			BrowserActions.clickOnElementX(lnkAboutFullBeautyBrands, driver, "About FBB brand ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("About Full Beauty Brand is not displayed", driver);
		}
	}

	/**
	 * To navigate to In The Community
	 * @throws Exception - Exception
	 */
	public void navigateToInTheCommunity()throws Exception{
		BrowserActions.clickOnElementX(lnkInTheCommunity, driver, "In The Community link ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to Full Beauty brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToFullBeautyBrand()throws Exception{
		if(Utils.waitForElement(driver, imgFullBeauty)) {
			BrowserActions.clickOnElementX(imgFullBeauty, driver, "Full Beauty ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "fullbeauty", "url", "");
		} else {
			Log.failsoft("The Full Beauty brand is disabled", driver);
		}
	}

	/**
	 * To navigate to Jessica London brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToJessicaLondonBrand()throws Exception{
		if(Utils.waitForElement(driver, imgJessicaLondon)){
			BrowserActions.clickOnElementX(imgJessicaLondon, driver, "Jessica London ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "jessicalondon", "url", "");
		} else {
			Log.failsoft("The Jessica London brand is disabled", driver);
		}
	}
	
	/**
	 * To click the brand image by index
	 * @param index -
	 * @throws Exception -
	 */
	public void navigateToNthBrand(int index)throws Exception{
		BrowserActions.clickOnElementX(lnkBrands.get(index-1), driver, "Jessica London ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to Woman Within brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToWomanWithinBrand()throws Exception{
		if(Utils.waitForElement(driver, imgWomanWithin)) {
		BrowserActions.clickOnElementX(imgWomanWithin, driver, "Woman Within ");
		Utils.waitForPageLoad(driver);
		Utils.switchWindows(driver, "womanwithin", "url", "");
		} else {
			Log.failsoft("The Woman Within brand is disabled", driver);
		}
	}

	/**
	 * To navigate to Roamans brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToRoamansBrand()throws Exception{
		if(Utils.waitForElement(driver, imgRoamans)) {
			BrowserActions.clickOnElementX(imgRoamans, driver, "Roamans ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "roamans", "url", "");
		} else {
			Log.failsoft("The Roamans brand is disabled", driver);
		}
	}

	/**
	 * To navigate to ellos brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToEllosBrand()throws Exception{
		if(Utils.waitForElement(driver, imgEllos)) {
			BrowserActions.clickOnElementX(imgEllos, driver, "Ellos ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "ellos", "url", "");
		} else {
			Log.failsoft("The Ellos brand is disabled", driver);
		}
	}

	/**
	 * To navigate to swimsuits brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToSwimSuitsBrand()throws Exception{
		if(Utils.waitForElement(driver, imgSwimSuits)) {
			BrowserActions.clickOnElementX(imgSwimSuits, driver, "Swimsuits brand ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "swimsuitsforall", "url", "");
		} else {
			Log.failsoft("The Swim Suits brand is disabled", driver);
		}
	}

	/**
	 * To navigate to kingsize brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToKingSizeBrand()throws Exception{
		if(Utils.waitForElement(driver, imgKingSize)) {
			BrowserActions.clickOnElementX(imgKingSize, driver, "King Size brand ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "kingsizedirect", "url", "");
		} else {
			Log.failsoft("The King Size brand is disabled", driver);
		}
	}

	/**
	 * To navigate to Brylane Home brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToBrylaneHome()throws Exception{
		if (Utils.waitForElement(driver, imgBrylaneHome)) {
			BrowserActions.clickOnElementX(imgBrylaneHome, driver, "Brylane Home brand ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "brylanehome", "url", "");
		} else {
			Log.failsoft("The Brylane brand is disabled", driver);
		}
	}

	/**
	 * To navigate to BCO brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToBCO()throws Exception{
		if(Utils.waitForElement(driver, imgBCO)) {
			BrowserActions.clickOnElementX(imgBCO, driver, "BCO brand ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "bcoutlet", "url", "");
		} else {
			Log.failsoft("The BCOutlet brand is disabled", driver);
		}
	}

	/**
	 * To navigate to Privacy Policy
	 * @throws Exception - Exception
	 */
	public void navigateToPrivacyPolicy()throws Exception{
		//if(Utils.getRunPlatForm().equals("mobile"))
			//BrowserActions.clickOnElementX(lnkPrivacyPolicy.get(1), driver, "Privacy Policy ");
		//else
		if (Utils.waitForElement(driver, lnkPrivacyPolicy.get(0))) {
			BrowserActions.clickOnElementX(lnkPrivacyPolicy.get(0), driver, "Privacy Policy ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("The Privacy policy is not displayed", driver);
		}
		//Need to add return page statement  after the pages have been configured in the application 
	}

	/**
	 * To navigate to Terms of use
	 * @throws Exception - Exception
	 */
	public void navigateToTermsOfUse()throws Exception{
		//if(Utils.getRunPlatForm().equals("mobile"))
			//BrowserActions.clickOnElementX(lnkTermsOfUse.get(1), driver, "Privacy Policy ");
		//else
		if(Utils.waitForElement(driver, lnkTermsOfUse.get(0))) {
			BrowserActions.clickOnElementX(lnkTermsOfUse.get(0), driver, "Privacy Policy ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("The Terms of use is not displayed", driver);
		}
		//Need to add return page statement  after the pages have been configured in the application 
	}

	/**
	 * To navigate to Supply Chains Act
	 * @throws Exception - Exception
	 */
	public void navigateToSupplyChainsAct()throws Exception{
		//if(Utils.getRunPlatForm().equals("mobile"))
		//	BrowserActions.clickOnElementX(lnkSupplyChainsAct.get(1), driver, "Privacy Policy ");
		//else
		if (Utils.waitForElement(driver, lnkSupplyChainsAct.get(0))) {
			BrowserActions.clickOnElementX(lnkSupplyChainsAct.get(0), driver, "Privacy Policy ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("The Supply chain link is not displayed", driver);
		}
		//Need to add return page statement  after the pages have been configured in the application 
	}

	/**
	 * To navigate to customer service page 
	 * @return CustomerService
	 * @throws Exception - Exception
	 */
	public CustomerService navigateToCustomerService()throws Exception{
		if (Utils.waitForElement(driver, lnkCustomerService)) {
		BrowserActions.scrollToViewElement(lnkCustomerService, driver);
		BrowserActions.clickOnElementX(lnkCustomerService, driver, "Customer Service");
		Utils.waitForPageLoad(driver);
		return new CustomerService(driver).get();
		} else {
			Log.failsoft("The Customer Service link is not displayed", driver);
			return null;
		}
	}
	
	/**
	 * To navigate to gift card page
	 * @return GiftCardPage- Page object
	 * @throws Exception - Exception
	 */
	public GiftCardPage navigateToGiftCardPage()throws Exception{
		if(Utils.waitForElement(driver, lnkGiftCardPage))
		{
			BrowserActions.javascriptClick(lnkGiftCardPage, driver, "Gift Card in Footer");
		}else
		{
			Log.message("Navigation element not available in footer, using workaround to navigate to Gift Card page.");
			String url = driver.getCurrentUrl()+"giftcard-landing-asset-"+Utils.getCurrentBrand()+".html";
			driver.navigate().to(url);
		}		
		Utils.waitForPageLoad(driver);
		return new GiftCardPage(driver).get();
	}
	
	@FindBy(css = "footer a[href*='quick-order']")
	WebElement lnkQuickOrder;
	
	/**
	 * To navigate to quick order page
	 * @return QuickOrderPage - Page object
	 * @throws Exception - Exception
	 */
	public QuickOrderPage navigateToQuickOrder()throws Exception{
		BrowserActions.clickOnElementX(lnkQuickOrder, driver, "Order From Catalog Link");
		return new QuickOrderPage(driver).get();
	}
	
	/**
	 * To navigate to Catalog Preferences
	 * @return CatalogPreferencePage - page object for Catalog Preferences
	 * @throws Exception - Exception
	 */
	public CatalogPreferencePage navigateToCatalogPreferences()throws Exception{
		BrowserActions.clickOnElementX(lnkRequestCatalog, driver, "Catalog Preferences");
		return new CatalogPreferencePage(driver).get();
	}
	
	/**
	 * To navigate to physical Gift Card
	 * @return PdpPage - Gift Card PDP object
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPhysicalGiftCard()throws Exception{
		if(Utils.waitForElement(driver, lnkPhysicalGiftCard))
		{
			BrowserActions.clickOnElementX(lnkPhysicalGiftCard, driver, "physical Gift Card in Footer");
		}else
		{
			Log.message("Navigation element not available in footer, using workaround to navigate to Gift Card page.");
		}		
		
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}
	
	/**
	 * To navigate to EGift Card
	 * @return PdpPage - EGiftcard page object 
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToEGiftCard() throws Exception{
		if(Utils.waitForElement(driver, lnkEGiftCard))
		{
			BrowserActions.javascriptClick(lnkEGiftCard, driver, "EGift Card link in Footer");
		}else
		{
			Log.message("Navigation element not available in footer.");
		}
		Utils.waitForPageLoad(driver);
		List<WebElement> prdList = driver.findElements(By.cssSelector("#search-result-items .product-name-image-container > a"));
		if(prdList.size() == 0) {
			return new PdpPage(driver).get();
		}else {
			BrowserActions.clickOnElementX(prdList.get(0), driver, "EGift Card");
			Utils.waitForPageLoad(driver);
			return new PdpPage(driver).get();
		}
	}
}
