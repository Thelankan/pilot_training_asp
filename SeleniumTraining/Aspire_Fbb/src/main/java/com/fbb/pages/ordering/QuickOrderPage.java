package com.fbb.pages.ordering;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class QuickOrderPage  extends LoadableComponent <QuickOrderPage>{

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;

	public String runPltfrm = Utils.getRunPlatForm(); 
	private static final String SIZE_SELECTED = ".swatches.size .selected";

	private static final String MINI_CART_OVERLAY = ".minicartpopup ";


	/**********************************************************************************************
	 ********************************* WebElements of Quick Order Page ***********************************
	 **********************************************************************************************/
	@FindBy(css = ".loader-bg")
	WebElement PDPspinner;
	
	@FindBy(css = "#wrapper")
	WebElement containerElement;
	
	@FindBy(css = "footer .footer-container.footer-top")
	WebElement footerTopSection;

	@FindBy(css = ".catalog-quick-order")
	WebElement divCatalogQuickOrder;

	//@FindBy(css = "input[id*='dwfrm_quickorder_search_itemnumber']")
	@FindBy(css = ".itemnumber input")
	WebElement txtItemSearch;
	
	@FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
	WebElement breadcrumbArrowMobile;
	
	@FindBy(css = ".blueboxnumberfield input")
	WebElement txtBlueBoxNumberField;
	
	@FindBy(css = ".itemnumber input")
	List<WebElement> txtItemSearchList;
	
	@FindBy(css = ".hide-desktop.hide-tablet .catalog-item-number")
	List<WebElement> txtCQONumberOfItems_Mobile;
	
	@FindBy(css = ".hide-mobile .catalog-item-number")
	List<WebElement> txtCQONumberOfItems_Des_Tab;
	
	@FindBy(css = ".pdp-main form.pdpForm:not(.bottom-add-all)")
	List<WebElement> productsInQuickOrder;
	
	@FindBy(css = ".itemnumber1 input")
	WebElement txtItemSearch1;
	
	@FindBy(css = ".itemnumber2 input")
	WebElement txtItemSearch2;
	
	@FindBy(css = ".itemnumber3 input")
	WebElement txtItemSearch3;
	
	@FindBy(css = ".itemnumber4 input")
	WebElement txtItemSearch4;
	
	@FindBy(css =".catalog-quick-order__main.make-label-absolute")
	WebElement contentBox;
	
	@FindBy(css = ".pdpForm fieldset")
	WebElement frmOrderForm;

	@FindBy(css = ".cqo-btn-search")
	WebElement btnSearch;
	
	@FindBy(css = ".generic-error-required.error")
	WebElement genericErrorRequired;
	
	@FindBy(css =".itemnumber1.error-handle")
	WebElement item1InvalidErrorMsg;
	
	@FindBy(css =".itemnumber1 .error")
	WebElement item1ErrorMsg;
	
	@FindBy(css =".itemnumber2 .error")
	WebElement item2ErrorMsg;
	
	@FindBy(css =".itemnumber3 .error")
	WebElement item3ErrorMsg;
	
	@FindBy(css =".itemnumber4 .error")
	WebElement item4ErrorMsg;
	
	@FindBy(css = ".generic-error.error")
	WebElement genericError;
	
	@FindBy(css = ".product-price")
	List<WebElement> productPriceList;

	@FindBy(css = "#pdpMain")
	WebElement divPdpMain;
	
	@FindBy(css =".itemnumber1 .invalid")
	WebElement invalidErrorMsg;
	
	@FindBy(css =".hide-mobile .catalog-item-number")
	WebElement orderCatlogNumberDesktop;

	@FindBy(css =".hide-desktop.hide-tablet .catalog-item-number")
	WebElement orderCatlogNumberMobile;
	
	@FindBy(css = ".itemnumber .error")
	WebElement txtInvalidErrorMessage;

	@FindBy(css = ".error-handle .label-text")
	WebElement txtrequiredErrorMessage;

	@FindBy(css = ".svg.svg-converted")
	WebElement divHeadBanner;

	@FindBy(css = ".footer-brand-selector .content-asset")
	WebElement divFooterBrandBanner;

	@FindBy(css = ".catalog-quick-order__leftNav")
	WebElement divCatalogQuickOrderleft;

	@FindBy(css = ".product-name.hide-mobile .name-text")
	WebElement txtProductName;

	@FindBy(css = ".product-name .name-text")
	WebElement txtProductNameMobileNew;

	@FindBy(css = ".product-col-2.product-quickorder-detail .product-price")
	WebElement txtPrice;

	@FindBy(css = ".product-variation-content")
	WebElement divApplicableVariation;

	@FindBy(css = ".size_icon")
	WebElement divSizeChart;

	@FindBy(css = ".product-name .name-text")
	List<WebElement> lblPrdNameInDesktop;

	@FindBy(css = ".product-name .name-text")
	List<WebElement> lblPrdNameInMobile;

	@FindBy(css = SIZE_SELECTED)
	WebElement selectedSize;

	@FindBy(xpath = "//select[@id='Quantity']//following-sibling::div[@class='selected-option selected']")
	WebElement selectedQty;

	@FindBy(css = ".swatches.size li.selectable a")
	List<WebElement> lstSizeSwatchesSelectable;
	
	@FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop")
	List<WebElement> shopNowWindow_ProductSet;
	
	@FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop.open")
	List<WebElement> shopNowWindow_ProductSet_Opened;
	
	@FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop.open")
	WebElement shopNowWindow_ProductSet_Open;

	@FindBy(css = ".swatches.size li")
	List<WebElement> lstSizeSwatches;

	@FindBy(css = ".swatches.size li.unselectable a")
	List<WebElement> lstSizeSwatchesUnSelectable;

	@FindBy(css = ".swatches.color li.selected a")
	WebElement selectedColor;

	@FindBy(css = ".swatches.color li.selectable:not([class*='selected']) a")
	List<WebElement> lstColorSelectable;

	@FindBy(css = "#add-to-cart")
	WebElement btnAddToCart;
	
	@FindBy(css = ".swatches.sizefamily .selected a")
	WebElement selectedSizeFamily;
	
	@FindBy(css = ".swatches.sizefamily .selectable:not(.selected) a")
	List<WebElement> sizeFamilyVariation;
	
	@FindBy(css = ".attribute.sizeFamily")
	WebElement divSizeFamily;
	
	@FindBy(css = ".add-all-to-cart-quick-order")
	WebElement btnAddAllToCartQuickOrder;

	@FindBy(css =".primary-image-quick-order")
	WebElement imgPrimaryImage;
	
	@FindBy(css =".product-primary-image .slick-current.slick-active img")
	WebElement imgPrimaryImage_Mobile;
	
	@FindBy(css= ".product-name.hide-mobile span")
	WebElement productName;
	
	@FindBy(css= ".hide-desktop.hide-tablet .product-name span")
	WebElement productName_Mobile;
	
	@FindBy(css= ".product-add-to-cart .inventory")
	WebElement qtyDropDown;
	
	@FindBy(css= ".attribute.color")
	WebElement colorAttribute;
	
	@FindBy(css= ".attribute.size")
	WebElement sizeAttribute;
	
	@FindBy(css= ".swatches.size a")
	WebElement sizeAnchor;
	
	@FindBy(css= ".size-chart-link a")
	WebElement sizeChart;
	
	@FindBy(css= ".size-chart-link a .size_label")
	WebElement sizeChartLabel;
	
	@FindBy(css= ".size-guide-container")
	WebElement sizeChartDescription;
	
	@FindBy(css=".ui-dialog-titlebar-close")
	WebElement sizeChartClose;
	
	@FindBy(css =".primary-image")
	WebElement imgPrimaryImageNew;

	@FindBy(css =".product-col-2.product-quickorder-detail")
	WebElement productInformationSection;

	@FindBy(css =".price-sales.price-standard-exist")
	WebElement txtCatalogPrice;

	@FindBy(css = "select[id='Quantity']")
	WebElement drpQty;

	@FindBy(css = "div[class='quantity']")
	WebElement drpQuantity;

	@FindBy(css =".btn-remove-item.hide-mobile")
	WebElement btnClose;
	
	@FindBy(css =".hide-desktop.hide-tablet .btn-remove-item")
	WebElement btnClose_Mobile;
	
	@FindBy(css =".btn-remove-item.hide-mobile")
	List<WebElement> lstBtnClose;
	
	@FindBy(css =".hide-desktop.hide-tablet .btn-remove-item")
	List<WebElement> lstBtnClose_Mobile;

	@FindBy(css= ".product-monogramming .title")
	WebElement txtMonogrammingTitle;

	@FindBy(css= ".monogramming-enabled.form") 
	WebElement chkEnableMonogramming;

	@FindBy(css= ".monogramming-enabled.form>div>div>label>span")
	WebElement txtMonogramMessaging;

	@FindBy(css= ".color_gray")
	WebElement txtPrdAvailabilityMessaging;

	@FindBy(css = ".product-add-to-cart div[class='selected-option selected']")
	WebElement selectedQty1;
	
	@FindBy(css = ".hide-mobile.btn-remove-item")
	List<WebElement> removeItems_Des_Tab;
	
	@FindBy(css = ".hide-desktop.hide-tablet .btn-remove-item")
	List<WebElement> removeItems_Mobile;

	@FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop")
	WebElement btnShopNow;

	@FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop.open")
	WebElement btnShopNowClose;

	@FindBy(css = ".product-variations")
	WebElement divProductVariations;

	@FindBy(css = ".variation-add-to-cart-content")
	WebElement divProductVariationsDisplay;


	@FindBy(css = ".breadcrumb-element")
	WebElement lnkBreadcrumbHome;

	@FindBy(css = ".breadcrumb-element.current-element")
	WebElement lnkBreadcrumbCurrentPage;

	@FindBy(css = ".mainBanner")
	WebElement divGridHeader;

	@FindBy(css = ".breadcrumb")
	WebElement divBreadCrumb;

	@FindBy(css = "#product-content > div.product-hemmable")
	WebElement chkProductOptions;

	@FindBy(css = ".mainBanner .content-asset div.display-inline-block")
	WebElement divHeadAsset;
	
	@FindBy(css = ".mainBanner .content-asset img.hide-mobile.hide-tablet")
	WebElement divHeadAsset_WW_Desktop;
	
	@FindBy(css = ".mainBanner .content-asset img.hide-mobile.hide-desktop")
	WebElement divHeadAsset_WW_Tablet;
	
	@FindBy(css = ".mainBanner .content-asset img.hide-desktop.hide-tablet")
	WebElement divHeadAsset_WW_Mobile;

	@FindBy(css = ".catalog-quick-order__leftNav")
	WebElement divContentSlot;

	@FindBy(css = ".quantity .selected-option.selected")
	WebElement txtSelectedQuantity;

	@FindBy(css = ".quantity .selection-list li")
	List<WebElement> lstQty;

	@FindBy(css = ".swatches.size")
	List<WebElement> lstSizeGrid;

	@FindBy(css = ".availability-msg")
	List<WebElement> lstavailability;

	@FindBy(css = ".product-name.hide-mobile")
	List<WebElement> txtProductNameDesktopTablet;

	@FindBy(css = ".product-name.hide-mobile")
	List<WebElement> txtProductNameMobile;

	@FindBy(css = "#pdpMain .product-price")
	WebElement divWebPricing;

	@FindBy(css = ".promotion .callout-message")
	WebElement divPromotionCallout;

	@FindBy(css = ".swatches.color")
	List<WebElement> lstColorGrid;

	@FindBy(css = ".btn-remove-item.button-text")
	List<WebElement> divCloseProduct;

	@FindBy(css = MINI_CART_OVERLAY + ".ui-dialog-titlebar-close")
	WebElement btnCloseMiniCartOverLay;

	@FindBy(css = ".minicartpopup")
	WebElement mdlMiniCartOverLay;

	@FindBy(css = ".quantity .selection-list li:not([class='selected'])")
	List<WebElement> lstQtySelectable;

	@FindBy(css = ".quantity-ats-message")
	WebElement errMsgQuantityATS;

	@FindBy(css = ".product-name .name-text")
	List<WebElement> productNameDesktop;

	@FindBy(css = ".menu-utility-user.hide-desktop.hide-tablet .mini-cart-link")
	WebElement iconMyBag;

	@FindBy(css = "#add-all-to-cart")
	WebElement btnAddAllToBag;

	@FindBy(css = "#add-to-cart")
	WebElement btnAddToBag;

	@FindBy(css = ".product-primary-image .catalog-item-number")
	WebElement quickOrderCatalogNumber;
	
	@FindBy(css = ".hide-desktop.hide-tablet .catalog-item-number")
	WebElement quickOrderCatalogNumberMobile;
	
	@FindBy(css = ".hide-mobile .catalog-item-number")
	WebElement quickOrderCatalogNumberDesktopTab;

	@FindBy(css = "#pdpMain")
	WebElement quickOrderMainContent;

	@FindBy(css = "#pdpMain .hemmingmonograming form")
	List<WebElement> formQuickOrderProduct;
	
	@FindBy(css = ".quick-order-banner")
	WebElement quickOrderHeading;


	@FindBy(css = ".mini-cart-product.slick-slide")
	List<WebElement> overlayProductCount;

	@FindBy(css = ".cart-mini-overlay")
	WebElement addToCartOverlay;
	
	@FindBy(css = ".slick-active .mini-cart-subtotals")
	WebElement overlayProductAdded;
	
	@FindBy(css = ".only-for-mobile .mini-cart-subtotals")
	WebElement overlayProductAdded_Mobile;

	@FindBy(css = ".expired-price")
	WebElement catalogPriceExpired;
	
	@FindBy(css = ".product-price .price-sales.price-standard-exist")
	WebElement catlogPriceDiscount;

	@FindBy(css = ".catalog-quick-order .mainBanner > div")
	List<WebElement> divHeadAssetWithBreadCrumb;

	@FindBy(css = "form .swatches.size")
	List<WebElement> divProductCountBasedOnSize;

	@FindBy(css = ".price-sales")
	WebElement productPrice;

	@FindBy(css = ".color_gray")
	WebElement availabilityMessaging;

	@FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop")
	WebElement shopNowMobile;
	
	@FindBy(xpath = "//span[contains(text(),'Hide Options')]")
	WebElement shopNowMobileOpened;
	
	@FindBy(xpath = "//span[contains(text(),'Shop Now')]")
	WebElement shopNowMobileClosed;
	
	@FindBy(id = "thumbnails")
	WebElement divAlternateImages;
	
	@FindBy(css = "#thumbnails .thumb")
	List<WebElement> lstAlternateImages;
	
	@FindBy(css = ".prod-details p")
	WebElement lblProductDetails;
	
	@FindBy(css = ".prod-details.make-height-auto")
	WebElement hiddenDataInProductDetail;
	
	@FindBy(css = "span[class='read-more']")
	WebElement btnReadMore;
	
	@FindBy(css = ".read-more.opened")
	WebElement btnCloseProductDetails;
	
	/**********************************************************************************************
	 ********************************* WebElements of PDP Quick Order - Ends ****************************
	 **********************************************************************************************/



	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public QuickOrderPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, divCatalogQuickOrder))) {
			Log.fail("Quick Order Page did not open up. Site might be down.", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);		

	}// isLoaded

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * TO Search for a product in Quick order page
	 * @param productID - Product ID
	 * @param enableError - True - if enabled error
	 * @throws Exception - Exception
	 */
	public void searchItemInQuickOrder(String productID, boolean enableError) throws Exception {
		Utils.waitForElement(driver, txtItemSearch);
		BrowserActions.scrollInToView(txtItemSearch, driver);
		BrowserActions.typeOnTextField(txtItemSearch, productID, driver, "Search Item");
		BrowserActions.clickOnElementX(btnSearch, driver, "Search Button");

		if(enableError && Utils.waitForElement(driver, txtInvalidErrorMessage)){
			Log.fail("Given ID Not Available.", driver);
		}
	}

	/**
	 * To search an Item in Quick order page with search box 4
	 * @param productID - Product ID
	 * @throws Exception - Exception - Exception
	 */
	public void searchItemInQuickOrderSearchBox1(String productID) throws Exception {
		BrowserActions.typeOnTextField(txtItemSearch1, productID, driver, "Search Item");
		BrowserActions.clickOnElementX(btnSearch, driver, "Search Button");
		
		Utils.waitForPageLoad(driver);

	}

	/**
	 * To search an Item in Quick order page with search box 4
	 * @param productID - Product ID
	 * @throws Exception - Exception - Exception
	 */
	public void searchItemInQuickOrderSearchBox2(String productID) throws Exception {
		BrowserActions.typeOnTextField(txtItemSearch2, productID, driver, "Search Item");
		BrowserActions.clickOnElementX(btnSearch, driver, "Search Button");
		
		Utils.waitForPageLoad(driver);

	}

	/**
	 * To search an Item in Quick order page with search box 4
	 * @param productID - Product ID
	 * @throws Exception - Exception - Exception
	 */
	public void searchItemInQuickOrderSearchBox3(String productID) throws Exception {
		BrowserActions.typeOnTextField(txtItemSearch3, productID, driver, "Search Item");
		BrowserActions.clickOnElementX(btnSearch, driver, "Search Button");
		
		Utils.waitForPageLoad(driver);

	}
	
	/**
	 * To search an Item in Quick order page with search box 4
	 * @param productID - Product ID
	 * @throws Exception - Exception - Exception
	 */
	public void searchItemInQuickOrderSearchBox4(String productID) throws Exception {
		BrowserActions.typeOnTextField(txtItemSearch4, productID, driver, "Search Item");
		BrowserActions.clickOnElementX(btnSearch, driver, "Search Button");
		
		Utils.waitForPageLoad(driver);

	}
	
	/**
	 * Search multiple product Id in quick order
	 * @param productID - Array of product Ids need to search
	 * @throws Exception
	 */
	public void searchMultipleProductInQuickOrder(String[] productID) throws Exception {
		int size = productID.length;
		
		BrowserActions.clearTextField(txtItemSearch1, "Text Filed 4");
		BrowserActions.clearTextField(txtItemSearch2, "Text Filed 4");
		BrowserActions.clearTextField(txtItemSearch3, "Text Filed 4");
		BrowserActions.clearTextField(txtItemSearch4, "Text Filed 4");
		
		if (size > 4) {
			Log.reference("The given number of items is "+size+", hence searching first 4 items");
			size = 4;
		}
		
		for(int i=0; i<size ;i++) {
			BrowserActions.typeOnTextField(txtItemSearchList.get(i), productID[i], driver, "Search Item");
		}
		
		BrowserActions.clickOnElementX(btnSearch, driver, "Search Button");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To verify all the given items are searched in the quickorder
	 * @param productID - Array of product Ids need to search
	 * @throws Exception - Exception
	 */
	public boolean verifyWithoutOrderAllTheSearchedItemsAreGettingDisplayed(String[] productID) throws Exception {
		List<WebElement> elem = null;
		if(Utils.isMobile()) {
			elem = txtCQONumberOfItems_Mobile;
		} else {
			elem = txtCQONumberOfItems_Des_Tab;
		}
		
		int prdSearchCount = 0;
		if (productID.length != elem.size()) {
			return false;
		}
		
		for(int i=0; i<elem.size() ;i++) {
			String value = BrowserActions.getText(driver, elem.get(i), "CQO Searched Item");
			
			for (int j=0; j < productID.length; j++) {
				Log.event(">>>>>Text in element : "+value);
				Log.event(">>>>>Text from param : "+productID[j]);
				if (value.trim().equals(productID[j])) {
					prdSearchCount++;
				}
			}
		}
		
		if(prdSearchCount == elem.size())
			return true;
		
		return false;
	}
	
	/**
	 * To verify all the given items are searched in correct order
	 * @param productID - Array of product Ids need to search
	 * @throws Exception - Exception
	 */
	public boolean verifyWithOrderAllTheSearchedItemsAreGettingDisplayed(String[] productID) throws Exception {
		List<WebElement> elem = null;
		if(Utils.isMobile()) {
			elem = txtCQONumberOfItems_Mobile;
		} else {
			elem = txtCQONumberOfItems_Des_Tab;
		}
		
		if (productID.length != elem.size()) {
			return false;
		}
		
		for(int i=0; i<elem.size() ;i++) {
			String value = BrowserActions.getText(driver, elem.get(i), "CQO Searched Item");
			Log.event(">>>>>Text in element : "+value);
			Log.event(">>>>>Text from param : "+productID[i]);
			if (!(value.trim().equals(productID[i]))) {
				return false;
			}
		}
		
		return true;
	}
	
	
	/**
	 * To remove all items from quick order
	 * @throws Exception - Exception
	 */
	public void removeAllItemsFromQuickOrder() throws Exception {
		List<WebElement> elem = null;
		if(Utils.isMobile()) {
			elem = removeItems_Mobile;
		} else {
			elem = removeItems_Des_Tab;
		}
		int size = elem.size();
		for(int i=size; i>=1 ;i--) {
			BrowserActions.clickOnElementX(elem.get(i-1), driver, "Remove link");
			Utils.waitForPageLoad(driver);
		}
	}
	
	/**
	 * To search an Item in Quick order page with given product id
	 * @param productID - Product ID
	 * @throws Exception - Exception - Exception
	 */
	public void searchItemInQuickOrder(String productID) throws Exception {
		BrowserActions.typeOnTextField(txtItemSearch, productID, driver, "Search Item");
		BrowserActions.clickOnElementX(btnSearch, driver, "Search Button");
		Utils.waitForElement(driver, imgPrimaryImage);
	}

	/**
	 * To verify the invalid product error message in item search fields
	 * @param productID - Product ID
	 * @throws Exception - Exception - Exception
	 */
	public void verifyErrorMessage(String productID) throws Exception {
		Utils.waitForElement(driver, txtItemSearch);
		BrowserActions.scrollInToView(txtItemSearch, driver);
		BrowserActions.clickOnElementX(txtItemSearch, driver, "Search Item");
		BrowserActions.typeOnTextField(txtItemSearch, productID, driver, "Search Item");
		BrowserActions.clickOnElementX(btnSearch, driver, "Search Button");

	}


	/**
	 * To Select size in Quick order product page
	 * @param size - Size swatch text
	 * @return String
	 * @throws Exception - Exception - Exception
	 */
	public String selectSize(String... size) throws Exception {
		boolean flag = true;
		if (!(size.length > 0)) {
			if (Utils.waitForElement(driver, selectedSize)) {
				return selectedSize.getText().trim();
			}
		}
		String sizeSelected = null;
		int sizeOfSwatch = lstSizeSwatchesSelectable.size();
		if (size.length > 0) {
			for (int i = 0; i < lstSizeSwatchesSelectable.size(); i++) {
				if (lstSizeSwatchesSelectable.get(i).getText().trim().toLowerCase().equals(size[0].toLowerCase())) {
					BrowserActions.scrollToViewElement(lstSizeSwatchesSelectable.get(i), driver);
					BrowserActions.clickOnElementX(lstSizeSwatchesSelectable.get(i), driver, size[0] + " Size Swatch");
					Utils.waitForPageLoad(driver);
				}
			}
			for (int i = 0; i < lstSizeSwatchesUnSelectable.size(); i++) {
				if (lstSizeSwatchesUnSelectable.get(i).getText().trim().equals(size[0])) {
					Log.message("--->>> Given Size(" + size[0]
							+ ") <b>Not Available</b> in Size list. Selecting Random Size...");
					flag = false;
					break;
				}
			}
			if (flag)
				Log.message("--->>> Given Size(" + size[0]
						+ ") <b>Not Applicable</b> in Size list. Selecting Random Size...");
			BrowserActions.clickOnElementX(lstSizeSwatchesSelectable.get(0), driver, "Size Swatches ");
			Log.event("Clicked on 1st Size...");
			Utils.waitForPageLoad(driver);
		} else {
			BrowserActions.javascriptClick(lstSizeSwatchesSelectable.get(0), driver, "Size Swatches ");
			Log.event("Clicked On 1st Size...");
			Utils.waitForPageLoad(driver);
		}
		
		try{
			sizeSelected = selectedSize.getText().trim();
		} catch (NoSuchElementException e) {
			if (sizeOfSwatch != lstSizeSwatchesSelectable.size()) {
				BrowserActions.javascriptClick(lstSizeSwatchesSelectable.get(0), driver, "Size Swatches ");
				Utils.waitForPageLoad(driver);
				return selectedSize.getText().trim();
			} else {
				Log.fail("Size not selected", driver);
			}
		}
		return sizeSelected;
	}
	
	/**
	 * To get list of products added in quick order page
	 * @return List of Product Names
	 * @throws Exception - Exception - Exception
	 */
	public List<String> getProductNames() throws Exception {
		List<String> productName= new ArrayList<String>();
		List<WebElement> prdName = null;
		if(runPltfrm.equals("mobile"))
			prdName = lblPrdNameInMobile;
		else
			prdName = lblPrdNameInDesktop;
		for(WebElement ele : prdName){
			productName.add(ele.getAttribute("innerHTML").trim());
		}
		
		return productName;
	}

	/**
	 * To get recently added product into page
	 * @return Recently added product name
	 * @throws Exception - Exception - Exception
	 */
	public String getRecentlyAddedProduct() throws Exception{
		if(runPltfrm.equals("mobile"))
			return lblPrdNameInMobile.get(0).getAttribute("innerHTML");
		else
			return lblPrdNameInDesktop.get(0).getAttribute("innerHTML");
	}

	/**
	 * To wait for spinner in the PDP
	 */
	public void waitForSpinner() {
		Utils.waitUntilElementDisappear(driver, PDPspinner);
	}

	/**
	 * To Select color of product
	 * @param color -
	 * @return selected color
	 * @throws Exception - Exception
	 */
	public String selectColor(String... color)throws Exception{
		if(Utils.waitForElement(driver, selectedColor)){
			return selectedColor.getAttribute("title").split("\\:")[1].trim();
		}
		if(color.length > 0){
			String xpathForColor = "//ul[@class='swatches color']//li//a[contains(translate(@title,'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'"+ color[0].toLowerCase() +"')]";
			if(Utils.waitForElement(driver, driver.findElement(By.xpath(xpathForColor)))){
				WebElement colorToSelect = driver.findElement(By.xpath(xpathForColor));
				BrowserActions.clickOnElementX(colorToSelect, driver, "Quantity ");
				Log.event("Clicked on Expected Color...");
				Utils.waitForPageLoad(driver);
				return color[0];
			}else{
				Log.failsoft("--->>>Given Color Not Available...", driver);
				if(!Utils.waitForElement(driver, selectedColor)){
					int rand = Utils.getRandom(0, lstColorSelectable.size());
					BrowserActions.clickOnElementX(lstColorSelectable.get(rand), driver, "Quantity ");
					Utils.waitForPageLoad(driver);
					return selectedColor.getAttribute("title").split("\\:")[1].trim();
				}else {
					Utils.waitForPageLoad(driver);
					return selectedColor.getAttribute("title").split("\\:")[1].trim();
				}
			}
		}else{
			int rand = Utils.getRandom(0, lstColorSelectable.size());
			Log.event("Scrolled To Random Color...");
			BrowserActions.clickOnElementX(lstColorSelectable.get(rand), driver, "Quantity ");
			Log.event("Clicked on Random Color...");
			Utils.waitForPageLoad(driver);
			return selectedColor.getAttribute("title").split("\\:")[1].trim();
		}
	}
	
	/**
	 * To click on 'Add to cart' button
	 * @throws Exception - Exception
	 */

	public void clickAddProductToBag()throws Exception{
		BrowserActions.scrollInToView(btnAddToCart, driver);
		BrowserActions.clickOnElementX(btnAddToCart, driver, "Add To Cart Button ");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To select size family and return the random size
	 * @param sizeF - Size family to select (Optional)
	 * @return String - Selected size family
	 * @throws Exception - Exception
	 */
	public String selectSizeFamily(String... sizeF) throws Exception {
		if(!Utils.waitForElement(driver, divSizeFamily)){
			return "No Size Family";
		}
		if (Utils.waitForElement(driver, selectedSizeFamily)) {
			String selectedSizeF = selectedSizeFamily.getAttribute("title").split("\\:")[1].trim();
			if (sizeF.length > 0) {
				if (sizeF[0].trim().equalsIgnoreCase(selectedSizeF.trim())) {
					return selectedSizeF;
				} else {
					Log.event("Selected Size family is not equal to given size family : ");
				}
			} else {
				return selectedSizeF;
			}
		}
		if (sizeF.length > 0) {
			int vairationSize = sizeFamilyVariation.size();
			for (int i=0; i<vairationSize; i++) {
				String selectSizeF = sizeFamilyVariation.get(i).getAttribute("title").split("\\:")[1].trim();
				if(sizeF[0].trim().equalsIgnoreCase(selectSizeF.trim())) {
					BrowserActions.clickOnElementX(sizeFamilyVariation.get(i), driver, "Size Family");
					return selectSizeF;
				}
			}
		} else {
			String sizeFSelecected = sizeFamilyVariation.get(0).getAttribute("title").split("\\:")[1].trim();
			BrowserActions.clickOnElementX(sizeFamilyVariation.get(0), driver, "Size Family");
			return sizeFSelecected;
		}
		return null;
	}
	
	/**
	 * To click on 'Add to cart' button
	 * @throws Exception - Exception
	 */

	public void clickAddAllProductToBag()throws Exception{
		BrowserActions.scrollInToView(btnAddAllToCartQuickOrder, driver);
		BrowserActions.javascriptClick(btnAddAllToCartQuickOrder, driver, "Add To Cart Button ");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To Check the add to cart button disabled state
	 * @return String - value
	 * @throws Exception
	 */
	public String addToCartButtonDisabledState() throws Exception {
		BrowserActions.scrollInToView(btnAddToCart, driver);
		BrowserActions.mouseHover(driver, btnAddToCart);
		BrowserActions.javascriptClick(btnAddToCart, driver, "button");
		return BrowserActions.getTextFromAttribute(driver, btnAddToCart, "disabled", "Number");
	}
	
	/**
	 * To click on 'Shop Now' button
	 * @throws Exception - Exception
	 */
	public void tapShopNow()throws Exception{
		BrowserActions.clickOnElementX(btnShopNow, driver, "Shop Now Button");
	}
	
	/**
	 * To click on 'Add all Product To Bag' button
	 * @throws Exception - Exception
	 */

	public void clickAddallProductToBag()throws Exception{
		Utils.waitForElement(driver, btnAddAllToBag);
		BrowserActions.scrollToView(btnAddAllToBag, driver);
		BrowserActions.javascriptClick(btnAddAllToBag, driver, "Add all to Bag To Cart Button ");
	}

	/**
	 * To verify content asset properties of product
	 * @param property -
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyProperties(String...property)throws Exception {
		boolean state = false;
		WebElement elem = null;
		if(Utils.getCurrentBrand().equals(Brand.ww)) {
			if(Utils.isDesktop()) {
				elem = divHeadAsset_WW_Desktop;
			} else if(Utils.isMobile()) {
				elem = divHeadAsset_WW_Mobile;
			} else {
				elem = divHeadAsset_WW_Tablet;
			}
			String[] value = BrowserActions.getTextFromAttribute(driver, elem, "src", "Heading Asset").split("\\/");
			if(value[value.length - 1].trim().toLowerCase().contains("ww")) {
				state = true;
			}
		} else {
			String textToVerify = divHeadAsset.getText().trim().replaceAll("\\n", "").replaceAll(" ", "");
			String textFromProperty = null;
			if (property.length > 0) {
				textFromProperty = EnvironmentPropertiesReader.getInstance("demandware").get(property[0]);
			} else {
				state = false;
			}
			Log.message("Text From Element : "+textToVerify);
			Log.message("Text From Property : "+textFromProperty);
			if(textFromProperty.equalsIgnoreCase(textToVerify)) {
				state = true;
			}
		}
		return state;
	}
	
	/**
	 * To click on 'Home in breadcrumb' button
	 * @return HomePage - Home Page object
	 * @throws Exception - Exception
	 */
	
	public HomePage clickOnBreadCrumbHome()throws Exception{
		if(Utils.isMobile()) {
			BrowserActions.clickOnElementX(breadcrumbArrowMobile, driver, "BreadCrumb Home element");
		} else {
			BrowserActions.clickOnElementX(lnkBreadcrumbHome, driver, "BreadCrumb Home element");
		}
		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();
	}
	
	/**
	 * To verify given element is clickable
	 * @param ele - Element String
	 * @param obj - Object
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyElementClickable(String ele, Object obj) throws Exception{
		Field f = null;
		String stringValue=null;
		boolean bool=false;
		try {
			f = obj.getClass().getDeclaredField(ele);
			f.setAccessible(true);
		} catch (NoSuchFieldException | SecurityException e1) {
			throw new Exception("No such a field present on this page, Please check the value:: " + ele);
		}
		WebElement element = null;
		try {
			element = ((WebElement) f.get(obj));
			stringValue = element.getAttribute("href");
			if (stringValue != "null") {
				bool=true;
			} 
		} catch (IllegalArgumentException | IllegalAccessException e1) {
			Log.exception(e1);
		}
		return bool;
	}
	
	/**
	 * To click on breadcrumb button
	 * @param elementName -
	 * @param obj -
	 * @param elementDescription -
	 * @throws Exception - Exception
	 */
	public void clickBreadCrumb(String elementName, Object obj, String elementDescription) throws Exception {
		WebElement element =ElementLayer.getElement(elementName, obj);
		BrowserActions.clickOnElementX(element, driver, elementDescription);

		Log.event("Clicked on " + elementDescription);

	}

/**
 * to select a value in Quantity drpdwn
 * @param index - 
 * @return String 
 * @throws Exception - Exception
 */
	public String selectQuantity(int index)throws Exception{
		BrowserActions.clickOnElementX(txtSelectedQuantity, driver, "Quantity dropdown");
		BrowserActions.clickOnElementX(lstQty.get(index-1), driver, "");
		return lstQty.get(index-1).getText().trim();
	}

	/**
	 * To select size of all product with given size index
	 * @param index -
	 * @return String[] array of selected size values
	 * @throws Exception - Exception
	 */
	public String[] selectAllProductSetSizeBasedOnIndex(int index)throws Exception {
		if (lstSizeGrid.size() > 0) {
			BrowserActions.scrollToView(lstSizeGrid.get(0), driver);
			int intNoOfProd;
			String[] productText = new String[lstSizeGrid.size()];
			int NoOfSizeGrid = lstSizeGrid.size();
			for (intNoOfProd = 0; intNoOfProd < NoOfSizeGrid; intNoOfProd++) {
				List<WebElement> lstOfSizes = lstSizeGrid.get(intNoOfProd)
						.findElements(By.cssSelector(".selectable a"));
				BrowserActions.javascriptClick(lstOfSizes.get(index), driver, "Size Swatch");
				Utils.waitForPageLoad(driver);
			}
			return productText;
		}
		return null;
	}

	/**
	 * To select product Set color bases on color index
	 * @param index - 
	 * @return string
	 * @throws Exception - Exception
	 */
	public String[] selectAllProductSetColorBasedOnIndex(int index)throws Exception {
		if(lstColorGrid.size() > 0) {
			int intNoOfProd;
			String[] productText = new String[10];
			List<WebElement> prdGrid = driver.findElements(By.cssSelector(".product-set-item"));
			int NoOfProd = prdGrid.size();
			int NoOfColorGrid = lstColorGrid.size();
			for (intNoOfProd = 0; intNoOfProd < NoOfProd; intNoOfProd++) {
				if(runPltfrm=="mobile"){
					productText[intNoOfProd] = prdGrid.get(intNoOfProd).findElement(By.cssSelector(".product-name.hide-desktop")).getText();
				}else{
					productText[intNoOfProd] = prdGrid.get(intNoOfProd).findElement(By.cssSelector(".product-name.hide-mobile")).getText();
				}


			}
			for (intNoOfProd = 0; intNoOfProd < NoOfColorGrid; intNoOfProd++) {
				BrowserActions.javascriptClick(lstColorGrid.get(index).findElement(By.cssSelector("a")),driver,"Color Swatch");
			}
			return productText;
		}
		return null;
	}

	/**
	 * To click on search button
	 * @throws Exception -
	 */
	public void clickOnSearchButton()throws Exception{
		BrowserActions.clickOnElementX(btnSearch, driver, "Search Button");
	}
	
	/**
	 * To get on no. of products in quick order page
	 * @return int - no.of products
	 * @throws Exception - Exception
	 */
	public int getNoOfProductInQuickOrderPage()throws Exception {
		return lstProducts.size();
	}
	
	/**
	 * To verify default selected color swatches in Quick order
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifydefaultSelectedcolorSwatchesQuickOrder() throws Exception {
		boolean temp=false;
		List<WebElement> swatches= driver.findElements(By.cssSelector("li[class='selectable selected'] img"));
		if(swatches.size()>0)
		{
			temp=true;
		}		
		return temp;
	}

	/**
	 * To remove all the products in page
	 * @return boolean value
	 * @throws Exception - Exception
	 *//*
	public boolean removeAllProducts() throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		boolean flag=false;
		try{
			if((Utils.waitForElement(driver, divPdpMain))){
				for(int i = 0; i < divCloseProduct.size();){
					BrowserActions.scrollToViewElement(divCloseProduct.get(0), driver);
				}
				BrowserActions.clickOnElementX(divCloseProduct.get(0), driver, "Click close button,");
			}
			BrowserActions.actionClick(divCloseProduct.get(0), driver, "Click close button,");

			BrowserActions.clickOnElementX(divCloseProduct.get(0), driver, "Click close button,");

			Utils.waitForPageLoad(driver);
			flag=true;
		}catch(ArrayIndexOutOfBoundsException e){

		}

		return flag;		

	}*/
	
	/**
	 * To remove all the products in page
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean removeAllProducts() throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		List<WebElement> elem = null;
		if (Utils.isMobile()) {
			elem = lstBtnClose_Mobile;
		} else {
			elem = lstBtnClose;
		}
		boolean flag=false;
		try{
			for(int i = elem.size(); i > 0; i--){
				BrowserActions.clickOnElementX(elem.get(i-1), driver, "Click close button");
				Utils.waitForPageLoad(driver);
			}
			flag=true;
		}catch(ArrayIndexOutOfBoundsException e){

		}

		return flag;

	}
	
	/**
	 * To verify content slot
	 * @return boolean value
	 * @throws Exception - Exception
	 */	
	public boolean verifyContentSlot() throws Exception{

		boolean flag=true;
		if(Utils.waitForElement(driver, divContentSlot)){
			if(divContentSlot.getAttribute("style").contains("display: inner-block"))
				flag=false;
			else
				flag=true;

		}
		return flag;


	}
	
	/**
	 * To click on mini cart flyout close button
	 * @throws Exception - Exception
	 */
	public void clickOnMiniCartFlyoutCloseButton() throws Exception {
		if (Utils.waitForElement(driver, btnCloseMiniCartOverLay))
			BrowserActions.clickOnElementX(btnCloseMiniCartOverLay, driver, "Close button");
	}
	
	/**
	 * To get product name in QO page
	 * @return string - product name
	 * @throws Exception - Exception
	 */
	public String getProductNameInQOPage()throws Exception {
		String prdName = BrowserActions.getText(driver, productNameDesktop.get(0), "Product Name");
		return prdName;
	}
	
	/**
	 * To get product count
	 * @return int - product count
	 * @throws Exception - Exception
	 */
	public int getProductCount()throws Exception {
		return formQuickOrderProduct.size();
	}
	
	/**
	 * To verify default selected size swatch in quick order products
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean verifydefaultSelectedsizeSwatchesQuickOrder() throws Exception {
		boolean temp=false;
		String sizeClassName="selectable selected";
		for(int indexValue=0;indexValue<lstSizeSwatches.size();indexValue++){
			if(sizeClassName.equals(lstSizeSwatches.get(indexValue).getAttribute("Class").trim())){
				temp=true;
				break;
			}
		}

		return temp;
	}
	
	/**
	 * To verify products in Overlay
	 * @param count - 
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean verifyProductsInOverlay(int count) throws Exception {
		Utils.waitForElement(driver,addToCartOverlay);
		String countOverlayProduct= BrowserActions.getText(driver, overlayProductAdded, "");
		System.out.println("countOverlayProduct -- --- --" + countOverlayProduct + " -- --- -- --- count" + count);
		if(countOverlayProduct.toLowerCase().contains(count + " items")){
			return true;
		}
		return false;
	}

	/**
	 * To close add to bag overlay
	 * @throws Exception - Exception
	 */
	public void closeAddToBagOverlay() throws Exception {
		try{
			BrowserActions.clickOnElementX(btnCloseMiniCartOverLay, driver, "AddToBag OverLay Close Button");
		}catch(NoSuchElementException e){
			if(Utils.waitForElement(driver, mdlMiniCartOverLay)){
				Log.failsoft("Close Button in minicart overlay not found", driver);
				mdlMiniCartOverLay.sendKeys(Keys.ESCAPE);
				Log.event("Sent Escape Keypress");
			}else{
				if(Utils.waitForElement(driver, errMsgQuantityATS))
					Log.fail("Inventory Problem occured :: " + errMsgQuantityATS.getText(), driver);
				else
					Log.fail("Minicart Overlay not opened", driver);
			}

		}

	}

	/**
	 * TO verify Element's header asset is displayed
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyElementHeadassetDisplayed()throws Exception{
		boolean flag=false;
		if(divHeadAssetWithBreadCrumb.size()==2){
			if(divHeadAssetWithBreadCrumb.get(0).getAttribute("class").contains("breadcrumb")&& divHeadAssetWithBreadCrumb.get(1).getAttribute("class").contains("mainBanner__image")){
				flag=true;	
			}
		}
		return flag;
	}
	
	/**
	 * TO verify shop now
	 * @throws Exception - Exception
	 */
	public void scrollToShopNow()throws Exception{
		BrowserActions.scrollToViewElement(btnShopNow, driver);
	}
	
	/**
	 * TO verify quantity
	 * @throws Exception - Exception
	 */
	public void scrollToQuantity()throws Exception{
		BrowserActions.scrollToViewElement(btnShopNow, driver);
	}


	/**
	 * To Select Size by product with given product index and Size
	 * @param productIndex - 
	 * @param size - 
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean selectSizeByProduct(int productIndex,String... size) throws Exception {
		boolean flag = false;
		if (size.length > 0 && productIndex>0) {
			WebElement formProduct=driver.findElement(By.xpath("//form[@name='dwfrm_quickorder_items_i"+(productIndex-1)+"']"));
			List<WebElement> divProductCount=formProduct.findElements(By.cssSelector(".product-content-quickorder .attribute.size .value li a"));
			for (int i = 0; i < divProductCount.size(); i++) {
				if (divProductCount.get(i).getText().trim().equals(size[0])) {
					BrowserActions.scrollToViewElement(divProductCount.get(i), driver);
					BrowserActions.clickOnElementX(divProductCount.get(i), driver, "Size Swatches ");
					Log.event("Clicked On Expected Size...");
					waitForSpinner();
					flag=true;
					break;
				}
			}
		}	
		return flag;
	}

	/**
	 * To Select Quantity 
	 * @param qty - 
	 * @return Selected quantity
	 * @throws Exception - Exception
	 */
	public String selectQty(String... qty) throws Exception {
		if (Utils.waitForElement(driver, drpQty)) {
			BrowserActions.scrollToViewElement(drpQty, driver);
			Select qtySelect = new Select(drpQty);
			if (qty.length > 0) {
				if (qtySelect.getAllSelectedOptions().get(0).getText().equals(qty[0]) || drpQty.getText().equals(qty[0]))
					return qty[0];
			}
			if (qty.length > 0)
				qtySelect.selectByVisibleText(qty[0].trim());
			else
				qtySelect.selectByIndex(4);

			return drpQty.getAttribute("value") == null ? drpQty.getText() : drpQty.getAttribute("value");
		}

		if (qty.length > 0)
			if (selectedQty1.getText().trim().equals(qty[0]))
				return selectedQty1.getText().trim();


		BrowserActions.scrollToViewElement(selectedQty1, driver);

		if (qty.length > 0) {
			if (Utils.waitForElement(driver,
					driver.findElement(
							By.xpath("//div[@class='quantity']//div[@class='selection-list']//li[contains(text(),'"
									+ qty[0] + "')]")))) {
				WebElement qtyToSelect = driver.findElement(
						By.xpath("//div[@class='quantity']//div[@class='selection-list']//li[contains(text(),'" + qty[0]
								+ "')]"));
				BrowserActions.clickOnElementX(qtyToSelect, driver, "Quantity ");
				Log.event("Clicked on Expected Quantity...");
				BrowserActions.scrollToViewElement(selectedQty1, driver);
				return qty[0];
			} else {
				Log.failsoft("--->>>Given Quantity Not Available...", driver);
				return selectedQty.getText();
			}
		} else {
			int rand = Utils.getRandom(0, lstQtySelectable.size());
			BrowserActions.clickOnElementX(lstQtySelectable.get(rand), driver, "Quantity menu ");
			Log.message("Clicked on random Quantity...");
			BrowserActions.scrollToViewElement(selectedQty1, driver);
			return selectedQty.getText();
		}
	}
	
	/**
	 * TO click on shop now in mobile
	 * @throws Exception - Exception
	 */
	public void clickShopNowOnMobile() throws Exception{
		BrowserActions.clickOnElementX(shopNowMobile, driver, "Shop Now ");
	}
	
	/**
	 * TO open/close shop now in mobile
	 * @throws Exception - Exception
	 */
	public void openCloseShopNowMobile(boolean isOpen) throws Exception{
		if(isOpen && Utils.waitForElement(driver, shopNowMobileClosed)) {
			BrowserActions.clickOnElementX(shopNowMobileClosed, driver, "Shop Now ");
		} else if (!isOpen && Utils.waitForElement(driver, shopNowMobileOpened)) {
			BrowserActions.clickOnElementX(shopNowMobileOpened, driver, "Shop Now ");
		}
	}
	
	/**
	 * TO wait for error message to display
	 * @throws Exception - Exception
	 */
	public void waitForError() throws Exception{
		Utils.waitForElement(driver, txtInvalidErrorMessage, 120);
	}

	/**
	 * To get product price of given product id
	 * @param prdID - 
	 * @return String - Product Price
	 * @throws Exception - Exception
	 */
	public String getProductPrice(String prdID)throws Exception{
		String price = new String();
		try{
			WebElement priceEle = driver.findElement(By.xpath("//div[@class='catalog-item-number'][contains(text(),'"+prdID+"')]//ancestor::form//div[@class='product-price']//span[@class='price-sales price-standard-exist']"));
			BrowserActions.scrollToViewElement(priceEle, driver);
			price = BrowserActions.getText(driver, priceEle, "Product Price");
			Log.event("Product Sale Price :: " + price);
		}catch(NoSuchElementException e){
			try{
				WebElement priceEle = driver.findElement(By.xpath("//div[@class='catalog-item-number'][contains(text(),'"+prdID+"')]//ancestor::form//div[@class='product-price']//span[@class='quick-order-price']"));
				BrowserActions.scrollToViewElement(priceEle, driver);
				price = BrowserActions.getText(driver, priceEle, "Product Price");
				Log.event("Product Quick Order Price :: " + price);
			}catch(NoSuchElementException e1){
				WebElement priceEle = driver.findElement(By.xpath("//div[@class='catalog-item-number'][contains(text(),'"+prdID+"')]//ancestor::form//div[@class='product-price']//span"));
				BrowserActions.scrollToViewElement(priceEle, driver);
				price = BrowserActions.getText(driver, priceEle, "Product Price");
				Log.event("Product Standard Price :: " + price);
			}
		}
		return price;

	}
	
	/**
	 * TO remove all items from QO Page
	 * @throws Exception - Exception
	 */
	public void removeAllItemsFromQOPage()throws Exception{
		for(int i = lstBtnClose.size()-1; i >= 0 ; i--){
			BrowserActions.clickOnElementX(lstBtnClose.get(i), driver, (i+1) + "th Item's Remove Button");
			Log.event((i+1) + "th Item removed from Cart");
			Utils.waitForPageLoad(driver);
		}
	}
	/**
	 * To verify color is out of stock
	 * @param coloSwtchId -
	 * @param secondColor -
	 * @param size -
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyColorIsOutOfStock(String coloSwtchId,String secondColor,String size) throws Exception {
			
			WebElement colorSwatch= driver.findElement(By.cssSelector("li[data-swatchvalue='"+secondColor+"'] img"));
			BrowserActions.clickOnElement(colorSwatch, driver, "Clicking on color");
			WebElement sizeSwatch= driver.findElement(By.cssSelector(".swatches.size li[data-swatchvalue='"+size+"']"));
			BrowserActions.clickOnElement(sizeSwatch, driver, "Clicking on color");
			WebElement firstColorSwatch= driver.findElement(By.cssSelector("li[data-swatchvalue='"+coloSwtchId+"']"));
			if (firstColorSwatch.getAttribute("class").contains("unselectable"))
				return true;
			else
				return false;
	}
	
	/**
	 * To click on size chart
	 * @throws Exception - Exception
	 */
	public void clickOnSizeChart() throws Exception {
		BrowserActions.javascriptClick(sizeChart, driver, "Size Chart");
		Utils.waitForElement(driver, sizeChartDescription);
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To close size chart
	 * @throws Exception -
	 */
	public void closeSizeChart() throws Exception {
		BrowserActions.javascriptClick(sizeChartClose, driver, "Size Chart");
		Utils.waitUntilElementDisappear(driver, sizeChartClose);
	}

	@FindBy(css = "form[name='dwfrm_quickorder_items_i0']")
	WebElement firstProduct;
	
	@FindBy(css = "form[name='dwfrm_quickorder_items_i1']")
	WebElement secondProduct;
	
	@FindBy(css = "a.breadcrumb-element")
	WebElement bcCurrentMobile;
	
	@FindBy(css = ".quick-order-breadcrumb")
	WebElement qcBreadCrumb;
	
	@FindBy(css = "#header .top-banner")
	WebElement headerTopBanner;
	
	@FindBy(css = "#main")
	WebElement divMain;
	
	@FindBy(css = ".mainBanner .content-asset .catalog-banner .short-description")
	WebElement divHeadContentAsset;
	
	@FindBy(css = ".mainBanner .content-asset img.hide-mobile.hide-desktop")
	WebElement divHeadContentAssetImg_Tablet;
	
	@FindBy(css = ".mainBanner .content-asset img.hide-mobile.hide-tablet")
	WebElement divHeadContentAssetImg_Desktop;
	
	@FindBy(css = ".mainBanner .content-asset img.hide-desktop.hide-tablet")
	WebElement divHeadContentAssetImg_Mobile;
	
	@FindBy(css = ".mainBanner .content-asset img")
	List<WebElement> lstImgHeadContentAsset;
	
	@FindBy(css = ".select-size-text")
	WebElement btnAddAllToBagSizeError;

	@FindBy(css = "form[name*='dwfrm_quickorder_items_i']")
	List<WebElement> lstProducts;
	
	@FindBy(css = ".minicartpopup .mini-cart-product")
	List<WebElement> lstProductsInATBMdl;
	
	@FindBy(css = ".product-quickorder-detail .hide-desktop.hide-tablet .catalog-item-number")
	List<WebElement> lstProductItemNumber_Mobile;
	
	@FindBy(css = ".ui-button-icon.ui-icon.ui-icon-closethick")
	WebElement btnCloseAddToBagOverlay;
	
	/**
	 * To get product Id by index
	 * @param index - index of the product
	 * @return prdId - Id to return
	 * @throws Exception - Exceptions
	 */
	public String getProductIDByIndex(int index)throws Exception{
		String prdId = null;
		if(Utils.isMobile()) {
			prdId = BrowserActions.getText(driver, lstProductItemNumber_Mobile.get(index - 1), "Item Id");
		} else {
			prdId = driver.findElement(By.cssSelector("form[name='dwfrm_quickorder_items_i"+(index-1)+"'] .catalog-item-number")).getText();
		}
		Log.event("Extracted Product ID :: " + prdId);
		return prdId.trim();
	}
	
	/**
	 * Mouse hover in the Add all to bag button
	 * @throws Exception - Exception
	 */
	public void mouseHoverOnAddAllToBag()throws Exception{
		if(Utils.isDesktop()) {
			BrowserActions.mouseHover(driver, btnAddAllToBag, "Add All To Bag");
		} else {
			BrowserActions.scrollToViewElement(btnAddAllToBag, driver);
			BrowserActions.clickOnElement(btnAddAllToBag, driver, "Add All To Bag");
		}
	}
	
	/**
	 * Select size for all products
	 * @throws Exception - Exception
	 */
	public void selectSizeForAllPrd() throws Exception {
		for(WebElement prd : lstProducts) {
			List<WebElement> lstSizeSwatchesSelectable = prd.findElements(By.cssSelector(".swatches.size li.selectable a"));
			BrowserActions.scrollToViewElement(lstSizeSwatchesSelectable.get(0), driver);
			BrowserActions.clickOnElementX(lstSizeSwatchesSelectable.get(0), driver, "Size Swatch");
			waitForSpinner();
		}
	}
	
	/**
	 * To open or close all the shop now section in product set
	 * @param isOpen - true, if needed to open all else false
	 * @throws Exception - Exception
	 */
	public void openOrCloseAllShopNowSectionInPrdSet(boolean isOpen) throws Exception {
		if (Utils.isMobile()) {
			int size = shopNowWindow_ProductSet.size();
			if(isOpen) {
				for (int i=0; i<size; i++) {
					String opened = BrowserActions.getTextFromAttribute(driver, shopNowWindow_ProductSet.get(i), "class", "Shop Now");
					if (!(opened.toLowerCase().contains("open"))) {
						BrowserActions.clickOnElement(shopNowWindow_ProductSet.get(i), driver, "Shop Now");
					}
				}
			} else {
				for (int i=0; i<size; i++) {
					String opened = BrowserActions.getTextFromAttribute(driver, shopNowWindow_ProductSet.get(i), "class", "Shop Now");
					if (opened.toLowerCase().contains("open")) {
						BrowserActions.javascriptClick(shopNowWindow_ProductSet.get(i), driver, "Shop Now");
					}
				}
			}
		}
		Utils.waitForPageLoad(driver);
	}
	
	
	/**
	 * Get No of product in the add to bag overlay
	 * @return value - No of product to return
	 * @throws Exception - Exception
	 */
	public int getnoOfProductInATBMdl()throws Exception{
		int value = lstProductsInATBMdl.size()-2;
		Log.event("Number of Product Available :: " + value);
		return value;
	}
	
	@FindBy(css = ".slick-next")
	WebElement icoNextArrowInSlickTrack;
	
	/**
	 * To select index th alternate image
	 * @param index - 
	 * @throws Exception - 
	 */
	public void selectAlternateImageByIndex(int index)throws Exception{
		
		int sizeAI = lstAlternateImages.size();
		
		if(index > sizeAI) {
			Log.failsoft("Given index is greater than no.of alternative images displaying");
		} else {
			String isHidden = lstProductAltThumbnail.get(index - 1).getAttribute("aria-hidden");;
			while (isHidden.equalsIgnoreCase("true")) {
				BrowserActions.clickOnElement(icoNextArrowInSlickTrack, driver, "Next Arrow");
				isHidden = lstProductAltThumbnail.get(index - 1).getAttribute("aria-hidden");
			}
			BrowserActions.clickOnElement(lstAlternateImages.get(index-1), driver, index + "th Alternate Image");
			Utils.waitForPageLoad(driver);
		}
	}
	
	@FindBy(css = "#thumbnails .thumb.selected img")
	WebElement selectedAltImage;
	
	@FindBy(css = "#thumbnails .thumb.selected a")
	WebElement lnkSelectedAltImage;
	
	public String getselectedAltImgCode()throws Exception{
		String codes[] = selectedAltImage.getAttribute("src").trim().split("\\?")[0].split("\\/");
		String code = codes[codes.length-1];
		return code;
	}
	
	public String getPrimaryImgCode()throws Exception{
		String codes[] = imgPrimaryImage.getAttribute("src").trim().split("\\?")[0].split("\\/");
		String code = codes[codes.length-1];
		return code;
	}
	
	@FindBy(css = ".thumbnail-link")
	List<WebElement> lstProductThumbnaillink;
	
	/**
	 * To get number of alternate images available in slick slide
	 * @return Integer - 
	 * @throws Exception -
	 */
	public int getNoOfThumbnailProdImages() throws Exception {
		try{
			int NoOfThumbnails = lstProductThumbnaillink.size();
			return NoOfThumbnails;
		} catch(Exception e) {
			return 0;
		}
	}
	
	@FindBy(css = ".thumb.slick-slide")
	List<WebElement> lstProductAltThumbnail;
	
	/**
	 * To check the alternate image is displayed fully/not when the image is clicked in slick slider.
	 * @param imgToVerify - 
	 * @param statusToVerify - 
	 * @return boolean - 
	 * @throws Exception -
	 */
	public boolean verifyAlternateImageDisplayStatus(int imgToVerify, String statusToVerify) throws Exception {
		String displayStatus = lstProductAltThumbnail.get(imgToVerify - 1).getAttribute("aria-hidden");
		if (displayStatus.equals(statusToVerify)) {
			return true;
		}
		return false;
	}
	
	@FindBy(css = "#thumbnails .slick-next.slick-arrow.slick-disabled")
	WebElement btnNextImageDisable;
	
	@FindBy(css = "#thumbnails .slick-prev.slick-arrow.slick-disabled")
	WebElement btnPrevImageDisable;

	@FindBy(css = "#thumbnails .slick-prev.slick-arrow")
	WebElement btnPrevImageEnable;

	@FindBy(css = "#thumbnails .slick-next.slick-arrow")
	WebElement btnNextImageEnable;
	
	/**
	 * To click on slick slide left/right arrows
	 * @param direction - 'left' / 'right'
	 * @throws Exception -
	 */
	public void scrollAlternateImageInSpecifiedDirection(String direction) throws Exception {
		if (direction.equals("Next")) {
			while (!(Utils.waitForElement(driver, btnNextImageDisable))) {
				BrowserActions.clickOnElementX(btnNextImageEnable, driver, "Next Arrow in Alternate product image ");
			}
		} else if (direction.equals("Prev")) {
			while (!(Utils.waitForElement(driver, btnPrevImageDisable))) {
				BrowserActions.clickOnElementX(btnPrevImageEnable, driver, "Prev Arrow in Alternate product image ");
			}
		}
	}
	
	@FindBy(css = "div.zoomWindow")
	WebElement divZoomWindowContainer;
	
	/**
	 * To zoom the product image in the Quick Shop overlay
	 * @throws Exception - Exception
	 */
	public void zoomProductImage()throws Exception{
		BrowserActions.scrollInToView(imgPrimaryImage, driver);
		BrowserActions.mouseHover(driver, imgPrimaryImage, "");
		Actions action = new Actions(driver);
		action.moveToElement(imgPrimaryImage).build().perform();
		Utils.waitForElement(driver, divZoomWindowContainer);
	}
	
	@FindBy(css = ".zoomLens")
	WebElement divZoomLens;
	
	/**
	 * To verify Xoomlens and zoom window are displayed
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyZoomLensAndZoomWindowAreDisplayed()throws Exception{
		Point lens = divZoomLens.getLocation();
		Point window = divZoomWindowContainer.getLocation();

		if((lens.x != 0 || lens.y != 0) && (window.x != 0 || window.y != 0))
			return true;
		else
			return false;

	}
	
	/**
	 * to verify that the top of zoomed image alligns with the top of product image
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyTopOfZoomedImageAllignsWithTopOfProductImage()throws Exception{
		return BrowserActions.verifyElementsAreInSameRow(driver, divZoomWindowContainer, imgPrimaryImage);
	}
	
	public void expandCollapseProductDetails(String state)throws Exception{
		if(state.equalsIgnoreCase("expand")) {
			if(Utils.waitForElement(driver, btnReadMore))
				BrowserActions.clickOnElement(btnReadMore, driver, "Read More");
			else if(Utils.waitForElement(driver, btnCloseProductDetails))
				Log.event("Already Expanded.");
			else
				Log.fail("Something went wrong.", driver);
		}else {
			if(Utils.waitForElement(driver, btnCloseProductDetails))
				BrowserActions.clickOnElement(btnCloseProductDetails, driver, "Read More");
			else if(Utils.waitForElement(driver, btnReadMore))
				Log.event("Already Collapsed.");
			else
				Log.fail("Something went wrong.", driver);
		}
	}
	
	public String getOrderCatlogNumber()throws Exception{
		String valueFromCatlogNumber = new String();
		if(Utils.isMobile())
		{
			valueFromCatlogNumber = BrowserActions.getText(driver, orderCatlogNumberMobile, "Order Catlog Number");
		}
		else
		{
			valueFromCatlogNumber = BrowserActions.getText(driver, orderCatlogNumberDesktop, "Order Catlog Number");
		}
		
		return valueFromCatlogNumber.trim();
	}
	
}
