package com.fbb.pages;

import java.awt.Color;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

/**
 * ElementLayer page is used to verify each page elements.
 * 
 * We can declare and initialize this class on each page object classes
 */
public class ElementLayer {

	private final WebDriver driver;

	private static EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader
			.getInstance("demandware");

	/**
	 * Constructor class for ElementLayer, here we initializing the driver for
	 * page
	 * @param driver -
	 */
	public ElementLayer(WebDriver driver) {
		this.driver = driver;
	}

	/**
	 * Verify if expected page WebElements are present
	 * <p>If expected element is present on the current page, add to list of
	 * value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - expected WebElement values
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 4 possibilities
	 */
	public boolean verifyPageElements(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				BrowserActions.scrollToViewElement(element, driver);
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if (Utils.waitForElement(driver, element, 2)) {
				actual_elements.add(expEle);
			}
		}
		return Utils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify if expected page WebElements are disabled
	 * <p>If expected element is present on the current page, add to list of
	 * value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - expected WebElement values
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 4 possibilities
	 */
	public boolean verifyPageElementsDisabled(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if (Utils.waitForDisabledElement(driver, element, Utils.maxElementWait)) {
				actual_elements.add(expEle);
			}
		}
		return Utils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify if expected page WebElements are checked/selected 
	 * <p>If expected element checked/selected on the current page, add to list
	 * of value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - expected WebElement values
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 4 possibilities
	 */
	public boolean verifyPageElementsChecked(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if(!Utils.isTablet()) {
				(new WebDriverWait(driver, 5).pollingEvery(250, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class).withMessage("Creat an Event mobel box did not open")).until(ExpectedConditions.visibilityOf(element));
			}
			if (element.isSelected()) {
				actual_elements.add(expEle);
			}
		}
		return Utils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify if expected page WebElements are unchecked/unselected 
	 * <p>If expected element checked/selected on the current page, add to list
	 * of value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - expected WebElement values
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 4 possibilities
	 */
	public boolean verifyPageElementsUnChecked(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if(!Utils.isTablet()) {
				(new WebDriverWait(driver, 5).pollingEvery(250, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class).withMessage("Creat an Event mobel box did not open")).until(ExpectedConditions.visibilityOf(element));
			}
			if (!element.isSelected()) {
				actual_elements.add(expEle);
			}
		}
		return Utils.compareTwoList(expectedElements, actual_elements);
	}


	/**
	 * Verify the lack of presence of page WebElements
	 * <p>If expected element is NOT present on this current page, add to list of
	 * value/fields to actualElement list and then compare to expectedElements
	 * @param expectedNotToSee - expected non-existing WebElement values
	 * @param obj - the page object the elements should not be on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 2 possibilities
	 */
	public boolean verifyPageElementsDoNotExist(List<String> expectedNotToSee, Object obj) throws Exception {
		List<String> nonexisting_elements = new ArrayList<String>();
		for (String expEle : expectedNotToSee) {
			Field f = null;
			WebElement element = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
				element = ((WebElement) f.get(obj));
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such field present on this page, Please check the expected list values:: " + expEle);
			}
			if (!Utils.waitForElement(driver, element, 2)) {
				nonexisting_elements.add(expEle);
			}
		}
		return Utils.compareTwoList(expectedNotToSee, nonexisting_elements);
	}

	/**
	 * Verify if expected page List WebElements are present
	 * <p>If size of the list element is greater than zero and first element from
	 * expected list elements present on the current page, add to list of
	 * value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - expected List WebElement values
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 5 possibilities
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPageListElements(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such fields present on this page, Please check the expected list values:: " + expEle);
			}
			List<WebElement> elements = null;
			try {
				elements = ((List<WebElement>) f.get(obj));
			} catch (ClassCastException | IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if (elements.size() > 0 
					&& Utils.waitForElement(driver, elements.get(0))) {
				actual_elements.add(expEle);
			}
		}
		return Utils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify the lack of presence of page WebElements
	 * <p>If expected element is NOT present on this current page, add to list of
	 * value/fields to actualElement list and then compare to expectedElements
	 * @param expectedNotToSee - expected non-existing WebElement values
	 * @param obj - the page object the elements should not be on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 2 possibilities
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPageListElementsDoNotExist(List<String> expectedNotToSee, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedNotToSee) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such fields present on this page, Please check the expected list values:: " + expEle);
			}
			List<WebElement> elements = null;
			try {
				elements = ((List<WebElement>) f.get(obj));
			} catch (ClassCastException | IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if (elements.size() == 0) {
				actual_elements.add(expEle);
			}
		}
		return Utils.compareTwoList(expectedNotToSee, actual_elements);
	}

	/**
	 * Verify if expected list of page WebElements are disabled
	 * <p>If expected list element is present on the current page, add to list of
	 * value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - expected WebElement values
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 4 possibilities
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPageListElementsDisabled(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			List<WebElement> elements = null;
			try {
				elements = ((List<WebElement>) f.get(obj));
			} catch (ClassCastException |IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if (elements.size() > 0 
					&& Utils.waitForDisabledElement(driver, elements.get(0), Utils.maxElementWait)) {
				actual_elements.add(expEle);
			}
		}
		return Utils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify contents of a WebElement equals a passed in string variable
	 * 
	 * @param textToVerify - expected text
	 *            
	 * @param elementToVerify
	 *            - element to verify the text of
	 * @param obj -
	 * @return true if text on screen matches passed variable contents
	 */
	public boolean verifyTextEquals(String elementToVerify, String textToVerify, Object obj) {
		boolean status = false;
		WebElement element = null;
		try{
			Field elementField = obj.getClass().getDeclaredField(elementToVerify);
			elementField.setAccessible(true);
			element = ((WebElement) elementField.get(obj));
			String eleText = element.getText().trim().replaceAll("\\s+", " ");
			Log.event("Text from element:: " + eleText);
			return (eleText.equals(textToVerify));
		}//try
		catch(Exception e){
			e.printStackTrace();

		}//catch


		return status;
	}

	/**
	 * To get the element which have exact variable name
	 * @param expectedEle - String "Expected Element"
	 * @param obj -
	 * @return WebElement - Actual WebElement with expected Element variable name
	 * @throws Exception -
	 */
	public static WebElement getElement(String expectedEle, Object obj)throws Exception{
		WebElement element = null;
		try{
			Field elementField = obj.getClass().getDeclaredField(expectedEle);
			elementField.setAccessible(true);
			element = ((WebElement) elementField.get(obj));
		}
		catch(Exception e){
			e.printStackTrace();

		}
		return element;
	}

	/**
	 * To get the element which have exact variable name
	 * @param expectedEle - String "Expected Element"
	 * @param obj -
	 * @return WebElement - Actual WebElement with expected Element variable name
	 * @throws Exception -
	 */
	@SuppressWarnings("unchecked")
	public static List<WebElement> getListElement(String expectedEle, Object obj)throws Exception{
		List<WebElement> element = null;
		try{
			Field elementField = obj.getClass().getDeclaredField(expectedEle);
			elementField.setAccessible(true);
			element = ((List<WebElement>) elementField.get(obj));
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return element;
	}

	/**
	 * Verify the List element size
	 * @param elementName - element to be checked
	 * @param sizeToVerify - size to be verified
	 * @param obj - driver object
	 * @return boolean - if size matches - true if size not match - false
	 * @throws Exception - Exception
	 */
	public boolean verifyListElementSize(String elementName, int sizeToVerify,  Object obj)throws Exception{
		List<WebElement> listElement = getListElement(elementName, obj);
		boolean state = true;
		Log.event(">>>>>>>List element size : " +listElement.size());
		Log.event(">>>>>>>Given size : " +sizeToVerify);
		if (!(listElement.size() == sizeToVerify)) {
			state = false;
		}
		return state;
	}

	/**	
	 * To verify the list of page elements are displayed
	 * @param expectedElements -
	 * @param obj -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean VerifyPageElementDisplayed(List<String> expectedElements, Object obj)throws Exception{

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				BrowserActions.scrollInToView(element, driver);
				if (element.isDisplayed()) {
					actual_elements.add(expEle);
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			} catch (NoSuchElementException e){
				Log.event("No Such Element present in Page :: " + expEle);
			}

		}
		return Utils.compareTwoList(expectedElements, actual_elements);
	}

	/**	
	 * To verify the list of page elements are displayed
	 * @param expectedElements -
	 * @param obj -
	 * @return -boolean
	 * @throws Exception -
	 */
	public boolean VerifyElementDisplayed(List<String> expectedElements, Object obj)throws Exception{

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				if (element.isDisplayed()) {
					actual_elements.add(expEle);
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			} catch (NoSuchElementException e){
				Log.event("No Such Element present in Page :: " + expEle);
				return false;
			}

		}
		return Utils.compareTwoList(expectedElements, actual_elements);

	}

	/**
	 * To verify the list of page elements are displayed
	 * @param expectedElements -
	 * @param obj -
	 * @return -boolean
	 * @throws Exception -
	 */
	@SuppressWarnings("unchecked")
	public boolean VerifyPageListElementDisplayed(List<String> expectedElements, Object obj)throws Exception{

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			List<WebElement> element = null;
			try {
				element = ((List<WebElement>) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			boolean flag = true;
			for(int i = 0; i < element.size(); i++){
				if (!element.get(i).isDisplayed()) {
					flag = false;
				}
			}
			if(flag)
				actual_elements.add(expEle);
		}
		return Utils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * to verfigy the List of page elements are not displayed
	 * @param expectedElements - elemment 
	 * @param obj -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean VerifyPageElementNotDisplayed(List<String> expectedElements, Object obj)throws Exception{

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
				actual_elements.add(expEle);
			}
			catch (NoSuchElementException err) {
				actual_elements.add(expEle);
			}

			try {
				if (element.isDisplayed() == false) {
					actual_elements.add(expEle);
				}else {
					BrowserActions.scrollToViewElement(element, driver);
				}
			}catch(NoSuchElementException e) {
				actual_elements.add(expEle);
			}

		}
		return Utils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * to verfigy the List of page elements are not displayed
	 * @param expectedElements - elemment
	 * @param obj - 
	 * @return -boolean
	 * @throws Exception -
	 */
	@SuppressWarnings("unchecked")
	public boolean VerifyPageListElementNotDisplayed(List<String> expectedElements, Object obj)throws Exception{

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			List<WebElement> element = null;
			try {
				element = ((List<WebElement>) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			boolean flag = true;
			for(int i = 0; i < element.size(); i++){
				if (element.get(i).isDisplayed() == true) {
					flag = false;
				}
				if(flag)
					actual_elements.add(expEle);
			}
		}
		return Utils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * To get element's position as hash map
	 * @param elementName -
	 * @param obj -
	 * @return LinkedHashMap -
	 * @throws Exception -
	 */
	public LinkedHashMap<String, String> getElementXandYAxis(String elementName, Object obj)throws Exception{
		LinkedHashMap<String, String> axisDetails =  new LinkedHashMap<String, String>();;
		WebElement element = getElement(elementName, obj);

		Point eleAxis = element.getLocation();

		String AxisX = Integer.toString(eleAxis.x);
		String AxisY = Integer.toString(eleAxis.y);

		axisDetails.put("XAxis", AxisX);
		axisDetails.put("YAxis", AxisY);

		return axisDetails;
	}

	/**
	 * To verify the first element displayed below the second element
	 * @param firstElement -
	 * @param secondElement -
	 * @param obj -
	 * @return 'true' / 'false'
	 * @throws Exception -
	 */
	public boolean verifyElementDisplayedBelow(String firstElement, String secondElement, Object obj)throws Exception{
		Field f = obj.getClass().getDeclaredField(firstElement);
		f.setAccessible(true);
		WebElement firstEle = ((WebElement) f.get(obj));

		Field f1 = obj.getClass().getDeclaredField(secondElement);
		f1.setAccessible(true);
		WebElement secondEle = ((WebElement) f1.get(obj));

		int firstPoint = firstEle.getLocation().getY();
		System.out.println("firstElement Y: "+ firstPoint);
		int secondPoint = secondEle.getLocation().getY();
		System.out.println("secondElement Y: "+ secondPoint);
		if(firstPoint < secondPoint)
			return true;
		else 
			return false;
	}

	/**
	 * To verify the List of String in ascending/descending order
	 * @param strList - List of String
	 * @param order - 'ascending' / 'descending'
	 * @return 'true' - 'false'
	 * @throws Exception -
	 */
	public boolean verifyListInOrder(List<String> strList, String order)throws Exception{
		boolean flag = true;

		for(int i = 0; i < strList.size() - 1; i++)
			for(int j = i + 1; j < strList.size(); j++){
				if(order.equalsIgnoreCase("ascending")){
					if(strList.get(i).compareTo(strList.get(j)) >= 0)
						flag = false;
				}else{
					if(strList.get(i).compareTo(strList.get(j)) <= 0)
						flag = false;
				}
			}

		return flag;
	}

	/**
	 * Verify the css property for an element
	 * 
	 * @param element
	 *            - WebElement for which to verify the css property
	 * @param cssProperty
	 *            - the css property name to verify
	 * @param expectedValue
	 *            - the actual css value of the element
	 * @param obj -
	 * @return boolean
	 * 
	 *@throws Exception -
	 */
	public boolean verifyCssPropertyForElement(String element, String cssProperty, String expectedValue, Object obj)throws Exception {
		boolean result = false;
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		BrowserActions.scrollToViewElement(webElement, driver);
		String actualCSSValue = null;
		try {
			actualCSSValue = webElement.getCssValue(cssProperty);
		} catch (NoSuchElementException ex) {
			Log.reference(element + " is not displayed");
			return false;
		}

		Log.event("Element CSS Value :: "+actualCSSValue);
		Log.event("Expected CSS Value :: "+expectedValue);
		if (actualCSSValue.toLowerCase().contains(expectedValue.toLowerCase())) {
			result = true;
		}
		return result;
	}
	
	/**
	 * Verify the css property for an element
	 * 
	 * @param element
	 *            - WebElement for which to verify the css property
	 * @param cssProperty
	 *            - the css property name to verify
	 * @param expectedValue
	 *            - the actual css value of the element
	 * @param obj -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyComputedCssPropertyForElement(String element, String cssProperty, String expectedValue, Object obj)throws Exception {
		boolean result = false;
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		String actualValue = (String) executor.executeScript("return window.getComputedStyle(arguments[0]).getPropertyValue('"+cssProperty+"')",webElement);

		Log.event("Element CSS Value :: "+actualValue);
		Log.event("Expected CSS Value :: "+expectedValue);
		if(actualValue.contains(expectedValue)){
			result = true;
		}
		return result;
	}

	/**
	 * Verify the css property for an element
	 * 
	 * @param element
	 *            - WebElement for which to verify the css property
	 * @param cssProperty
	 *            - the css property name to verify
	 * @param expectedValue
	 *            - the actual css value of the element
	 * @param psuedoEle -
	 * @param obj -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyCssPropertyForPsuedoElement(String element,String psuedoEle, String cssProperty, String expectedValue, Object obj)throws Exception {
		boolean result = false;
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		BrowserActions.scrollToViewElement(webElement, driver);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		String actualValue = (String) executor.executeScript("return window.getComputedStyle(arguments[0], ':"+psuedoEle+"').getPropertyValue('"+cssProperty+"')",webElement);
		Log.event("Actual Value :: " + actualValue);
		Log.event("Expected Value :: " + expectedValue);
		if(actualValue.contains(expectedValue)){
			result = true;
		}
		return result;
	}

	/**
	 * Verify the css property for an element
	 * 
	 * @param element
	 *            - WebElement for which to verify the css property
	 * @param cssProperty
	 *            - the css property name to verify
	 * @param psuedoEle -
	 * @param expectedValue
	 *            - the actual css value of the element
	 * @param psuedoEle -
	 * @param obj -
	 * @return boolean
	 * @throws Exception -
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyCssPropertyForPsuedoListElement(String element,String psuedoEle, String cssProperty, String expectedValue, Object obj)throws Exception {
		boolean result = false;
		int missingcount = 0;
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		List<WebElement> webElement = ((List<WebElement>) f.get(obj));
		for(WebElement ele : webElement){
			BrowserActions.scrollToViewElement(ele, driver);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			String actualValue = (String) executor.executeScript("return window.getComputedStyle(arguments[0], ':"+psuedoEle+"').getPropertyValue('"+cssProperty+"')",ele);
			Log.event("Actual Value :: " + actualValue);
			Log.event("Expected Value :: " + expectedValue);
			if(actualValue.contains(expectedValue)){
				result = true;
			}else missingcount++;
		}
		return result && (missingcount==0);
	}

	/**
	 * To verify attribute value of element contains given string
	 * @param element - 
	 * @param attributeName - 
	 * @param actualValue -
	 * @param obj - 
	 * @return boolean - 
	 * @throws Exception - 
	 */
	public boolean verifyAttributeForElement(String element, String attributeName, String actualValue, Object obj)throws Exception {
		boolean result = false;
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		BrowserActions.scrollToViewElement(webElement, driver);
		String actualAttibuteValue = webElement.getAttribute(attributeName);
		
		actualAttibuteValue = actualAttibuteValue.replaceAll("&amp;", "");
		actualValue = actualValue.replaceAll("&", "");
		
		Log.event("--->>>Actual Value ::" + attributeName + " :: " + actualAttibuteValue);
		Log.event("--->>>Expected Value ::"  + actualValue);

		if (actualAttibuteValue.contains(actualValue)) {
			result = true;
		}
		return result;
	}
	
	/**
	 * To get an attribute of an element 
	 * @param element - Declared element name
	 * @param attributeName - Name of the Attribute get the value
	 * @param obj - Page object where the element is declared
	 * @return String - The value extracted from attribute
	 * @throws Exception - Exception 
	 */
	public String getAttributeForElement(String element, String attributeName, Object obj) throws Exception{
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		BrowserActions.scrollToViewElement(webElement, driver);
		String attibuteValue = webElement.getAttribute(attributeName);
		Log.event("Attribute from Element:: " + attibuteValue);
		return attibuteValue;
	}

	/**
	 * Verify the css property for an element
	 * 
	 * @param element
	 *            - WebElement for which to verify the css property
	 * @param cssProperty
	 *            - the css property name to verify
	 * @param actualValue
	 *            - the actual css value of the element
	 * @param obj -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyCssPropertyForParentElement(String element, String cssProperty, String actualValue, Object obj)throws Exception {
		boolean result = false;
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = (WebElement) f.get(obj);
		WebElement parentEle = webElement.findElement(By.xpath(".."));
		String actualClassProperty = parentEle.getCssValue(cssProperty);
		System.out.println("--->>>Parent : " + cssProperty + " :: " + actualClassProperty);
		if (actualClassProperty.contains(actualValue)) {
			result = true;
		}
		return result;
	}

	/**
	 * To verify attribute value of parent element of given element contains given string
	 * @param element - 
	 * @param property - 
	 * @param actualValue -
	 * @param obj - 
	 * @return boolean - 
	 * @throws Exception - 
	 */
	public boolean verifyAttributeForParentElement(String element, String property, String actualValue, Object obj)throws Exception {
		boolean result = false;
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = (WebElement) f.get(obj);
		WebElement parentEle = webElement.findElement(By.xpath(".."));
		String actualClassProperty = parentEle.getAttribute(property);
		System.out.println("--->>>Parent : " + property + " :: " + actualClassProperty);
		if (actualClassProperty.contains(actualValue)) {
			result = true;
		}
		return result;
	}

	/**
	 * Verify the css property for an element
	 * 
	 * @param element
	 *            - WebElement for which to verify the css property
	 * @param cssProperty
	 *            - the css property name to verify
	 * @param actualValue
	 *            - the actual css value of the element
	 * @param obj -           
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyCssPropertyForListElement(String element, String cssProperty, String actualValue, Object obj)throws Exception {
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		@SuppressWarnings("unchecked")
		List<WebElement> webElement = ((List<WebElement>) f.get(obj));
		for(int i = 0; i < webElement.size(); i++){
			System.out.println("----->>>>>" + element + "(" + (i+1) + "):: " + webElement.get(i).getCssValue(cssProperty));
			if(!webElement.get(i).getCssValue(cssProperty).contains(actualValue)){
				return false;
			}
		}
		return true;
	}

	/**
	 * Verify the Attribute value for an element
	 * 
	 * @param element
	 *            - WebElement for which to verify the css property
	 * @param attributeName
	 *            - the attributeName to verify
	 * @param actualValue
	 *            - the actual css value of the element
	 * @param obj -           
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyAttibuteForListElement(String element, String attributeName, String actualValue, Object obj)throws Exception {
		try {
			Log.event("Value to verify :: " + actualValue);
			List<WebElement> webElement = getListElement(element, obj);
			for(int i = 0; i < webElement.size(); i++){
				Log.event("Value from Element-" + (i+1) + " :: " + webElement.get(i).getAttribute(attributeName));
				if(!webElement.get(i).getAttribute(attributeName).contains(actualValue)){
					return false;
				}
			}
		} catch (NoSuchElementException ex) {
			Log.event("<<<The element is not displayed in the page>>> "+element);
			return false;
		}
		return true;
	}

	/**
	 * Verify the css property for an element
	 * 
	 * @param expectedElements
	 *            - WebElement for which to verify the css property
	 * @param cssProperty
	 *            - the css property name to verify
	 * @param actualValue
	 *            - the actual css value of the element
	 * @param obj -
	 *
	 * @return boolean
	 * 
	 * @throws Exception -
	 */
	public boolean verifyCssPropertyForListOfElement(List<String> expectedElements, String cssProperty, String actualValue, Object obj)throws Exception {

		List<String> actual_elements = new ArrayList<String>();;
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				BrowserActions.scrollToViewElement(element, driver);
				if (element.getCssValue(cssProperty).contains(actualValue)) {
					actual_elements.add(expEle);
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			} catch (NoSuchElementException e){
				Log.failsoft("No Such Element present in Page :: " + expEle);
			}

		}
		return Utils.compareTwoList(expectedElements, actual_elements);

	}

	/**
	 *  Verify the Attribute Value for List of elements
	 * @param expectedElements -
	 * @param attributeName -
	 * @param actualValue -
	 * @param obj -
	 * @return boolean 
	 * @throws Exception -
	 */
	public boolean verifyAttributeForListOfElement(List<String> expectedElements, String attributeName, String actualValue, Object obj)throws Exception {

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				BrowserActions.scrollToViewElement(element, driver);
				Log.event("Actual Value :: " + element.getAttribute(attributeName));
				Log.event("Expected Value :: " + actualValue);
				if (element.getAttribute(attributeName).contains(actualValue)) {
					actual_elements.add(expEle);
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			} catch (NoSuchElementException e){
				Log.reference("No Such Element present in Page :: " + expEle);
			} catch (NullPointerException e){
				Log.reference("Element throws Null Pointer Exception :: " + expEle);
			}
		}
		return Utils.compareTwoList(expectedElements, actual_elements);

	}

	/**
	 * Wrapper to verify if the first element is right to the second element in the UI
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @param rightElement
	 *            : WebElement which is present right side in the UI
	 * @param obj -
	 * @return: Boolean
	 * @param leftElement
	 *            : WebElement which is present left side in the UI
	 * @throws Exception -
	 */
	public Boolean verifyHorizontalAllignmentOfElements(WebDriver driver,
			String rightElement, String leftElement, Object obj)
					throws Exception {

		Field f = obj.getClass().getDeclaredField(rightElement);
		f.setAccessible(true);
		WebElement elementRight = ((WebElement) f.get(obj));

		Field f1 = obj.getClass().getDeclaredField(leftElement);
		f1.setAccessible(true);
		WebElement elementLeft = ((WebElement) f1.get(obj));
		
		Point eleRight = null;
		Point eleLeft = null;
		
		if (Utils.waitForElement(driver, elementRight)) {
			BrowserActions.scrollInToView(elementRight, driver);
			eleRight = elementRight.getLocation();
		} else {
			Log.event("Right element not displaying : "+rightElement);
			return false;
		}
		
		if (Utils.waitForElement(driver, elementLeft)) {
			BrowserActions.scrollInToView(elementLeft, driver);
			eleLeft = elementLeft.getLocation();
		} else {
			Log.event("Left element not displaying : "+leftElement);
			return false;
		}
		
		Log.event(eleLeft.x + "-->" + eleRight.x);
		if(eleLeft.x < eleRight.x)
			return true;
		else
			return false;
	}
	
	/**
	 * Wrapper to verify if the first element is right to the second element in the UI
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @param rightElement
	 *            : WebElement which is present right side in the UI
	 * @param obj -
	 * @return: Boolean
	 * @param leftElement
	 *            : WebElement which is present left side in the UI
	 * @throws Exception -
	 */
	public Boolean verifyHorizontalAllignmentOfElementsWithoutScroll(WebDriver driver,
			String rightElement, String leftElement, Object obj)
					throws Exception {

		Field f = obj.getClass().getDeclaredField(rightElement);
		f.setAccessible(true);
		WebElement elementRight = ((WebElement) f.get(obj));

		Field f1 = obj.getClass().getDeclaredField(leftElement);
		f1.setAccessible(true);
		WebElement elementLeft = ((WebElement) f1.get(obj));
		
		Point eleRight = null;
		Point eleLeft = null;
		
		if (Utils.waitForElement(driver, elementRight)) {
			eleRight = elementRight.getLocation();
		} else {
			Log.event("Right element not displaying : "+rightElement);
			return false;
		}
		
		if (Utils.waitForElement(driver, elementLeft)) {
			eleLeft = elementLeft.getLocation();
		} else {
			Log.event("Left element not displaying : "+leftElement);
			return false;
		}
		
		Log.event(eleLeft.x + "-->" + eleRight.x);
		if(eleLeft.x < eleRight.x)
			return true;
		else
			return false;
	}

	/**
	 * To verify elements in the list are horizontally aligned
	 * @param element - Name of element
	 * @param obj - Page object element declared in
	 * @return boolean - true if aligned correctly else false
	 * @throws Exception - Exception
	 */
	public boolean verifyHorizontalAlignmentOfListElementItem(String element, Object obj) throws Exception{
		boolean status = true;
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		@SuppressWarnings("unchecked")
		List<WebElement> elementList = (List<WebElement>) (f.get(obj));
		int size = elementList.size();
		if(size > 1) {
			for(int k=0; k < size - 1; k++) {

				WebElement elemLeft = elementList.get(k);
				WebElement elemRight = elementList.get(k+1);

				Point eleRight = elemRight.getLocation();
				Point eleLeft = elemLeft.getLocation();

				Log.event(eleLeft.x + "-->" + eleRight.x);

				if(eleLeft.x > eleRight.x) {
					status = false;
				}
			}
		} else {
			Log.message("The size of the list element is "+size, driver);
			return false;
		}
		return status;
	}

	/**
	 * To verify elements in the list are veritically aligned
	 * @param element - Name of element
	 * @param obj - Page object element declared in
	 * @return boolean - true is aligned correctly else false
	 * @throws Exception - Exception
	 */
	public boolean verifyVerticalAlignmentOfListElementItem(String element, Object obj) throws Exception{
		boolean status = true;
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		@SuppressWarnings("unchecked")
		List<WebElement> elementList = (List<WebElement>) (f.get(obj));
		int size = elementList.size();
		if(size > 1) {
			for(int k=0; k < size - 1; k++) {

				WebElement elemAbove = elementList.get(k);
				WebElement elemBelow = elementList.get(k+1);

				Point eleAbove = elemAbove.getLocation();
				Point eleBelow = elemBelow.getLocation();

				Log.event(eleAbove.y + "-->" + eleBelow.y);

				if(eleAbove.y > eleBelow.y) {
					status = false;
				}
			}
		} else {
			Log.message("The size of the list element is "+size, driver);
			return false;
		}
		return status;
	}

	/**
	 * Wrapper to verify if the first element is above the second element in the UI
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @param aboveElement
	 *            : WebElement which is present above in the UI
	 * @return: Boolean
	 * @param obj -
	 * @param belowElement
	 *            : WebElement which is present below in the UI
	 * @throws Exception -
	 */
	public Boolean verifyVerticalAllignmentOfElements(WebDriver driver,
			String aboveElement, String belowElement, Object obj)
					throws Exception {

		Field f = obj.getClass().getDeclaredField(aboveElement);
		f.setAccessible(true);
		WebElement elementAbove = ((WebElement) f.get(obj));

		Field f1 = obj.getClass().getDeclaredField(belowElement);
		f1.setAccessible(true);
		WebElement elementBelow = ((WebElement) f1.get(obj));

		try {
			BrowserActions.scrollToView(elementAbove, driver);
		} catch (NoSuchElementException ex) {
			Log.event("***Missing Element :: "+ aboveElement +"***");
			return false;
		}
		if(!Utils.waitForElementFromDOM(driver, elementAbove)){
			Log.event("***Missing Element :: "+ aboveElement +"***");
			return false;
		}

		try {
			BrowserActions.scrollToView(elementBelow, driver);
		} catch(NoSuchElementException exx) {
			Log.event("***Missing Element :: "+ belowElement +"***");
			return false;
		}
		if(!Utils.waitForElementFromDOM(driver, elementBelow)){
			Log.event("***Missing Element :: "+ belowElement +"***");
			return false;
		}

		Point eleAbove = elementAbove.getLocation();
		Point eleBelow = elementBelow.getLocation();
		Log.event(eleAbove.y + "-->" + eleBelow.y);
		if(eleAbove.y < eleBelow.y)
			return true;
		else
			return false;
	}
	
	/**
	 * Wrapper to verify if the first element is above the second element in the UI
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @param aboveElement
	 *            : WebElement which is present above in the UI
	 * @return: Boolean
	 * @param obj -
	 * @param belowElement
	 *            : WebElement which is present below in the UI
	 * @throws Exception -
	 */
	public Boolean verifyVerticalAllignmentOfElementsWithoutScrolling(WebDriver driver,
			String aboveElement, String belowElement, Object obj)
					throws Exception {

		Field f = obj.getClass().getDeclaredField(aboveElement);
		f.setAccessible(true);
		WebElement elementAbove = ((WebElement) f.get(obj));

		Field f1 = obj.getClass().getDeclaredField(belowElement);
		f1.setAccessible(true);
		WebElement elementBelow = ((WebElement) f1.get(obj));

		if(!Utils.waitForElementFromDOM(driver, elementAbove)){
			Log.event("***Missing Element :: "+ aboveElement +"***");
			return false;
		}

		if(!Utils.waitForElementFromDOM(driver, elementBelow)){
			Log.event("***Missing Element :: "+ belowElement +"***");
			return false;
		}

		Point eleAbove = elementAbove.getLocation();
		Point eleBelow = elementBelow.getLocation();
		Log.event(eleAbove.y + "-->" + eleBelow.y);
		if(eleAbove.y < eleBelow.y)
			return true;
		else
			return false;
	}

	/**
	 * Wrapper to verify if the first element is above the second element in the UI
	 * @param elementNames -
	 *  @param obj -
	 * @return boolean
	 * @throws Exception -
	 */
	public Boolean verifyVerticalAllignmentOfListOfElements(List<String> elementNames, Object obj) throws Exception{
		boolean flag = true;
		List<Integer> elementYPos = new ArrayList<Integer>();
		List<String> missingEle = new ArrayList<String>();
		for(int i = 0; i < elementNames.size(); i++){
			Field f = obj.getClass().getDeclaredField(elementNames.get(i));
			f.setAccessible(true);
			try{
				WebElement element = ((WebElement) f.get(obj));
				elementYPos.add(element.getLocation().y);
				System.out.print("-->>"+element.getLocation().y);
			}catch(NoSuchElementException e){
				missingEle.add(elementNames.get(i));
				flag=false;
			}
		}

		for(int i = 0; i < elementYPos.size()-1; i++){
			for(int j = i+1; j < elementYPos.size(); j++){
				if(elementYPos.get(i) > elementYPos.get(i)){
					flag = false;
				}
			}
		}

		if(missingEle.size() > 0)
			Log.failsoft("Missing element on this page:: " + missingEle);

		return flag;
	}

	/**
	 * Wrapper to verify if the Element order in the UI
	 * @param obj -
	 * @param elementNames -
	 * @return: Boolean
	 * @throws Exception -
	 */
	public Boolean verifyHorizondalAllignmentOfListOfElements(List<String> elementNames, Object obj)
			throws Exception {
		boolean flag = true;
		List<Integer> elementXPos = new ArrayList<Integer>();
		for(int i = 0; i < elementNames.size(); i++){
			Field f = obj.getClass().getDeclaredField(elementNames.get(i));
			f.setAccessible(true);
			WebElement element = ((WebElement) f.get(obj));
			elementXPos.add(element.getLocation().x);
		}

		for(int i = 0; i < elementXPos.size()-1; i++){
			for(int j = i+1; j < elementXPos.size(); j++){
				if(elementXPos.get(i) > elementXPos.get(i)){
					flag = false;
				}
			}
		}
		return flag;
	}

	/**
	 * To verify the Center alignment of an element
	 * @param elementName -
	 * @param obj -
	 * @return true(if element alignment is expected), false(If element alignment is not as expected
	 * @throws Exception -
	 */
	public Boolean verifyCenterAllignmentOfElement(String elementName, Object obj)throws Exception{
		boolean flag = true;

		Field f = obj.getClass().getDeclaredField(elementName);
		f.setAccessible(true);
		WebElement element = ((WebElement) f.get(obj));
		int screenWidth = driver.manage().window().getSize().width;
		int xPosOfElement = element.getLocation().x;

		if(xPosOfElement > (screenWidth/2))
			flag = false;

		return flag;
	}

	/**
	 * To scroll to a particular element in the given page object
	 * @param elementName -
	 * @param obj -
	 * @throws Exception -
	 */
	public void scrollToViewElement(String elementName, Object obj)throws Exception{
		Field f = obj.getClass().getDeclaredField(elementName);
		f.setAccessible(true);
		WebElement element = ((WebElement) f.get(obj));
		BrowserActions.scrollToViewElement(element, driver);
	}


	/**
	 * To verify the container have Vertical scroll bar
	 * @param innerElementName -
	 * @param outterElementName -
	 * @param obj -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyHorizondalScrollBar(String innerElementName, String outterElementName, Object obj)throws Exception{

		Field f1 = obj.getClass().getDeclaredField(innerElementName);
		f1.setAccessible(true);
		WebElement innerElement = ((WebElement) f1.get(obj));

		Field f2 = obj.getClass().getDeclaredField(outterElementName);
		f2.setAccessible(true);
		WebElement outterElement = ((WebElement) f2.get(obj));

		Log.event("Inner Element Height :: " + innerElement.getSize().height);
		Log.event("Outter Element Height :: " + outterElement.getSize().height);

		if(innerElement.getSize().height > outterElement.getSize().height)
			return true;
		else
			return false;

	}

	/**
	 * To verify container have vertical scroll bar
	 * @param innerElementName -
	 * @param outterElementName -
	 * @param obj -
	 * @return -
	 * @throws Exception -
	 */
	public boolean verifyVerticalScrollBar(String innerElementName, String outterElementName, Object obj)throws Exception{

		Field f1 = obj.getClass().getDeclaredField(innerElementName);
		f1.setAccessible(true);
		WebElement innerElement = ((WebElement) f1.get(obj));

		Field f2 = obj.getClass().getDeclaredField(outterElementName);
		f2.setAccessible(true);
		WebElement outterElement = ((WebElement) f2.get(obj));

		if(innerElement.getSize().width > outterElement.getSize().width)
			return true;
		else
			return false;

	}

	/**
	 * To get the page load status of page objects
	 * @param obj - Page Object
	 * @return PageLoad status
	 * @throws Exception -
	 */
	public boolean verifyPageLoadStatus(Object obj)throws Exception{
		Field pageLoad = obj.getClass().getDeclaredField("isPageLoaded");
		pageLoad.setAccessible(true);
		boolean status = ((Boolean) pageLoad.get(obj));
		return status;
	}

	/**
	 *  To verify the maximum length of the Text field related web elements
	 * @param expectedElements -
	 * @param maxLength -
	 * @param obj -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyMaximumLengthOfTxtFldsEqualsTo(List<String> expectedElements, int maxLength, Object obj)throws Exception{
		/*Field f = obj.getClass().getDeclaredField("isPageLoaded");
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));

		return Integer.parseInt(webElement.getAttribute("maxlength")) >= maxLength ? true : false;*/

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				BrowserActions.scrollInToView(element, driver);
				if(!Utils.waitForElement(driver, element)){
					Log.reference("No Such Element on Page :: " + expEle);
					continue;
				}
				BrowserActions.scrollToViewElement(element, driver);
				Log.event("--->>>Max Length of " + expEle + " :: " + element.getAttribute("maxlength"));
				if (Integer.parseInt(element.getAttribute("maxlength")) >= maxLength) {
					actual_elements.add(expEle);
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}

		}
		return Utils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify text field allow special characters
	 * @param expectedElements -
	 * @param specialCharacters -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyTextFieldAllowSpecialCharacters(List<String> expectedElements, List<String> specialCharacters, Object obj)throws Exception{

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				BrowserActions.scrollToViewElement(element, driver);
				boolean state = true;
				for(int i=0; i < specialCharacters.size(); i++){
					BrowserActions.typeOnTextField(element, specialCharacters.get(i), driver, "Text Fields");
					String txtInBox = element.getAttribute("value");
					if(!specialCharacters.contains(txtInBox))
						state = false;
				}
				if (state) {
					actual_elements.add(expEle);
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
		}
		return Utils.compareTwoList(expectedElements, actual_elements);
	}


	/**
	 * Verify element text count equals to expected
	 * @param element -
	 * @param count -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean VerifyElementTextCountEqualTo(String element, int count, Object obj)throws Exception{
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));

		int cnt = 0;
		if(webElement.getTagName().equals("input")  || webElement.getTagName().equals("textarea"))
			cnt = webElement.getAttribute("value").length();
		else
			cnt = webElement.getText().length();

		if(cnt == 0) cnt = webElement.getText().length();

		Log.event("Text Count from Element :: " + cnt);
		Log.event("Text Count from User :: " + count);
		return (cnt == count) ? true : false;
	}

	/**
	 * To verify given inner element is aligned with container element as per given alignment condition
	 * @param innerElement -
	 * @param outterElement -
	 * @param allignment -
	 * @param obj - 
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyInsideElementAlligned(String innerElement, String outterElement, String allignment, Object obj) throws Exception{
		boolean status = false;
		WebElement innerEle = getElement(innerElement, obj);
		WebElement outterEle = getElement(outterElement, obj);

		Log.event("Inner Element Co-ordinates :: " + innerEle.getLocation());
		Log.event("Outter Element Co-ordinates :: " + outterEle.getLocation());

		switch(allignment){
		case "top":{
			int innerY = innerEle.getLocation().y;
			int outterY = outterEle.getLocation().y;
			int outterYCenterPoint = outterEle.getSize().height/2;
			if(innerY >= outterY && innerY <= (outterY+outterYCenterPoint))
				status = true;
		}break;

		case "bottom":{
			int innerY = innerEle.getLocation().y;
			int outterY = outterEle.getLocation().y;
			int outterYCenterPoint = outterEle.getSize().height/2;
			if(innerY >= (outterY+outterYCenterPoint))
				status = true;
		}break;

		case "left":{
			int innerX = innerEle.getLocation().x;
			int outterX = outterEle.getLocation().x;
			int outterXCenterPoint = outterEle.getSize().width/2;
			if(innerX >= outterX && innerX <= (outterX+outterXCenterPoint))
				status = true;
		} break;

		case "right":{
			int innerX = innerEle.getLocation().x;
			int outterX = outterEle.getLocation().x;
			int outterXCenterPoint = outterEle.getSize().width/2;
			if( innerX > (outterX+outterXCenterPoint))
				status = true;
		} break;
		}

		return status;
	}



	/**
	 * Verify element text equals to expected
	 * @param String - element
	 * @param String - testString
	 * @param Object - page object
	 * @return boolean - true/false if element text equals to expected
	 * @throws Exception - Exception
	 */
	public boolean verifyElementTextEqualTo(String element, String testString, Object obj)throws Exception
	{
		Field f1 = obj.getClass().getDeclaredField(element);
		f1.setAccessible(true);
		WebElement innerElement = ((WebElement) f1.get(obj));

		String text = new String();
		if(innerElement.getTagName().contains("select") || innerElement.getTagName().contains("input"))
			text = innerElement.getAttribute("value");
		else
			text = innerElement.getText().trim();

		Log.event("Text From Element :: " + text);
		Log.event("Text From User :: " + testString);

		return (text.equalsIgnoreCase(testString))?true : false;

	}
	
	/**
	 * To get element text
	 * @param String - element name
	 * @param Object - page object
	 * @throws Exception - Exception
	 */
	public String getElementText(String element, Object obj)throws Exception
	{
		Field f1 = obj.getClass().getDeclaredField(element);
		f1.setAccessible(true);
		WebElement innerElement = ((WebElement) f1.get(obj));

		String text = new String();
		if(innerElement.getTagName().contains("select") || innerElement.getTagName().contains("input"))
			text = innerElement.getAttribute("value");
		else
			text = innerElement.getText().trim();
		return text;
	}

	/**
	 * Verify list element equals text
	 * @param elements -
	 * @param index -
	 * @param testString -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyListElementTextEqualTo(List<String> elements,int index, String testString, Object obj)throws Exception
	{
		String text = new String();
		for (String expEle : elements) {
			Field f = null;			
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			List<WebElement> innerElement = null;
			try {
				innerElement = ((List<WebElement>) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}



			if(innerElement.get(index).getTagName().contains("select") || innerElement.get(index).getTagName().contains("input"))
				text = innerElement.get(index).getAttribute("value");
			else
				text = innerElement.get(index).getText().trim();
		}
		System.out.println("--->>>>"+text);

		return (text.equals(testString))?true : false;

	}

	/**
	 * To verify the email address from an element is in correct format
	 * @param elementString -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyEmailAddress(String elementString, Object obj)throws Exception
	{
		String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

		Field f1 = obj.getClass().getDeclaredField(elementString);
		f1.setAccessible(true);
		WebElement element = ((WebElement) f1.get(obj));

		if(!Utils.waitForElement(driver, element)){
			Log.reference("No Such Element on the Page :: " + elementString);
			return false;
		}
		BrowserActions.scrollToViewElement(element, driver);
		String text = new String();
		if(element.getTagName().contains("select") || element.getTagName().contains("input"))
			text = element.getAttribute("value");
		else
			text = element.getText().trim();

		Log.event("Extracted Text :: " + text);

		return text.matches(EMAIL_REGEX);
	}

	/**
	 * To verify element text contains string
	 * @param String - elementString
	 * @param String - testString
	 * @param Object - obj
	 * @return boolean - true/false if text contains
	 * @throws Exception - Exception
	 */
	public boolean verifyElementTextContains(String elementString, String testString, Object obj)throws Exception
	{
		Field f1 = obj.getClass().getDeclaredField(elementString);
		f1.setAccessible(true);
		WebElement element = ((WebElement) f1.get(obj));

		if(!Utils.waitForElement(driver, element)){
			Log.reference("No Such Element on the Page :: " + elementString);
			return false;
		}
		BrowserActions.scrollToViewElement(element, driver);
		String text = new String();
		if(element.getTagName().contains("select") || element.getTagName().contains("input") || element.getTagName().contains("textarea"))
			text = element.getAttribute("value");
		else
			text = element.getText().trim();

		Log.event("Extracted Text :: " + text);
		return (text.toLowerCase().contains(testString.toLowerCase()))?true : false;

	}

	/**
	 * To verify RGB code 
	 * @param element1 -
	 * @param element2 -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyRGBColorCodeEquals(String element1,String element2,Object obj)throws Exception {
		boolean textcolor=false;
		Field f1 = obj.getClass().getDeclaredField(element1);
		f1.setAccessible(true);
		WebElement Element1 = ((WebElement) f1.get(obj));
		Field f2 = obj.getClass().getDeclaredField(element2);
		f2.setAccessible(true);
		WebElement Element2 = ((WebElement) f2.get(obj));

		String color1=BrowserActions.getRGBvalue(Element1);
		String color2=BrowserActions.getRGBvalue(Element2);
		if(color1.equals(color2)) textcolor=true;
		return textcolor;
	}

	/**
	 * To verify selected color swatches product set
	 * @param expectedElements -
	 * @param obj -
	 * @return status as boolean 
	 * @throws Exception -
	 */
	public boolean verifySelectedcolorSwatchesProdSet(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if (Utils.waitForElement(driver, element, 2)) {
				actual_elements.add(expEle);
			}
		}
		return Utils.compareTwoList(expectedElements, actual_elements);
	}



	/**
	 * To verify element is displayed left side
	 * @param secondElement -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 * @deprecated This method uses only one parameter(which is the element to be verified) missing the container element for referencing position 
	 * We have added another method with same name and container element and used appropriate validation based in the x-axis and width calculations
	 * Please change the method calls accordingly
	 */
	@Deprecated
	public boolean verifyElementDisplayedleft(String secondElement, Object obj)throws Exception{
		boolean flag = false;
		Field f1 = obj.getClass().getDeclaredField(secondElement);
		f1.setAccessible(true);
		WebElement firstEle = ((WebElement) f1.get(obj));

		if(firstEle.getLocation().getX() <500)
			flag = true;

		return flag;
	}

	/**
	 * To verify element is displayed left side inside / of container
	 * @param container - 
	 * @param element - 
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyElementDisplayedleft(String container, String element, Object obj)throws Exception{
		Field containerField = obj.getClass().getDeclaredField(container);
		Field elementField = obj.getClass().getDeclaredField(element);
		containerField.setAccessible(true);
		elementField.setAccessible(true);
		WebElement containerEle = ((WebElement) containerField.get(obj));
		WebElement elementEle = ((WebElement) elementField.get(obj));

		int container_x = containerEle.getLocation().x;
		int container_size_width = containerEle.getSize().width;
		int element_x = elementEle.getLocation().x;
		int element_size_width = elementEle.getSize().width;

		int centerPoint = container_x + (container_size_width/2);
		int leftHalfCenterPoint = container_x + (container_size_width/2) + (container_size_width/4);
		int eleEndPosition = element_x + element_size_width;

		if(element_x < centerPoint && eleEndPosition < leftHalfCenterPoint)
			return true;
		else
			return false;

	}

	/**
	 * To verify the first element displayed below the second element
	 * @param firstElement -
	 * @param secondElement -
	 * @param obj -
	 * @return 'true' / 'false'
	 * @throws Exception -
	 */
	public boolean verifyElementDisplayedOverlay(String firstElement, String secondElement, Object obj)throws Exception{
		boolean flag = false;

		Field f = obj.getClass().getDeclaredField(firstElement);
		f.setAccessible(true);
		WebElement firstEle = ((WebElement) f.get(obj));

		Field f1 = obj.getClass().getDeclaredField(secondElement);
		f1.setAccessible(true);
		WebElement secondEle = ((WebElement) f1.get(obj)); 

		if(firstEle.getLocation().getY() <= secondEle.getLocation().getY())
			flag = true;

		return flag;
	}


	/**
	 * To verify text field element allows character type
	 * @param elementString -
	 * @param characterType -
	 * @param obj -
	 * @param elementDesc -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyTxtFldElementAllows(String elementString, String characterType, Object obj, String elementDesc)throws Exception{
		boolean stateToBeReturned = false;
		String dataToChk = new String();

		Field f = obj.getClass().getDeclaredField(elementString);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		if(!Utils.waitForElement(driver, webElement)) {
			Log.reference("No Such Element present in Page :: " + elementString);
			return false;
		}
		BrowserActions.scrollToViewElement(webElement, driver);

		switch(characterType){
		case "number" : {
			BrowserActions.typeOnTextField(webElement, "1234567890", driver, elementDesc);
			dataToChk = BrowserActions.getText(driver, webElement, elementDesc); 
			stateToBeReturned = ("1234567890").contains(dataToChk) ? true : false;
			Log.message("--->>>Tried to type Numbers", driver);
		}
		break;

		case "alphabet" : {
			BrowserActions.typeOnTextField(webElement, "AaBbCcDd", driver, elementDesc);
			webElement.sendKeys(Keys.TAB);
			dataToChk = BrowserActions.getText(driver, webElement, elementDesc); 
			stateToBeReturned = (dataToChk.equals("AaBbCcDd")) ? true : false;
			Log.message("--->>>Tried to type Alphabets", driver);
		}
		break;

		case "special" : {
			BrowserActions.typeOnTextField(webElement, "@#$%&*,./", driver, elementDesc);
			dataToChk = BrowserActions.getText(driver, webElement, elementDesc); 
			stateToBeReturned = (dataToChk.equals("@#$%&*,./")) ? true : false;
			Log.message("--->>>Tried to type Special Characters", driver);
		}
		break;

		default:
			BrowserActions.typeOnTextField(webElement, characterType, driver, elementString);
			dataToChk = BrowserActions.getText(driver, webElement, elementString); 
			stateToBeReturned = (dataToChk.equals(characterType)) ? true : false;
			Log.message("--->>>Tried to type Special Characters", driver);
			break;
		}


		return stateToBeReturned;
	}

	/**
	 * To verify list of field element allows specified type
	 * @param elementString -
	 * @param characterType -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyListOfTxtFldElementAllows(List<String> elementString, List<String> characterType, Object obj)throws Exception{
		List<String> missedEle = new ArrayList<String>();
		for(int i = 0; i < elementString.size(); i++){
			boolean stateToBeReturned = false;
			String dataToChk = new String();

			Field f = obj.getClass().getDeclaredField(elementString.get(i));
			f.setAccessible(true);
			WebElement webElement = ((WebElement) f.get(obj));
			BrowserActions.scrollInToView(webElement, driver);


			switch(characterType.get(i)){
			case "number" : {
				BrowserActions.typeOnTextField(webElement, "1234567890", driver, elementString.get(i));
				dataToChk = BrowserActions.getText(driver, webElement, elementString.get(i)); 
				stateToBeReturned = (dataToChk.equals("1234567890")) ? true : false;
			}
			break;

			case "alphabet" : {
				BrowserActions.typeOnTextField(webElement, "AaBbCcDd", driver, elementString.get(i));
				dataToChk = BrowserActions.getText(driver, webElement, elementString.get(i)); 
				stateToBeReturned = (dataToChk.equals("AaBbCcDd")) ? true : false;
			}
			break;

			case "special" : {
				BrowserActions.typeOnTextField(webElement, "@#$%&*,./", driver, elementString.get(i));
				dataToChk = BrowserActions.getText(driver, webElement, elementString.get(i)); 
				stateToBeReturned = (dataToChk.equals("@#$%&*,./")) ? true : false;
			}
			break;

			default:
				BrowserActions.typeOnTextField(webElement, characterType.get(i), driver, elementString.get(i));
				dataToChk = BrowserActions.getText(driver, webElement, elementString.get(i)); 
				stateToBeReturned = (dataToChk.equals(characterType.get(i))) ? true : false;
				break;
			}

			if(!stateToBeReturned)
				missedEle.add(elementString.get(i));

			BrowserActions.scrollInToView(webElement, driver);
			webElement.clear();
		}

		if(missedEle.size() > 0){
			Log.failsoft("Missed Elements :: " + missedEle.toString());
			return false;
		}else{
			Log.event("Verified All Elements :: " + elementString.toString());
			return true;
		}
	}



	/**
	 * To verify element text with demandware property
	 * @param elements -
	 * @param dmwrProp -
	 * @param obj -
	 * @return status as boolean 
	 * @throws Exception - 
	 */
	public boolean verifyElementTextWithDWREProp(List<String> elements, String dmwrProp, Object obj)throws Exception{
		boolean stateToReturn = true;


		for(int i = 0 ; i < elements.size(); i++){
			WebElement element = getElement(elements.get(i), obj);
			if(!(BrowserActions.getText(driver, element, "element").toLowerCase().equals(demandWareProperty.getProperty(dmwrProp).toLowerCase()))){
				stateToReturn = false;
			}
		}

		return stateToReturn;
	}

	/**
	 * To verify text with demandware property
	 * @param textToVerify  -
	 * @param dmwrProp -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyTextWithDWREProp(List<String> textToVerify, String dmwrProp, Object obj)throws Exception{
		boolean stateToReturn = true;


		for(int i = 0 ; i < textToVerify.size(); i++){
			if(!(textToVerify.get(i).toLowerCase().contains(demandWareProperty.getProperty(dmwrProp).toLowerCase()))){
				stateToReturn = false;
				Log.failsoft("Text :: " + textToVerify.get(i) + "  ---  DWRE Prop Value :: " + demandWareProperty.getProperty(dmwrProp));
			}
		}

		return stateToReturn;
	}

	/**
	 * To verify text is available in the element 
	 * @param elementToVerify -
	 * @param textToVerify -
	 * @param obj -
	 * @return status as boolean
	 */
	public boolean verifyTextContains(String elementToVerify, String textToVerify, Object obj) {
		boolean status = false;
		WebElement element = null;
		try{
			Field elementField = obj.getClass().getDeclaredField(elementToVerify);
			elementField.setAccessible(true);
			element = ((WebElement) elementField.get(obj));
			String txtFrmEle = element.getText().trim().replaceAll("\\s+", " ");

			Log.event("Text From Element :: " + txtFrmEle);
			Log.event("Text From user :: " + textToVerify);

			if (txtFrmEle.toLowerCase().contains(textToVerify.toLowerCase())) {
				status = true;
			}
		}//try
		catch(Exception e){
			e.printStackTrace();

		}//catch

		return status;
	}

	/**
	 * To verify text is available in the element 
	 * @param elementToVerify -
	 * @param textToVerify -
	 * @param obj -
	 * @return status as boolean
	 */
	public boolean verifyTextContainsForListElement(String elementToVerify, String textToVerify, Object obj) {
		boolean status = false;
		try{
			List<WebElement> listElement = getListElement(elementToVerify, obj);
			for(WebElement element : listElement) {
				String txtFrmEle = element.getText().trim().replaceAll("\\s+", " ");

				Log.event("Text From Element :: " + txtFrmEle);
				Log.event("Text From user :: " + textToVerify);

				if (txtFrmEle.toLowerCase().contains(textToVerify.toLowerCase())) {
					status = true;
				}
			}
		}//try
		catch(Exception e){
			e.printStackTrace();

		}//catch

		return status;
	}

	/**
	 * To verify placeholder moves above
	 * @param txtBoxElementString -
	 * @param lblElementString -
	 * @param valueToEnter -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyPlaceHolderMovesAbove(String txtBoxElementString, String lblElementString, String valueToEnter, Object obj)throws Exception{
		WebElement txtBox = getElement(txtBoxElementString, obj);
		WebElement lblBox = getElement(lblElementString, obj);

		BrowserActions.scrollInToView(txtBox, driver);
		if(!Utils.waitForElement(driver, txtBox)) {
			Log.reference("No Such Element present in Page :: " + txtBoxElementString);
			return false;
		}
		BrowserActions.typeOnTextField(txtBox, "", driver, txtBoxElementString);
		int y1 = lblBox.getLocation().y;
		Log.event(lblElementString + " Initial Position of Place Holder :: " + y1);
		BrowserActions.typeOnTextField(txtBox, valueToEnter, driver, txtBoxElementString);
		int y2 = lblBox.getLocation().y;
		Log.event(lblElementString + " After Text entered, Position of Place Holder :: " + y2);

		if(y1 > y2)
			return true;
		else
			return false;
	}

	/**
	 * To get y axis of element
	 * @param lblElement -
	 * @param obj -
	 * @return axis -
	 * @throws Exception -
	 */
	public int getYAxisOfElement(String lblElement, Object obj)throws Exception{
		WebElement lblBox = getElement(lblElement, obj);
		int y2 = lblBox.getLocation().y;
		return y2;
	}

	/**
	 * To verify placeholder moves above for list of elements
	 * @param txtBoxElement -
	 * @param lblElement -
	 * @param valueToEnter -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyPlaceHolderMovesAboveForList(List<String> txtBoxElement, List<String> lblElement, List<String> valueToEnter, Object obj)throws Exception{
		List<String> txtBoxEleActual = new ArrayList<String>();
		List<String> txtBoxEleMissed = new ArrayList<String>();
		for(int i=0; i < txtBoxElement.size(); i++ ){
			boolean stat = verifyPlaceHolderMovesAbove(txtBoxElement.get(i), lblElement.get(i), valueToEnter.get(i), obj);

			if(stat)
				txtBoxEleActual.add(txtBoxElement.get(i));
			else
				txtBoxEleMissed.add(txtBoxElement.get(i));
		}

		if(Utils.compareTwoList(txtBoxElement, txtBoxEleActual)){
			Log.event("Checkout All elements in Page :: " + txtBoxElement.toString());
			return true;
		}else{
			Log.failsoft("Missed Elements :: " + txtBoxEleMissed.toString());
			return false;
		}
	}

	/**
	 * To verify text element is not empty
	 * @param elementNames -
	 * @param obj -
	 * @return status as boolean 
	 * @throws Exception -
	 */
	public boolean verifyTxtElementsNotEmpty(List<String> elementNames, Object obj)throws Exception{
		List<String> missedEle = new ArrayList<String>();
		for(String ele : elementNames){
			WebElement txtBox = getElement(ele, obj);
			BrowserActions.scrollToViewElement(txtBox, driver);
			String value = BrowserActions.getText(driver, txtBox, ele);
			if(value.equals("") || value.isEmpty())
				missedEle.add(ele);

		}
		if(missedEle.size() > 0){
			Log.failsoft("Missed Elements :: " + missedEle.toString());
			return false;
		}else{
			Log.event("Verified All Elements :: " + elementNames.toString());
			return true;
		}
	}

	/**
	 * To verify element is empty
	 * @param elementNames -
	 * @param obj -
	 * @return status as boolean 
	 * @throws Exception -
	 */
	public boolean verifyTxtElementsIsEmpty(List<String> elementNames, Object obj)throws Exception{
		List<String> missedEle = new ArrayList<String>();
		for(String ele : elementNames){
			WebElement txtBox = getElement(ele, obj);
			BrowserActions.scrollToViewElement(txtBox, driver);
			String value = BrowserActions.getText(driver, txtBox, ele);
			if(!value.equals("") || !value.isEmpty())
				missedEle.add(ele);

		}
		if(missedEle.size() > 0){
			Log.failsoft("Missed Elements :: " + missedEle.toString());
			return false;
		}else{
			Log.event("Verified All Elements :: " + elementNames.toString());
			return true;
		}
	}

	/**
	 * To verify the element color
	 * @param elementName -
	 * @param colorCode -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyElementColor(String elementName, String colorCode, Object obj)throws Exception{
		WebElement element = getElement(elementName, obj);
		BrowserActions.scrollToViewElement(element, driver);

		String colorValue = element.getCssValue("color");
		colorValue = colorValue.split("\\)")[0];
		colorValue = colorValue.replace("rgb(", "");
		colorValue = colorValue.replace("rgba(", "");

		String[] colors = colorValue.split("\\,");
		Color color = new Color(Integer.parseInt(colors[0].trim()), Integer.parseInt(colors[1].trim()), Integer.parseInt(colors[2].trim()));
		String alpha = Integer.toHexString(color.getAlpha());
		String red = Integer.toHexString(color.getRed());
		String green = Integer.toHexString(color.getGreen());
		String blue = Integer.toHexString(color.getBlue());
		String hex = alpha + red + green + blue;

		hex = hex.replaceAll("f", "");
		colorCode = colorCode.replaceAll("#", "");

		Log.event("Element Color :: " + hex);
		Log.event("Color code given :: " + colorCode);
		return (colorCode.contains(hex))? true : false;
	}

	/**
	 * To verify list of element colors
	 * @param elementName -
	 * @param colorCode -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyListOfElementColor(List<String> elementName, String colorCode, Object obj)throws Exception{

		List<String> missedList = new ArrayList<String>();

		for(String ele: elementName){
			if(!verifyElementColor(ele, colorCode, obj))
				missedList.add(ele);
		}

		if(missedList.size() > 0){
			Log.failsoft("Missed Elements :: " + missedList.toString());
			return false;
		}

		return true;

	}

	/**
	 * To clear the specified text fields
	 * @param elements -
	 * @param obj -
	 * @throws Exception -
	 */
	public void clearTextFields(List<String> elements, Object obj)throws Exception{
		for(String ele : elements){
			WebElement element = getElement(ele, obj);
			element.clear();
		}
	}


	/**
	 * To get tagname of the element
	 * @param ele -
	 * @param obj -
	 * @return tagname 
	 * @throws Exception -
	 */
	public String getTagName(String ele, Object obj) throws Exception{
		Field f = null;		
		String tag=null;
		try {
			f = obj.getClass().getDeclaredField(ele);
			f.setAccessible(true);
		} catch (NoSuchFieldException | SecurityException e1) {
			throw new Exception("No such a field present on this page, Please check the value:: " + ele);
		}
		WebElement element = null;
		try {
			element = ((WebElement) f.get(obj));
			element.getAttribute("innerHTML");
			tag=element.getTagName();


		} catch (IllegalArgumentException | IllegalAccessException e1) {
			Log.exception(e1);
		} catch (NoSuchElementException e2){
			Log.failsoft("Required Element["+ele+"] not displayed in Page Test.", driver);

		}
		return tag;
	}

	/**
	 * To verify given list of elements displayed in page
	 * @param elements - 
	 * @param obj - 
	 * @return boolean - 
	 * @throws Exception -
	 */
	public boolean isElementsDisplayed(List<String> elements, Object obj)throws Exception{
		List<String> missedElements = new ArrayList<String>();
		boolean status = true;
		for(String element : elements){
			WebElement ele = getElement(element, obj);
			if(!Utils.waitForElement(driver, ele)){
				missedElements.add(element);
				status = false;
			}
		}

		Log.event("Missed Elements :: " + missedElements.toString());
		return status;
	}

	/**
	 * To verify given elements are same
	 * @param element1 -
	 * @param element2 -
	 * @param obj -
	 * @return Boolean -
	 * @throws Exception -
	 */
	public Boolean verifyElementsAreInSameRow(String element1, String element2, Object obj)throws Exception {
		Point ele1 = getElement(element1, obj).getLocation();
		Point ele2 = getElement(element1, obj).getLocation();

		Log.event("Element-1 Position :: " + ele1.y);
		Log.event("Element-2 Position :: " + ele2.y);

		if(ele1.y == ele2.y)
			return true;
		else
			return false;
	}

	/**
	 * To get height of given element
	 * @param element -
	 * @param obj -
	 * @return Integer -
	 * @throws Exception -
	 */
	public int getElementHeight(String element, Object obj)throws Exception{
		return getElement(element, obj).getSize().height;
	}

	/**
	 * To get width of given element
	 * @param element -
	 * @param obj -
	 * @return  Integer -
	 * @throws Exception -
	 */
	public int getElementWidth(String element, Object obj)throws Exception{
		return getElement(element, obj).getSize().width;
	}

	public boolean verifyMaxElementsInRow(String listElement, int maxCountInRow, Object obj)throws Exception{
		List<WebElement> listEle = getListElement(listElement, obj);
		boolean returnValue = true;
		for(int i=0; i <= listEle.size()/maxCountInRow; i++){
			int noOfEleAvalialble = listEle.size() - (maxCountInRow * i);
			for(int j=1; j < maxCountInRow && j < noOfEleAvalialble; j++){
				int firstY = listEle.get(i*(maxCountInRow-1)).getLocation().y;
				int secondY = listEle.get(j).getLocation().y;
				Log.event((i*(maxCountInRow-1) + 1) + " :: " + firstY);
				Log.event((j+1) + " :: " + secondY);
				if(firstY != secondY){
					Log.failsoft("In row " + (i+1) + ", " + j + "th item not in same row.");
					returnValue = false;
				}
			}
		}
		return returnValue;
	}
	
	/**
	 * To get element css value
	 * @param element - element
	 * @return cssProperty - cssValue
	 * @param obj - obj
	 * @throws Exception - e
	 * */
	public String getElementCSSValue(String element, String cssProperty, Object obj) throws Exception{
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		BrowserActions.scrollToView(webElement, driver);
		String cssValue = webElement.getCssValue(cssProperty);
		
		return cssValue;
	}
	
	/**
	 * To get computed CSS value of an element
	 * @param element - element name to get CSS value of
	 * @param cssProperty - CSS property to get value
	 * @param obj - page object the element is declared in
	 * @return String - Computed CSS value of element
	 * @throws Exception - Exception  
	 */
	public String getComputedCssValueForElement(String element, String cssProperty, Object obj)throws Exception {
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		String computedCSSValue = (String) executor.executeScript("return window.getComputedStyle(arguments[0]).getPropertyValue('"+cssProperty+"')",webElement);

		Log.event("Element Computed CSS Value :: " + computedCSSValue);
		return computedCSSValue;
	}
	
	/**
	 * To extract numerical value from element
	 * @param element - name of element
	 * @param obj - page object element declared in
	 * @throws Exception - Exception
	 */
	public int getNumberInElement(String element, Object obj) throws Exception{
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		String numberAsStr = webElement.getText().replaceAll("[^0-9]", "");
		
		if(numberAsStr.length() == 0) {
			Log.event("No number found in Element");
			return 0;
		}
		
		else {
			int valueFromElement = Integer.parseInt(numberAsStr);
			Log.event("Number found in Element :: " + valueFromElement);
			return valueFromElement;
		}
	}
	
	/**
	 * To verify if the first element is above the second element in the UI
	 * @param driver - WebDriver Instance
	 * @param aboveElement - WebElement which is present above in the UI
	 * @param belowElement - WebElement which is present below in the UI
	 * @param obj - Page object element is declared in
	 * @return Boolean - true/false if first element is above second element
	 * @throws Exception - Exception
	 */
	public Boolean verifyVerticalAllignmentOfElementsWithoutPadding(WebDriver driver, String aboveElement, String belowElement, Object obj) throws Exception {

		Field f = obj.getClass().getDeclaredField(aboveElement);
		f.setAccessible(true);
		WebElement elementAbove = ((WebElement) f.get(obj));

		Field f1 = obj.getClass().getDeclaredField(belowElement);
		f1.setAccessible(true);
		WebElement elementBelow = ((WebElement) f1.get(obj));

		if(!Utils.waitForElementFromDOM(driver, elementAbove)){
			Log.event("***Missing Element :: "+ aboveElement +"***");
			return false;
		}

		if(!Utils.waitForElementFromDOM(driver, elementBelow)){
			Log.event("***Missing Element :: "+ belowElement +"***");
			return false;
		}

		Point eleAbove = elementAbove.getLocation();
		int elementHeightAbove = eleAbove.y;
		if(elementAbove.getCssValue("padding-top").length()!=0) {
			elementHeightAbove = elementHeightAbove + Utils.getNumberInString(elementAbove.getCssValue("padding-top"));
		}
		Point eleBelow = elementBelow.getLocation();
		int elementHeightBelow = eleBelow.y;
		if(elementBelow.getCssValue("padding-top").length()!=0) {
			elementHeightBelow = elementHeightBelow + Utils.getNumberInString(elementBelow.getCssValue("padding-top"));
		}
		Log.event(elementHeightAbove + "-->" + elementHeightBelow);
		if(elementHeightAbove < elementHeightBelow)
			return true;
		else
			return false;
	}
	
	/**
	 * To verify an element is within bound of another element
	 * @param WebDriver - driver
	 * @param String - inner element name
	 * @param String outer element name
	 * @param Object - page object elements declared in
	 * @return boolean - true/false if inner element is within bounds
	 * @throws Exception - Exception 
	 */
	public boolean verifyElementWithinElement(WebDriver driver, String innerElement, String outerElement, Object obj) throws Exception{
		Field f = obj.getClass().getDeclaredField(innerElement);
		f.setAccessible(true);
		WebElement elementInner = ((WebElement) f.get(obj));
		Field f1 = obj.getClass().getDeclaredField(outerElement);
		f1.setAccessible(true);
		WebElement elementOuter = ((WebElement) f1.get(obj));

		if(!Utils.waitForElementFromDOM(driver, elementInner)){
			Log.event("***Missing Element :: "+ innerElement +"***");
			return false;
		}
		if(!Utils.waitForElementFromDOM(driver, elementOuter)){
			Log.event("***Missing Element :: "+ outerElement +"***");
			return false;
		}
		
		if((elementInner.getSize().height > elementOuter.getSize().height) || (elementInner.getSize().width > elementOuter.getSize().width)) {
			return false;
		}
		boolean horizontalBound = (elementInner.getLocation().x >= elementOuter.getLocation().x) &&
				(elementInner.getLocation().x + elementInner.getSize().width <= elementOuter.getLocation().x + elementOuter.getSize().width);
		boolean verticalBound = (elementInner.getLocation().y >= elementOuter.getLocation().y) &&
				(elementInner.getLocation().y + elementInner.getSize().height <= elementOuter.getLocation().y + elementOuter.getSize().height);
		
		return horizontalBound && verticalBound;
	}
	
	/**
	 * To verify is given radio button is selected or not
	 * @param String - name of radio button 
	 * @param boolean - selection state to verify
	 * @param Object - page object
	 * @return boolean - true/false if radio button is in expected state
	 * @throws Exception - Exception
	 */
	public boolean verifyRadioButtonSelection(String element, boolean stateExpected, Object obj)throws Exception{
		if(stateExpected) {
			return verifyCssPropertyForElement(element, "background-image", "circle-selected.svg", obj) 
					|| verifyCssPropertyForElement(element, "background-position", "-32px -5px", obj);
		}
		if(!stateExpected) {
			return verifyCssPropertyForElement(element, "background-image", "circle-outline.svg", obj) 
					|| verifyCssPropertyForElement(element, "background-position", "-5px -5px", obj);
		}
		else 
			return false;
	}

    /**
     * To verify given input box / text area is editable or not
     * @param driver - WebDriver
     * @param element - element to verify
     * @param obj - Page object
     * @return boolean - true/false
     * @throws Exception - Exception
     */
	public boolean verifyElementIsEditable(WebDriver driver, String element, Object obj)throws Exception{
		boolean flag = false;
		try{
			WebElement ele = getElement(element, obj);

			String valueBefore = BrowserActions.getText(driver, ele, "");
			ele.clear();
			ele.clear();
			ele.sendKeys("word");
			String valueAfter = BrowserActions.getText(driver, ele, "");
			if(valueAfter.equals("word"))
				flag = true;

			ele.clear();
			ele.sendKeys(valueBefore);
			return flag;

		}catch(Exception e){
			Log.exception(e);
		}
		return flag;
	}

    /**
     * To verify field validation for error message
     * @param driver - WebDriver
     * @param elementsToValidate - text fields / text area elements
     * @param elementsToVerify - error message values
     * @param submitElement - submit button element / tab
     * @param obj - Page Object
     * @return boolean - true/false
     * @throws Exception - Exception
     */
	public boolean verifyFieldValidation(WebDriver driver, List<String> elementsToValidate, List<String> elementsToVerify, String submitElement, Object obj)throws Exception{
		boolean flag = true;
		try{
			for(String element : elementsToValidate){
				WebElement ele = getElement(element, obj);
				ele.clear();
				if(submitElement.equals("tab"))
					ele.sendKeys(Keys.TAB);
			}

			if(!submitElement.equals("tab")){
				WebElement ele = getElement(submitElement, obj);
				ele.click();
			}

			for(String element : elementsToVerify){
				WebElement ele = getElement(element, obj);
				if(!Utils.waitForElement(driver, ele)){
					flag = false;
					Log.event("Element not found :: " + element);
				}
			}
		}catch(Exception e){
			Log.exception(e);
		}
		return flag;

	}

    /**
     * To verify zip code field validations for multiple type of zip code formats
     *  like "55555" or "55555-4444"
     * @param driver - WebDriver
     * @param elementName - area code field name
     * @param errorEleName - Error element to be validated
     * @param obj - Page Object
     * @return boolean - true/false
     * @throws Exception - Exception
     */
	public boolean verifyZipCodeValidation(WebDriver driver, String elementName, String errorEleName, Object obj)throws Exception{
	    boolean flag = true;
	    try{
	        WebElement element = getElement(elementName, obj);
	        String eleText = BrowserActions.getText(driver, element, "");
	        element.clear();
	        element.sendKeys("28205");
	        element.sendKeys(Keys.TAB);
	        if(Utils.waitForElement(driver, getElement(errorEleName, obj)))
	            flag = false;

	        element.clear();
	        element.sendKeys("48813-8363");
            element.sendKeys(Keys.TAB);
            if(Utils.waitForElement(driver, getElement(errorEleName, obj)))
                flag = false;
            element.clear();
            element.sendKeys(eleText);
        }catch(Exception e){
            Log.exception(e);
        }

        return flag;
    }

}