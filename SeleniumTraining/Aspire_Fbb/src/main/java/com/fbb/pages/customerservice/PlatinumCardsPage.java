package com.fbb.pages.customerservice;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;


public class PlatinumCardsPage extends LoadableComponent<PlatinumCardsPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();

	/**********************************************************************************************
	 ********************************* WebElements of Platinum Cards Page ****************************
	 **********************************************************************************************/

	private static EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader
			.getInstance("demandware");

	@FindBy(css = ".pt_article-page")
	WebElement readyElement;
	
	@FindBy(css = ".article-heading")
	WebElement csCurrentHeading;

	@FindBy(css ="div[class*='pt_specials']" )
	WebElement lnkNavPlatinumCards;

	@FindBy(css =".plcc-top-content" )
	WebElement topContentSlot;

	@FindBy(css =".plcc-landing-perks .content-asset .perks-content" )
	WebElement platinumPerksLogos;

	@FindBy(css =".plccbenefits-n-email .plcc-platinumcard-benefits" )
	WebElement exclusiveBenefitsCopy;
	
	@FindBy(css = "#navigation")
	WebElement divGlobalNavigation;

	@FindBy(css =".plccbenefits-n-email .footer-email-signup .email-signup.make-label-absolute" )
	WebElement emailSignup;

	@FindBy(css =".perks-offer.clearboth" )
	WebElement freeShippingBlock;

	@FindBy(css =".bottom-content" )
	WebElement bottomContentSlot;

	@FindBy(css =".header-promo-bottom .promo-banner" )
	WebElement promotionalContent;

	@FindBy(css =".plccbenefits-n-email .form-row.emailsignup.email.required input" )
	WebElement emailTextField;

	@FindBy(css =".plccbenefits-n-email .form-row.emailsignup.email.required .field-wrapper" )
	WebElement emailField;

	@FindBy(css =".plccbenefits-n-email .label-text" )
	WebElement emailTextFieldPlaceHolder;

	@FindBy(css =".plccbenefits-n-email .input-focus .error" )
	WebElement emailPlaceHolderIncorrectErrorText;

	@FindBy(css =".plccbenefits-n-email .error" )
	WebElement emailPlaceHolderEmptyErrorText;

	@FindBy(css =".plccbenefits-n-email .form-row.form-row-button button" )
	WebElement signupButton;

	@FindBy(css =".plcc-top-content .plcc-top-banner .solid-wine-berry.button")
	WebElement applyButton;

	@FindBy(css =".plccbenefits-n-email .form-row.form-row-button" )
	WebElement signup;

	@FindBy(css =".plccbenefits-n-email .email-signup.make-label-absolute .email-signup-footer-success .content-asset .email-signup-footer-success-heading" )
	WebElement thankYouMessage;

	@FindBy(css =".plccbenefits-n-email .plcc-platinumcard-benefits .plcc-benefits-head" )
	WebElement exclusiveBenefitsContentHeading ;

	@FindBy(css =".plccbenefits-n-email .plcc-platinumcard-benefits .plcc-benefit-msg" )
	WebElement exclusiveBenefitsContentMessage ;

	@FindBy(css = ".plcc-top-image-conatiner")
	WebElement divBannerElement;
	
	@FindBy(css=".hide-mobile.hide-tablet")
	WebElement divBannerDesktop;
	
	@FindBy(css=".hide-tablet.hide-desktop")
	WebElement divBannerMobile;
	
	@FindBy(css=".hide-mobile.hide-desktop")
	WebElement divBannerTablet;
	
	@FindBy(css = ".plcc-secondary")
	WebElement divPLCCMessages;

	@FindBy(css = ".plcc-secondary a")
	WebElement divSeeBenifits;

	@FindBy(css = ".plcc-top-content")
	WebElement divPlccTopContent;

	@FindBy(css = ".plcc-secondary .plcc-left-section-1 .plcc-left-nav-heading")
	WebElement divInfoMessage;

	@FindBy(css = ".web-instant-submit")
	WebElement btnInstantSubmit;


	/*********************************************************
	 * **************** WebElements of Mandatory Error********
	 *********************************************************/
	@FindBy(xpath = "//div[@class='form-row  firstName required error-handle']")
	WebElement txtFirstNameMandatoryError;

	@FindBy(xpath = "//div[@class='form-row  lastName required error-handle']")
	WebElement txtLastNameMandatoryError;

	@FindBy(xpath = "//div[@class='form-row  email required error-handle']")
	WebElement txtEmailMandatoryError;
	
	@FindBy (css = ".make-label-absolute .form-row.email.error-handle .input-focus .error")
	WebElement txtInvalidEmailError;

	@FindBy(xpath = "//div[@class='form-row  password required error-handle']")
	WebElement txtPasswordMandatoryError;

	@FindBy(xpath = "//div[@class='form-row  address1 required error-handle']")
	WebElement txtAddressOneMandatoryError;

	@FindBy(xpath = "//div[@class='form-row  city required error-handle']")
	WebElement txtcityMandatoryError;

	@FindBy(css = ".form-row.required.state.error-handle >label")
	WebElement selectStateMandatoryError;

	@FindBy(xpath = "//div[@class='form-row  zipcode required error-handle']")
	WebElement txtzipcodeMandatoryError;
	
	@FindBy(css = ".make-label-absolute:not(.catalog-quick-order__main) .form-row label span.error")
	WebElement txtInvalidZipError;

	@FindBy(xpath = "//div[@class='form-row  phone required error-handle']")
	WebElement txtPhoneMandatoryError;

	@FindBy(xpath = "//div[@class='form-row  ssn required error-handle']")
	WebElement txtSsnMandatoryError;

	@FindBy(xpath = "//div[@class='form-row month month required error-handle']")
	WebElement selectMonthMandatoryError;

	@FindBy(xpath = "//div[@class='form-row day day required error-handle']")
	WebElement selectDateMandatoryError;

	@FindBy(xpath = "//div[@class='form-row year year required error-handle']")
	WebElement selectYearMandatoryError;

	@FindBy(css = ".input-checkbox.required.error")
	WebElement selectCheckboxMandatoryError;
	
	@FindBy(css = ".input-checkbox.required")
	WebElement selectCheckboxMandatory;

	@FindBy(css = ".make-label-absolute .form-row.email .option-text")
	WebElement selectEmailOptionalTxt;

	@FindBy(css = ".form-row.password .option-text")
	WebElement txtpasswordField;

	@FindBy(css = ".form-row.alternativePhone .option-text")
	WebElement selectAlternativePhoneOptionalTxt;


	/*********************************************************
	 * **************** WebElements of Register page element********
	 *********************************************************/
	@FindBy(css = ".plcc-primary")
	WebElement divPLCCForm;
	
	@FindBy(css = ".form-row.firstName.required .input-text")
	WebElement txtFirstName;

	@FindBy(css = ".form-row.lastName.required .input-text")
	WebElement txtLastName;

	@FindBy(css = ".form-row.address1.required .input-text")
	WebElement txtaddressOne;

	@FindBy(css = ".form-row.address2 .input-text")
	WebElement txtaddressTwo;

	@FindBy(css = ".form-row.city.required .input-text")
	WebElement txtcity;

	@FindBy(css = ".form-row.zipcode.required .input-text")
	WebElement txtZipCode;

	@FindBy(css = "input[id*='dwfrm_creditapplication_email_']")
	WebElement txtEmail;

	@FindBy(css = ".form-row .alternativePhone")
	WebElement txtAlternativePhone;

	@FindBy(css = ".form-row .phone")
	WebElement txtphone;

	@FindBy(css = ".form-row.ssn.required .input-text")
	WebElement txtSsnNO;

	@FindBy(css = ".form-row.month .field-wrapper .selected-option.selected")
	WebElement selectMonth;

	@FindBy(css = ".form-row.day.required .label-text")
	WebElement selectDayLabel;

	@FindBy(css = ".form-row.month.required .label-text")
	WebElement selectMonthLabel;

	@FindBy(css = ".form-row.year.required .label-text")
	WebElement selectYearLabel;

	@FindBy(css = ".form-row.day .field-wrapper .selected-option.selected")
	WebElement selectDay;

	@FindBy(css = ".form-row.year .field-wrapper .selected-option.selected")
	WebElement selectYear;

	@FindBy(css = ".form-row.phone-number-msg")
	WebElement divContactDisclaimer;

	@FindBy(css = ".plcc-authorized-buyer .cr-icon")
	WebElement divAuthorizedBuyer;

	@FindBy(css = ".plcc-form-section")
	WebElement divPlccFormSection;

	@FindBy(css = ".plcc-main .plcc-secondary .plcc-left-sections .plcc-left-section.plcc-left-section-1 .plcc-left-nav-heading")
	WebElement txtOR;

	@FindBy(css = ".cr-icon")
	WebElement iconExpand;

	@FindBy(css = ".plcc-authorized-buyer-section.display-inline-block.width-full")
	WebElement autherisedBuyerBlock;
	
	@FindBy(css = ".bottom-buttons")
	WebElement divSubmitBlock;


	/*********************************************************
	 * **************** WebElements of Register page PlaceHolder********
	 *********************************************************/
	@FindBy(css = ".form-row.firstName.required .label-text")
	WebElement spanPlaceHolderFirstName;

	@FindBy(css = ".form-row.lastName.required .label-text")
	WebElement spanPlaceHolderLastName;

	@FindBy(css = ".form-row.address1.required .label-text")
	WebElement spanPlaceHolderAddressOne;

	@FindBy(css = ".form-row.address2 .label-text")
	WebElement spanPlaceHolderAddressTwo;

	@FindBy(css = ".form-row.city.required .label-text")
	WebElement spanPlaceHolderCity;

	@FindBy(css = ".form-row.state .label-text")
	WebElement spanPlaceHolderState;

	@FindBy(css = ".form-row.zipcode .label-text")
	WebElement spanPlaceHolderZipcode;

	@FindBy(css = ".plcc-form-row-2 .email .label-text")
	WebElement spanPlaceHolderEmail;

	@FindBy(css = ".plcc-form-row-2 .phone .label-text")
	WebElement spanPlaceHolderPhone;

	@FindBy(css = ".plcc-form-row-2 .alternativePhone .label-text")
	WebElement spanPlaceHolderaAlternativePhone;

	@FindBy(css = ".plcc-form-row-2 .ssn .label-text")
	WebElement spanPlaceHolderSSN;

	@FindBy(css = ".form-row.month .label-text")
	WebElement spanPlaceHolderMonth;

	@FindBy(css = ".form-row.day .label-text")
	WebElement spanPlaceHolderDay;

	@FindBy(css = ".form-row.year .label-text")
	WebElement spanPlaceHolderYear;

	@FindBy(css = ".form-row.required.state .selected-option.selected")
	WebElement selectStateDrop;
	
	@FindBy(css= ".form-row.required.state .field-wrapper .selection-list li")
	List<WebElement> lstStateList;
	
	@FindBy(css = ".form-row.state .field-wrapper .selection-list")
    WebElement lstStateListValue;

	@FindBy(css = ".form-row.phone-number-msg .field-wrapper")
	WebElement divContactMessage;

	@FindBy(css = ".form-row.day.required .input-select .select-option")
	List<WebElement> lstDaylist;

	@FindBy(css = ".form-row.day.required .selection-list")
	WebElement lstDaylistValue;

	@FindBy(css = ".form-row.month.required .input-select .select-option")
	List<WebElement> lstMonthlist;

	@FindBy(css = ".form-row.month.required .selection-list")
	WebElement lstMonthlistValue;

	@FindBy(css = ".form-row.year.required .selection-list")
	WebElement lstYearlistValue;

	@FindBy(css = ".form-row.year.required .input-select .select-option")
	List<WebElement> lstYearlist;

	@FindBy(xpath = "//div[@class='clearboth plcc-bottom-section']/div[@class='iframe-content'][1]")
	WebElement accountTerms;

	@FindBy(css = ".financial-disc-iframe")
	WebElement FinanceTerms;

	@FindBy(css = ".clearboth.plcc-bottom-section .content-asset")
	WebElement contentTerms;

	@FindBy(css = ".input-checkbox.required.valid")
	WebElement consentCheckbox;

	@FindBy(id = "#dwfrm_creditapplication_consent-error")
	WebElement consentCheckboxError;


	@FindBy(css = ".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable.plcc-apply-error-model")
	WebElement plccApprovedOverlay;

	@FindBy(css = "label[for^='dwfrm_creditapplication_ssn_']")
	WebElement ssnplaceholder;

	@FindBy(css = ".form-row.address2 .option-text")
	WebElement txtAddressTwoOptional;

	@FindBy(css = "input[name='dwfrm_creditapplication_ssn_d0layuordgwh']")
	WebElement ssnInputText;


	@FindBy(css = "label[for^='dwfrm_creditapplication_phone_']")
	WebElement phoneplaceholder;

	@FindBy(css = ".plcc-ssn-label .question-mark-icon.dialog-tooltip")
	WebElement icoSSNToolTip;

	@FindBy(css = ".tooltip-dialog .ui-dialog-titlebar-close")
	WebElement iconCloseToolTip;

	@FindBy(css = "input[id*='dwfrm_creditapplication_phone_']")
	WebElement txtMobileNoInPLCCStep2;	

	@FindBy(css = "label[for^='dwfrm_creditapplication_alternativePhone_']")
	WebElement alternativePhoneplaceholder;

	@FindBy(css = ".account-term-iframe")
	WebElement consenttoAccount;

	@FindBy(css = ".financial-disc-iframe")
	WebElement consenttoFinance;

	@FindBy(css = ".financial-disc-iframe")
	WebElement creditcardDisclaimer;

	@FindBy(css = ".input-checkbox.required ")
	WebElement consentChkBox;

	@FindBy(css = ".plcc-landing-content")
	WebElement divplccLandingPage;

	@FindBy(css = ".form-row.phone-number-msg")
	WebElement phoneContactDisclaimer;

	@FindBy(css = ".see-benefits")
	WebElement linkSeeBenefits;

	@FindBy(css = ".landing-page>button")
	WebElement btnCancel;
	
	@FindBy(css = ".review-form-error-msg")
	WebElement errorFormReview;
	
	@FindBy(css = ".important-info")
	WebElement divCreditCardDisclaimer;
	/**********************************************************************************************
	 ********************************* WebElements of Platinum Cards Page ****************************
	 **********************************************************************************************/	


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public PlatinumCardsPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}




	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Platinum Card Page did not open up.", driver);
		}
	}

	/**
	 * To verify Top content is displayed below promotional content
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyTopContentSlotBelowPromotionalContent() throws Exception {
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, promotionalContent, topContentSlot);
	}

	/**
	 * To verify Platinum perks logo displayed below Top content slot
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPlatinumPerksLogosBelowTopContentSlot() throws Exception {
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, topContentSlot, platinumPerksLogos);
	}

	/**
	 * To verify Platinum Perks Logo displayed Right side of execlusive benefits
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyplatinumPerksLogosRightOfOrBelowexclusiveBenefits()throws Exception{
		if(runPltfrm.equals("mobile")) {
			return BrowserActions.verifyVerticalAllignmentOfElements(driver, platinumPerksLogos,exclusiveBenefitsCopy);
		} else {
			return BrowserActions.verifyHorizontalAllignmentOfElements(driver, exclusiveBenefitsCopy,platinumPerksLogos);
		}
	}

	/**
	 * To verify Submit button is displayed right side to Email Field
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifySubmitRightOfEmailTextField()throws Exception{
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, signupButton, emailField);
	}

	/**
	 * To verify Email Address textbox is displayed below Benefits
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailAddressTextBoxBelowExclusiveBenefits() throws Exception {
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, exclusiveBenefitsCopy, emailSignup);
	}

	/**
	 * To verify Content slot displayed below Platinum logo
	 * @return boolean value
	 * @throws Exception - 
	 */
	public Boolean verifyBottomContentSlotBelowPlatinumLogo() throws Exception {

		Boolean flag=true;
		flag = BrowserActions.verifyVerticalAllignmentOfElements(driver, platinumPerksLogos, freeShippingBlock);
		flag = BrowserActions.verifyVerticalAllignmentOfElements(driver, freeShippingBlock, bottomContentSlot);

		return flag;
	}

	/**
	 * To Type text into Email Field
	 * @param email - 
	 * @throws Exception - 
	 */
	public void typeOnFields(String email)throws Exception{
		emailTextField.clear();
		BrowserActions.typeOnTextField(emailTextField, email, driver, "Email Address ");


	}

	/**
	 * To type default email address in email address
	 * @return string value
	 * @throws Exception - Exception
	 */
	public String EnterEmailAddressInTextBox() throws Exception {
		emailTextField.clear();
		emailTextField.sendKeys("automation@yopmail.com");
		return BrowserActions.getText(driver, emailTextField, "Entered Email Field Text");
	}

	/**
	 * To type click on signup button
	 * @throws Exception - Exception
	 */
	public void clickOnSignUp()throws Exception{
		BrowserActions.clickOnElementX(signupButton, driver, "Clicked on signup");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on apply button
	 * @throws Exception - Exception
	 */
	public void clickOnApplyButton()throws Exception{
		Utils.waitForElement(driver, applyButton);
		BrowserActions.scrollToView(applyButton, driver);
		BrowserActions.clickOnElementX(applyButton, driver, "Apply Button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To verify email place holder
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailPlaceholderMovestotheTop()throws Exception {
		Boolean flag = true;
		emailTextField.clear();
		float y1=emailTextFieldPlaceHolder.getLocation().getY();
		typeOnFields("email@gmail.com");
		float y2=emailTextFieldPlaceHolder.getLocation().getY();
		if(y2>y1)
			flag=true;
		return flag;
	}

	/**
	 * To get displayed error message
	 * @return string - error message
	 * @throws Exception - Exception
	 */
	public String getDisplayedEmailErrorMessage() throws Exception{
		typeOnFields("emailgmail");
		BrowserActions.clickOnElementX(signupButton, driver, "Entered incorrect email address and Clicked on signup");
		Utils.waitForPageLoad(driver);
		return BrowserActions.getText(driver, emailPlaceHolderIncorrectErrorText, "Error Message");

	}

	/**
	 * To verify email field error displayed
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailFieldBlankError()throws Exception {
		emailTextField.clear();
		BrowserActions.clickOnElementX(signupButton, driver, "Blank email address and Clicked on signup");
		return Utils.verifyCssPropertyForElement(emailPlaceHolderEmptyErrorText, "color", "rgba(189, 3, 4, 1)");
	}

	/**
	 * To get thank you message
	 * @return string - thank you message
	 * @throws Exception - Exception
	 */

	public String getDisplayedThankyouMessage() throws Exception{
		return BrowserActions.getText(driver, thankYouMessage, "Error Message");
	}

	/**
	 * To verify exclusive benefits copy message property
	 * @return boolean true/false
	 * @throws Exception - Exception
	 */

	public boolean verifyExclusiveBenefitsCopyMessageProperty() throws Exception {
		Boolean flag= false;
		String textToVerify = exclusiveBenefitsContentMessage.getText();
		String propertyText = demandWareProperty.getProperty("PlatinumcardExclusiveBenefitsMessage");
		if (textToVerify.equals(propertyText)) {

			Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
			return flag=true;
		}
		return flag;
	}

	/**
	 * To click on benefits button
	 * @return boolean true/false
	 * @throws Exception - Exception
	 */

	public boolean ClickOnBenefitsButton() throws Exception{
		Boolean flag=false;
		if(Utils.waitForElement(driver,divSeeBenifits)){
			BrowserActions.clickOnElementX(divSeeBenifits, driver, "Benifis link");
			Utils.waitForPageLoad(driver);
		}
		if(Utils.waitForElement(driver,divPlccTopContent)){
			flag=true;
		}

		return flag;
	}

	/**
	 * To click on register button
	 * @throws Exception - Exception
	 */

	public void clickOnRegisterBtn() throws Exception{
		if(Utils.waitForElement(driver,btnInstantSubmit )){
			BrowserActions.scrollToView(btnInstantSubmit, driver);
			BrowserActions.clickOnElementX(btnInstantSubmit, driver, "elementDescription");
			Utils.waitForPageLoad(driver);

		}
	}

	/**
	 * To get expand status
	 * @param index - 
	 * @return string  - expanded/collapsed
	 * @throws Exception - Exception
	 */
	public String getExpandStatusOfQuestion(int index)throws Exception{
		Utils.waitForElement(driver, divPlccFormSection);
		BrowserActions.scrollToView(divPlccFormSection, driver);
		return (divPlccFormSection.getAttribute("class").contains("active"))? "expanded":"collapsed";
	}

	/**
	 * To error message
	 * @param textToType - 
	 * @param errorMessage - 
	 * @param fieldToVerify - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifyFieldErrorMessages(String textToType, String errorMessage ,String fieldToVerify)throws Exception{
		boolean flag=false;

		switch(fieldToVerify){
		case "FirstName":
			BrowserActions.typeOnTextField(txtFirstName, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtFirstNameMandatoryError);

			Log.message(txtFirstNameMandatoryError.getText().trim());
			Log.message(errorMessage.trim());
			if(txtFirstNameMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;
		case "LastName":
			BrowserActions.typeOnTextField(txtLastName, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtLastNameMandatoryError);

			if(txtLastNameMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;
		case "Email":
			BrowserActions.typeOnTextField(txtEmail, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtInvalidEmailError);
			if(txtInvalidEmailError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;
		case "Password":
			BrowserActions.typeOnTextField(txtpasswordField, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtEmailMandatoryError);
			if(txtPasswordMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}
			break;



		case "zipCode":
			BrowserActions.typeOnTextField(txtZipCode, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtzipcodeMandatoryError);

			Log.message(txtzipcodeMandatoryError.getText().trim());
			Log.message(errorMessage.trim());
			if(txtzipcodeMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;

		case "Phone Number":
			BrowserActions.typeOnTextField(txtphone, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtPhoneMandatoryError);

			Log.message(txtPhoneMandatoryError.getText().trim());
			Log.message(errorMessage.trim());
			if(txtPhoneMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;

		case "SSN":
			BrowserActions.typeOnTextField(txtSsnNO, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtSsnMandatoryError);

			Log.message(txtSsnMandatoryError.getText().trim());
			Log.message(errorMessage.trim());
			if(txtSsnMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}
			
			break;
		}
		return flag;
	}


	/**
	 * To enter text on field
	 * @param element - 
	 * @param textToType - 
	 * @param Description - 
	 * @param obj - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean enterTextOnField(String element,String textToType,String Description,Object obj)throws Exception{

		boolean flag=false;

		WebElement verifyElement=ElementLayer.getElement(element, obj);
		BrowserActions.typeOnTextField(verifyElement, textToType, driver, Description);
		String customerTextValue=verifyElement.getAttribute("value");
		if(customerTextValue.equals(textToType)){
			flag=true;
		}
		if(Description.contains("SSN")) {
			if(customerTextValue.substring(7).equals(textToType.substring(5))) {
				flag=true;
			}
		}
		Log.messageT(textToType+", "+ customerTextValue);
		return flag;

	}

	/**
	 * To verify optional field
	 * @param element - 
	 * @param obj - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifyOptionalField(String element,Object obj)throws Exception{
		boolean flag=false;

		if (Utils.waitForElement(driver, ElementLayer.getElement(element, obj))){

			flag=true;
		}

		return flag;
	}

	/**
	 * To select month
	 * @param State - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean selectMonth(String State) throws Exception {
		boolean flag=false;
		Utils.waitForElement(driver, selectStateDrop);
		BrowserActions.clickOnElementX(selectStateDrop, driver, "State dropdown");
		Utils.waitForElement(driver, lstStateListValue);
		BrowserActions.mouseHover(driver, lstStateListValue);
		for(int stateIndex= 1; stateIndex< lstStateList.size(); stateIndex++) {
			BrowserActions.scrollToView(lstStateList.get(stateIndex), driver);
			if(lstStateList.get(stateIndex).getAttribute("label").trim().equalsIgnoreCase(State)) {
				BrowserActions.clickOnElementX(lstStateList.get(stateIndex), driver, "Selecting a State");
				flag=true;
			}
		}
		return flag;
	}

	/**
	 * To verify and select state element
	 * @param dateType - 
	 * @param dateElement - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifyandSelectDateElements(String dateType,String dateElement)throws Exception{

		boolean flag=true;
		switch(dateType){
		case "month":
			if(Utils.waitForElement(driver, selectMonth)){
				String monthindex="";
				BrowserActions.clickOnElementX(selectMonth, driver, "month drop down");

				if(Utils.waitForElement(driver, lstMonthlist.get(0))){
					for(int dayIndex=1;dayIndex<lstMonthlist.size();dayIndex++){
						if(dayIndex<=9){
							monthindex="0"+String.valueOf(dayIndex);

						}else{
							monthindex=String.valueOf(dayIndex);
						}

						if(!lstMonthlist.get(dayIndex).getAttribute("label").equals(String.valueOf(monthindex) )){
							flag=true;
							Log.failsoft(" System is not display the Month (1 to 12) accordingly in the respective order");
							break;
						}
						else{
							flag=true;
						}
					}
					for(int dayIndex=1;dayIndex<lstMonthlist.size();dayIndex++){

						if(lstMonthlist.get(dayIndex).getAttribute("label").equals(dateElement)){
							BrowserActions.mouseHover(driver, lstMonthlistValue);
							BrowserActions.scrollToView(lstMonthlist.get(dayIndex), driver);
							BrowserActions.clickOnElementX(lstMonthlist.get(dayIndex), driver, "month selection");
							break;
						}

					}


				}

			}

			break;

		case "day":
			String dayindex="";
			if(Utils.waitForElement(driver, selectDay)){

				BrowserActions.clickOnElementX(selectDay, driver, "Day drop down");

				if(Utils.waitForElement(driver, lstDaylist.get(0))){
					for(int dayIndex=1;dayIndex<lstDaylist.size();dayIndex++){

						if(dayIndex<=9){
							dayindex="0"+String.valueOf(dayIndex);

						}else{
							dayindex=String.valueOf(dayIndex);
						}
						if(!lstDaylist.get(dayIndex).getAttribute("label").equals(dayindex)){
							flag=true;
							Log.failsoft(" System is not display the Day (1 to 31) accordingly in the respective order");
							break;
						}
						else{
							flag=true;
						}
					}

					for(int dayIndex=1;dayIndex<lstDaylist.size();dayIndex++){

						if(lstDaylist.get(dayIndex).getAttribute("label").equals(dateElement)){
							BrowserActions.mouseHover(driver, lstDaylistValue);
							BrowserActions.scrollToView(lstDaylist.get(dayIndex), driver);
							BrowserActions.clickOnElementX(lstDaylist.get(dayIndex), driver, "Day selection");
							break;
						}

					}

				}

			}

			break;
		case "year":
			if(Utils.waitForElement(driver, selectYear)){
				String yearindex="";
				BrowserActions.clickOnElementX(selectYear, driver, "year drop down");

				if(Utils.waitForElement(driver, lstYearlist.get(0))){
					for(int dayIndex=1;dayIndex<lstYearlist.size();dayIndex++){

						if(dayIndex<=9){
							yearindex="0"+String.valueOf(2018-dayIndex);

						}else{
							yearindex=String.valueOf(2018-dayIndex);
						}

						if(!lstYearlist.get(dayIndex).getAttribute("label").equals(yearindex)){
							flag=true;
							break;
						}
						else{
							flag=true;
						}
					}

					for(int dayIndex=1;dayIndex<lstYearlist.size();dayIndex++){

						if(lstYearlist.get(dayIndex).getAttribute("label").equals(dateElement)){
							BrowserActions.mouseHover(driver, lstYearlistValue);
							BrowserActions.scrollToView(lstYearlist.get(dayIndex), driver);

							BrowserActions.clickOnElementX(lstYearlist.get(dayIndex), driver, "Year selection");
							break;
						}

					}

				}

			}

			break;
		}

		return flag;

	}

	/**
	 * To verify authorize buyer state
	 * @param blockState - 
	 * @param obj - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifyAuthorizeBuyerState(String blockState, Object obj )throws Exception{
		boolean flag=true;
		if(blockState.equals("open")){
			if(elementLayer.verifyAttributeForElement("autherisedBuyerBlock", "style", "display: block", obj)){
				Log.message("Block is already open");
			}else{
				if(Utils.waitForElement(driver, iconExpand)){

					BrowserActions.clickOnElementX(iconExpand, driver, "Autherized Buyer Symbol");
					Utils.waitForElement(driver, autherisedBuyerBlock);
					Thread.sleep(1500);
					flag=elementLayer.verifyAttributeForElement("autherisedBuyerBlock", "style", "display: block;", obj);
				}
			}
		}
		if(blockState.equals("close")){
			if(elementLayer.verifyAttributeForElement("autherisedBuyerBlock", "style", "display: none", obj)){
				Log.message("Block is already closed");
			}else{
				if(Utils.waitForElement(driver, iconExpand)){

					BrowserActions.clickOnElementX(iconExpand, driver, "Autherized Buyer Symbol");
					Utils.waitForElement(driver, autherisedBuyerBlock);
					Thread.sleep(1500);
					flag=elementLayer.verifyAttributeForElement("autherisedBuyerBlock", "style", "display: none;", obj);
				}
			}
		}

		return flag;
	}


	/**
	 * To click on consent checkbox
	 * @param obj - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean clickOnConsentCheckbox(Object obj)throws Exception{
		boolean flag=true;
		if(Utils.waitForElement(driver,selectCheckboxMandatoryError)){
			BrowserActions.clickOnElementX(selectCheckboxMandatoryError, driver, "Consent Checkbox");
		}
		if(Utils.waitForElement(driver, consentCheckboxError)){


			flag=elementLayer.verifyAttributeForElement("consentCheckboxError", "style", "display: none", obj);
		}
		//consentCheckbox

		return flag;
	}


	/**
	 * To verify PLCC approved overlay
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifyplccApprovedOverlay()throws Exception{
		boolean flag=true;
		if(Utils.waitForElement(driver, plccApprovedOverlay)){

		}

		return flag;
	}

	/**
	 * To verify place holder
	 * @param element - 
	 * @param obj - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean vaidateplaceholder(String element, Object obj)throws Exception{
		boolean flag=false;
		WebElement verifyElement=ElementLayer.getElement(element, obj);
		WebElement placeholder=verifyElement.findElement(By.cssSelector(".input-focus"));
		if(placeholder.isDisplayed())
		{
			flag=true;
		}
		else 
		{
			Log.message("Placeholder did not change for "+element);
		}
		return flag;
	}


	/**
	 * To verify SSN input text masked
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifySsnInputtextIsMasked()throws Exception{
		boolean flag=false;

		if(ssnInputText.getAttribute("type").equals("hidden")){
			flag=true;
		}

		return flag;
	}

	/**
	 * To click SSN tool tip
	 * @throws Exception - Exception
	 */
	public void clickOnSSNToolTip()throws Exception{
		Log.event("Trying to click on Toot tip in PLCC");
		BrowserActions.clickOnElementX(icoSSNToolTip, driver, "SSN Tool Tip");
	}

	/**
	 * To click on SSN tool tip close
	 * @throws Exception - Exception
	 */
	public void clickOnSSNToolTipClose()throws Exception{
		Log.event("Trying to click on Close button in Tool Tip modal.");
		BrowserActions.clickOnElementX(iconCloseToolTip, driver, "Tool Tip close Icon");
	}

	/**
	 * To click see benefits link
	 * @throws Exception - Exception
	 */

	public void clickSeeBenefits()throws Exception{
		BrowserActions.clickOnElementX(linkSeeBenefits, driver, "See Benefit Link");
		Utils.waitForPageLoad(driver);
	}



	/**
	 * To click cancel button
	 * @throws Exception - Exception
	 */
	public void clickCancelbtn()throws Exception{
		BrowserActions.clickOnElementX(btnCancel, driver, "Cancel Button");
		Utils.waitForPageLoad(driver);
	}

}






