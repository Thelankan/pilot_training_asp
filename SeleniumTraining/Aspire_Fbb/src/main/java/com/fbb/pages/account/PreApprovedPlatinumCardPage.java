package com.fbb.pages.account;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class PreApprovedPlatinumCardPage extends LoadableComponent<PreApprovedPlatinumCardPage>{
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	String runPltfrm = Utils.getRunPlatForm();

	/**********************************************************************************************
	 ********************************* WebElements of Platinum Cards Page ****************************
	 **********************************************************************************************/

	@FindBy(css = ".pt_specials")
	WebElement readyElement;

	@FindBy(css ="div[class*='pt_specials']" )
	WebElement lnkNavPlatinumCards;

	@FindBy(css =".plcc-top-content" )
	WebElement topContentSlot;
	
	@FindBy(css =".plcc-landing-perks .content-asset .perks-content" )
	WebElement platinumPerksLogos;

	@FindBy(css =".plccbenefits-n-email .plcc-platinumcard-benefits" )
	WebElement exclusiveBenefitsCopy;
	
	@FindBy(css =".plcc-pre-approval-points" )
	WebElement plccCreditLimit;

	@FindBy(css =".plccbenefits-n-email .footer-email-signup .email-signup.make-label-absolute" )
	WebElement emailSignup;

	@FindBy(css =".perks-offer.clearboth" )
	WebElement freeShippingBlock;

	@FindBy(css =".bottom-content" )
	WebElement bottomContentSlot;

	@FindBy(css =".header-promo-bottom .promo-banner" )
	WebElement promotionalContent;

	@FindBy(css =".plccbenefits-n-email .form-row.emailsignup.email.required input" )
	WebElement emailTextField;

	@FindBy(css =".plccbenefits-n-email .form-row.emailsignup.email.required .field-wrapper" )
	WebElement emailField;

	@FindBy(css =".plccbenefits-n-email .label-text" )
	WebElement emailTextFieldPlaceHolder;

	@FindBy(css =".plccbenefits-n-email .input-focus .error" )
	WebElement emailPlaceHolderIncorrectErrorText;

	@FindBy(css =".plccbenefits-n-email .error" )
	WebElement emailPlaceHolderEmptyErrorText;

	@FindBy(css =".plccbenefits-n-email .form-row.form-row-button button" )
	WebElement signupButton;

	@FindBy(css =".plcc-top-content .plcc-top-banner .button")
	WebElement applyButton;

	@FindBy(css =".plccbenefits-n-email .form-row.form-row-button" )
	WebElement signup;

	@FindBy(css =".plccbenefits-n-email .email-signup.make-label-absolute .email-signup-footer-success .content-asset" )
	WebElement thankYouMessage;

	@FindBy(css =".plccbenefits-n-email .plcc-platinumcard-benefits .plcc-benefits-head" )
	WebElement exclusiveBenefitsContentHeading ;

	@FindBy(css =".plccbenefits-n-email .plcc-platinumcard-benefits .plcc-benefit-msg" )
	WebElement exclusiveBenefitsContentMessage ;

	@FindBy(css = ".plcc-landing-content .plcc-top-banner .bck-images")
	WebElement divBannerElement;

	@FindBy(css = ".plcc-secondary a")
	WebElement divSeeBenifits;

	@FindBy(css = ".plcc-top-content")
	WebElement divPlccTopContent;
	
	@FindBy(css= ".plcc-main-heading")
	WebElement formHeading;

	@FindBy(css = ".plcc-main .plcc-secondary .plcc-left-sections .plcc-left-section.plcc-left-section-1 .plcc-left-nav-heading")
	WebElement divInfoMessage;

	@FindBy(css = ".web-instant-submit")
	WebElement BtnInstantSubmit;
	
	@FindBy(css = ".form-action .pre-approved-submit")
	WebElement btnPreApprovedSubmit;


	/*********************************************************
	 * **************** WebElements of Mandatory Error********
	 *********************************************************/
	@FindBy(xpath = "//div[@class='form-row  firstName required error-handle']")
	WebElement txtFirstNameMandatoryError;

	@FindBy(xpath = "//div[@class='form-row  lastName required error-handle']")
	WebElement txtLastNameMandatoryError;

	@FindBy(xpath = "//div[@class='form-row  email required error-handle']")
	WebElement txtEmailMandatoryError;
	
	@FindBy (css = ".make-label-absolute .form-row.email.error-handle .input-focus .error")
	WebElement txtInvalidEmailError;

	@FindBy(xpath = "//div[@class='form-row  password required error-handle']")
	WebElement txtPasswordMandatoryError;

	@FindBy(xpath = "//div[@class='form-row  address1 required error-handle']")
	WebElement txtAddressOneMandatoryError;

	@FindBy(xpath = "//div[@class='form-row  city required error-handle']")
	WebElement txtcityMandatoryError;

	@FindBy(css = ".form-row.required.state.error-handle >label")
	WebElement selectStateMandatoryError;

	@FindBy(xpath = "//div[@class='form-row  zipcode required error-handle']")
	WebElement txtzipcodeMandatoryError;
	
	@FindBy(css = ".make-label-absolute:not(.catalog-quick-order__main) .form-row label span.error")
	WebElement txtInvalidZipError;

	@FindBy(css= ".form-row.phone.required.error-handle .error")
	WebElement txtPhoneMandatoryError;
	
	@FindBy(css= ".form-row.alternativePhone.error-handle .input-focus .error")
	WebElement txtInvalidAlternatePhoneError;

	@FindBy(xpath = "//div[@class='form-row  ssn required error-handle']")
	WebElement txtSsnMandatoryError;

	@FindBy(css = "#dwfrm_creditapplication_birthday_month-error")
	WebElement selectMonthMandatoryError;

	@FindBy(css = "#dwfrm_creditapplication_birthday_day-error")
	WebElement selectDateMandatoryError;

	@FindBy(css = "#dwfrm_creditapplication_birthday_year-error")
	WebElement selectYearMandatoryError;

	@FindBy(css = ".input-checkbox.required.error")
	WebElement selectCheckboxMandatoryError;

	@FindBy(css = ".make-label-absolute .form-row.email .option-text")
	WebElement selectEmailOptionalTxt;

	@FindBy(css = ".form-row.password .option-text")
	WebElement txtpasswordField;

	@FindBy(css = ".form-row.alternativePhone.show-text .label-text")
	WebElement selectAlternativePhoneOptionalTxt;


	/*********************************************************
	 * **************** WebElements of Register page element********
	 *********************************************************/
	@FindBy(css = ".plcc-top-image-conatiner")
	WebElement plccBannerElement;
	
	@FindBy(css= ".plcc-edit-profile")
	WebElement btnEditProfile;
	
	
	@FindBy(css = ".form-row.firstName.required .input-text")
	WebElement txtFirstName;

	@FindBy(css = ".form-row.lastName.required .input-text")
	WebElement txtLastName;

	@FindBy(css = ".form-row.address1.required .input-text")
	WebElement txtaddressOne;

	@FindBy(css = ".form-row.address2 .input-text")
	WebElement txtaddressTwo;

	@FindBy(css = ".form-row.city.required .input-text")
	WebElement txtcity;

	@FindBy(css = ".form-row.zipcode.required .input-text")
	WebElement txtZipCode;

	@FindBy(css = ".plcc-form-row.plcc-form-row-2 .make-label-absolute .form-row.email .input-text.email")
	WebElement txtEmail;

	@FindBy(css = ".plcc-form-section .make-label-absolute .form-row.alternativePhone .input-text.alternativePhone")
	WebElement txtAlternativePhone;

	@FindBy(css = ".form-row.phone input")
	WebElement txtphone;

	@FindBy(css = ".form-row.ssn.required .input-text")
	WebElement txtSsnNO;
	
	@FindBy(css= "#tool-dialog-container > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > p:nth-child(2)")
	WebElement toolTipForPLCC;

	@FindBy(css = ".form-row.month .field-wrapper .selected-option.selected")
	WebElement selectMonth;
	
	@FindBy(css = ".form-row.day .field-wrapper .selected-option.selected")
	WebElement selectDay;

	@FindBy(css = ".form-row.year .field-wrapper .selected-option.selected")
	WebElement selectYear;
	
	@FindBy(css = ".form-row.month.required .input-select .select-option")
	List<WebElement> lstMonthlist;

	@FindBy(css = ".form-row.month.required .selection-list")
	WebElement lstMonthlistValue;
	
	@FindBy(css = ".form-row.day.required .input-select .select-option")
	List<WebElement> lstDaylist;

	@FindBy(css = ".form-row.day.required .selection-list")
	WebElement lstDaylistValue;

	@FindBy(css = ".form-row.year.required .input-select .select-option")
	List<WebElement> lstYearlist;
	
	@FindBy(css = ".form-row.year.required .selection-list")
	WebElement lstYearlistValue;

	@FindBy(css = ".form-row.day.required .label-text")
	WebElement selectDayLabel;

	@FindBy(css = ".form-row.month.required .label-text")
	WebElement selectMonthLabel;

	@FindBy(css = ".form-row.year.required .label-text")
	WebElement selectYearLabel;

	@FindBy(css = ".form-row.phone-number-msg")
	WebElement divContactDisclaimer;

	@FindBy(css = ".plcc-authorized-buyer .cr-icon")
	WebElement divAuthorizedBuyer;

	@FindBy(css = ".plcc-form-section")
	WebElement divPlccFormSection;

	@FindBy(css = ".plcc-main .plcc-secondary .plcc-left-sections .plcc-left-section.plcc-left-section-1 .plcc-left-nav-heading")
	WebElement txtOR;

	@FindBy(css = ".cr-icon")
	WebElement iconExpand;

	@FindBy(css = ".plcc-authorized-buyer-section.display-inline-block.width-full")
	WebElement autherisedBuyerBlock;
	
	@FindBy(css = ".bottom-buttons")
	WebElement divBottomButtons;


	/*********************************************************
	 * **************** WebElements of Register page PlaceHolder********
	 *********************************************************/
	@FindBy(css = ".form-row.firstName.required.show-text .input-focus .label-text")
	WebElement spanPlaceHolderFirstName;

	@FindBy(css = ".form-row.lastName.required.show-text .input-focus .label-text")
	WebElement spanPlaceHolderLastName;

	@FindBy(css = ".form-row.address1.required.show-text .input-focus .label-text")
	WebElement spanPlaceHolderAddressOne;

	@FindBy(css = ".form-row.address2.show-text .input-focus .label-text")
	WebElement spanPlaceHolderAddressTwo;

	@FindBy(css = ".form-row.city.required.show-text .input-focus .label-text")
	WebElement spanPlaceHolderCity;

	@FindBy(css = ".form-row.state.required.show-text .input-focus .label-text")
	WebElement spanPlaceHolderState;

	@FindBy(css = ".form-row.zipcode.required.show-text .input-focus .label-text")
	WebElement spanPlaceHolderZipcode;

	@FindBy(css = ".form-row.email.show-text .input-focus .label-text")
	WebElement spanPlaceHolderEmail;

	@FindBy(css = ".form-row.phone.show-text .input-focus .label-text")
	WebElement spanPlaceHolderPhone;

	@FindBy(css = ".form-row.alternativePhone.show-text .input-focus .label-text")
	WebElement spanPlaceHolderaAlternativePhone;

	@FindBy(css = ".form-row.ssn.required.required.show-text .input-focus .label-text")
	WebElement spanPlaceHolderSSN;

	@FindBy(css = ".form-row.month label")
	WebElement spanPlaceHolderMonth;

	@FindBy(css = ".form-row.day label")
	WebElement spanPlaceHolderDay;

	@FindBy(css = ".form-row.year label")
	WebElement spanPlaceHolderYear;

	@FindBy(css = ".form-row.required.state .selected-option.selected")
	WebElement selectStateDrop;
	
	@FindBy(css= ".form-row.required.state .field-wrapper .selection-list li")
	List<WebElement> lstStateList;
	
	@FindBy(css = ".form-row.state .field-wrapper .selection-list")
    WebElement lstStateListValue;

	@FindBy(css = ".form-row.phone-number-msg .field-wrapper")
	WebElement divContactMessage;

	@FindBy(css= ".iframe-content.account-term-iframe-block")
	WebElement accountTerms;

	@FindBy(css = ".financial-disc-iframe")
	WebElement FinanceTerms;
	
	@FindBy(css = ".legal-section-paragraph strong span")
	WebElement lblAPR;

	@FindBy(css = ".clearboth.plcc-bottom-section .content-asset")
	WebElement contentTerms;

	@FindBy(css = ".input-checkbox.required.valid")
	WebElement consentCheckbox;

	@FindBy(id = "#dwfrm_creditapplication_consent-error")
	WebElement consentCheckboxError;

	@FindBy(css = ".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable.plcc-apply-error-model")
	WebElement plccApprovedOverlay;

	@FindBy(css = "label[for^='dwfrm_creditapplication_ssn_']")
	WebElement ssnplaceholder;

	@FindBy(css = ".form-row.address2 .option-text")
	WebElement txtAddressTwoOptional;

	@FindBy(css = "input[name*='dwfrm_creditapplication_ssn_']")
	WebElement ssnInputText;

	@FindBy(css = "label[for^='dwfrm_creditapplication_phone_']")
	WebElement phoneplaceholder;

	@FindBy(css = ".plcc-ssn-label .question-mark-icon.dialog-tooltip")
	WebElement icoSSNToolTip;

	@FindBy(css = ".tooltip-dialog .ui-dialog-titlebar-close")
	WebElement iconCloseToolTip;

	@FindBy(css = "input[id*='dwfrm_creditapplication_phone_']")
	WebElement txtMobileNoInPLCCStep2;	

	@FindBy(css = "label[for^='dwfrm_creditapplication_alternativePhone_']")
	WebElement alternativePhoneplaceholder;

	@FindBy(css = ".account-term-iframe")
	WebElement consenttoAccount;

	@FindBy(css = ".financial-disc-iframe")
	WebElement consenttoFinance;

	@FindBy(css = ".financial-disc-iframe")
	WebElement creditcardDisclaimer;

	@FindBy(css = ".input-checkbox.required ")
	WebElement consentChkBox;

	@FindBy(css = ".plcc-landing-content")
	WebElement divplccLandingPage;

	@FindBy(css = ".phone-number-msg:nth-child(8)")
	WebElement phoneContactDisclaimer;
	
	@FindBy(css = ".phone-number-msg:nth-child(2)")
	WebElement phoneContactDisclaimerAlt;

	@FindBy(css = ".see-benefits")
	WebElement linkSeeBenefits;

	@FindBy(css = ".landing-page>button")
	WebElement btnCancel;
	
	@FindBy(css = ".plcc-profile-summary")
	WebElement divProfileSummery;
	/**********************************************************************************************
	 ********************************* WebElements of Platinum Cards Page ****************************
	 **********************************************************************************************/	


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public PreApprovedPlatinumCardPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}




	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Platinum Page did not open up.", driver);
		}
	}

	/**
	 * To Type text into Email Field
	 * @param String - email 
	 * @throws Exception - Exception
	 */
	public void typeOnFields(String email)throws Exception{
		emailTextField.clear();
		BrowserActions.typeOnTextField(emailTextField, email, driver, "Email Address ");
	}

	/**
	 * To click on apply button
	 * @throws Exception - Exception
	 */
	public void clickOnApplyButton()throws Exception{
		Utils.waitForElement(driver, applyButton);
		BrowserActions.scrollToView(applyButton, driver);
		BrowserActions.clickOnElementX(applyButton, driver, "Apply Button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To error message
	 * @param textToType - 
	 * @param errorMessage - 
	 * @param fieldToVerify - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifyFieldErrorMessages(String textToType, String errorMessage ,String fieldToVerify)throws Exception{
		boolean flag=false;

		switch(fieldToVerify){
		case "FirstName":
			BrowserActions.typeOnTextField(txtFirstName, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtFirstNameMandatoryError);

			Log.event("Expected Error Message:: " + errorMessage.trim());
			Log.event("Actual Error Message:: " + txtFirstNameMandatoryError.getText().trim());
			if(txtFirstNameMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;
		case "LastName":
			BrowserActions.typeOnTextField(txtLastName, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtLastNameMandatoryError);
			
			Log.event("Expected Error Message:: " + errorMessage.trim());
			Log.event("Actual Error Message:: " + txtLastNameMandatoryError.getText().trim());
			if(txtLastNameMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;
		case "Email":
			BrowserActions.typeOnTextField(txtEmail, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtInvalidEmailError);
			
			Log.event("Expected Error Message:: " + errorMessage.trim());
			Log.event("Actual Error Message:: " + txtInvalidEmailError.getText().trim());
			if(txtInvalidEmailError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;
		case "Password":
			BrowserActions.typeOnTextField(txtpasswordField, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtPasswordMandatoryError);
			
			Log.event("Expected Error Message:: " + errorMessage.trim());
			Log.event("Actual Error Message:: " + txtPasswordMandatoryError.getText().trim());
			if(txtPasswordMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}
			break;

		case "zipCode":
			BrowserActions.typeOnTextField(txtZipCode, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtzipcodeMandatoryError);

			Log.event("Expected Error Message:: " + errorMessage.trim());
			Log.event("Actual Error Message:: " + txtzipcodeMandatoryError.getText().trim());
			if(txtzipcodeMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;

		case "PhoneNumber":
			BrowserActions.typeOnTextField(txtphone, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtPhoneMandatoryError);

			Log.event("Expected Error Message:: " + errorMessage.trim());
			Log.event("Actual Error Message:: " + txtPhoneMandatoryError.getText().trim());
			if(txtPhoneMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;

		case "SSN":
			BrowserActions.typeOnTextField(txtSsnNO, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtSsnMandatoryError);

			System.out.println("Expected Error Message:: " + errorMessage.trim());
			System.out.println("Actual Error Message:: " + txtSsnMandatoryError.getText().trim());
			if(txtSsnMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;
			
		case "AlternatePhoneNumber":
			BrowserActions.typeOnTextField(txtAlternativePhone, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtInvalidAlternatePhoneError);

			System.out.println("Expected Error Message:: " + errorMessage.trim());
			System.out.println("Actual Error Message:: "+ txtInvalidAlternatePhoneError.getText().trim());
			if(txtInvalidAlternatePhoneError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}
			break;
		}
		return flag;
	}

	/**
	 * To enter text on field
	 * @param element - 
	 * @param textToType - 
	 * @param Description - 
	 * @param obj - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean enterTextOnField(String element,String textToType,String Description,Object obj)throws Exception{

		boolean flag=false;

		WebElement verifyElement=ElementLayer.getElement(element, obj);
		BrowserActions.typeOnTextField(verifyElement, textToType, driver, Description);
		String customerTextValue=verifyElement.getAttribute("value");
		if(customerTextValue.equals(textToType)){
			flag=true;
		}
		if(Description.contains("SSN")) {
			if(customerTextValue.substring(7).equals(textToType.substring(5))) {
				flag=true;
			}
		}
		Log.event(textToType+", "+ customerTextValue);
		return flag;

	}
	
	/**
	 * Tp verify  fields are populated
	 * @param element -
	 * @param obj -
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyFieldIsPrePopulated(String element, Object obj) throws Exception{

		WebElement verifyElement=ElementLayer.getElement(element, obj); 	
		if(verifyElement.getAttribute("value").length()>0)
				return true;
		else
			return false;
	}

	/**
	 * To verify optional field
	 * @param element - 
	 * @param obj - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifyOptionalField(String element,Object obj)throws Exception{
		boolean flag=false;

		if (Utils.waitForElement(driver, ElementLayer.getElement(element, obj))){

			flag=true;
		}

		return flag;
	}

	/**
	 * To select month
	 * @param State - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean selectMonth(String State) throws Exception {
		boolean flag=false;
		Utils.waitForElement(driver, selectStateDrop);
		BrowserActions.clickOnElementX(selectStateDrop, driver, "State dropdown");
		Utils.waitForElement(driver, lstStateListValue);
		BrowserActions.mouseHover(driver, lstStateListValue);
		for(int stateIndex= 0; stateIndex< lstStateList.size(); stateIndex++) {
			BrowserActions.scrollToView(lstStateList.get(stateIndex), driver);
			if(lstStateList.get(stateIndex).getAttribute("label").trim().equalsIgnoreCase(State)) {
				BrowserActions.clickOnElementX(lstStateList.get(stateIndex), driver, "Selecting a State");
				flag=true;
			}
		}
		return flag;
	}

	/**
	 * To verify and select state element
	 * @param dateType - 
	 * @param dateElement - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifyandSelectDateElements(String dateType,String dateElement)throws Exception{

		boolean flag=true;
		switch(dateType){
		case "month":
			if(Utils.waitForElement(driver, selectMonth)){
				String monthindex="";
				BrowserActions.clickOnElementX(selectMonth, driver, "month drop down");
				if(Utils.waitForElement(driver, lstMonthlist.get(0))){
					for(int dayIndex=1;dayIndex<lstMonthlist.size();dayIndex++){
						if(dayIndex<=9){
							monthindex="0"+String.valueOf(dayIndex);
						}else{
							monthindex=String.valueOf(dayIndex);
						}
						if(!lstMonthlist.get(dayIndex).getAttribute("label").equals(String.valueOf(monthindex) )){
							flag=true;
							Log.failsoft(" System is not display the Month (1 to 12) accordingly in the respective order");
							break;
						}
						else{
							flag=true;
						}
					}
					for(int dayIndex=1;dayIndex<lstMonthlist.size();dayIndex++){
						if(lstMonthlist.get(dayIndex).getAttribute("label").equals(dateElement)){
							BrowserActions.mouseHover(driver, lstMonthlistValue);
							BrowserActions.scrollToView(lstMonthlist.get(dayIndex), driver);
							BrowserActions.clickOnElementX(lstMonthlist.get(dayIndex), driver, "month selection");
							break;
						}
					}
				}
			}
			break;
		case "day":
			String dayindex="";
			if(Utils.waitForElement(driver, selectDay)){
				BrowserActions.clickOnElementX(selectDay, driver, "Day drop down");
				if(Utils.waitForElement(driver, lstDaylist.get(0))){
					for(int dayIndex=1;dayIndex<lstDaylist.size();dayIndex++){
						if(dayIndex<=9){
							dayindex="0"+String.valueOf(dayIndex);

						}else{
							dayindex=String.valueOf(dayIndex);
						}
						if(!lstDaylist.get(dayIndex).getAttribute("label").equals(dayindex)){
							flag=true;
							Log.failsoft(" System is not display the Day (1 to 31) accordingly in the respective order");
							break;
						}
						else{
							flag=true;
						}
					}
					for(int dayIndex=1;dayIndex<lstDaylist.size();dayIndex++){
						if(lstDaylist.get(dayIndex).getAttribute("label").equals(dateElement)){
							BrowserActions.mouseHover(driver, lstDaylistValue);
							BrowserActions.scrollToView(lstDaylist.get(dayIndex), driver);
							BrowserActions.clickOnElementX(lstDaylist.get(dayIndex), driver, "Day selection");
							break;
						}
					}
				}
			}
			break;
		case "year":
			if(Utils.waitForElement(driver, selectYear)){
				String yearindex="";
				BrowserActions.clickOnElementX(selectYear, driver, "year drop down");
				if(Utils.waitForElement(driver, lstYearlist.get(0))){
					for(int dayIndex=1;dayIndex<lstYearlist.size();dayIndex++){
						if(dayIndex<=9){
							yearindex="0"+String.valueOf(2018-dayIndex);
						}else{
							yearindex=String.valueOf(2018-dayIndex);
						}
						if(!lstYearlist.get(dayIndex).getAttribute("label").equals(yearindex)){
							flag=true;
							break;
						}
						else{
							flag=true;
						}
					}
					for(int dayIndex=1;dayIndex<lstYearlist.size();dayIndex++){
						if(lstYearlist.get(dayIndex).getAttribute("label").equals(dateElement)){
							BrowserActions.mouseHover(driver, lstYearlistValue);
							BrowserActions.scrollToView(lstYearlist.get(dayIndex), driver);
							BrowserActions.clickOnElementX(lstYearlist.get(dayIndex), driver, "Year selection");
							break;
						}
					}
				}
			}
			break;
		}
		return flag;
	}

	/**
	 * To verify authorize buyer state
	 * @param blockState - 
	 * @param obj - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifyAuthorizeBuyerState(String blockState, Object obj )throws Exception{
		boolean flag=true;
		if(blockState.equals("open")){
			if(elementLayer.verifyAttributeForElement("autherisedBuyerBlock", "style", "display: block", obj)){
				Log.message("Block is already open");
			}else{
				if(Utils.waitForElement(driver, iconExpand)){

					BrowserActions.clickOnElementX(iconExpand, driver, "Autherized Buyer Symbol");
					Utils.waitForElement(driver, autherisedBuyerBlock);
					Thread.sleep(1500);
					flag=elementLayer.verifyAttributeForElement("autherisedBuyerBlock", "style", "display: block;", obj);
				}
			}
		}
		if(blockState.equals("close")){
			if(elementLayer.verifyAttributeForElement("autherisedBuyerBlock", "style", "display: none", obj)){
				Log.message("Block is already closed");
			}else{
				if(Utils.waitForElement(driver, iconExpand)){

					BrowserActions.clickOnElementX(iconExpand, driver, "Autherized Buyer Symbol");
					Utils.waitForElement(driver, autherisedBuyerBlock);
					Thread.sleep(1500);
					flag=elementLayer.verifyAttributeForElement("autherisedBuyerBlock", "style", "display: none;", obj);
				}
			}
		}

		return flag;
	}

	/**
	 * To click on consent checkbox
	 * @param obj - 
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean clickOnConsentCheckbox(Object obj)throws Exception{
		boolean flag=true;
		if(Utils.waitForElement(driver,selectCheckboxMandatoryError)){
			BrowserActions.clickOnElementX(selectCheckboxMandatoryError, driver, "Consent Checkbox");
		}
		if(Utils.waitForElement(driver, consentCheckboxError)){


			flag=elementLayer.verifyAttributeForElement("consentCheckboxError", "style", "display: none", obj);
		}
		//consentCheckbox

		return flag;
	}


	/**
	 * To verify PLCC approved overlay
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifyplccApprovedOverlay()throws Exception{
		boolean flag=true;
		if(Utils.waitForElement(driver, plccApprovedOverlay)){

		}

		return flag;
	}

	/**
	 * To verify SSN input text masked
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifySSNInputTextIsMasked()throws Exception{
		boolean flag=false;
		if(ssnInputText.getAttribute("type").equals("hidden")){
			flag=true;
		}
		return flag;
	}

	/**
	 * To click SSN tool tip
	 * @throws Exception - Exception
	 */
	public void clickOnSSNToolTip()throws Exception{
		Log.event("Trying to click on Toot tip in PLCC");
		BrowserActions.clickOnElementX(icoSSNToolTip, driver, "SSN Tool Tip");
	}

	/**
	 * To click on SSN tool tip close
	 * @throws Exception - Exception
	 */
	public void clickOnSSNToolTipClose()throws Exception{
		Log.event("Trying to click on Close button in Tool Tip modal.");
		BrowserActions.clickOnElementX(iconCloseToolTip, driver, "Tool Tip close Icon");
	}

	/**
	 * To click see benefits link
	 * @throws Exception - Exception
	 */

	public void clickSeeBenefits()throws Exception{
		BrowserActions.clickOnElementX(linkSeeBenefits, driver, "See Benefit Link");
		Utils.waitForPageLoad(driver);
	}



	/**
	 * To click cancel button
	 * @throws Exception - Exception
	 */
	public void clickCancelbtn()throws Exception{
		BrowserActions.clickOnElementX(btnCancel, driver, "Cancel Button");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on submit button
	 * @throws Exception -
	 */
	public void clickSubmit() throws Exception{
		BrowserActions.clickOnElementX(btnPreApprovedSubmit, driver, "Click Submit for Pre-Approved user.");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on edit profile
	 * @throws Exception -
	 */
	public void clickEditProfile() throws Exception{
		BrowserActions.clickOnElementX(btnEditProfile, driver, "Click Pre-Approved user information edit");
	}

}
