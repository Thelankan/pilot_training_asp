package com.fbb.pages.headers;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.PlpPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OffersAndCouponsPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.account.RewardPointsPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.customerservice.TrackOrderPage;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class HamburgerMenu extends LoadableComponent<HamburgerMenu> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public String brand = System.getProperty("brand");
	String runPltfrm = Utils.getRunPlatForm();

	private static final String FOOTER_MOBILE = ".menu-footer ";
	private static final String SLIDER_MENU_MOBILE = ".slider-menu ";
	private static final String SLIDER_MENU_UTILITY_MOBILE = SLIDER_MENU_MOBILE + ".menu-utility ";
	//=====================================================================//
	//	-------------------Mobile Elements-----------------------------    //
	//=====================================================================//

	@FindBy(css = "button[class='menu-toggle']")
	WebElement lnkHamburger;

	@FindBy(css = ".hamburger-toggle.open")
	WebElement lnkHamburgerOpened;

	@FindBy(css = "button.menu-toggle")
	WebElement btnHamburgerClose;
	
	@FindBy(css = ".offer-flyout-main")
	WebElement lnkOffers;

	@FindBy(css = FOOTER_MOBILE + "input[id*='dwfrm_emailsignup_email']")
	WebElement fldEmailSignUp;
	
	@FindBy(css = FOOTER_MOBILE + "button[name='dwfrm_emailsignup_signup']")
	WebElement btnEmailSignUp;
	
	@FindBy(css = FOOTER_MOBILE + "span[id*='dwfrm_emailsignup_email_']")
	WebElement txtEmailSignUpError;
	
	@FindBy(css = FOOTER_MOBILE + "div.footer_item:nth-child(1)")
	WebElement divFooterCustomerService;
	
	@FindBy(css = ".slider-menu .quick-links")
	WebElement hMenuQuickLinks;
	
	@FindBy(css = "a.user-account")
	WebElement lnkSignIn;
	
	@FindBy(css = ".mobile-sign-in .user-info.info a")
	WebElement lnkSignInMobile;

	@FindBy(css = ".user-account .log-in")
	WebElement imgUserProfile;

	@FindBy(css = "#loggedInUserName")
	WebElement lblUserName;

	@FindBy(css = "span.my-account")
	WebElement lnkMyAccount;
	
	@FindBy(xpath = "//div[@class='account-links']/a[1]")
	WebElement lnkAccOverview;

	@FindBy(css = ".slider-menu .menu-item-toggle.fa-chevron-right")
	WebElement lnkMenuItemToggle;
	
	@FindBy(css = ".slider-menu  .menu-category.level-1>li>a")
	WebElement lnkCategoryName;

	@FindBy(css = ".slider-menu div.header-brand-selector")
	WebElement divUtilityBar;
	
	@FindBy(css = ".slider-menu .woman-within")
	WebElement lnkSliderMenuWomansWithin;
	
	@FindBy(css = ".slider-menu .jessica")
	WebElement lnkSliderMenuJessica;
	
	@FindBy(css = ".slider-menu .roman")
	WebElement lnkSliderMenuRoman;
	
	@FindBy(css = SLIDER_MENU_MOBILE + ".offer-flyout-main .offer-flyout")
	WebElement lnkSliderMenuOfferFlyOut;
	
	@FindBy(css = ".active>.menu-item-toggle.fa.fa-chevron-up.active")
	WebElement lnkBackArrow;
	
	@FindBy(css = SLIDER_MENU_MOBILE + ".sub-cat-nav")
	WebElement divSubCategories;

	@FindBy(css = "ul.level-1>li")
	List<WebElement> lnkMenuItems;
	
	@FindBy(css = ".active .active .has-sub-menu")
	WebElement lblActiveMenuHeading;
	
	@FindBy(css = ".active .active .menu-item-toggle")
	WebElement lblActiveMenuToggleBack;
	
	@FindBy(css = ".active .active div>ul>li.nav-viewAll")
	WebElement lnkLevel2ViewAll;
	
	@FindBy(css = ".active .has-sub-menu")
	WebElement lnkSelectedCategory;
	
	@FindBy(css = ".slider-menu .level-1 .menu-item-toggle.fa-chevron-right")
	List<WebElement> lstCategoryArrow;
	
	@FindBy(css = ".slider-menu .level-1 > li > a")
	List<WebElement> lstCategoryName;

	@FindBy(css = SLIDER_MENU_MOBILE + ".menu-category.level-1")
	WebElement navigationCategory1;
	
	@FindBy(css = ".slider-menu li.active .level-2 .nav-column")
	List<WebElement> activeCategoryNav2;
	
	@FindBy(css = ".slider-menu li.active > a")
	WebElement activeCat2Header;
	
	@FindBy(css = ".active > .level-2")
	WebElement activeCat2Content;
	
	@FindBy(css = SLIDER_MENU_UTILITY_MOBILE + ".header-brand-selector .brand-logos a")
	List<WebElement> lnkOtherBrandLinksUnderNav;
	
	@FindBy(css = SLIDER_MENU_UTILITY_MOBILE + ".header-brand-selector")
	WebElement divOtherBrandLinksUnderNav;
	
	@FindBy(css = ".primary-links.hide-desktop.hide-tablet a[href*='help']")
	WebElement lnkCustomerService_Mobile;
	
	@FindBy(css = ".hamburger-toggle.open .menu-toggle")
	WebElement btnCloseHamburger;
	
	@FindBy(css = ".hamburger-toggle .menu-toggle")
	WebElement btnOpenHamburger;
	
	@FindBy(css = ".my-account.heading")
	WebElement lblMyAccountHeader;
	
	@FindBy(css = ".account-logout a")
	WebElement lnkSignOut;
	
	@FindBy(css = "a.Profile")
	WebElement lnkProfile;
	
	@FindBy(css = "a.Wishlist")
	WebElement lnkWishList;
	
	@FindBy(css = "a.Orderhistory")
	WebElement lnkOrderHistory;
	
	@FindBy(css = "a.Rewards")
	WebElement lnkRewardPoints;
	
	@FindBy(css = "a.Rewards")
	WebElement lnkQuickOrder; 	
	
	@FindBy(css = ".primary-links")
	WebElement divCustomerService;
	
	@FindBy(css = ".slider-menu li.active .level-2 .nav-viewAll a")
	WebElement lnkViewAllActive;
	
	@FindBy(css = ".email-signup-footer-success")
	WebElement lblEmailSignUpThankYou;
	
	@FindBy(css = ".slider-menu .email.icon")
	WebElement lnkEmailUs;
	
	@FindBy(css = ".slider-menu .livechat-icon a div")
	WebElement lnkChat;
	
	@FindBy(css = "#bc-chat-container")
	WebElement mdlLiveChat;
	
	@FindBy(css = ".slider-menu .quick-links a.Track-order")
	WebElement lnkTrackOrder;
	
	@FindBy(css = ".slider-menu .quick-links a.Return-items")
	WebElement lnkReturnItems;
	
	@FindBy(css = ".slider-menu .quick-links a.Update-information")
	WebElement lnkUpdateInfo;
	
	@FindBy(css = FOOTER_MOBILE + ".footer-nav .primary-links.hide-desktop.hide-tablet h2 a")
	WebElement btnCustomerService;
	
	@FindBy(css = ".top-banner .menuUnderLay")
	WebElement divHamburgerOverlay;
	//=====================================================================//
	//=====================================================================//
	//=====================================================================//


	

	//================================================================================
	//			WebElements Declaration End
	//================================================================================




	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public HamburgerMenu(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, lnkHamburgerOpened))) {
			Log.fail("Hamburger menu didn't display", driver);
		}
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the element which have exact variable name
	 * @param expectedEle - String "Expected Element"
	 * @return WebElement - Actual WebElement with expected Element variable name
	 * @throws Exception - Exception
	 */
	public static WebElement getElement(String expectedEle)throws Exception{
		WebElement element = null;
		try{
			Field elementField = HamburgerMenu.class.getClass().getDeclaredField(expectedEle);
			elementField.setAccessible(true);
			element = ((WebElement) elementField.get(HamburgerMenu.class.getClass()));
		}//try
		catch(Exception e){
			e.printStackTrace();

		}//catch
		return element;
	}

	
	
	/**
	 * To open/close the hamburger menu in mobile
	 * @param state - 
	 * @return object of pages
	 * @throws Exception - Exception
	 */
	public Object openCloseHamburgerMenu(String state) throws Exception {
		if (Utils.waitForElement(driver, lnkHamburger, 2) && (state.equals("open"))) {
			if (!Utils.waitForElement(driver, lnkHamburgerOpened, 2)) {
				BrowserActions.clickOnElementX(lnkHamburger, driver, "Hamburger menu ");
			}
			Log.event("Hamburger menu opened");
			return new HamburgerMenu(driver).get();
		}else if(Utils.waitForElement(driver, lnkHamburgerOpened, 2) && (state.equals("close"))){
			BrowserActions.javascriptClick(btnHamburgerClose, driver, "Hamburger menu ");
			Log.event("Hamburger menu closed");
			return new Headers(driver).get();
		}else if(Utils.waitForElement(driver, lnkHamburgerOpened, 2))
			return new HamburgerMenu(driver).get();
		else
			return new Headers(driver).get();
	}
	
	/**
	 * TO scroll to footer page
	 * @throws InterruptedException - 
	 */
	public void scrollToDown() throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", divFooterCustomerService);
		Log.event("Scrolled down to Customer Service in Hambuger menu!");
	}
	
	/**
	 * To click on the Email SignUp button
	 * @throws Exception - Exception
	 */
	public void clickOnEmailSignUp()throws Exception{
		BrowserActions.scrollToView(btnEmailSignUp, driver);
		BrowserActions.clickOnElementX(btnEmailSignUp, driver, "Email Sign Up ");
		
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To get the error message from Email signup error text
	 * @return String - Error Message
	 * @throws Exception - Exception
	 */
	public String getEmailSignUpErrMsg()throws Exception{
			return Utils.waitForElement(driver, txtEmailSignUpError) == true ?
					BrowserActions.getText(driver, txtEmailSignUpError, "Email SignUp Error Message ") :
						"No Error Message displayed on the page.";
	}
	
	/**
	 * To type the email address into email signup textbox
	 * @param txtToType - 
	 * @throws Exception - Exception
	 */
	public void typeInEmailSignUp(String txtToType)throws Exception{
		BrowserActions.typeOnTextField(fldEmailSignUp, txtToType, driver, "Email SignUp in Footer ");
	}
	
	/**
	 * To click on sign in
	 * @return SignIn - 
	 * @throws Exception - Exception
	 */
	public SignIn navigateToSignIn()throws Exception{
		Log.event("Clicking on SignIn button in Hamburger menu.");
		BrowserActions.clickOnElementX(lnkSignIn, driver, "SignIn Link ");
		return new SignIn(driver).get(); 
	}
	
	/**
	 * To navigate to Offers page
	 * @return offer and coupon page
	 * @throws Exception - Exception
	 */
	public OffersAndCouponsPage navigateToOffers()throws Exception{
		BrowserActions.clickOnElementX(lnkOffers, driver, "Offers Link ");
		Utils.waitForPageLoad(driver);
		return new OffersAndCouponsPage(driver).get();
	}

	/**
	 * To navigate to Offers page
 	 * @return MyAccountPage object
	 * @throws Exception - Exception
	 */
	public MyAccountPage navigateToMyAccount()throws Exception{
		//Have to click on Overview after clicking on Account to navigate to My Account Overview page.
		BrowserActions.clickOnElementX(lnkMyAccount, driver, "My Account Link ");
		BrowserActions.clickOnElementX(lnkAccOverview, driver, "Overview ");
		Utils.waitForPageLoad(driver);
		return new MyAccountPage(driver).get();
	}

	/**
	 * To navigate to L2 Categories
	 * @param categoryName - 
 	 * @return HamburgerMenu object
	 * @throws Exception - Exception
	 */
	public HamburgerMenu navigateToSubCategory(String categoryName)throws Exception{
		for (WebElement ele : lnkMenuItems) {
			WebElement element = ele.findElement(By.cssSelector("a"));
			if(element.getText().trim().equalsIgnoreCase(categoryName))
			{
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);
				break;
			}
		}
		Utils.waitForPageLoad(driver);
		return new HamburgerMenu(driver).get();
	}
	
	/**
	 * To check the category name in the hamburger menu is displaying
	 * @param categoryName - category name to navigate
 	 * @return true if displayed, else false
	 * @throws Exception - Exception
	 */
	public boolean checkNavigatedCategoryName(String categoryName)throws Exception{
		return BrowserActions.getTextFromAttribute(driver, lblActiveMenuHeading, "title", "Active menu").trim().equalsIgnoreCase(categoryName);
	}
	
	/**
	 * To navigate to L2 Categories
 	 * @return HamburgerMenu object
	 * @throws Exception - Exception
	 */
	public HamburgerMenu clickOnBackArrowInActiveLevel2Category()throws Exception{
		BrowserActions.javascriptClick(lblActiveMenuToggleBack, driver, "Back Arrow in Level 2");
		Utils.waitForPageLoad(driver);
		return new HamburgerMenu(driver).get();
	}
	
	
	
	/**
	 * To navigate to L2 Categories
	 * @param index - 
 	 * @return HamburgerMenu object
	 * @throws Exception - Exception
	 */
	public HamburgerMenu navigateToSubCategory(int index)throws Exception{
		WebElement element = lstCategoryArrow.get(index);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Utils.waitForPageLoad(driver);
		return new HamburgerMenu(driver).get();
	}

	/**
	 * To navigate back
 	 * @return HamburgerMenu object
	 * @throws Exception - Exception
	 */
	public HamburgerMenu clickOnBackArrow()throws Exception{
		try{
		BrowserActions.clickOnElementX(lblMyAccountHeader, driver, "Back arrow ");
		Utils.waitForPageLoad(driver);
		}catch(Exception e){
			if(e.toString().contains("is not clickable at point"))
				Log.fail("MyAccount Header Not displayed.");
		}
		return new HamburgerMenu(driver).get();
	}
	
	/**
	 * To click on the SignIn button in Hamburger Menu
	 * @return SignIn Page
	 * @throws Exception - Exception
	 */
	public SignIn clickOnSignIn()throws Exception{
		BrowserActions.clickOnElementX(lnkSignIn, driver, "Sign In Link ");
		Utils.waitForPageLoad(driver);
		return new SignIn(driver).get();
	}
	
	/**
	 * To click and hold on 'My Account'
	 * @throws Exception - Exception
	 */
	
	public void ClickAndHoldOnMyAccount()throws Exception{
		BrowserActions.clickAndHoldOnElement(lnkMyAccount, driver, "My Account label");
	}
	
	/**
	 * To click on 'my account'
	 * @throws Exception - Exception
	 */
	
	public void ClickOnMyAccount()throws Exception{
		BrowserActions.clickOnElementX(lnkMyAccount, driver, "My Account");
	}
	
	/**
	 * To verify backarrow exists
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyBackArrow()throws Exception{
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		String imageURL = (String) executor.executeScript("return window.getComputedStyle(document.querySelector('.my-account.heading'), ':before').getPropertyValue('background-image')");
		Log.event("--->>> Image URL :: " + imageURL);
		if(imageURL.contains(".svg"))
			return true;
		else
			return false;
	}
	
	/**
	 * To click on sign out
	 * @throws Exception - Exception
	 */
	
	
	public void clickOnSignOut()throws Exception{
		BrowserActions.clickOnElementX(lnkMyAccount, driver, "User Profile");
		BrowserActions.clickOnElementX(lnkSignOut, driver, "Sign Out link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on profile element
	 * @return ProfilePage
	 * @throws Exception - Exception
	 */
	public ProfilePage clickOnProfile()throws Exception{
		BrowserActions.clickOnElementX(lnkProfile, driver, "Profile Link");
		Utils.waitForPageLoad(driver);
		return new ProfilePage(driver).get();
	}
	
	/**
	 * To click on wish list element
	 * @return WishListPage
	 * @throws Exception - Exception
	 */
	public WishListPage clickOnWishList()throws Exception{
		BrowserActions.clickOnElementX(lnkWishList, driver, "WishListPage Link");
		Utils.waitForPageLoad(driver);
		return new WishListPage(driver).get();
	}
	
	/**
	 * To click on order history element
	 * @return TrackOrderPage
	 * @throws Exception - Exception
	 */
	public TrackOrderPage clickOnOrderHistory()throws Exception{
		BrowserActions.clickOnElementX(lnkOrderHistory, driver, "OrderPage Link");
		Utils.waitForPageLoad(driver);
		return new TrackOrderPage(driver).get();
	}
	
	/**
	 * To click on reward points element
	 * @return RewardPointsPage
	 * @throws Exception - Exception
	 */
	public RewardPointsPage clickOnRewards()throws Exception{
		BrowserActions.clickOnElementX(lnkRewardPoints, driver, "RewardPointsPage Link");
		Utils.waitForPageLoad(driver);
		return new RewardPointsPage(driver).get();
	}
	
	/**
	 * To click on Active category header
	 * @throws Exception - Exception
	 */
	public void clickOnActiveCat2Header()throws Exception{
		BrowserActions.clickOnElementX(activeCat2Header, driver, "Header of current Categegory 2 Nav");
	}
	
	/**
	 * To click on nth sub category element based on the brand
	 * @param int - index 
	 * @return PlpPage - PLP page object
	 * @throws Exception - Exception
	 */
	public PlpPage clickOnNthSubCategory(int index)throws Exception{
		List<WebElement> cat2 = new ArrayList<WebElement>();
		cat2 = driver.findElements(By.cssSelector(".slider-menu li.active .level-2 .nav-column .nonBoldCat a"));
		BrowserActions.clickOnElementX(cat2.get(index), driver, "Nth Sub Category");
		Utils.waitForPageLoad(driver);
		return new PlpPage(driver).get();
	}
	
	/**
	 * To click on active view all element
	 * @throws Exception - Exception
	 */
	public void clickOnActiveViewAll()throws Exception{
		BrowserActions.clickOnElementX(lnkViewAllActive, driver, "Active View All Link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on email us link
	 * @throws Exception - Exception
	 */
	public void clickOnEmailUs()throws Exception{
		BrowserActions.clickOnElementX(lnkEmailUs, driver, "Email Us Link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on chat us link
	 * @throws Exception - Exception
	 */
	public void clickOnChatUs()throws Exception{
		BrowserActions.scrollToViewElement(lnkEmailUs, driver);
		if(Utils.waitForElement(driver, lnkChat)){
			BrowserActions.javascriptClick(lnkChat, driver, "Email Us Link");
			Utils.waitForElement(driver, mdlLiveChat);	
		}else{
			Log.fail("Live Chat Link not found in Hamburger Menu", driver);
		}
		
	}
	
	/**
	 * To click on track order element
	 * @return TrackOrderPage
	 * @throws Exception - Exception
	 */
	public TrackOrderPage clickOnTrackOrder()throws Exception{
		BrowserActions.clickOnElementX(lnkTrackOrder, driver, "Track Order Link");
		Utils.waitForPageLoad(driver);
		return new TrackOrderPage(driver).get();
	}
	
	/**
	 * To click on return order items
	 * @return TrackOrderPage
	 * @throws Exception - Exception
	 */
	public TrackOrderPage clickOnReturnItems()throws Exception{
		BrowserActions.clickOnElementX(lnkReturnItems, driver, "Return Items Link");
		Utils.waitForPageLoad(driver);
		return new TrackOrderPage(driver).get();
	}
	
	/**
	 * To click on update info link
	 * @return ProfilePage
	 * @throws Exception - Exception
	 */
	public ProfilePage clickOnUdateInfo()throws Exception{
		BrowserActions.clickOnElementX(lnkUpdateInfo, driver, "Update Info Link");
		Utils.waitForPageLoad(driver);
		return new ProfilePage(driver).get();
	}
	
	/**
	 * To get signin URL from signin link
	 * @return string value
	 * @throws Exception - Exception
	 */
	public String getSignInURL()throws Exception{
		return lnkSignInMobile.getAttribute("href").replace("https", "http");
	}
	
	/**
	 * To navigate customer service hamburger button
	 * @return CustomerService
	 * @throws Exception - Exception
	 */
	public CustomerService navigateToCustomerServiceHamburger()throws Exception{
		BrowserActions.clickOnElementX(btnCustomerService, driver, "Customer Serice Link in Hamburger");
		Utils.waitForPageLoad(driver);
		return new CustomerService(driver).get();
	}
	
}