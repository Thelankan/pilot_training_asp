package com.fbb.pages.account;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StopWatch;
import com.fbb.support.Utils;

public class AddressesPage extends LoadableComponent<AddressesPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;

	String runPltfrm = Utils.getRunPlatForm();

	private static final String MyAccountPageLinksNavigation = ".navigation-links-row";

	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader
			.getInstance("checkout");

	private static EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader
			.getInstance("demandware");

	@FindBy(css = "#addresses .address-conatiner")
	WebElement addressListSection;

	@FindBy(css = "#addresses .noaddress")
	WebElement noAddressSection;

	@FindBy(css = ".pt_account.addresses")
	WebElement readyElement;

	@FindBy(id = "edit-address-form")
	WebElement addressEditModule;

	@FindBy(css = "button[name='dwfrm_profile_address_create']")
	WebElement btnSaveAddress;

	@FindBy(css = ".form-validation-error-msg.error")
	WebElement duplicateAddressError;

	@FindBy(css = ".address-delete.delete")
	WebElement lnkDelete;

	@FindBy(css = ".address-main-heading")
	WebElement lblAddressesHeading;
	
	@FindBy(css = ".paragraph")
	WebElement lblAddressesSubHeading;

	@FindBy(css = MyAccountPageLinksNavigation + " .addresses a[style*='cursor']")
	WebElement lnkAddressesAfterNavigate;

	@FindBy(css = ".address-delete.delete")
	List<WebElement> lnkDeleteAddressSection;

	@FindBy(css = ".delete-text")
	List<WebElement> lnkDeleteAddress_desktop;

	@FindBy(css = ".delete-icon")
	List<WebElement> lnkDeleteAddress_MobileTablet;

	@FindBy(css = ".delete-controls .address-delete")
	WebElement lnkYes;

	@FindBy(css = ".breadcrumb-element.hide-mobile")
	List<WebElement> breadCrumbFull;

	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement breadCrumbFull_mobile;
	
	@FindBy(css = ".promo-banner")
	WebElement promoContent;

	@FindBy(css = ".breadcrumb-element.current-element")
	WebElement breadCrumbCurrent;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop")
	WebElement breadCrumbCurrentMobile;
	
	@FindBy(css = ".main")
	WebElement lblMyAccount;

	@FindBy(css = "div.breadcrumb a.breadcrumb-element.hide-mobile")
	List<WebElement> breadCrumbRoots;

	@FindBy(css = "li>div.address-list-sec")
	List<WebElement> lstAddresses;

	@FindBy(css = ".display-inline-block.width-full .paragraph")
	WebElement addressSubText;

	@FindBy(css = ".address-list-sec.border-box.display-inline-block.full-width:not([class*='default']")
	List<WebElement> addressesSaved;
	
	@FindBy(css = ".address-list-sec.border-box.display-inline-block.full-width")
	List<WebElement> lstSavedAddresses;

	@FindBy(css = ".address-create")
	WebElement btnAddNewAddress;

	@FindBy(css = ".address-create")
	WebElement btnAddNewAddressMobile;

	@FindBy(css = ".cancel-address")
	WebElement btnAddAddressCancel;

	@FindBy(css = "label[for='dwfrm_profile_address_addressid'][class='input-focus']")
	WebElement txtNickNamePlaceHolderUpward;

	@FindBy(css = "label[for='dwfrm_profile_address_firstname'][class='input-focus']")
	WebElement txtFirstNamePlaceHolderUpward;

	@FindBy(css = "label[for='dwfrm_profile_address_lastname'][class='input-focus']")
	WebElement txtLastNamePlaceHolderUpward;

	@FindBy(css = ".address-list-sec.border-box.display-inline-block.full-width.default .mini-address-title")
	WebElement txtDefaultNickName;
	
	@FindBy(css = ".default .mini-address-name")
	WebElement txtDefaultAddressName;

	@FindBy(css = "label[for='dwfrm_profile_address_phone'][class='input-focus']")
	WebElement txtPhonePlaceHolderUpward;

	@FindBy(css = "label[for='dwfrm_profile_address_address1'][class='input-focus']")
	WebElement txtAddress1PlaceHolderUpward;

	@FindBy(css = "label[for='dwfrm_profile_address_address2'][class='input-focus']")
	WebElement txtAddress2PlaceHolderUpward;

	@FindBy(css = "label[for='dwfrm_profile_address_postal'][class='input-focus']")
	WebElement txtPostalPlaceHolderUpward;

	@FindBy(css = "label[for='dwfrm_profile_address_city'][class='input-focus']")
	WebElement txtCityPlaceHolderUpward;

	@FindBy(css = "label[for='dwfrm_profile_address_states_state'][class='input-focus']")
	WebElement txtStatePlaceHolderUpward;

	@FindBy(css = ".address-list-sec.border-box.display-inline-block.full-width.default")
	WebElement addressSaved;

	@FindBy(css = ".address-list-sec.border-box.display-inline-block.full-width .position-relative .cancel")
	List<WebElement> btnAddDeleteCancel;

	@FindBy(css = ".address-list-sec.border-box.display-inline-block.full-width.default .position-relative")
	WebElement addressDefaultDeleteModule;

	@FindBy(css = ".column-2.address-tile .position-relative")
	WebElement address2DeleteModule;

	@FindBy(css = ".column-2.address-tile")
	WebElement address2Section;

	@FindBy(css = ".default .mini-address-title")
	WebElement addressDefaultNickName;

	@FindBy(css = ".address-list-sec.border-box.display-inline-block.full-width .address-make-default")
	List<WebElement> btnMakeDefaultAddr;

	@FindBy(css = ".address-list-sec.border-box.display-inline-block.full-width .address-make-default")
	WebElement btnMakeDefaultAddrSingle;

	@FindBy(css = ".address-list-sec.border-box.display-inline-block.full-width .mini-address-title")
	List<WebElement> txtAddressNickName;

	@FindBy(css = "#dwfrm_profile_address_addressid")
	WebElement inputAddressRequired;

	@FindBy(css = ".form-row.firstname.required input")
	WebElement inputAddressFirstName;

	@FindBy(css = ".form-row.lastname.required input")
	WebElement inputAddressLastName;

	@FindBy(css = ".form-row.phone.required input")
	WebElement inputAddressPhone;

	@FindBy(css = "label[for='dwfrm_profile_address_states_state']")
	WebElement selectStatePlaceHolder;

	@FindBy(css = ".form-row.address1.required input")
	WebElement inputAddressLine1;

	@FindBy(css = ".form-row.address2 input")
	WebElement inputAddressLine2;

	@FindBy(css = ".form-row.postal.required input")
	WebElement inputAddressPostal;

	@FindBy(css = ".form-row.city.required input")
	WebElement inputAddressCity;

	@FindBy(css = ".makeDefault .input-checkbox")
	WebElement checkBoxDefaultAddress;

	@FindBy(css = ".state .selected-option.selected")
	WebElement selectState;

	@FindBy(css = ".country .selected-option.selected")
	WebElement selectCountry;
	
	@FindBy(css = "#dwfrm_profile_address_states_state")
	WebElement drpState;
	
	@FindBy(css = "#dwfrm_profile_address_country")
	WebElement drpCountry;

	@FindBy(css = ".form-row.country.required .selection-list li")
	List<WebElement> selectCountryList;

	@FindBy(css = ".form-row.state.required .selection-list li")
	List<WebElement> selectStateList;

	@FindBy(css = ".apply-button")	
	WebElement buttonSaveAddress;

	@FindBy(css = ".form-validation-error-msg.error")
	WebElement errorMsgAddressValidation;

	@FindBy(css = "label[for='dwfrm_profile_address_addressid'] .label-text")
	WebElement placeHolderNickName;

	@FindBy(css = "label[for='dwfrm_profile_address_firstname'] .label-text")
	WebElement placeHolderFirstName;

	@FindBy(css = "label[for='dwfrm_profile_address_lastname'] .label-text")
	WebElement placeHolderLastName;

	@FindBy(css = "label[for='dwfrm_profile_address_phone'] .label-text")
	WebElement placeHolderPhone;

	@FindBy(css = "label[for='dwfrm_profile_address_country'] .label-text")
	WebElement placeHolderCountry;

	@FindBy(css = "label[for='dwfrm_profile_address_address1'] .label-text")
	WebElement placeHolderAddressLine1;

	@FindBy(css = "label[for='dwfrm_profile_address_address2'] .label-text")
	WebElement placeHolderAddressLine2;

	@FindBy(css = "label[for='dwfrm_profile_address_postal'] .label-text")
	WebElement placeHolderPostal;

	@FindBy(css = "label[for='dwfrm_profile_address_states_state'] .label-text")
	WebElement placeHolderState;

	@FindBy(css = "label[for='dwfrm_profile_address_city'] .label-text")
	WebElement placeHolderCity;

	@FindBy(css = ".column-1.address-tile .address-list-sec.border-box.display-inline-block.full-width")
	List<WebElement> addressTileCol1;

	@FindBy(css = ".column-2.address-tile .address-list-sec.border-box.display-inline-block.full-width")
	List<WebElement> addressTileCol2;

	@FindBy(css = ".address-list-sec.border-box.display-inline-block.full-width.default")
	List<WebElement> addressDefault;

	@FindBy(css = ".default-indicator")
	WebElement addressDefaultIndicator;

	@FindBy(css = ".address-edit")
	List<WebElement> btnAddressEdit;

	@FindBy(css = ".address-edit")
	WebElement btnAddressEditSingle;

	@FindBy(css = ".mini-address-location")
	WebElement addressSavedList;
	
	@FindBy(css = ".mini-address-title")
	List<WebElement> addressNicknameList;
	
	@FindBy(css = ".address-form-block")
	WebElement divAddressForm;

	@FindBy(id = "dwfrm_profile_address_firstname-error")
    WebElement errorFirstName;

	@FindBy(id = "dwfrm_profile_address_lastname-error")
    WebElement errorLastName;

	@FindBy(id = "dwfrm_profile_address_address1-error")
    WebElement errorAddress1;

    @FindBy(id = "dwfrm_profile_address_postal-error")
    WebElement errorPostalCodeInvalid;

    @FindBy(id = "dwfrm_profile_address_phone-error")
    WebElement errorMsgPhoneNo;

    @FindBy(id = "dwfrm_profile_address_city-error")
    WebElement errorCity;

    @FindBy(id = "dwfrm_profile_address_makeDefault")
    WebElement chkMakeDefault;

	/**
	 * constructor of the class
	 * @param driver - 
	 */
	public AddressesPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement) || Utils.waitForElement(driver, btnSaveAddress))) {
			Log.fail("Address page is not loaded", driver);
		}

		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}

	/**
	 * To verify the delete address
	 * @param nickName - 
	 * @throws Exception - Exception
	 */
	/**
	 * To Delete address matches nickname
	 * @param nickName - 
	 * @throws Exception - Exception
	 */
	public void deleteAddress(String nickName)throws Exception{
		for (WebElement address : lstAddresses) {
			if(address.findElement(By.cssSelector(" .address-info .mini-address-title")).getText().trim().equalsIgnoreCase(nickName))
			{
				BrowserActions.clickOnElementX(address.findElement(By.cssSelector(" .address-delete.delete")), driver, "Delete Link");
				BrowserActions.clickOnElementX(address.findElement(By.cssSelector(" .delete-controls .address-delete")), driver, "Yes link");
				Utils.waitForPageLoad(driver);
				break;
			}
		}

	}
	
	/**
	 * To edit address matches nickname
	 * @param nickName - 
	 * @throws Exception - Exception
	 */
	public void clickEditAddress(String nickName)throws Exception{
		for (WebElement address : lstAddresses) {
			if(address.findElement(By.cssSelector(" .address-info .mini-address-title")).getText().trim().equalsIgnoreCase(nickName))
			{
				BrowserActions.clickOnElementX(address.findElement(By.cssSelector(".address-edit")) , driver, "Clicked edit button for Address");
				Utils.waitForPageLoad(driver);
				break;
			}
		}
	}
	
	/**
	 * To verify default address nickname
	 * @return String - nickname value 
	 * @throws Exception - Exception
	 */
	public String getDefaultAddressNickName()throws Exception{
		for (WebElement address : lstAddresses) {
			if(address.getAttribute("class").contains("default"))
			{
				return address.findElement(By.cssSelector(" .address-info .mini-address-title")).getText().trim();
			}
		}
		return null;
	}
	
	/**
	 * To get number of non-default addresses
	 * @return Size as Integer
	 * @throws Exception - Exception
	 */
	public int getNoOfAddresses()throws Exception{
		return addressesSaved.size();
	}
	
	/**
	 * To get number of saved addresses
	 * @return Size as Integer
	 * @throws Exception - Exception
	 */
	public int getSavedAddressesCount()throws Exception{
		return lstSavedAddresses.size();
	}

	/**
	 * To verify click delete address button
	 * @param index - 
	 * @throws Exception - Exception
	 */
	public void clickDeleteAddress(int index)throws Exception {
		WebElement delete;
		if(Utils.isDesktop()) {
			Utils.waitForPageLoad(driver);
			delete = lnkDeleteAddressSection.get(index-1).findElement(By.cssSelector(".delete-text"));
			Utils.waitForPageLoad(driver);
			BrowserActions.scrollToViewElement(delete, driver);
			BrowserActions.javascriptClick(delete, driver, "Address delete "+(index-1));
		} else {
			Utils.waitForPageLoad(driver);
			delete = lnkDeleteAddressSection.get(index-1).findElement(By.cssSelector(".delete-icon"));
			Utils.waitForPageLoad(driver);
			BrowserActions.scrollToViewElement(delete, driver);
			BrowserActions.javascriptClick(delete, driver, "Address delete "+(index-1));
		}
	}
	/**
	 * To verify click delete cancel address button.
	 * @param index - 
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean clickDeleteCancelAddress(int index)throws Exception{
		if (btnAddDeleteCancel.size() >= index) {
			Utils.waitForPageLoad(driver);
			WebElement elem = btnAddDeleteCancel.get(index-1);
			Utils.waitForPageLoad(driver);
			if(Utils.waitForElement(driver, elem)) {
				Utils.waitForPageLoad(driver);
				BrowserActions.clickOnElementX(elem, driver, "Delete address");
				Utils.waitForPageLoad(driver);
				return true;
			}
		}
		Utils.waitForPageLoad(driver);
		return false;
	}
	/**
	 * To verify click make default address button
	 * @param index - 
	 * @throws Exception - Exception
	 */
	public void clickMakeDefaultAddr(int index)throws Exception{
		if (btnMakeDefaultAddr.size() >= (index-1)) {
			WebElement elem = btnMakeDefaultAddr.get(index-2);
			BrowserActions.clickOnElementX(elem, driver, "Make default address");
		}
		Utils.waitForPageLoad(driver);
	}
	/**
	 * To verify and get nick name in address
	 * @param index - 
	 * @return Nickname as String
	 * @throws Exception - Exception
	 */
	public String getNickNameOfAnAddress(int index)throws Exception{
		if (txtAddressNickName.size() >= index) {
			WebElement elem = txtAddressNickName.get(index-1);
			return BrowserActions.getText(driver, elem, "Nick Name");
		}
		return null;
	}
	/**
	 * To verify and get nickname from default address
	 * @return  Nickname as String
	 * @throws Exception - Exception
	 */
	public String getNickNameOfDefaultAddress()throws Exception{
		BrowserActions.scrollInToView(txtDefaultNickName, driver);
		return BrowserActions.getText(driver, txtDefaultNickName, "Nick Name");
	}

	/**
	 * To verify and get color of address section
	 * @return  background color as String
	 * @throws Exception - Exception
	 */
	public String getOnHoverColorOfAddressSection(int AddressIndex)throws Exception{
		WebElement savedAddressNonDefault = addressesSaved.get(AddressIndex-1);
		BrowserActions.mouseHover(driver, savedAddressNonDefault, "Address 2 section");
		String backGroundColor = savedAddressNonDefault.getCssValue("background-color");
		return backGroundColor;
	}

	/**
	 * To delete all the addresses
	 * @throws Exception - Exception
	 */
	public void deleteAllTheAddresses()throws Exception {
		int size = lnkDeleteAddressSection.size();
		WebElement delete;
		WebElement ok;
		if (size > 0) {
			if(Utils.isDesktop()) {
				for (int i=(size-1); i>=0; i--) {
					delete = lnkDeleteAddressSection.get(i).findElement(By.cssSelector(".delete-text"));
					BrowserActions.clickOnElementX(delete, driver, "Address delete "+i);

					ok = driver.findElement(By.cssSelector(".delete-content .address-delete"));
					Utils.waitForElement(driver, ok);
					BrowserActions.clickOnElementX(ok, driver, "Confirm delete "+i);
					Utils.waitForPageLoad(driver);
				}
			} else {
				for (int i=(size-1); i>=0; i--) {
					delete = lnkDeleteAddressSection.get(i).findElement(By.cssSelector(".delete-icon"));
					BrowserActions.clickOnElementX(delete, driver, "Address delete "+i);

					ok = driver.findElement(By.cssSelector(".delete-content .address-delete"));
					Utils.waitForElement(driver, ok);
					BrowserActions.clickOnElementX(ok, driver, "Confirm delete "+i);
					Utils.waitForPageLoad(driver);
				}
			}
		}
	}
	/**
	 * To click add new address
	 * @throws Exception - Exception
	 */
	public void clickOnAddNewAddress()throws Exception{
		if(Utils.waitForElement(driver, btnAddNewAddress)) {
			BrowserActions.scrollToViewElement(btnAddNewAddress, driver);
			BrowserActions.javascriptClick(btnAddNewAddress, driver, "Add New address");
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To verify breadcrum in the page
	 * @return Boolean - Status
	 * @throws Exception - Exception
	 */
	public boolean verifyBreadCrumbInThisPage()throws Exception{
		boolean status = true;
		if(runPltfrm.contains("mobile")) {
			String text = "my account";
			String breadCrumbText = BrowserActions.getText(driver, breadCrumbFull_mobile, "Bread crumb");
			if (!(breadCrumbText.toLowerCase().contains(text))) {
				status = false;
			}
		} else {
			String[] text = {"home","my account","addresses"};
			for(int i = 0; i < breadCrumbFull.size(); i++) {
				WebElement breadCrumb = breadCrumbFull.get(i);
				String breadCrumbText = BrowserActions.getText(driver, breadCrumb, "Bread crumb");
				if (!(breadCrumbText.toLowerCase().contains(text[i]))) {
					status = false;
				}
			}
		}
		return status;
	}

	/**
	 * To fill the address details in address page
	 * @param String - addressFromProperty 
	 * @param boolean - isDefault  
	 * @return LinkedHaspMap - Address as hashmap
	 * @throws Exception - Exception
	 */
	public LinkedHashMap<String, String> fillingAddressDetails(String addressFromProperty, boolean isDefault)
			throws Exception {

		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> addressDetails = new LinkedHashMap<String, String>();

		if(!Utils.waitForElement(driver, inputAddressRequired)){
			clickOnAddNewAddress();
		}
		String address = checkoutProperty.getProperty(addressFromProperty);
		String phoneNo = address.split("\\|")[0];
		String address1 = address.split("\\|")[1];
		String address2 = address.split("\\|")[2];
		String postal = address.split("\\|")[3];
		String city = address.split("\\|")[4];
		String state = address.split("\\|")[5];
		String country = address.split("\\|")[6];
		String addressTitle = "Nick"+ RandomStringUtils.randomAlphabetic(5).toLowerCase();
		String FirstName = address.split("\\|")[7];
		String LastName = address.split("\\|")[8];

		BrowserActions.typeOnTextField(inputAddressRequired, addressTitle, driver, "Address title");
		BrowserActions.typeOnTextField(inputAddressFirstName, FirstName, driver, "FirstName");
		BrowserActions.typeOnTextField(inputAddressLastName, LastName, driver, "LastName");
		BrowserActions.typeOnTextField(inputAddressPhone, phoneNo, driver, "phoneNo");
		BrowserActions.typeOnTextField(inputAddressLine1, address1, driver, "Address Line1");
		BrowserActions.typeOnTextField(inputAddressLine2, address2, driver, "Address Line2");
		BrowserActions.typeOnTextField(inputAddressPostal, postal, driver, "postal");
		BrowserActions.typeOnTextField(inputAddressCity, city, driver, "city");

		if(isDefault) {
			if(Utils.waitForElement(driver, checkBoxDefaultAddress)) {
				if(!(checkBoxDefaultAddress.isSelected())) {
					checkBoxDefaultAddress.click();
				}
			}
		} else {
			if(Utils.waitForElement(driver, checkBoxDefaultAddress)) {
				if(checkBoxDefaultAddress.isSelected()) {
					checkBoxDefaultAddress.click();
				}
			}
		}

		if(selectState.getText().equals(state)) {
			Log.event(state + " is selected");
		} else {
			BrowserActions.clickOnElementX(selectState, driver, "State drop down");
			WebElement Element = selectState.findElement(By.xpath("//following-sibling::ul//li[contains(text(),'"+state+"')]"));
			BrowserActions.clickOnElementX(Element, driver, "State drop down");
		}

		if(selectCountry.getText().equals(country)) {
			Log.event(country + " is selected");
		} else {
			BrowserActions.clickOnElementX(selectCountry, driver, "Country drop down");
			List<WebElement> lstElement = selectCountry.findElement(By.xpath("..")).findElements(By.cssSelector("ul li"));
			for(WebElement e: lstElement) {	
				if(e.getText().trim().equals(state)) {						
					BrowserActions.scrollToViewElement(e, driver);
					BrowserActions.javascriptClick(e, driver, "list elements");
					break;
				}
			}
		}

		addressDetails.put("Firstname", FirstName);
		addressDetails.put("Lastname", LastName);
		addressDetails.put("AddressTitle", addressTitle);
		addressDetails.put("phoneNo", phoneNo);
		addressDetails.put("address1", address1);
		addressDetails.put("address2", address2);
		addressDetails.put("postal", postal);
		addressDetails.put("city", city);
		addressDetails.put("country", country);
		addressDetails.put("state", state);


		StopWatch.elapsedTime(startTime);
		Utils.waitForPageLoad(driver);
		return addressDetails;
	}

	/**
	 * To enter address title in the address creation
	 * @param String - addressTitle 
	 * @throws Exception - Exception
	 */
	public void enterAddressTitle(String addressTitle)throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		BrowserActions.typeOnTextField(inputAddressRequired, addressTitle, driver, "Address title");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To enter first name in the address creation
	 * @param String - FirstName 
	 * @throws Exception - Exception
	 */
	public void enterFirstName(String FirstName)throws Exception {
		BrowserActions.typeOnTextField(inputAddressFirstName, FirstName, driver, "FirstName");
	}

	/**
	 * To enter last name in the address creation
	 * @param String - LastName 
	 * @throws Exception - Exception
	 */
	public void enterLastName(String LastName)throws Exception {
		BrowserActions.typeOnTextField(inputAddressLastName, LastName, driver, "LastName");
	}

	/**
	 * To enter phone number in the address creation
	 * @param String - phoneNo 
	 * @throws Exception - Exception
	 */
	public void enterphoneNo(String phoneNo)throws Exception {
		BrowserActions.typeOnTextField(inputAddressPhone, phoneNo, driver, "phoneNo");
		inputAddressPhone.sendKeys(Keys.TAB);
	}

	/**
	 * To enter address line 1 in the address creation
	 * @param String - address1  
	 * @throws Exception - Exception
	 */
	public void enterAddressLine1(String address1)throws Exception {
		BrowserActions.typeOnTextField(inputAddressLine1, address1, driver, "Address Line1");
	}

	/**
	 * To enter address line 2 in the address creation
	 * @param String - address2 
	 * @throws Exception - Exception
	 */
	public void enterAddressLine2(String address2)throws Exception {
		Utils.waitForPageLoad(driver);
		BrowserActions.typeOnTextField(inputAddressLine2, address2, driver, "Address Line2");
	}

	/**
	 * To enter postal code in the address creation
	 * @param String - Postal 
	 * @throws Exception - Exception
	 */
	public void enterPostal(String postal)throws Exception {
		BrowserActions.typeOnTextField(inputAddressPostal, postal, driver, "postal");
		inputAddressPostal.sendKeys(Keys.TAB);
	}

	/**
	 * To enter city code in the address creation
	 * @param String - City 
	 * @throws Exception - Exception
	 */
	public void enterCity(String city)throws Exception {
		BrowserActions.typeOnTextField(inputAddressCity, city, driver, "city");
	}
	/**
	 * To verify address allignment 
	 * @param String - Hello1
	 * @param String - Hello2
	 * @param String - Hello3
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyAddressAlignment(String Hello1, String Hello2, String Hello3)throws Exception {
		boolean status = false;
		Utils.waitForPageLoad(driver);

		BrowserActions.scrollToViewElement(driver.findElement(By.xpath("//div[contains(text(),'"+Hello1+"')]")), driver);
		WebElement addressTile2 = driver.findElement(By.xpath("//div[contains(text(),'"+Hello1+"')]"));
		
		BrowserActions.scrollToViewElement(driver.findElement(By.xpath("//div[contains(text(),'"+Hello2+"')]")), driver);
		WebElement addressTile3 = driver.findElement(By.xpath("//div[contains(text(),'"+Hello2+"')]"));
		
		BrowserActions.scrollToViewElement(driver.findElement(By.xpath("//div[contains(text(),'"+Hello3+"')]")), driver);
		WebElement addressTile1 = driver.findElement(By.xpath("//div[contains(text(),'"+Hello3+"')]"));
		
		Utils.waitForPageLoad(driver);

		if (BrowserActions.verifyHorizontalAllignmentOfElements(driver,	addressTile2, addressTile1) || 
				BrowserActions.verifyHorizontalAllignmentOfElements(driver,	addressTile1, addressTile2)) {
			status = true;
		}
		if(BrowserActions.verifyVerticalAllignmentOfElements(driver, addressTile3, addressTile1) ||
				BrowserActions.verifyVerticalAllignmentOfElements(driver, addressTile1, addressTile3)) {
			status = true;
		}
		return status;
	}

	/**
	 * To verify default address indicator position
	 * @return Status Boolean 
	 * @throws Exception - Exception
	 */
	public boolean verifyDefaultAddressIndicatorPosition()throws Exception {
		if (BrowserActions.verifyVerticalAllignmentOfElements(driver, txtDefaultNickName, txtDefaultAddressName)
			&& Integer.parseInt(txtDefaultNickName.getCssValue("padding-left").replaceAll("[^0-9]", "")) > 0) {
			return true;
		}
		return false;
	}

	/**
	 * To verify address fields value entered
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyAddressFieldValueEntered()throws Exception{

		String NickName = BrowserActions.getTextFromAttribute(driver, inputAddressRequired, "value", "NickName");
		String FirstName = BrowserActions.getTextFromAttribute(driver, inputAddressFirstName, "value", "FirstName");
		String LastName = BrowserActions.getTextFromAttribute(driver, inputAddressLastName, "value", "LastName");
		String Phone = BrowserActions.getTextFromAttribute(driver, inputAddressPhone, "value", "Phone");
		String AddLine1 = BrowserActions.getTextFromAttribute(driver, inputAddressLine1, "value", "Address Line 1");
		String AddLine2 = BrowserActions.getTextFromAttribute(driver, inputAddressLine2, "value", "Address Line 2");
		String Postal = BrowserActions.getTextFromAttribute(driver, inputAddressPostal, "value", "Postal");
		String City = BrowserActions.getTextFromAttribute(driver, inputAddressCity, "value", "City");
		String Country = BrowserActions.getText(driver, selectCountry, "Country");
		String State = BrowserActions.getText(driver, selectState, "State");

		if (NickName.isEmpty() && FirstName.isEmpty() && LastName.isEmpty() && Phone.isEmpty() && AddLine1.isEmpty() && 
				AddLine2.isEmpty() && Postal.isEmpty() && City.isEmpty() && Country.isEmpty() && State.isEmpty()) {
			return false;
		}

		return true;
	}

	/**
	 * To verify click save address
	 * @throws Exception - Exception
	 */
	public void clickSaveAddress()throws Exception{
		BrowserActions.scrollToViewElement(btnSaveAddress, driver);
		BrowserActions.javascriptClick(btnSaveAddress, driver, "Save address");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on edit of the given address
	 * @param index of the address
	 * @throws Exception - Exception
	 */
	public void clickAddressEdit(int index)throws Exception{
		if(btnAddressEdit.size() >= index) {
			WebElement elem = btnAddressEdit.get(index-1);
			BrowserActions.clickOnElementX(elem, driver, "Edit address");
		}else{
			Log.fail("There are no address to edit.", driver);
		}
		Utils.waitForPageLoad(driver);
	}


	/**
	 * To get default address
	 * @return no of addresses
	 * @throws Exception - Exception
	 */
	public int NoOfDefaultAddress()throws Exception{
		return addressDefault.size();
	}

	/**
	 * To verify click cancel address creation
	 * @throws Exception - Exception
	 */
	public void clickCancelAddressCreation()throws Exception{
		Utils.waitForPageLoad(driver);
		BrowserActions.scrollToViewElement(btnAddAddressCancel, driver);
		BrowserActions.clickOnElementX(btnAddAddressCancel, driver, "Save address");
	}
	/**
	 * To get the error messages
	 * @return Error message as String
	 * @throws Exception - Exception
	 */
	public String getErrorMessage()throws Exception{
		//BrowserActions.nap(10);
		return BrowserActions.getText(driver, errorMsgAddressValidation, "Error message");
	}

	/**
	 * To verify country drop down values in address
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyCountryDropDownValues()throws Exception{
		BrowserActions.clickOnElementX(selectCountry, driver, "Country drop down");
		List<WebElement> lstElement = selectCountry.findElement(By.xpath("..")).findElements(By.cssSelector("ul li"));
		if(lstElement.size() == 1) {
			String country = BrowserActions.getText(driver, lstElement.get(0), "Countries");
			if(country.contains("United States")) {
				return true;
			}
		}
		return true;
	}
	/**
	 * To verify header coming form the text property
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyHeaderTextIsComingFromProperty()throws Exception{
		String textToVerify = BrowserActions.getText(driver, lblAddressesHeading, "Heading");
		String propText = demandWareProperty.getProperty("Header");

		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}
		return true;
	}
	/**
	 * To verify default address text is coming from property
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyDefaultAddressTextIsComingFromProperty()throws Exception{
		String textToVerify = BrowserActions.getText(driver, addressDefaultIndicator, "DefaultIndicator");
		String propText = demandWareProperty.getProperty("DefaultAddress");

		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}
		return true;
	}
	/**
	 * To verify the add new address text coming from the demandware property.
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyAddNewAddressTextIsComingFromProperty()throws Exception{
		String textToVerify = BrowserActions.getText(driver, btnAddNewAddress, "NewAddress add");
		String propText = demandWareProperty.getProperty("AddNewAddress");

		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}
		return true;
	}
	/**
	 * To verify subhead text is coming from demandware property
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifySubheadTextIsComingFromProperty()throws Exception{
		String textToVerify = BrowserActions.getText(driver, addressSubText, "Heading");
		String propText = demandWareProperty.getProperty("Subhead");

		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}
		return true;
	}
	/**
	 * To verify place holder is coming from demandware property
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyPlaceHolderComingFromProperty()throws Exception{
		String textToVerify = BrowserActions.getText(driver, placeHolderNickName, "NickName");
		String propText = demandWareProperty.getProperty("NickName");
		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}

		textToVerify = BrowserActions.getText(driver, placeHolderFirstName, "FirstName");
		propText = demandWareProperty.getProperty("Firstname");
		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}

		textToVerify = BrowserActions.getText(driver, placeHolderLastName, "LastName");
		propText = demandWareProperty.getProperty("Lastname");
		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}

		textToVerify = BrowserActions.getText(driver, placeHolderAddressLine1, "AddressLine1");
		propText = demandWareProperty.getProperty("Address1");
		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}

		textToVerify = BrowserActions.getText(driver, placeHolderAddressLine2, "AddressLine2");
		propText = demandWareProperty.getProperty("Address2");
		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}

		textToVerify = BrowserActions.getText(driver, placeHolderPostal, "Postal");
		propText = demandWareProperty.getProperty("Postal");
		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}

		textToVerify = BrowserActions.getText(driver, placeHolderState, "State");
		propText = demandWareProperty.getProperty("State");
		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}

		textToVerify = BrowserActions.getText(driver, placeHolderCity, "City");
		propText = demandWareProperty.getProperty("City");
		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}

		return true;
	}
	/**
	 * To get save address text.
	 * @return Address as String
	 * @throws Exception - Exception
	 */
	public String getSavedAddress()throws Exception {
		return BrowserActions.getText(driver, addressSavedList, "Saved Address").trim();
	}
	
	/**
	 * To verify if default address is sequenced first.
	 * @return boolean - verification 
	 * @throws Exception - Exception
	 */
	public boolean verifyDefaultAddressLocatedFirst()throws Exception {
		for(WebElement element: btnMakeDefaultAddr) {
			if(addressDefaultNickName.getLocation().x > element.getLocation().x 
					&& addressDefaultNickName.getLocation().y > element.getLocation().y) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify if address nickname is present.
	 * @param nickNameFromPage - nickname
	 * @return boolean - verification 
	 * @throws Exception - Exception
	 */
	public boolean verifyNickNameIsPresent(String nickNameFromPage) throws Exception{
		for(WebElement nickname : addressNicknameList) {
			if (nickname.getText().trim().equals(nickNameFromPage))
				return true;
		}
		return false;
	}
	
	/**
	 * To edit nickname of address.
	 * @param newNickName - nickname
	 * @throws Exception - Exception
	 */
	public void editNickName(String newNickName) throws Exception{
		BrowserActions.typeOnTextField(inputAddressRequired, newNickName, driver, "New Nickname of Address");
		BrowserActions.clickOnElementX(buttonSaveAddress, driver, "Saved adddress");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To get Autofilled City name
	 * @return String - City
	 * @throws - Exception
	 **/
	public String getEnteredCity() throws Exception{
		return Utils.getValueOfInputField(inputAddressCity, driver);
	}
	
	/**
	 * To verify relative location of new address form
	 * @return boolean - true/false if address form is displayed at correct location
	 * @throws Exception - Exception
	 */
	public boolean verifyAddressFormLocation() throws Exception{
		if(!Utils.waitForElement(driver, divAddressForm)) {
			clickOnAddNewAddress();
		}
		WebElement addressFirst = lstSavedAddresses.get(0);
		WebElement addressSecond = lstSavedAddresses.get(1);
		if(!Utils.isMobile())
		{
			if(!BrowserActions.verifyHorizontalAllignmentOfElements(driver, divAddressForm, addressFirst)) {
				return false;
			}
			if(!BrowserActions.verifyVerticalAllignmentOfElements(driver, divAddressForm, addressSecond)) {
				return false;
			}
		}else {
			if(!BrowserActions.verifyVerticalAllignmentOfElements(driver, divAddressForm, addressFirst)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To check or uncheck make default checkbox
	 * @param isDefault - true/false to check/uncheck make default checkbox
	 * @throws Exception - Exception 
	 */
	public void checkUncheckMakeDefaultCheckbox(boolean isDefault)throws Exception{
		if(isDefault) {
			if(Utils.waitForElement(driver, checkBoxDefaultAddress)) {
				if(!(checkBoxDefaultAddress.isSelected())) {
					BrowserActions.scrollToViewElement(checkBoxDefaultAddress, driver);
					BrowserActions.selectRadioOrCheckbox(checkBoxDefaultAddress, driver, "YES");
				}
			}
		} else {
			if(Utils.waitForElement(driver, checkBoxDefaultAddress)) {
				if(checkBoxDefaultAddress.isSelected()) {
					BrowserActions.scrollToViewElement(checkBoxDefaultAddress, driver);
					BrowserActions.selectRadioOrCheckbox(checkBoxDefaultAddress, driver, "NO");
				}
			}
		}
	}
	
	/**
	 * To verify Edit and Delete on each address card 
	 * @return boolean - true/false if all cards have the buttons
	 * @throws Exception - Exception
	 */
	public boolean verifyEditDeleteSavedCards()throws Exception{
		for(WebElement addrCard: addressesSaved) {
			WebElement cardEdit = addrCard.findElement(By.cssSelector(".address-edit"));
			WebElement cardDelete = addrCard.findElement(By.cssSelector(".address-delete.delete"));
			if(!Utils.waitForElement(driver, cardEdit) || !Utils.waitForElement(driver, cardDelete)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify make default button present on all non-default address
	 * @return boolean - true/false if all non-default cards have Make Default button
	 * @throws Exception - Exception
	 */
	public boolean verifyMakeDefaultButton()throws Exception{
		for(int i=1 ; i<addressesSaved.size() ; i++) {
			WebElement cardMakeDefault = addressesSaved.get(i).findElement(By.cssSelector(".address-make-default"));
			if(!Utils.waitForElement(driver, cardMakeDefault)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify edit and delete placement on address cards 
	 * @return boolean - true/false if all edit/delete buttons are displayed correctly
	 * @throws Exception - Exception
	 */
	public boolean verifyEditDeleteRelativePlacement()throws Exception{
		for(WebElement addrCard: addressesSaved) {
			WebElement cardEdit = addrCard.findElement(By.cssSelector(".address-edit"));
			WebElement cardDelete = addrCard.findElement(By.cssSelector(".address-delete.delete"));
			if(!BrowserActions.verifyHorizontalAllignmentOfElements(driver, cardDelete, cardEdit)) {
				return false;
			}
		}
		if(!Utils.isDesktop()) {
			for(WebElement addrCard: addressesSaved) {
				WebElement cardEdit = addrCard.findElement(By.cssSelector(".address-edit"));
				WebElement cardDelete = addrCard.findElement(By.cssSelector(".address-delete.delete"));
				if(!BrowserActions.verifyVerticalAllignmentOfElements(driver, cardDelete, cardEdit)) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * To verify delete address modal is shown for intended address card
	 * @param index - index
	 * @return boolean - true/false if modal is displayed
	 * @throws Exception - Exception
	 */
	public boolean verifyDeleteModalAddressCard(int index) throws Exception{
		WebElement deleteModal = lstSavedAddresses.get(index-1).findElement(By.cssSelector(".delete-content"));
		return BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(deleteModal));
	}
	
	/**
	 * To verify background color of delete modal
	 * @param index - index of card
	 * @param bgColor - background color to verify
	 * @return true/false if background color is correct
	 * @throws Exception - Exception 
	 */
	public boolean verifyDeleteModalBackground(int index, String bgColor) throws Exception{
		WebElement deleteModal = lstSavedAddresses.get(index-1).findElement(By.cssSelector(".delete-content > span"));
		return deleteModal.getCssValue("background-color").contains(bgColor);
	}
	
	/**
	 * To verify delete modal has correct content
	 * @param index - index of card
	 * @return boolean - true/false if content are shown
	 * @throws Exception - Exception
	 */
	public boolean verifyDeleteModalContent(int index)throws Exception{
		WebElement msgConfirmation = lstSavedAddresses.get(index-1).findElement(By.cssSelector(".delete-msg"));
		WebElement btnCancel = lstSavedAddresses.get(index-1).findElement(By.cssSelector(".delete-controls .cancel"));
		WebElement btnYes = lstSavedAddresses.get(index-1).findElement(By.cssSelector(".delete-controls .address-delete"));
		return BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(msgConfirmation, btnCancel, btnYes));
	}
	
	/**
	 * To verify if modal has transparency 
	 * @param index - index of card
	 * @return boolean - true/false if transparency is available
	 * @throws Exception - Exception
	 */
	public boolean verifyDeleteModalTransparency(int index) throws Exception{
		WebElement deleteModal = lstSavedAddresses.get(index-1).findElement(By.cssSelector(".delete-content > span"));
		return Float.parseFloat(deleteModal.getCssValue("opacity").trim()) < 0.95;
	}
	
	/**
	 * To verify relative location of cancel button
	 * @param index - index of card
	 * @return boolean - true/false if cancel button is located correctly
	 * @throws Exception - Exception
	 */
	public boolean verifyCancelButtonMessageRelativePosition(int index) throws Exception{
		WebElement msgConfirmation = lstSavedAddresses.get(index-1).findElement(By.cssSelector(".delete-msg"));
		WebElement btnCancel = lstSavedAddresses.get(index-1).findElement(By.cssSelector(".delete-controls .cancel"));
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, msgConfirmation, btnCancel);
	}
	
	/**
	 * To click cancel button on delete confirmation 
	 * @param index - index of card
	 * @throws Exception - Exception
	 */
	public void clickCancelOnConfirmation(int index) throws Exception{
		WebElement btnCancel = lstSavedAddresses.get(index-1).findElement(By.cssSelector(".delete-controls .cancel"));
		BrowserActions.clickOnElementX(btnCancel, driver, "Cancel button on confirmation.");
	}
	
	/**
	 * To click yes on delete confirmations 
	 * @param index - index of card
	 * @throws Exception - Exception
	 */
	public boolean verifyCancelYesRelativePosition(int index) throws Exception{
		WebElement btnCancel = lstSavedAddresses.get(index-1).findElement(By.cssSelector(".delete-controls .cancel"));
		WebElement btnYes = lstSavedAddresses.get(index-1).findElement(By.cssSelector(".delete-controls .address-delete"));
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, btnYes, btnCancel);
	}
	
	/**
	 * To click Yes button on delete confirmation 
	 * @param index - index of card
	 * @throws Exception - Exception
	 */
	public void clickYesOnConfirmation(int index) throws Exception{
		WebElement btnYes = lstSavedAddresses.get(index-1).findElement(By.cssSelector(".delete-controls .address-delete"));
		BrowserActions.clickOnElementX(btnYes, driver, "Cancel button on confirmation.");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To verify Edit, Delete and Make Default on all address card when no default card is saved 
	 * @return boolean - true/false if all cards have the buttons in correct location
	 * @throws Exception - Exception
	 */
	public boolean verifyEditDeleteMakeDefaultAllSavedCards()throws Exception{
		for(WebElement addrCard: addressesSaved) {
			try{
				WebElement cardEdit = addrCard.findElement(By.cssSelector(".address-edit"));
				WebElement cardDelete = addrCard.findElement(By.cssSelector(".address-delete.delete"));
				WebElement cardMakeDefault = addrCard.findElement(By.cssSelector(".address-make-default"));
				WebElement cardAddress = addrCard.findElement(By.cssSelector(".mini-address-location"));
				boolean verifyDelete=false;
				if(Utils.isDesktop()) {
					verifyDelete = BrowserActions.verifyVerticalAllignmentOfElements(driver, cardDelete, cardAddress);
				}else {
					verifyDelete = BrowserActions.verifyHorizontalAllignmentOfElements(driver, cardAddress, cardDelete);
				}
				if (BrowserActions.verifyVerticalAllignmentOfElements(driver, cardEdit, cardAddress)
					|| verifyDelete
					|| BrowserActions.verifyVerticalAllignmentOfElements(driver, cardMakeDefault, cardAddress))
						return false;
			}catch(Exception e) {
				Log.event(e.getMessage());
				return false;
			}
		}
		return true;
	}

    /**
     * To verify addresses are displayed in chronological order
     * @return boolean - True/False
     * @throws Exception - Exception
     */
	public boolean verifyAddressDisplayedInChronologicalOrder()throws Exception{
	    boolean flag = true;
	    try{
	        List<WebElement> addressCol1 = driver.findElements(By.cssSelector(".address-list li.column-2 >div:not([class*='default']) .mini-address-title"));
	        List<WebElement> addressCol2 = driver.findElements(By.cssSelector(".address-list li.column-1 >div:not([class*='default']) .mini-address-title"));
	        List<WebElement> addressList = Utils.listAlternateMerge(addressCol1, addressCol2);
	        List<String> addressListString = BrowserActions.getText(addressList, "Address Nick name list", driver);
            if(addressListString.size() >= 2)
            for(int i = 0; i <= addressListString.size()-2; i++ ){
                if(addressListString.get(i).compareTo(addressListString.get(i+1)) > 0)
                    flag = false;
            }
        }catch(Exception e){
            Log.exception(e);
        }

	    return flag;
    }

    /**
     * To click on update button in Edit address module
     * @throws Exception - Exception
     */
    public void clickOnUpdateButton()throws Exception{
        BrowserActions.scrollToViewElement(buttonSaveAddress, driver);
	    BrowserActions.clickOnElementX(buttonSaveAddress, driver, "Update Button");
	    Utils.waitForPageLoad(driver);
		Utils.waitForPageLoad(driver);
		Utils.waitForPageLoad(driver);
    }

    /**
     * To get value from text fields in edit address module
     * @param fieldName - text field name
     * @return String - value from text field
     * @throws Exception - Exception
     */
    public String getValueFrom(String fieldName)throws Exception{
	    String valueToReturn = new String();
	    try {
            switch (fieldName.toLowerCase()) {
                case "nickname":
                    valueToReturn = inputAddressRequired.getAttribute("value");
                    break;

                default:
                    valueToReturn = null;
            }
        }catch (Exception e){
	        Log.exception(e);
        }
        return valueToReturn;
    }

    /**
     * To verify given nickname address is default
     * @param nickName - nickname of address to verify
     * @return boolean - true/false
     * @throws Exception - Exception
     */
    public boolean verifyAddressIsDefault(String nickName)throws Exception{
	    boolean flag = true;
	    try{
	        WebElement addressSection = By.xpath("//div[@class='mini-address-title'][normalize-space(text())='" + nickName + "']//ancestor::div[contains(@class,'address-list-sec')]").findElement(driver);
            if(addressSection.getAttribute("class").contains("default"))
                flag = false;
	        try{
                addressSection.findElement(By.cssSelector(".address-make-default"));
            }catch(NoSuchElementException e)
            {flag = false;}
        }catch(Exception e){
	        Log.exception(e);
        }

        return flag;
    }

    /**
     * To verify default address indicator is present for given nickname address
     * @param nickName - nick name of the address
     * @return boolean - true/false
     * @throws Exception - Exception
     */
    public boolean verifyDefaultAddressIndicatorForNickName(String nickName)throws Exception{
	    boolean flag = false;
	    try {
            WebElement indicator = driver.findElement(By.xpath("//div[@class='mini-address-title'][normalize-space(text())='"+nickName+"']"));
            if(indicator.getCssValue("background").contains(".svg"))
                flag = true;
        }catch(Exception e){
            Log.exception(e);
        }
        return flag;
    }

    /**
     * To get address position by nickname
     * @param nickName - nick name in address
     * @return int - position value in integer
     * @throws Exception - Exception
     */
    public int getAddressPositionForNickName(String nickName)throws Exception{
	    int position = 0;
	    try{
            for(int i=0; i<txtAddressNickName.size(); i++)
                if(txtAddressNickName.get(i).getAttribute("innerHTML").trim().equals(nickName))
                    position = i+1;
        }catch(Exception e){
            Log.exception(e);
        }
        if(position == 0){
	        Log.failsoft("Given nick name match not found.", driver);
        }
        Log.event("Position :: " + position);
        return position;
    }

}

