package com.fbb.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.PaypalConfirmationPage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;


public class PaypalPage extends LoadableComponent<PaypalPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Headers headers;
	public Footers footers;

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(css = "div[id='main']")
	WebElement paypalContent;
	
	@FindBy(css = "#injectedUnifiedLogin")
	WebElement iframeParent;
	
	 @FindBy(css = ".textInput input#email")
	 WebElement emailId;
	 
	 @FindBy(css = ".textInput input#password")
	 WebElement passwordId;
	 
	 @FindBy(name = "injectedUl")
	 WebElement iframe;

	 @FindBy(id = "btnLogin")
	 WebElement paypalSubmitButton;
	 
	 
	//================================================================================
	//			WebElements Declaration End
	//================================================================================

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public PaypalPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, paypalContent, 60);
		if (isPageLoaded && !(Utils.waitForElement(driver, paypalContent))) {
			Log.fail("Paypal Page didn't open", driver);
		}

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}
	
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}
	
	/**
	 * To type credentials and Open Paypal Dashboard Page
 	 * @param username -
	 * @param password -
	 * @return PaypalConfirmationPage if the credentials are valid
	 * @throws Exception - Exception
	 */
	public PaypalConfirmationPage enterPayapalCredentials(String username,String password)throws Exception{
		
		//driver.switchTo().frame(iframe);
		//System.out.println("Switched to iframe");
		BrowserActions.typeOnTextField(emailId, username, driver, "Entering the username");
		if(!Utils.waitForElement(driver, passwordId))
		{
			BrowserActions.clickOnElementX(paypalSubmitButton, driver, "Login Button in Paypal Modal");
			Utils.waitForPageLoad(driver);
		}
		BrowserActions.typeOnTextField(passwordId, password, driver, "Entering the Password");
		BrowserActions.clickOnElementX(paypalSubmitButton, driver, "Login Button in Paypal Modal");
		Utils.waitForPageLoad(driver);
		return new PaypalConfirmationPage(driver).get();
		
	}
	
}
