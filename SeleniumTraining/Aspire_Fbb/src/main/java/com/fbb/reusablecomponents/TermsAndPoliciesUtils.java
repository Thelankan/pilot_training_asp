package com.fbb.reusablecomponents;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.fbb.support.BrowserActions;
import com.fbb.support.Utils;

/**
 * Re-Usable methods of Terms and Policies Functionality for Retail Sites
 * 
 * Also created re-usable methods for DemandWare business flows
 * 
 * Some of the DW methods can be optimized and use for other platform based retail site also
 * 
 * 
 */
public class TermsAndPoliciesUtils {

	/**
	 * Verify Policy Statements listed in the Policies Page
	 * 
	 * @param policyLink
	 *            : WebElement to navigate to the Policies page
	 * 
	 * @param driver
	 *            : WebDriver instance
	 * @throws Exception -
	 */
	final public static void verifyPolicyStatement(WebElement policyLink, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(policyLink, driver, "Policies Link");
		Utils.waitForPageLoad(driver);

	}// verifyPolicyStatement

	/**
	 * Verify Term Statements listed in the Terms Page
	 * 
	 * @param termsLink
	 *            : WebElement to navigate to the Terms page
	 * 
	 * @param driver
	 *            : WebDriver instance
	 * @throws Exception -
	 */
	final public static void verifyTerms(WebElement termsLink, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(termsLink, driver, "Terms Link");
		Utils.waitForPageLoad(driver);

	}// verifyTerms

}// TermsAndPolicies_Util
