package com.fbb.reusablecomponents;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

/**
 * Re-Usable methods of Payment Functionality for Retail Sites
 * 
 * Also created re-usable methods for DemandWare business flows
 * 
 * Some of the DW methods can be optimized and use for other platform based
 * retail site also
 * 
 * 
 */
public class PaymentUtils {

	/**
	 * Click Paypal Button/Radio - DW and Non-DW Sites
	 * 
	 * @param btnPaypal
	 *            : Paypal Button WebElement
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception - 
	 */
	final public static void selectPaypalPayment(WebElement btnPaypal, WebDriver driver) throws Exception {


		BrowserActions.clickOnElementX(btnPaypal, driver, "Paypal");

		Utils.waitForPageLoad(driver);

	}// selectPaypalPayment

	/**
	 * Click Credit Card Button - DW and Non-DW Sites
	 * 
	 * This method can be used to click specific type of CC type like Visa,
	 * MasterCard, AMEX... This option for Non-DW site
	 * 
	 * @param btnCreditCard
	 *            : Credit Card Button WebElement
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception - 
	 */
	final public static void clickCreditCardPayment(WebElement btnCreditCard, WebDriver driver) throws Exception {


		BrowserActions.clickOnElementX(btnCreditCard, driver, "Credit Card");
		Log.trace("Clicked Credit Card button.");

		Utils.waitForPageLoad(driver);

	}// clickCreditCardPayment

	/**
	 * Select Credit Card option from ComboBox - DW and Non-DW Sites [Can be
	 * used for Expiry Month and year selection]
	 * 
	 * This method can be used to click specific type of CC type like Visa,
	 * MasterCard, AMEX... This option for Non-DW site
	 * 
	 * @param btnCreditCard
	 *            : Credit Card Button WebElement
	 * @param optToSelect
	 *            : Option to select from combobox
	 * @param driver
	 *            : WebDriver Instance
	 */
	final public static void selectCreditCardPaymentAndExpiryYrMonth(WebElement btnCreditCard, String optToSelect,
			WebDriver driver) {


		BrowserActions.selectFromComboBox(btnCreditCard, optToSelect, driver, "Credit Card - " + optToSelect);

		Utils.waitForPageLoad(driver);

	}// selectCreditCardPayment

	/**
	 * Enter CC/GC/Brand Card Number - DW and Non-DW Sites
	 * 
	 * @param txtCCNumber
	 *            : CC/GC/Brand Card WebElement
	 * @param ccNumber
	 *            : CC/GC/Brand Number
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception - 
	 */
	final public static void enterCCNumber(WebElement txtCCNumber, String ccNumber, WebDriver driver) throws Exception {


		BrowserActions.typeOnTextField(txtCCNumber, ccNumber, driver, "CC/GC");

		Utils.waitForPageLoad(driver);

	}// enterCCNumber

	/**
	 * Enter CVC or CVV number
	 * 
	 * @param txtCVCNumber
	 *            : CVC or CVV WebElement
	 * @param cvcNumber
	 *            : CVV Number
	 * @param driver
	 *            : Webdriver Instance
	 * @throws Exception - 
	 */
	final public static void enterCVCNumber(WebElement txtCVCNumber, String cvcNumber, WebDriver driver)
			throws Exception {


		BrowserActions.typeOnTextField(txtCVCNumber, cvcNumber, driver, "CVC");

		Utils.waitForPageLoad(driver);

	}// enterCCNumber

	/**
	 * Click Pay Button - DW and Non-DW Sites
	 * 
	 * @param btnPay
	 *            : Pay Button WebElement
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception - 
	 */
	final public static void clickPayButton(WebElement btnPay, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(btnPay, driver, "Pay");

		Utils.waitForPageLoad(driver);

	}// clickPayButton

	/**
	 * Select Paypal and Click Proceed to Paypal Login Page Button - For DW
	 * 
	 * @param btnPaypal
	 *            : Paypal Radio WebElement
	 * @param btnProceedToPaypalPage
	 *            : ProceedToPaypalPage WebElement
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception - 
	 */
	final public static void proceedToPaypalPortal(WebElement btnPaypal, WebElement btnProceedToPaypalPage,
			WebDriver driver) throws Exception {


		selectPaypalPayment(btnPaypal, driver);
		BrowserActions.clickOnElementX(btnProceedToPaypalPage, driver, "Proceed To Paypal Page");

		Utils.waitForPageLoad(driver);

	}// proceedToPaypalPortal

	/**
	 * Do Credit Card Payment by selecting CC Type, Entering CC Number and Click
	 * on Pay Button
	 * 
	 * @param btnCreditCard
	 *            : CC WebElement
	 * @param optToSelect
	 *            : CC Option to select
	 * @param txtCCNumber
	 *            : CC NUmber WebElement
	 * @param ccNumber
	 *            :CC NUmber
	 * @param btnPay
	 *            : Pay Button WebElement
	 * @param driver
	 *            : WebDriver Instance
	 * @param expryMonth
	 *            : WebDriver Instance
	 * @param expryYear
	 *            : WebDriver Instance
	 * @throws Exception - 
	 */
	final public static void doCreditCardPayment(WebElement btnCreditCard, String optToSelect, WebElement txtCCNumber,
			String ccNumber, WebElement btnPay, WebElement expryMonth, WebElement expryYear, WebDriver driver)
					throws Exception {

		selectCreditCardPaymentAndExpiryYrMonth(btnCreditCard, optToSelect, driver);
		clickCreditCardPayment(btnCreditCard, driver);

		clickPayButton(btnPay, driver);

		Utils.waitForPageLoad(driver);

	}// doCreditCardPayment

	/**
	 * proceed to Order Review Page
	 * 
	 * @param btnOrdrReviewPg
	 *            : Order Review Button Webelement
	 * @param driver
	 *            : Webdriver Instance
	 * @throws Exception - 
	 */
	final public static void proceedToOrderReviewPage(WebElement btnOrdrReviewPg, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(btnOrdrReviewPg, driver, "Credit Card");

		Utils.waitForPageLoad(driver);

	}// proceedToOrderReviewPage

	/**
	 * Click Submit Order
	 * 
	 * @param btnSubmitOrder
	 *            : Submit Order Button WebElement
	 * @param driver
	 *            : Webdriver Instance
	 * @throws Exception - 
	 */
	final public static void submitOrder(WebElement btnSubmitOrder, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(btnSubmitOrder, driver, "Place order button");

		Utils.waitForPageLoad(driver);

	}// submitOrder

	/**
	 * To get a text from Payment Page
	 * 
	 * @param fromWhichTxtShldExtract
	 *            : WebElement from which Text need to extracted
	 * @param driver
	 *            : WebDriver instance
	 * 
	 * @return: String - text from Wishlist Page
	 * @throws Exception - 
	 */
	final public static String getTextFromPaymentPage(WebElement fromWhichTxtShldExtract, WebDriver driver)
			throws Exception {

		String textToReturn = BrowserActions.getText(driver, fromWhichTxtShldExtract, "Payment Page");

		return textToReturn;

	}// getTextFromPaymentPage

	final public static void enterCardDetails(LinkedHashMap<String, String> paymentDetails, WebDriver driver)
			throws Exception {


		Set<?> PaymentDetailsSet = paymentDetails.entrySet();
		Iterator<?> paymentDetailsIterator = PaymentDetailsSet.iterator();

		while (paymentDetailsIterator.hasNext()) {

			@SuppressWarnings("rawtypes")
			Map.Entry mapEntry = (Map.Entry) paymentDetailsIterator.next();
			String[] keyWithElementTypeAndDescriptionAndTextToType = mapEntry.getKey().toString().split("_");
			String locator = mapEntry.getValue().toString();

			switch (keyWithElementTypeAndDescriptionAndTextToType[0].toLowerCase()) {

			case "type":
				BrowserActions.typeOnTextField(locator, keyWithElementTypeAndDescriptionAndTextToType[2], driver,
						keyWithElementTypeAndDescriptionAndTextToType[1]);
				break;
			case "click":
				BrowserActions.clickOnElement(locator, driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
				break;
			case "select": {

				WebElement element = driver.findElement(By.cssSelector(locator));
				BrowserActions.javascriptClick(element, driver, "Drop down");
				List<WebElement> lstElement = element.findElement(By.xpath("..")).findElements(By.cssSelector("ul li"));
				for (WebElement e : lstElement) {

					if (!(element.getText().trim().toLowerCase().contains("select"))) {

						BrowserActions.clickOnElementX(element, driver, "list elements");
						break;
					}

					if (e.getText().trim().equals(keyWithElementTypeAndDescriptionAndTextToType[2])) {
						BrowserActions.scrollToViewElement(e, driver);
						BrowserActions.javascriptClick(e, driver, "list elements");
						break;
					}
				}
				break;
			}
			case "check": {
				BrowserActions.selectRadioOrCheckbox(driver.findElement(By.cssSelector(locator)),
						keyWithElementTypeAndDescriptionAndTextToType[2]);
				break;
			}
			default:
				break;

			}// Switch

			Utils.waitForPageLoad(driver);

		} // While

	}// enterCardDetails

}// Payment_Util