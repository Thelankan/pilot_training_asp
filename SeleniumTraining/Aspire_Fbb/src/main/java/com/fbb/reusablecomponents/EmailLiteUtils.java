package com.fbb.reusablecomponents;

//import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.fbb.support.BrowserActions;
import com.fbb.support.Utils;

/**
 * Re-Usable methods of Email Sign-Up Functionality for Retail Sites
 * 
 * Also created re-usable methods for DemandWare business flows
 * 
 * Some of the DW methods can be optimized and use for other platform based retail site also
 * 
 * 
 */
public class EmailLiteUtils {

	/**
	 * Close Email Signup Popup - DemandWare Functionality
	 * 
	 * @param btnClsEmailPopup
	 *            : WebElement for Email Close Button
	 * @param driver
	 *            : WebDriver instance
	 * @throws Exception -  
	 */
	final public static void closeEmailSignUpPopup(WebElement btnClsEmailPopup, WebDriver driver) throws Exception {


		BrowserActions.clickOnElementX(btnClsEmailPopup, driver, "Email Popup Close");

		Utils.waitForPageLoad(driver);

	}// closeEmailSignUpPopup

	/**
	 * Can be used to do a Email Signup on Email Lite Popup and on Footer Email Signup - DemandWare Functionality
	 * 
	 * @param txtEmailAdd
	 *            : WebElement for Email Signup Text Field
	 * @param btnSignup
	 *            : WebElement for Email Signup Button
	 * @param emailID
	 *            : Email Address. Either you can provide a email or if you want random email address just pass "random"
	 * @param driver
	 *            : WebDriver instance
	 * @throws Exception - 
	 */
	final public static void emailSignup(WebElement txtEmailAdd, WebElement btnSignup, String emailID, WebDriver driver) throws Exception {

		String emailIDToEnter = "";

		if (emailID.equalsIgnoreCase("random")) {
		}
		else
			emailIDToEnter = emailID;

		BrowserActions.typeOnTextField(txtEmailAdd, emailIDToEnter, driver, "Email ID");

		BrowserActions.clickOnElementX(btnSignup, driver, "Email Signup");

		Utils.waitForPageLoad(driver);

	}// emailSignup

	/**
	 * To get a text from Email Signup Popup
	 * 
	 * @param fromWhichTxtShldExtract
	 *            : WebElement from which Text need to extracted
	 * @param driver
	 *            : WebDriver instance
	 * @return: String - text from Email Lite Popup
	 * @throws Exception  -
	 */
	final public String getTextFromEmailSignup(WebElement fromWhichTxtShldExtract, WebDriver driver) throws Exception {

		String textToReturn = BrowserActions.getText(driver, fromWhichTxtShldExtract, "Email Signup Popup");

		return textToReturn;

	}// getTextFromEmailSignup

}// EmailLite_Util