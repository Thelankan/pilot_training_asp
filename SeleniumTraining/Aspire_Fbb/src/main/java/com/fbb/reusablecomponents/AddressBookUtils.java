package com.fbb.reusablecomponents;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.fbb.support.BrowserActions;
import com.fbb.support.Utils;

/**
 * Re-Usable methods of Billing Page Functionality for Retail Sites
 * 
 * Also created re-usable methods for DemandWare business flows
 * 
 * Some of the DW methods can be optimized and use for other platform based retail site also
 * 
 */
public class AddressBookUtils {
	
	/**
	 * Enter Billing Details
	 * 
	 * @param billingDetails
	 *            : HashMap String, String (key,Value) of list of webElement action to be perform <br>
	 * <br>
	 *            Example for Type: key: text_DescriptionOfElement_TextToTypeInTextBox || Value: Actual Locator in CSS Form <br>
	 * <br>
	 *            Example for Click: key: Click_DescriptionOfElement || Value: Actual Locator in CSS Form <br>
	 * <br>
	 *            Example for Select: key: text_DescriptionOfElement_OptionToSelectInOptionCombo || Value: Actual Locator in CSS Form <br>
	 * <br>
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	@SuppressWarnings("rawtypes")
	final public static void enterAddressDetails(LinkedHashMap <String, String> billingDetails, WebDriver driver) throws Exception {

		Set billingDetailsSet = billingDetails.entrySet();
		Iterator billingDetailsIterator = billingDetailsSet.iterator();

		while (billingDetailsIterator.hasNext()) {

			Map.Entry mapEntry = (Map.Entry) billingDetailsIterator.next();
			String[] keyWithElementTypeAndDescriptionAndTextToType = mapEntry.getKey().toString().split("_");
			String locator = mapEntry.getValue().toString();

			switch (keyWithElementTypeAndDescriptionAndTextToType[0].toLowerCase()) {
				
				case "type":
					BrowserActions.typeOnTextField(locator, keyWithElementTypeAndDescriptionAndTextToType[2], driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
					break;
				case "click":
					BrowserActions.clickOnElement(locator, driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
					break;
			case "select": {
				
				WebElement element = driver.findElement(By.cssSelector(locator));
				BrowserActions.javascriptClick(element, driver, "Drop down");				
				List<WebElement> lstElement = element.findElement(By.xpath("..")).findElements(By.cssSelector("ul li"));
				for(WebElement e: lstElement) {						
					
					if (!(element.getText().trim()
							.toLowerCase().contains("select"))) {						
						
						BrowserActions.clickOnElementX(element, driver,
								"list elements");
						break;
					}  
					
					
					if(e.getText().trim().equals(keyWithElementTypeAndDescriptionAndTextToType[2])) {						
						BrowserActions.scrollToViewElement(e, driver);
						BrowserActions.javascriptClick(e, driver, "list elements");
						break;
					}
				}
				break;
			}
			case "select1": {
				
				WebElement element = driver.findElement(By.cssSelector(locator));
				BrowserActions.javascriptClick(element, driver, "Drop down");				
				List<WebElement> lstElement = element.findElement(By.xpath("..")).findElements(By.cssSelector("ul li"));
				for(WebElement e: lstElement) {	
					
					if(e.getText().trim().equals(keyWithElementTypeAndDescriptionAndTextToType[2])) {						
						BrowserActions.scrollToViewElement(e, driver);
						BrowserActions.javascriptClick(e, driver, "list elements");
						break;
					}
				}
				break;
			}
				case "check": {
					BrowserActions.selectRadioOrCheckbox(driver.findElement(By.cssSelector(locator)), keyWithElementTypeAndDescriptionAndTextToType[2]);
					break;
				}

			}// Switch

			Utils.waitForPageLoad(driver);

		}// While

	}// enterBillingDetails
	

	/**
	 * Click Shipping Page Button - DW and Non-DW Sites
	 * 
	 * @param btnShippingPage
	 *            : Shipping Page Button WebElement
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void clickGoToShippingPage(WebElement btnShippingPage, WebDriver driver) throws Exception {


		BrowserActions.clickOnElementX(btnShippingPage, driver, "Shipping Page");

		Utils.waitForPageLoad(driver);

	}// clickGoToShippingPage

}// Billing_Page_Util