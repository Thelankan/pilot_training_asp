package com.fbb.reusablecomponents;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.fbb.support.BrowserActions;
import com.fbb.support.Utils;

/**
 * Re-Usable methods of PDP for Retail Sites
 *  
 * 
 */
public class PDPUtils {

	/**
	 * Select Size in PDP - DW and Non DW site
	 * 
	 * @param btnSize
	 *            : Size Button WebElement
	 * @param optToSelect
	 *            : Option to select from combobox
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception - 
	 */
	final public static void selectSize(WebElement btnSize, String optToSelect, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(btnSize, driver, "Clicked Size Button");
		BrowserActions.clickOnElement(optToSelect, driver, "Clicked Size Option Button");

		Utils.waitForPageLoad(driver);

	}// selectSize

	/**
	 * Select Quantity in PDP - DW and Non DW site
	 * 
	 * @param btnQty
	 *            : Quantity Button WebElement
	 * @param optToSelect
	 *            : Option to select from combobox
	 * @param driver
	 *            : WebDriver Instance
	 */
	final public static void selectQty(WebElement btnQty, String optToSelect, WebDriver driver) {

		BrowserActions.selectFromComboBox(btnQty, optToSelect, driver, "Quantity - " + optToSelect);

		Utils.waitForPageLoad(driver);

	}// selectQty

	/**
	 * Select Quantity in PDP - DW and Non DW site
	 * 
	 * @param btnQty
	 *            : Quantity Button WebElement
	 * @param optToSelect
	 *            : Option to select from combobox
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void pickQty(WebElement btnQty, String optToSelect, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(btnQty, driver, "Clicked Qty Button");
		BrowserActions.clickOnElement(optToSelect, driver, "Clicked Qty Option Button");

		Utils.waitForPageLoad(driver);

	}// pickQty

	/**
	 * Select Color in PDP - DW and Non DW site
	 * 
	 * @param btnColor
	 *            : Color Button WebElement
	 * @param optToSelect
	 *            : Option to select from combobox
	 * @param driver
	 *            : WebDriver Instance
	 */
	final public static void selectColor(WebElement btnColor, String optToSelect, WebDriver driver) {

		BrowserActions.selectFromComboBox(btnColor, optToSelect, driver, "Color - " + optToSelect);
		Utils.waitForPageLoad(driver);

	}// selectColor

	/**
	 * Click Color in PDP - DW and Non DW site
	 * 
	 * @param btnColor
	 *            : Color Button WebElement
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void selectColor(WebElement btnColor, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(btnColor, driver, "Color");

		Utils.waitForPageLoad(driver);

	}// selectColor

	/**
	 * Click Color in PDP - DW and Non DW site
	 * 
	 * @param btnColor
	 *            : Color Button CSS Locator
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void selectColor(String btnColor, WebDriver driver) throws Exception {


		BrowserActions.clickOnElement(btnColor, driver, "Color");

		Utils.waitForPageLoad(driver);

	}// selectColor

	/**
	 * Click on Product Thumbnail in PDP - DW and Non DW site
	 * 
	 * @param linkPrdThumbnail
	 *            : Product Thumbnail WebElement
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void clickProductThumbnail(WebElement linkPrdThumbnail, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(linkPrdThumbnail, driver, "Product Thumbnail");

		Utils.waitForPageLoad(driver);

	}// clickProductThumbnail

	/**
	 * Expand Product Details Tab - DW site
	 * 
	 * @param tabPrdDetails
	 *            : Product Details Tab WebElement
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void expandProductDetailsTab(WebElement tabPrdDetails, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(tabPrdDetails, driver, "Product Details Tab");

		Utils.waitForPageLoad(driver);

	}// expandProductDetailsTab

	/**
	 * Expand Product Size Guide Tab - DW site
	 * 
	 * @param tabPrdSizeGuide
	 *            : Product Size Guide Tab WebElement
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void expandProductSizeGuideTab(WebElement tabPrdSizeGuide, WebDriver driver) throws Exception {


		BrowserActions.clickOnElementX(tabPrdSizeGuide, driver, "Product Size Guide Tab");

		Utils.waitForPageLoad(driver);

	}// expandProductSizeGuideTab

	/**
	 * Expand Product Shipping Returns Tab - DW site
	 * 
	 * @param tabPrdShippingReturns
	 *            : Product Shipping Returns Tab WebElement
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void expandProductShippingReturnsTab(WebElement tabPrdShippingReturns, WebDriver driver) throws Exception {


		BrowserActions.clickOnElementX(tabPrdShippingReturns, driver, "Product Shipping Returns Tab");

		Utils.waitForPageLoad(driver);

	}// expandProductShippingReturnsTab

	/**
	 * Click on Product Play Video in PDP - DW and Non DW site
	 * 
	 * @param btnPlayVideo
	 *            : Play Video WebElement
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void clickPlayVideo(WebElement btnPlayVideo, WebDriver driver) throws Exception {


		BrowserActions.clickOnElementX(btnPlayVideo, driver, "Product Play Video");

		Utils.waitForPageLoad(driver);

	}// clickPlayVideo

	/**
	 * Click on Certona Previous/Next Arrow - DW Site [you might also like, Recently viewed and Trending now]
	 * 
	 * @param btnArrow
	 *            : Certona Arrow WebElement
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void clickCertonaArrow(WebElement btnArrow, WebDriver driver) throws Exception {


		BrowserActions.clickOnElementX(btnArrow, driver, "Certona Arrow");

		Utils.waitForPageLoad(driver);

	}// clickPlayVideo

	/**
	 * Open PDP from Certona - DW Site [you might also like, Recently viewed and Trending now]
	 * 
	 * @param btnPDPFromCertona
	 *            : PDP button from Certona
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void openPDPFromCertona(WebElement btnPDPFromCertona, WebDriver driver) throws Exception {


		BrowserActions.clickOnElementX(btnPDPFromCertona, driver, "Open PDP from Certona");

		Utils.waitForPageLoad(driver);

	}// openPDPFromCertona

	/**
	 * To get a text from PDP
	 * 
	 * @param fromWhichTxtShldExtract
	 *            : WebElement from which Text need to extracted
	 * @param driver
	 *            : WebDriver instance
	 * 
	 * @return: String - text from Wishlist Page
	 * @throws Exception -
	 */
	final public static String getTextFromPDP(WebElement fromWhichTxtShldExtract, WebDriver driver) throws Exception {

		String textToReturn = BrowserActions.getText(driver, fromWhichTxtShldExtract, "Paymet Page");

		return textToReturn;

	}// getTextFromPaymetPage

	/**
	 * Click Checkout/Shopping bag Button in Mini-cart PDP
	 * 
	 * @param btnCheckout
	 *            : Checkout button
	 * @param driver
	 *            : Webdriver Instance
	 * @throws Exception -
	 */
	final public static void goToCheckoutOrShoppingBagPage(WebElement btnCheckout, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(btnCheckout, driver, "Product Thumbnail");

		Utils.waitForPageLoad(driver);

	}// goToCheckoutPage
	
}// PDP_Util