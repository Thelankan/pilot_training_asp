package com.fbb.reusablecomponents;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

/**
 * Re-Usable methods of Shipping Page Functionality for Retail Sites
 * 
 * Also created re-usable methods for DemandWare business flows
 * 
 * Some of the DW methods can be optimized and use for other platform based retail site also
 * 
 * 
 */
public class ShippingPageUtils {

	/**
	 * Enter Shipping Details
	 * 
	 * @param shippingDetails
	 *            : HashMap String, String (key,Value) of list of webElement action to be perform <br>
	 * <br>
	 *            Example for Type: key: type_DescriptionOfElement_TextToTypeInTextBox || Value: Actual Locator in CSS Form <br>
	 * <br>
	 *            Example for Click: key: Click_DescriptionOfElement || Value: Actual Locator in CSS Form <br>
	 * <br>
	 *            Example for Select: key: select_DescriptionOfElement_OptionToSelectInOptionCombo || Value: Actual Locator in CSS Form <br>
	 * <br>
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	@SuppressWarnings("rawtypes")
	final public static void enterShippingDetails(LinkedHashMap <String, String> shippingDetails, WebDriver driver) throws Exception {

		Set shippingDetailsSet = shippingDetails.entrySet();
		Iterator shippingDetailsIterator = shippingDetailsSet.iterator();

		while (shippingDetailsIterator.hasNext()) {

			Map.Entry mapEntry = (Map.Entry) shippingDetailsIterator.next();
			
			String[] keyWithElementTypeAndDescriptionAndTextToType = mapEntry.getKey().toString().split("_");
			String locator = mapEntry.getValue().toString();
			
			switch (keyWithElementTypeAndDescriptionAndTextToType[0].toLowerCase()) {

				case "type":		
					BrowserActions.scrollToViewElement(driver.findElement(By.cssSelector(locator)), driver);
					BrowserActions.typeOnTextField(locator, keyWithElementTypeAndDescriptionAndTextToType[2], driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
					break;
				case "click":
					Utils.waitForPageLoad(driver);
					BrowserActions.clickOnElement(locator, driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
					Utils.waitForPageLoad(driver);
					break;
				case "select": {
					WebElement element = driver.findElement(By.cssSelector(locator));
					Select select = new Select(element);
					select.selectByVisibleText(keyWithElementTypeAndDescriptionAndTextToType[2]);
					Log.event("Selected(" + keyWithElementTypeAndDescriptionAndTextToType[2] + ") from " + keyWithElementTypeAndDescriptionAndTextToType[1]);
					break;
				}
				case "select1":{
					WebElement element = driver.findElement(By.cssSelector(locator));
					if(element.getText().equals(keyWithElementTypeAndDescriptionAndTextToType[2])){
						break;
					}
					
					BrowserActions.clickOnElementX(element, driver, "Drop down");				
					List<WebElement> lstElement = element.findElement(By.xpath("..")).findElements(By.cssSelector("ul li"));
					for(WebElement e: lstElement) {	
						if(e.getText().trim().equals(keyWithElementTypeAndDescriptionAndTextToType[2])) {						
							BrowserActions.scrollToViewElement(e, driver);
							BrowserActions.javascriptClick(e, driver, "state");
							break;
						}
					}
					Log.event("Selected(" + keyWithElementTypeAndDescriptionAndTextToType[2] + ") from " + keyWithElementTypeAndDescriptionAndTextToType[1]);
					break;
				}
				case "check": {
					BrowserActions.selectRadioOrCheckbox(driver.findElement(By.cssSelector(locator)),driver, keyWithElementTypeAndDescriptionAndTextToType[2]);;
					Log.event("Selected Radio or Checkbox :: " + keyWithElementTypeAndDescriptionAndTextToType[1]);
					break;
				}
				default:
					Log.failsoft("Option not matched - please read Method document to pass correct form of parameter. Try: Type/Click/Select");
					break;

			}// Switch

			Utils.waitForPageLoad(driver);
		}// While

	}// enterShippingDetails

	/**
	 * Click Billing Page Button - DW and Non-DW Sites
	 * 
	 * @param btnBillingPage
	 *            : Billing Page Button WebElement
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void clickGoToBillingPage(WebElement btnBillingPage, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(btnBillingPage, driver, "Billing Page");

		Utils.waitForPageLoad(driver);

	}// clickGoToBillingPage

	/**
	 * Click Shipping Button/Radio - DW and Non-DW Sites (STS or STH)
	 * 
	 * @param btnShippingType
	 *            : Shipping Button WebElement
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void selectShippingType(WebElement btnShippingType, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(btnShippingType, driver, "Shipping Type");

		Utils.waitForPageLoad(driver);

	}// selectShippingType

	/**
	 * Click Shipment Type Button/Radio - DW and Non-DW Sites (Standard, 2 3 Day and Overnight)
	 * 
	 * @param btnShipmentType
	 *            : Shipment Type Button WebElement
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void selectShipmentType(WebElement btnShipmentType, WebDriver driver) throws Exception {

		BrowserActions.clickOnElementX(btnShipmentType, driver, "Shipment Type");

		Utils.waitForPageLoad(driver);

	}// selectShipmentType
	
	/**
	 * Click Payment Page navigation button - DW and Non-DW Sites 
	 * 
	 * @param btnPaymentpage
	 *            : Payment Button WebElement
	 * 
	 * @param driver
	 *            : WebDriver Instance
	 * @throws Exception -
	 */
	final public static void goToPaymentPage(String btnPaymentpage, WebDriver driver) throws Exception {

		BrowserActions.clickOnElement(btnPaymentpage, driver, "Payment Page");

		Utils.waitForPageLoad(driver);

	}// goToPaymentPage

}// Shipping_Page_Util