
package com.fbb.reusablecomponents;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

/**
 * Re-Usable methods of Account Page Functionality for Retail Sites
 * 
 * Also created re-usable methods for DemandWare business flows
 * 
 * Some of the DW methods can be optimized and use for other platform based retail site also
 * 
 */
public class AccountUtils {

	/**
	 * Use this method to do all kind of Account related operations like Account Creation, Edit created address, Add Address etc.. <br>
	 * 
	 * - DW and Non DW Application
	 * 
	 * @param accountDetails - 
	 *            : Like Username, password, address etc.. : HashMap String, String (key,Value) of list of webElement action to be perform <br>
	 * <br>
	 *            Example for Type: key: text_DescriptionOfElement_TextToTypeInTextBox || Value: Actual Locator in CSS Form <br>
	 * <br>
	 *            Example for Click: key: Click_DescriptionOfElement || Value: Actual Locator in CSS Form <br>
	 * <br>
	 *            Example for Select: key: text_DescriptionOfElement_OptionToSelectInOptionCombo || Value: Actual Locator in CSS Form <br>
	 * <br>
	 * 
	 * @param driver - WebDriver Instance
	 * @throws Exception - 
	 */
	@SuppressWarnings("rawtypes")
	final public static void doAccountOperations(LinkedHashMap <String, String> accountDetails, WebDriver driver) throws Exception {

		Set accountDetailsSet = accountDetails.entrySet();
		Iterator accountDetailsIterator = accountDetailsSet.iterator();

		while (accountDetailsIterator.hasNext()) {

			Map.Entry mapEntry = (Map.Entry) accountDetailsIterator.next();
			String[] keyWithElementTypeAndDescriptionAndTextToType = mapEntry.getKey().toString().split("_");
			String locator = mapEntry.getValue().toString();

			switch (keyWithElementTypeAndDescriptionAndTextToType[0].toLowerCase()) {

				case "type":
					BrowserActions.typeOnTextField(locator, keyWithElementTypeAndDescriptionAndTextToType[2], driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
					break;
				case "click": 
					BrowserActions.clickOnElement(locator, driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
					break;
			case "select":
				WebElement element = driver
						.findElement(By.cssSelector(locator));
				if (element.getText().trim().equals(
						keyWithElementTypeAndDescriptionAndTextToType[2])) {
					Log.event(keyWithElementTypeAndDescriptionAndTextToType[2]+" is selected already");
				} else {
					BrowserActions
							.javascriptClick(element, driver, "Drop down");
					List<WebElement> lstElement = element.findElement(
							By.xpath("..")).findElements(
							By.cssSelector("ul li"));
					for (WebElement e : lstElement) {
						if (e.getText()
								.trim()
								.equals(keyWithElementTypeAndDescriptionAndTextToType[2])) {
							BrowserActions.scrollToViewElement(e, driver);
							BrowserActions.javascriptClick(e, driver,
									"list elements");
							Utils.waitForPageLoad(driver);
							break;
						}
					}
				}
				Utils.waitForPageLoad(driver);
				break;
				case "pickdate":
					BrowserActions.typeOnTextField(locator, keyWithElementTypeAndDescriptionAndTextToType[2], driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
					driver.findElement(By.cssSelector(locator)).sendKeys(Keys.ENTER);
					Utils.waitForPageLoad(driver);
					break;
				case "check": {
					BrowserActions.selectRadioOrCheckbox(driver.findElement(By.cssSelector(locator)), keyWithElementTypeAndDescriptionAndTextToType[2]);
					break;
				}
				default:
					Log.trace("Option not matched - please read Method document to pass correct form of parameter. Try: Type/Click/Select");
					break;

			}// Switch

			Utils.waitForPageLoad(driver);

		}// While

	}// doAccountOperations
	
	/**
	 * To generate a dynamic email id based on the environment
	 * @param driver -
	 * @param mailExtention -
	 * @return String - Email Id
	 * @throws Exception -
	 */
	final public static String generateEmail(WebDriver driver, String... mailExtention)throws Exception{
		String emailToReturn = new String();
		
		String mailExt =  new String();
		if(mailExtention.length > 0)
			mailExt = mailExtention[0];
		else
			mailExt = "@yopmail.com";
		
		String tcID = Utils.getCurrentTestName().split("\\_")[3].replace("C", "");
		String os = Utils.getRunDevicePlatform(driver);
		String browser = Utils.getRunBrowser(driver).toLowerCase();
		String brand = Utils.getCurrentBrandShort();
		String pltfrm = Utils.getRunPlatForm();
		
		switch(browser){
		case "microsoftedge" : browser = "edge";
			break;
		case "internet explorer" : browser = "ie";
			break;
		}
		
		switch(os){
		case "Windows10" : os = "win10";
			break;
		case "Windows7" : os = "win7";
			break;
		case "macOS10.12" : os = "mac";
			break;
		case "WIN8_1" : os = "win10";
			break;
		case "android" : os = "android";
			break;
		default: os = "win10";
			break;
		}
		
		switch(pltfrm){
		case "desktop" : pltfrm = "d";
			break;
		case "mobile" : pltfrm = "m";
			break;
		case "tablet" : pltfrm = "t";
			break;
		}
		
		emailToReturn = "aut" + "_" + tcID + "_" + os + "_" + browser + "_" + brand + "_" + pltfrm + "1" + mailExt;
		return emailToReturn;
	}

}// Account_Util