package com.fbb.reusablecomponents;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.EmailPreferencePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.headers.Headers;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.SheetsQuickstart;
import com.fbb.support.Utils;

/**
 * Re-Usable methods of End-To-End Script Functionality for Retail Sites
 */
public class GlobalNavigation {
	public static ElementLayer elementLayer;

	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");

	public static Object[] checkout(WebDriver driver, String searchKey, int i, String... credentials)throws Exception{
		String username = new String();
		String password = new String();
		String chkType = new String();
		boolean productAddRequired = false;
		if(credentials[0].contains("|")){
			username = credentials[0].split("\\|")[0];
			password = credentials[0].split("\\|")[1];
			chkType = "user";
		}else{
			username = credentials[0];
			chkType = "guest";
		}

		HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
		Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

		ShoppingBagPage cartPage = null;
		PdpPage pdpPage = null;
		CheckoutPage checkoutPage =  null;
		Headers headers = null;
		if(chkType.equals("user")){
			Log.message(i++ + ". Trying to Login with :: " + username + " / " + password, driver);

			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Signed In as :: " + myAcc.headers.getUserName(), driver);
			if(!myAcc.headers.getMiniCartCount().equals("0")){

				cartPage = myAcc.headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);

				cartPage.removeAllItemsFromCart();
				productAddRequired = true;
				Log.message(i++ + ". Removed All other Items from the Cart except given product id's.", driver);

				headers = cartPage.headers;
			}else{
				productAddRequired = true;
			}

		}else{
			productAddRequired = true;
		}

		if(productAddRequired){
			List<String> searchKeys = Arrays.asList(searchKey.split("\\|"));

			for(String searchkey : searchKeys){
				pdpPage = homePage.headers.navigateToPDP(searchkey);
				Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);

				pdpPage.addToBag();
				Log.message(i++ + ". Product Added to Cart.", driver);

				headers = pdpPage.headers;
			}
			if(headers == null) headers = homePage.headers;
			cartPage = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
		}

		LinkedList<LinkedHashMap<String, String>> productInformation = cartPage.getProductDetails();

		if(chkType.equals("user")){
			checkoutPage = (CheckoutPage)cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
		}else{
			SignIn signIn = (SignIn) cartPage.navigateToCheckout();
			signIn.typeGuestEmail(username);
			Log.message(i++ + ". Guest Email Id("+ username +") entered.", driver);
			checkoutPage = signIn.clickOnContinueInGuestLogin();
			Log.message(i++ + ". Navigated to Checkout page", driver);
		}

		//Handling PLCC Modal
		while(checkoutPage.getPLCCModalStatus())
			checkoutPage.closePLCCOfferByNoThanks1();

		Object[] obj = new Object[3];
		obj[0] = checkoutPage;
		obj[1] = i;
		obj[2] = productInformation;

		return obj;
	}

	public static Object[] shoppingCart(WebDriver driver, String searchKey, int i, String... credentials)throws Exception{
		String username = new String();
		String password = new String();
		String chkType = new String();
		if(credentials.length > 0) {
			if(credentials[0].contains("|")){
				username = credentials[0].split("\\|")[0];
				password = credentials[0].split("\\|")[1];
				chkType = "user";
			}else{
				username = credentials[0];
				chkType = "guest";
			}
		}else {
			username = AccountUtils.generateEmail(driver);
			chkType = "guest";
		}

		HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
		Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

		ShoppingBagPage cartPage = null;
		PdpPage pdpPage = null;
		Headers headers = homePage.headers;
		if(chkType.equals("user")){
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Signed In as :: " + myAcc.headers.getUserName(), driver);
			if(!myAcc.headers.getMiniCartCount().equals("0")){

				cartPage = myAcc.headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);

				cartPage.removeAllItemsFromCart();
				Log.message(i++ + ". Removed All Items from the Cart.", driver);

				headers = cartPage.headers;
			}

		}

		List<String> searchKeys = Arrays.asList(searchKey.split("\\|"));

		for(String searchkey : searchKeys){
			BrowserActions.refreshPage(driver);
			pdpPage = homePage.headers.navigateToPDP(searchkey);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);

			pdpPage.addToBag();
			Log.message(i++ + ". Product Added to Cart.", driver);

			headers = pdpPage.headers;
		}
		if(headers == null) headers = homePage.headers;

		cartPage = headers.navigateToShoppingBagPage();
		Log.message(i++ + ". Navigated to Shopping Bag Page." + cartPage.getCheckoutID(), driver);

		Object[] obj = new Object[2];
		obj[0] = cartPage;
		obj[1] = i;

		return obj;
	}

	public static Object[] addProduct_Checkout(WebDriver driver, String searchKey, int i, String credentials, boolean... returnPrdNames)throws Exception{

		String nav_URL = Utils.getWebSite();
		if(EnvironmentPropertiesReader.getInstance("env").getProperty("siteProtection").equals("true"))
			nav_URL = nav_URL.replace("http://", "https://" + EnvironmentPropertiesReader.getInstance("env").getProperty("appUserName") + ":" + EnvironmentPropertiesReader.getInstance("env").getProperty("appPassword") + "@");
		Log.event("Updated URL :: " + nav_URL);
		driver.get(nav_URL);
		Utils.waitForPageLoad(driver);

		Log.messageT("----------------------------------------------");
		Log.messageT("Platform        :: " + Utils.toCamelCase(Utils.getRunPlatForm()));
		Log.messageT("Device Platform :: " + Utils.toCamelCase(Utils.getRunDevicePlatform(driver)));
		Log.messageT("Browser Name    :: " + Utils.getRunBrowser(driver));
		Log.messageT("Browser Version :: " + Utils.getRunBrowserVersion(driver));
		Log.messageT("Testing Brand   :: " + Utils.getCurrentBrand());
		Log.messageT("----------------------------------------------");

		LinkedHashMap<String, String> prdNames = new LinkedHashMap<String, String>();
		HomePage homePage = new HomePage(driver);
		Headers headers = homePage.headers;
		PdpPage pdpPage = null;
		for(int x=0; x < searchKey.split("\\|").length; x++){
			pdpPage = headers.navigateToPDP(searchKey.split("\\|")[x]);
			headers = pdpPage.headers;
			if(returnPrdNames.length > 0 && (returnPrdNames[0])) {
				String prdName = pdpPage.getProductName();
				prdNames.put(searchKey.split("\\|")[x], prdName);
				Log.message(i++ + ". Navigated to PDP Page for Product :: " + prdName, driver);
			}else
				Log.message(i++ + ". Navigated to PDP Page", driver);

			pdpPage.addToBag();
			Log.message(i++ + ". Product Added to Bag!", driver);
		}

		ShoppingBagPage cartPage = pdpPage.headers.navigateToShoppingBagPage();//new ShoppingBagPage(driver).get();
		Log.message(i++ + ". Navigated to Cart Page.", driver);

		CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
		Log.message(i++ + ". Navigated to Checkout Page - SignIn Section", driver);

		checkoutPage.continueToShipping(credentials);
		Log.message(i++ + ". Checkout As :: " + credentials.split("\\|")[0], driver);

		Object[] obj = new Object[3];
		obj[0] = checkoutPage;
		obj[1] = i;
		obj[2] = prdNames;

		return obj;
	}

	public static Object[] addProduct_PlaceOrder(WebDriver driver, String searchKey, int i, String credentials)throws Exception{

		Object[] obj= addProduct_Checkout(driver, searchKey, i, credentials);

		CheckoutPage checkoutPage = (CheckoutPage) obj[0];
		i = (Integer) obj[1];

		checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", "Ground", "valid_address7");
		Log.message(i++ + ". Shipping Address entered successfully", driver);

		checkoutPage.continueToPayment();
		Log.message(i++ + ". Continued to Payment Page", driver);

		checkoutPage.fillingCardDetails1("card_Visa", false, false);
		Log.message(i++ + ". Card Details filling Successfully", driver);

		checkoutPage.clickOnPaymentDetailsContinueBtn();
		Log.message(i++ + ". Continued to Review & Place Order", driver);

		OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
		Log.message(i++ + ". Order Placed successfully", driver);

		LinkedHashMap<String, String> orderInfo = new LinkedHashMap<String, String>();
		orderInfo.put("OrderNumber", receipt.getOrderNumber());
		orderInfo.put("OrderDate", receipt.getOrderDate());
		orderInfo.put("OrderTotal", receipt.getOrderTotal());

		HomePage homePage = checkoutPage.headers.clickOnBrandFromHeader();
		Log.message(i++ + ". Returned To Home Page.", driver);

		obj = new Object[5];
		obj[0] = homePage;
		obj[1] = i;
		obj[2] = "valid_address1";
		obj[3] = "card_Visa";
		obj[4] = orderInfo;

		return obj;
	}

	/**
	 * To remove all items from quick order
	 * @throws Exception - Exception
	 */
	public static void removeAllItemsFromQuickOrder(WebDriver driver) throws Exception {
		try {
			String nav_URL = Utils.getWebSite() + redirectData.getProperty("quick-order");
			driver.get(nav_URL);

			Log.event("---------------------------------------------");
			Log.event("-------Removing Items from Quick Order-------");
			Log.event("---------------------------------------------");

			QuickOrderPage quickOrderPage = new QuickOrderPage(driver).get();

			quickOrderPage.removeAllItemsFromQuickOrder();
			Log.event("Quick Order products are removed!.");

		} catch (Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}

	public static Object[] addProduct_PlaceOrder_G(WebDriver driver, String searchKey, int i, String credentials)throws Exception{

		Object[] obj= addProduct_Checkout(driver, searchKey, i, credentials);

		CheckoutPage checkoutPage = (CheckoutPage) obj[0];
		i = (Integer) obj[1];

		checkoutPage.fillingShippingDetailsAsGuest("NO", "valid_address7", "Ground");
		Log.message(i++ + ". Shipping Address entered successfully", driver);

		checkoutPage.continueToPayment();
		Log.message(i++ + "Continued to Payment Page", driver);

		checkoutPage.fillingCardDetails1("card_Visa", false);
		Log.message(i++ + ". Card Details filling Successfully", driver);

		checkoutPage.clickOnPaymentDetailsContinueBtn();
		Log.message(i++ + ". Continued to Review & Place Order", driver);

		OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
		Log.message(i++ + ". Order Placed successfully", driver);

		LinkedHashMap<String, String> orderInfo = new LinkedHashMap<String, String>();
		orderInfo.put("OrderNumber", receipt.getOrderNumber());
		orderInfo.put("OrderDate", receipt.getOrderDate());
		orderInfo.put("OrderTotal", receipt.getOrderTotal());

		obj = new Object[5];
		obj[0] = checkoutPage;
		obj[1] = i;
		obj[2] = "valid_address7";
		obj[3] = "card_Visa";
		obj[4] = orderInfo;

		return obj;
	}

	public static Object[] Direct_Checkout(WebDriver driver, int i, String credentials)throws Exception{

		String nav_URL = Utils.getWebSite() + "/account";
		driver.get(nav_URL);

		SignIn signIn = new SignIn(driver).get();
		Log.message(i++ + ". Navigated To Sign In Page", driver);

		signIn.navigateToMyAccount(credentials.split("\\|")[0], credentials.split("\\|")[1]);
		Log.message(i++ + ". Signed In As :: " + credentials.toString());

		nav_URL = Utils.getWebSite() + "/checkout";
		driver.get(nav_URL);

		CheckoutPage checkoutPage = new CheckoutPage(driver).get();
		Log.message(i++ + ". Navigated to Checkout Page - SignIn Section", driver);

		Object[] obj = new Object[2];
		obj[0] = checkoutPage;
		obj[1] = i;

		return obj;
	}

	public static void RemoveWishListProducts(WebDriver driver)throws Exception{
		try{
			String wishlist = redirectData.get("wishlist");
			String nav_URL = Utils.getWebSite() + wishlist;
			driver.get(nav_URL);

			Log.event("----------------------------------------------------");
			Log.event("------------Removing Items from Wishlist------------");
			Log.event("----------------------------------------------------");

			WishListPage wishList = new WishListPage(driver).get();
			Log.event("Navigated to Wishlist page.");

			wishList.removeAllProduct();
			Log.event("Removed All Items from Wishlist.");
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}

	public static void RemoveAllProducts(WebDriver driver)throws Exception{
		try{
			String nav_URL = Utils.getWebSite() + redirectData.get("cart");
			driver.get(nav_URL);

			Log.event("------------------------------------------------");
			Log.event("------------Removing Items from Cart------------");
			Log.event("------------------------------------------------");

			ShoppingBagPage cartPage = new ShoppingBagPage(driver).get();
			Log.event("Navigated to Cart page.");

			cartPage.removeAllItemsFromCartExcept();
			Log.event("Removed All Items from Cart.");
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}

	@FindBy(css = ".pt_account.overview")
	WebElement accountPage;

	@FindBy(css = ".server-error")
	WebElement serverError;

	@FindBy(css = ".login-create .create-account")
	WebElement createAccBtn;
	public static Object registerNewUser(WebDriver driver, String... credentails)throws Exception{
		String firstName = "Hello";
		String lastName = "Welcome";
		String userID = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		String signOutUrl = Utils.getWebSite() + redirectData.get("logout");
		if(credentails.length > 0){
			userID = credentails[0].split("\\|")[0];
			password = credentails[0].split("\\|")[1];
		}

		try{
			driver.findElement(By.cssSelector(".pt_account null"));
		}catch(Exception x){
			String appURL = Utils.getWebSite();
			if(EnvironmentPropertiesReader.getInstance("env").getProperty("siteProtection").equals("true")){
				if(Utils.getRunBrowser(driver).equalsIgnoreCase("safari")) {
					Log.event("Turning of Phishing site warning.");
					driver.get("http://a");
				}
				String url = appURL.replace("http://", "https://" + EnvironmentPropertiesReader.getInstance("env").getProperty("appUserName") + ":" + EnvironmentPropertiesReader.getInstance("env").getProperty("appPassword") + "@");
				Log.event("Updated URL :: " + url);
				driver.get(url);
			}
			driver.get(appURL);
			HomePage homePage = new HomePage(driver).get();
			boolean state = homePage.headers.mouseOverSignIn();

			if(!state) {
				if(Utils.isMobile()){
					homePage.headers.openCloseHamburgerMenu("open");
					homePage.headers.clickOnSignInMobile();
				}else{
					homePage.headers.clickOnSignInLink();
				}
			}
		}
		Log.event("Navigated to Login Page!!!");
		try{
			WebElement element = null;
			if(Utils.getRunPlatForm().equals("mobile"))
				element = driver.findElement(By.cssSelector(".create-account.hide-desktop.hide-tablet"));
			else
				element = driver.findElement(By.cssSelector(".create-account-section a span"));

			BrowserActions.clickOnElementX(element, driver, "Create Account Button");
			Log.event("Clicked on Create Account Button.");
			Utils.waitForElement(driver, driver.findElement(By.id("dwfrm_profile_customer_firstname")));
			driver.findElement(By.id("dwfrm_profile_customer_firstname")).sendKeys(firstName);
			driver.findElement(By.id("dwfrm_profile_customer_lastname")).sendKeys(lastName);
			driver.findElement(By.id("dwfrm_profile_customer_email")).sendKeys(userID);
			driver.findElement(By.id("dwfrm_profile_customer_emailconfirm")).sendKeys(userID);
			driver.findElement(By.cssSelector("input[id*='dwfrm_profile_login_password_']")).sendKeys(password);
			driver.findElement(By.cssSelector("input[id*='dwfrm_profile_login_passwordconfirm_']")).sendKeys(password);
			WebElement registerBtn = null;//driver.findElement(By.name("dwfrm_login_register"));

			if(driver.findElement(By.id("wrapper")).getAttribute("class").contains("wish-list"))
				registerBtn = driver.findElement(By.name("dwfrm_profile_confirm"));
			else
				registerBtn = driver.findElement(By.name("dwfrm_login_register"));

			BrowserActions.scrollToViewElement(registerBtn, driver);
			BrowserActions.javascriptClick(registerBtn, driver, "Register Button");
			Log.event("Clicked On Register Button");
		}catch(NoSuchElementException e){
			Log.exception(e, driver);
		}
		Utils.waitForPageLoad(driver);
		try{
			if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(".pt_account.overview")))){
				Log.message("User Account Created for :: " + userID, driver);
				return new MyAccountPage(driver).get();
			}else if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(".server-error")))){
				Log.message("User ID already taken.", driver);
				driver.get(signOutUrl);
				SignIn signIn = new SignIn(driver).get();
				signIn.navigateToMyAccount(userID, password);
				return new MyAccountPage(driver).get();
			}else{
				Log.message("Unhandled Error Occured.", driver);
				return null;
			}
		}catch(NoSuchElementException e){
			try{
				if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(".server-error")))){
					Log.message("User ID already taken.", driver);
					driver.get(signOutUrl);
					SignIn signIn = new SignIn(driver).get();
					signIn.navigateToMyAccount(userID, password);
					return new MyAccountPage(driver).get();
				}else{
					Log.message("Unhandled Error Occured.", driver);
					return null;
				}
			}catch(NoSuchElementException e1){
				Log.message("User ID already taken.", driver);
				driver.get(signOutUrl);
				SignIn signIn = new SignIn(driver).get();
				signIn.navigateToMyAccount(userID, password);
				return new MyAccountPage(driver).get();
			}
		}

	}

	public static LinkedHashMap<String, Object> registerNewUser(WebDriver driver, int noOfAddr, int noOfCards, String... credentials)throws Exception{
		try {
			LinkedHashMap<String, Object> objToReturn = new LinkedHashMap<String, Object>(); 
			String appURL = Utils.getWebSite();
			if(EnvironmentPropertiesReader.getInstance("env").getProperty("siteProtection").equals("true")){
				if(Utils.getRunBrowser(driver).equalsIgnoreCase("safari")) {
					Log.event("Turning of Phishing site warning.");
					driver.get("http://a");
				}
				String url = appURL.replace("http://", "https://" + EnvironmentPropertiesReader.getInstance("env").getProperty("appUserName") + ":" + EnvironmentPropertiesReader.getInstance("env").getProperty("appPassword") + "@");
				Log.event("Updated URL :: " + url);
				driver.get(url);
			}
			driver.get(appURL);
			HomePage homePage = new HomePage(driver).get();
			MyAccountPage myAcc = null;
			if(credentials.length > 0){
				myAcc = homePage.headers.navigateToMyAccountWithRememberChkBox(credentials[0].split("\\|")[0], credentials[0].split("\\|")[1], false, true);
				objToReturn.put("email", credentials[0].split("\\|")[0]);
				objToReturn.put("password", credentials[0].split("\\|")[1]);
			}else{
				String autoGeneratedEmail = AccountUtils.generateEmail(driver);
				myAcc = homePage.headers.navigateToMyAccountWithRememberChkBox(autoGeneratedEmail, accountData.get("password_global"), false, true);
				objToReturn.put("email", autoGeneratedEmail);
				objToReturn.put("password", accountData.get("password_global"));
			}

			if(noOfAddr >= 1 && noOfAddr <= 4){
				AddressesPage addPage = myAcc.navigateToAddressPage();
				Log.event("Navigated To Address Page!");
				for(int i = 1; i <= noOfAddr; i++){
					addPage.clickOnAddNewAddress();
					addPage.fillingAddressDetails("address_" + i, false);
					addPage.clickSaveAddress();
					objToReturn.put("address_" + i, EnvironmentPropertiesReader.getInstance("checkout").getProperty("address_" + i));
					BrowserActions.refreshPage(driver);
				}
			}

			if(noOfCards >=1 && noOfCards <= 4){
				PaymentMethodsPage paymentPage = myAcc.navigateToPaymentMethods();
				int x = paymentPage.getNumberOfSavedCards();
				if(x < noOfCards)
					for(int i = 1; i <= (noOfCards-x); i++){
						paymentPage.addNewCard("card_" + i);
						objToReturn.put("card_" + i, EnvironmentPropertiesReader.getInstance("checkout").getProperty("card_" + i));
					}

			}

			String url = Utils.getWebSite() + EnvironmentPropertiesReader.getInstance("redirect").getProperty("logout");
			driver.get(url);
			return objToReturn;
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}

	public static void registerNewUser(WebDriver driver, int noOfAddr, int noOfCards, String credentails, String cardNumber)throws Exception{
		HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
		MyAccountPage myAcc = homePage.headers.navigateToMyAccount(AccountUtils.generateEmail(driver), accountData.get("password_global"), true);

		if(noOfAddr > 1 && noOfAddr <= 4){
			AddressesPage addPage = myAcc.clickOnAddressLink();
			for(int i = 1; i <= noOfAddr; i++){
				addPage.clickOnAddNewAddress();
				addPage.fillingAddressDetails("address_" + i, false);
				addPage.clickSaveAddress();
				BrowserActions.refreshPage(driver);
			}
		}

		if(noOfCards > 1 && noOfCards <= 4){
			PaymentMethodsPage paymentPage = myAcc.navigateToPaymentMethods();
			int x = paymentPage.getNumberOfSavedCards();
			if(x < noOfCards)
				for(int i = 1; i <= (noOfCards-x); i++){
					paymentPage.addNewCard("card_" + i);
				}
		}

		removeCard(driver, cardNumber);

		String url = Utils.getWebSite() + EnvironmentPropertiesReader.getInstance("redirect").getProperty("logout");
		driver.get(url);
	}

	public static void removeCard(WebDriver driver, String cardNumber) throws Exception{
		try{
			driver.get(Utils.getWebSite() + "/wallet");
			PaymentMethodsPage paymentPage = new PaymentMethodsPage(driver).get();
			Log.message("Navigated To Payments Page", driver);
			paymentPage.deleteCard(cardNumber);
			if(paymentPage.verifyCardPresent(cardNumber)){
				Log.fail("Cannot Delete Card.");
			}
			Log.message("Card("+ cardNumber +") deleted from payment methods");
		}catch(Exception e){
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}

	public static void emptyCartForUser(String username, String password, WebDriver driver)throws Exception{
		String appURL = Utils.getWebSite() + EnvironmentPropertiesReader.getInstance("redirect").getProperty("login");
		if(EnvironmentPropertiesReader.getInstance("env").getProperty("siteProtection").equals("true")){
			if(Utils.getRunBrowser(driver).equalsIgnoreCase("safari")) {
				Log.event("Turning of Phishing site warning.");
				driver.get("http://a");
			}
			String url = appURL.replace("http://", "https://" + EnvironmentPropertiesReader.getInstance("env").getProperty("appUserName") + ":" + EnvironmentPropertiesReader.getInstance("env").getProperty("appPassword") + "@");
			Log.event("Updated URL :: " + url);
			driver.get(url);
		}
		driver.get(appURL);
		SignIn signIn = new SignIn(driver).get();
		Log.event("Navigated to SignIn Page.");

		signIn.navigateToMyAccount(username, password);
		Log.event("Navigated to MyAccount Page.");

		RemoveAllProducts(driver);

		appURL = Utils.getWebSite() + EnvironmentPropertiesReader.getInstance("redirect").getProperty("logout");
		driver.get(appURL);
		Log.event("Logged out successfully.");
	}

	public static void loginAddNewAddress(WebDriver driver, int noOfAddr, String... credentails)throws Exception{
		String appURL = Utils.getWebSite();
		if(EnvironmentPropertiesReader.getInstance("env").getProperty("siteProtection").equals("true")){
			if(Utils.getRunBrowser(driver).equalsIgnoreCase("safari")) {
				Log.event("Turning of Phishing site warning.");
				driver.get("http://a");
			}
			String url = appURL.replace("http://", "https://" + EnvironmentPropertiesReader.getInstance("env").getProperty("appUserName") + ":" + EnvironmentPropertiesReader.getInstance("env").getProperty("appPassword") + "@");
			Log.event("Updated URL :: " + url);
			driver.get(url);
		}
		driver.get(appURL);
		HomePage homePage = new HomePage(driver).get();
		Log.event("Navigated to Home Page.");

		MyAccountPage myAcc = homePage.headers.navigateToMyAccount(credentails[0].split("\\|")[0], credentails[0].split("\\|")[1]);
		Log.event("Navigated to My Account page.");

		AddressesPage addPage = myAcc.navigateToAddressPage();
		Log.event("Navigated To Address Page!");
		for(int i = 1; i <= noOfAddr; i++){
			addPage.clickOnAddNewAddress();
			addPage.fillingAddressDetails("address_" + i, false);
			addPage.clickSaveAddress();
			BrowserActions.refreshPage(driver);
		}
		Log.event("Added Addresses.");

		String url = Utils.getWebSite() + EnvironmentPropertiesReader.getInstance("redirect").getProperty("logout");
		driver.get(url);
		Log.event("Signed Out of website.");
	}

	public static String formatToBillingAddress(String address)throws Exception{
		String nickName = address.split("\\|")[0].trim();
		String firstName = address.split("\\|")[1].trim();
		String lastName = address.split("\\|")[2].trim();
		String addressLine1 = address.split("\\|")[3].trim();
		String addressLine2 = address.split("\\|")[4].trim();
		String city = address.split("\\|")[5].trim();
		String state = address.split("\\|")[6].trim();
		String zipcode = address.split("\\|")[7].trim();

		String billingAddress = nickName + firstName + lastName + addressLine1 + addressLine2 + city + StateUtils.getStateCode(state) + zipcode + "USA";
		return billingAddress;
	}

	public static HashMap<String, String> formatAddressToMap(String address)throws Exception{
		HashMap<String, String> addressAsMap = new HashMap<String, String>();
		addressAsMap.put("addressLine1", address.split("\\|")[0].trim());
		addressAsMap.put("addressLine2", address.split("\\|")[1].trim());
		addressAsMap.put("city", address.split("\\|")[2].trim());
		addressAsMap.put("state", address.split("\\|")[3].trim());
		addressAsMap.put("zipcode", address.split("\\|")[4].trim());
		addressAsMap.put("phone", address.split("\\|")[5].trim());
		addressAsMap.put("country", address.split("\\|")[6].trim());
		addressAsMap.put("firstName", address.split("\\|")[7].trim());
		addressAsMap.put("lastName", address.split("\\|")[8].trim());
		return addressAsMap;
	}

	/**
	 * To delete all saved payment methods from account 
	 * @param WebDriver - driver
	 * @throws Exception - Exception
	 */
	public static void removeAllPaymentMethods(WebDriver driver)throws Exception{
		try {
			Log.event("-------------Removeing Saved Payment Methods-------------");
			driver.get(Utils.getWebSite() + "/wallet");			
			PaymentMethodsPage paymentPage = new PaymentMethodsPage(driver).get();
			Log.event("Navigated To Payments Page");
			paymentPage.deleteAllSavedCards();
			Log.event("Removed all saved payment methods.");

		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}

	/**
	 * To delete all saved addresses from account 
	 * @param WebDriver - driver
	 * @throws Exception - Exception
	 */
	public static void removeAllSavedAddress(WebDriver driver)throws Exception{
		try {
			Log.event("-------------Removeing Saved Addresses-------------");
			driver.get(Utils.getWebSite() + redirectData.get("addressBook"));
			AddressesPage addrPage = new AddressesPage(driver).get();
			Log.event("Navigated to Addresses.");
			addrPage.deleteAllTheAddresses();
			Log.event("Removed all saved addresses.");
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}

	/**
	 * To reset email subscription state for user
	 * @param WebDriver - driver
	 * @throws Exception - Exception
	 */
	public static void resetSubscriptionStatus(WebDriver driver)throws Exception{
		try {
			Log.event("-------------Reseting authenticated user email subscription status-------------");
			driver.get(Utils.getWebSite() + redirectData.get("emailPreference"));
			EmailPreferencePage emailPrefs = new EmailPreferencePage(driver).get();
			Log.event("Navigated to Email preferences page.");
			emailPrefs.resetSubscriptionState();
			Log.event("Reset current brand email subscription.");
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	} 

}// Billing_Page_Util