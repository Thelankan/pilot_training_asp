package com.fbb.reusablecomponents;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class BusinessManager {
	
	 // Description: login to Business Manager Web Application.
	// hard coded search box element
	 
	
	@SuppressWarnings("rawtypes")
	final public static void login_businessmanager(LinkedHashMap <String, String> loginDetails, WebElement element, WebDriver driver, String site) throws Exception {


		Set<?> loginDetailsSet = loginDetails.entrySet();
		Iterator<?> loginDetailsIterator = loginDetailsSet.iterator();

		while (loginDetailsIterator.hasNext()) {

			Map.Entry mapEntry = (Map.Entry) loginDetailsIterator.next();
			String[] keyWithElementTypeAndDescriptionAndTextToType = mapEntry.getKey().toString().split("_");
			String locator = mapEntry.getValue().toString();

			switch (keyWithElementTypeAndDescriptionAndTextToType[0].toLowerCase()) {
				case "type":
					BrowserActions.typeOnTextField(locator, keyWithElementTypeAndDescriptionAndTextToType[2], driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
					break;
				case "click":
					BrowserActions.clickOnElement(locator, driver, keyWithElementTypeAndDescriptionAndTextToType[1]);
					break;
				default:
					Log.trace("Option not matched - please read Method document to pass correct form of parameter. Try: Type/Click/Select");
					break;
			}// Switch
			
			Utils.waitForPageLoad(driver);
		} //while
		
			Utils.waitForElement(driver, element);
			
			
			if (site.equals("torrid")) {
				BrowserActions.clickOnElement("span[title='Torrid']", driver, "Navigate to Torrid site");
			}else if (site.equals("hottopic")) {
				BrowserActions.clickOnElement("span[title='HotTopic']", driver, "Navigate to Hottopic site");
			}else if (site.equals("boxlunch")) {
				BrowserActions.clickOnElement("span[title='BoxLunch']", driver, "Navigate to Hottopic box lunch site");
			}else if (site.equals("lovesick")){
				BrowserActions.clickOnElement("span[title='Lovesick']", driver,"Navigate to leve stick site");
			}
	}		
}
