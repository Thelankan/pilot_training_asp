package com.utils.testrail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;
import org.testng.Reporter;

import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.TestRailSupportUtils;

public class TestRailAPICalls extends LoadableComponent<TestRailAPICalls>{

	public static EnvironmentPropertiesReader envProperties = EnvironmentPropertiesReader.getInstance("env");
	public static String projectID = envProperties.getProperty("projectID");
	public static String suiteID = envProperties.getProperty("suiteID");
	public static String runID = envProperties.getProperty("testRunID");
	private boolean isConnected;
	private static TestRailAPIClient client;
	public TestRailAPICalls() {
		load();

	}

	@Override
	protected void isLoaded() {

		if (!isConnected) {
			Assert.fail("Failed To Connect");
		}

		if(client ==null)
			Assert.fail("Null Error");
	}

	@Override
	protected void load() {
		client = connectTo();
		isConnected = true;
	}

	/**
	 * To establish connection to TestRail
	 * @return - Connection
	 */
	public static TestRailAPIClient connectTo(){
		TestRailAPIClient client = new TestRailAPIClient(envProperties.getProperty("testrailUrl"));
		client.setUser(envProperties.getProperty("testrailUsername"));
		client.setPassword(envProperties.getProperty("testrailPassword"));

		return client;
	}

	/**
	 * To convert JSON Object into HashMap Strings
	 * @param obj - JSON Object
	 * @return HashMap -
	 * @throws Exception - Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static HashMap<String, String> convertJSON(Object obj)throws Exception{
		HashMap<String, String> objToReturn = new HashMap<String, String>();
		List<String> keySet = new ArrayList<String>(((HashMap) obj).keySet());
		for(int i = 0; i < keySet.size(); i++){
			String value = new String();
			value = ((HashMap<?, ?>) obj).get(keySet.get(i)) == null ? "":((HashMap<?, ?>) obj).get(keySet.get(i)).toString();
			objToReturn.put(keySet.get(i), value);
		}

		//System.out.println("Data :: " + obj.toString());

		return objToReturn;
	}

	/**
	 * To convert JSON List object into HashMap of HashMap
	 * @param obj - JSON List Object
	 * @return jsonlist -
	 * @throws Exception - Exception
	 */
	public static HashMap<String, HashMap<String, String>> convertJSONList(Object obj)throws Exception{
		HashMap<String, HashMap<String, String>> objToReturn = new HashMap<String, HashMap<String, String>>();
		String[] data = obj.toString().replace("[", "").replace("]", "").split("},");
		for(int i = 0; i < data.length; i++){
			HashMap<String, String> hashData = new HashMap<String, String>();
			for(int j=0; j < data[i].split(",").length; j++){
				data[i] = data[i].replace("{", ""); 
				hashData.put(data[i].split(",")[j].split(":")[0].replace("\"", ""), data[i].split(",")[j].split(":")[1].replace("\"", ""));
			}
			objToReturn.put(hashData.get("id"), hashData);
		}
		return objToReturn;
	}

	/**
	 * To get the information of given test case id
	 * @param caseId -
	 * @return HashMap -
	 * @throws Exception - Exception
	 */
	public static HashMap<String, String> getCaseInfo(String caseId)throws Exception{
		return convertJSON(client.sendGet("get_case/" + caseId));
	}

	/**
	 * To get the information of given Section id
	 * @param sectionId - 
	 * @return HashMap - 
	 * @throws Exception - Exception
	 */
	public static HashMap<String, String> getSectionInfo(String sectionId)throws Exception{
		return convertJSON(client.sendGet("get_section/" + sectionId));
	}

	/**
	 * To get the information of given Suite id
	 * @param suiteId - 
	 * @return HashMap -
	 * @throws Exception - Exception
	 */
	public static HashMap<String, String> getSuiteInfo(String suiteId)throws Exception{
		return convertJSON(client.sendGet("get_suite/" + suiteId));
	}

	/**
	 * To get the information of given Priority id
	 * @return HashMap -
	 * @throws Exception - Exception
	 */
	public static HashMap<String, HashMap<String, String>> getPriorityInfo()throws Exception{
		return convertJSONList(client.sendGet("get_priorities"));
	}

	/**
	 * To get the information of given Type id
	 * @return HashMap -
	 * @throws Exception - Exception
	 */
	public static HashMap<String, HashMap<String, String>> getTypeInfo()throws Exception{
		return convertJSONList(client.sendGet("get_case_types"));
	}

	/**
	 * To get the test case information as String
	 * @param caseId -
	 * @return String -
	 * @throws Exception - Exception
	 */
	public static String getCaseInfoAsString(String caseId)throws Exception{
		String dataToReturn = new String();
		HashMap<String, String> caseInfo = convertJSON(client.sendGet("get_case/" + caseId));

		List<String> keySet = new ArrayList<String>(caseInfo.keySet());
		for(int i=0; i<keySet.size(); i++)
			dataToReturn += System.lineSeparator() + keySet.get(i) + " : " + caseInfo.get(keySet.get(i));

		return dataToReturn;
	}

	/**
	 * To get the result for test case
	 * @param caseId -
	 * @return HashMap -
	 * @throws Exception - Exception
	 */
	public static HashMap<String, String> getResultForCase(String caseId)throws Exception{
		runID = runID.isEmpty()? System.getProperty("runID"): runID; 
		return convertJSON(client.sendGet("get_results_for_case/"+ runID + "/" + caseId));
	}

	/**
	 * To add result into testrail for given test case id
	 * @param caseId -
	 * @param status -
	 * @param comment -
	 * @return Status -
	 * @throws Exception - Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public HashMap<String, String> addResultForCase(String caseId, int status, String comment)throws Exception{
		runID = runID==null ? System.getProperty("runID"): runID;
		Map data = 	new HashMap();
		comment = comment.replace("<br>", "").replace("<b>", "").replace("</b>", "");
		data.put("status_id", status);
		data.put("comment", comment);
		return convertJSON(client.sendPost("add_result_for_case/" + runID + "/" + caseId, data));
	}

	/**
	 * To add result into testrail for given test case id with defect
	 * @param caseId -
	 * @param status -
	 * @param comment -
	 * @param defectID -
	 * @return Status -
	 * @throws Exception - Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public HashMap<String, String> addResultForCase(String caseId, int status, String comment, String defectID)throws Exception{

		runID = runID==null ? System.getProperty("runID"): runID;
		Map data = 	new HashMap();
		comment = comment.replace("<br>", "").replace("<b>", "").replace("</b>", "");
		data.put("status_id", status);
		data.put("comment", comment);
		data.put("defects", defectID);
		if(TestRailSupportUtils.STEP_RESULT_MAPPING.get(Reporter.getCurrentTestResult().getName()) != null)
			data.put("custom_step_results", TestRailSupportUtils.STEP_RESULT_MAPPING.get(Reporter.getCurrentTestResult().getName()));
		return convertJSON(client.sendPost("add_result_for_case/" + runID + "/" + caseId, data));
	}
	
	/**
	 * To add result into testrail for given test case id with defect
	 * @param run_id - 
	 * @param caseId -
	 * @param status -
	 * @param comment -
	 * @param defectID -
	 * @return Status -
	 * @throws Exception - Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public HashMap<String, String> addResultForCase(String run_id,String caseId, int status, String comment, String defectID)throws Exception{

		Map data = 	new HashMap();
		comment = comment.replace("<br>", "").replace("<b>", "").replace("</b>", "");
		data.put("status_id", status);
		data.put("comment", comment);
		data.put("defects", defectID);
		if(TestRailSupportUtils.STEP_RESULT_MAPPING.get(Reporter.getCurrentTestResult().getName()) != null)
			data.put("custom_step_results", TestRailSupportUtils.STEP_RESULT_MAPPING.get(Reporter.getCurrentTestResult().getName()));
		return convertJSON(client.sendPost("add_result_for_case/" + run_id + "/" + caseId, data));
	}

}
