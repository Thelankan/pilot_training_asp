package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C21653 extends BaseTest {
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	@Test( groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C21653(String browser) throws Exception {
		Log.testCaseInfo();

		//Load Test Data
		String productID = prdData.get("prd_variation2");
		String recommendation_color = prdData.get("recommendation_background");
		String salePrice_color=prdData.get("recommendation_saleprice");

		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);

		int i=1;
		try {

			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);

			cartPage.removeAllCartProduct();

			PdpPage pdpPage = cartPage.headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated to Pdp Page with the product!"+  productID, driver);

			pdpPage.addToBag();
			Log.message(i++ + ". Product Added to Bag.", driver);

			cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);

			//Step 1: Verify the display of Recommendations Heading
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("trendNowHeading"), cartPage), 
					"Check You may also like label should be displayed", 
					"You may also like label is displayed", 
					"You may also like label is not displayed", driver);

			//Step 2: Verify the display of Recommended products in the Cart
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("prodImageRecommendation","prodNameRecommendation","prodPriceRecommendation"), cartPage), 
					"Attributes should be displayed for the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
					"Attributes displayed for the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
					"Attributes are not displayed for recommendations product tile", driver);

			if(Utils.isDesktop()){

				Log.softAssertThat(cartPage.elementLayer.verifyCssPropertyForElement("sectionRecommendation", "background-color", recommendation_color, cartPage), 
						"Check background color in recommendation section", 
						"Background color is displayed with gray color", 
						"Background color is not displayed with gray color", driver);

				//Step 3: Verify the functionality when a mouse-over action is done for any of the product tiles in the Recommendations section
				Log.softAssertThat(cartPage.checkQuickShopIsDisplayedInRecommendation(),
						"The System should display the 'QUICKSHOP' button on top of the product image when mouse-hover action",
						"The System is displayed the 'QUICKSHOP' button on top of the product image when mouse-hover action",
						"The System is not displayed the 'QUICKSHOP' button on top of the product image when mouse-hover action", driver);

				QuickShop quickShop = cartPage.clickOnQuickShop();
				Log.message(i++ + ". Quick Shop overlay opened!", driver);

				Log.softAssertThat(cartPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("divQuickShop"), cartPage),
						"Check QUICKSHOP overlay dispalyed on click in Recommendation product from cart page", 
						"QUICKSHOP overlay displayed on click in Recommendation product from cart page", 
						"QUICKSHOP overlay is not displayed on click in Recommendation product from cart page", driver);

				quickShop.closeQuickShopOverlay();

			}
			else{
				//Step 4: Verify the functionality when a swipe left/right is performed for the products in the recommendations section
				Log.reference("Swipe functionality is not covered due to swipe limitation in selenium.");
			}


			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("prodStdPriceRecommendation","prodSalePriceRecommendation"), cartPage), 
					"Price Attributes should be displayed for the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price\"",
					"Price Attributes displayed for the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price.\"",
					"Price Attributes are not displayed in the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price.\"", driver);

			Log.softAssertThat(cartPage.elementLayer.verifyCssPropertyForElement("prodStdPriceRecommendation", "text-decoration", "line-through", cartPage),
					"Original price(if sale price exist), should be displayed with a strikethrough.",
					"Original price displayed with strikethrough.",
					"Original price not displayed with strikethrough.", driver);

			Log.softAssertThat(cartPage.elementLayer.verifyElementColor("prodSalePriceRecommendation", salePrice_color, cartPage),
					"Sale price should be displayed in red color.",
					"Sale price displayed in red color.",
					"Sale price not displayed in red color.", driver);

			//Step 5: Verify the functionality of the recommended products present in the recommendations section
			String name = cartPage.clickRecommendationPrdImageByIndex(0);
			String prdName = pdpPage.getProductName();

			Log.softAssertThat(prdName.toLowerCase().contains(name.toLowerCase()), 
					"When user click / tab on the product main image or product title, the system should navigate the user to the selected product detail page",
					"When user click / tab on the product main image or product title, the system is navigating to the selected product detail page",
					"When user click / tab on the product main image or product title, the system is not navigating to the selected product detail page", driver);

			Log.testCaseResult();

		}// try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}//TC_FBB_DROP4_C21653

}//search
