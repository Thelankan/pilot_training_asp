package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22649 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	@Test( groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22649(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String orderDetails = prdData.get("guest_order_return");
		String orderNumber = orderDetails.split("\\|")[0];
		String orderEmail = orderDetails.split("\\|")[1];
		String billingZipCode = orderDetails.split("\\|")[2];
		String invalidOrderNumber = "5709";
		String invalidOrderEmail = "49@yo";
		String invalidBillingZipCode = "##@$";
		String billingZipCodeFormat1 = "12345";
		String billingZipCodeFormat2 = "12345-1234";
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			SignIn signin = homePage.headers.clickOnSignInLink();
			Log.message(i++ + ". Navigated to SignIn Page.", driver);
	
			//Step-1: Verify the functionality of Check an Order Label
			Log.softAssertThat(signin.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblCheckAnOrder", "subHeadTxt", signin), 
					"Check an Order label should be displayed on top left in the Check an Order Section", 
					"Check an Order label is displayed on top left in the Check an Order Section", 
					"Check an Order label is not displayed on top left in the Check an Order Section", driver);
	
			Log.softAssertThat(signin.verifyDemandWarePropertyOfCheckAnOrderLabel(),
					"Label Text should be matched with the text in the Demandware Properties",
					"Label Text matched with the text in the Demandware Properties",
					"Label Text not  matched with the text in the Demandware Properties", driver);
	
			//Step-2: Verify the functionality of Subhead
			Log.softAssertThat(signin.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblCheckAnOrder", "subHeadTxt", signin), 
					"Subhead should be displayed below the Check an Order Label", 
					"Subhead is displayed below the Check an Order Label", 
					"Subhead is not displayed below the Check an Order Label", driver);
	
			Log.softAssertThat(signin.verifyDemandWarePropertyOfSubHeadText(),
					"Subhead Text should be matched with the text in the Demandware Properties",
					"Subhead Text matched with the text in the Demandware Properties",
					"Subhead Text not  matched with the text in the Demandware Properties", driver);
	
			//Step-3: Verify the functionality of Order Number
			Log.softAssertThat(signin.elementLayer.verifyVerticalAllignmentOfElements(driver, "subHeadTxt", "txtFldOrderNumber", signin), 
					"Order Number should be displayed below Subhead.", 
					"Order Number is displayed below Subhead.", 
					"Order Number is not displayed below Subhead.", driver);
	
			signin.typeOnOrderTrackFormFields(orderNumber,orderEmail,billingZipCode);
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("placeHolderOrderNumber"),signin),
					"Order number field should be  editable",
					"Order number field is editable ",
					"Order number field is not editable", driver); 
	
			Log.softAssertThat(signin.verifyOrderNoISMandatory(orderEmail,billingZipCode),
					"Without Order number(Mandatory Field) It should not proceed Further ",
					"Without Order number(Mandatory Field) So It not proceed Further",
					"Without Order number(Mandatory Field)  It proceed Further", driver); 
	
			signin.typeOnOrderTrackFormFields(orderNumber,orderEmail,billingZipCode);
	
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("placeHolderOrderNumber"),signin),
					"Place holder of Order number text  Position should be in correct place",
					"Place holder  of Order number text Position is in correct place",
					"Place holder  of Order number text Position is not in correct place", driver); 
	
			signin.navigateToGuestOrderStatusPage(orderNumber,orderEmail,billingZipCode);
	
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("lnkguestOrderStatusLandingPage"),signin),
					"Order details should be displayed when user passing valid order number and details",
					"Order details Displayed  ",
					"Order details not displayed", driver);
	
			driver.get(Utils.getWebSite());
			homePage= new HomePage(driver).get();
			Log.message(i++ + ". Navigated to HomePage.", driver);
	
			signin= homePage.headers.clickOnSignInLink();
			Log.message(i++ + ". Navigated to SignIn Page.", driver);
	
			Log.softAssertThat(signin.orderNumberMissmatch(invalidOrderNumber,orderEmail,billingZipCode),
					"Proper Error message should be displayed for Invalid order Number ",
					"Proper Error message displyed for invalid order number",
					"Proper Error message not displyed for invalid order number", driver); 
	
			Log.softAssertThat(signin.verifyOrderNoISMandatory(orderEmail,billingZipCode),
					"Proper Error message should be displayed for Blank Field of order Number",
					"Proper Error message  displayed for Blank Field of order Number",
					"No Proper Error message  displayed for Blank Field of order Number", driver); 
	
			//Step-4: Verify the functionality of Order Email
			signin.typeOnOrderTrackFormFields(orderNumber,orderEmail,billingZipCode);
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("placeHolderOrderEmail"),signin),
					"Order Email  field should be  editable",
					"Order Email field is editable ",
					"Order Email field is not editable", driver);  
	
			Log.softAssertThat(signin.elementLayer.verifyAttributeForElement("txtFldOrderEmail", "aria-required", "true", signin),
					"Without OrderEmail(Mandatory Field) It should not proceed Further ",
					"Without OrderEmail(Mandatory Field) So It not proceed Further",
					"Without OrderEmail(Mandatory Field)  It proceed Further", driver);
	
			signin.typeOnOrderTrackFormFields(orderNumber,orderEmail,billingZipCode);
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("placeHolderOrderEmail"),signin),
					"Place holder of Order Email text Position should be in correct place",
					"Place holder of Order Email text Position is in correct place",
					"Place holder of Order Email text Position is not in correct place", driver); 
	
			signin.navigateToGuestOrderStatusPage(orderNumber,orderEmail,billingZipCode);
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("lnkguestOrderStatusLandingPage"),signin),
					"Order details should be displayed when user passing valid order Email and details",
					"Order details Displayed  ",
					"Order details not displayed", driver); 
	
			driver.get(Utils.getWebSite());
			homePage= new HomePage(driver).get();
			Log.message(i++ + ". Navigated to Home page", driver);
	
			signin= homePage.headers.clickOnSignInLink();
			Log.message(i++ + ". Clicked on SignIn Link.", driver);
	
			Log.softAssertThat(signin.orderEmailMissmatch(orderNumber,invalidOrderEmail,billingZipCode),
					"Proper Error message should be displayed for Invalid orderEmail ",
					"Proper Error message displyed for invalid  orderEmail",
					"Proper Error message did not show for invalid  orderEmail. SFCCQA-313", driver); 
	
			Log.softAssertThat(signin.verifyOrderEmailISMandatory(orderNumber,billingZipCode),
					"Proper Error message should be displayed for Blank Field of orderEmail",
					"Proper Error message  displayed for Blank Field of orderEmail",
					"No Proper Error message  displayed for Blank Field of orderEmail", driver);
	
			//Step-5: Verify the functionality of Billing Zip Code
			signin.typeOnOrderTrackFormFields(orderNumber,orderEmail,billingZipCode);
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("placeHolderZipCode"),signin),
					"Billing Zip code should be  editable",
					"Billing Zip code field is editable ",
					"Billing Zip code field is not editable", driver); 
	
			Log.softAssertThat(!signin.zipCodeLimitType1(billingZipCodeFormat1),
					"User should able to enter  upto 5 digits in Zip code Field",
					"User is able to enter upto 5 digits in Zip code Field",
					"User not able to enter upto 5 digits in Zip code Field", driver);
	
			Log.softAssertThat(!signin.zipCodeLimitType2(billingZipCodeFormat2),
					"User should able to enter 5-4 digits in Zip code Field",
					"User is able to enter upto 5-4 digits in Zip code Field",
					"User not able to enter upto 5-4 digits in Zip code Field", driver);
	
			Log.softAssertThat(signin.verifyOrderPostalZipcCodeMandatory(orderNumber,orderEmail),
					"Without Postalzipcode(Mandatory Field) It should not proceed Further ",
					"Without PostalZipcoce(Mandatory Field) So It not proceed Further",
					"Without PostalZipcocde(Mandatory Field)  It proceed Further", driver);
	
			Log.softAssertThat(signin.orderBillingZipCodeMissmatch(orderNumber,orderEmail,invalidBillingZipCode),
					"Proper Error message should be displayed for Invalid Zipcode ",
					"Proper Error message displyed for invalid  Invalid Zipcode",
					"No Proper Error message displyed for Invalid Zipcode", driver); 
	
			Log.softAssertThat(signin.verifyOrderPostalZipcCodeMandatory(orderNumber,orderEmail),
					"Proper Error message should be displayed for Blank Field of Invalid Zipcode",
					"Proper Error message is displayed for Blank Field of Invalid Zipcode",
					"No Proper Error message is displayed for Blank Field of Invalid Zipcode", driver);
	
			signin.typeOnOrderTrackFormFields(orderNumber,orderEmail,billingZipCode);
	
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("placeHolderZipCode"),signin),
					"Place holder of Billing Zip code text Position should be in correct place",
					"Place holder of Billing Zip code text Position is in correct place",
					"Place holder of Billing Zip code text Position is not in correct place", driver); 
	
			signin.navigateToGuestOrderStatusPage(orderNumber,orderEmail,billingZipCode);
	
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("lnkguestOrderStatusLandingPage"),signin),
					"Order details should be displayed when user passing valid order Email and details",
					"Order details Displayed  ",
					"Order details not displayed", driver); 
	
			driver.get(Utils.getWebSite());
			homePage= new HomePage(driver).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
	
			signin= homePage.headers.clickOnSignInLink();
			Log.message(i++ + ". Navigated to SignIn Page.");
	
			//Step-6: Verify the functionality of Check Button
			signin.navigateToGuestOrderStatusPage(orderNumber,orderEmail,billingZipCode);
	
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("lnkguestOrderStatusLandingPage"),signin),
					"While clicking submit  button it should navigated to order detail page",
					"Navigation to order detail page is succesfull",
					"Navigation to order detail page is not succesfull", driver);
	
			Log.message("Covered Functionality in the test case id: C22748 STEP 1 TO 7");	 
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_HEADER_C22649

}// search
