package com.fbb.testscripts.drop2;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SearchResultPage;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19615 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	@Test(groups = { "medium", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19615(String browser) throws Exception {
		Log.testCaseInfo();

		String searchKey = prdData.get("Pagination");
		List<String> elementsToBeVerified = Arrays.asList("btnPaginationPrevious");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message(i++ + ". Enetered the search term in the Search Textbox!");

			int lastPage = Integer.parseInt(searchResultPage.getLastPageNumber());
			Log.message(i++ + ". Got the page number of the last page in the pagination bar!");

			if (lastPage == 0) {
				Log.fail("Page doesnot have pagination, Hence test case cannot be executed!", driver);
			}
			//1
			Log.softAssertThat(searchResultPage.verifyPaginationDisplayedBelowProductTiles(), 
					"Pagination in the Search results page, should be displayed below the product tiles.",
					"Pagination in the Search results page, displayed below the product tiles.", 
					"Pagination in the Search results page, not displayed below the product tiles.", driver);

			if (Utils.getCurrentBrand().equals(Brand.ks)) {
				Log.softAssertThat(searchResultPage.verifyIfCurrentPageNumberIsHighlihted()	&& 
						searchResultPage.elementLayer.verifyCssPropertyForPsuedoElement("btnPaginationCurrentPage", "after", "box-shadow", "rgba(0, 0, 0, 0.6) 0px 2px 5px 0px", searchResultPage),
						"The pagination number of the current page should be highlighted!",
						"The pagination number of the current page is highlighted!",
						"The pagination number of the current page is not highlighted", driver);
			} else {
				Log.softAssertThat(searchResultPage.verifyIfCurrentPageNumberIsHighlihted(),
						"The pagination number of the current page should be highlighted in black color!",
						"The pagination number of the current page is highlighted in black color!",
						"The pagination number of the current page is not highlighted in black color!", driver);
			}
			
			Log.softAssertThat(searchResultPage.verifyIfCurrentPageNumberIsDisplayedInWhiteText(),
					"The pagination number of the current page should be displayed in white color!",
					"The pagination number of the current page is displayed in white color!",
					"The pagination number of the current page is not displayed in white color!", driver);
			
			Log.softAssertThat(searchResultPage.verifyIfNonCurrentPageNumberHasWhiteBackground(),
					"The pagination number of the non-current pages should have white background!",
					"The pagination number of the non-current pages has white background!",
					"The pagination number of the non-current pages does not have white background!", driver);
			
			Log.softAssertThat(searchResultPage.verifyIfNonCurrentPageNumberIsDisplayedInGrayText(),
					"The pagination number of the non-current pages should be displayed in gray text!",
					"The pagination number of the non-current pages is displayed in gray text!",
					"The pagination number of the non-current pages is not displayed in gray text!", driver);
			
			//2
			Log.softAssertThat(searchResultPage.getCurrentPageNumber().equals("1"),
					"Always the 1st page number should be displayed!",
					"The 1st page number is displayed!",
					"The 1st page number is not displayed!", driver);
			
			Log.softAssertThat(searchResultPage.getLastPageNumber().equals(Integer.toString(lastPage)),
					"Always the last page number should be displayed!",
					"The last page number is displayed!",
					"The last page number is not displayed!", driver);
			
			Log.softAssertThat(searchResultPage.elementLayer.verifyPageElementsDoNotExist(elementsToBeVerified, searchResultPage),
					"In the 1st page: PREVIOUS link should not be displayed in Pagination!",
					"The PREVIOUS link is not displayed!",
					"The PREVIOUS link is displayed!", driver);
			
			elementsToBeVerified = Arrays.asList("btnPaginationNext");
			Log.softAssertThat(searchResultPage.elementLayer.VerifyPageElementDisplayed(elementsToBeVerified, searchResultPage),
					"The NEXT link should be displayed!",
					"The NEXT link is displayed!",
					"The NEXT link is not displayed!", driver);
			
			//3
			searchResultPage.clickNextButtonInPagination();
			Log.message(i++ + ". Clicked the NEXT link in the pagination bar!");

			Log.softAssertThat(searchResultPage.getCurrentPageNumber().equals("2"),
					"Page should be redirected to 2nd page!",
					"Page is redirected to 2nd page!",
					"Page is not redirected to 2nd page!", driver);
			
			Log.softAssertThat(searchResultPage.verifyPreviousLinkDisplayedToTheLeftOfFirstPageNumber(),
					"The Previous link should be displayed to the left of 1st page number!",
					"The Previous link is displayed to the left of 1st page number!",
					"The Previous link is not displayed to the left of 1st page number!", driver);
			
			//4
			searchResultPage.clickOnPageNumberInPagination(1);
			Log.message(i++ + ". Clicked the page number 1 in the pagination bar!");

			Log.softAssertThat(searchResultPage.isPageNumberDisplayed("1"),
					"The 1st page number should be displayed!",
					"The 1st page number is displayed!",
					"The 1st page number is not displayed!", driver);
			
			Log.softAssertThat(searchResultPage.getLastPageNumber().equals(Integer.toString(lastPage)),
					"Always the last page number should be displayed!",
					"The last page number is displayed!",
					"The last page number is not displayed!", driver);
			
			//5
			searchResultPage.clickNextButtonInPagination();
			Log.message(i++ + ". Clicked the NEXT link in the pagination bar!");
			
			elementsToBeVerified = Arrays.asList("btnPaginationPrevious", "btnPaginationNext", "btnPaginationFirstPage");
				
			if (lastPage > 2) {
				Log.softAssertThat(searchResultPage.elementLayer.VerifyPageElementDisplayed(elementsToBeVerified, searchResultPage),
						"The previous link, next link, first and last page numbers should be displayed!",
						"The previous link, next link, first and last page numbers are displayed!",
						"The previous link, next link, first and last page numbers are not displayed!", driver);
			} else {
				Log.failsoft("This verification is not possible as the number of the page are not sufficient to test it!", driver);
			}

			//6
			searchResultPage.clickLastPageNumber();
			Log.message(i++ + ". Clicked the last page number in the pagination bar!");
			
			Log.softAssertThat(searchResultPage.getCurrentPageNumber().equals(Integer.toString(lastPage)),
					"Should be redirected to last page!",
					"Is redirected to last page!",
					"Is not redirected to last page!", driver);
			
			Log.softAssertThat(searchResultPage.isPageNumberDisplayed(Integer.toString(lastPage-1)),
					"For the last page, previous two page numbers should be displayed!",
					"For the last page, previous two page numbers is displayed!",
					"For the last page, previous two page numbers is not displayed!", driver);
			
			Log.softAssertThat(searchResultPage.isPageNumberDisplayed(Integer.toString(lastPage-2)),
					"For the last page, previous two page numbers should be displayed!",
					"For the last page, previous two page numbers is displayed!",
					"For the last page, previous two page numbers is not displayed!", driver);
			
			elementsToBeVerified = Arrays.asList("btnPaginationPrevious");
			Log.softAssertThat(searchResultPage.elementLayer.VerifyPageElementDisplayed(elementsToBeVerified, searchResultPage),
					"The previous link should be displayed!",
					"The previous link is displayed!",
					"The previous link is not displayed!", driver);
			
			elementsToBeVerified = Arrays.asList("btnPaginationNext");
			Log.softAssertThat(searchResultPage.elementLayer.verifyPageElementsDoNotExist(elementsToBeVerified, searchResultPage),
					"The next link should be removed!",
					"The next link is removed!",
					"The next link is not removed!", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_DROP2_C19615


}// search


