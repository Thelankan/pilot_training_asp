package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C22778 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C22778(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		// Load Test Data
		String prd = prdData.get("prd_egift_card");
		
		//Pre-requisite - Account Should have more than one payment information
		int i = 1;
		try {
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			if(Utils.getCurrentBrand().equals(Brand.rm)) {
				SearchResultPage slpPage = homePage.headers.searchProductKeyword("Gift Card");
				Log.message(i++ + ". Navigated to PLP Page!", driver);

				QuickShop quickShop = slpPage.clickQuickShop(prd); 
				Log.message(i++ + ". Quick Shop overlay opened!", driver);
				
				/*quickShop.zoomProductImage();
				Log.message(i++ + ". Zoom the product image!", driver);

				Log.softAssertThat(quickShop.verifyZoomLensAndZoomWindowAreDisplayed(),
						"The primary product image should be zoomed!",
						"The primary product image is zoomed",
						"The primary product image is not zoomed", driver);

				Log.softAssertThat(quickShop.verifyZoomedImageDisplayedRightOfPrimaryProductImage(),
						"The zoomed image should be displayed to the right of primary product image!",
						"The zoomed image is displayed to the right of primary product image!",
						"The zoomed image is not displayed to the right of primary product image!", driver);

				Log.softAssertThat(quickShop.verifyTopOfZoomedImageAllignsWithTopOfProductImage(),
						"The top of zoomed image should be aligned with the top of primary product image!",
						"The top of zoomed image is aligned with the top of primary product image!",
						"The top of zoomed image is not aligned with the top of primary product image!", driver);*/
				
				Log.softAssertThat(quickShop.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtProductTitleDesktop", "imgProductPrimaryImage", quickShop),
						"The product name should be displayed right side to the primary product image!",
						"The product name is displayed right side to the primary product image!",
						"The product name is not displayed right side to the primary product image!", driver);
				
				if(quickShop.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtPromotionMessage"), quickShop)) {
					Log.softAssertThat(quickShop.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductTitleDesktop", "txtPromotionMessage", quickShop),
							"Special Promotional messaging should be displayed below the Gift Card name!",
							"Special Promotional messaging is displaying below the Gift Card name!",
							"Special Promotional messaging is not displaying below the Gift Card name!", driver);
				} else {
					Log.reference("Special Promotional messaging is not present for the product.!");
				}
				
				Log.softAssertThat(quickShop.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtSelectedQuantity"), quickShop),
						"Quantity dropdown should be displayed!",
						"Quantity dropdown is displayed!",
						"Quantity dropdown is not displayed!", driver);
				
				Log.softAssertThat(quickShop.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnAddToBag", "txtSelectedQuantity", quickShop),
						"The Quantity drop down should be displayed to the left side of the 'ADD TO BAG' button!",
						"The Quantity drop down is displayed to the left side of the 'ADD TO BAG' button!",
						"The Quantity drop down is not displayed to the left side of the 'ADD TO BAG' button!", driver);
				
				Log.softAssertThat(quickShop.getSelectedQuantityValue().trim().equals("1"),
						"The default selected value should be 1!",
						"The default selected value is 1!",
						"The default selected value is not 1!", driver);

				Log.softAssertThat(quickShop.getQuantityDropdownValues().equals(Arrays.asList("1","2","3","4","5","6","7","8","9","10")),
						"Values from 1 to 10 must be displayed in the dropdown!",
						"Values from 1 to 10 is displayed in the dropdown!",
						"Values from 1 to 10 is not displayed in the dropdown!", driver);
				
				Log.softAssertThat(!(quickShop.elementLayer.verifyElementDisplayedleft("divQuickShop", "btnClose", quickShop)),
						"X button should be displayed right side corner of the Quick shop overlay",
						"X button displayed right side corner of the Quick shop overlay",
						"X button not displayed right side corner of the Quick shop overlay", driver);

				quickShop.closeQuickShopOverlay();
				Log.message(i++ + ". Clicked on Close button in Quickshop overlay.", driver);

				Log.softAssertThat(quickShop.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divQuickShop"), quickShop),
						"On click, the modal should close",
						"Modal closed as expected.",
						"Modal not closed", driver);
			}else {
				Log.message("Quickshop for eGiftCard is not applicable to " + Utils.getCurrentBrand().getConfiguration().toString());
			}
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
		GlobalNavigation.RemoveAllProducts(driver);
		driver.quit();
		} // finally
	
	}//M1_FBB_DROP3_C22778
	
	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP3_C22778(String browser) throws Exception {

		String prd = prdData.get("prd_egift_card");

		final WebDriver driver = WebDriverFactory.get(browser);
		String userName = AccountUtils.generateEmail(driver);

		Log.testCaseInfo();
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			if(Utils.getCurrentBrand().equals(Brand.rm)) {
				homePage.headers.navigateToMyAccount(userName, accountData.get("password_global"), true);
				Log.message(i++ + ". Logged in as registered user With Email :: " + userName);

				SearchResultPage slpPage = homePage.headers.searchProductKeyword("Gift Card");
				Log.message(i++ + ". Navigated to PLP Page!", driver);

				QuickShop quickShop = slpPage.clickQuickShop(prd); 
				Log.message(i++ + ". Quick Shop overlay opened!", driver);

				quickShop.selectGCSize();
				Log.message(i++ + ". Amount selected!", driver);
				
				Log.softAssertThat(quickShop.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnAddToBag", "lnkAddToWishList", quickShop),
						"The Add To Bag should be displayed above the add to wishlist link!",
						"The Add To Bag is displayed above the add to wishlist link!",
						"The Add To Bag is not displayed above the add to wishlist link!", driver);

				WishListPage wishListPage = (WishListPage) quickShop.addToWishlist();
				Log.message(i++ + ". Clicked on Add To Wishlist!", driver);

				Log.softAssertThat(wishListPage.elementLayer.VerifyElementDisplayed(Arrays.asList("readyElement"), wishListPage),
						"The Wishlist page should be displayed!",
						"The Wishlist page is displayed!",
						"The Wishlist page is not displayed!", driver);
			}else {
				Log.message("Quickshop for eGiftCard is not applicable to " + Utils.getCurrentBrand().getConfiguration().toString());
			}
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			if(Utils.getCurrentBrand().equals(Brand.rm)) {
				GlobalNavigation.RemoveWishListProducts(driver);
			}
			driver.quit();
		} // finally

	}// M2_FBB_DROP3_C22778_4
	
	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP3_C22778(String browser) throws Exception {

		String prd = prdData.get("prd_egift_card");

		final WebDriver driver = WebDriverFactory.get(browser);
		String userName = AccountUtils.generateEmail(driver);

		Log.testCaseInfo();
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			if(Utils.getCurrentBrand().equals(Brand.rm)) {
				homePage.headers.navigateToMyAccount(userName, accountData.get("password_global"), true);
				Log.message(i++ + ". Logged in as registered user With Email :: " + userName);

				SearchResultPage slpPage = homePage.headers.searchProductKeyword("Gift Card");
				Log.message(i++ + ". Navigated to PLP Page!", driver);

				QuickShop quickShop = slpPage.clickQuickShop(prd); 
				Log.message(i++ + ". Quick Shop overlay opened!", driver);

				quickShop.selectGCSize();
				Log.message(i++ + ". Amount selected!", driver);
				
				quickShop.addToBag();
				Log.message(i++ + ". Clicked on Add to bag button when the personalization Checkbox is unchecked");

				Log.softAssertThat(quickShop.elementLayer.VerifyPageElementDisplayed(Arrays.asList("flytMiniCart"), quickShop),
						"The Gift card should be added to shopping bag",
						"The Gift card added to shopping bag",
						"The Gift card not added to shopping bag", driver);
				
				quickShop.closeCartOverlay();
			}else {
				Log.message("Quickshop for eGiftCard is not applicable to " + Utils.getCurrentBrand().getConfiguration().toString());
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// M3_FBB_DROP3_C22778_4
	
	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP3_C22778(String browser) throws Exception {
		//Load Test Data
		Log.testCaseInfo();
		String prd = prdData.get("prd_egift_card");

		final WebDriver driver = WebDriverFactory.get(browser);
		String textToCheck = prdData.get("txt_personalization_msg_max-alert");
		String intialTextToVerify = prdData.get("txt_personalization_msg_counter");
		String intialTextCount = prdData.get("txt_personalization_msg_count");
		// Get the web driver instance
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			if(Utils.getCurrentBrand().equals(Brand.rm)) {
				SearchResultPage slpPage = homePage.headers.searchProductKeyword("Gift Card");
				Log.message(i++ + ". Navigated to PLP Page!", driver);

				QuickShop quickShop = slpPage.clickQuickShop(prd); 
				Log.message(i++ + ". Quick Shop overlay opened!", driver);
		
				quickShop.chkOrUnchkAddPersonalMsg(true);
				Log.message(i++ + ". Selecting 'Add a personalized message' check box :: " + quickShop.getProductName(), driver);
		
				Log.softAssertThat(quickShop.verifyInitialValueOfTextCounter(intialTextCount, intialTextToVerify) && quickShop.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtPersonalMsgCounter"), quickShop),
						"For verification of the text displaying in Personal Message Text Area initally.",
						"The intial counter number and text are correct!",
						"The intial counter number and text is incorrect", driver);
		
				Log.softAssertThat(quickShop.verifyTextCounterIncreaseOrDecrease(10, false) && quickShop.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtPersonalMsgCounter"), quickShop),
						"For text count verification in Personal Message Text Area.",
						"Text Count is correctly reducing with or without spaces!",
						"Text Count is not reducing", driver);
		
				Log.softAssertThat(quickShop.verifyTextCounterIncreaseOrDecrease(10, true) && quickShop.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtPersonalMsgCounter"), quickShop),
						"For text count verification in Personal Message Text Area.",
						"Text Count is correctly increasing if a character is deleted!",
						"Text Count is not increasing if a character is deleted", driver);
		
				Log.softAssertThat(quickShop.verifyTextCounterIncreaseOrDecrease(330, false, textToCheck) && quickShop.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtPersonalMsgCounter"), quickShop),
						"For text count verification in Personal Message Text Area.",
						"Text 'You have reached the limit' is displayed!",
						"Text 'You have reached the limit' is not displayed!", driver);
		
				Log.softAssertThat(quickShop.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("txtAreaPersonalMsg"), 330, quickShop),
						"For text count verification in Personal Message Text Area.",
						"After maximum character the TextArea didnt allow to enter text!",
						"After maximum character the TextArea allowing to enter text!", driver);
			}else {
				Log.message("Quickshop for eGiftCard is not applicable to " + Utils.getCurrentBrand().getConfiguration().toString());
			}
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// M4_FBB_DROP3_C22778
	
}// search
