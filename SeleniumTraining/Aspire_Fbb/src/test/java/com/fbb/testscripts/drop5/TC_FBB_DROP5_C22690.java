package com.fbb.testscripts.drop5;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Arrays;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22690 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22690(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver);
		String password = EnvironmentPropertiesReader.getInstance("accounts").get("password_global");
		
		{
			GlobalNavigation.registerNewUser(driver, 3, 0, username + "|" + password);
		}

		int i=1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to HomePage.", driver);

			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to My Account page with e-mail :: " + username, driver);

            AddressesPage addressPage = myAcc.navigateToAddressPage();
            Log.message(i++ + ". Navigated to Address page in my account.", driver);

			//Verify the functionality of Edit
			
			Log.softAssertThat(addressPage.verifyEditDeleteMakeDefaultAllSavedCards(),
					"Edit should be displayed right of Make Default for respective address card if the address card that is edited by the user is not a default address card",
					"Edit is displayed right of Make Default for respective address card if the address card that is edited by the user is not a default address card",
					"Edit is not displayed right of Make Default for respective address card if the address card that is edited by the user is not a default address card", driver);

			Log.softAssertThat(addressPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "addressSavedList", "btnAddressEditSingle", addressPage),
					"Edit should be displayed at bottom left for respective address card if the address card that is edited by the user is a default address card.",
					"Edit is displayed at bottom left for respective address card if the address card that is edited by the user is a default address card.",
					"Edit is not displayed at bottom left for respective address card if the address card that is edited by the user is a default address card.", driver);

			addressPage.clickAddressEdit(1);
			Log.message(i++ + ". Clicked on Edit link in First address module.");
			Log.softAssertThat(addressPage.elementLayer.verifyPageElements(Arrays.asList("addressEditModule"), addressPage),
					"On clicking Edit in any of the saved address card - the respective address card should get converted to the edit module",
					"On clicking Edit in any of the saved address card - the respective address card is converted to the edit module",
					"On clicking Edit in any of the saved address card - the respective address card is not converted to the edit module", driver);

			Log.softAssertThat(addressPage.verifyAddressFieldValueEntered(),
					"On clicking Edit in any of the saved address card - the previously entered user data should be pre-populated.",
					"On clicking Edit in any of the saved address card - the previously entered user data is pre-populated.",
					"On clicking Edit in any of the saved address card - the previously entered user data is not pre-populated.", driver);

			//Verify the functionality of Edit Module
			Log.softAssertThat(addressPage.elementLayer.verifyElementIsEditable(driver, "inputAddressRequired", addressPage) &&
                            addressPage.elementLayer.verifyElementIsEditable(driver, "inputAddressFirstName", addressPage) &&
                            addressPage.elementLayer.verifyElementIsEditable(driver, "inputAddressLastName", addressPage) &&
                            addressPage.elementLayer.verifyElementIsEditable(driver, "inputAddressPhone", addressPage) &&
                            addressPage.elementLayer.verifyElementIsEditable(driver, "inputAddressLine1", addressPage) &&
                            addressPage.elementLayer.verifyElementIsEditable(driver, "inputAddressLine2", addressPage) &&
                            addressPage.elementLayer.verifyElementIsEditable(driver, "inputAddressPostal", addressPage) &&
                            addressPage.elementLayer.verifyElementIsEditable(driver, "inputAddressCity", addressPage),
					"User should be able to click/tap into each field and edit with the exception of Country",
					"User is able to click/tap into each field and edit with the exception of Country",
					"User is not able to click/tap into each field and edit with the exception of Country", driver);

			Log.softAssertThat(addressPage.elementLayer.verifyPageElements(Arrays.asList("txtNickNamePlaceHolderUpward",
		                    "txtFirstNamePlaceHolderUpward",
		                    "txtLastNamePlaceHolderUpward",
		                    "txtAddress1PlaceHolderUpward",
		                    "txtPhonePlaceHolderUpward",
		                    "txtPostalPlaceHolderUpward",
		                    "txtCityPlaceHolderUpward",
		                    "txtStatePlaceHolderUpward"), addressPage),
					"Place holder should be positioned on top of the field with previously entered data.",
					"Place holder is positioned on top of the field with previously entered data.",
					"Place holder is not positioned on top of the field with previously entered data.", driver);

			Log.softAssertThat(addressPage.elementLayer.verifyFieldValidation(driver, Arrays.asList(
                    		"inputAddressFirstName","inputAddressLastName","inputAddressPhone","inputAddressLine1","inputAddressPostal","inputAddressCity"),
                    		Arrays.asList("errorFirstName","errorLastName","errorAddress1","errorMsgPhoneNo","errorPostalCodeInvalid","errorCity","errorMsgAddressValidation"), "buttonSaveAddress", addressPage),
					"If user tries to leave the field blank or if any incorrect data format is entered, then appropriate error message should be displayed",
					"If user tries to leave the field blank or if any incorrect data format is entered, then appropriate error message is displayed",
					"If user tries to leave the field blank or if any incorrect data format is entered, then appropriate error message is not displayed", driver);

			Log.softAssertThat(addressPage.elementLayer.verifyAttributeForListOfElement(
					        Arrays.asList("inputAddressFirstName","inputAddressLastName","inputAddressPhone","inputAddressLine1","inputAddressPostal","inputAddressCity"),
		                    "aria-required", "true", addressPage),
					"Except address line-2 all other fields should be mandatory",
					"Except address line-2 all other fields is mandatory",
					"Except address line-2 all other fields is not mandatory", driver);

			Log.softAssertThat(addressPage.elementLayer.verifyZipCodeValidation(driver, "inputAddressPostal", "errorPostalCodeInvalid", addressPage),
					"System should allow the user to enter zip code as 5 digits (Ex: 55555) or 5 + 4 Digits (55555 4444)",
					"System allows the user to enter zip code as 5 digits (Ex: 55555) or 5 + 4 Digits (55555 4444)",
					"System does not allow the user to enter zip code as 5 digits (Ex: 55555) or 5 + 4 Digits (55555 4444)", driver);

			Log.softAssertThat(addressPage.elementLayer.verifyPageElementsChecked(Arrays.asList("chkMakeDefault"), addressPage),
					"The check box should be checked if the address card that is edited is default address card",
					"The check box is checked when the address card that is edited is default address card",
					"The check box is not checked when the address card that is edited is default address card", driver);

			addressPage.checkUncheckMakeDefaultCheckbox(true);
			Log.message(i++ + ". Make default address checkbox checked.", driver);
			Log.softAssertThat(addressPage.elementLayer.verifyPageElementsChecked(Arrays.asList("chkMakeDefault"), addressPage),
					"User should be able to select the check box to make the address default",
					"User is able to select the check box to make the address default",
					"User is not able to select the check box to make the address default", driver);

            addressPage.checkUncheckMakeDefaultCheckbox(false);
            Log.message(i++ + ". Make default address checkbox unchecked.", driver);
			Log.softAssertThat(!addressPage.elementLayer.verifyPageElementsChecked(Arrays.asList("chkMakeDefault"), addressPage),
					"User should be able to deselect the check box.",
					"User is able to deselect the check box.",
					"User is not able to deselect the check box.", driver);

			String addressNickName = addressPage.getNickNameOfAnAddress(2);
			addressPage.editNickName(addressNickName);
			Log.message(i++ + ". Existing nick name provided.", driver);
			Log.softAssertThat(addressPage.elementLayer.verifyPageElements(Arrays.asList("errorMsgAddressValidation"), addressPage),
					"If user tries to enter same Address Nickname for 2 addresses, then System should display the appropriate error message.",
					"When user tries to enter same Address Nickname for 2 addresses, System displays the appropriate error message.",
					"When user tries to enter same Address Nickname for 2 addresses, System does not display the appropriate error message.", driver);

			Log.softAssertThat(addressPage.elementLayer.verifyVerticalAllignmentOfListOfElements(
                    Arrays.asList("inputAddressFirstName","inputAddressLastName","inputAddressPhone","inputAddressLine1","inputAddressPostal","inputAddressCity"), addressPage),
					"Field alignment should be Nick Name -> FirstName -> Last Name -> Phone -> Country -> Address Line-1 -> Address Line-2 -> Zipcode <-> State -> City -> Make this Default Checkbox",
					"Fields are in expected alignment.",
					"Fields are not in expected alignment.", driver);

			//Verify the functionality of Cancel
			Log.softAssertThat(addressPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "chkMakeDefault", "btnAddAddressCancel", addressPage),
					"Cancel should be displayed below the Make this default check box",
					"Cancel is displayed below the Make this default check box",
					"Cancel is not displayed below the Make this default check box", driver);

			addressPage.clickCancelAddressCreation();
			Log.message(i++ + ". Clicked on Cancel button.", driver);
			Log.softAssertThat(!addressPage.elementLayer.VerifyElementDisplayed(Arrays.asList("addressEditModule"), addressPage),
					"On clicking Cancel, System should close the edit module and display the address card without making any changes to the previously entered user data.",
					"On clicking Cancel, System closes the edit module and display the address card without making any changes to the previously entered user data.",
					"On clicking Cancel, System does not close the edit module and display the address card without making any changes to the previously entered user data.", driver);

			Log.softAssertThat(addressPage.verifyEditDeleteRelativePlacement() && addressPage.elementLayer.verifyCssPropertyForElement("addressDefaultNickName", "background", "Dot_Green.svg", addressPage),
					"If the edited address card is previously a default address card, then Edit and Delete option should be displayed at the bottom of the respective address card with Default Payment Indicator (Green dot) ",
					"When the edited address card is previously a default address card, then Edit and Delete option is displayed at the bottom of the respective address card with Default Payment Indicator (Green dot) ",
					"When the edited address card is previously a default address card, then Edit and Delete option is not displayed at the bottom of the respective address card with Default Payment Indicator (Green dot) ", driver);

			Log.softAssertThat(addressPage.verifyDefaultAddressLocatedFirst(),
					"If the edited address card is previously a default address card, the respective address card should be positioned first",
					"When the edited address card is previously a default address card, the respective address card is positioned first",
					"When the edited address card is previously a default address card, the respective address card is not positioned first", driver);

			Log.softAssertThat(addressPage.verifyAddressDisplayedInChronologicalOrder(),
					"the other cards (if any) should be sorted based on the chronological order",
					"the other cards (if any) are sorted based on the chronological order",
					"the other cards (if any) are not sorted based on the chronological order", driver);

			Log.softAssertThat(addressPage.verifyEditDeleteMakeDefaultAllSavedCards(),
					"If the edited address card is previously not a default address card, then Make Default, Edit and Delete option should be displayed at the bottom of the respective address card",
					"If the edited address card is previously not a default address card, then Make Default, Edit and Delete option is displayed at the bottom of the respective address card",
					"If the edited address card is previously not a default address card, then Make Default, Edit and Delete option is not displayed at the bottom of the respective address card", driver);

			//Verify the functionality of Update
			Log.message("On successful validation, System should update the address card and save the changed address in the address book for the below data combination");

			addressPage.clickAddressEdit(1);
            Log.softAssertThat(addressPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "buttonSaveAddress", "btnAddAddressCancel", addressPage),
                    "Update should be displayed right of Cancel",
                    "Update is displayed right of Cancel",
                    "Update is not displayed right of Cancel", driver);

            addressPage.enterAddressLine2("AddressLine2");
            addressPage.clickOnUpdateButton();
			Log.message("If the user has updated the fields including the Address Line 2 Optional field", driver);
            Log.softAssertThat(addressPage.elementLayer.verifyElementTextContains("addressSavedList","AddressLine2", addressPage),
                    "then the respective address card should be updated",
                    "then the respective address card is made as default",
                    "then the respective address card is not made as default", driver);

            addressPage.clickAddressEdit(1);
            addressPage.enterAddressLine2("");
            addressPage.clickOnUpdateButton();
            Log.message("If the user has updated the field excluding the Address Line 2 Optional field (leaving it blank)");
            Log.softAssertThat(!addressPage.elementLayer.verifyElementTextContains("addressSavedList","AddressLine2", addressPage),
                    "then the respective address card should be updated",
                    "then the respective address card is updated",
                    "then the respective address card is not updated", driver);

            addressPage.clickAddressEdit(1);
            addressPage.enterAddressLine2("West");
            addressPage.enterAddressLine1("3580, Independence Blvd");
            addressPage.enterCity("Charlotte");
            addressPage.clickOnUpdateButton();
			Log.message("If the user has updated only some fields and not all the fields.");
            Log.softAssertThat(addressPage.elementLayer.verifyElementTextContains("addressSavedList","West", addressPage) &&
                     addressPage.elementLayer.verifyElementTextContains("addressSavedList","3580, Independence Blvd", addressPage) &&
                     addressPage.elementLayer.verifyElementTextContains("addressSavedList","Charlotte", addressPage),
                    "then the respective address card should be updated",
                    "then the respective address card is updated",
                    "then the respective address card is not updated", driver);

            addressPage.clickAddressEdit(2);
            Log.softAssertThat(!addressPage.elementLayer.verifyPageElementsChecked(Arrays.asList("chkMakeDefault"), addressPage),
                    "the check box unchecked if the address card that is edited is not a default address card",
                    "the check box unchecked if the address card that is edited is not a default address card",
                    "the check box unchecked if the address card that is edited is not a default address card", driver);

            addressNickName = addressPage.getValueFrom("nickname");
            addressPage.checkUncheckMakeDefaultCheckbox(true);
            addressPage.clickOnUpdateButton();
			Log.message("If the user has not updated any field but have only changed the status of checkbox (changed from uncheck to check)");
			Log.softAssertThat(addressPage.elementLayer.verifyElementTextEqualTo("addressDefaultNickName", addressNickName, addressPage),
					"then the respective address card should be made as default",
					"then the respective address card is made as default",
					"then the respective address card is not made as default", driver);

			Log.softAssertThat(addressPage.elementLayer.verifyCssPropertyForElement("addressDefaultNickName", "background", "Dot_Green.svg", addressPage),
					"default address indicator should be displayed",
					"default address indicator is displayed",
					"default address indicator is not displayed", driver);

			Log.softAssertThat(addressPage.verifyDefaultAddressLocatedFirst(),
					"the respective address card should be positioned first followed by other cards in chronological order",
					"the respective address cards are positioned first followed by other cards in chronological order",
					"the respective address cards are not positioned first followed by other cards in chronological order", driver);

			Log.softAssertThat(addressPage.verifyEditDeleteRelativePlacement(),
					"Edit and Delete should display at the bottom of the respective address card",
					"Edit and Delete displays at the bottom of the respective address card",
					"Edit and Delete do not display at the bottom of the respective address card", driver);

			Log.message("If the user has not updated any field but have only changed the status of checkbox (changed from check to uncheck), ");
			addressPage.clickAddressEdit(1);
			Log.message(i++ + ". Clicked edit on first address.", driver);
			addressNickName = addressPage.getValueFrom("nickname");
			Log.event("Edited Address Nickname :: " + addressNickName);
			addressPage.checkUncheckMakeDefaultCheckbox(false);
			Log.message(i++ + ". Unchcked make eafult checkbox.", driver);
			addressPage.clickOnUpdateButton();
			Log.softAssertThat(!addressPage.verifyAddressIsDefault(addressNickName),
					"The card should still be saved as default.",
					"The card is still be saved as default.",
					"The card is no longer saved as default.", driver);

			Log.softAssertThat(addressPage.getAddressPositionForNickName(addressNickName) == 1,
					"The card should still be positioned first.",
					"The card is positioned first.",
					"The card is not positioned first.", driver);

			Log.softAssertThat(addressPage.verifyEditDeleteMakeDefaultAllSavedCards(),
					"Make Default, Edit and Delete should display at the bottom of the respective address card",
					"Make Default, Edit and Delete are at the bottom of the respective address card",
					"Make Default, Edit and Delete are not at the bottom of the respective address card", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.removeAllSavedAddress(driver);
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP_C21550

}// search
