package com.fbb.testscripts.smoke;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class Smoke extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "desktop"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23450(String browser) throws Exception {


		String username="testuser1234@yopmail.com";//(testData.get("Credentials").split("\\|")[0]).toString();
		String password="test123@";

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo();
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);                   

			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);

			Log.softAssertThat(myAcc.elementLayer.VerifyPageElementDisplayed(Arrays.asList("readyElement"), myAcc), 
					"Page should be navigated to My account page when login with valid credentials", 
					"Page is navigated to My account page when login with valid credentials",
					"Page is not navigated to My account page when login with valid credentials", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_SMOKE_001
	
	@Test(groups = { "desktop"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23451(String browser) throws Exception {

		final WebDriver driver = WebDriverFactory.get(browser);
		String username= AccountUtils.generateEmail(driver);
		String password="test123@";
		Log.testCaseInfo();
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);                   

			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to My Account page!",driver);

			ProfilePage profilePage = myAcc.navigateToUpdateProfile();
			Log.message(i++ + ". Navigated to Profile Page!", driver);

			HashMap<String, String> profileInfoBefore = profilePage.getProfileInformation();

			profilePage.fillProfileInformation();
			profilePage.typeOnPassword(password);
			Log.message(i++ + ". Entered the fields with new information.", driver);

			profilePage.clickUpdateInfoBtn();
			Log.message(i++ + ". Updated profile with new information.", driver);

			profilePage = myAcc.navigateToUpdateProfile();
			Log.message(i++ + ". Navigated to Profile Page!", driver);

			HashMap<String, String> profileInfoAfter = profilePage.getProfileInformation();

			Log.message(i++ + ". Profile Before :: " + profileInfoBefore);
			Log.message(i++ + ". Profile After :: " + profileInfoAfter);

			Log.softAssertThat(Utils.compareTwoHashMapNotEqual(profileInfoBefore, profileInfoAfter),
					"Profile page should be updated.",
					"Profile is successfully updated with new information", 
					"Profile not updated.", driver);

			myAcc = homePage.headers.navigateToMyAccount();
			Log.message(i++ + ". Navigated To My Account Page!", driver);

			AddressesPage addPage = myAcc.navigateToAddressPage();
			Log.message(i++ + ". Navigated to Addressbook Page!", driver);

			Log.assertThat(addPage.elementLayer.verifyPageElements(Arrays.asList("noAddressSection"), addPage), 
					"Account Should not hold any address by default.", 
					"Account doesn't have any Address.", 
					"Account have some address exists.", driver);

			addPage.fillingAddressDetails("address_1", false);
			Log.message(i++ + ". Filled Address Details.", driver);

			addPage.clickSaveAddress();
			Log.message(i++ + ". Clicked on Save Address.", driver);

			Log.softAssertThat(addPage.elementLayer.verifyPageElements(Arrays.asList("addressListSection"), addPage), 
					"New Address Should Be added to Account.", 
					"New Address added to Account.", 
					"New Address Not Added to account.", driver);

			addPage.deleteAllTheAddresses();
			Log.message(i++ + ". Deleted saved addresses.", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_SMOKE_002
	
	@Test(groups = { "desktop"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23452(String browser) throws Exception {

		String searchKey = prdData.get("prd_variation");
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo();
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			//Load Regular Price Product
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);

			Log.softAssertThat(pdpPage.elementLayer.verifyPageLoadStatus(pdpPage),					 
					"Page should be navigated to PDP", 
					"Page is navigated to PDP", 
					"Page is not navigated to PDP", driver);						

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_SMOKE_003	
	
	@Test(groups = { "desktop"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23453(String browser) throws Exception {

		String level1 = "Dresses";

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo();
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PlpPage plpPage = homePage.headers.navigateTo(level1);
			Log.message(i++ + ". Navigated to PLP page for category :: " + level1, driver);

			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), plpPage),					 
					"Page should be navigated to category landing page", 
					"Page is navigated to category landing page", 
					"Page is not navigated to category landing page", driver);						

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_SMOKE_004
	
	@Test(groups = { "desktop"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23454(String browser) throws Exception {

		String searchKey = "Dresses";

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo();
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			SearchResultPage slp = homePage.headers.searchProductKeyword(searchKey);			
			Log.message(i++ + ". Searched with keyword :: " + searchKey, driver);

			Log.softAssertThat(slp.getSearchResultsCount()> 0,					 
					"Search results should be displayed when searching with valid keyword", 
					"Search results is displayed when searching with valid keyword", 
					"Search results is not displayed when searching with valid keyword", driver);						

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_SMOKE_005
	
	@SuppressWarnings("unchecked")
	@Test(groups = { "desktop"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23529(String browser) throws Exception {

		String searchKey = prdData.get("prd_variation");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String username= AccountUtils.generateEmail(driver) + "|test123@";
		
		Log.testCaseInfo();
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.checkout(driver, searchKey, i, username);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0]; 
			i = (Integer) obj[1];
			LinkedList<LinkedHashMap<String, String>> productInformationInCartPage = (LinkedList<LinkedHashMap<String, String>>) obj[2];

			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", "Ground", "valid_address1");
			Log.message(i++ + ". Entered shipping address", driver);

			checkoutPage.continueToPayment();
			Log.message(i++ + ". Clicked on 'Continue' button after filling shipping address");

			checkoutPage.fillingCardDetails1("card_Visa", false);
			Log.message(i++ + ". Entered Payment details",driver);

			checkoutPage.continueToReivewOrder();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details", driver);

			LinkedList<LinkedHashMap<String, String>> productInformationInCheckoutPage = checkoutPage.getProductDetails();
			
			/*boolean status = true;
			for(int x=0 ; x < productInformationInCartPage.size(); x++) {
				if(!Utils.compareTwoHashMap(productInformationInCartPage.get(x), productInformationInCheckoutPage.get(x)))
					status = false;
			}*/
			
			Log.softAssertThat(Utils.compareAndPrintTableLinkedListHashMap("Product Details Validation", "Cart", "Checkout", productInformationInCartPage, productInformationInCheckoutPage),
					"Product details such as Product Name, Color, Size, Quanity and Price should be same in Cart & Checkout Page.",
					"Cart Page & Checkout Page has the same values.",
					"Cart Page & Checkout Page doesn't have same values.", driver);
			
			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!", driver);

			Log.assertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPage.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order should be placed successfully as a Registered user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a Registered user and Order confirmation page is displayed!",
					"Order is not placed successfully as a Registered user and Order confirmation page is not displayed!", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_SMOKE_007
	
	@Test(groups = { "desktop"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23532(String browser) throws Exception {

		String searchKey = prdData.get("prd_variation");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String guestEmail = AccountUtils.generateEmail(driver);
		
		Log.testCaseInfo();
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.checkout(driver, searchKey, i, guestEmail);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0]; 

			checkoutPage.fillingShippingDetailsAsGuest("YES","valid_address7","Ground");
			Log.message(i++ + ". Entered shipping address", driver);

			checkoutPage.continueToPayment();
			Log.message(i++ + ". Clicked on 'Continue' button after filling shipping address", driver);

			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message(i++ + ". Entered Payment details",driver);

			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details", driver);

			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPage.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order should be placed successfully as a Guest user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a Guest user and Order confirmation page is displayed!",
					"Order is not placed successfully as a Guest user and Order confirmation page is not displayed!", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_SMOKE_008

	@Test(groups = { "desktop"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23531(String browser) throws Exception {

		String level1 = "Dresses";

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo();
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PlpPage plpPage = homePage.headers.navigateTo(level1);
			Log.message(i++ + ". Navigated to PLP page for category :: " + level1, driver);

			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), plpPage),					 
					"Page should be navigated to category landing page", 
					"Page is navigated to category landing page", 
					"Page is not navigated to category landing page", driver);

			QuickShop quickShop = plpPage.clickOnQuickShop();
			Log.message(i++ + ". Navigated to QuickShop Page for Product :: " + quickShop.getProductName());

			String color = quickShop.selectColor();
			Log.message(i++ + ". Selected Color :: " + color);

			String size = quickShop.selectSize();
			Log.message(i++ + ". Selected Size :: " + size);

			quickShop.addToBag();
			Log.message(i++ + ". Size & Color variation Selected and clicked on Add To Bag.", driver);

			Log.assertThat(quickShop.elementLayer.VerifyElementDisplayed(Arrays.asList("flytMiniCart"), quickShop), 
					"Product Should be added to Cart & ATB Overlay should appear.", 
					"Product added to Cart & ATB Overlay displayed.", 
					"Somthing went wrong.", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_SMOKE_004

	@Test(groups = { "desktop"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23533(String browser) throws Exception {

		String searchKey = prdData.get("prd_more-color");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo();
		int i = 1;
		try {
			Log.reference("C23532 :: Guest Checkout been covered in same test script.");
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName());

			pdpPage.addToBag();
			Log.message(i++ + ". Product Added to Shopping Cart.", driver);

			ShoppingBagPage cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);

			String prdName = cartPage.getProductName(0);
			String colorBefore = cartPage.getProductColorVariation(0);
			String sizeBefore = cartPage.getProductSizeVariation(0);
			Log.event("Product Name/Color/Size Values Before:: " + prdName + "/" + colorBefore + "/" + sizeBefore);
			
			cartPage.clickOnEditLink(0);
			Log.message(i++ + ". Clicked on Edit link for Product :: " + prdName, driver);

			String size = new String();
			String color = new String();
			if(Utils.isDesktop()) {
				Log.assertThat(new QuickShop(driver).get().getPageLoadStatus(), 
						"Update Overlay should be displayed.", 
						"Update Overlay displayed.", 
						"Update Overlay not displayed.", driver);
				
				QuickShop quickShop = new QuickShop(driver).get();
				color = quickShop.selectColor("random");
				size = quickShop.selectSize("random");
				Log.event("Updated Size/Color :: " + size + "/" + color);
				
				quickShop.addToBag();
				Log.message(i++ + ". Clicked on Update botton.", driver);
				
			}else {
				Log.assertThat(new PdpPage(driver).get().getPageLoadStatus(), 
						"Page should be navigated to PDP Page.",
						"Page navigated to PDP Page.",
						"Page not navigated to PDP Page.", driver);
				
				pdpPage = new PdpPage(driver).get();
				
				color = pdpPage.selectColor("random");
				size = pdpPage.selectSize("random");
				Log.event("Updated Size/Color :: " + size + "/" + color);
				
				pdpPage.clickOnUpdate();
				Log.message(i++ +". Clicked on Update Button.", driver);
			}

			Log.assertThat(new ShoppingBagPage(driver).get().getPageLoadStatus(), 
					"Page should be redirected to Shopping Cart Page.",
					"Page redirected to Shopping Cart Page.",
					"Page not redirected to Shopping Cart Page.", driver);
			
			String colorafter = cartPage.getProductColorVariation(0);
			String sizeafter = cartPage.getProductSizeVariation(0);
			
			Log.event("Product Name/Color/Size Values After:: " + prdName + "/" + colorafter + "/" + sizeafter);
			
			Log.softAssertThat(color.equalsIgnoreCase(colorafter) && size.equalsIgnoreCase(sizeafter),
					"Color/Size values should be updated.",
					"Color/Size values are updated.",
					"Color/Size values are not updated.", driver);
			
			String qtyBefore = cartPage.getProductQuantity(0);
			Log.message(i++ + ". Quantity before update :: " + qtyBefore, driver);
			
			cartPage.clickOnQuantityArrow("up");
			Log.message(i++ + ". Clicked on Up arrow in Quantity.", driver);
			
			String qtyAfterUp = cartPage.getProductQuantity(0);
			Log.event("Quantity Before/After :: " + qtyBefore + "/" + qtyAfterUp);
			Log.softAssertThat(!qtyBefore.equals(qtyAfterUp),
					"Quantity should be updated.",
					"Quantity values are updated.",
					"Quantity values are not updated.", driver);
			
			cartPage.clickOnQuantityArrow("down");
			Log.message(i++ + ". Clicked on Down arrow in Quantity.", driver);
			
			String qtyAfterDown = cartPage.getProductQuantity(0);
			Log.event("Quantity Before/After :: " + qtyBefore + "/" + qtyAfterUp);
			Log.softAssertThat(!qtyAfterDown.equals(qtyAfterUp),
					"Quantity should be updated.",
					"Quantity values are updated.",
					"Quantity values are not updated.", driver);
			
			cartPage.removeCartProduct(0);
			Log.message(i++ + ". Clicked on Remove button for product :: " + prdName);
			
			Log.assertThat(cartPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("divSingleCartRow"), cartPage),
					"Product should be removed from Cart.",
					"Product removed from cart.",
					"Product not removed from cart.", driver);
			
			pdpPage = cartPage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.addToBag();
			Log.message(i++ + ". Product Added to Cart.", driver);
			
			cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to cart Page.", driver);
			
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
			
			String guestMailID = AccountUtils.generateEmail(driver);
			checkoutPage.continueToShipping(guestMailID);
			Log.message(i++ + ". Continued as Guest with Email ID :: " + guestMailID);
			
			checkoutPage.fillingShippingDetailsAsGuest("YES", "valid_address7", "Ground");
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Shipping address filled and Continued to Payment.", driver);
			
			checkoutPage.fillingCardDetails1("card_MasterCard", false);
			Log.message(i++ + ". Master Card Details Filled :: " + EnvironmentPropertiesReader.getInstance("checkout").getProperty("card_MasterCard"), driver);
			
			checkoutPage.continueToReivewOrder();
			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on Place Order Button.", driver);

			Log.assertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtOrderConfirmationMsg"), checkoutPage), 
					"Order should place successfully.",
					"Order successfully placed. Order Number :: " + checkoutPage.getOrderNumber(),
					"Order was not successfully placed.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_SMOKE_004

	@Test(enabled = false, groups = { "desktop"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23456(String browser) throws Exception {
		Log.testCaseInfo();
		String searchKey = prdData.get("prd_variation");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String guestEmail = AccountUtils.generateEmail(driver);
		
		
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.checkout(driver, searchKey, i, guestEmail);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0]; 

			checkoutPage.fillingShippingDetailsAsGuest("YES","valid_address1","Ground");
			Log.message(i++ + ". Entered shipping address", driver);

			checkoutPage.continueToPayment();
			Log.message(i++ + ". Clicked on 'Continue' button after filling shipping address", driver);

			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message(i++ + ". Entered Payment details",driver);

			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details", driver);

			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPage.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order should be placed successfully as a Guest user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a Guest user and Order confirmation page is displayed!",
					"Order is not placed successfully as a Guest user and Order confirmation page is not displayed!", driver);

			checkoutPage.fillPasswordInOrderReceipt();
			Log.message(i++ + ". Passwords(test123@) Filled successfully in Registration Form.", driver);
			
			checkoutPage.clickOnCreateAccountOrderReceipt();
			Log.message(i++ + ". Clicked on Create Account Button.", driver);
			
			MyAccountPage myAcc = new MyAccountPage(driver).get();

			Log.assertThat(myAcc.getPageLoadStatus(),
					"Page should be redirected to My Account Page.",
					"Page redirected to My Account Page.",
					"Something went wrong.", driver);
			
			AddressesPage addressPage = myAcc.navigateToAddressPage();
			Log.message(i++ + ". Navigated to Addressbook Page.", driver);
			
			Log.assertThat(addressPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("addressDefaultIndicator"), addressPage),
					"Default Address should be displayed.",
					"Default Address displayed.",
					"Something went wrong.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//Need to create a BM Script to deleted login.
			driver.quit();
		} // finally

	}// TC_FBB_SMOKE_C23456
	
	@Test(groups = { "desktop"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23534(String browser) throws Exception {

		String searchKey = prdData.get("prd_variation");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver);
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|test123@");
			GlobalNavigation.emptyCartForUser(username, accountData.get("password_global"), driver);
		}
		Log.testCaseInfo();
		int i = 1;
		try {
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName());
			
			pdpPage.addToBag();
			Log.message(i++ + ". Product added Shopping Cart.", driver);

			ShoppingBagPage cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Cart Page.", driver);
			
			LinkedList<LinkedHashMap<String, String>> productInformationInCartPage = cartPage.getProductDetails();
			
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to Checkout-Login Page.", driver);

			checkoutPage.continueToShipping(username + "|test123@");
			Log.message(i++ + ". Continued as Registered user for Email :: " + username);
			
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", "Ground", "valid_address1");
			Log.message(i++ + ". Entered shipping address", driver);

			checkoutPage.continueToPayment();
			Log.message(i++ + ". Clicked on 'Continue' button after filling shipping address");

			checkoutPage.fillingCardDetails1("card_Visa", false);
			Log.message(i++ + ". Entered Payment details",driver);

			checkoutPage.continueToReivewOrder();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details", driver);

			LinkedList<LinkedHashMap<String, String>> productInformationInCheckoutPage = checkoutPage.getProductDetails();
			
			/*boolean status = true;
			for(int x=0 ; x < productInformationInCartPage.size(); x++) {
				if(!Utils.compareTwoHashMap(productInformationInCartPage.get(x), productInformationInCheckoutPage.get(x)))
					status = false;
			}*/
			
			Log.softAssertThat(Utils.compareAndPrintTableLinkedListHashMap("Product Details Validation", "Cart", "Checkout", productInformationInCartPage, productInformationInCheckoutPage),
					"Product details such as Product Name, Color, Size, Quanity and Price should be same in Cart & Checkout Page.",
					"Cart Page & Checkout Page has the same values.",
					"Cart Page & Checkout Page doesn't have same values.", driver);
			
			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!", driver);

			Log.assertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPage.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order should be placed successfully as a Registered user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a Registered user and Order confirmation page is displayed!",
					"Order is not placed successfully as a Registered user and Order confirmation page is not displayed!", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_SMOKE_001
	
	
	
}// search
