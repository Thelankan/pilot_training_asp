package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PaypalConfirmationPage;
import com.fbb.pages.PaypalPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22549 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22549(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
		String paypalemail = accountData.get("credential_paypal").split("\\|")[0];
		String paypalpassword = accountData.get("credential_paypal").split("\\|")[1];
	
		//Create Web Driver Instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Load Regular Price Product
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.getProductName();			
	
			String selectedSize = pdpPage.selectSize();
			Log.message(i++ + ". Size Selected :: '"+selectedSize+"'", driver);
	
			String selectedColor = pdpPage.selectColor();
			Log.message(i++ + ". Selected Color :: '"+selectedColor+"'");
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add to cart button");
	
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Clicked on 'X' in mini cart overlay");
	
			ShoppingBagPage shoppingBag = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on 'My Bag' icon in header and navigated to cart");
	
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("btnPaypal"), shoppingBag), 
					"Paypal button should be displayed in cart page!", 
					"Paypal button is displayed in cart page",
					"Paypal button is not displayed in cart page", driver);
	
			if(runPltfrm.equals("mobile") || runPltfrm.equals("tablet")){
				Log.message(++i + "Browser limitation for Payapal in mobile and tablet");
			}
			else{
	
				PaypalPage paypalPage = shoppingBag.clickOnPaypalButton();
				Log.message(i++ + ". Clicked on Paypal button in shopping bag page");
	
				PaypalConfirmationPage pcp=paypalPage.enterPayapalCredentials(paypalemail,paypalpassword);
	
				CheckoutPage checkOutPage =pcp.clickContinueConfirmation(); 
	
				Log.softAssertThat(checkOutPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkOutPage), 
						"Checkout page should displayed!", 
						"Checkout page is displayed",
						"Checkout page is not displayed", driver);
	
				Log.softAssertThat(checkOutPage.elementLayer.verifyPageElements(Arrays.asList("paypalEmail"), checkOutPage), 
						"Paypal email address should be displayed !", 
						"Paypal email address is displayed ",
						"Paypal email address is not displayed ", driver);
	
				Log.softAssertThat(checkOutPage.elementLayer.verifyPageElements(Arrays.asList("paypalShippinAddressSection"), checkOutPage), 
						"Shipping address section should be displayed !", 
						"Shipping address section is displayed ",
						"Shipping address section is not displayed ", driver);
			}
	
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			//driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C22549
}// search
