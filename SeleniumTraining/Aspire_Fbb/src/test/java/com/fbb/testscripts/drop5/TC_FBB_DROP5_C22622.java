package com.fbb.testscripts.drop5;

import java.util.Arrays;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.pages.headers.HamburgerMenu;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22622 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22622(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
		String username; 
		String password = accountData.get("password_global");
	
		HashMap<String, String> cardDetails1 = new HashMap<String, String>();
		String[] cardInfo_1 = checkoutData.get("card_Visa").split("\\|");
		cardDetails1.put("CardType", cardInfo_1[0]);
		cardDetails1.put("Name", cardInfo_1[1]);
		cardDetails1.put("Number", cardInfo_1[2]);
		cardDetails1.put("ExpMonth", cardInfo_1[3]);
		cardDetails1.put("ExpYear", cardInfo_1[4]);
		cardDetails1.put("MakeDefaultPayment", "No");
		cardDetails1.put("IsPLCC", "No");
	
		HashMap<String, String> cardDetails2 = new HashMap<String, String>();
		String[] cardInfo_2 = checkoutData.get("card_MasterCard").split("\\|");
		cardDetails2.put("CardType", cardInfo_2[0]);
		cardDetails2.put("Name", cardInfo_2[1]);
		cardDetails2.put("Number", cardInfo_2[2]);
		cardDetails2.put("ExpMonth", cardInfo_2[3]);
		cardDetails2.put("ExpYear", cardInfo_2[4]);
		cardDetails2.put("MakeDefaultPayment", "Yes");
		cardDetails2.put("IsPLCC", "No");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to My Account page.", driver);
	
			PaymentMethodsPage paymentMethods = myAcc.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to Payment Methods Page!", driver);
	
			paymentMethods.deleteCard(cardDetails1.get("Number"));
			paymentMethods.deleteCard(cardDetails2.get("Number"));
			Log.message(i++ + ". Deleted previously saved cards!", driver);
	
	
			//1 - Verify the functionality of Active/Hover state
			String valueToCheckCSS = prdData.get("highlight_color_2");
			Log.softAssertThat(paymentMethods.elementLayer.verifyCssPropertyForElement("lnkPaymentMethods", "color", valueToCheckCSS, paymentMethods),
					"The Payment Methods link should be highlighted",
					"The Payment Methods link is highlighted",
					"The Payment Methods link is not highlighted", driver);
	
			if(Utils.isMobile()) {
				Log.reference("To Do when site design has been updated following a CR");
				/*Log.softAssertThat(false, 
						"Payment Method Link should be displayed below the Email Preferences in Navigation Drawer", 
						"Payment Method Link is displayed below the Email Preferences in Navigation Drawer", 
						"Payment Method Link is not displayed below the Email Preferences in Navigation Drawer", driver);*/
			}
			else{
				Log.softAssertThat(!paymentMethods.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkPaymentMethods", "lnkCatalogPreferences", paymentMethods)
								&& !paymentMethods.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkCatalogPreferences", "lnkPaymentMethods", paymentMethods), 
						"Payment Method Link should be displayed next to Catalog Preferences in the Navigation Pane", 
						"Payment Method Link is displayed next to Catalog Preferences in the Navigation Pane", 
						"Payment Method Link is not displayed next to Catalog Preferences in the Navigation Pane", driver);
			}
	
			//2 - Verify the functionality of Payment Method
			Log.softAssertThat(paymentMethods.verifyPaymentMethodsTitle(),
					"The Payment Methods title should be displayed correctly",
					"The Payment Methods title is displayed correctly",
					"The Payment Methods title is not displayed correctly", driver);
	
			//3 - Verify the functionality of No Saved Cards Found label
			Log.softAssertThat(paymentMethods.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblNoSavedCardMessage"), paymentMethods),
					"The No Saved Cards message should be displayed",
					"The No Saved Cards message is displayed",
					"The No Saved Cards message is not displayed", driver);
	
			Log.softAssertThat(paymentMethods.verifyNoSavedCardsMessage(),
					"The message should be displayed correctly",
					"The message is displayed correctly",
					"The message is not displayed correctly", driver);
	
			//4 - Verify the functionality of Credit Card Information
			Log.softAssertThat(paymentMethods.verifyPlaceHolderText(),
					"The placeholder texts should be displayed correctly",
					"The placeholder texts are displayed correctly",
					"The placeholder texts are not displayed correctly", driver);
	
			//***Select Credit Card Type:
			Log.softAssertThat(paymentMethods.verifyOrderOfCardsDisplayed(),
					"Drop down should display the non-PLCC by card name first and then PLCC by card name",
					"The card types are displayed correctly",
					"The card types are not displayed correctly", driver);
	
			paymentMethods.savePaymentMethod();
			Log.message(i++ + ". Clicked save!", driver);
	
			Log.softAssertThat(paymentMethods.verifyErrorMessageDisplayedWhenFieldAreEmpty(),
					"All the fields must be mandatory and error message should be displayed when left empty",
					"The error message is displayed",
					"The error message is not displayed", driver);
	
			paymentMethods.selectCardType("Jessica London Platinum");
			Log.message(i++ + ". selected PLCC card!", driver);
	
			Log.softAssertThat(paymentMethods.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtCardNumber"), paymentMethods),
					"The Card Number field should be displayed",
					"The Card Number field is displayed",
					"The Card Number field is not displayed", driver);
	
			Log.message("As per comments in PXSFCC-2385, Name On Card field should not be displayed for PLCC card type");
			Log.softAssertThat(paymentMethods.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("txtNameOnCard"), paymentMethods),
					"The Name On Card field should not be displayed",
					"The Name On Card field is not displayed",
					"The Name On Card field is displayed", driver);
	
			Log.softAssertThat(paymentMethods.verifyExpiryMonthDropdownDisplayed()==false && paymentMethods.verifyExpiryYearDropdownDisplayed()==false,
					"The expiry month and year dropdown should not be displayed",
					"The expiry month and year dropdown is not displayed",
					"The expiry month and year dropdown is displayed", driver);
	
			paymentMethods.typeCardNumber("123456789123456789");
			Log.message(i++ + ". Typed card number more than 9 digits!", driver);
	
			Log.softAssertThat(paymentMethods.getCardNumberLength() == 9,
					"System should not allow the user to enter more than 9 digits for PLCC card",
					"Only 9 digits are displayed for PLCC card",
					"More than 9 digits are displayed for PLCC card", driver);
	
			paymentMethods.selectCardType("Visa");
			Log.message(i++ + ". selected Non-PLCC card!", driver);
	
			Log.softAssertThat(paymentMethods.verifyExpiryMonthDropdownDisplayed() && paymentMethods.verifyExpiryYearDropdownDisplayed()
					&& paymentMethods.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtNameOnCard", "txtCardNumber"), paymentMethods),
					"All the fields should be displayed",
					"All the fields are displayed",
					"All the fields are not displayed", driver);
	
			paymentMethods.typeName("1234#");
			Log.message(i++ + ". Typed invalid name!", driver);
	
			paymentMethods.typeCardNumber("abcd");
			Log.message(i++ + ". Typed invalid card number!", driver);
	
			paymentMethods.savePaymentMethod();
			Log.message(i++ + ". Clicked Save!", driver);
	
			Log.softAssertThat(paymentMethods.verifyInvalidEntryInNameOnCardField(),
					"Error should be displayed for invalid name",
					"Error is displayed for invalid name",
					"Error is not displayed for invalid name", driver);
	
			Log.softAssertThat(paymentMethods.verifyInvalidEntryInCardNumberField(),
					"Error should be displayed for invalid card number",
					"Error is displayed for invalid card number",
					"Error is not displayed for invalid card number", driver);
	
			paymentMethods.typeCardNumber("123456789123456789");
			Log.message(i++ + ". Typed card number more than 16 digits!", driver);
	
			Log.softAssertThat(paymentMethods.getCardNumberLength() == 16,
					"System should not allow the user to enters more than 16 digits",
					"Only 16 digits are displayed",
					"More than 16 digits are displayed", driver);
	
			Log.softAssertThat(paymentMethods.verifyExpiryMonthDropdownValues(),
					"The values must be displayed properly in Expiry month dropdown",
					"The values are displayed properly in Expiry month dropdown",
					"The values are not displayed properly in Expiry month dropdown", driver);
	
			Log.softAssertThat(paymentMethods.verifyExpiryYearDropdownValues(),
					"Years should be displayed as current year + 11 years top down.",
					"Years is displayed as current year + 11 years top down.",
					"Years is not displayed as current year + 11 years top down.", driver);
	
			//5 - Verify the functionality of Default Payment
			Log.softAssertThat(paymentMethods.isMakeDefaultPaymentChecked(),
					"Make this my default payment should be selected by default",
					"Make this my default payment is selected by default",
					"Make this my default payment is not selected by default", driver);
	
			//6 - Verify the functionality of Payment Methods Accepted
			Log.softAssertThat(paymentMethods.elementLayer.VerifyPageElementDisplayed(Arrays.asList("divPaymentMethodsAccepted"), paymentMethods),
					"The Payment Methods Accepted should be displayed",
					"The Payment Methods Accepted is displayed",
					"The Payment Methods Accepted is not displayed", driver);
	
			//7 - Verify the functionality of Save button
			//No default method
			paymentMethods.fillCardDetails(cardDetails1);
			Log.message(i++ + ". Added a card!", driver);
	
			paymentMethods.savePaymentMethod();
			Log.message(i++ + ". Saved the card!", driver);
	
			paymentMethods.clickAddNewCardLink();
			Log.message(i++ + ". Clicked on add new card!", driver);
	
			paymentMethods.fillCardDetails(cardDetails2);
			Log.message(i++ + ". Added another card as default payment method!", driver);
	
			paymentMethods.savePaymentMethod();
			Log.message(i++ + ". Saved the card!", driver);
	
			Log.message("<br>");
			Log.softAssertThat(paymentMethods.getDefaultCardType().toLowerCase().contains(cardDetails2.get("CardType").toLowerCase()),
					"The "+ cardDetails2.get("CardType") +" should be displayed as default payment method",
					"The "+ cardDetails2.get("CardType") +" is displayed as default payment method",
					"The "+ cardDetails2.get("CardType") +" is not displayed as default payment method", driver);
	
			ShoppingBagPage cartPage = paymentMethods.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			cartPage.removeAllItemsFromCart();
			Log.message(i++ + ". Removed All Items from the Cart.", driver);
	
			PdpPage pdpPage = cartPage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBag();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			CheckoutPage checkoutPage = (CheckoutPage)cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
	
			Log.softAssertThat(checkoutPage.getDefaultPaymentMethod().toLowerCase().contains(cardDetails2.get("CardType").toLowerCase()),
					"The "+ cardDetails2.get("CardType") +" should be displayed as default payment method",
					"The "+ cardDetails2.get("CardType") +" is displayed as default payment method",
					"The "+ cardDetails2.get("CardType") +" is not displayed as default payment method", driver);
	
			//Delete the payment methods
			homePage = checkoutPage.headers.chooseBrandFromHeader(Utils.getCurrentBrand());
			Log.message(i++ + ". Clicked on Brand icon on header", driver);
	
			if(!Utils.isMobile())
			{
				myAcc = homePage.headers.navigateToMyAccount();
				Log.message(i++ + ". Navigated to My Account page.", driver);
			}
			else 
			{
				HamburgerMenu hMenu = (HamburgerMenu) homePage.headers.openCloseHamburgerMenu("open");
				Log.message(i++ + ". Hamburger Menu Opened!", driver);
	
				myAcc = hMenu.navigateToMyAccount();
				Log.message(i++ + ". Navigated to My Account page!", driver);
	
				myAcc.clickOnOverViewLink();
				Log.message(i++ + ". Expanded OverView.", driver);
			}
	
	
			paymentMethods = myAcc.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to Payment Methods Page!", driver);
	
			paymentMethods.deleteCard(cardDetails1.get("Number"));
			paymentMethods.deleteCard(cardDetails2.get("Number"));
			Log.message(i++ + ". Deleted previously saved cards!", driver);
	
	
			//8 - Verify the functionality of Breadcrumb
			if(Utils.getRunPlatForm().equals("desktop") || Utils.getRunPlatForm().equals("tablet"))
			{
				Log.message("<br>");
				Log.softAssertThat(paymentMethods.verifyBreadCrumb(),
						"The breadcrumb should be displayed",
						"The breadcrumb is displayed",
						"The breadcrumb is not displayed", driver);
	
				myAcc = (MyAccountPage) paymentMethods.clickBreadCrumb("my account");
				Log.message(i++ + ". Clicked on My Account in bread crumb!", driver);
	
				paymentMethods = myAcc.navigateToPaymentMethods();
				Log.message(i++ + ". Navigated to Payments page!", driver);
	
				homePage = (HomePage) paymentMethods.clickBreadCrumb("home");
				Log.message(i++ + ". Clicked on Home in bread crumb!", driver);
	
			}
			else
			{
				Log.softAssertThat(paymentMethods.getMobileBreadCrumb().equalsIgnoreCase("My Account"),
						"The breadcrumb current element should be displayed",
						"The breadcrumb current element is displayed",
						"The breadcrumb current element is not displayed", driver);
	
				myAcc = paymentMethods.clickBackArrowInBreadcrumb();
				Log.message(i++ + ". Clicked on My Account in bread crumb!", driver);
			}
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP5_C22622

}// search
