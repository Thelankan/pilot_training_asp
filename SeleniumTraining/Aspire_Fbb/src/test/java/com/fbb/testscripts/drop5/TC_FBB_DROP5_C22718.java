package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22718 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22718(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String username = accountData.get("credential_plcc_global").split("\\|")[0];
		String password = accountData.get("credential_plcc_global").split("\\|")[1];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAccount = homePage.headers.signInToMyAccountPage(username, password); //Navigate to My Account Page
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			// Step 1: Verify the functionality of Rewards Tracker in PLCC
			if(!Utils.isMobile()) {
				Log.softAssertThat(myAccount.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblOverViewHeading", "rewardTrackerSectionActive", myAccount),
						"Rewards Tracker should be displayed below the Overview.",
						"Rewards Tracker is displayed below the Overview.",
						"Rewards Tracker is not displayed below the Overview.", driver);
			}else {
				Log.softAssertThat(myAccount.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkOverView", "rewardTrackerSectionActive", myAccount),
						"Rewards Tracker should be displayed below the Overview.",
						"Rewards Tracker is displayed below the Overview.",
						"Rewards Tracker is not displayed below the Overview.", driver);
			}
	
			//Step 2: Verify the functionality of Rewards Information in PLCC
			Log.softAssertThat(myAccount.checkCurrentSessionBrand(),
					"System should display the current brand of the user’s current session on page load.",
					"Current brand reward card is displayed first.",
					"Current brand reward card is not displayed first.", driver);
	
			Log.softAssertThat(myAccount.elementLayer.VerifyPageElementDisplayed(Arrays.asList("rewardTrackerActiveCardHolderName"), myAccount),
					"To check whether the reward card name is correct.",
					"The reward card name is correct!",
					"The reward card name is not correct", driver);
	
			Log.softAssertThat(myAccount.elementLayer.verifyVerticalAllignmentOfElements(driver, "rewardTrackerSectionActive", "rewardTrackerActiveCardName", myAccount),
					"Card Name should be displayed below the Rewards Tracker",
					"Card Name is displayed below the Rewards Tracker",
					"Card Name is not displayed below the Rewards Tracker", driver);
	
			Log.softAssertThat(myAccount.elementLayer.VerifyPageElementDisplayed(Arrays.asList("rewardTrackerActiveCardName"), myAccount),
					"To check whether the reward card holder name is correct.",
					"The reward card holder name is correct!",
					"The reward card holder name is not correct", driver);
	
			Log.softAssertThat(myAccount.elementLayer.verifyVerticalAllignmentOfElements(driver, "rewardTrackerActiveCardName", "rewardTrackerActiveCardHolderName", myAccount),
					"Account Cardholder Name should be displayed below the Card Name",
					"Account Cardholder Name is displayed below the Card Name",
					"Account Cardholder Name is not displayed below the Card Name", driver);
	
			Log.softAssertThat(myAccount.elementLayer.VerifyPageElementDisplayed(Arrays.asList("rewardTrackerActiveCardHolderAmount"), myAccount),
					"To check whether the reward card holder amount is correct.",
					"The reward card holder amount is correct!",
					"The reward card holder amount is not correct", driver);
	
			Log.softAssertThat(myAccount.elementLayer.verifyVerticalAllignmentOfElements(driver, "rewardTrackerActiveCardHolderName", "rewardTrackerActiveCardHolderAmount", myAccount),
					"Available credit should be displayed below the Account Cardholder Name",
					"Available credit is displayed below the Account Cardholder Name",
					"Available credit is not displayed below the Account Cardholder Name", driver);
	
			Log.softAssertThat(myAccount.elementLayer.VerifyPageElementDisplayed(Arrays.asList("rewardTrackerActiveCardRewards"), myAccount),
					"To check whether the reward card reward is correct.",
					"The reward card reward is correct!",
					"The reward card reward is not correct", driver);
	
			Log.softAssertThat(myAccount.verifyCreditAndRewardPlacement(),
					"Rewards points should be displayed right of Available Credit",
					"Rewards points is displayed right of Available Credit",
					"Rewards points is not displayed right of Available Credit", driver);
	
			Log.softAssertThat(myAccount.checkRewardSectionCardRewardsMsg(),
					"To check whether the reward card reward message is correct.",
					"The reward card reward message is correct!",
					"The reward card reward message is not correct", driver);
	
			//Step-3: Verify the functionality of Carousel in PLCC
			if(myAccount.getNoOfRewardCards() > 1) {
				Log.softAssertThat(myAccount.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkRewardSectionSlickDot",
						"lnkRewardSectionPrevArrow","lnkRewardSectionNextArrow"), myAccount),
						"To check Arrow and Carousel displayed when more than one cards is displaying in reward section.",
						"Arrow and Carousel displayed when more than one cards is displaying in reward section",
						"Arrow and Carousel displayed when more than one cards is not displaying in reward section", driver);
	
				Log.softAssertThat(myAccount.getNoOfRewardCards() == myAccount.getNoOfRewardSlickDots(),
						"To check Carousel displayed for all the cards seperately.",
						"Carousel displayed for all the cards seperately",
						"Carousel is not displaying for all the cards seperately", driver);
	
				Log.softAssertThat(myAccount.elementLayer.verifyPageElements(Arrays.asList("lnkRewardSectionPrevArrow", "lnkRewardSectionNextArrow", "lstRewardSlickDot"), myAccount),
						"To check reward tracker is not displaying slickdot and arrows when there more than one card.",
						"Reward tracker is displaying slickdot and arrows when there more than one card",
						"Reward tracker is not displaying slickdot and arrows when there more than one card", driver);
	
				Log.softAssertThat(myAccount.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkRewardSectionNextArrow", "lnkRewardSectionPrevArrow", myAccount),
						"To check Next arrow is displaying right side of the previous arrow.",
						"The Next arrow is displaying right side of the previous arrow",
						"The Next arrow is not displaying right side of the previous arrow", driver);
	
				Log.softAssertThat(myAccount.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkRewardSectionNextArrow", "lstRewardSlickDot", myAccount),
						"To check Next arrow is displaying right side of the slick dot.",
						"The Next arrow is displaying right side of the slick dot",
						"The Next arrow is not displaying right side of the slick dot", driver);
	
				Log.softAssertThat(myAccount.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lstRewardSlickDot", "lnkRewardSectionPrevArrow", myAccount),
						"To check previous arrow is displaying left side of the slick dot.",
						"The previous arrow is displaying left side of the slick dot",
						"The previous arrow is not displaying left side of the slick dot", driver);
	
				Log.softAssertThat(myAccount.checkRewardSectionPrevAndNextArrowIsNavigatable(),
						"To check Carousel navigated based on previous and next arrow.",
						"Carousel navigated based on previous and next arrow",
						"Carousel is not navigated based on previous and next arrow", driver);
	
				Log.softAssertThat(myAccount.checkRewardSelectedBasedOnSlickDot(),
						"To check reward card are selected based on slick dot.",
						"Reward card are selected based on slick dot",
						"Reward card are not selected based on slick dot", driver);
	
				Log.softAssertThat(myAccount.checkTheRewardTrackerIsSelectedInContinousLoop(),
						"To check reward tracker is selected in continous loop.",
						"Reward tracker is selected in continous loop",
						"Reward tracker is not selected in continous loop", driver);
			} else {
				Log.softAssertThat(myAccount.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lnkRewardSectionPrevArrow", "lnkRewardSectionNextArrow", "lstRewardSlickDot"), myAccount),
						"To check reward tracker is not displaying slickdot and arrows when there is only one cards.",
						"Reward tracker is not displaying slickdot and arrows when there is only one cards",
						"Reward tracker is displaying slickdot and arrows when there is only one cards", driver);
	
				Log.message("The cards are not enough to test the carousel");
			}
	
			//Step-4: Verify the functionality of Manage Your Card in PLCC 
			myAccount.clickOnManageYourCardLink();
			Log.softAssertThat(myAccount.checkManageYourCardTab(),
					"To check whether the url navigated for 'Manage Your Card' link is correct.",
					"The url navigated for 'Manage Your Card' link is correct!",
					"The url navigated for 'Manage Your Card' link is not correct", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_FBB_DROP4_C22718

}// search
