package com.fbb.testscripts.drop2;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19595 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "critical", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19595(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		//Load test data
		String globalNavigation = prdData.get("level-2");
		String level1 = globalNavigation.split("\\|")[0].trim();
		String level2 = globalNavigation.split("\\|")[1].trim();
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			PlpPage plp = headers.navigateTo(level1, level2);
			Log.message(i++ + ". Navigated to " + level2 + " PLP", driver);
			
			//Step-1: Verify the number of products displayed in Product Listing Page on initial load
			Log.softAssertThat(plp.getProductTileCount() <= 60, 
					"The Product Listing Page should show only up to 60 products on initial load", 
					"The Product Listing Page shows less than or 60 products on initial load", 
					"The Product Listing Page shows more than 60 products on initial load", driver);
			
			//Step-2: Verify the display of product tiles per row in Product Listing Page 
			Log.softAssertThat(plp.elementLayer.verifyVerticalAllignmentOfElements(driver, "divCurrentBreadCrumbMobile", "divContentSlotHeader", plp), 
					"Breadcrumb should be displayed above slot cat-banner.", 
					"Breadcrumb is displayed above slot cat-banner.", 
					"Breadcrumb is not displayed above slot cat-banner.", driver);
			
			Log.softAssertThat(plp.elementLayer.verifyAttributeForElement("categoryBannerText", "innerHTML", level2, plp), 
					"Banner should be displayed with the corresponding category information ", 
					"Banner is displayed with the corresponding category information ", 
					"Banner is not displayed with the corresponding category information ", driver);
			
			Log.softAssertThat(plp.elementLayer.verifyVerticalAllignmentOfElements(driver, "categoryBannerText", "divHorizontalRefinement", plp), 
					"Horizontal refinement bar and Sorting Options should be shown below Slot ID cat-banner.", 
					"Horizontal refinement bar and Sorting Options is shown below Slot ID cat-banner.", 
					"Horizontal refinement bar and Sorting Options is not shown below Slot ID cat-banner.", driver);
			
			Log.softAssertThat(plp.getNumberOfProductTilesPerRow() == 2, 
					"Only 2 product tiles should be displayed per row.", 
					"Only 2 product tiles are displayed per row.", 
					"2 product tiles are not displayed per row.", driver);
			
			//Step-3: Verify the display of Breadcrumb in the Product Listing Page
			Log.softAssertThat(plp.elementLayer.verifyVerticalAllignmentOfElements(driver, "divCurrentBreadCrumbMobile", "categoryBannerText", plp), 
					"Alignment of breadcrumb should be displayed above the content slot", 
					"Alignment of breadcrumb is displayed above the content slot", 
					"Alignment of breadcrumb is not displayed above the content slot", driver);
			
			Log.softAssertThat(headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkArrowBreadCrumb_mobile"), headers), 
					"Brand category root should be displayed as '<'.", 
					"Brand category root is displayed as '<'.", 
					"Brand category root is not displayed as '<'.", driver);
			
			//Step-4: Verify the Content Slot | Slot ID cat-banner
			Log.softAssertThat(plp.elementLayer.verifyVerticalAllignmentOfElements(driver, "categoryBannerText", "divHorizontalRefinement", plp), 
					"Horizontal refinement bar should be displayed below the Slot ID cat-banner.", 
					"Horizontal refinement bar is displayed below the Slot ID cat-banner.", 
					"Horizontal refinement bar is not displayed below the Slot ID cat-banner.", driver);
			
			//Step-5: Verify the horizontal refinement bar
			BrowserActions.scrollToTopOfPage(driver);
			Log.softAssertThat(plp.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "categoryBannerText", "divHorizontalRefinement", plp), 
					"Horizontal refinement bar should be displayed below the Slot ID cat-banner.", 
					"Horizontal refinement bar is displayed below the Slot ID cat-banner.", 
					"Horizontal refinement bar is not displayed below the Slot ID cat-banner.", driver);
			
			//Step-6: Verify the Product count
			BrowserActions.scrollToTopOfPage(driver);
			Log.softAssertThat(plp.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divHorizontalRefinement", "lblResultCountTablet", plp), 
					"Product count should be displayed below the horizontal refinement bar.", 
					"Product count is displayed below the horizontal refinement bar.", 
					"Product count is not displayed below the horizontal refinement bar.", driver);
			
			//Step-7, 8, 9
			Log.message("<br>"+ i++ +". Steps 7, 8, 9 are ad placements configured in BM.");
			
			//Step-10: Verify Back to Top button
			BrowserActions.scrollToBottomOfPage(driver);
			Log.softAssertThat(Integer.parseInt(plp.elementLayer.getElementCSSValue("btnBackToTop", "right", plp).replaceAll("[^0-9]", "")) >=
							Integer.parseInt(plp.elementLayer.getElementCSSValue("divProductTileContainer", "padding-right", plp).replaceAll("[^0-9]", "")), 
					"Back to Top button should be displayed at right side of the product grid.", 
					"Back to Top button is displayed at right side of the product grid.", 
					"Back to Top button is not displayed at right side of the product grid.", driver);
			
			plp.clickBackToTopButton();
			Log.message(i++ + ". Clicked on the Back To Top Button", driver);
			Log.softAssertThat(plp.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("btnBackToTop"), plp),
					"Back To Top button should not be visible.",
					"Back To Top button is not visible.", 
					"Back To Top button is visible.", driver);
			
			//Step-11: Verify Product tile in product list page
			Log.softAssertThat(plp.verifyProductTileContent(), 
					"Product tiles should load with correct elements.", 
					"Product tiles loaded with correct elements.", 
					"Product tiles didn't load with correct elements.", driver);
			
			//Step-12: Verify click on color swatches
			Log.softAssertThat(plp.verifyProductImageUpdate(), 
					"Main image should update based on the selected color.", 
					"Main image updated based on the selected color.", 
					"Main image did not update based on the selected color.", driver);
			
			//Step-13: Verify click on product main image or product name
			plp.clickProductByIndex(1);
			Utils.waitForPageLoad(driver);
			Log.softAssertThat(new PdpPage(driver).get().getPageLoadStatus(), 
					"System should redirect user to the PDP page.", 
					"System redirected user to the PDP page.", 
					"System did not redirect user to the PDP page.", driver);
			driver.navigate().back();
			
			//Step-14: Verify "View More" button in Product list page
			if(plp.getProductTileCount() == 60) {
				Log.softAssertThat(plp.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionSearchResults", "btnViewMoreButton", plp), 
						"\"View More\" button should be displayed below the product grid", 
						"\"View More\" button is displayed below the product grid", 
						"\"View More\" button is not displayed below the product grid", driver);
			}
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M1_FBB_DROP2_C19595

}
