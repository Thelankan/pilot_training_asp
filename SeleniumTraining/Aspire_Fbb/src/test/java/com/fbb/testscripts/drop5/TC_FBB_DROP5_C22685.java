package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22685 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22685(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String username;
		String password = accountData.get("password_global");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
	
		//Prerequisite: User should have saved address in account
		{
			GlobalNavigation.registerNewUser(driver, 3, 0, username + "|" + password);
		}
	
		int i = 1;
		try {	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAccount = homePage.headers.signInToMyAccountPage(username, password); //Navigate to My Account Page
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			myAccount.expandCollapseOverview("open");
	
			String HighlightColor = prdData.get("highlight_color_2");
			String BackgroundColor = "245, 245, 245";
	
			//Step 2: Verify the functionality of Active State
			AddressesPage addressesPg = myAccount.clickOnAddressLink();
			if (!Utils.isMobile()) {
				Log.softAssertThat(myAccount.elementLayer.verifyCssPropertyForElement("lnkAddresses", "color", HighlightColor, myAccount),
						"To check 'Addresses' link is highlighted.",
						"'Addresses' link is highlighted!",
						"'Addresses' link is not highlighted", driver);
				
			}
			else {
				/*Log.softAssertThat(myAccount.elementLayer.verifyElementTextEqualTo("lblOverView", "overview", myAccount),
						"To check 'Addresses' link is highlighted in navigatin dropdown.",
						"'Addresses' link is highlighted in navigatin dropdown!",
						"'Addresses' link is not highlighted in navigatin dropdown", driver);*/
				Log.reference("Navigation drawer currently not in build. Step disabled.");
			}
			addressesPg.deleteAllTheAddresses();
	
			//Step 13: Verify the functionality of Breadcrumb
			if (Utils.isMobile()) {
				Log.softAssertThat(addressesPg.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "promoContent", "breadCrumbCurrentMobile", addressesPg),
						"Breadcrumb should be displayed below the promotion content.",
						"Breadcrumb is displayed below the promotion content.",
						"Breadcrumb is not displayed below the promotion content.", driver);
				
				Log.softAssertThat(addressesPg.headers.getBreadCrumbText().equals("MY ACCOUNT"),
						"Breadcrumb should be displayed as '< My Account'",
						"Breadcrumb is displayed as '< My Account'",
						"Breadcrumb is not displayed as '< My Account'", driver);
			} 
			else {
				Log.softAssertThat(addressesPg.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "promoContent", "breadCrumbCurrent", addressesPg),
						"Breadcrumb should be displayed below the promotion content.",
						"Breadcrumb is displayed below the promotion content.",
						"Breadcrumb is not displayed below the promotion content.", driver);
				
				Log.softAssertThat(addressesPg.headers.getBreadCrumbText().equals("HOME MY ACCOUNT ADDRESSES"), 
						"Breadcrumb should be displayed as Home/My Account/Addresses", 
						"Breadcrumb is displayed as Home/My Account/Addresses", 
						"Breadcrumb is not displayed as Home/My Account/Addresses", driver);
	
				myAccount.clickOnBreadCrumb(2);
				Log.softAssertThat(driver.getCurrentUrl().toLowerCase().contains("account"),
						"To check 'Addresses' breadcrumb is displaying correctly.",
						"'Addresses' breadcrumb is displaying correctly!",
						"'Addresses' breadcrumb is not displaying correctly", driver);
				myAccount.clickOnAddressLink();
			}
			//Step 13 partially ends
	
			//Step 1: Verify the design pattern of the address sequence
			boolean status = false;
			String DefaultAddNickName = null;
			for (int j=1; j<=3; j++) {
				if(j==3) {
					status = true;
				}
				Utils.waitForPageLoad(driver);
				addressesPg.fillingAddressDetails("CreateUserAddress1", status);
				Utils.waitForPageLoad(driver);
				addressesPg.enterAddressTitle("Hello" + Integer.toString(j));
				Utils.waitForPageLoad(driver);
				addressesPg.clickSaveAddress();
				Utils.waitForPageLoad(driver);
				DefaultAddNickName = "Hello" + Integer.toString(j);
				Utils.waitForPageLoad(driver);
				if(j!=3) {
					Utils.waitForPageLoad(driver);
					addressesPg.clickOnAddNewAddress();
				}
	
			}
			if(!Utils.isMobile()) {
				Log.softAssertThat(addressesPg.verifyAddressAlignment("Hello3","Hello2","Hello1"),
						"Addresses should be arranged in a Z pattern.",
						"Addresses are arranged in a Z pattern",
						"Addresses are not arranged in a Z pattern", driver);
			}
	
			//Step 3: Verify the functionality of Default Address Label
			Log.softAssertThat(addressesPg.getDefaultAddressNickName().equals(DefaultAddNickName),
					"To check Default address is selected if we give Make default checkbox true.",
					"Default address is selected if we give Make default checkbox true",
					"Default address is not selected based on default checkbox true", driver);
	
			if(Utils.isMobile()) {
				Log.softAssertThat(addressesPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblAddressesHeading", "addressDefaultIndicator", addressesPg), 
						"Default Address Label should be displayed below the Addresses Heading", 
						"Default Address Label is displayed below the Addresses Heading", 
						"Default Address Label is not displayed below the Addresses Heading", driver);
			}
			else {
				Log.softAssertThat(addressesPg.elementLayer.verifyHorizontalAllignmentOfElements(driver, "addressDefaultIndicator", "lblAddressesHeading", addressesPg), 
						"Default Address Label should be displayed right of Addresses Heading", 
						"Default Address Label is displayed right of Addresses Heading", 
						"Default Address Label is not displayed right of Addresses Heading", driver);
			}
	
			Log.softAssertThat(addressesPg.verifyDefaultAddressLocatedFirst(), 
					"Default address should be sequenced first in the address book.", 
					"Default address is sequenced first in the address book.", 
					"Default address is not sequenced first in the address book.", driver);
	
			Log.softAssertThat(addressesPg.verifyDefaultAddressTextIsComingFromProperty(),
					"To check whether the 'Address default indicator' button coming from property.",
					"The 'Address default indicator' button is coming from property!",
					"The 'Address default indicator' button is not coming from property", driver);
	
			if(addressesPg.getNoOfAddresses()!=0) {
				//Step 4: Verify the functionality of Default Address Icon
				Log.softAssertThat(addressesPg.elementLayer.verifyCssPropertyForElement("txtDefaultNickName", "background-image", "Dot_Green", addressesPg), 
						"Default Address Icon should be displayed left of Default Address Label as a green dot. ", 
						"Default Address Icon is displayed as a green dot. ", 
						"Default Address Icon is not displayed as a green dot. ", driver);
	
				//Step 6: Verify the functionality of Default Address Indicator
				Log.softAssertThat(addressesPg.verifyDefaultAddressIndicatorPosition(),
						"To check whether the Address default indicator is in top left spot.",
						"The Address default indicator is in top left spot!",
						"The Address default indicator is not in top left spot", driver);
			}else {
				Log.message("No address is saved to show Default indicator.");
			}
	
			//Step 7: Verify the functionality of Edit in Default Address Options/Address Card Options
			addressesPg.clickAddressEdit(1);
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("inputAddressRequired"), addressesPg),
					"To check 'Edit Address' is displayed.",
					"'Edit Address' is displayed!",
					"'Edit Address' is not displayed", driver);
			addressesPg.clickCancelAddressCreation();
	
			//Step 8: Verify the functionality of Delete Default Address Options
			addressesPg.clickDeleteAddress(1);
			Log.softAssertThat(addressesPg.clickDeleteCancelAddress(1),
					"To check 'Delete Address' is displayed.",
					"'Delete Address' is displayed!",
					"'Delete Address' is not displayed", driver);
	
			//Step 9: Verify the functionality of Make Default in Non-Default Address Options/Address Card Options 
			String nickName = addressesPg.getNickNameOfDefaultAddress();
			addressesPg.clickMakeDefaultAddr(2);
			Log.softAssertThat(!(nickName.equals(addressesPg.getNickNameOfDefaultAddress())),
					"To check clicking on 'Make default' make the address default.",
					"Clicking on 'Make default' make the address default!",
					"Clicking on 'Make default' is not making the address default", driver);
	
			//Step 10: Verify the functionality of Edit in Non-Default Address Options
			addressesPg.clickAddressEdit(2);
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("inputAddressRequired"), addressesPg),
					"To check 'Edit Address' is displayed.",
					"'Edit Address' is displayed!",
					"'Edit Address' is not displayed", driver);
			addressesPg.clickCancelAddressCreation();
	
			//Step 11: Verify the functionality of Delete in Non-Default Address Options
			addressesPg.clickDeleteAddress(2);
			Log.softAssertThat(addressesPg.clickDeleteCancelAddress(2),
					"To check 'Delete Address' is displayed.",
					"'Delete Address' is displayed!",
					"'Delete Address' is not displayed", driver);
	
			//Step 12: Verify the functionality of Add New Address
			Log.softAssertThat(addressesPg.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnAddNewAddress", "addressDefaultIndicator", addressesPg), 
					"Add New Address should be displayed on right corner of the Default Address Label", 
					"Add New Address is displayed on right corner of the Default Address Label", 
					"Add New Address is not displayed on right corner of the Default Address Label", driver);
	
			if((addressesPg.getNoOfAddresses() > 0)) {
				addressesPg.clickOnAddNewAddress();
	
				Log.softAssertThat(addressesPg.verifyAddNewAddressTextIsComingFromProperty(),
						"To check whether the 'Add New Address' button coming from property.",
						"The 'Add New Address' button is coming from property!",
						"The 'Add New Address' button is not coming from property", driver);
	
				Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("inputAddressRequired"), addressesPg),
						"To check 'Add New module' is displayed.",
						"'Add New module' is displayed!",
						"'Add New module' is not displayed", driver);
	
				addressesPg.clickCancelAddressCreation();
			} else {
				Log.failsoft("There are no Addresses created already to verify 'Add New Address' button");
			}
	
			Log.reference("Step 12c is refered in C22687");
	
			//Step 5: Verify the functionality of Hover State - Desktop only
			if(Utils.isDesktop()) {
				Log.softAssertThat(addressesPg.getOnHoverColorOfAddressSection(addressesPg.getNoOfAddresses()).contains(BackgroundColor),
						"To check 'Address' section is greyed on hover.",
						"'Address' section is greyed on hover!",
						"'Address' section is not greyed on hover", driver);
			}
	
			//Step 13: Verify the functionality of Breadcrumb cntd.
			addressesPg.deleteAllTheAddresses();
			if((addressesPg.getNoOfAddresses() == 0)) {
				Log.softAssertThat(addressesPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnAddNewAddress"), addressesPg),
						"To check 'Add New address' is displayed or not.",
						"'Add New address' is not displayed!",
						"'Add New address' is displayed even there are some stored address", driver);
			}
			if (!Utils.isMobile()) {
				myAccount.clickOnBreadCrumb(1);
				Log.softAssertThat(new HomePage(driver).get().getPageLoadStatus(),
						"To check 'Home' page is displayed or not.",
						"'Home page' is displayed!",
						"'Home page' is not displayed", driver);
			}
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_FBB_DROP4_C22685

}// search
