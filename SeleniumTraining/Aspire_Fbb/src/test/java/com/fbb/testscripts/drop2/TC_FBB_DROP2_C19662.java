package com.fbb.testscripts.drop2;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19662 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	@Test(groups = { "medium", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19662(String browser) throws Exception {
		Log.testCaseInfo();
	
		String level1 = prdData.get("level-4_1").split("\\|")[0];
		String level2 = prdData.get("level-4_1").split("\\|")[1];
		String level3 = prdData.get("level-4_1").split("\\|")[2];
		String level4 = prdData.get("level-4_1").split("\\|")[3];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PlpPage plpPage = homePage.headers.navigateTo(level1, level2, level3);
			Log.message(i++ + ". Navigated to Product listing Page!", driver);
	
			Log.softAssertThat(plpPage.refinements.getActiveLevel1CategoryName().equalsIgnoreCase(level1),
					"The Level1 Category should be active.",
					"The Level1 Category is active!",
					"The Level1 Category is not active.", driver);
			
			//Step-1: Verify the Vertical Refinement is displayed in the left side of the Product list page
			Log.softAssertThat(plpPage.verifyVerticalRefinementDisplayedToTheLeftOfSearchResults(),
					"The Verftical Refinement should be displayed to the left of Search Results!",
					"The Verftical Refinement is displayed to the left of Search Results!",
					"The Verftical Refinement is not displayed to the left of Search Results!", driver);
	
			Log.softAssertThat(plpPage.refinements.elementLayer.VerifyElementDisplayed(Arrays.asList("level2Section"), plpPage.refinements),
					"The Level2 Category should be expanded and active by default!",
					"The Level2 Category is expanded and active by default!",
					"The Level2 Category is not expanded and active by default!", driver);
	
			//Step-2: Verify the categories in Vertical Refinements
			Log.softAssertThat(plpPage.refinements.verifyLevel2PlacementOnLeft(level2), 
					"L2 Category Name should be displayed in Vertical refinement.", 
					"L2 Category Name is displayed in Vertical refinement.", 
					"L2 Category Name is not displayed in Vertical refinement.", driver);
			
			Log.softAssertThat(plpPage.refinements.verifyLevel3PlacementOnLeft(level3), 
					"L3 Category Name should be displayed in Vertical refinement.", 
					"L3 Category Name is displayed in Vertical refinement.", 
					"L3 Category Name is not displayed in Vertical refinement.", driver);
			
			Log.softAssertThat(plpPage.refinements.verifyLevel3Selected(level3), 
					"L3 Category Name should be selected.", 
					"L3 Category Name is selected.", 
					"L3 Category Name is not selected.", driver);
			
			Log.softAssertThat(plpPage.refinements.verifyLevel4PlacementOnLeft(level4), 
					"L4 Category Name should be displayed in Vertical refinement.", 
					"L4 Category Name is displayed in Vertical refinement.", 
					"L4 Category Name is not displayed in Vertical refinement.", driver);

			
			//Step-3: Verify the behavior of the L2 Category Name
			Log.softAssertThat(plpPage.verifyIfLevel3Displayed(),
					"The Level2 Category should be expanded!",
					"The Level2 Category is expanded!",
					"The Level2 Category is not expanded!", driver);
	
			//Step-4: Verify the behavior of the L3 Category Name
			Log.softAssertThat(plpPage.refinements.verifyLevel3CategoryDisplayedBelowLevel2Category(),
					"The Level3 Category should be dispalyed below the Level2 Category!",
					"The Level3 Category is dispalyed below the Level2 Category!",
					"The Level3 Category is not dispalyed below the Level2 Category!", driver);
			
			plpPage.refinements.clickLevel3Category(level3);
			Log.message(i++ + ". clicked on Level3 category!", driver);
	
			Log.softAssertThat(plpPage.refinements.isTheSelectedLevel3CategoryHighlighted(level3),
					"The Level3 Category should be highlighted and active!",
					"The Level3 Category is highlighted and active!",
					"The Level3 Category is not highlighted and active!", driver);
			
			//Step-7, 8:
			Log.message(i++ + ". Steps 7 & 8 are covered in Step-2.");
			
			//Step-5, 6: Verify the behavior of the L4 Category Name
			String testData= prdData.get("menu_navigation_2");
			level1 = testData.split("\\|")[0];
			level2 = testData.split("\\|")[1];
			plpPage = homePage.headers.navigateTo(level1, level2);
			
			testData= prdData.get("menu_navigation_3");
			level2 = testData.split("\\|")[1];
			level3 = testData.split("\\|")[2];
			level4 = testData.split("\\|")[3];
			
			plpPage.refinements.navigateLevel2Category(level2);
			Log.message(i++ + ". Navigated to Features", driver);
			plpPage.refinements.expandCollapseLevel3Category(level3, true);
			Log.message(i++ + ". Expanded Denim Bar", driver);
			
			Log.softAssertThat(plpPage.verifyIfLevel4Displayed(),
					"The Level3 Category should be expanded!",
					"The Level3 Category is expanded!",
					"The Level3 Category is not expanded!", driver);
	
			Log.softAssertThat(plpPage.refinements.verifyLevel4CategoryDisplayedBelowLevel3Category(level3, level4),
					"The Level4 Category should be dispalyed below the Level3 Category!",
					"The Level4 Category is dispalyed below the Level3 Category!",
					"The Level4 Category is not dispalyed below the Level3 Category!", driver);
			
			plpPage.refinements.clickLevel4Category(level4);
			Log.message(i++ + ". clicked on Level4 category!", driver);
	
			Log.softAssertThat(plpPage.refinements.isTheSelectedLevel4CategoryHighlighted(level4),
					"The Level4 Category should be highlighted and active!",
					"The Level4 Category is highlighted and active!",
					"The Level4 Category is not highlighted and active!", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP2_C19662


}// search


