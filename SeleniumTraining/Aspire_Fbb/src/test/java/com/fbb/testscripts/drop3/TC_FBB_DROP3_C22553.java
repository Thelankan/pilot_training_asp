package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C22553 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	@SuppressWarnings("unused")
	private static String runPlatfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C22553(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_size-chart");
		String sizeGuide1 = prdData.get("menu_size_guide").split("\\|")[0];
		String sizeGuide2 = prdData.get("menu_size_guide").split("\\|")[1];
		String sizeGuide3 = prdData.get("menu_size_guide").split("\\|")[2];
		String sizeGuideDrp = prdData.get("menu_size_guide_drp");
	
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.redirectToPDP(searchKey, driver);
			Log.message(i++ + ". Navigated to Pdp Page!", driver);
	
			//1
			pdpPage.clickSizeChartLink();
			Log.message(i++ + ". Clicked on Size Chart link!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("modalSizechart"), pdpPage),
					"The Size Chart modal should be displayed!",
					"The Size Chart modal is displayed!",
					"The Size Chart modal is not displayed!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblSizeChartTitle", "btnCloseSizeChartModal"), pdpPage),
					"The Size Chart modal title, close button should be displayed!",
					"The Size Chart modal title, close button is displayed!",
					"The Size Chart modal title, close button is not displayed!", driver);
	
			//2
			pdpPage.closeSizeChartModalPopup();
			Log.message(i++ + ". Clicked on close button on the Size Chart popup!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("modalSizechart"), pdpPage),
					"The Size Chart modal should be closed!",
					"The Size Chart modal is closed!",
					"The Size Chart modal is not closed!", driver);
	
			if(System.getProperty("runMode").equals("local")) {
				pdpPage.clickSizeChartLink();
				Log.message(i++ + ". Clicked on Size Chart link!", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("modalSizechart"), pdpPage),
						"The Size Chart modal should be displayed!",
						"The Size Chart modal is displayed!",
						"The Size Chart modal is not displayed!", driver);
	
				pdpPage.headers.clickOutSide("btnCloseSizeChartModal",pdpPage);
				Log.message(i++ + ". Clicked outside the Size Chart popup!", driver);
	
	
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("modalSizechart"), pdpPage),
						"The Size Chart modal should be closed!",
						"The Size Chart modal is closed!",
						"The Size Chart modal is not closed!", driver);
			}else {
				Log.reference("OutSide Modal Click not applicable for Sauce.");
			}
			//3
			pdpPage.clickSizeChartLink();
			Log.message(i++ + ". Clicked on Size Chart link!", driver);
	
			pdpPage.clickOnSizeGuide(sizeGuide1);
			Log.message(i++ + ". Clicked on a Size Guide :: " + sizeGuide1, driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("lstSizeGuideRows"), pdpPage),
					"The size guide chart should be displayed!",
					"The size guide chart is displayed!",
					"The size guide chart is not displayed!", driver);
			
			String color = null;
			if (Utils.getCurrentBrand().equals(Brand.rm)) {
				color = "rgb(51, 30, 84)";
			} else if (Utils.getCurrentBrand().equals(Brand.ww)) {
				color = "rgb(236, 1, 140)";
			} else if (Utils.getCurrentBrand().equals(Brand.ks)) {
				color = "rgb(0, 41, 83)";
			} else {
				color = "rgb(51, 30, 84)";
			}
			//4
			if (Utils.getCurrentBrand().equals(Brand.ww)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("selectedNavigationCategoryDesktop", "border-color", color, pdpPage),
						"The selected size guide link should be highlighted!",
						"The selected size guide link is highlighted!",
						"The selected size guide link is not highlighted!", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForPsuedoElement("selectedNavigationCategoryDesktop", "after", "border", color, pdpPage),
						"The selected size guide link should be highlighted!",
						"The selected size guide link is highlighted!",
						"The selected size guide link is not highlighted!", driver);
			}
			
			if(Utils.isDesktop())
			{
				Log.softAssertThat(pdpPage.verifyIfSizeGuideLinkHighlightedOnHover(sizeGuide2).contains(color),
						"The hovered size guide link should be highlighted!",
						"The hovered size guide link is highlighted!",
						"The hovered size guide link is not highlighted!", driver);
			}
			else
				Log.reference("Mouse hover not applicable for tablet. Hence skipping step-4");
	
			String beforeSelect = pdpPage.getSelectedGuideInSizeChartContainer();
			//5
			pdpPage.clickOnSizeGuide(sizeGuideDrp);
			Log.message(i++ + ". Clicked on a Size Guide :: " + sizeGuide3, driver);
	
			String afterSelect = pdpPage.getSelectedGuideInSizeChartContainer();
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("drpBrandMenu"), pdpPage),
					"The Brand menu dropdown should be displayed!",
					"The Brand menu dropdown is displayed!",
					"The Brand menu dropdown is not displayed!", driver);
	
			Log.softAssertThat(!beforeSelect.equals(afterSelect),
					"Check the user should be able to select different navigation to view the different size guide!",
					"The user is able to select different navigation to view the different size guide!",
					"The user is not able to select different navigation to view the different size guide!", driver);
	
			pdpPage.clickOnBrandMenuToggle("expand");
			Log.message(i++ + ". Brand Menu dropdown expanded!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("lstBrandOptions"), pdpPage),
					"The Brand menu dropdown options should be displayed!",
					"The Brand menu dropdown options are displayed!",
					"The Brand menu dropdown options are not displayed!", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP2_C22553


}// search
