package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.PasswordResetPage;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22737 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");
	private static EnvironmentPropertiesReader demandData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22737(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		// Test data
		String pwResetLink = redirectData.get("resetPassword");
		String expiredColor = demandData.get("expired_pw_reset_color");
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			driver.navigate().to(Utils.getWebSite() + pwResetLink);
			PasswordResetPage pwReset = new PasswordResetPage(driver).get();
			Log.message(i++ + ". Navigated to password reset link.", driver);
			
			Log.softAssertThat(pwReset.elementLayer.VerifyElementDisplayed(Arrays.asList("frmPasswordReset"), pwReset), 
					"System should navigate to the Forgot Password Request Page", 
					"System navigated to the Forgot Password Request Page", 
					"System did not navigate to the Forgot Password Request Page", driver);
			
			Log.softAssertThat(pwReset.elementLayer.VerifyElementDisplayed(Arrays.asList("breadcrumbMobile","pwdResetHeader","pwdResetSubHead","txtEmailId","btnSend","btnBackToLogin"), pwReset), 
					"System should display Breadcrumb, Header, Subhead(Expired copy), Email address, Send, Back to Login in Forgot Password Request Page", 
					"System shows correct components in Password Request Page", 
					"System does not show correct components in Password Request Page", driver);
			
			Log.softAssertThat(pwReset.elementLayer.verifyVerticalAllignmentOfElements(driver, "pwdResetHeader", "pwdResetSubHead", pwReset), 
					"Password reset subhead should be displayed below the header.", 
					"Password reset subhead is displayed below the header.", 
					"Password reset subhead is not displayed below the header.", driver);
			
			Log.softAssertThat(pwReset.elementLayer.verifyCssPropertyForElement("pwdResetSubHead", "color", expiredColor, pwReset), 
					"Password reset subhead should be displayed in red color.", 
					"Password reset subhead is displayed in red color.", 
					"Password reset subhead is not displayed in red color.", driver);
			
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M1_FBB_DROP5_C22737

}
