package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.GuestEmailPreferencesPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22707 extends BaseTest{
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");
	String runPltfrm = Utils.getRunPlatForm();
	
	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22707(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String username; // Guest Email
		String emailPlaceholder = demandwareData.get("emailPlaceholder");
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
	
			new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			driver.navigate().to(driver.getCurrentUrl() + redirectData.get("email_Unsubscribe"));
			Log.message(i++ + ". Navigated to Email Unsubscribe Page!", driver);
			GuestEmailPreferencesPage guestEmailPreference = new GuestEmailPreferencesPage(driver).get();
			
			//Step-1: Verify the display of available components in Email unsubscribe page
			Log.softAssertThat(guestEmailPreference.elementLayer.VerifyElementDisplayed(Arrays.asList("headerText", "subheadText", "emailAddressTxtField"), guestEmailPreference), 
					"Email Unsubscribe Page for unauthenticated user should be displayed with correct set of components.", 
					"Email Unsubscribe Page for unauthenticated user is displayed with correct set of components.", 
					"Email Unsubscribe Page for unauthenticated user is not displayed with correct set of components.", driver);
			
			Log.reference("Questions and Answers, \"divQuestionAnswerSection\", is currently diabled for this page in all environemnts. Removed from automation until enabled.");
			
			//Step-2: Verify the display of Breadcrumb
			if(!Utils.isMobile()) {
				Log.softAssertThat(guestEmailPreference.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divPromoBanner", "homeBreadcrum", guestEmailPreference), 
						"Breadcrumb should be displayed below the promotional content", 
						"Breadcrumb is displayed below the promotional content", 
						"Breadcrumb is not displayed below the promotional content", driver);
				
				Log.softAssertThat(guestEmailPreference.headers.getBreadCrumbText().equals("HOME EMAIL UNSUBSCRIBE"), 
						"Breadcrumb should be displayed as HOME / EMAIL UNSUBSCRIBE", 
						"Breadcrumb is displayed as HOME / EMAIL UNSUBSCRIBE", 
						"Breadcrumb is not displayed as HOME / EMAIL UNSUBSCRIBE", driver);
			}else {
				Log.softAssertThat(guestEmailPreference.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divPromoBanner", "homeBreadcrumbMobile", guestEmailPreference), 
						"Breadcrumb should be displayed below the promotional content", 
						"Breadcrumb is displayed below the promotional content", 
						"Breadcrumb is not displayed below the promotional content", driver);
				
				Log.softAssertThat(guestEmailPreference.headers.getBreadCrumbText().equals("HOME"), 
						"Breadcrumb should be displayed as HOME", 
						"Breadcrumb is displayed as HOME", 
						"Breadcrumb is not displayed as HOME", driver);
			}
			
			//Step-3: Verify the display of Email Unsubscribe header
			if(!Utils.isMobile()) {
				Log.softAssertThat(guestEmailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "homeBreadcrum", "headerText", guestEmailPreference), 
						"\"Unsubscribe\" header should be displayed below the Breadcrumb", 
						"\"Unsubscribe\" header is displayed below the Breadcrumb", 
						"\"Unsubscribe\" header is not displayed below the Breadcrumb", driver);
			}else {
				Log.softAssertThat(guestEmailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "homeBreadcrumbMobile", "headerText", guestEmailPreference), 
						"\"Unsubscribe\" header should be displayed below the Breadcrumb", 
						"\"Unsubscribe\" header is displayed below the Breadcrumb", 
						"\"Unsubscribe\" header is not displayed below the Breadcrumb", driver);
			}
			
			//Step-4: Verify the display of Sub Header
			Log.softAssertThat(guestEmailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "headerText", "subheadText", guestEmailPreference), 
					"Subhead should be displayed below the Email Unsubscribe header", 
					"Subhead is displayed below the Email Unsubscribe header", 
					"Subhead is not displayed below the Email Unsubscribe header", driver);
			
			//Step-5: Verify the display of Email Address Field
			Log.softAssertThat(guestEmailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "subheadText", "emailAddressTxtField", guestEmailPreference), 
					"Email Address Field should be displayed below the Subhead ", 
					"Email Address Field is displayed below the Subhead ", 
					"Email Address Field is not displayed below the Subhead ", driver);
			
			Log.softAssertThat(guestEmailPreference.elementLayer.verifyElementTextEqualTo("txtEmailPlaceHolder", emailPlaceholder, guestEmailPreference), 
					"Field label should display as 'Enter your email address'", 
					"Field label displays as 'Enter your email address'", 
					"Field label does not display as 'Enter your email address'", driver);
			
			Log.softAssertThat(guestEmailPreference.verifyPlaceHolder("normal"),
					"The placeholder text should be displayed correctly",
					"The placeholder text is displayed correctly",
					"The placeholder text is not displayed correctly", driver);
			
			Log.softAssertThat(guestEmailPreference.verifyEmailFieldEditable(),
					"The system should allow the user to enter text in the email address field.",
					"The system allows the user to enter text in the email address field.",
					"The system does not allow the user to enter text in the email address field.", driver);
			
			//Step-6: Verify the display of Submit
			if(Utils.isMobile()) {
				Log.softAssertThat(guestEmailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtEmail", "btnGuestEmailUnsubscribeSubmit", guestEmailPreference), 
						"Submit should be displayed below the Email Address", 
						"Submit is displayed below the Email Address", 
						"Submit is not displayed below the Email Address", driver);
			}else {
				Log.softAssertThat(guestEmailPreference.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnGuestEmailUnsubscribeSubmit", "txtEmail", guestEmailPreference), 
						"Submit should be displayed right at the Email Address", 
						"Submit is displayed right at the Email Address", 
						"Submit is not displayed right at the Email Address", driver);
			}
			
			guestEmailPreference.typeEmail(username.split("@")[0]);
			Log.message(i++ + ". Typed invalid email!", driver);
			guestEmailPreference.clickUnsubscribeSubmit();
			Log.message(i++ + ". Clicked submit!", driver);
	
			Log.softAssertThat(guestEmailPreference.verifyPlaceHolder("invalid"),
					"The placeholder text should be displayed correctly",
					"The placeholder text is displayed correctly",
					"The placeholder text is not displayed correctly", driver);
			
			guestEmailPreference.typeEmail("");
			Log.message(i++ + ". Email is empty!", driver);
			guestEmailPreference.clickUnsubscribeSubmit();
			Log.message(i++ + ". Clicked submit!", driver);
	
			Log.softAssertThat(guestEmailPreference.verifyPlaceHolder("blank"),
					"Email address field should not be left blank",
					"Email address field shows as manadatory if left blank.",
					"Email address field does not show as manadatory if left blank.", driver);
			
			//Step-7: Verify Unsubscribe Email Preference Modal
			guestEmailPreference.typeEmail(username);
			Log.message(i++ + ". Entered correct email.", driver);
			guestEmailPreference.clickUnsubscribeSubmit();
			Log.message(i++ + ". Clicked submit!", driver);
			
			Log.softAssertThat(guestEmailPreference.elementLayer.VerifyElementDisplayed(Arrays.asList("modalHeader", "modalCloseIcon", 
									"radioModalLessFrequent", "radioUnsubscribe"), guestEmailPreference), 
					"Correct components should be displayed for unsubscribing Email Preference Modal", 
					"Correct components are displayed for unsubscribing Email Preference Modal", 
					"Correct components are not displayed for unsubscribing Email Preference Modal", driver);
			
			//Step-8: Verify when user selects "Please email me less frequently" option from the radio button
			Log.softAssertThat(guestEmailPreference.elementLayer.verifyCssPropertyForElement("radioModalLessFrequent", "background-image", "circle-selected", guestEmailPreference)
						|| (guestEmailPreference.elementLayer.verifyCssPropertyForElement("radioModalLessFrequent", "background-image", "radio-button-sprite", guestEmailPreference) && guestEmailPreference.elementLayer.verifyCssPropertyForElement("radioModalLessFrequent", "background-position", "-", guestEmailPreference)), 
					"\"Please email me less frequently\" - option should be selected by default.", 
					"\"Please email me less frequently\" - option is selected by default.", 
					"\"Please email me less frequently\" - option is not selected by default.", driver);
			
			{
				//Step-9: Verify when user selects "unsubscribe" option from the radio button
				guestEmailPreference.clickRadioUnSub();
				Log.message(i++ + ". Clicked Unsub radio option",  driver);
				Log.softAssertThat(guestEmailPreference.elementLayer.VerifyElementDisplayed(Arrays.asList("divUnsubscibeReasons"), guestEmailPreference), 
						"the system should expand the modal and display the reasons for \"unsubscribe\"", 
						"the system expanded the modal and displayed the reasons for \"unsubscribe\"", 
						"the system did not expand the modal and display the reasons for \"unsubscribe\"", driver);
				
				guestEmailPreference.clickRadioFewerEmail();
				Log.message(i++ + ". Clicked fewer menu radio options",  driver);
				Log.softAssertThat(guestEmailPreference.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divUnsubscibeReasons"), guestEmailPreference), 
						"The reasons to unsubscribe should become hidden.", 
						"The reasons to unsubscribe is hidden.", 
						"The reasons to unsubscribe is not hidden.", driver);
			}
			
			guestEmailPreference.clickModalClose();
			Log.message(i++ + ". Clicked close button on modal", driver);
			
			Log.softAssertThat(guestEmailPreference.elementLayer.verifyElementTextEqualTo("txtEmail", username, guestEmailPreference), 
					"If user closes modal, the unsubscribe page should display with email field pre-populated.", 
					"The unsubscribe page is displayed with email field pre-populated.", 
					"The unsubscribe page is not displayed with email field pre-populated..", driver);
			
			guestEmailPreference.clickUnsubscribeSubmit();
			Log.message(i++ + ". Clicked submit!", driver);
			guestEmailPreference.clickModalSubmit();
			Log.message(i++ + ". Clicked submit on modal", driver);
			
			//Step-10: Unauthenticated Email Preference Confirmation message
			Log.softAssertThat(guestEmailPreference.elementLayer.VerifyElementDisplayed(Arrays.asList("lblEmailPreferencesSubHeading"), guestEmailPreference), 
					"Modal should be closed and display a confirmation message.", 
					"Modal should be closed and display a confirmation message.", 
					"Modal should be closed and display a confirmation message.", driver);
			
			//Step-11: Verify the display of Questions and Answers
			Log.softAssertThat(guestEmailPreference.verifyQuestionAnswersClosed(), 
					"System should display the Question and Answer section in collapsed state by default", 
					"System displayed the Question and Answer section in collapsed state by default", 
					"System did not display the Question and Answer section in collapsed state by default", driver);
			
			Log.softAssertThat(guestEmailPreference.verifyQASectionCaretSymbol(guestEmailPreference), 
					"Caret symbol should be available on the right corner for each Question", 
					"Caret symbol is available on the right corner for each Question", 
					"Caret symbol is not available on the right corner for each Question", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP5_C22707

}
