package com.fbb.testscripts.drop6;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.customerservice.ContactUsPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.customerservice.FaqPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP6_C22774 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	private static EnvironmentPropertiesReader demandwareProperty = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP6_C22774(String browser) throws Exception{
		//Get the webdriver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		//Test data
		String csHeaderLabel = demandwareProperty.get("customer_service_header_label");
		Log.testCaseInfo();
		int i=1;
		try
		{
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			if(!Utils.isMobile()) {
				homePage.headers.mouseOverCustomerService();
			}
			CustomerService customerService = homePage.headers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Customer Service Page!", driver);
			
			//Step-1: Verify the functionality of the Breadcrumb
			Log.softAssertThat(customerService.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "txtPromoBanner", "sectionBreadcrum", customerService), 
					"Breadcrumb should be displayed below the promotional content", 
					"Breadcrumb is display below the promotional content", 
					"Breadcrumb not display below the promotional content", driver);
			
			if (!(Utils.isMobile())) {
				Log.softAssertThat(customerService.headers.getBreadCrumbText().equals("HOME CUSTOMER SERVICE"), 
						"Breadcrumb should be displayed as Home / Customer Service", 
						"Breadcrumb is display as Home / Customer Service", 
						"Breadcrumb not display as Home / Customer Service", driver);
			}
			else {
				Log.softAssertThat(customerService.elementLayer.verifyPageElements(Arrays.asList("mobileHome"), customerService), 
						"Breadcrumb should be displayed as Home", 
						"Breadcrumb is display as Home", 
						"Breadcrumb not display as Home", driver);
			}
			
			homePage = customerService.clickOnHomeInBC();
			Log.message(i++ + ". Clicked on Home link in Breadcrumb");

			Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage), 
					"Clicking on Bread crumb should navigate to the home page of the respective brand", 
					"Navigate to the home page of the respective brand", 
					"Not navigate to the home page of the respective brand", driver);
			
			if(Utils.isDesktop()) {
				customerService = homePage.headers.navigateToCustomerService();
			} else {
				customerService = homePage.footers.navigateToCustomerService();
			}
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Customer Service Page!", driver);
			
			//Step-2: Verify the functionality of Customer Service Label
			Log.softAssertThat(customerService.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionBreadcrum", "lblCustomerSupport", customerService), 
					"Customer Service label should be displayed below the Breadcrumb", 
					"Customer Service label should be displayed below the Breadcrumb", 
					"Customer Service label should be displayed below the Breadcrumb", driver);
			
			Log.softAssertThat(customerService.elementLayer.verifyElementTextContains("lblCustomerSupport", csHeaderLabel, customerService), 
					"System should display the folder ID name attribute", 
					"System should display the folder ID name attribute", 
					"System should display the folder ID name attribute", driver);
			
			//Step-3: Verify the functionality of Article tiles
			Log.softAssertThat(customerService.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblCustomerSupport", "tileSection", customerService), 
					"Article tiles should be displayed below the Customer Service label", 
					"Article tiles should be displayed below the Customer Service label", 
					"Article tiles should be displayed below the Customer Service label", driver);

			Log.message(i++ + ". Article tiles arranged from left to right based on the sort sequence!", driver);
			List<String> AtricleTestdata = Arrays.asList(demandwareProperty.getProperty("sort_sequence_customer_service_" + Utils.getCurrentBrand().name()).toLowerCase().split("\\|"));;

			Log.softAssertThat(Utils.compareTwoList(AtricleTestdata, customerService.getArticleTileHeaders()), 
					" Tiles should be arranged left to right based on the sort sequence ", 
					" Tiles arranged left to right based on the sort sequence", 
					" Tiles not arranged left to right based on the sort sequence", driver);

			customerService.getNumberOfArticleTilesPerRow();

			if (!(Utils.isMobile())) {
				Log.softAssertThat(customerService.getNumberOfArticleTilesPerRow()==3, 
						"CustomerService page should display the 3 tiles in a row", 
						"CustomerService page is displaying the 3 tiles in a row", 
						"CustomerService page is not displaying the 3 tiles in a row", driver);
			} else {
				Log.softAssertThat(customerService.getNumberOfArticleTilesPerRow()==1, 
						"CustomerService page should display the 1 tiles in a row", 
						"CustomerService page is displaying the 1 tiles in a row", 
						"CustomerService page is not displaying the 1 tiles in a row", driver);
			}
			
			if(Utils.isMobile()) {
				Log.softAssertThat(customerService.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkCallUs", "lnkChatWithUs", customerService) &&
						customerService.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkChatWithUs", "lnkMailWithUs", customerService), 
						"Check the link call us, chat with us, email to us links display order", 
						"The link 'Call us' displays first, 'Chat with us' link displays second, 'Email to us' displays third", 
						"The link are not in order", driver);
			} else {
				Log.softAssertThat(customerService.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkChatWithUs", "lnkCallUs", customerService) &&
						customerService.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkMailWithUs", "lnkChatWithUs", customerService), 
						"Check the link call us, chat with us, email to us links display order", 
						"The link 'Call us' displays first, 'Chat with us' link displays second, 'Email to us' displays third", 
						"The link are not in order", driver);
			}

			Log.message("Chat with us is not automatable");
			
			if(Utils.isDesktop())
			{
				customerService.mouseHoverOnTile(1);
				Log.message(i++ + ". Mouse hovered on Tile - 1.", driver);
				
				Log.softAssertThat(customerService.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkHoverTile"), customerService), 
						"Check hovering on the tile is displaying the titles", 
						"Hovering on the tile is displaying the titles", 
						"Hovering on the tile is not displaying the titles", driver);
			}

			//Step 6
			Log.message("FAQ page for the brand specific is covered in the test case id: C22787 Step 1 to 4");
			Log.softAssertThat(customerService.elementLayer.verifyVerticalAllignmentOfElements(driver, "contactLandingSection", "faqSectionHeading", customerService), 
					"Check the faq section is displaying below contact landing section", 
					"The faq section is displaying below contact landing section", 
					"The faq section is not displaying below contact landing section", driver);

			Log.softAssertThat(!(customerService.elementLayer.VerifyElementDisplayed(Arrays.asList("faqQuestionActive"), customerService)), 
					"All FAQ topics should be in collapsed state.", 
					"All FAQ topics in collapsed state.", 
					"All FAQ topics not in collapsed state.", driver);
			customerService.clickOnFaqQuestion();
			Log.softAssertThat(customerService.elementLayer.verifyPageElements(Arrays.asList("faqQuestionActive"), customerService), 
					"FAQ anaswer should visible when clicking the cart symbol", 
					"FAQ anaswer is visible when clicking the cart symbol", 
					"FAQ anaswer is not visible when clicking the cart symbol", driver);
			customerService.clickOnFaqQuestionActive();
			Log.softAssertThat(!(customerService.elementLayer.VerifyElementDisplayed(Arrays.asList("faqQuestionActive"), customerService)), 
					"All FAQ answer should be in collapsed state.", 
					"All FAQ answer in collapsed state.", 
					"All FAQ answer not in collapsed state.", driver);

			Log.reference("Caret symbol should not verified since no specific element for this.");

			Log.softAssertThat(customerService.elementLayer.verifyVerticalAllignmentOfElements(driver, "contactLandingSection", "btnViewAllFaq", customerService), 
					"Check the view all faq button is displaying below contact landing section", 
					"The view all faq button is displaying below contact landing section", 
					"The view all faq button is not displaying below contact landing section", driver);
			
			if (!Utils.isMobile()) {
				Log.softAssertThat(customerService.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnViewAllFaq", "faqSectionHeading", customerService), 
						"Check the view all faq button is displaying right side to the Faq title", 
						"The view all faq button is displaying right side to the Faq title", 
						"The view all faq button is not displaying right side to the Faq title", driver);
			}
			
			FaqPage faqPage = customerService.clickOnViewAllFaq();
			
			Log.softAssertThat(faqPage.elementLayer.verifyPageElements(Arrays.asList("lnknavFaq"), faqPage), 
					"Check Faq Page is displaying", 
					"Faq Page page is displaying", 
					"Faq Page is not displaying", driver);
			
			if(Utils.isDesktop()) {
				customerService = homePage.headers.navigateToCustomerService();
			} else {
				customerService = homePage.footers.navigateToCustomerService();
			}
			
			ContactUsPage contactUsPage = customerService.clickOnMailWithUs();
			
			Log.softAssertThat(contactUsPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), contactUsPage), 
					"Check contact us page is displaying", 
					"Contact us page is displaying", 
					"Contact us page is not displaying", driver);

			Log.testCaseResult();
		} // Ending try block
		catch(Exception e)
		{
			Log.exception(e, driver);
		} // Ending catch block
		finally{
			Log.endTestCase();
			driver.quit();

		}// Ending finally
	}

}//TC_FBB_DROP6_C22783
