package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22585 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22585(String browser) throws Exception {
		Log.testCaseInfo();
		
		//Load Test Data
		String prd_shipping_eligible = prdData.get("prd_variation");
		String prd_exclude_all_ship = prdData.get("prd_exclude_all_ship");
		String prd_surcharge = prdData.get("prd_surcharge");
		String prd_backOrder = prdData.get("prd_back-order");
		String prd_gift_cert = prdData.get("prd_gc_physical");
		String cpn_Shipping = prdData.get("cpn_promo-ship-level");
		String gc_Shipping = prdData.get("gift_card_fixed_shipping_rate");
		
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Full Beauty Home Page.", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_shipping_eligible);
			Log.message(i++ + ". Navigated to PDP Page for :: " + pdpPage.getProductName(), driver);
			
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.addProductToBag();
			Log.message(i++ + ". Product added to Bag.", driver);
			
			ShoppingBagPage cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
			
			SignIn signIn = (SignIn) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout-SignIn Section.");
			
			signIn.typeGuestEmail(AccountUtils.generateEmail(driver));
			CheckoutPage checkoutPage = signIn.clickOnContinueInGuestLogin();
			Log.message(i++ + ". Continued to Shipping Section.");
			
			//Step-1: Verify the design logic pattern of the method name in the shipping page
			Log.softAssertThat(checkoutPage.verifyShippingMethodPattern(),
					"Methods should be presented in Z pattern based on the method sequence in Business Manager",
					"Methods is presented in Z pattern based on the method sequence in Business Manager",
					"Methods is not presented in Z pattern based on the method sequence in Business Manager", driver);
			
			//Step-3: Verify the display of Shipping Method Name
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("radioGroundShipment", "selected", "true", checkoutPage),
					"Standard Delivery should be selected by default.",
					"Standard Delivery is selected by default.",
					"Standard Delivery is not selected by default.", driver);
			
			checkoutPage.selectShippingMethod("express");
			Log.message(i++ + ". Clicked on Express Shipping.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("radio2DayExpress", "selected", "true", checkoutPage),
					"User should be able to select other shipping method name if available",
					"User is able to select other shipping method name when available",
					"User is not able to select other shipping method name when available", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblDeliveryOptionHeading", "lblRegularShippingMethodName", checkoutPage),
					"Shipping Method Name should be displayed below the Delivery Options Heading",
					"Shipping Method Name is displayed below the Delivery Options Heading",
					"Shipping Method Name is not displayed below the Delivery Options Heading", driver);
			
			cartPage = checkoutPage.clickOnEditInShoppingBagList();
			Log.message(i++ + ". Navigated to Cart page.", driver);
			
			cartPage.enterPromoCode(cpn_Shipping);
			cartPage.clickOnApplycouponButton();
			Log.message(i++ + ". Shipping Promotion Applied.", driver);
			
			signIn = (SignIn) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout-SignIn Section.");
			
			signIn.typeGuestEmail(AccountUtils.generateEmail(driver));
			checkoutPage = signIn.clickOnContinueInGuestLogin();
			Log.message(i++ + ". Continued to Shipping Section.");
			
			//Step-4: Verify the functionality of Shipping Method Cost
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lblRegularShippingMethodCost", "lblRegularShippingMethodName", checkoutPage),
					"Shipping Method Cost should be displayed right to the Shipping Method Name",
					"Shipping Method Cost is displayed right to the Shipping Method Name",
					"Shipping Method Cost is not displayed right to the Shipping Method Name", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblRegularShippingMethodCost"), checkoutPage),
					"System should display the cost of the Shipping method",
					"System displays the cost of the Shipping method",
					"System does not display the cost of the Shipping method", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("lblRegularShipping_standard_cost", "text-decoration", "line-through", checkoutPage) &&
					checkoutPage.elementLayer.verifyElementColor("lblRegularShipping_discount_cost", "e700", checkoutPage),
					"If the Shipping method has a promotion, the original cost should have a strike through it and the discounted cost should be displayed in red color",
					"When Shipping method has a promotion, the original cost is striken through it and the discounted cost is displayed in red color",
					"When Shipping method has a promotion, the original cost is not striken through it and the discounted cost is not displayed in red color", driver);
			
			//Step-6: Verify the functionality of Shipping Promotion Callout
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblRegularShippingMethodName", "lblRegularShippingPromoMsg", checkoutPage),
					"If shipping method has a promotion, Shipping Promotion Callout should be displayed down to the Shipping Method Name.",
					"When shipping method has a promotion, Shipping Promotion Callout is displayed down to the Shipping Method Name.",
					"When shipping method has a promotion, Shipping Promotion Callout is not displayed down to the Shipping Method Name.", driver);
			
			//Step-7: Verify the functionality of Shipping Method Details
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblRegularShippingPromoMsg", "lblRegularShippingMessage_with_promo", checkoutPage),
					"If Shipping Promotion Callout is available then Shipping Method Details should be displayed under the Shipping Promotion Callout.",
					"When Shipping Promotion Callout is available then Shipping Method Details is displayed under the Shipping Promotion Callout.",
					"When Shipping Promotion Callout is available then Shipping Method Details is not displayed under the Shipping Promotion Callout.", driver);
			
			cartPage = checkoutPage.clickOnEditInShoppingBagList();
			Log.message(i++ + ". Navigated to Cart page.", driver);
			
			cartPage.removeAllCartProduct();
			Log.message(i++ + ". Products removed from Cart.", driver);
			
			pdpPage = cartPage.headers.navigateToPDP(prd_exclude_all_ship);
			Log.message(i++ + ". Navigated to PDP Page for :: " + pdpPage.getProductName());
			
			pdpPage.selectColor();
			pdpPage.selectSize();
			try {
				pdpPage.selectWidthBasedOnIndex(0);
			}catch(Exception e) {
				Log.message(i++ + ". Product has not width option to select");
			}
			pdpPage.addProductToBag();
			Log.message(i++ + ". Product added to Bag.", driver);
			
			cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart Page.", driver);
			
			signIn = (SignIn) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout-SignIn Section.");
			
			signIn.typeGuestEmail(AccountUtils.generateEmail(driver));
			checkoutPage = signIn.clickOnContinueInGuestLogin();
			Log.message(i++ + ". Continued to Shipping Section.");
			
			//Step-2: Verify the functionality of No Methods Available State
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtNoShippingException"), checkoutPage),
					"The No Methods Available State message should be displayed only when all the products in the cart is excluded from all the shipping methods",
					"The No Methods Available State message is displayed only when all the products in the cart is excluded from all the shipping methods",
					"The No Methods Available State message is not displayed only when all the products in the cart is excluded from all the shipping methods", driver);
			
			cartPage = checkoutPage.clickOnEditInShoppingBagList();
			Log.message(i++ + ". Navigated to Cart page.", driver);
			
			cartPage.removeAllCartProduct();
			Log.message(i++ + ". Products removed from Cart.", driver);
			
			pdpPage = cartPage.headers.navigateToPDP(prd_surcharge);
			Log.message(i++ + ". Navigated to PDP Page for :: " + pdpPage.getProductName());
			
			pdpPage.addToBag();
			Log.message(i++ + ". Product added to Bag.", driver);
			
			pdpPage = cartPage.headers.navigateToPDP(prd_shipping_eligible);
			Log.message(i++ + ". Navigated to PDP Page for :: " + pdpPage.getProductName());
			
			pdpPage.addToBag();
			Log.message(i++ + ". Product added to Bag.", driver);
			
			cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart Page.", driver);
			
			cartPage.clickOnCouponRemoveButton();
			signIn = (SignIn) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout-SignIn Section.");
			
			signIn.typeGuestEmail(AccountUtils.generateEmail(driver));
			checkoutPage = signIn.clickOnContinueInGuestLogin();
			Log.message(i++ + ". Continued to Shipping Section.");
			
			//Step-5: Verify the functionality of Shipping Surcharge
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lblShippingSurcharge", "lblSuperFastShippingMethodName", checkoutPage) &&
					checkoutPage.elementLayer.verifyTextContains("lblRegularShippingSurcharge", "+", checkoutPage),
					"Shipping Surcharge should be displayed right to the Shipping Method Cost with '+' sign in between.",
					"Shipping Surcharge is displayed right to the Shipping Method Cost with '+' sign in between.",
					"Shipping Surcharge is not displayed right to the Shipping Method Cost with '+' sign in between.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblShippingSurcharge"), checkoutPage),
					"Shipping Surcharge should be displayed only if the Cart contains corresponding items applicable for surcharge",
					"Shipping Surcharge is displayed only if the Cart contains corresponding items applicable for surcharge",
					"Shipping Surcharge is not displayed only if the Cart contains corresponding items applicable for surcharge", driver);
			
			//Step-7: Verify the functionality of Shipping Method Details
			if(checkoutPage.elementLayer.VerifyElementDisplayed(Arrays.asList("lblRegularShippingPromoMsg"), checkoutPage)) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblRegularShippingPromoMsg", "txtShippingMethodDescription", checkoutPage),
						"If Shipping Promotion Callout is available then Shipping Method Details should be displayed under the Shipping Promotion Callout.",
						"Shipping Method Details is displayed under the Shipping Promotion Callout.",
						"Shipping Method Details is not displayed under the Shipping Promotion Callout.", driver);
			}
			else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblRegularShippingMethodName", "txtShippingMethodDescription", checkoutPage),
						"If Shipping Promotion Callout is not available then Shipping Method Details should be displayed under the Shipping Method Name.",
						"Shipping Method Details is displayed under the Shipping Method Name.",
						"Shipping Method Details is not displayed under the Shipping Method Name.", driver);
			}
			
			
			//Step-8: Verify the functionality of Estimated Delivery
			/*Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "", "", checkoutPage),
					"Estimated Delivery should be displayed in the 2nd line right to the Shipping Method Details",
					"Estimated Delivery is displayed in the 2nd line right to the Shipping Method Details",
					"Estimated Delivery is not displayed in the 2nd line right to the Shipping Method Details", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "", "", checkoutPage),
					"This string should be placed after the Shipping Method Details text",
					"This string is placed after the Shipping Method Details text",
					"This string is not placed after the Shipping Method Details text", driver);*/
			
			cartPage = checkoutPage.clickOnEditInShoppingBagList();
			Log.message(i++ + ". Navigated to Cart page.", driver);
			
			cartPage.removeAllCartProduct();
			Log.message(i++ + ". Products removed from Cart.", driver);
			
			pdpPage = cartPage.headers.navigateToPDP(prd_backOrder);
			Log.message(i++ + ". Navigated to PDP Page for :: " + pdpPage.getProductName());
			
			pdpPage.addToBag();
			Log.message(i++ + ". Product added to Bag.", driver);
			
			pdpPage = cartPage.headers.navigateToPDP(prd_shipping_eligible);
			Log.message(i++ + ". Navigated to PDP Page for :: " + pdpPage.getProductName());
			
			pdpPage.addToBag();
			Log.message(i++ + ". Product added to Bag.", driver);
			
			cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart Page.", driver);
			
			signIn = (SignIn) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout-SignIn Section.");
			
			signIn.typeGuestEmail(AccountUtils.generateEmail(driver));
			checkoutPage = signIn.clickOnContinueInGuestLogin();
			Log.message(i++ + ". Continued to Shipping Section.");
			
			//Step-9: Verify the functionality of Mixed Method Messaging
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblSuperFastShippingMessage", "lblSuperFast_Mixed_Method_Exception", checkoutPage),
					"Mixed Method Messaging should be displayed below shipping method message.",
					"Mixed Method Messaging is displayed below shipping method message.",
					"Mixed Method Messaging is not displayed below shipping method message.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtShippingMethodException"), checkoutPage),
					"Mixed Method Messaging should be displayed only if all items or any of the items can't be sent using a particular shipping method",
					"Mixed Method Messaging is displayed only if all items or any of the items can't be sent using a particular shipping method",
					"Mixed Method Messaging is not displayed only if all items or any of the items can't be sent using a particular shipping method", driver);
			
			checkoutPage.selectShippingMethod("superfast");
			Log.message(i++ + ". Clicked on SuperFast Shipping.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("mdlPOBox"), checkoutPage),
					"If the User selects this method system should initiate the Shipping Method Exception",
					"When the User selects this method system initiates the Shipping Method Exception",
					"When the User selects this method system does not initiate the Shipping Method Exception", driver);
			
			checkoutPage.clickCancelInPOBoxOverlay();
			Log.message(i++ + ". PO Box overlay closed.", driver);
			
			//Step-10: Verify the functionality of Shipping Tool Tip
			Log.softAssertThat(checkoutPage.elementLayer.verifyInsideElementAlligned("lnkDeliveryOptionsToolTip", "section_Shipping_methods", "top", checkoutPage) &&
					checkoutPage.elementLayer.verifyInsideElementAlligned("lnkDeliveryOptionsToolTip", "section_Shipping_methods", "right", checkoutPage),
					"Shipping Tool Tip should be displayed top right corner to the Delivery Options Heading.",
					"Shipping Tool Tip is displayed top right corner to the Delivery Options Heading.",
					"Shipping Tool Tip is not displayed top right corner to the Delivery Options Heading.", driver);
			
			cartPage = checkoutPage.clickOnEditInShoppingBagList();
			Log.message(i++ + ". Navigated to Cart page.", driver);
			
			cartPage.removeAllCartProduct();
			Log.message(i++ + ". Products removed from Cart.", driver);
			
			pdpPage = cartPage.headers.navigateToPDP(prd_gift_cert);
			Log.message(i++ + ". Navigated to PDP Page for :: " + pdpPage.getProductName());
			
			pdpPage.addToBag();
			Log.message(i++ + ". Product added to Bag.", driver);
			
			cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart Page.", driver);
			
			signIn = (SignIn) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout-SignIn Section.");
			
			signIn.typeGuestEmail(AccountUtils.generateEmail(driver));
			checkoutPage = signIn.clickOnContinueInGuestLogin();
			Log.message(i++ + ". Continued to Shipping Section.", driver);
			
			//Step-11: Verify the functionality of the Gift Cards for the various Shipping Method Name
			Log.softAssertThat(checkoutPage.getShippingMethodCount() == 1 &&
					checkoutPage.elementLayer.verifyTextContains("lblRegularShippingMethodCost", gc_Shipping, checkoutPage),
					"Gift cards should be excluded from all methods except standard using standard SG shipping exclusions",
					"Gift cards are excluded from all methods except standard using standard SG shipping exclusions",
					"Gift cards are not excluded from all methods except standard using standard SG shipping exclusions", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}
}// search
