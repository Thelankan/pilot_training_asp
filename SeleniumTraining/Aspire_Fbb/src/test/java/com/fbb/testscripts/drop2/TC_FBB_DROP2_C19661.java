package com.fbb.testscripts.drop2;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.Refinements;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19661 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	
	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19661(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		//Load test data
		String navigationCategory = prdData.get("level-2");
		String level1 = navigationCategory.split("\\|")[0];
		String filters = prdData.get("plp_slp_filters");
		String size = filters.split("\\|")[1];
		String color = filters.split("\\|")[3];
		String size1 = filters.split("\\|")[4];
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			PlpPage plp = headers.navigateTo(level1);
			Refinements refinements = plp.refinements;
			Log.message(i++ + ". Navigated to " + level1, driver);
			
			//Step-1: Verify click on downward arrow mark in color Refinement header.
			refinements.clickOpenCloseColorFilterDesktop("open");
			Log.message(i++ + ". Opened color options", driver);
			
			Log.softAssertThat(refinements.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("lstAvailableColors"), refinements), 
					"Color refinement should be opened and displayed with the color swatches", 
					"Color refinement is displayed with the color swatches", 
					"Color refinement is not displayed with the color swatches", driver);
			
			//Step-2: Verify Select color swatches ( Ex: RED ) color from the color refinement
			int resultNoRefinement = plp.getSearchResultCount();
			
			refinements.selectColorOnRefinement(color);
			Log.message(i++ + ". " + color + " selected as refinement.", driver);
			
			int resultCountAfterColor = plp.getSearchResultCount();
			
			Log.softAssertThat(refinements.verifyColorRefinementSelected(color), 
					"Selected Color swatch should be displayed as Breadcrumb", 
					"Selected Color swatch is displayed as Breadcrumb", 
					"Selected Color swatch is not displayed as Breadcrumb", driver);
			
			Log.softAssertThat(!refinements.elementLayer.getAttributeForElement("divRefinementBar", "innerHTML", refinements).contains("and")
							|| !refinements.elementLayer.getAttributeForElement("divRefinementBar", "innerHTML", refinements).contains("AND"), 
					"The word \"and\" should not be present in between refinement values", 
					"The word \"and\" is not present in between refinement values", 
					"The word \"and\" is present in between refinement values", driver);
			
			Log.softAssertThat(refinements.elementLayer.verifyElementsAreInSameRow("spanFilteredBy", "btnClearAllInDesktop", refinements), 
					"Bread Crumbs should be displayed in \"Filtered By:\" section", 
					"Bread Crumbs are displayed in \"Filtered By:\" section", 
					"Bread Crumbs are not displayed in \"Filtered By:\" section", driver);
			
			//Step-3: Click on downward arrow mark in Size Refinement header.
			refinements.clickOpenCloseSizeFilterDesktop("open");
			Log.message(i++ + ". Opened size options", driver);
			
			Log.softAssertThat(refinements.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("lstAvailableSizes"), refinements), 
					"Size refinement should open up with the sizes ", 
					"Size refinement is open up with the sizes ", 
					"Size refinement is not opned with the sizes ", driver);
			
			//Step-4: Select a size from the Bra size Refinement in Bra size Refinement header.
			refinements.clickOnClearAllInRefinement();
			Log.message(i++ + ". Cleared all refinements.", driver);
			
			refinements.selectSizeOnRefinement(size);
			Log.message(i++ + ". Size " + size + " selected.", driver);
			
			Log.softAssertThat(refinements.verifyRefinementSelected(size1), 
					"Selected refinement should be added in the breadcrumb.", 
					"Selected refinement is added in the breadcrumb.", 
					"Selected refinement is not added in the breadcrumb.", driver);
			
			//Step-5: Verify remove button present besides each Breadcrumb in the Filtered section
			Log.softAssertThat(refinements.verifyRefinementXButton(), 
					"Each Breadcrumb should contain a Remove button", 
					"Each Breadcrumb contains a Remove button", 
					"Each Breadcrumb does not contain a Remove button", driver);
			
			Log.softAssertThat(refinements.verifyRemoveButtonShape(), 
					"The Remove button should be a cross sign", 
					"The Remove button is a cross sign", 
					"The Remove button is not a cross sign", driver);
			
			//Step-6: Click on remove button of any one of the Breadcrumb in the Filtered section
			refinements.selectColorOnRefinement(color);
			Log.message(i++ + ". " + color + " color selected as refinement.", driver);
			
			refinements.removeRefinement(size1);
			Log.message(i++ + ". Removed " + size, driver);
			
			Log.softAssertThat(refinements.verifyRefinementRemoved(size), 
					"Selected refinement value should be removed.", 
					"Selected refinement value is removed.", 
					"Selected refinement value is not removed.", driver);
			
			Log.softAssertThat(plp.getSearchResultCount() == resultCountAfterColor, 
					"Products should be updated and based on the other Refinement values", 
					"Products is updated and based on the other Refinement values", 
					"Products is not updated and based on the other Refinement values", driver);
			
			//Step-7: Click on Clear All button present below the right corner of Horizontal Refinement Bar
			refinements.clickOnClearAllInRefinement();
			Log.message(i++ + ". Cleared all refinements.", driver);
			
			Log.softAssertThat(plp.getSearchResultCount() == resultNoRefinement, 
					"All the products belonging to that particular subcategory should be displayed.", 
					"All the products belonging to that particular subcategory are displayed.", 
					"All the products belonging to that particular subcategory are not displayed.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M1_FBB_DROP2_C19661
}
