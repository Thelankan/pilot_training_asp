package com.fbb.testscripts.drop5;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22637 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(enabled = false, groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22637(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
		String email = accountData.get("credential_return_order").split("\\|")[0];
		String password = accountData.get("credential_return_order").split("\\|")[1];
	
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			homePage.headers.signInToMyAccountPage(email, password);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			//Load Regular Price Product
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			String productInPDP = pdpPage.getProductName();			
	
			String selectedSize = pdpPage.selectSize();
			Log.message(i++ + ". Size Selected :: '"+selectedSize+"'", driver);
	
			String selectedColor = pdpPage.selectColor();
			Log.message(i++ + ". Selected Color :: '"+selectedColor+"'");
	
			String selectedSizeFamily = pdpPage.selectSizeFamily();
			Log.message(i++ + ". Selected Color :: '"+selectedSizeFamily+"'");
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add to cart button");
	
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Clicked on 'X' in mini cart overlay");
	
			ShoppingBagPage shoppingBag = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on 'My Bag' icon in header");
	
			Log.softAssertThat((productInPDP.equals(shoppingBag.getProductName())),					 
					"Product should be added to the cart", 
					"Product is added to the cart", 
					"Product is not added to the cart", driver);			
	
			CheckoutPage checkoutPage = shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Clicked on 'Checkout Now' button in shopping bag page");
	
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", "Ground", "valid_address1");
			Log.message(i++ + ". Filled Shipping details", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Clicked on 'Continue' button after filling shipping address");
	
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message(i++ + ". Entered Payment details",driver);
	
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details");
	
			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!");
	
			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPage.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order should be placed successfully as a Guest user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a Guest user and Order confirmation page is displayed!",
					"Order is not placed successfully as a Guest user and Order confirmation page is not displayed!", driver);
	
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C22637

}// search
