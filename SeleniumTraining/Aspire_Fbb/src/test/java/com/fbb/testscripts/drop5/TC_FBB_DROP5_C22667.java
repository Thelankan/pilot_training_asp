package com.fbb.testscripts.drop5;

import java.util.Arrays;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22667 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22667(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		//Test data
		String CardType = "card_Visa";
		String cardPLCC = "Roamans Platinum";
		
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, email + "|" + password);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			MyAccountPage myAcc = headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged in to user account.", driver);
			
			PaymentMethodsPage payment = myAcc.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to payment methods page.", driver);
			
			if(payment.getNumberOfSavedCards()==0) {
				payment.addNewCard(CardType, true);
				Log.message(i++ + ". New card saved.", driver);
			}
			
			//Step-1: Verify the functionality of Default Payment Label
			if(!Utils.isMobile()) {
				Log.softAssertThat(payment.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lblDefaultPaymentIcon", "lblPaymentMethodsTitle", payment), 
						"Default Payment Label should be displayed right of Payment Methods Heading.", 
						"Default Payment Label is displayed right of Payment Methods Heading.", 
						"Default Payment Label is not displayed right of Payment Methods Heading.", driver);
			}
			else {
				Log.softAssertThat(payment.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblPaymentMethodsTitle", "lblDefaultPaymentIcon", payment), 
						"Default Payment Label should be displayed below the Payment Methods Heading.", 
						"Default Payment Label is displayed below the Payment Methods Heading.", 
						"Default Payment Label is not displayed below the Payment Methods Heading.", driver);
			}
			
			//Step-2: Verify the functionality of Default Payment Icon
			Log.softAssertThat(payment.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lblDefaultPayment", "iconDefaultPayment", payment), 
					"Default Payment Icon should be displayed left of the Default Payment Label", 
					"Default Payment Icon is displayed left of the Default Payment Label", 
					"Default Payment Icon is not displayed left of the Default Payment Label", driver);
			
			//Step-3: Verify the functionality of Add New Card
			if(payment.getNumberOfSavedCards() > 0) {
				Log.softAssertThat(payment.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkAddNewCard", "lblDefaultPayment", payment), 
						"Add New Card should be displayed on right corner of the Default Payment Label", 
						"Add New Card is displayed on right corner of the Default Payment Label", 
						"Add New Card is not displayed on right corner of the Default Payment Label", driver);
				
				Log.softAssertThat(payment.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkAddNewCard"), payment), 
						"System should enable/display the Add New Card Option only when there is at least 1 Payment Instrument available in the Saved Cards List", 
						"System displayes New Card Option when card is/are saved.", 
						"System displayes New Card Option when card is/are saved.", driver);
				
				payment.deleteAllSavedCards();
				Log.message(i++ + ". Removed all cards.", driver);
				
				Log.softAssertThat(!payment.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkAddNewCard"), payment), 
						"System should enable/display the Add New Card Option only when there is at least 1 Payment Instrument available in the Saved Cards List", 
						"New Card option is not displayed with no saved card.", 
						"New Card option displayed with no saved card.", driver);
				
				payment.addNewCard(CardType, true);
				Log.message(i++ + ". New card saved.", driver);
				
				payment.clickAddNewCardLink();
				Log.message(i++ + ". Clicked on Add New Card", driver);
				
				Utils.WaitForAjax(driver);
				
				Log.softAssertThat(payment.elementLayer.VerifyElementDisplayed(Arrays.asList("divAddNewCardModule"), payment), 
						"System should display the new card module on clicking the Add New Card.", 
						"System displays the new card module on clicking the Add New Card.", 
						"System does not display the new card module on clicking the Add New Card.", driver);
				
				Log.softAssertThat(payment.elementLayer.verifyVerticalAllignmentOfElements(driver, "divAddNewCardModule", "paymentList", payment), 
						"New card module should display above Saved Cards List.", 
						"New card module displays above Saved Cards List", 
						"New card module does not display above Saved Cards List", driver);
				
				Log.softAssertThat(payment.elementLayer.verifyCssPropertyForElement("lnkAddNewCard", "pointer-events", "none", payment)
								&& !payment.elementLayer.verifyCssPropertyForElement("lnkAddNewCard", "color", "#000000", payment), 
						"System should disable the add new card link when the New Card Module is open.", 
						"System disables the add new card link when the New Card Module is open.", 
						"System does not disable the add new card link when the New Card Module is open.", driver);
			}
			
			//Step-4: Verify the functionality of New Card Module
			Log.softAssertThat(payment.elementLayer.VerifyElementDisplayed(Arrays.asList("txtCreditCardTypePlaceholder","txtNameOnCard","txtCardNumber",
									"txtExpiryMonthPlaceholder","txtExpiryYearPlaceholder","divPaymentMethodsAccepted","chkMakeDefaultPayment"), payment), 
					"New Card module should have correct components.", 
					"New Card module has all the components present.", 
					"New Card module does not have all components present.", driver);
			
			Log.softAssertThat(payment.verifyPlaceHolderText(), 
					"Placeholder texts should be showing correctly.", 
					"Placeholder texts shows corrctly", 
					"All placeholder texts do not show corretly.", driver);
			
			String cardInfo[] = checkoutProperty.getProperty(CardType+1).split("\\|");
			String cardType = cardInfo[0];
			String nameOnCard = cardInfo[1];
			String cardNo = cardInfo[2];
			String expMonth = cardInfo[3];
			String expYear = cardInfo[4];
			
			//PLCC tests
			payment.selectCardType(cardPLCC);
			Log.message(i++ + ". PLCC card selected.", driver);
			payment.typeCardNumber(cardNo);
			Log.message(i++ + ". Card number typed in.", driver);
			Log.softAssertThat(payment.getCardNumberLength() == 9, 
					"If the Selected Card Type is PLCC, then system should not allow the user to enter more than 9 digits", 
					"If the Selected Card Type is PLCC, then system does not allow the user to enter more than 9 digits", 
					"When the Selected Card Type is PLCC, then system allowed the user to enter more than 9 digits", driver);
			
			Log.softAssertThat(!payment.elementLayer.VerifyElementDisplayed(Arrays.asList("drpExpiryMonth"), payment), 
					"If the Selected Card Type is PLCC, then system should not display (or disabled) the Expiry Month Field", 
					"If the Selected Card Type is PLCC, then system does not display (or disabled) the Expiry Month Field", 
					"When the Selected Card Type is PLCC, then system displays the Expiry Month Field", driver);
			
			Log.softAssertThat(!payment.elementLayer.VerifyElementDisplayed(Arrays.asList("drpExpiryYear"), payment), 
					"If the Selected Card Type is PLCC, then system should not display (or disabled) the Expiry Year Field", 
					"If the Selected Card Type is PLCC, then system does not display (or disabled) the Expiry Year Field", 
					"When the Selected Card Type is PLCC, then system displays the Expiry Year Field", driver);
			
			//regular card
			payment.selectCardType(cardType);
			payment.typeName("");
			Log.message(i++ + ". Regular card selected.", driver);
			Log.softAssertThat(payment.getSelectedCard().equalsIgnoreCase(cardType), 
					"User should be able to select a card type from the drop down menu.", 
					"User is able to select a card type from the drop down menu.", 
					"User is not able to select a card type from the drop down menu.", driver);
			
			Log.softAssertThat(payment.elementLayer.verifyVerticalAllignmentOfElementsWithoutPadding(driver, "txtCreditCardTypePlaceholder", "drpSelectCardType", payment), 
					"Place holder text Select Credit Card Type should be positioned on top when user selects the card type.", 
					"Place holder text Select Credit Card Type is positioned on top when user selects the card type.", 
					"Place holder text Select Credit Card Type is not positioned on top when user selects the card type.", driver);
			
			Log.softAssertThat(payment.verifyOrderOfCardsDisplayed(), 
					"Drop down should display the PLCC cards first and then non-PLCC cards.", 
					"Drop down displays the PLCC cards first and then non-PLCC cards.", 
					"Drop down does not display the PLCC cards first and then non-PLCC cards.", driver);
			
			//Name on Card
			payment.typeName(nameOnCard);
			Log.message(i++ + ". Name typed in", driver);
			Log.softAssertThat(payment.getNameText().equalsIgnoreCase(nameOnCard), 
					"User should be able to enter text in the Name on Card field", 
					"User is able to enter text in the Name on Card field", 
					"User is not able to enter text in the Name on Card field", driver);
			
			BrowserActions.scrollToTopOfPage(driver);
			Log.softAssertThat(payment.elementLayer.verifyVerticalAllignmentOfElementsWithoutPadding(driver, "txtNamePlaceholder", "txtNameOnCard", payment), 
					"Place holder text \"Name on Card\" should be positioned on top when user starts entering the data.", 
					"Place holder text \"Name on Card\" is positioned on top when user starts entering the data.", 
					"Place holder text \"Name on Card\" is not positioned on top when user starts entering the data.", driver);
			
			//Card Number
			payment.typeCardNumber(cardNo);
			Log.message(i++ + ". Card number typed in.", driver);
			Log.softAssertThat(payment.getCardNumber().equalsIgnoreCase(cardNo), 
					"User should be able to numbers in the Card Number field", 
					"User is able to numbers in the Card Number field", 
					"User is not able to numbers in the Card Number field", driver);
			
			payment.typeCardNumber(cardNo + Utils.getRandom(1000, 9999));
			Log.message(i++ + ". Card number with extra length typed in.", driver);
			Log.softAssertThat(payment.getCardNumberLength() == 16, 
					"System should not allow the user to enters more than 16 digits.", 
					"Number limit is 16.", 
					"Number limit is not 16.", driver);
			
			BrowserActions.scrollToTopOfPage(driver);
			Log.softAssertThat(payment.elementLayer.verifyVerticalAllignmentOfElementsWithoutPadding(driver, "txtCardNumberPlaceholder", "txtCardNumber", payment), 
					"Place holder text \"Card Number\" should be positioned on top when user starts entering the data.", 
					"Place holder text \"Card Number\" is positioned on top when user starts entering the data.", 
					"Place holder text \"Card Number\" is not positioned on top when user starts entering the data.", driver);
			
			//Expiration Month
			payment.selectMonth(expMonth);
			payment.typeName(nameOnCard);
			
			Log.softAssertThat(payment.getSelectedMonth().equalsIgnoreCase(expMonth), 
					"User should be able to select a Expiry Month from the drop down menu.", 
					"User is able to select a Expiry Month from the drop down menu.", 
					"User is not able to select a Expiry Month from the drop down menu.", driver);
			
			Log.softAssertThat(payment.elementLayer.verifyVerticalAllignmentOfElementsWithoutPadding(driver, "txtExpiryMonthPlaceholder", "drpExpiryMonth", payment), 
					"Place holder text \"Expiry Month\" should be positioned on top when user selects the Expiry Month", 
					"Place holder text \"Expiry Month\" is positioned on top when user selects the Expiry Month", 
					"Place holder text \"Expiry Month\" is not positioned on top when user selects the Expiry Month", driver);
			
			Log.softAssertThat(payment.verifyMonthList(), 
					"Months should be displayed as 1 to 12 top down", 
					"Months are displayed as 1 to 12 top down", 
					"Months adisplayed as 1 to 12 top down", driver);
			
			//Expiration Year
			payment.selectYear(expYear);
			payment.typeName(nameOnCard);
			
			Log.softAssertThat(payment.getSelectedYear().equalsIgnoreCase(expYear), 
					"User should be able to select a Expiry Year from the drop down menu.", 
					"User is able to select a Expiry Year from the drop down menu.", 
					"User is not able to select a Expiry Year from the drop down menu.", driver);
			
			Log.softAssertThat(payment.elementLayer.verifyVerticalAllignmentOfElementsWithoutPadding(driver, "txtExpiryYearPlaceholder", "drpExpiryYear", payment), 
					"Place holder text \"Expiry Year\" should be positioned on top when user selects the Expiry Year", 
					"Place holder text \"Expiry Year\" is positioned on top when user selects the Expiry Year", 
					"Place holder text \"Expiry Year\" is not positioned on top when user selects the Expiry Year", driver);
			
			Log.softAssertThat(payment.verifyYearList(), 
					"Years should be displayed as current year + 11 years top down", 
					"Years are displayed as current year + 11 years top down", 
					"Years are not displayed as current year + 11 years top down", driver);
			
			Log.softAssertThat(payment.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtExpiryMonthPlaceholder", "chkMakeDefaultPayment", payment)
							&& payment.elementLayer.verifyAttributeForElement("chkMakeDefaultPayment", "type", "checkbox", payment), 
					"Default Payment should be displayed as a checkbox below the Expiry Month", 
					"Default Payment is displayed as a checkbox below the Expiry Month", 
					"Default Payment is not displayed as a checkbox below the Expiry Month", driver);
			
			Log.softAssertThat(payment.isMakeDefaultPaymentChecked(), 
					"\"Make this my default payment\" should be selected by default", 
					"\"Make this my default payment\" is selected by default", 
					"\"Make this my default payment\" is not selected by default", driver);
			
			payment.checkOrUnCheckMakeDefaultInCardsSection(false);
			Log.message(i++ + ". Unchecked make default tickbox.", driver);
			payment.savePaymentMethod();
			Log.message(i++ + ". Clicked on card save button", driver);
			
			Log.softAssertThat(!cardNo.endsWith(payment.getDefaultCardNumber()), 
					"If user unselect the checkbox, then system should not make the payment method as default.", 
					"If user unselect the checkbox, then system did not save the payment method as default.", 
					"If user unselect the checkbox, then system saved the payment method as default.", driver);
			
			//Step-5: Verify the functionality of Cancel Button (Desktop) and X Button (Mobile)
			int numberCardsSavedOld = payment.getNumberOfSavedCards();
			HashMap<String, String> cardDetails1 = new HashMap<String, String>();
			String[] cardInfoNew = checkoutProperty.get(CardType+3).split("\\|");
			cardDetails1.put("CardType", cardInfoNew[0]);
			cardDetails1.put("Name", cardInfoNew[1]);
			cardDetails1.put("Number", cardInfoNew[2]);
			cardDetails1.put("ExpMonth", cardInfoNew[3]);
			cardDetails1.put("ExpYear", cardInfoNew[4]);
			cardDetails1.put("MakeDefaultPayment", "Yes");
			cardDetails1.put("IsPLCC", "No");
			
			payment.clickAddNewCardLink();
			Log.message(i++ + ". Opened new card module");
			
			if(!Utils.isMobile()) {	
				Log.softAssertThat(payment.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnSave", "lnkCancelAddCard", payment), 
							"Cancel button should be displayed left of the Save button.", 
							"Cancel button is displayed left of the Save button.", 
							"Cancel button is not displayed left of the Save button.", driver);
			}
			
			payment.fillCardDetails(cardDetails1);
			Log.message(i++ + ". Filled in card details", driver);
			
			payment.clickCancelAddNewCard();
			Log.message(i++ + ". Cancelled adding new card.", driver);
			
			Log.softAssertThat(!payment.elementLayer.VerifyElementDisplayed(Arrays.asList("divAddNewCardModule"), payment), 
					"System should collapse the New Card Module", 
					"System collapsed the New Card Module", 
					"System did not collapse the New Card Module", driver);
			
			Log.softAssertThat(payment.elementLayer.verifyCssPropertyForElement("lnkAddNewCard", "color", "0, 0, 0", payment), 
					"Add New Card button should return to the active state.", 
					"Add New Card button returned to the active state.", 
					"Add New Card button did not return to the active state.", driver);
			
			Log.softAssertThat(payment.getNumberOfSavedCards() == numberCardsSavedOld, 
					"System should not save any information to the customer's account.", 
					"System did not save any information to the customer's account.", 
					"System saved card information to the customer's account.", driver);
			
			//Step-6: Verify the functionality of Save Button
			payment.clickAddNewCardLink();
			Log.message(i++ + ". Opened new card module");
			payment.savePaymentMethod();
			payment.savePaymentMethod();
			Log.message(i++ + ". Clicked on card save button", driver);
			
			Log.softAssertThat(payment.verifyErrorMessageDisplayedWhenFieldAreEmpty(), 
					"System should allow the user to save the payment method only if the user has filled the required data in all the fields", 
					"Error displayes when fields are empty", 
					"Error doesn't display.", driver);
			
			payment.clickCancelAddNewCard();
			Log.message(i++ + ". Cancelled adding new card.", driver);
			payment.clickAddNewCardLink();
			Log.message(i++ + ". Opened new card module");
			
			payment.fillCardDetails(cardDetails1);
			Log.message(i++ + ". Filled in card details", driver);
			
			payment.checkOrUnCheckMakeDefaultInCardsSection(false);
			Log.message(i++ + ". Unchecked make default tickbox.", driver);
			
			payment.savePaymentMethod();
			Log.message(i++ + ". Clicked on card save button", driver);
			
			cardNo = cardDetails1.get("Number");
			Log.softAssertThat(!cardNo.endsWith(payment.getDefaultCardNumber()), 
					"If user unselect the checkbox, then system should not make the payment method as default.", 
					"If user unselect the checkbox, then system did not save the payment method as default.", 
					"When user unselected the checkbox, then system saved the payment method as default.", driver);
			
			payment.addNewCard("card_MasterCard", true);
			Log.message(i++ + ". Added a default card", driver);
			
			//Step-7: Verify the sorting functionality of the saved cards
			Log.softAssertThat(payment.verifyPositionOfDefaultCard(), 
					"Default card should show on top of all non-PLCC cards.", 
					"Default card is shown on top.", 
					"Default card is not on top.", driver);
			
			Log.softAssertThat(payment.verifySavedPLCCLocation(), 
					"PLCC cards should be displayed above all other cards.",
					"PLCC cards are displayed above all other cards.", 
					"PLCC cards are not displayed above all other cards.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.removeAllPaymentMethods(driver);
			driver.quit();
		} // finally
	} // M1_FBB_DROP5_C22667

}
