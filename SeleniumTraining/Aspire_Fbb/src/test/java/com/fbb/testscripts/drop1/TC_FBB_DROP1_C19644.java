package com.fbb.testscripts.drop1;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OffersAndCouponsPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.customerservice.TrackOrderPage;
import com.fbb.pages.headers.HamburgerMenu;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP1_C19644 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "critical", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP1_C19644(String browser) throws Exception {
		Log.testCaseInfo();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		List<String> elementsToBeVerified = Arrays.asList("btnHamburgerClose", "lnkOffers", "lnkSignIn");
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			//1
			HamburgerMenu hMenu = (HamburgerMenu) homePage.headers.hamburgerMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger menu opened!", driver);

			
			Log.message("The Hamburger close btn, Offers link and Sign in link should be displayed");
			Log.softAssertThat(hMenu.elementLayer.VerifyPageElementDisplayed(elementsToBeVerified, hMenu), 
					"Check the Hamburger close btn, Offers link and Sign in link are displaying",
					"The Hamburger close btn, Offers link and Sign in link are displayed", 
					"The Hamburger close btn, Offers link and Sign in link are not displayed - SM-1991", driver);

			//2
			OffersAndCouponsPage offersPage = hMenu.navigateToOffers();
			Log.message(i++ + ". Navigated to Offers page!", driver);

			Log.softAssertThat(offersPage.headers.hamburgerMenu.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnOpenHamburger"), offersPage.headers.hamburgerMenu), 
					"Hamburger menu should be closed", 
					"Hamburger menu closed", 
					"Hamburger menu not closed", driver);

			Log.softAssertThat(offersPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), offersPage), 
					"Page should be navigated to Offers and coupons page.", 
					"Page navigated to Offers and coupons page.", 
					"Page not navigated to Offers and coupons page.", driver);

			//3
			hMenu = (HamburgerMenu) homePage.headers.hamburgerMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger menu opened!", driver);

			SignIn signIn = hMenu.navigateToSignIn();
			Log.message(i++ + ". Navigated to Sign In Page!", driver);

			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("divLoginBox"), signIn), 
					"System should be redirected to sign in page.", 
					"System redirected to sign in page.", 
					"System not redirected to sign in page.", driver);

			MyAccountPage myAccountPage = signIn.headers.navigateToMyAccount(email, password, true);
			Log.message(i++ + ". Valid Email and Password entered and Clicked on Login Button!");

			//step 1 continue
			hMenu = (HamburgerMenu) myAccountPage.headers.hamburgerMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger menu opened!", driver);

			Log.softAssertThat(hMenu.elementLayer.VerifyPageElementDisplayed(Arrays.asList("imgUserProfile", "lblUserName", "lnkMyAccount"), hMenu), 
					"The User Profile pic, User Name and My Account link should be displayed",
					"The User Profile pic, User Name and My Account link are displayed",
					"The User Profile pic, User Name and My Account link are not displayed", driver);
			
			//step 4  
			hMenu.ClickOnMyAccount();
			Log.message(i++ + ". Tabbed on My Account", driver);
			
			Log.softAssertThat(hMenu.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblMyAccountHeader"), hMenu), 
					"My Account label should be displayed in Authenticated Account Menu.", 
					"My Account label displayed in Authenticated Account Menu.",
					"My Account label not displayed in Authenticated Account Menu.", driver);

			Log.softAssertThat(hMenu.verifyBackArrow(), 
					"Back arrow should be displayed near to the My Account label", 
					"Back arrow displayed near to the My Account label", 
					"Back arrow not displayed near to the My Account label", driver);

			Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("lnkProfile","lnkWishList","lnkOrderHistory","lnkRewardPoints","lnkQuickOrder"), hMenu), 
					"Below back arrow my account menu items should be shown", 
					"Below back arrow my account menu items shown", 
					"Below back arrow my account menu items not shown", driver);

			ProfilePage profPage = hMenu.clickOnProfile();
			Log.message(i++ + ". Clicked on Profile Link", driver);

			Log.softAssertThat(profPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), profPage), 
					"Page should be redirected to profile page", 
					"Page redirected to profile page", 
					"Page not redirected to profile page", driver);

			profPage.headers.hamburgerMenu.openCloseHamburgerMenu("open");

			WishListPage wishListPage = hMenu.clickOnWishList();
			Log.message(i++ + ". Clicked on WishList Link", driver);

			Log.softAssertThat(wishListPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), wishListPage), 
					"Page should be redirected to wishlist page", 
					"Page redirected to wishlist page", 
					"Page not redirected to wishlist page", driver);

			wishListPage.headers.hamburgerMenu.openCloseHamburgerMenu("open");

			TrackOrderPage orderPage = hMenu.clickOnOrderHistory();
			Log.message(i++ + ". Clicked on Order History Link", driver);

			Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage), 
					"Page should be redirected to Order History page", 
					"Page redirected to Order History page", 
					"Page not redirected to Order History page", driver);

			orderPage.headers.hamburgerMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger Menu opened.", driver);

			orderPage.headers.hamburgerMenu.ClickOnMyAccount();
			Log.message(i++ + ". My Account Menu opened.", driver);
			
			Headers headers = orderPage.headers;
			/*RewardPointsPage rewardsPage = hMenu.clickOnRewards();
			Log.message(i++ + ". Clicked on Rewards Link", driver);

			Log.softAssertThat(rewardsPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), rewardsPage), 
					"Page should be redirected to Rewards page", 
					"Page redirected to Rewards page", 
					"Page not redirected to Rewards page", driver);

			rewardsPage.headers.hamburgerMenu.openCloseHamburgerMenu("open");
			rewardsPage.headers.hamburgerMenu.ClickOnMyAccount();*/
			headers.hamburgerMenu.clickOnBackArrow();
			Log.message(i++ + ". Clicked on Back Arrow", driver);			

			Log.softAssertThat(headers.hamburgerMenu.elementLayer.VerifyPageElementDisplayed(Arrays.asList("navigationCategory1"), headers.hamburgerMenu), 
					"Page should be redirected to global navigation menu state.", 
					"Page redirected to global navigation menu state.", 
					"Page not redirected to global navigation menu state.", driver);

			headers.hamburgerMenu.ClickOnMyAccount();
			Log.message(i++ + ". Clicked on My Account", driver);

			Log.softAssertThat(headers.hamburgerMenu.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkSignOut"), headers.hamburgerMenu), 
					"Sign out link should be displayed", 
					"Sign out link displayed", 
					"Sign out link not displayed", driver);

			headers.hamburgerMenu.clickOnSignOut();
			Log.message(i++ + ". Clicked on Sign-out link", driver);

			headers.hamburgerMenu.openCloseHamburgerMenu("open");
			Log.softAssertThat(headers.hamburgerMenu.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkSignIn"), headers.hamburgerMenu), 
					"Sign-In Link should be displayed", 
					"Sign-In Link displayed", 
					"Sign-In Link not displayed", driver);
			
			//step-11
			Log.message("Not verified due to waiting time constraints");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_GLOBALNAV_C19644


}// search
