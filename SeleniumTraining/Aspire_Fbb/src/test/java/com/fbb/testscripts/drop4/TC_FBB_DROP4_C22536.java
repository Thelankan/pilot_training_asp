package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22536 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22536(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_promo-prd-level");
		String promoCode = prdData.get("cpn_freefixed");
	
		//Create web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Load Regular Price Product
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.getProductName();	
	
			String selectedSize = pdpPage.selectSize();
			Log.message(i++ + ". Size Selected :: '"+selectedSize+"'", driver);
	
			String selectedColor = pdpPage.selectColor();
			Log.message(i++ + ". Selected Color :: '"+selectedColor+"'");
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add to cart button for 1 st product");
	
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Clicked on 'X' in mini cart overlay");		
	
			ShoppingBagPage shoppingBag = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on 'My Bag' icon in header");
	
			Log.message("Refer TC# 22533 for Step 1");
			
			double unitPrice = shoppingBag.getUnitPrice();
			
			shoppingBag.clickOnSplProductEdit();
			
			if(Utils.isDesktop()) {
				shoppingBag.selectQtyInEditOverlay(3);
			} else {
				pdpPage.selectQty("3");
			}
			
			pdpPage.clickAddProductToBag();
			
			double SubtotalBefApplyPrCde = Math.round(shoppingBag.getUpdatedSubtotalPrice());
			
			shoppingBag.enterPromoCode(promoCode);
			Log.message(i++ + ". Entered Promo Code '"+promoCode+"' ");
	
			shoppingBag.clickOnApplycouponButton();
	
			double SubtotalAfApplyPrCde = Math.round(shoppingBag.getUpdatedSubtotalPrice());
	
			//Step - 2:
			Log.softAssertThat(shoppingBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtUnitPrice", "promotionCalloutMessage", shoppingBag),
					"Promotion Callout Message should be displayed below the 'PRICE' field",
					"Promotion Callout Message is displayed below the 'PRICE' field",
					"Promotion Callout Message is not displayed below the 'PRICE' field", driver);
	
			if(Utils.isMobile()) {
				Log.softAssertThat(shoppingBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "qtyDropDownInCart", "productTotalPrice", shoppingBag), 
						"Item Total should be displayed to the right hand side of the Quantity Selected.", 
						"Item Total is displayed to the right hand side of the Quantity Selected.", 
						"Item Total is not displayed to the right hand side of the Quantity Selected", driver);
			} else {
				Log.softAssertThat(shoppingBag.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productTotalPrice", "sectionQtyDropDown", shoppingBag), 
						"Item Total should be displayed to the right hand side of the Quantity Selected.", 
						"Item Total is displayed to the right hand side of the Quantity Selected.", 
						"Item Total is not displayed to the right hand side of the Quantity Selected", driver);
			}
			
	
			Log.softAssertThat(SubtotalBefApplyPrCde != SubtotalAfApplyPrCde, 
					"Subtotal Price should be updated based on the qty added", 
					"Subtotal Price is updated based on the qty added",
					"Subtotal Price is not updated based on the qty added",
					driver);
	
			shoppingBag.clickOnSplProductEdit();
			
			if(Utils.isDesktop()) {
				shoppingBag.selectQtyInEditOverlay(3);
			} else {
				pdpPage.selectQty("3");
			}
			
			pdpPage.clickAddProductToBag();
	
			Log.softAssertThat(shoppingBag.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtUpdatedSubTotal"), shoppingBag),
					"Discounted Price should be displayed in Shopping bag", 
					"Discounted Price is displayed in Shopping bag", 
					"Discounted Price is not displayed in Shopping bag", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(shoppingBag.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtUpdatedSubTotal", "totalProductPriceUnAdjusted", shoppingBag), 
						"New price should be displayed right side to the old price.", 
						"New price is displayed right side to the old price.", 
						"New price is not displayed right side to the old price", driver);
			} else {
				Log.softAssertThat(shoppingBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "totalProductPriceUnAdjusted", "txtUpdatedSubTotal", shoppingBag), 
						"New price should be displayed below the old price.", 
						"New price is displayed below the old price.", 
						"New price is not displayed below the old price", driver);
			}
			
			Log.softAssertThat(unitPrice == shoppingBag.getUnitPrice(), 
					"Price should be updated based on the qty added", 
					"Price is updated based on the qty added",
					"Price is not updated based on the qty added",driver);
	
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C22536
}// search