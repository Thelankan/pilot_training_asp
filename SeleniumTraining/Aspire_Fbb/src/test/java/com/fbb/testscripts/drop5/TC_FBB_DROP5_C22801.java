package com.fbb.testscripts.drop5;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22801 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	@Test(enabled = false, groups = { "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22801(String browser) throws Exception {
	
		Log.testCaseInfo();
		try {
	
			//step 1
			Log.reference("Verify the functionality of available components in Order Details section is reference to C22671 and C21658");
			Log.message("<br>");
	
			//step 2
			Log.reference("Verify the functionality of available components in Shipment & Line Item Details is covered in C22673 and C22672");
			Log.message("<br>");
	
			//step 3
			Log.reference("Verify the functionality of available components in Sale Pricing & Product Options is covered in C22675");
			Log.message("<br>");
	
			//step 4
			Log.reference("Verify the functionality of available components in Electronic Gift Certificates & Gift Cards is covered in C22677");
			Log.message("<br>");
	
			//step 5
			Log.reference("Verify the functionality of available components in Order Summary & FAQ is covered in C22679");
			Log.message("<br>");
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
		} // catch
		finally {
			Log.endTestCase();
		} // finally
	
	}// TC_FBB_DROP_C21550

}// search
