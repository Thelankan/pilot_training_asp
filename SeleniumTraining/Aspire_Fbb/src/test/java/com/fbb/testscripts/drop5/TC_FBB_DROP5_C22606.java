package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.WishlistLoginPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22606 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22606(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String[] orderDetail = prdData.get("wishList_Friend-1").split("\\|");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			homePage.footers.navigateToWishLists();
			WishlistLoginPage wishlistlogin = new WishlistLoginPage(driver).get();
			Log.message(i++ + ". Navigated to WishList Login Page.", driver);
	
			WishListPage wishlist=wishlistlogin.searchWishlist(orderDetail[0],orderDetail[1],orderDetail[2]);
	
			//Step-1: Verify the functionality of 'Looking for a Wishlist' section
			Log.reference("Verify the functionality of 'Looking for a Wishlist?' section--> Covered in the test case C22635 Step 1 to 6.");
	
			//Step-2: Verify the functionality of Results Found lblResultFound
			if(Utils.isMobile()) {
				Log.softAssertThat(!wishlist.elementLayer.VerifyElementDisplayed(Arrays.asList("lblResultFound"), wishlist), 
						"Results Found heading should not be displayed on Mobile", 
						"Results Found heading is not displayed", 
						"Results Found heading is displayed", driver);
			}
			else {
				Log.softAssertThat(wishlist.elementLayer.verifyVerticalAllignmentOfElements(driver, "searchWishlistBox", "lblResultFound", wishlist), 
						"Results Found Heading should be displayed below the Looking for a Wishlist section ", 
						"Results Found Heading is displayed below the Looking for a Wishlist section ", 
						"Results Found Heading is not displayed below the Looking for a Wishlist section ", driver);
			}		
	
			//Step-3: Verify the functionality of Search Results
			Log.softAssertThat(wishlist.elementLayer.verifyTextContains("firstnameResultFound",orderDetail[0], wishlist)	,					 
					"First Name should match with the Input data entered in the 'Looking for a Friend's wishlist?' Section", 
					"First Name matches with the input data entered", 
					"First Name does not match with the input data entered", driver);	
			
			Log.softAssertThat(wishlist.elementLayer.verifyTextContains("lastnameResultFound",orderDetail[1], wishlist)	,					 
					"Last Name should match with the Input data entered in the 'Looking for a Friend's wishlist?' Section", 
					"Last Name matches with the input data entered", 
					"Last Name does not match with the input data entered", driver);
	
			//Step-4: Verify the functionality of View in Search Results
			Log.softAssertThat(wishlist.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnViewFriendWishlist", "firstnameResultFound", wishlist), 
					"View link/Carat symbol should be displayed at right corner of each row of the search result", 
					"View link/Carat symbol is displayed at right corner of each row of the search result", 
					"View link/Carat symbol is not displayed at right corner of each row of the search result", driver);
			
			
			if(Utils.isMobile()) {
				Log.softAssertThat(wishlist.elementLayer.verifyCssPropertyForElement("btnViewFriendWishlist", "background-image", "back-arrow", wishlist), 
						"A carat symbol should be displayed.", 
						"A carat symbol is displayed.", 
						"A carat symbol is not displayed.", driver);
			}
			else {
				Log.softAssertThat(!wishlist.elementLayer.verifyCssPropertyForElement("btnViewFriendWishlist", "innerHTML", "View", wishlist), 
						"View link should be displayed.", 
						"View link is displayed.", 
						"View link is not displayed.", driver);
			}
			
			Log.reference("overed functionality in the test case id: C22616 Step 1 to 13");
			
			wishlist.clickView();
			Log.softAssertThat(wishlist.elementLayer.verifyAttributeForElement("lblWishlistTitle", "innerHTML", orderDetail[0], wishlist)
						&& wishlist.elementLayer.verifyAttributeForElement("lblWishlistTitle", "innerHTML", orderDetail[1], wishlist)
						&& wishlist.elementLayer.verifyAttributeForElement("lblWishlistTitle", "innerHTML", "'s Wish List", wishlist), 
					"On clicking View, system should navigate the User to public Wishlist page.", 
					"On clicking View, system navigated the User to public Wishlist page.", 
					"On clicking View, system did not navigate the User to public Wishlist page.", driver);
			
			Log.reference("overed functionality in the test case id: C22628 Step 1 to 13");
	
			//Step-5: Verify the functionality of Pagination
			if(Utils.isDesktop()) {
				Log.reference("Covered functionality in the test case id: C19615 Step 1 to 6");
			}
			else {
				Log.reference("Covered functionality in the test case id: C19617 Step 1 to 4");
			}
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} 
	}//TC_FBB_DROP5_C22606

}// search
