package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C21578 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	@SuppressWarnings("unused")
	private static String runPlatfrm = Utils.getRunPlatForm();
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C21578(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("sku_stock<5");
		String btnErrMessage = "PLEASE SELECT COLOR & SIZE";
		String variation = prdData.get("sku_stock<5_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		//new Products(driver).get().setInventory(variation, "inventory_FullBeautyBrands", "4");
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PdpPage for Product :: " + pdpPage.getProductName(), driver);
	
			//Step-1: Verify when the user does not select the required fields & tab on the Add to Bag
			pdpPage.mouseHoverAddToBag();
			if(Utils.isDesktop())
				Log.message(i++ + ". Mouse Hovered on Add to Bag Button!", driver);
			else
				Log.message(i++ + ". Tabbed on Add to Bag Button!", driver);
	
			pdpPage.mouseHoverAddToBag();
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDisabled(Arrays.asList("btnAddToCart"), pdpPage),
					"Add To Bag button should be disabled.",
					"Add to Bag button disabled!",
					"Add to bag Button not disabled.", driver);
	
			Log.softAssertThat(pdpPage.getAdd2BagBtnText().trim().toLowerCase().equals(btnErrMessage.toLowerCase()), 
					"Error Message should be displayed on the Button",
					"Error Message displayed on the Button!",
					"Error Message not displayed on the Button", driver);
	
			pdpPage.headers.redirectToPDP(variation.split("\\|")[0], driver);
			Log.message(i++ + ". Navigated to PDP for product :: " + pdpPage.getProductName());
			
			pdpPage.selectColor(variation.split("\\|")[1]);
			pdpPage.selectSize(variation.split("\\|")[2]);
			
			try {
				pdpPage.selectSizeFamily(variation.split("\\|")[3]);
			} catch (ArrayIndexOutOfBoundsException e) {
				Log.event("No Size family for this product!");
			}
	
			Log.softAssertThat(pdpPage.getQuantityListSize() == pdpPage.getStockValuefrmInventoryMsg(), 
					"When stock availability is less than 10, it should display Quantity values upto inventory value.", 
					"Quantity drop down displays values upto inventory value only.", 
					"Quantity drop down displays more than inventory value.", driver);
			//Step3
			Log.softAssertThat(pdpPage.getQuantityListSize() == pdpPage.getStockValuefrmInventoryMsg(), 
					"Quantity drop down should displayed based the inventory items available.", 
					"Quantity drop down is displayed based the inventory items available.", 
					"Quantity drop down is not displayed based the inventory items available.", driver);
	
			pdpPage.selectMaxQty();
			Log.message(i++ + ". Maximum Quantity Selected.", driver);
	
			pdpPage.addProductToBag();
			pdpPage.addProductToBag();
	
			ShoppingBagPage cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Cart Page.", driver);
	
			Log.softAssertThat(cartPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("qtyError"), cartPage),
					"The system will display an error message",
					"The system will display an error message",
					"The system will display an error message", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP2_C21578

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP3_C21578(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String variation2 = prdData.get("prd_variation");
	
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Step-2:Verify the functionality when the user reaches the maximum 40 items in the bag.
			PdpPage	pdpPage = homePage.headers.navigateToPDP(variation2);
			Log.message(i++ + ". Navigated to PdpPage for Product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.selectColor();
			pdpPage.selectSize();
			int mcQty = 0;
			int selectedQty = 1;
			do{
				selectedQty = Integer.parseInt(pdpPage.selectMaxQty());
				
				if((mcQty + selectedQty) > 40) {
					pdpPage.selectQty(Integer.toString(40 - mcQty));
				}
				
				pdpPage.clickAddProductToBag();
				pdpPage.closeAddToBagOverlay();
				mcQty = Integer.parseInt(pdpPage.headers.getMiniCartCount());
			}while(mcQty < 40);
			
			Log.message(i++ + ". Added 40 Items to Shopping Cart.", driver);
			
	
			pdpPage.selectQty("3");
			pdpPage.clickAddProductToBag();
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblMaxCartError"), pdpPage) && 
					pdpPage.elementLayer.verifyCssPropertyForElement("lblMaxCartError", "display", "block", pdpPage), 
					"If the user attempts to add more quantity than is ATS, Error message should be displayed",
					"Error message is displayed",
					"Error message not displayed", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP2_C21578
	
}// search
