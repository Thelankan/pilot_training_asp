package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlatinumCardLandingPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22727 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22727(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		String propertyText = demandwareData.get("PlatinumcardExclusiveBenefitsMessage");
		String errorColorCode = prdData.get("error_color_1");
		
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
	
			MyAccountPage myAccountPage = homePage.headers.signInToMyAccountPage(username, password);
			Log.message(i++ +". Logged into Authenticated User account ",driver);
	
			PlatinumCardLandingPage platinumCreditCard = myAccountPage.footers.navigateToPlatinumCreditCardLanding();
			Log.message(i++ +". Navigated to PLCC lnading page.",driver);
	
			//Step-1: Verify the display of Top Content Slot
			Log.softAssertThat(platinumCreditCard.elementLayer.verifyPageElements(Arrays.asList("topContentSlot"), platinumCreditCard), 
					"Top content slot should be displayed", 
					"Top content slot is displayed", 
					"Top content slot not displayed", driver);
	
			Log.softAssertThat(platinumCreditCard.verifyTopContentSlotBelowPromotionalContent(), 
					"Top Content Slot should be displayed Below promotional content ", 
					"Top Content Slot is displayed Below promotional content ", 
					"Top Content Slot is not displayed Below promotional content", driver);
	
			// System should react differently based on the Customer Group identified for the User verified in C22734, C22764, C22736  
			//Step-2: Verify the display of Platinum Perks Logo
			Log.softAssertThat(platinumCreditCard.elementLayer.verifyPageElements(Arrays.asList("platinumPerksLogos"), platinumCreditCard), 
					"Platinum perks logos should be displayed", 
					"Platinum perks logos is displayed", 
					"Platinum perks logos not displayed", driver);
			
			Log.softAssertThat(platinumCreditCard.verifyPlatinumPerksLogosBelowTopContentSlot(), 
					"Platinum Perks Logos should be displayed Below Top ContentSlot ", 
					"Platinum Perks Logos is displayed Below Top ContentSlot ", 
					"Platinum Perks Logos is displayed Below Top ContentSlot not display", driver);
	
			Log.softAssertThat(platinumCreditCard.verifyplatinumPerksLogosRightOfOrBelowexclusiveBenefits(), 
					"Platinum perks logos should be displayed right of exclusive benefits copy", 
					"Platinum perks logos is displayed right of exclusive benefits copy", 
					"Platinum perks logos is not displayed right of exclusive benefits copy", driver);
	
			//Step-3: Verify the display of Exclusive Benefits Copy
			Log.softAssertThat(platinumCreditCard.elementLayer.verifyPageElements(Arrays.asList("exclusiveBenefitsCopy"), platinumCreditCard), 
					"Exclusive benefits copy should be displayed", 
					"Exclusive benefits copy is displayed", 
					"Exclusive benefits copy not displayed", driver); 
			
			Log.softAssertThat(platinumCreditCard.elementLayer.verifyTextContains("exclusiveBenefitsContentMessage", propertyText, platinumCreditCard), 
					"Exclusive Benefits Copy message should be displayed as given in property", 
					"Exclusive Benefits Copy message is displayed as given in property", 
					"Exclusive Benefits Copy message is not displayed as given in property", driver);
	
			//Step-4: Verify the display and functionality of Email Signup
			Log.softAssertThat(platinumCreditCard.elementLayer.verifyPageElements(Arrays.asList("emailSignup"), platinumCreditCard), 
					"Email Signup should be displayed", 
					"Email Signup is displayed", 
					"Email Signup not displayed", driver); 
			
			Log.softAssertThat(platinumCreditCard.verifyEmailAddressTextBoxBelowExclusiveBenefits(), 
					"Email address textbox should be displayed below exclusive benefits", 
					"Email address textbox is displayed below exclusive benefits", 
					"Email address textbox is not displayed below exclusive benefits", driver);
	
			Log.softAssertThat(platinumCreditCard.verifySubmitRightOfEmailTextField(), 
					"Email Signup button should be displayed attached to the right of Email Address field", 
					"Email Signup button is displayed attached to the right of Email Address field", 
					"Email Signup button is not displayed attached to the right of Email Address field", driver);
	
			Log.softAssertThat(platinumCreditCard.EnterEmailAddressInTextBox().equals("automation@yopmail.com"),
					"Error address field should be editable", 
					"Error address field is editable",
					"Error address field is not editable", driver);
	
			Log.softAssertThat(platinumCreditCard.verifyEmailFieldBlankError(errorColorCode), 
					"Error message should be displayed when email address field is blank", 
					"Error message is displayed when email address field is blank", 
					"Error message is not displayed when email address field is blank", driver);
	
			Log.softAssertThat(platinumCreditCard.getDisplayedEmailErrorMessage().equals("Please enter a valid email."), 
					"Appropriate Error message should be displayed when entering invalid Email", 
					"Appropriate Error message is displayed when entering invalid Email",
					"Appropriate Error message is not displayed when entering invalid Email", driver);
	
			Log.softAssertThat(platinumCreditCard.verifyEmailPlaceholderMovestotheTop(), 
					"Email Address Place Holder text should be displayed at the top", 
					"Email Address Place Holder text is displayed at the top",
					"Email Address Place Holder text is not displayed at the top", driver);
	
			platinumCreditCard.clickOnSignUp();
			Log.softAssertThat(platinumCreditCard.elementLayer.verifyPageElements(Arrays.asList("thankYouMessage"), platinumCreditCard),
					"If user enters valid email address then Thankyou message should be returned", 
					"If user enters valid email address then Thankyou message is returned",
					"If user enters valid email address then Thankyou message is not returned", driver);
	
			//Step-5: Verify the display of Bottom Content Slot
			Log.softAssertThat(platinumCreditCard.elementLayer.verifyPageElements(Arrays.asList("freeShippingBlock","bottomContentSlot"), platinumCreditCard), 
					"Bottom content slot should be displayed", 
					"Bottom content slot is displayed", 
					"Bottom content slot not displayed", driver);
			
			Log.softAssertThat(platinumCreditCard.verifyBottomContentSlotBelowPlatinumLogo(), 
					"Bottom content slot should be displayed below Platinum Logo", 
					"Bottom content slot is displayed below Platinum Logo", 
					"Bottom content slot is not displayed below Platinum Logo", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

}// search
