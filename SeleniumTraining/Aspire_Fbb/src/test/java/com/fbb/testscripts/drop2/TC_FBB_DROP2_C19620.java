package com.fbb.testscripts.drop2;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19620 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19620(String browser) throws Exception {
		Log.testCaseInfo();
		
		String category1 = prdData.get("level-1");
		String refinement = "Price";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			//Navigate to PLP Page for given category
			PlpPage plpPage = homePage.headers.navigateTo(category1);
			Log.message(i++ + ". Navigated to PLP page for category :: " + category1, driver);

			Log.message("<br>");
			Log.message("<b>Expected Result : </b>Refinement options should be displayed.");
			Log.softAssertThat(plpPage.getRefinementList().isEmpty() == false,
					"<b>Actual Result : </b>Refinement options are displayed.",
					"<b>Actual Result : </b>Refinement options are not displayed.", driver);

			if(Utils.getRunBrowser(driver).equals("firefox"))
			{
				Log.message("<br>");
				Log.message("<b>Expected Result : </b>By default, refinement name should be displayed with White back ground with black text.");
				Log.softAssertThat(plpPage.elementLayer.verifyCssPropertyForListElement("lstRefinementFilterOptionsInMobile",
						"background", "transparent", plpPage) && 
						plpPage.elementLayer.verifyCssPropertyForListElement("lstRefinementFilterOptionsInMobile",
								"color", "rgba(0, 0, 0, 1)", plpPage) ,
						"<b>Actual Result : </b>Refinement name displayed as expected!",
						"<b>Actual Result : </b>Refinement name not displayed as Expected.", driver);
			}
			else
			{
				Log.message("<br>");
				Log.message("<b>Expected Result : </b>By default, refinement name should be displayed with White back ground with black text.");
				Log.softAssertThat(plpPage.elementLayer.verifyCssPropertyForListElement("lstRefinementFilterOptionsInMobile",
						"background", "rgba(0, 0, 0, 0)", plpPage) && 
						plpPage.elementLayer.verifyCssPropertyForListElement("lstRefinementFilterOptionsInMobile",
								"color", "rgba(0, 0, 0, 1)", plpPage) ,
						"<b>Actual Result : </b>Refinement name displayed as expected!",
						"<b>Actual Result : </b>Refinement name not displayed as Expected.", driver);
			}

			Log.message("<br>");
			Log.message("<b>Expected Result : </b>In horizontal refinement near attribute name downward arrow should be display.");
			Log.softAssertThat(plpPage.elementLayer.verifyPageListElements(Arrays.asList("lstRefinementDownArrow"), plpPage),
					"<b>Actual Result : </b>Downward arrow mark icon displayed!",
					"<b>Actual Result : </b>Downward arrow mark icon not displayed", driver);

			//Step-2: Click on downward arrow mark near the attribute name
			plpPage.refinements.clickOnRefinementToggle(refinement);
			Log.message(i++ + ". Clicking on Refinement :: " + refinement, driver);

			if(Utils.getRunBrowser(driver).toLowerCase().contains("edge"))
			{
				Log.fail("The refinement is not expeneded if resolution is 1024x768 or less : PXSFCC-3827");
			}
			Log.message("<br>");
			Log.message("<b>Expected Result : </b>System should be expand the refinement to display the values");
			Log.softAssertThat(plpPage.refinements.getRefinementStatus(refinement).equals("expanded"),
					"<b>Actual Result : </b>System expanded and refinement displays the values",
					"<b>Actual Result : </b>System did not expanded.", driver);

			if(Utils.getCurrentBrand().equals(Brand.rm))
			{
				Log.message("<br>");
				Log.message("<b>Expected Result : </b>In horizontal refinement selected attribute name should be displayed with black ground with white text");
				Log.softAssertThat(plpPage.elementLayer.verifyCssPropertyForElement("drpExpandedRefinement", "color", "rgba(255, 255, 255, 1)", plpPage),
						"<b>Actual Result : </b>In horizontal refinement selected attribute name displayed with black ground with white text",
						"<b>Actual Result : </b>In horizontal refinement selected attribute name not displayed with black ground with white text", driver);

			}
			else
			{
				Log.message("<br>");
				Log.message("<b>Expected Result : </b>In horizontal refinement selected attribute name should be displayed with black ground with white text");
				Log.softAssertThat(plpPage.elementLayer.verifyCssPropertyForElement("drpExpandedRefinement", "color", "rgba(255, 255, 255, 1)", plpPage),
						"<b>Actual Result : </b>In horizontal refinement selected attribute name displayed with black ground with white text",
						"<b>Actual Result : </b>In horizontal refinement selected attribute name not displayed with black ground with white text", driver);

			}

			Log.message("<br>");
			Log.message("<b>Expected Result : </b>In horizontal refinement near selected attribute name upward arrow mark should be shown.");
			Log.softAssertThat(plpPage.refinements.getRefinementArrowState(refinement).get(1).equals("upward"),
					"<b>Actual Result : </b>In horizontal refinement near selected attribute name upward arrow mark shown.",
					"<b>Actual Result : </b>In horizontal refinement near selected attribute name upward arrow mark not shown.", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result : </b>System should display the count of applied refinement values next to the refinement heading");
			Log.softAssertThat(plpPage.elementLayer.verifyPageListElements(Arrays.asList("lblRefinementCountDesktop"), plpPage),
					"<b>Actual Result : </b>System displays the count of applied refinement values next to the refinement heading",
					"<b>Actual Result : </b>System does not displays the count of applied refinement values next to the refinement heading", driver);

			//Step-3: Select sub categories values from the drop down box.
			String selectedSubRef1 = plpPage.refinements.selectRandomSubRefinementOf(refinement);			
			Log.message(i++ + ". Clicking Sub-Refinement(" + selectedSubRef1 + ") on Refinement" + refinement);

			Log.message("<br>");
			Log.message("<b>Expected Result : </b>Available product count should be displayed below the horizontal refinement pane.");
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("lblResultCountDesktop"), plpPage),
					"<b>Actual Result : </b>Available product count displayed below the horizontal refinement pane.",
					"<b>Actual Result : </b>Available product count not displayed below the horizontal refinement pane.", driver);

			//Step-4: Select multiple sub categories values from dropdown box
			String selectedSubRef2 = plpPage.refinements.selectSubRefinementOf(refinement,1);			
			Log.message(i++ + ". Clicking Sub-Refinement(" + selectedSubRef1 + "," + selectedSubRef2 + ") on Refinement" + refinement, driver);

			Log.message("<br>");
			Log.message("<b>Expected Result : </b>Available product count should be displayed below the horizontal refinement pane.");
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("lblResultCountDesktop"), plpPage),
					"<b>Actual Result : </b>Available product count displayed below the horizontal refinement pane.",
					"<b>Actual Result : </b>Available product count not displayed below the horizontal refinement pane.", driver);

			//Step-5: Click on upward arrow mark near attribute name by without uncheck any selected value
			List<String> prdListBeforeClosing = plpPage.getProductNames();
			int prdCountBeforeClosing = plpPage.getProductTileCount();
			plpPage.refinements.closeOpenedRefinement();
			List<String> prdListAfterClosing = plpPage.getProductNames();
			int prdCountAfterClosing = plpPage.getProductTileCount();
			//Log.message(i++ + ". Refinement Dropdown closed!", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result : </b>Opened refinement dropdown box should be properly closed");
			Log.softAssertThat(plpPage.refinements.verifyRefinementSection(refinement).equals("closed"),
					"<b>Actual Result : </b>Opened refinement dropdown box properly closed",
					"<b>Actual Result : </b>Opened refinement dropdown box not closed", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result : </b>Products should be remains as same in product list page");
			Log.softAssertThat(prdListBeforeClosing.equals(prdListAfterClosing),
					"<b>Actual Result : </b>Products remains as same in product list page",
					"<b>Actual Result : </b>Products changed in product list page", driver);

			Log.message("<br>");
			Log.message("<b>Expected Result : </b>Available product count should remain as same in product list page");
			Log.softAssertThat(prdCountBeforeClosing == prdCountAfterClosing,
					"<b>Actual Result : </b>Available product count remains as same in product list page",
					"<b>Actual Result : </b>Available product count changed in product list page", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_DROP2_C19620


}// search


