package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22572 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22572(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
		String username;
		String password = accountData.get("password_global");
	
		String address_1 = checkoutProperty.get("valid_address7");
		String address_2 = checkoutProperty.get("valid_address8");
		String nickName = "nick";
		String firstName = address_1.split("\\|")[7];
		String lastName = address_1.split("\\|")[8];
		String address1 = address_1.split("\\|")[0];
		String address2 = address_1.split("\\|")[1];
		String city = address_1.split("\\|")[2];
		String zipcode = address_1.split("\\|")[4];
		String phone = address_1.split("\\|")[5];
		String giftMessage = "Message-1";
	
		String nicknameNew = "newNick";
		String phoneNew = address_2.split("\\|")[5];
		String cityNew = address_2.split("\\|")[2];
		String zipcodeNew = address_2.split("\\|")[4];
		String giftMessageNew = "Message-2";
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
	
			AddressesPage addressesPage = myAcc.clickOnAddressLink();
			Log.message(i++ + ". Navigated to Addresses Page!", driver);
	
			addressesPage.deleteAllTheAddresses();
			Log.message(i++ + ". Deleted saved addresses!", driver);
	
			ShoppingBagPage cartPage = addressesPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			cartPage.removeAllItemsFromCart();
			Log.message(i++ + ". Removed All Items from the Cart.", driver);
	
			//#item 1 - In stock
			PdpPage pdpPage = cartPage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBag();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			//Navigate to cart
			cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			CheckoutPage checkoutPage = (CheckoutPage)cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
	
			//Enter all shipping details
			checkoutPage.typeTextInNickNameField(nickName);
			Log.message(i++ + ". Typed nickname.", driver);
	
			checkoutPage.typeTextInFirstNameField(firstName);
			Log.message(i++ + ". Typed first name.", driver);
	
			checkoutPage.typeTextInLastNameField(lastName);
			Log.message(i++ + ". Typed last name.", driver);
	
			checkoutPage.typeTextInAddressLine1Field(address1);
			Log.message(i++ + ". Typed address 1.", driver);
	
			checkoutPage.typeTextInAddressLine2Field(address2);
			Log.message(i++ + ". Typed address 2.", driver);
	
			checkoutPage.typeTextInZipcodeField(zipcode);
			Log.message(i++ + ". Typed zipcode.", driver);
	
			checkoutPage.typeTextInCityField(city);
			Log.message(i++ + ". Typed city.", driver);
	
			checkoutPage.typeTextInPhoneNumberField(phone);
			Log.message(i++ + ". Typed phone number.", driver);
	
			checkoutPage.selectState("Colorado");
			Log.message(i++ + ". Selected state.", driver);
	
			checkoutPage.selectCountry(0);
			Log.message(i++ + ". Selected country.", driver);
	
			checkoutPage.checkUncheckSaveThisAddress(false);
			Log.message(i++ + ". DeSelected Save This Address chkbox.", driver);
	
			checkoutPage.checkUncheckGiftMessage(true);
			Log.message(i++ + ". Selected gift message checkbox.", driver);
	
			checkoutPage.typeGiftMessage(giftMessage);
			Log.message(i++ + ". Typed the gift message.", driver);
	
			checkoutPage.checkUncheckGiftReceipt(true);
			Log.message(i++ + ". Selected gift receipt checkbox.", driver);
	
			checkoutPage.clickOnContinue();
			Log.message(i++ + ". Clicked Continue.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("divShippingAddress","divDeliveryOptions","divGiftInfo"), checkoutPage), 
					"Check the shipping address, delivery options and gift info are displaying",
					"The shipping address, delivery options and gift info are displaying",
					"The shipping address, delivery options and gift info are not displaying", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkEditBillingAddress", "lblShippingDetailsHeading", checkoutPage), 
					"Check the Edit button is displaying right side to the shipping address heading",
					"The Edit button is displaying right side to the shipping address heading",
					"The Edit button is not displaying right side to the shipping address heading", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblShippingDetailsHeading", "divShippingAddress", checkoutPage), 
					"Check the shipping address is displaying below to the shipping address heading",
					"The shipping address is displaying below to the shipping address heading",
					"The shipping address is not displaying below to the shipping address heading", driver);
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divShippingAddress", "divDeliveryOptions", checkoutPage), 
						"Check the delivery options is displaying below to the shipping address",
						"The delivery options is displaying below to the shipping address",
						"The delivery options is not displaying below to the shipping address", driver);
	
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divDeliveryOptions", "divGiftInfo", checkoutPage), 
						"Check the delivery options is displaying above to the gift info",
						"The delivery options is displaying above to the gift info",
						"The delivery options is not displaying above to the gift info", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divDeliveryOptions", "divShippingAddress", checkoutPage), 
						"Check the delivery options is displaying right side to the shipping address",
						"The delivery options is displaying right side to the shipping address",
						"The delivery options is not displaying right side to the shipping address", driver);
	
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divGiftInfo", "divDeliveryOptions", checkoutPage), 
						"Check the delivery options is displaying left side to the gift info",
						"The delivery options is displaying left side to the gift info",
						"The delivery options is not displaying left side to the gift info", driver);
			}
	
			//1 - Verify the functionality of the Edit Button
			//Without Save Address
			checkoutPage.clickOnEditAddress();
			Log.message(i++ + ". Clicked Edit Address.", driver);
	
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("drpSelectAddressMobile"), checkoutPage),
						"The Select Address dropdown should not be displayed",
						"The Select Address dropdown is not displayed",
						"The Select Address dropdown is displayed", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("drpSelectAddress"), checkoutPage),
						"The Select Address dropdown should not be displayed",
						"The Select Address dropdown is not displayed",
						"The Select Address dropdown is displayed", driver);
			}
	
			Log.softAssertThat(checkoutPage.getNickName().equalsIgnoreCase(nickName),
					"The Nick Name should be displayed correctly",
					"The Nick Name is displayed correctly",
					"The Nick Name is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getFirstName().equalsIgnoreCase(firstName),
					"The First Name should be displayed correctly",
					"The First Name is displayed correctly",
					"The First Name is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getLastName().equalsIgnoreCase(lastName),
					"The Last Name should be displayed correctly",
					"The Last Name is displayed correctly",
					"The Last Name is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getAddressLine1().equalsIgnoreCase(address1),
					"The Address Line1 should be displayed correctly",
					"The Address Line1 is displayed correctly",
					"The Address Line1 is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getAddressLine2().equalsIgnoreCase(address2),
					"The Address Line2 should be displayed correctly",
					"The Address Line2 is displayed correctly",
					"The Address Line2 is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getZipcode().equalsIgnoreCase(zipcode),
					"The Zipcode should be displayed correctly",
					"The Zipcode is displayed correctly",
					"The Zipcode is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getCity().equalsIgnoreCase(city),
					"The City should be displayed correctly",
					"The City is displayed correctly",
					"The City is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getPhoneNumber().equalsIgnoreCase(phone),
					"The Phone number should be displayed correctly",
					"The Phone number is displayed correctly",
					"The Phone number is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getGiftMessage().equalsIgnoreCase(giftMessage),
					"The Gift Message should be displayed correctly",
					"The Gift Message is displayed correctly",
					"The Gift Message is not displayed correctly", driver);
	
			checkoutPage.typeTextInNickNameField(nicknameNew);
			Log.message(i++ + ". Typed nickname.", driver);
	
			checkoutPage.typeTextInZipcodeField(zipcodeNew);
			Log.message(i++ + ". Typed zipcode.", driver);
	
			checkoutPage.typeTextInCityField(cityNew);
			Log.message(i++ + ". Typed city.", driver);
	
			checkoutPage.typeTextInPhoneNumberField(phoneNew);
			Log.message(i++ + ". Typed phone number.", driver);
	
			checkoutPage.checkUncheckSaveThisAddress(true);
			Log.message(i++ + ". Selected Save This Address chkbox.", driver);
	
			checkoutPage.checkUncheckGiftMessage(true);
			Log.message(i++ + ". Selected gift message checkbox.", driver);
	
			checkoutPage.typeGiftMessage(giftMessageNew);
			Log.message(i++ + ". Typed the gift message.", driver);
	
			checkoutPage.checkUncheckGiftReceipt(true);
			Log.message(i++ + ". Selected gift receipt checkbox.", driver);
	
			checkoutPage.checkUncheckExpressDelivery(true);
			Log.message(i++ + ". Selected 2-Day Express checkbox.", driver);
	
			checkoutPage.clickOnContinue();
			Log.message(i++ + ". Clicked Continue.", driver);
	
	
			Log.softAssertThat(checkoutPage.getSavedNickname().equalsIgnoreCase(nicknameNew),
					"The modified nick name should be displayed",
					"The modified nick name is displayed",
					"The modified nick name is not displayed", driver);
	
			Log.softAssertThat(checkoutPage.getSavedCity().toLowerCase().contains(cityNew.toLowerCase()),
					"The modified city should be displayed",
					"The modified city is displayed",
					"The modified city is not displayed", driver);
	
			Log.softAssertThat(checkoutPage.getSavedZipcode().toLowerCase().contains(zipcodeNew.toLowerCase()),
					"The modified zipcode should be displayed",
					"The modified zipcode is displayed",
					"The modified zipcode is not displayed", driver);
	
			Log.softAssertThat(checkoutPage.getSavedPhoneNumber().equalsIgnoreCase(phoneNew),
					"The modified phone number should be displayed",
					"The modified phone number is displayed",
					"The modified phone number is not displayed", driver);
	
			Log.softAssertThat(checkoutPage.getSavedDeliveryOption().equalsIgnoreCase("Express Delivery"),
					"The modified delivery method should be displayed",
					"The modified delivery method is displayed",
					"The modified delivery method is not displayed", driver);
	
			Log.softAssertThat(checkoutPage.getSavedGiftMessage().equalsIgnoreCase(giftMessageNew),
					"The modified gift message should be displayed",
					"The modified gift message is displayed",
					"The modified gift message is not displayed", driver);
	
			checkoutPage.clickOnEditAddress();
			Log.message(i++ + ". Clicked Edit Address.", driver);
	
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.getValueFromSavedAddressDropdown().equals("("+ nicknameNew +") "+ firstName +" "+ lastName),
						"The address should start with the Nickname, followed by the First and Last names in the saved address dropdown.",
						"The address is displayed correctly",
						"The address is not getting displayed correctly.", driver);
	
				String address = checkoutPage.getFullAddressInMobile();
	
				Log.softAssertThat(address.contains(firstName)
						&&address.contains(lastName)
						&&address.contains(address1)
						&&address.contains(address2)
						&&address.contains(cityNew)
						&&address.contains(zipcodeNew)
						&&address.contains("USA"),
						"The full address should be displayed below the dropdown",
						"The full address is displayed below the dropdown",
						"The full address is not displayed below the dropdown", driver);
			} else {
				String addressCheck = checkoutPage.getValueFromSavedAddressDropdown().toLowerCase();
	
				Log.softAssertThat(addressCheck.contains(nicknameNew.toLowerCase()) 
						&&addressCheck.contains(firstName.toLowerCase())
						&&addressCheck.contains(lastName.toLowerCase())
						&&addressCheck.contains(address1.toLowerCase())
						&&addressCheck.contains(address2.toLowerCase())
						&&addressCheck.contains(zipcodeNew.toLowerCase() + " | USA".toLowerCase())
						&&addressCheck.contains(cityNew.toLowerCase()),
						"The saved address should be displayed",
						"The saved address is displayed",
						"The saved address is not displayed", driver);
			}
	
			Log.softAssertThat(checkoutPage.getNickNameFromDropDown().toLowerCase().contains(nicknameNew.toLowerCase()),
					"The Nick Name should be displayed correctly",
					"The Nick Name is displayed correctly",
					"The Nick Name is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getFirstName().equalsIgnoreCase(firstName),
					"The First Name should be displayed correctly",
					"The First Name is displayed correctly",
					"The First Name is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getLastName().equalsIgnoreCase(lastName),
					"The Last Name should be displayed correctly",
					"The Last Name is displayed correctly",
					"The Last Name is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getAddressLine1().equalsIgnoreCase(address1),
					"The Address Line1 should be displayed correctly",
					"The Address Line1 is displayed correctly",
					"The Address Line1 is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getAddressLine2().equalsIgnoreCase(address2),
					"The Address Line2 should be displayed correctly",
					"The Address Line2 is displayed correctly",
					"The Address Line2 is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getZipcode().equalsIgnoreCase(zipcodeNew),
					"The Zipcode should be displayed correctly",
					"The Zipcode is displayed correctly",
					"The Zipcode is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getCity().equalsIgnoreCase(cityNew),
					"The City should be displayed correctly",
					"The City is displayed correctly",
					"The City is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getPhoneNumber().equalsIgnoreCase(phoneNew),
					"The Phone number should be displayed correctly",
					"The Phone number is displayed correctly",
					"The Phone number is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.getGiftMessage().equals(giftMessageNew),
					"The Gift Message should be displayed correctly",
					"The Gift Message is displayed correctly",
					"The Gift Message is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.verifyExpressDeliveryIsChecked(),
					"The 2-Day Express should be selected",
					"The 2-Day Express is selected",
					"The 2-Day Express is not selected", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C22572
}// search
