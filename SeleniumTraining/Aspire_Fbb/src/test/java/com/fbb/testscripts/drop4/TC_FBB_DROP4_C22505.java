package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22505 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22505(String browser) throws Exception {
		Log.testCaseInfo();
	
		String searchKey = prdData.get("prd_po-box-restricted") + "|" + prdData.get("prd_po-box-unrestricted") + "|" + prdData.get("prd_po-box-restricted-1"); 
		String addressline1 = "PO Box 344";
		String randomFirstName = RandomStringUtils.randomAlphabetic(5).toLowerCase();
		String randomLastName = RandomStringUtils.randomAlphabetic(5).toLowerCase();
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
	
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, AccountUtils.generateEmail(driver));
			CheckoutPage checkout = (CheckoutPage) obj[0];
			i = (Integer) obj[1];
	
			checkout.fillFirstnameShipping(randomFirstName);
			checkout.fillLastnameShipping(randomLastName);
			checkout.enterAddressLine1(addressline1);
			Log.message(i++ +". Filled PO Box address.");
	
			Log.softAssertThat(checkout.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtPoBoxBlockOverlayHeading", "txtPoBoxBlockOverlayIntro", checkout),					 
					"Po Box Block overlay heading should in top of the overlay", 
					"Po Box Block overlay heading is in top of the overlay", 
					"Po Box Block overlay heading is not in top of the overlay", driver);
	
			Log.softAssertThat(checkout.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtPoBoxBlockOverlayIntro", "productNameInPoBoxOverlay", checkout),					 
					"Po Box item should display below the Po Box Block overlay intro", 
					"Po Box item is below Po Box Block overlay intro", 
					"Po Box item is not below Po Box Block overlay intro", driver);
	
			Log.softAssertThat(checkout.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtPoBoxBlockOverlayHeading", "txtPoBoxBlockOverlayIntro", checkout),					 
					"Po Box Block intro should display below the Po Box Block overlay heading", 
					"Po Box Block intro is below Po Box Block overlay heading", 
					"Po Box Block intro is not below Po Box Block overlay heading", driver);
	
			if (Utils.isMobile()) {
				Log.softAssertThat(checkout.elementLayer.verifyVerticalAlignmentOfListElementItem("productNamePoBoxOverlay", checkout),
						"Po Box item should dislpay in Coloumns", 
						"Po Box item is dislpaying in Coloumns", 
						"Po Box item is not dislpaying in Coloumns", driver);
			} else {			
				Log.softAssertThat(checkout.elementLayer.verifyHorizontalAlignmentOfListElementItem("productNamePoBoxOverlay", checkout),
						"Po Box item should dislpay in rows", 
						"Po Box item is dislpaying in rows", 
						"Po Box item is not dislpaying in rows", driver);
			}
	
			Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("btnPoBoxBlockOverlayRemoveItems"), checkout),					 
					"Po Box item should have 'Remove Item' button", 
					"Po Box item is having 'Remove Item' button", 
					"Po Box item is not having 'Remove Item' button", driver);
	
			Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("productColorInPoBoxOverlay","productNameInPoBoxOverlay"
					,"mainImageInPoBoxOverlay","productPriceInPoBoxOverlay"), checkout),					 
					"Po Box item should contains the product item details", 
					"Po Box item is having product item details", 
					"Po Box item is not having product item details", driver);
	
			Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("productShoeSizeInPoBoxOverlay"), checkout) || 
					checkout.elementLayer.verifyPageElements(Arrays.asList("productSizeInPoBoxOverlay"), checkout),					 
					"Po Box item size should be displayed", 
					"Po Box item size is displaying", 
					"Po Box item size is not displaying", driver);
	
			checkout.clickEditShipAddPOBoxOverlay();
			Log.message(i++ +". Clicked \"Edit Shipping Address\" button.", driver);
	
			Log.softAssertThat(checkout.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("divPoBoxBlockOverlay"), checkout),					 
					"Closes the PO Box restriction overlay.", 
					"PO Box overlay is closed.", 
					"PO Box overlay is not closed", driver);
	
			driver.get(driver.getCurrentUrl());
			checkout = new CheckoutPage(driver).get();
			int InitialCount = checkout.itemsinCartCount();
			Log.event("Item count before removal :: " + InitialCount);
			
			checkout.continueToShipping(AccountUtils.generateEmail(driver));
			checkout.enterAddressLine1(addressline1);
			Log.message(i++ + ". PO Box address entered.", driver);
	
			checkout.clickRemoveItemsPOBoxOverlay();
			Log.message(i++ +". Clicked \"Remove Items\" button.", driver);
	
			int FinalCount = checkout.itemsinCartCount();
			Log.event("Item count after removal :: " + FinalCount);
	
			Log.softAssertThat(InitialCount != FinalCount,					 
					"PO Box restricted product should be removed and total should be updated.", 
					"Product removed and total updated.", 
					"Something went wrong.", driver);
			//
			Log.softAssertThat(checkout.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("divPoBoxBlockOverlay"), checkout),					 
					"Closes the PO Box restriction overlay.", 
					"PO Box overlay is closed.", 
					"PO Box overlay is not closed", driver);
			//
			Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("formShippingDetails"), checkout),					 
					"Navigates the User to the Shipping Details section of the Checkout page", 
					"Navigated to Shipping Details", 
					"Could not navigate to Shipping Details", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}
}// search
