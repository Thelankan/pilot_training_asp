package com.fbb.testscripts.drop4;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22501 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22501(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = prdData.get("prd_variation");
		String username = "automationtesting@yopmail.com";
		String password = "test@123";
		String nickName = "Nickname (Default)";
		String firstName = "FirstName";
		String lastName = "LastName";
		
		List<String> elementsToBeVerified = Arrays.asList("txtSavedAddressCount");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
	
			PdpPage pdpPage = myAcc.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBag();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			ShoppingBagPage cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			CheckoutPage checkoutPage = (CheckoutPage)cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
	
			//Step-1: Verify the display of Module Heading
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblShippingDetailsTitle"), checkoutPage), 
					"Module Heading (Shipping Details) should be positioned to the top of section, with the section number and a left indentation", 
					"Module Heading (Shipping Details) is positioned to the top of section, with the section number and a left indentation", 
					"Module Heading (Shipping Details) is not positioned to the top of section, with the section number and a left indentation", driver);
	
			//Step-2: Verify the functionality of Shipping Address Heading
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblShippingDetailsTitle", "lblShippingAddressTitle", checkoutPage),
					"Shipping Address heading should be displayed below the Module Heading (Shipping Details).",
					"Shipping Address heading is displayed below the Module Heading (Shipping Details)",
					"Shipping Address heading is not displayed below the Module Heading (Shipping Details)", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "shippingSection", "lblShippingAddressTitle", checkoutPage)
							&& checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lblShippingAddressTitle", "shippingSection", checkoutPage),
					"Shipping Address should be inside the gray-area section in the page that contains the Shipping Details",
					"Shipping Address is inside the gray-area section in the page that contains the Shipping Details",
					"Shipping Address is not inside the gray-area section in the page that contains the Shipping Details", driver);
	
			//Step-3: Verify the functionality of Edit link
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkShippingAddressEdit", "lblShippingAddressTitle", checkoutPage), 
					"Edit link should be displayed to the right corner of the shipping address heading.", 
					"Edit link is displayed to the right corner of the shipping address heading.", 
					"Edit link is not displayed to the right corner of the shipping address heading.", driver);
			
			checkoutPage.clickEditShippingAddress();
			Log.softAssertThat(checkoutPage.elementLayer.VerifyElementDisplayed(Arrays.asList("txtNickName"), checkoutPage),
					"On clicking 'EDIT' link, Shipping address module should be changed to 'Add Address' module", 
					"On clicking 'EDIT' link, Shipping address module should be changed to 'Add Address' module",
					"On clicking 'EDIT' link, Shipping address module should be changed to 'Add Address' module", driver);
			checkoutPage.clickCancelEditShippingAddress();
			
			//Continued below
			Log.reference("Full functionality covered in the Test Case Id: C22491 Step 1 and 2");
	
			//Step-4: Verify the functionality of Add New Address
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnAddNewShippingAddress", "lnkShippingAddressEdit", checkoutPage), 
					"ADD NEW' Address link should be displayed right to the 'EDIT' Link. Add New should be separated from Edit with a pipe symbol.", 
					"ADD NEW' Address link is displayed right to the 'EDIT' Link. Add New is separated from Edit with a pipe symbol.", 
					"ADD NEW' Address link is not displayed right to the 'EDIT' Link. Add New should be separated from Edit with a pipe symbol.", driver);
			
			//Continued below
			Log.reference("Full functionality covered in the Test Case Id: C22490 Step 1 to 10");
	
			//Step-5: Verify the functionality of Saved Address Count
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblShippingAddressTitle", "txtSavedAddressCount", checkoutPage), 
					"Saved Address Count should be displayed below the Shipping Address Heading.", 
					"Saved Address Count is displayed below the Shipping Address Heading.", 
					"Saved Address Count is not displayed below the Shipping Address Heading.", driver);
	
			//Step-6: Verify the functionality of Collapsed Saved Address Menu
			if(Utils.isMobile())
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayedBelow("txtSavedAddressCount", "drpSelectAddressMobile", checkoutPage),
						"The saved Address dropdown should be displayed below the addresses count",
						"The saved Address dropdown is displayed below the addresses count",
						"The saved Address dropdown is not displayed below the addresses count", driver);
				Log.softAssertThat(checkoutPage.getValueFromSavedAddressDropdown().equalsIgnoreCase(nickName +" "+ firstName +" "+ lastName),
						"The address should start with the Nickname, followed by the First and Last names in the saved address dropdown.",
						"The address is displayed correctly",
						"The address is not getting displayed correctly.", driver);
			}
			else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayedBelow("txtSavedAddressCount", "drpSelectAddress", checkoutPage),
						"The saved Address dropdown should be displayed below the addresses count",
						"The saved Address dropdown is displayed below the addresses count",
						"The saved Address dropdown is not displayed below the addresses count", driver);
			}
			//There are no element property is changing due to collapsing/expanding the dropdown hence blocking the validation
			/*Log.softAssertThat(checkoutPage.isSavedAddressDropdownExpanded() == false,
					"The saved Address dropdown should be collapsed by default",
					"The saved Address dropdown is collapsed by default",
					"The saved Address dropdown is not collapsed by default", driver);*/
	
			if(Utils.getRunBrowser(driver).equalsIgnoreCase("chrome") || Utils.getRunBrowser(driver).equalsIgnoreCase("safari"))
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("drpSelectAddress", "color", "rgba(51, 51, 51, 1)", checkoutPage),
						"The saved Address dropdown should have white background",
						"The saved Address dropdown has white background",
						"The saved Address dropdown does not have white background", driver);
			}
			else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("drpSelectAddress", "#333333", checkoutPage),
						"The saved Address dropdown should have white background",
						"The saved Address dropdown has white background",
						"The saved Address dropdown does not have white background", driver);
			}
	
			Log.softAssertThat(checkoutPage.getValueFromSavedAddressDropdown().contains("Nickname"),
					"The New Address value should be displayed",
					"The New Address value is displayed",
					"The New Address value is not displayed", driver);
	
			//Step-7: Verify the functionality of Full Address (Only for Mobile)
	
			checkoutPage.clickOnSelectedShipingAddress();
			if(Utils.isMobile())
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "addressSelectorShippingListMobile", "selectedShippingFullAddress", checkoutPage), 
						"Full Address should be displayed below the Saved Addresses white box, in comma separated values for the selected address instead of expanding the white box.", 
						"Full Address is displayed below the Saved Addresses white box, in comma separated values for the selected address instead of expanding the white box", 
						"Full Address is not displayed below the Saved Addresses white box, in comma separated values for the selected address instead of expanding the white box", driver);
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "addressSelectorShippingListMobile", "chkUseThisAsBillingAddress", checkoutPage), 
						"The 'Use as Billing Address' checkbox should appear below the Saved Addresses white box.", 
						"The 'Use as Billing Address' checkbox is appear below the Saved Addresses white box.", 
						"The 'Use as Billing Address' checkbox is not appear below the Saved Addresses white box.", driver);
			}
			else
			{
			//Step-8: Verify the functionality of Use as Billing Address
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "selectedOptionShipping", "chkUseThisAsBillingAddress", checkoutPage), 
					"The 'Use as Billing Address' checkbox should appear below the Saved Addresses white box.", 
					"The 'Use as Billing Address' checkbox is appear below the Saved Addresses white box.", 
					"The 'Use as Billing Address' checkbox is not appear below the Saved Addresses white box.", driver);
			}
	
			Log.softAssertThat(checkoutPage.verifyUseThisAsBillingAddressIsChecked(),
					"The Use This As Billing Address should be checked by default",
					"The Use This As Billing Address is checked by default",
					"The Use This As Billing Address is not checked by default", driver);
	
			Log.reference("Step 8 further covered in C22510 Step 1 to 5");
			Log.reference("Step 8 further covered in C22514 Step 1 to 7");
	
			//Step-9: Verify the functionality of Delivery Options Tool Tip
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "chkUseThisAsBillingAddress", "lnkDeliveryOptionsToolTip", checkoutPage), 
					"Delivery Options Heading should be displayed below the 'Use as Billing Address' checkbox.", 
					"Delivery Options Heading is displayed below the 'Use as Billing Address' checkbox.", 
					"Delivery Options Heading is not displayed below the 'Use as Billing Address' checkbox", driver);
	
			checkoutPage.clickDeliveryOptionToolTip();
			Log.message(i++ + ". Clicked on Tool Tip icon.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("divDeliveryOptionsToolTipDialog"), checkoutPage),
					"The Delivery Options tool tip dialog should be displayed",
					"The Delivery Options tool tip dialog is displayed",
					"The Delivery Options tool tip dialog is not displayed", driver);
	
			checkoutPage.closeDeliveryOptionToolTip();;
			Log.message(i++ + ". Closed the Tool Tip.", driver);
	
			//Step-10: Verify the functionality of Shipping Methods
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblDeliveryOptionHeading", "shipingMethod", checkoutPage), 
					"Shipping Methods should be displayed below the Delivery options Heading after a faint line break.", 
					"Shipping Methods is displayed below the Delivery options Heading after a faint line break.", 
					"Shipping Methods is not displayed below the Delivery options Heading after a faint line break.", driver);
			
			Log.reference("Step 10 further covered in the Test Case Id: C22504 Step 1 to 11");
	
			//Step-11: Verify the functionality of Gift Options
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "shipingMethod", "lblGiftOptionHeading", checkoutPage), 
					"Gift Options should be displayed below the Shipping Methods.", 
					"Gift Options is displayed below the Shipping Methods.", 
					"Gift Options is not displayed below the Shipping Methods.", driver);
			
			Log.reference("Step 11 further covered in C22502 Step 1 to 7");
	
			//Step-3 - Verify the functionality of Edit link
			checkoutPage.clickEditShippingAddress();
			Log.message(i++ + ". Clicked on Edit.", driver);
	
			Log.softAssertThat(checkoutPage.getFirstName().equals("") == false
					&&checkoutPage.getLastName().equals("") == false
					&&checkoutPage.getAddressLine1().equals("") == false
					&&checkoutPage.getAddressLine2().equals("") == false
					&&checkoutPage.getZipcode().equals("") == false
					&&checkoutPage.getCity().equals("") == false
					&&checkoutPage.getPhoneNumber().equals("") == false,
					"The Shipping Address should get prepopulated",
					"The Shipping Address is prepopulated",
					"The Shipping Address is not prepopulated", driver);
	
			//Step-4 - Verify the functionality of Add New Address
			checkoutPage.clickCancelEditShippingAddress();
			Log.message(i++ + ". Clicked on Cancel.", driver);
	
			checkoutPage.clickOnAddShippingAddress();
			Log.message(i++ + ". Clicked on Add New.", driver);
	
			Log.softAssertThat(checkoutPage.getValueFromSavedAddressDropdown().equals("New Address"),
					"The New Address value should be displayed",
					"The New Address value is displayed",
					"The New Address value is not displayed", driver);
	
			Log.softAssertThat(checkoutPage.getNickName().equals("")
					&&checkoutPage.getFirstName().equals("")
					&&checkoutPage.getLastName().equals("")
					&&checkoutPage.getAddressLine1().equals("")
					&&checkoutPage.getAddressLine2().equals("")
					&&checkoutPage.getZipcode().equals("")
					&&checkoutPage.getCity().equals("")
					&&checkoutPage.getPhoneNumber().equals(""),
					"The Shipping Address should be empty",
					"The Shipping Address is empty",
					"The Shipping Address is not empty", driver);
	
			checkoutPage.selectValueFromSavedAddressesDropdownByIndex(1);
			Log.message(i++ + ". Selected default address from dropdown.", driver);
	
			//Step-12: Verify the functionality of Continue button
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Clicked Continue button.", driver);
	
			elementsToBeVerified = Arrays.asList("divPaymentDetailsSection");
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(elementsToBeVerified, checkoutPage),
					"The Payment Details section should be displayed",
					"The Payment Details section is displayed",
					"The Payment Details section is not displayed", driver);
	
			Log.reference("Full functionality for PO Box Exception covered in C22563 steps 1-3 and C22505 steps 1-5");
			Log.reference("Full functionality for Shipping Method Exception covered in C22564 steps 1-3 and C22507 steps 1-6");
	
			//Step-13: Verify the functionality of Expanded Saved Address Menu
			checkoutPage.EditShippingAddress();
			Log.message(i++ + ". Clicked Edit button.", driver);
	
			checkoutPage.expandCollapseSavedAddressDropdown(true);
			Log.message(i++ + ". Saved Address dropdown expanded.", driver);
	
			checkoutPage.selectValueFromSavedAddressesDropdownByIndex(2);
			Log.message(i++ + ". Selected new value from dropdown.", driver);
	
			//There are no element property is changing due to collapsing/expanding the dropdown hence blocking the validation
			/*Log.softAssertThat(checkoutPage.isSavedAddressDropdownExpanded() == false,
					"The saved Address dropdown should be collapsed",
					"The saved Address dropdown is collapsed",
					"The saved Address dropdown is not collapsed", driver);*/
	
			Log.softAssertThat(checkoutPage.getValueFromSavedAddressDropdown().contains("Office"),
					"The Newly selected Address value should be displayed",
					"The Newly selected value is displayed",
					"The Newly selected value is not displayed", driver);
	
			checkoutPage.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Selected the Use This As Billing Address checkbox.", driver);
	
			Log.softAssertThat(checkoutPage.verifyUseThisAsBillingAddressIsChecked() == true,
					"The Use This As Billing Address should be selected",
					"The Use This As Billing Address is selected",
					"The Use This As Billing Address is not selected", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C22501
}// search
