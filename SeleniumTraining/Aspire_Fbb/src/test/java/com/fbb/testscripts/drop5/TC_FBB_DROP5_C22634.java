package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.WishlistLoginPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22634 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	@Test(groups = { "medium", "desktop", "tablet", "mobile" },dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22634(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.event("Navigated to: " + driver.getCurrentUrl());
			Log.message(i++ + ". Navigated to PDP Page for product :: " + searchKey, driver);
	
			String size=pdpPage.selectSizeSwatch(1);
			Log.message(i++ +". Selected size :: " + size);
	
			String color = pdpPage.selectColorSwatch(0);
			Log.message(i++ +". Selected Color :: " + color);
	
			pdpPage.selectSizeFamily("Women");
			WishlistLoginPage wishlistlogin=(WishlistLoginPage)pdpPage.addToWishlist();	
	
			//Step-1: Verify the functionality of Create a Wishlist
			if(!Utils.isMobile()) {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "currentBreadCrumb", "headingCreateWishlist", wishlistlogin)
						&& wishlistlogin.elementLayer.verifyHorizontalAllignmentOfElements(driver, "headingCreateWishlist", "accountHeading", wishlistlogin),					 
						"Wishlist heading should be displayed on top right of the Returning Customer section.", 
						"Wishlist heading is displayed on top right of the Returning Customer section.", 
						"Wishlist heading is not displayed on top right of the Returning Customer section.", driver);
			}
			else {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "accountHeading", "headingCreateWishlistMobile", wishlistlogin),					 
						"Wishlist heading should be displayed below the Returning Customer section.", 
						"Wishlist heading is displayed below the Returning Customer section.", 
						"Wishlist heading is not displayed below the Returning Customer section.", driver);
			}
	
			//Step-2: Verify the functionality of Wishlist Benefits
			if(!Utils.isMobile()) {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "headingCreateWishlist", "subHeadingCreateWishlist", wishlistlogin),					 
						"Wishlist Benefits should be displayed below the Create a Wishlist heading.", 
						"Wishlist Benefits is displayed below the Create a Wishlist heading.", 
						"Wishlist Benefits is not displayed below the Create a Wishlist heading.", driver);			
			}
			else {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "headingCreateWishlistMobile", "subHeadingCreateWishlistMobile", wishlistlogin),					 
						"Wishlist Benefits should be displayed below the Create a Wishlist heading.", 
						"Wishlist Benefits is displayed below the Create a Wishlist heading.", 
						"Wishlist Benefits is not displayed below the Create a Wishlist heading.", driver);
			}
			
			//Step-3: Verify the functionality of Wishlist Steps
			if(!Utils.isMobile()) {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "subHeadingCreateWishlist", "wishlistStep", wishlistlogin),					 
						"Wishlist Steps should be displayed below the Create a Wishlist Benefits.", 
						"Wishlist Steps is displayed below the Create a Wishlist Benefits.", 
						"Wishlist Steps is not displayed below the Create a Wishlist Benefits.", driver);	
			}
	
			//Step-4: When user clicks on create account button, system should display the create account form fields.
			if(Utils.isDesktop() || Utils.isTablet()) {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "wishlistStep", "btnCreateAccount_Desktop", wishlistlogin),					 
						"Wishlist Create button should be displayed below the Create a Wishlist Steps.", 
						"Wishlist Create button is displayed below the Create a Wishlist Steps.", 
						"Wishlist Create button is not displayed below the Create a Wishlist Steps.", driver);
			} else {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "divContentAssetMobile", "btnCreateAccount_Mobile", wishlistlogin),					 
						"\"Create Account\" button should be displayed below the \"Create a Wishlist\" message.", 
						"\"Create Account\" button is displayed below the \"Create a Wishlist\" message.", 
						"\"Create Account\" button is not displayed below the \"Create a Wishlist\" message.", driver);
			}
	
			//Step-5:  Verify the display of View more link (Only For Mobile)
			if(Utils.isMobile()) {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnCreateAccount_Mobile", "lnkViewMore", wishlistlogin),					 
						"View more should be displayed below the Create Account button.", 
						"View more is displayed below the Create Account button.", 
						"View more is not displayed below the Create Account button.", driver);
			}
	
			//Step-4
			wishlistlogin.clickOnCreateAccount();
			Log.message(i++ +". Clicked on Create Account button", driver);
	
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("sectionCreateAccFormField"), wishlistlogin),					 
					"Create account form field should displayed", 
					"Create account form field is displayed", 
					"Create account form field is not displayed", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

}// search
