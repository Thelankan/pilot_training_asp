package com.fbb.testscripts.drop2;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19612 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19612(String browser) throws Exception {
		Log.testCaseInfo();

		String level1 = prdData.get("level-2").split("\\|")[0];
		String level2 = prdData.get("level-2").split("\\|")[1];
		String sortOrder = "Lowest Priced";
		String sortOrderNew = "Highest Priced";
		String defaultSortOption = prdData.get("sortOption");
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		int i = 1;
		try {
			//Load Home Page
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver); 

			//Navigate to Category 1 landing Page
			PlpPage plpPage = homePage.headers.navigateTo(level1,level2);
			Log.message(i++ + ". Navigated to Product Listing Page for " + level1 + " >>> " + level2, driver);
			int searchResultLoadTime = plpPage.getSearchResultCount();

			//Step-1: Verify Sort By bar.
			Log.softAssertThat(plpPage.refinements.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "refinementFilterOptionsDesktop", "drpSortByCollapsedDesktop", plpPage.refinements),
					"Sort By option should be displayed below Horizondal Refinement",
					"Sort By option displayed below Horizondal Refinement",
					"Sort By option not displayed below Horizondal Refinement", driver);

			Log.softAssertThat(plpPage.refinements.elementLayer.verifyHorizontalAllignmentOfElements(driver, "drpSortByCollapsedDesktop", "divResultCountDesktop", plpPage.refinements),
					"Sort By option should be displayed to the right of result count",
					"Sort By option is displayed to the right of result count",
					"Sort By option is not displayed to the right of result count", driver);

			Log.softAssertThat(plpPage.getCurrentSortMode().contains(defaultSortOption),
					defaultSortOption + " should be the default sort by option selected.", 
					defaultSortOption + " is the default sort by option selected.", 
					defaultSortOption + " is not the default sort by option selected.", driver);
			
			Log.softAssertThat(plpPage.refinements.verifySortApplied(defaultSortOption, plpPage),
					"The products should be sorted by default with " + defaultSortOption,
					"The products is sorted by default with " + defaultSortOption,
					"The products is not sorted by default with " + defaultSortOption, driver);

			//Step-2: Verify arrow mark in "Sort by"
			plpPage.refinements.openCloseSortBy("expanded");
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("sortbyRecommended", "sortbyLowestPriced", "sortbyHighestPriced", "sortbyTopRated", "sortbyBestMatches"), 
					plpPage), 
					"Correct sort-by options should be displayed.", 
					"Correct sort-by options are displayed.", 
					"Correct sort-by options are not displayed.", driver);

			//Step-3: Verify sort options and arrow mark in "Sort by" box
			plpPage.refinements.openCloseSortBy("collapsed");
			Log.softAssertThat(plpPage.refinements.getSortByState().equals("collapsed"), 
					"Dropdown list should be closed", 
					"Dropdown list is closed", 
					"Dropdown list is not closed", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyCssPropertyForPsuedoElement("selectedSortOption", "before", "background", prdData.get("sortarrow"), plpPage), 
					"Downward arrow mark should be displayed in dropdown box.", 
					"Downward arrow mark is displayed in dropdown box.", 
					"Downward arrow mark is not displayed in dropdown box.", driver);

			//Step-4: User can select only one option from "Sort by" drop-down menu
			String sortOption = plpPage.refinements.selectSortBy(sortOrder);
			Log.message(i++ + ". Sort Option(" + sortOption + ") selected from Sorting menu.", driver);

			Log.softAssertThat(plpPage.refinements.verifySortApplied(sortOption, plpPage),
					"Products should be sorted as per the selected option and listed in PLP",
					"Products sorted as per the selected option",
					"Products not sorted as per the selected sorting option", driver);

			sortOption = plpPage.refinements.selectSortBy(sortOrderNew);
			Log.message(i++ + ". Sort Option(" + sortOption + ") selected from Sorting menu.", driver);

			Log.softAssertThat(plpPage.refinements.verifySortApplied(sortOption, plpPage),
					"Products should be sorted as per the selected option and listed in PLP",
					"Products sorted as per the selected option",
					"Products not sorted as per the selected sorting option", driver);

			//Step-5: Click anywhere outside the drop box in product list page
			plpPage.clickAnywhereInPlpPage();
			Log.softAssertThat(plpPage.refinements.getSortByState().equals("collapsed"), 
					"\"Sort by\" drop box should be closed properly",
					"\"Sort by\" drop box is closed properly",
					"\"Sort by\" drop box is not closed", driver);

			int searchResultFinal = plpPage.getSearchResultCount();
			Log.softAssertThat((searchResultFinal == searchResultLoadTime), 
					"Result count should remains the same in Product List page", 
					"Result count remains the same in Product List page", 
					"Result count does not remain the same in Product List page", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// M1_FBB_DROP2_C19612
	
	@Test(groups = { "critical", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP2_C19612(String browser) throws Exception {
		Log.testCaseInfo();

		String level1 = prdData.get("level-2").split("\\|")[0];
		String level2 = prdData.get("level-2").split("\\|")[1];
		String sortOrder = "Lowest Priced";
		String sortOrderNew = "Highest Priced";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		int i = 1;
		try {
			//Load Home Page
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			//Navigate to Category 1 landing Page
			PlpPage plpPage = homePage.headers.navigateTo(level1,level2);
			Log.message(i++ + ". Navigated to Product Listing Page for " + level1 + " >>> " + level2, driver);
			int searchResultLoadTime = plpPage.getSearchResultCount();

			//Step-1: Verify Sort By bar.
			Log.softAssertThat(plpPage.refinements.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divPromoBanner", "drpSortByCollapsedMobile", plpPage.refinements),
					"The Sort By bar should be located below the content slot",
					"The Sort By bar is located below the content slot",
					"The Sort By bar is not located below the content slot", driver);
			
			if(Utils.isTablet()) {
				Log.softAssertThat(plpPage.refinements.elementLayer.VerifyElementDisplayed(Arrays.asList("drpSortSelectedOption"), plpPage.refinements), 
						"For tablet selected option should be properly displayed in the header.", 
						"For tablet selected option is properly displayed in the header.", 
						"For tablet selected option is not properly displayed in the header.", driver);
				
				int widthRefinementBar = plpPage.refinements.elementLayer.getElementWidth("drpSortByCollapsedMobile", plpPage.refinements);
				plpPage.refinements.openCloseSortBy("expanded");
				int widthSortMenu = plpPage.refinements.elementLayer.getElementWidth("divSortMenu", plpPage.refinements)
							+ Integer.parseInt(plpPage.refinements.elementLayer.getElementCSSValue("divSortMenu", "border-right-width", plpPage.refinements).replaceAll("[^0-9]", ""));
				
				int widthDiff = widthRefinementBar - widthSortMenu;
				
				Log.softAssertThat(widthDiff >= -5 && widthDiff <= 5, 
						"The dropdown should maintain the width of sort by refinement bar header.", 
						"The dropdown should maintain the width of sort by refinement bar header.", 
						"The dropdown should maintain the width of sort by refinement bar header.", driver);
			}
			if(Utils.isMobile()) {
				int widthViewPort = plpPage.refinements.elementLayer.getElementWidth("readyElementDesktop", plpPage.refinements);
				plpPage.refinements.openCloseSortBy("expanded");
				int widthSortMenu = plpPage.refinements.elementLayer.getElementWidth("divSortMenu", plpPage.refinements);
				Log.softAssertThat(widthViewPort == widthSortMenu, 
						"Dropdown width should be 100%that of the viewport.", 
						"Dropdown width is 100%that of the viewport.", 
						"Dropdown width is not 100%that of the viewport.", driver);
			}

			//Step-2: Verify arrow mark in "Sort by"
			plpPage.refinements.openCloseSortBy("expanded");
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(
						Arrays.asList("sortbyRecommended", "sortbyLowestPriced", "sortbyHighestPriced", "sortbyTopRated", "sortbyBestMatches"), plpPage), 
					"Correct sort-by options should be displayed.", 
					"Correct sort-by options are displayed.", 
					"Correct sort-by options are not displayed.", driver);

			//Step-3: Verify sort options and arrow mark in "Sort by" box
			plpPage.refinements.openCloseSortBy("collapsed");
			Log.softAssertThat(plpPage.refinements.getSortByState().equals("collapsed"), 
					"Dropdown list should be closed", 
					"Dropdown list is closed", 
					"Dropdown list is not closed", driver);

			if(Utils.getCurrentBrand().equals(Brand.ks)) { 
				Log.softAssertThat(plpPage.elementLayer.verifyCssPropertyForPsuedoElement("sortbydropdown_Mob_Tab", "before", "background", "carat-down", plpPage), 
						"Downward arrow mark should be displayed in dropdown box.", 
						"Downward arrow mark is displayed in dropdown box.", 
						"Downward arrow mark is not displayed in dropdown box.", driver);
			} else {
				Log.softAssertThat(plpPage.elementLayer.verifyCssPropertyForPsuedoElement("sortbydropdown_Mob_Tab", "before", "background", "down-arrow", plpPage), 
						"Downward arrow mark should be displayed in dropdown box.", 
						"Downward arrow mark is displayed in dropdown box.", 
						"Downward arrow mark is not displayed in dropdown box.", driver);
			}
			

			//Step-4: User can select only one option from "Sort by" drop-down menu
			String sortOption = plpPage.refinements.selectSortBy(sortOrder);
			Log.message(i++ + ". Sort Option(" + sortOption + ") selected from Sorting menu.");

			Log.softAssertThat(plpPage.refinements.verifySortApplied(sortOption, plpPage),
					"Products should be sorted as per the selected option and listed in PLP",
					"Products sorted as per the selected option",
					"Products not sorted as per the selected sorting option", driver);

			sortOption = plpPage.refinements.selectSortBy(sortOrderNew);
			Log.message(i++ + ". Sort Option(" + sortOption + ") selected from Sorting menu.");

			Log.softAssertThat(plpPage.refinements.verifySortApplied(sortOption, plpPage),
					"Products should be sorted as per the selected option and listed in PLP",
					"Products sorted as per the selected option",
					"Products not sorted as per the selected sorting option", driver);

			//Step-5: Click anywhere outside the drop box in product list page
			plpPage.clickAnywhereInPlpPage();
			Log.softAssertThat(plpPage.refinements.getSortByState().equals("collapsed"), 
					"\"Sort by\" drop box should be closed properly",
					"\"Sort by\" drop box is closed properly",
					"\"Sort by\" drop box is not closed", driver);

			int searchResultFinal = plpPage.getSearchResultCount();
			Log.softAssertThat((searchResultFinal == searchResultLoadTime), 
					"Result count should remains the same in Product List page", 
					"Result count remains the same in Product List page", 
					"Result count does not remain the same in Product List page", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// M2_FBB_DROP2_C19612


}// search


