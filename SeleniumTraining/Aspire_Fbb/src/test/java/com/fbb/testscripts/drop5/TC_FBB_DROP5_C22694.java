package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22694 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22694(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		//Test data
		String bgColor = prdData.get("background_color");
		
		{
			GlobalNavigation.registerNewUser(driver, 3, 0, email + "|" + password);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			MyAccountPage myAcc = headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged in to user account.", driver);
			
			AddressesPage addrPage = myAcc.navigateToAddressPage();
			Log.message(i++ + ". Navigated to address page.", driver);
			
			int addressesSaved = addrPage.getSavedAddressesCount();
			int cardIndex = Utils.getRandom(1, addressesSaved);
			
			//Step-1: Verify the functionality of Delete
			if(Utils.isDesktop()) {
				Log.softAssertThat(addrPage.verifyEditDeleteRelativePlacement(), 
						"Delete button should be displayed at the right of Edit in the respective saved address card", 
						"Delete button is displayed at the right of Edit in the respective saved address card", 
						"Delete button is not displayed at the right of Edit in the respective saved address card", driver);
			}else {
				Log.softAssertThat(addrPage.verifyEditDeleteRelativePlacement(), 
						"X button should be displayed on top right corner of the respective saved address card.", 
						"X button is displayed on top right corner of the respective saved address card.", 
						"X button is not displayed on top right corner of the respective saved address card.", driver);
			}
			
			addrPage.clickDeleteAddress(cardIndex);
			Log.message(i++ + ". Clicked delete on address card.", driver);
			
			Log.softAssertThat(addrPage.verifyDeleteModalAddressCard(cardIndex), 
					"On clicking delete, system should display the confirmation message modal.", 
					"On clicking delete, system displays the confirmation message modal.", 
					"On clicking delete, system does not display the confirmation message modal.", driver);
			
			Log.softAssertThat(addrPage.verifyDeleteModalBackground(cardIndex, bgColor), 
					"Confirmation message modal should display in grey background color.", 
					"Confirmation message modal displays in grey background color.", 
					"Confirmation message modal does not display in grey background color.", driver);
			
			//Step-2: Verify the functionality of Confirmation Message
			Log.softAssertThat(addrPage.verifyDeleteModalContent(cardIndex), 
					"Delete Address Confirmation should have confirmation message, Cancel and Yes buttons.", 
					"Delete Address Confirmation has confirmation message, Cancel and Yes buttons.", 
					"Delete Address Confirmation do not have confirmation message, Cancel and Yes buttons.", driver);
			
			Log.softAssertThat(addrPage.verifyDeleteModalTransparency(cardIndex), 
					"Confirmation message should be displayed a grey transparent background over the respective address card.", 
					"Confirmation message is displayed a grey transparent background over the respective address card.", 
					"Confirmation message is not displayed a grey transparent background over the respective address card.", driver);
			
			//Step-3: Verify the functionality of Cancel
			Log.softAssertThat(addrPage.verifyCancelButtonMessageRelativePosition(cardIndex), 
					"Cancel should be displayed below the Confirmation Message", 
					"Cancel is displayed below the Confirmation Message", 
					"Cancel is not displayed below the Confirmation Message", driver);
			
			addrPage.clickCancelOnConfirmation(cardIndex);
			Log.message(i++ + ". Clicked Cancel on delete confirmation overlay.", driver);
			
			Log.softAssertThat(!addrPage.verifyDeleteModalAddressCard(cardIndex), 
					"System should close the confirmation message modal.", 
					"System closed the confirmation message modal.", 
					"System did not close the confirmation message modal.", driver);
			
			Log.softAssertThat(addrPage.getSavedAddressesCount() == addressesSaved, 
					"System should not have deleted the address card.", 
					"System did not deleted the address card.", 
					"System deleted the address card.", driver);
			
			//Step-4: Verify the functionality of Yes
			cardIndex=1;
			Log.event("Index set to default card.");
			
			addrPage.clickDeleteAddress(cardIndex);
			Log.message(i++ + ". Clicked delete on default address card.", driver);
			
			Log.softAssertThat(addrPage.verifyCancelYesRelativePosition(cardIndex), 
					"Yes button should be displayed right of Cancel button.", 
					"Yes button is displayed right of Cancel button.", 
					"Yes button is not displayed right of Cancel button.", driver);
			
			
			addrPage.clickYesOnConfirmation(cardIndex);
			Log.message(i++ + ". Clicked Yes on delete confirmation.", driver);
			
			Log.softAssertThat(addrPage.getSavedAddressesCount() == (addressesSaved-1), 
					"On clicking Yes, system should delete the respective saved address card from the address book.", 
					"The address card is deleted from addressbook.", 
					"The address card is not deleted from addressbook.", driver);
			
			Log.softAssertThat(addrPage.NoOfDefaultAddress() == 0, 
					"If the deleted address card is the default address card, then System should not automatically set any other address card as default.", 
					"System did not automatically set any other address card as default.", 
					"System automatically set another address card as default.", driver);
			
			Log.softAssertThat(addrPage.verifyEditDeleteMakeDefaultAllSavedCards(), 
					"Make Default, Edit and Delete options should be displayed at the bottom of the respective address card", 
					"Make Default, Edit and Delete options displayed at the bottom of the respective address card", 
					"Make Default, Edit and Delete options not displayed at the bottom of the respective address card", driver);
			
			Log.softAssertThat(addrPage.elementLayer.VerifyElementDisplayed(Arrays.asList("btnAddNewAddress"), addrPage), 
					"Add New Address Link should be displayed", 
					"Add New Address Link is displayed", 
					"Add New Address Link is not displayed", driver);
			
			addrPage.deleteAllTheAddresses();
			Log.message(i++ + ". Deleted all remainig addresses.", driver);
			
			Log.softAssertThat(addrPage.elementLayer.VerifyElementDisplayed(Arrays.asList("divAddressForm"), addrPage), 
					"System should display the No Address View for the user to add new address.", 
					"System displays the No Address View for the user to add new address.", 
					"System does not display the No Address View for the user to add new address.", driver);
			
			Log.softAssertThat(!addrPage.elementLayer.VerifyElementDisplayed(Arrays.asList("btnAddNewAddress"), addrPage), 
					"Add New Address Link should not be displayed", 
					"Add New Address Link is not displayed", 
					"Add New Address Link is displayed", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.removeAllSavedAddress(driver);
			driver.quit();
		} // finally
	} // M1_FBB_DROP5_C22694

}
