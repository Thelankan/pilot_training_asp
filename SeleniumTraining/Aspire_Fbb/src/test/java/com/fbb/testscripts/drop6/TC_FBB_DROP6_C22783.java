package com.fbb.testscripts.drop6;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.customerservice.ContactConfirmationPage;
import com.fbb.pages.customerservice.ContactUsPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP6_C22783 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP6_C22783(String browser) throws Exception {

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;

		Log.testCaseInfo();
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);

			CustomerService customerservice = homePage.headers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to Customer Service Page.", driver);

			Log.softAssertThat(customerservice.verifyCSBreadCrumb("HomeCustomer Service"), 
					"Breadcrumb should be displayed as Home / Customer Service", 
					"Breadcrumb is display as Home / Customer Service", 
					"Breadcrumb not display as Home / Customer Service", driver); 

			ContactUsPage contactUsPage = customerservice.clickOnMailWithUs();
			Log.message(i++ +". Navigated to Contact Us page.");
			
			contactUsPage.verifyFieldsAreEditable();
			Log.message(i++ + ". Edit Fields", driver);

			ContactConfirmationPage contactConfirmPage = contactUsPage.clickOnSubmitButton();
			Log.message(i++ + ". Submitted information", driver);
			
			//Step-1: Verify the display and functionality of Confirmation Heading
			Log.softAssertThat(contactConfirmPage.verifyConfirmationHeadingBelowCustomerService(), 
					"Confirmation Heading should be displayed below the Customer Service Label", 
					"Confirmation Heading is displayed below the Customer Service Label", 
					"Confirmation Heading is not displayed below the Customer Service Label", driver);
			
			//Step-2: Verify the display and functionality of Thank you Body
			Log.softAssertThat(contactConfirmPage.verifyThankYouBodyBelowConfirmationHeading(), 
					"Thank You Body should be displayed below the Confirmation Heading.", 
					"Thank You Body is displayed below the Confirmation Heading.", 
					"Thank You Body is not displayed below the Confirmation Heading.", driver); 

			Log.softAssertThat(contactConfirmPage.elementLayer.verifyPageElements(Arrays.asList("contactModule"), contactConfirmPage),
					"Contact Module should be displayed.",
					"Contact Module displayed!",
					"Contact Module not displayed", driver);  

			Log.softAssertThat(contactConfirmPage.verifyContactConfirmationTextProperty(), 
					"Confirmation text should be same as property", 
					"Confirmation text is same as property", 
					"Confirmation text is not same as property", driver);	
			
			String thankYouBodyText = contactConfirmPage.elementLayer.getElementText("thankYouBodyText", contactConfirmPage);
			thankYouBodyText = thankYouBodyText.replaceAll("[\n\t]+", " ").replace("’", "'").trim();
			Log.softAssertThat(thankYouBodyText.equalsIgnoreCase(prdData.get("ThankyouBodyContent")), 
					"ThankyouBodyContent should be same as property", 
					"ThankyouBodyContent text is same as property", 
					"ThankyouBodyContent text is not same as property", driver);
			
			//Step-3: Verify the display and functionality of Contact Module
			Log.softAssertThat(contactConfirmPage.verifyContactModuleBelowThankYouBody(), 
					"ContactModule should be present below ThankYouBody", 
					"ContactModule is present below ThankYouBody", 
					"ContactModule is not present below ThankYouBody", driver);

			Log.reference("Covered display components in the test case id: C22773 Step 5");
			Log.reference("Covered functionality in the test case id: C22774 Step 4");

			//To update result as PASS, FAIL
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

}//TC_FBB_DROP6_C22783
