package com.fbb.testscripts.drop1;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PasswordResetPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP1_C19630 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP1_C19630(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String validUsername = AccountUtils.generateEmail(driver);
		String inValidUsername = "test@yop";
		String inValidIP = "444.222.111.333";
		String validPassword = accountData.get("password_global");
		String inValidPassword = "asdf123asd";
		
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, validUsername + "|" + validPassword);
		}
		
		if(Utils.getRunBrowser(driver).equals("ie")){
			throw new SkipException("Due Mouse Hover feature limitation this TC cannot be run in Safari/IE");
		}

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			Headers headers = homePage.headers;
			
			//Step-1: Check the contents in the Sign in section.
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkViewBagDesktop", "lnkSignInDesktop", headers), 
					"\"SIGN IN\" link should be displayed left to \"MY BAG\" in Homepage", 
					"\"SIGN IN\" link is displayed left to \"MY BAG\" in Homepage", 
					"\"SIGN IN\" link is not displayed left to \"MY BAG\" in Homepage", driver);

			//Step-2: hover over/Tap on the sign in link
			Log.softAssertThat(homePage.headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkViewBagDesktop","lnkSignInDesktop1", homePage.headers), 
					"'Sign-In' should display leftside of the 'My Bag' link", 
					"'Sign-In' is displaying leftside of the 'My Bag' link", 
					"'Sign-In' is not displaying leftside of the 'My Bag' link", driver);
			
			homePage.headers.mouseOverSignIn();
			Log.message(i++ + ". SignIn Flyout Opened.", driver);
			
			Log.softAssertThat(homePage.headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("flytSignInUnregisteredDesktop"), homePage.headers), 
					"Desktop(on-hover Sign-In)/Table(On-Tab Sign-In) Fly out should be displayed on the home page.", 
					"Desktop(on-hover Sign-In)/Table(On-Tab Sign-In) Fly out displayed on the home page.", 
					"Desktop(on-hover Sign-In)/Table(On-Tab Sign-In) Fly out not displayed on the home page.", driver);
			
			
			if(Utils.isDesktop()) {
				homePage.headers.mouseOverMyBag();
				Log.message(i++ + ". SignIn Flyout Closed.", driver);
				
				Log.softAssertThat(homePage.headers.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("flytSignInUnregisteredDesktop"), homePage.headers), 
						"Desktop(on-hover Sign-In)/Table(On-Tab Sign-In) Fly out should not be displayed.", 
						"Desktop(on-hover Sign-In)/Table(On-Tab Sign-In) Fly out is not displayed on the home page.", 
						"Desktop(on-hover Sign-In)/Table(On-Tab Sign-In) Fly out is displayed on the home page.", driver);
			}
			
			if(Utils.isDesktop()) {
				homePage.headers.mouseOverSignIn();
				Log.message(i++ + ". SignIn Flyout Opened.", driver);
			}
			homePage.headers.typeUserNameInFlyout("");
			//Step-3: Click/Tap on Sign in button without giving any data in Sign in fly out
			homePage.headers.clickOnSignInDesktop();
			Log.message(i++ + ". Clicked on Sign-In Button without Username/Password.", driver);

			Log.softAssertThat(homePage.headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblUserNameErrorDesktop","lblPasswordErrorDesktop"), homePage.headers), 
					"The words, \"Email Address\" and \"Password\" that are in their respective textboxes, turn red when user selects \"Sign In.\"", 
					"Appropriate error message displayed in Sign in Flyout.", 
					"Appropriate error message not displayed in Sign in Flyout.", driver);

			//Step-4: Enter Email field and click/Tap sign in button without entering password.
			homePage.headers.typeUserNameInFlyout(validUsername);
			Log.message(i++ + ". Password Field Left empty & Email filled!", driver);

			homePage.headers.clickOnSignInDesktop();
			Log.message(i++ + ". Clicked on Sign-In Button without Username/Password.", driver);

			Log.softAssertThat(homePage.headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblPasswordErrorDesktop"), homePage.headers), 
					"The word \"Password\" in the password textbox turns red when user selects \"Sign In.\"", 
					"Appropriate error message displayed in Sign in Flyout.", 
					"Appropriate error message not displayed in Sign in Flyout.", driver);

			//Step-5:Enter password, keep empty on Email address and click/tap sign in button
			BrowserActions.refreshPage(driver);
			homePage.headers.mouseOverSignIn();
			homePage.headers.typeUserNameInFlyout("");
			homePage.headers.typePasswordInFlyout(validPassword);
			Log.message(i++ + ". Email Field Left empty & Password filled!", driver);
			
			homePage.headers.clickOnSignInDesktop();
			Log.message(i++ + ". Clicked on Sign-In Button without Username/Password.", driver);
			
			Log.softAssertThat(homePage.headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblUserNameErrorDesktop"), homePage.headers), 
					"The word \"Email Address\" that is in the email address textbox turns red when user selects \"Sign In.\"", 
					"Appropriate error message displayed in Sign in Flyout.", 
					"Appropriate error message not displayed in Sign in Flyout.", driver);

			//Step-6: Enter valid email address and invalid password and click/Tap sign in button
			//BrowserActions.refreshPage(driver);
			homePage.headers.typeUserNameInFlyout(validUsername);
			Log.message(i++ + ". Entered Valid Username into Email Field", driver);

			homePage.headers.typePasswordInFlyout(inValidPassword);
			Log.message(i++ + ". Entered Invalid Password into Password Field", driver);

			SignIn signIn = (SignIn)homePage.headers.clickOnSignInDesktop();
			Log.message(i++ + ". Clicked on Sign In Button in Flyout", driver);

			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement","txtSignInError"), signIn),
					"User receives an error message stating 'Sorry, this does not match our records. Check your spelling and try again'", 
					"User navigated to Account-Show-Controller page with error condition messaging", 
					"User not navigated to Account-Show-Controller page with error condition messaging", driver);
			
			//Step: Enter invalid email address and invalid password and click/Tap sign in button
			BrowserActions.refreshPage(driver);
			homePage.headers.mouseOverSignIn();
			
			homePage.headers.typeUserNameInFlyout(inValidUsername);
			Log.message(i++ + ". Entered Invalid Username into Email Field", driver);

			homePage.headers.typePasswordInFlyout(inValidPassword);
			Log.message(i++ + ". Entered Invalid Password into Password Field", driver);

			signIn = (SignIn)homePage.headers.clickOnSignInDesktop();
			Log.message(i++ + ". Clicked on Sign In Button in Flyout", driver);

			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement","txtSignInError"), signIn),
					"User should be navigated to Account-Show-Controller page with error condition messaging", 
					"User navigated to Account-Show-Controller page with error condition messaging", 
					"User not navigated to Account-Show-Controller page with error condition messaging", driver);

			//Step-7: Enter invalid email address and valid password and click/Tap sign in button
			BrowserActions.refreshPage(driver);
			homePage.headers.mouseOverSignIn();

			homePage.headers.typeUserNameInFlyout(inValidUsername);
			Log.message(i++ + ". Entered Invalid Username into Email Field", driver);

			homePage.headers.typePasswordInFlyout(validPassword);
			Log.message(i++ + ". Entered Valid Password into Password Field", driver);

			signIn = (SignIn)homePage.headers.clickOnSignInDesktop();
			Log.message(i++ + ". Clicked on Sign In Button in Flyout", driver);

			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement","txtSignInError"), signIn),
					"User receives an error message" ,
					"User navigated to Account-Show-Controller page with error condition messaging", 
					"User not navigated to Account-Show-Controller page with error condition messaging", driver);

			//Step-8: Enter Invalid IP format in email address field
			BrowserActions.refreshPage(driver);
			homePage.headers.mouseOverSignIn();

			homePage.headers.typeUserNameInFlyout(inValidIP);
			Log.message(i++ + ". Entered Invalid IP Format into Email Field", driver);

			homePage.headers.typePasswordInFlyout(validPassword);
			Log.message(i++ + ". Entered Valid Password into Password Field", driver);

			signIn = (SignIn)homePage.headers.clickOnSignInDesktop();
			Log.message(i++ + ". Clicked on Sign In Button in Flyout", driver);

			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement","txtSignInError"), signIn),
					"User should be navigated to Account-Show-Controller page with error condition messaging", 
					"User navigated to Account-Show-Controller page with error condition messaging", 
					"User not navigated to Account-Show-Controller page with error condition messaging", driver);

			//Step-9: Click/tap on forget password link
			signIn.clickOnForgetPassword1();
			Log.message(i++ + ". Clicked on Forget Password Link", driver);

			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("mdlForgetPassword"), signIn), 
					"The pop up window should be displayed with please enter email address text", 
					"The pop up window displayed with please enter email address text", 
					"The pop up window not displayed with please enter email address text", driver);

			//Step-10: Enter invalid email address in forgot password popup window
			signIn.enterEmailInForgotPasswordMdl(inValidUsername);
			Log.message(i++ + ". Entered Invalid Email Address.", driver);
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																												signIn.clickonSendBtnInForgotPassword();
			Log.message(i++ + ". Clicked on Send Button", driver);

			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("lblErrorEmailInForgotPassword"), signIn), 
					"An error message stating \"Please enter a valid email\" will be shown.", 
					"The error message like 'Please enter valid email address.' shown", 
					"The error message like 'Please enter valid email address.' not shown", driver);

			//Step-11&12: Enter valid email address in the popup window and click/tap send button
			signIn.enterEmailInForgotPasswordMdl(validUsername);
			Log.message(i++ + ". Entered valid Email Address in forgot password popup.", driver);

			signIn.clickonSendBtnInForgotPassword();
			signIn.waitForSuccessMessage();
			Log.message(i++ + ". Clicked on Send Button", driver);

			Log.softAssertThat(signIn.getForgetPasswordSuccessMsg().contains("We’ve received your request"), 
					"User will receive a message stating \"We’ve received your request\"", 
					"Appropriate success message displayed in Sign In page.", 
					"Appropriate success message not displayed in Sign In page.", driver);

			signIn.closeForgotPasswordDialog();
			Log.message(i++ + ". Closed forget password dialog", driver);

			//Step-13: Verify the SHOW /HIDE options available in password field
			BrowserActions.refreshPage(driver);
			headers.mouseOverSignIn();
			Log.message(i++ + ". Mouse moved over Sign In button.", driver);

			headers.typePasswordInFlyout("abcde");

			
			Log.softAssertThat(headers.verifyPasswordFieldTextMode().toLowerCase().equals("password"),
					"By default the password field should be hidden mode. The SHOW Link should be present at the corner of the password field .",
					"By default the password field is hidden mode.",
					"By default the password field not hidden mode.", driver);
			
			
			Log.softAssertThat(headers.verifyPresenceOfShowInPassword(),
					"The SHOW Link should be present at the corner of the password field",
					"The SHOW Link present at the corner of the password field ",
					"The SHOW Link did not present at the corner of the password field ", driver);
			
			String currentState = headers.clickOnShowHideInPassword();
			Log.message(i++ + ". Show Link Clicked!");

			
			Log.softAssertThat(currentState.toLowerCase().equals("show") 
					&& headers.verifyPasswordFieldTextMode().toLowerCase().equals("text"),
					"When clicking on Show link It should display the text in Password field.",
					"It displays the text in Password field.",
					"It did not display the text in Password field.", driver);
			
			currentState = headers.clickOnShowHideInPassword();
			Log.message(i++ + ". Hide Link Clicked!", driver);

			
			Log.softAssertThat(currentState.toLowerCase().equals("hide") 
					&& headers.verifyPasswordFieldTextMode().toLowerCase().equals("password"),
					"When clicking on Hide link It should display the dot character in Password field.",
					"It displays the dot character in Password field.",
					"It did not display dot character in Password field.", driver);
			
			headers.clickOnShowHideInPassword();
			headers.clearUserNamePassword();
			Log.message(i++ + ". Values cleared from Email & Password Fields.", driver);

			//Step-14: Click/Tap on Forget password and enter email address
			PasswordResetPage passwordResetPage = (PasswordResetPage) headers.clickOnForgetPassword();
			Log.message(i++ + ". Clicked on Forget Password in Sign-In Flyout",driver);

			Log.softAssertThat(passwordResetPage.elementLayer.verifyPageElements(Arrays.asList("frmPasswordReset"), passwordResetPage), 
					"When clicked/tap on 'Forget your password' link, User should be navigated to Password Recovery landing page", 
					"When clicked/tap on 'Forget your password' link, User navigated to Password Recovery landing page", 
					"When clicked/tap on 'Forget your password' link, User not navigated to Password Recovery landing page", driver);

			//Step-14a: Verify the error message for Password Recovery landing page.
			passwordResetPage.enterEmailAddress(inValidUsername);
			Log.message(i++ + ". Invalid Email Address entered into Email Address Field", driver);

			passwordResetPage.clickOnSendButton();
			Log.message(i++ + ". Clicked on Send Button", driver);

			Log.softAssertThat(passwordResetPage.elementLayer.verifyPageElements(Arrays.asList("lblEmailError"), passwordResetPage), 
					"Appropriate Error message should be displayed", 
					"Appropriate Error message displayed", 
					"Appropriate Error message not displayed", driver);

			//Step-15: Verify when the user enters the Email address of an existing account and clicks Submit.
			passwordResetPage.enterEmailAddress(validUsername);
			Log.message(i++ + ". Valid Email Address entered into Email Address Field", driver);

			passwordResetPage.clickOnSendButton();
			passwordResetPage.waitForSuccessMessage();
			Log.message(i++ + ". Clicked on Send Button", driver);

			Log.softAssertThat(passwordResetPage.elementLayer.verifyElementTextContains("subHeadRequestPassword", "We’ve received your request!", passwordResetPage),
					"The Password reset Email is sent to the Email address provided and appropriate Success message should be displayed", 
					"Appropriate Success message displayed", 
					"Appropriate Success message not displayed", driver);


			//Step-16: Verify the login message displayed in Sign in flyout
			headers.mouseOverSignIn();
			Log.message(i++ + ". Sign-In Flyout opened.", driver);

			Log.softAssertThat(headers.elementLayer.verifyPageElements(Arrays.asList("lblOtherBrandLoginMsgDesktop","lblOtherBrandLoginBrandsDesktop"), headers), 
					"The login Message should be displayed", 
					"The login Message displayed", 
					"The login Message not displayed", driver);

			Log.softAssertThat(!headers.elementLayer.verifyTextContains("lblOtherBrandLoginBrandsDesktop", Utils.getCurrentBrand().toString(), headers), 
					"The Login message should not contain current brand name.", 
					"The Login message not contain current brand name.", 
					"The Login message contains current brand name.", driver);

			//Step-17: Verify "create an account "button in the sign in flyout
			//Log.message(i++ + ". SignIn Flyout displayed!", driver);
			Log.softAssertThat(headers.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "lblNewCustomerCreateMessage", "lnkCreateNewAccount", headers), 
					"'Create An Account' button should be displayed below 'New Customer?' Label.", 
					"'Create An Account' button displayed below 'New Customer?' Label.", 
					"'Create An Account' button not displayed below 'New Customer?' Label.", driver);

			headers.clickOnCreateAccount();
			Log.message(i++ + ". Clicked on Create Account Page.", driver);

			signIn = new SignIn(driver).get();
			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement"), signIn), 
					"User should be navigated to Sign-In Page.", 
					"User navigated to sign in page.", 
					"User not navigated to Sign in page.", driver);

			//Step-18: Enter valid Email, valid password and click/tap sign in button
			MyAccountPage myAccPage = headers.navigateToMyAccountWithRememberChkBox(validUsername, validPassword, true);
			Log.message(i++ + ". Entered valid username & valid password!", driver);
			
			Log.softAssertThat(myAccPage.elementLayer.verifyPageElements(Arrays.asList("lblOverViewHeading"), myAccPage), 
					"User should be navigated to My Account Page.", 
					"User navigated to My Account Page.", 
					"User not navigated to My Account Page", driver);

			//Step-19: Verify the behavior of Sign in flyout after user is logged in to the site
			//Step-20: Verify the Authenticated Account Menu
			headers.mouseOverMyAccount();
			Log.message(i++ + ". My Account Flyout Opened!", driver);

			Log.softAssertThat(headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList
					("lnkProfileMyAccFlytDesktop", "lnkWishListMyAccFlytDesktop", "lnkOrderHistoryMyAccFlytDesktop",
					"lnkRewardPointsMyAccFlytDesktop"), headers),
					"'PROFILE','WISHLIST','ORDER HISTORY','REWARD POINTS' links should be displayed for authendicated user.",
					"'PROFILE','WISHLIST','ORDER HISTORY','REWARD POINTS' links displayed for authendicated user!",
					"'PROFILE','WISHLIST','ORDER HISTORY','REWARD POINTS' links not displayed as expected.", driver);
			
			//Step-21: Click/Tap the Sign out button. ( For authenticated user)
			headers.clickOnSignOut();
			Log.message(i++ + ". Clicked on Sign Out link!", driver);

			Log.softAssertThat(headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkSignInDesktop1"), headers),
					"When click/tap on \"SIGN OUT\" button, Utility bar should update with SIGN IN button.",
					"User Signed off and Utility bar updated with SIGN IN button.",
					"User not Signed off and Utility bar updated with SIGN IN button.", driver);
			
			headers.mouseOverSignIn();
			Log.message(i++ + ". SignIn Flyout displayed!", driver);
			
			Log.softAssertThat(headers.getValueEnteredInEmailAddress().equals(validUsername),
					"User should be Signed off and Utility bar updated with SIGN IN button.",
					"User Signed off and Utility bar updated with SIGN IN button.",
					"User not Signed off and Utility bar updated with SIGN IN button.", driver);
				
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// M1_FBB_DROP1_C19630
	
	@Test(groups = { "critical", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP1_C19630(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String validUsername = AccountUtils.generateEmail(driver);
		String inValidUsername = "test@yop";
		String inValidIP = "444.222.111.333";

		String validPassword = accountData.get("password_global");
		String inValidPassword = "asdf123asd";
		
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, validUsername + "|" + validPassword);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			Headers headers = homePage.headers;
			
			//Step-1: Check the contents in the Sign in section.
			homePage.headers.hamburgerMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger menu Opened.", driver);
			
			Log.softAssertThat(homePage.headers.hamburgerMenu.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkSignIn"), homePage.headers.hamburgerMenu), 
					"\"SIGN IN\" link should be displayed under hamburger menu.", 
					"\"SIGN IN\" link is displayed under hamburger menu.", 
					"\"SIGN IN\" link is not displayed under hamburger menu.", driver);

			//Step-2: hover over/Tap on the sign in link
			SignIn signIn = homePage.headers.hamburgerMenu.clickOnSignIn();
			//signIn = new SignIn(driver).get();
			Log.message(i++ + ". Clicked on SignIn.", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement"), signIn), 
					"The system should navigate the user to the account sign in page.", 
					"The system navigated the user to the account sign in page.", 
					"The system not navigated the user to the account sign in page.", driver);

			//Step-3: Click/Tap on Sign in button without giving any data in Sign in fly out
			signIn.clickSignInButton();
			Log.message(i++ + ". Clicked on Sign-In Button", driver);

			Log.softAssertThat(homePage.headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblUserNameErrorDesktop","lblPasswordErrorDesktop"), homePage.headers), 
					"Appropriate error message should be displayed in Sign in Page.", 
					"Appropriate error message displayed in Sign in Page.", 
					"Appropriate error message not displayed in Sign in Page.", driver);

			//Step-4: Enter Email field and click/Tap sign in button without entering password.
			signIn.typeOnEmail(validUsername);
			Log.message(i++ + ". Password Field Left empty & Email filled!", driver);

			signIn.clickSignInButton();
			Log.message(i++ + ". Clicked on Sign-In Button", driver);

			Log.softAssertThat(homePage.headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblPasswordErrorDesktop"), homePage.headers), 
					"Appropriate error message should be displayed in Password Field.", 
					"Appropriate error message displayed in Password Field.", 
					"Appropriate error message not displayed in Password Field.", driver);

			//Step-5: Enter password, keep empty on Email address and click/tap sign in button
			BrowserActions.refreshPage(driver);

			signIn.typeOnPassword(validPassword);
			Log.message(i++ + ". Email Field Left empty & Password filled!", driver);

			signIn.clickSignInButton();
			Log.message(i++ + ". Clicked on Sign-In Button", driver);

			Log.softAssertThat(homePage.headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblUserNameErrorDesktop"), homePage.headers), 
					"Appropriate error message should be displayed in Username field.", 
					"Appropriate error message displayed in Username field.", 
					"Appropriate error message not displayed in Username field.", driver);

			//Step-6: Enter valid email address and invalid password and click/Tap sign in button
			
			//Step-5: Enter invalid email address and invalid password and click/Tap sign in button
			signIn.typeOnEmail(inValidUsername);
			Log.message(i++ + ". Entered Invalid Username into Email Field", driver);

			signIn.typeOnPassword(inValidPassword);
			Log.message(i++ + ". Entered Invalid Password into Password Field", driver);

			signIn.clickSignInButton();
			Log.message(i++ + ". Clicked on Sign In Button!", driver);

			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement","txtEmailError"), signIn),
					"User should be navigated to Account-Show-Controller page with error condition messaging", 
					"User navigated to Account-Show-Controller page with error condition messaging", 
					"User not navigated to Account-Show-Controller page with error condition messaging", driver);

			//Step-7: Enter invalid email address and valid password and click/Tap sign in button
			signIn.typeOnEmail(inValidUsername);
			Log.message(i++ + ". Entered Invalid Username into Email Field", driver);

			signIn.typeOnPassword(validPassword);
			Log.message(i++ + ". Entered Valid Password into Password Field", driver);

			signIn.clickSignInButton();
			Log.message(i++ + ". Clicked on Sign In Button!", driver);

			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement","txtEmailError"), signIn),
					"User should be navigated to Account-Show-Controller page with error condition messaging", 
					"User navigated to Account-Show-Controller page with error condition messaging", 
					"User not navigated to Account-Show-Controller page with error condition messaging", driver);

			//Step-8: Enter Invalid IP format in email address field
			signIn.typeOnEmail(inValidIP);
			Log.message(i++ + ". Entered Invalid IP Format into Email Field", driver);

			signIn.typeOnPassword(validPassword);
			Log.message(i++ + ". Entered Valid Password into Password Field", driver);

			signIn.clickSignInButton();
			Log.message(i++ + ". Clicked on Sign In Button!", driver);

			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement","txtEmailError"), signIn),
					"User should be navigated to Account-Show-Controller page with error condition messaging", 
					"User navigated to Account-Show-Controller page with error condition messaging", 
					"User not navigated to Account-Show-Controller page with error condition messaging", driver);

			//Step-9: Click/tap on forget password link
			signIn.clickOnForgetPassword();
			Log.message(i++ + ". Clicked on Forget Password Link", driver);

			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("mdlForgetPassword"), signIn), 
					"The pop up window should be displayed with please enter email address text", 
					"The pop up window displayed with please enter email address text", 
					"The pop up window not displayed with please enter email address text", driver);

			//Step-10: Enter invalid email address in forgot password popup window
			signIn.enterEmailInForgotPasswordMdl(inValidUsername);
			Log.message(i++ + ". Entered Invalid Email Address.", driver);

			signIn.clickonSendBtnInForgotPassword();

			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("lblErrorEmailInForgotPassword"), signIn), 
					"The error message like 'Please enter valid email address.' should be shown", 
					"The error message like 'Please enter valid email address.' shown", 
					"The error message like 'Please enter valid email address.' not shown", driver);

			//Step-11&12: Enter valid email address in the popup window and click/tap send button
			signIn.enterEmailInForgotPasswordMdl(validUsername);
			Log.message(i++ + ". Entered Valid Email Address.", driver);

			signIn.clickonSendBtnInForgotPassword();
			
			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("subHeadRequestPasswordMobile"), signIn), 
					"Appropriate success message should be displayed as Modal in Sign In page.", 
					"Appropriate success message displayed as Modal in Sign In page.", 
					"Appropriate success message not displayed as Modal in Sign In page.", driver);

			//Step-13,14,15,16,17,19,20 Not applicable for mobile.

			//Step-18: Enter valid Email, valid password and click/tap sign in button
			MyAccountPage myAccPage = headers.navigateToMyAccountWithRememberChkBox(validUsername, validPassword, true);
			Log.message(i++ + ". Entered valid username & valid password!", driver);

			Log.softAssertThat(myAccPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAccPage), 
					"User should be navigated to My Account Page.", 
					"User navigated to My Account Page.", 
					"User not navigated to My Account Page", driver);
			
			//Step-21: Click/Tap the Sign out button. ( For authenticated user)
			myAccPage.headers.hamburgerMenu.openCloseHamburgerMenu("open");
			myAccPage.headers.hamburgerMenu.ClickOnMyAccount();
			Log.message(i++ + ". Opened My Account menu", driver);

			myAccPage.headers.hamburgerMenu.clickOnSignOut();
			Log.message(i++ + ". Clicked on Sign-out Link", driver);

			signIn = new SignIn(driver).get();
			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement"), signIn), 
					"User should be signed out successfully.", 
					"User signed out successfully.", 
					"User not signed out successfully.", driver);
			
			myAccPage.headers.hamburgerMenu.openCloseHamburgerMenu("open");
			
			homePage.headers.hamburgerMenu.clickOnSignIn();
			signIn = new SignIn(driver).get();
			
			Log.softAssertThat(signIn.getValueEnteredInEmailAddress().equals(validUsername),
					"User should be Signed off and Utility bar updated with SIGN IN button.",
					"User Signed off and Utility bar updated with SIGN IN button.",
					"User not Signed off and Utility bar updated with SIGN IN button.", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// M2_FBB_DROP1_C19630

}// search
