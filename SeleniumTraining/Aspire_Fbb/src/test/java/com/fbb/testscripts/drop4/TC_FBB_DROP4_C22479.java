package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22479 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "low", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22479(String browser) throws Exception {
	
		Log.testCaseInfo();
		String prodId = prdData.get("ps_special-product-set").split("\\|")[0];
		int quantity = 3;
		String username;
		String password = accountData.get("password_global");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username= AccountUtils.generateEmail(driver).replace("1@", "2@");
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			ShoppingBagPage shoppingBagPg;
			PdpPage pdpPage;
	
			MyAccountPage myAccount = homePage.headers.navigateToMyAccount(username, password, true); //Navigate to My Account Page
			Log.message(i++ + ". Navigated to 'My Account page' ",driver);
	
			if(!myAccount.headers.getMiniCartCount().equals("0")){
				shoppingBagPg = myAccount.headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
				shoppingBagPg.removeAllItemsFromCart();
				Log.message(i++ + ". Removed All Items from the Cart.", driver);
	
				pdpPage = shoppingBagPg.headers.navigateToPDP(prodId);
			}else{
				Utils.waitForPageLoad(driver);
				pdpPage = myAccount.headers.navigateToPDP(prodId);
			}
	
			String productName = pdpPage.getSplProductSetName();
			Log.message(i++ + ". Navigated to PDP Page for product :: " + productName, driver);
	
	
			pdpPage.selectAllSplProductSetSizeBasedOnIndex(0);
	
			pdpPage.selectAllSplProductSetColorBasedOnIndex(0);
	
			pdpPage.selectAllProductSetWidthBasedOnIndex(0);
	
			pdpPage.selectAllSplProductSetSizeFamilyBasedOnIndex(0);
	
			pdpPage.selectQty("1");
	
			pdpPage.clickAddProductToBagInProductSet();
	
			shoppingBagPg = pdpPage.clickOnCheckoutInMCOverlay();
	
			//Step 1
	
			Log.reference("1a step is covered in 22533 test case");
	
			System.out.println(" -- -- -- productName -- "+ productName +" -- -- ---- prodId --- -- "+prodId);
	
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productDetails", "imgProductSection", shoppingBagPg), 
					"The Product Set image should be displayed to the left of the cart.", 
					"The Product Set image is displayed to the left of the cart.", 
					"The Product Set image is not displayed to the left of the cart.", driver);
	
			pdpPage =shoppingBagPg.clickOnSplProductMainImg(productName);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("cntPdpContent"), pdpPage), 
					"Upon clicking the Product Set Image, the User should be taken to the PDP of the Special Product Set. ", 
					"User is taken to the PDP of the Special product set.", 
					"User is not navigated to the Special product set.", driver);
	
			shoppingBagPg = pdpPage.headers.navigateToShoppingBagPage();
	
			//2
	
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productNameSection", "imgProductSection", shoppingBagPg), 
					"The Product Set name should be displayed to the right of the Product Set image", 
					"The Product Set name is displayed to the right of the Product Set image",
					"The Product Set name is not displayed to the right of the Product Set image", driver);
	
			pdpPage = shoppingBagPg.clickOnSplProductName(productName);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("cntPdpContent"), pdpPage), 
					"Upon clicking the Product Set Name, the User should be taken to the PDP of the Special Product Set. ", 
					"User is taken to the PDP of the Special product set.", 
					"User is not navigated to the Special product set.", driver);
	
			shoppingBagPg = pdpPage.headers.navigateToShoppingBagPage();
	
			//4
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "productNameSection", "productPriceAttribute", shoppingBagPg), 
					"The Product Set price should be displayed beneath the Product Set Name.", 
					"The Product Set price is displayed beneath the Product Set Name.", 
					"The Product Set price is not displayed beneath the Product Set Name.", driver);
	
			//Step 5a
			int verifyQty = shoppingBagPg.getQtyInCart();
	
			Log.softAssertThat(verifyQty == 1,
					"To check the quantity is correctly displaying in cart page.",
					"The added quantity is displaying in cart page!",
					"The added quantity is not displaying in cart page", driver);
	
			Log.softAssertThat(shoppingBagPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnQtyArrowDownDisabled"), shoppingBagPg),
					"To check the quantity downward arrow is disabled or not.",
					"The downward quantity arrow is disabled",
					"The downward quantity arrow is not disabled", driver);
			//6
			if(runPltfrm.equals("mobile")){
				
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "productNameSection", "prdSetChildProductMobile", shoppingBagPg), 
						"The Product Set components should be displayed below the Product Set!",
						"The Product Set components is displayed below the Product Set!",
						"The Product Set components is not displayed below the Product Set", driver);
			}
			else
			{
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "productNameSection", "prdSetChildProduct", shoppingBagPg), 
						"The Product Set components should be displayed below the Product Set!",
						"The Product Set components is displayed below the Product Set!",
						"The Product Set components is not displayed below the Product Set", driver);
			}
			
			
			//7
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgProductSection", "productBrandImg", shoppingBagPg), 
					"The Add to Cart Brand for the Product Set should be displayed below the Product Set's image.", 
					"The Add to Cart Brand for the Product Set is displayed below the Product Set's image.", 
					"The Add to Cart Brand for the Product Set is not displayed below the Product Set's image.", driver);
	
			//Step 8
			if(runPltfrm.equals("desktop")) {
	
				QuickShop quickShop=(QuickShop)shoppingBagPg.clickOnEditLink(0);
				Log.message(i++ + ". Navigated to Quick shop page when click the Edit link ");
				shoppingBagPg.selectQtyInEditOverlay(quantity);
				quickShop.clickOnUpdateCartSplProdEditOverlay();
				verifyQty = shoppingBagPg.getQtyInCart();
				System.out.println("verifyQty" + verifyQty);
				Log.softAssertThat(verifyQty == quantity,
						"To check the edited quantity is updating in cart page.",
						"The edited quantity is update in cart page!",
						"The edited quantity is not update in cart page.", driver);
			}
			//Step 9
			try {
				shoppingBagPg.clickOnSplProductRemove();
			} catch (Exception e) {
				Log.fail(i++ + ". Something went wrong error displayed  -  PXSFCC-4084");
			}
	
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnRemoveInCartPage"), shoppingBagPg),
					"To check the special product set is removed from cart page.",
					"The special product is removed after clicking in the 'Remove' link",
					"The special product is not removed after clicking in the 'Remove' link", driver); 
	
			//Step 5b
			pdpPage = myAccount.headers.navigateToPDP(prodId);
	
			pdpPage.selectAllSplProductSetSizeBasedOnIndex(0);
	
			//pdpPage.selectAllSplProductSetColorBasedOnIndex(0);
	
			//pdpPage.selectAllProductSetWidthBasedOnIndex(0);
	
			//pdpPage.selectAllSplProductSetSizeFamilyBasedOnIndex(0);
	
			for(int j=0;j<4;j++) {
				pdpPage.selectQty("10");
				pdpPage.clickAddProductToBagInProductSet();
				pdpPage.closeAddToBagOverlay();
			}
	
			shoppingBagPg = myAccount.headers.navigateToShoppingBagPage();
	
			Log.softAssertThat(shoppingBagPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnQtyArrowUpDisabled"), shoppingBagPg),
					"To check the quantity upward arrow is disabled or not.",
					"The upward quantity arrow is disabled",
					"The upward quantity arrow is not disabled", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	}// TC_FBB_DROP4_C22479
}// search
