package com.fbb.testscripts.drop5;

import java.util.Arrays;
import java.util.LinkedHashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22683 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22683(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String username;
		String password = accountData.get("password_global");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAccount = homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			// Step 1: Verify the functionality of Breadcrumb
			myAccount.expandCollapseOverview("open");
			AddressesPage addressesPg = myAccount.clickOnAddressLink();
			Log.message(i++ + ". Navigated to 'Address page' ",driver);
	
			if(!Utils.isMobile()) {
				Log.softAssertThat(addressesPg.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "promoContent", "breadCrumbCurrent", addressesPg),
						"Breadcrumb should be displayed below the promotion content",
						"Breadcrumb is displayed below the promotion content",
						"Breadcrumb is not displayed below the promotion content", driver);
			}else {
				Log.softAssertThat(addressesPg.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "promoContent", "breadCrumbCurrentMobile", addressesPg),
						"Breadcrumb should be displayed below the promotion content",
						"Breadcrumb is displayed below the promotion content",
						"Breadcrumb is not displayed below the promotion content", driver);
			}
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblAddressesHeading"), addressesPg),
					"To check 'Addresses' section is opened.",
					"'Addresses' section is opened!",
					"'Addresses' section is not opened", driver);
	
			Log.softAssertThat(addressesPg.verifyBreadCrumbInThisPage(),
					"To check 'Addresses' breadcrumb is displaying correctly.",
					"'Addresses' breadcrumb is displaying correctly!",
					"'Addresses' breadcrumb is not displaying correctly", driver);
			//Step 1 continued at end.
	
			// Step 2: Verify the functionality of Addresses
			if(!Utils.isMobile()) {
				Log.softAssertThat(addressesPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblMyAccount", "lblAddressesHeading", addressesPg),
						"Addresses should be displayed below the My Account Navigation",
						"Addresses is displayed below the My Account Navigation",
						"Addresses is not displayed below the My Account Navigation", driver);
			}
	
			Log.softAssertThat(addressesPg.verifyHeaderTextIsComingFromProperty(),
					"To check 'Addresses Heading' text is coming from property.",
					"'Addresses Heading' text is coming from property!",
					"'Addresses Heading' text is not coming from property", driver);
	
			// Step 3: Verify the functionality of Addresses Subtext
			if(addressesPg.getSavedAddressesCount() != 0) {
				addressesPg.deleteAllTheAddresses();
				Log.message(i++ + ". Deleted all saved addresses.", driver);
			}
	
			Log.softAssertThat(addressesPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblAddressesHeading", "lblAddressesSubHeading", addressesPg),
					"Addresses Subtext should be displayed below the Addresses",
					"Addresses Subtext is displayed below the Addresses",
					"Addresses Subtext is not displayed below the Addresses", driver);
	
			Log.softAssertThat(addressesPg.verifySubheadTextIsComingFromProperty(),
					"To check 'Addresses Subtext' text is coming from property.",
					"'Addresses Subtext' text is coming from property!",
					"'Addresses Subtext' text is not coming from property", driver);
	
			//Step 4: Verify the functionality of Address Fields
			Log.softAssertThat(addressesPg.verifyCountryDropDownValues(),
					"To check country is only having 'United States'.",
					"Country is only having 'United States'!",
					"Country is not having 'United States'", driver);
	
			Log.softAssertThat(addressesPg.verifyPlaceHolderComingFromProperty(),
					"To check placeholders text are coming from property file.",
					"The placeholders text are coming from property file!",
					"The placeholders text are not coming from property file", driver);
	
			addressesPg.enterAddressTitle("a");
			addressesPg.clickSaveAddress();
			Log.message(i++ + ". Clicked on Save button.", driver);
			String ErrorMessage = addressesPg.getErrorMessage();
	
			Log.softAssertThat(ErrorMessage.contains("State"),
					"To check the unfilling mandatory fields in 'add address' is throwing error.",
					"The fields 'State' is throwing error when unfilled, It is mandatory!",
					"The fields 'State' is not throwing error when unfilled", driver);
	
			LinkedHashMap<String, String> addressDetails = addressesPg.fillingAddressDetails("CreateUserAddress1", false);
	
			Log.message(i++ + ". Entered all the mandatory values in 'Address details'",driver);
	
			Log.softAssertThat(addressesPg.verifyAddressFieldValueEntered(),
					"To check AddressFields are able to take values.",
					"AddressFields are able to take values!",
					"AddressFields are not taking values", driver);
	
			addressesPg.enterAddressLine2("a");
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtPostalPlaceHolderUpward"), addressesPg),
					"To check the Postal placeholder is going up when entering value.",
					"The Postal placeholder is going up when entering value!",
					"The Postal placeholder is not going up when entering value", driver);
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtCityPlaceHolderUpward"), addressesPg),
					"To check the City placeholder is going up when entering value.",
					"The City placeholder is going up when entering value!",
					"The City placeholder is not going up when entering value", driver);
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtNickNamePlaceHolderUpward"), addressesPg),
					"To check the NickName placeholder is going up when entering value.",
					"The NickName placeholder is going up when entering value!",
					"The NickName placeholder is not going up when entering value", driver);
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtFirstNamePlaceHolderUpward"), addressesPg),
					"To check the FirstName placeholder is going up when entering value.",
					"The FirstName placeholder is going up when entering value!",
					"The FirstName placeholder is not going up when entering value", driver);
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtLastNamePlaceHolderUpward"), addressesPg),
					"To check the LastName placeholder is going up when entering value.",
					"The LastName placeholder is going up when entering value!",
					"The LastName placeholder is not going up when entering value", driver);
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtPhonePlaceHolderUpward"), addressesPg),
					"To check the Phone placeholder is going up when entering value.",
					"The Phone placeholder is going up when entering value!",
					"The Phone placeholder is not going up when entering value", driver);
	
			addressesPg.enterphoneNo("22333223331");
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("errorMsgPhoneNo"), addressesPg),
					"To check 'PhoneNo' field will not allow more than 10 character.",
					"'PhoneNo' field will not allow more than 10 character!",
					"'PhoneNo' field is allowing more than 10 character", driver);
	
			addressesPg.enterphoneNo(addressDetails.get("phoneNo"));
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtAddress1PlaceHolderUpward"), addressesPg),
					"To check the Address line1 placeholder is going up when entering value.",
					"The Address line1 placeholder is going up when entering value!",
					"The Address line1 placeholder is not going up when entering value", driver);
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtAddress2PlaceHolderUpward"), addressesPg),
					"To check the Address line2 placeholder is going up when entering value.",
					"The Address line2 placeholder is going up when entering value!",
					"The Address line2 placeholder is not going up when entering value", driver);
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtStatePlaceHolderUpward"), addressesPg),
					"To check the State placeholder is going up when entering value.",
					"The State placeholder is going up when entering value!",
					"The State placeholder is not going up when entering value", driver);
	
			addressesPg.enterAddressLine2("");
	
			//Address Firstname
			addressesPg.enterFirstName("");
	
			addressesPg.clickSaveAddress();
			Log.message(i++ + ". Clicked on Save button.", driver);
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("errorMsgAddressValidation"), addressesPg),
					"To check the unfilling mandatory fields in 'add address' is throwing error.",
					"The fields 'First Name' is throwing error when unfilled, It is mandatory!",
					"The fields 'First Name' is not throwing error when unfilled", driver);
	
			addressesPg.enterFirstName(addressDetails.get("Firstname"));
	
			//Address Lastname
			addressesPg.enterLastName("");
	
			addressesPg.clickSaveAddress();
			Log.message(i++ + ". Clicked on save button.", driver);
	
			ErrorMessage = addressesPg.getErrorMessage();
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("errorMsgAddressValidation"), addressesPg),
					"To check the unfilling mandatory fields in 'add address' is throwing error.",
					"The fields 'Last Name' is throwing error when unfilled, It is mandatory!",
					"The fields 'Last Name' is not throwing error when unfilled", driver);
	
			addressesPg.enterLastName(addressDetails.get("Lastname"));
	
			//Address phoneNo
			addressesPg.enterphoneNo("");
	
			addressesPg.clickSaveAddress();
			Log.message(i++ + ". Clicked on Save button.", driver);
	
			ErrorMessage = addressesPg.getErrorMessage();
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("errorMsgAddressValidation"), addressesPg),
					"To check the unfilling mandatory fields in 'add address' is throwing error.",
					"The fields 'Phone Number' is throwing error when unfilled, It is mandatory!",
					"The fields 'Phone Number' is not throwing error when unfilled", driver);
	
			addressesPg.enterphoneNo(addressDetails.get("phoneNo"));
	
			//Address address1
			addressesPg.enterAddressLine1("");
	
			addressesPg.clickSaveAddress();
			Log.message(i++ + ". Clicked on Save button.", driver);
	
			ErrorMessage = addressesPg.getErrorMessage();
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("errorMsgAddressValidation"), addressesPg),
					"To check the unfilling mandatory fields in 'add address' is throwing error.",
					"The fields 'Address Line1' is throwing error when unfilled, It is mandatory!",
					"The fields 'Address Line1' is not throwing error when unfilled", driver);
	
			addressesPg.enterAddressLine1(addressDetails.get("address1"));
	
			//Address postal
			addressesPg.enterPostal("");
			Log.message(i++ + ". Cleared Zip code", driver);
	
			addressesPg.clickSaveAddress();
			Log.message(i++ + ". Clicked on Save button.", driver);
	
			ErrorMessage = addressesPg.getErrorMessage();
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("errorMsgAddressValidation"), addressesPg),
					"To check the unfilling mandatory fields in 'add address' is throwing error.",
					"The fields 'Zip Code' is throwing error when unfilled, It is mandatory!",
					"The fields 'Zip Code' is not throwing error when unfilled", driver);
	
			BrowserActions.refreshPage(driver);
			addressDetails = addressesPg.fillingAddressDetails("CreateUserAddress1", false);
			addressesPg.enterPostal("12345-4444");
			Log.message(i++ + ". Entred 9-digit ZIP", driver);
	
			//Address city
			addressesPg.enterCity("");
			if(Utils.isDesktop()) {
				BrowserActions.clickOnEmptySpaceHorizontal(driver, "inputAddressCity", addressesPg);
			}
			Log.message(i++ + ". Cleared City.", driver);
	
			Log.softAssertThat(addressesPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("errorPostalCodeInvalid"), addressesPg),
					"To check the postal code field accepts '12345-4444' format code.",
					"The field 'Postal Code' is accepting '12345-4444' format code!",
					"The field 'Postal Code' is not accepting '12345-4444' format code", driver);
	
			addressesPg.enterPostal(addressDetails.get("postal"));
			addressesPg.enterCity("");
	
			addressesPg.clickSaveAddress();
			Log.message(i++ + ". Clicked on Save button.", driver);
	
			ErrorMessage = addressesPg.getErrorMessage();
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("errorMsgAddressValidation"), addressesPg),
					"To check the unfilling mandatory fields in 'add address' is throwing error.",
					"The fields 'City' is throwing error when unfilled, It is mandatory!",
					"The fields 'City' is not throwing error when unfilled", driver);
	
			String cityName = addressDetails.get("city");
	
			//Address address2 and Address Nickname
			addressesPg.enterAddressLine2("");
			Log.message(i++ + ". Empty address line 2", driver);
			addressesPg.enterAddressTitle("");
			Log.message(i++ + ". Empty address nickname", driver);
			addressesPg.enterCity(cityName);
			Log.event(". Entered City name as: " + cityName);
			
			String txtCity = addressesPg.getEnteredCity();
	
			addressesPg.clickSaveAddress();
	
			Log.softAssertThat(addressesPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("errorMsgAddressValidation"), addressesPg),
					"To check the 'AddressLine2' field in 'add address' is optional.",
					"The 'AddressLine2' field in 'add address' is optional!",
					"The 'AddressLine2' field in 'add address' is not optional", driver);
	
			Log.softAssertThat(addressesPg.getDefaultAddressNickName().equalsIgnoreCase(txtCity),
					"City name should be used as nick name in case of empty Address nickname.",
					"City name is used as nick name in case of empty Address nickname.",
					"City name is not used as nick name in case of empty Address nickname.", driver);
	
			//Step 5: Verify the functionality of Save Address
			addressesPg.deleteAllTheAddresses();
			Log.message(i++ + ". Deleted all saved address.", driver);
			addressDetails = addressesPg.fillingAddressDetails("CreateUserAddress1", false);
			Log.message(i++ + ". Filled address details.", driver);
			addressesPg.clickSaveAddress();
			Log.message(i++ + ". Clicked on save button.", driver);
			
			addressesPg.clickOnAddNewAddress();
			Log.message(i++ + ". Clicked on Add New Address.", driver);
			addressesPg.fillingAddressDetails("CreateUserAddress1", false);
			Log.message(i++ + ". Filled address details.", driver);
			addressesPg.enterAddressTitle(addressDetails.get("AddressTitle"));
			addressesPg.clickSaveAddress();
			Log.message(i++ + ". Clicked on save button.", driver);
	
			Log.softAssertThat(addressesPg.elementLayer.verifyElementTextEqualTo("duplicateAddressError", "You have already used this nickname!", addressesPg),
					"'Address NickName' must be unique for every address.",
					"'Address NickName' is unique for every address!",
					"'Address NickName' is not unique for every address", driver);
	
			addressesPg.clickCancelAddressCreation();
			Log.message(i++ + ". Clicked Cancel on address creation.", driver);
	
			Log.reference("Full functionality is covered in the test case id: C22691 Step 1, 2");
	
			//Step 3: Verify the functionality of Addresses Subtext
			if(!(addressesPg.getNoOfAddresses() == 0)) {
				Log.softAssertThat(addressesPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("addressSubText"), addressesPg),
						"To check 'Addresses Subtext' is displayed or not.",
						"'Addresses Subtext' is not displayed!",
						"'Addresses Subtext' is displayed even there are some stored address", driver);
	
				addressesPg.deleteAllTheAddresses();
				Log.message(i++ + ". Deleted all saved addresses.", driver);
			}
	
			//Step-6: Verify that the address saves to the address book when user presses Enter with appropriate data filled
			addressesPg.fillingAddressDetails("CreateUserAddress1", false);
			Log.message(i++ + ". Filled in address details.", driver);
			addressesPg.clickSaveAddress();
			Log.message(i++ + ". Clicked on Save button.", driver);
			
			Log.softAssertThat(addressesPg.getNoOfAddresses() > 0,
					"Address should be saved.", 
					"Address is saved.", 
					"Address is not saved.", driver);
			
			addressesPg.deleteAllTheAddresses();
			Log.message(i++ + ". Deleted all saved address.", driver);
	
			//Step-1 continued
			if (Utils.isMobile()) {
				myAccount.clickOnBreadCrumb(1);
				Log.message(i++ + ". Navigated to 'My Account page' by clicking breadcrumb",driver);
				
				Log.softAssertThat(driver.getCurrentUrl().toLowerCase().contains("account"),
						"To check 'Addresses' breadcrumb is displaying correctly.",
						"'Addresses' breadcrumb is displaying correctly!",
						"'Addresses' breadcrumb is not displaying correctly", driver);
	
				myAccount.expandCollapseOverview("open");
				addressesPg = myAccount.clickOnAddressLink();
				Log.message(i++ + ". Navigated back to 'Address page' ",driver);
	
			} else {
				myAccount.clickOnBreadCrumb(2);
				Log.softAssertThat(driver.getCurrentUrl().toLowerCase().contains("account"),
						"When user clicks on My Account, then the user should be taken to the Overview page.",
						"When user clicks on My Account, then the user is taken to the Overview page.",
						"When user clicks on My Account, then the user is not taken to the Overview page.", driver);
			}
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_FBB_DROP4_C22683

}// search
