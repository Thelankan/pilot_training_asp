package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.CatalogPreferencePage;
import com.fbb.pages.account.EmailPreferencePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22631 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22631(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String username = accountData.get("credential_plcc_preapproved_all").split("\\|")[0];
		String password = accountData.get("credential_plcc_preapproved_all").split("\\|")[1];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			String HighlightColor = prdData.get("highlight_color_2");
			MyAccountPage myAccount = homePage.headers.signInToMyAccountPage(username, password); //Navigate to My Account Page
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			//Step 1: Verify the functionality of Breadcrumb in My Account overview page
			String breadCrumbText = headers.getBreadCrumbText();
			if(Utils.isMobile()) {
				Log.softAssertThat(breadCrumbText.equalsIgnoreCase("HOME"),
						"Breadcrumb should be displayed as Home",
						"Breadcrumb is displayed as Home",
						"Breadcrumb is not displayed as Home", driver);
			}
			else {
				Log.softAssertThat(breadCrumbText.equalsIgnoreCase("HOME MY ACCOUNT"),
						"Breadcrumb should be displayed as Home",
						"Breadcrumb is displayed as Home",
						"Breadcrumb is not displayed as Home", driver);
			}
	
			//Step 2: Verify the functionality of My Account in My Account overview page
			if(!Utils.isMobile()) {
				Log.reference("International Slot is currently not in build.");
			}
	
			//Step 3: Verify the functionality of the navigation bar in My Account Overview page.
			//Step 3 - 1. Overview
			if(!Utils.isMobile()) {
				Log.softAssertThat(myAccount.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblMyAccount", "divNavOverview", myAccount),
						"Navigation should be displayed below My Account.",
						"Navigation is displayed below My Account.",
						"Navigation is not displayed below My Account.", driver);
			}
	
			Log.softAssertThat(myAccount.elementLayer.VerifyPageElementDisplayed(Arrays.asList("divPLCCSection"), myAccount),
					"To check 'Overview' section is opened.",
					"'Overview' section is opened!",
					"'Overview' section is not opened", driver);
	
			if (!Utils.isMobile()) {
				Log.softAssertThat(myAccount.elementLayer.verifyCssPropertyForElement("lnkOverViewLink", "color", HighlightColor, myAccount),
						"To check 'Overview' link is highlighted.",
						"'Overview' link is highlighted!",
						"'Overview' link is not highlighted", driver);
	
			} else {
				Log.softAssertThat(myAccount.elementLayer.verifyElementTextEqualTo("lblOverView", "OVERVIEW", myAccount),
						"To check 'Overview' link is highlighted in navigatin dropdown.",
						"'Overview' link is highlighted in navigatin dropdown!",
						"'Overview' link is not highlighted in navigatin dropdown", driver);
			}
	
			//Step 3 - 2. Profile
			myAccount.expandCollapseOverview("open");
			ProfilePage profilePg = myAccount.navigateToUpdateProfile();
			Log.message(i++ + ". Navigated to Profile page", driver);
	
			Log.softAssertThat(profilePg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblProfileHeading"), profilePg),
					"To check 'Profile' section is opened.",
					"'Profile' section is opened!",
					"'Profile' section is not opened", driver);
	
			Log.softAssertThat(profilePg.elementLayer.verifyAttributeForElement("lnkProfileAfterNavigate", "style", "cursor: text", profilePg),
					"To check whether the Profile link is clickable after navingating to it.",
					"The Profile is not clickable",
					"The Profile is clickable!", driver);
	
			if (!Utils.isMobile()) {
				Log.softAssertThat(myAccount.elementLayer.verifyCssPropertyForElement("lnkProfile", "color", HighlightColor, myAccount),
						"To check 'Profile' link is highlighted.",
						"'Profile' link is highlighted!",
						"'Profile' link is not highlighted", driver);
			} else {
				Log.softAssertThat(myAccount.elementLayer.verifyElementTextEqualTo("headingAccountSubpage", "My Profile", myAccount),
						"'My Profile' should be shown as section header.",
						"'My Profile' is shown as section header.",
						"'My Profile' is not shown as section header.", driver);
			}
	
			myAccount.expandCollapseOverview("open");
	
			if (Utils.isMobile()) {
				headers.openCloseHamburgerMenu("open");
			}
	
			profilePg.headers.navigateToMyAccount();
	
			//Step 3 - 3. Addresses
			myAccount.expandCollapseOverview("open");
			AddressesPage addressesPg = myAccount.clickOnAddressLink();
			Log.message(i++ + ". Navigated to Address page", driver);
	
			Log.softAssertThat(addressesPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblAddressesHeading"), addressesPg),
					"To check 'Addresses' section is opened.",
					"'Addresses' section is opened!",
					"'Addresses' section is not opened", driver);
	
			Log.softAssertThat(addressesPg.elementLayer.verifyAttributeForElement("lnkAddressesAfterNavigate", "style", "cursor: text", addressesPg),
					"To check whether the Addresses link is clickable after navingating to it.",
					"The Addresses is not clickable!",
					"The Addresses is clickable", driver);
	
			if (!Utils.isMobile()) {
				Log.softAssertThat(myAccount.elementLayer.verifyCssPropertyForElement("lnkAddresses", "color", HighlightColor, myAccount),
						"To check 'Addresses' link is highlighted.",
						"'Addresses' link is highlighted!",
						"'Addresses' link is not highlighted", driver);
			} else {
				Log.softAssertThat(myAccount.elementLayer.verifyElementTextEqualTo("headingAddressPageMobile", "Addresses", myAccount),
						"To check 'Addresses' link is highlighted in navigatin dropdown.",
						"'Addresses' link is highlighted in navigatin dropdown!",
						"'Addresses' link is not highlighted in navigatin dropdown", driver);
			}
	
			if (Utils.isMobile()) {
				headers.openCloseHamburgerMenu("open");
			}
	
			addressesPg.headers.navigateToMyAccount();
	
			//Step 3 - 4. OrderHistory
			myAccount.expandCollapseOverview("open");
			OrderHistoryPage orderHistoryPg = myAccount.clickOnOrderHistoryLink();
			Log.message(i++ + ". Navigate to Order History page.", driver);
	
			Log.softAssertThat(driver.getCurrentUrl().toLowerCase().contains("orders"),
					"To check 'Order History' section is opened.",
					"'Order History' section is opened!",
					"'Order History' section is not opened", driver);
	
			Log.softAssertThat(orderHistoryPg.elementLayer.verifyAttributeForElement("lnkOrderHistoryAfterNavigation", "style", "cursor: text", orderHistoryPg),
					"To check whether the Order History is clickable after navigation.",
					"The Order History is not clickable!",
					"The Order History is clickable", driver);
	
			if (!Utils.isMobile()) {
				Log.softAssertThat(myAccount.elementLayer.verifyCssPropertyForElement("lnkOrderHistory", "color", HighlightColor, myAccount),
						"To check 'Order History' link is highlighted.",
						"'Order History' link is highlighted!",
						"'Order History' link is not highlighted", driver);
			} else {
				Log.softAssertThat(myAccount.elementLayer.verifyElementTextEqualTo("headingOrderHistoryMobile", "Recent Order History", myAccount),
						"To check 'Order History' link is highlighted in navigatin dropdown.",
						"'Order History' link is highlighted in navigatin dropdown!",
						"'Order History' link is not highlighted in navigatin dropdown", driver);
			}
	
			if (Utils.isMobile()) {
				headers.openCloseHamburgerMenu("open");
				orderHistoryPg.headers.navigateToMyAccount();
			}
	
			//Step 3 - 7. Payment
			myAccount.expandCollapseOverview("open");
			PaymentMethodsPage PaymentMethodPg = myAccount.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to Payment Method page", driver);
	
			Log.softAssertThat(PaymentMethodPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblPaymentMethodsTitle"), PaymentMethodPg),
					"To check 'Payment' section is opened.",
					"'Payment' section is opened!",
					"'Payment' section is not opened", driver);
	
			Log.softAssertThat(PaymentMethodPg.elementLayer.verifyAttributeForElement("lnkPaymentMethodsAfterNavigate", "style", "cursor: text", PaymentMethodPg),
					"To check whether the Payment is clickable or not after navigation.",
					"The Payment is not clickable!",
					"The Payment is clickable", driver);
	
			if (!Utils.isMobile()) {
				Log.softAssertThat(myAccount.elementLayer.verifyCssPropertyForElement("lnkPaymentMethods", "color", HighlightColor, myAccount),
						"To check 'Payment' link is highlighted.",
						"'Payment' link is highlighted!",
						"'Payment' link is not highlighted", driver);
			} else {
				Log.softAssertThat(myAccount.elementLayer.verifyElementTextEqualTo("headingPaymentMethodsMobile", "Payment Methods", myAccount),
						"To check 'Payment' link is highlighted in navigatin dropdown.",
						"'Payment' link is highlighted in navigatin dropdown!",
						"'Payment' link is not highlighted in navigatin dropdown", driver);
			}
	
			if (Utils.isMobile()) {
				headers.openCloseHamburgerMenu("open");
				PaymentMethodPg.headers.navigateToMyAccount();
			}
	
			//Step-3-8. WishList
			myAccount.expandCollapseOverview("open");
			WishListPage wishListPg = myAccount.clickOnWishListLink();
			Log.message(i++ + ". Navigated to Wishlist page",driver);
	
			Log.softAssertThat(driver.getCurrentUrl().toLowerCase().contains("wishlist"),
					"To check 'WishList' section is opened.",
					"'WishList' section is opened!",
					"'WishList' section is not opened", driver);
	
			Log.softAssertThat(wishListPg.elementLayer.verifyAttributeForElement("lnkWishListAfterNavigate", "style", "cursor: text", wishListPg),
					"To check whether the WishList is clickable after navigation.",
					"The WishList is not clickable!",
					"The WishList is clickable", driver);
	
			if (!Utils.isMobile()) {
				Log.softAssertThat(myAccount.elementLayer.verifyCssPropertyForElement("lnkWishList", "color", HighlightColor, myAccount),
						"To check 'WishList' link is highlighted.",
						"'WishList' link is highlighted!",
						"'WishList' link is not highlighted", driver);
			} else {
				Log.softAssertThat(myAccount.elementLayer.verifyElementTextEqualTo("headingAccountSubpage", "My Wishlist", myAccount),
						"'My WishList' should be shown as section header.",
						"'My WishList' is shown as section header.",
						"'My WishList' is not shown as section header.", driver);
			}
	
			if (Utils.isMobile()) {
				headers.openCloseHamburgerMenu("open");
				wishListPg.headers.navigateToMyAccount();
			}
	
			//Step-3-5: EmailPref
			myAccount.expandCollapseOverview("open");
			EmailPreferencePage EmailPrefPg = myAccount.clickOnEmailPrefLink();
			Log.message(i++ + ". Navigated to Email Preference page",driver);
	
			Log.softAssertThat(EmailPrefPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblEmailPref"), EmailPrefPg),
					"To check 'Email Preference' section is opened.",
					"'Email Preference' section is opened!",
					"'Email Preference' section is not opened", driver);
	
			Log.softAssertThat(EmailPrefPg.elementLayer.verifyAttributeForElement("lnkEmailPrefAfterNavigate", "style", "cursor: text", EmailPrefPg),
					"To check whether the Email Preference is clickable after navigated.",
					"The Email Preference is not clickable!",
					"The Email Preference is clickable", driver);
	
			if (!Utils.isMobile()) {
				Log.softAssertThat(myAccount.elementLayer.verifyCssPropertyForElement("lnkEmailPref", "color", HighlightColor, myAccount),
						"To check 'Email Preference' link is highlighted.",
						"'Email Preference' link is highlighted!",
						"'Email Preference' link is not highlighted", driver);
			} else {
				Log.softAssertThat(myAccount.elementLayer.verifyElementTextEqualTo("headingAccountSubpage", "Email Preferences", myAccount),
						"'Email Preferences' shoould be shown as section header.",
						"'Email Preferences' is shown as section header.",
						"'Email Preferences' is not shown as section header.", driver);
			}
	
			if (Utils.isMobile()) {
				headers.openCloseHamburgerMenu("open");
				EmailPrefPg.headers.navigateToMyAccount();
			}
	
			//Step 3 - 6. CatalogPref
			myAccount.expandCollapseOverview("open");
			CatalogPreferencePage CatalogPrefPg = myAccount.clickOnCatalogPrefLink();
			Log.message(i++ + ". Navigated to Catalog preference page",driver);
	
			Log.softAssertThat(CatalogPrefPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("readyElement"), CatalogPrefPg),
					"To check 'Catalog Preference' section is opened.",
					"'Catalog Preference' section is opened!",
					"'Catalog Preference' section is not opened", driver);
	
			Log.softAssertThat(CatalogPrefPg.elementLayer.verifyAttributeForElement("lnkCatalogPrefAfterNavigate", "style", "cursor: text", CatalogPrefPg),
					"To check whether the Catalog Preference is clickable after navigation.",
					"The Catalog Preference is not clickable!",
					"The Catalog Preference is clickable", driver);
	
			if (!Utils.isMobile()) {
				Log.softAssertThat(myAccount.elementLayer.verifyCssPropertyForElement("lnkCatalogPref", "color", HighlightColor, myAccount),
						"To check 'Catalog Preference' link is highlighted.",
						"'Catalog Preference' link is highlighted!",
						"'Catalog Preference' link is not highlighted", driver);
			} else {
				Log.softAssertThat(myAccount.elementLayer.verifyElementTextEqualTo("catalogRequestHeader", "Catalog Preferences", myAccount),
						"To check 'Catalog Preference' link is highlighted in navigatin dropdown.",
						"'Catalog Preference' link is highlighted in navigatin dropdown!",
						"'Catalog Preference' link is not highlighted in navigatin dropdown", driver);
			}
	
			//Step-4: Verify the functionality of PLCC in My Account overview page
			Log.reference("The Step-4 'a' section covered in the test case id: C22718");
			Log.reference("The Step-4 'b' section covered in the test case id: C22643 and C22644");
	
			//Step 5: Verify the functionality of Current Offers in My Account overview page
			Log.reference("Current Offers are currently disabled.");
	
			//Step 7: Verify the functionality of Banner in My Account overview page
			Log.reference("Section disabled in site. Test commented out");
			//Current Offers section is disabled in site. Relevant steps 6 and 7 disabled in test script. 
			/*myAccount.clickOnOverViewLink();
			OffersAndCouponsPage offerCouponPg = myAccount.clickOnViewOffersAndCoupons();
			Log.message(i++ +". Navigated to Offer and Coupons Page", driver);
	
			Log.softAssertThat(driver.getCurrentUrl().contains("offers-coupons"),
					"To check 'Offers and Coupons' page is opened.",
					"'Offers and Coupons' page is opened!",
					"'Offers and Coupons' page is not opened", driver);
	
			int NoOfOffers = offerCouponPg.getNoOfOffers();
	
			Log.softAssertThat(NoOfOffers > 0,
					"To check 'Offers and Coupons' in the page are displayed.",
					"'Offers and Coupons' in the page are displayed!",
					"'Offers and Coupons' in the page are not displayed", driver);
	
						//Step-6: Verify the functionality of View All Offers in My Account overview page
	
			//Step-6: Verify the functionality of View All Offers in My Account overview page
			//Dependent to step7, after 7th step 6 need to do
	
			profilePg = new ProfilePage(driver);
	
			if(Utils.isMobile()) {
				profilePg = offerCouponPg.headers.navigateToProfilePage();
				myAccount = profilePg.navigateToUpdateOverview();
			} else {			
				myAccount = offerCouponPg.headers.navigateToMyAccount();
			}
	
			Log.message(i++ + ". Navigated back to 'My Account Overview' page", driver);
	
			Log.softAssertThat(myAccount.elementLayer.VerifyPageElementDisplayed(Arrays.asList("offerSection"), myAccount),
					"To check atleast one 'Offer' is displayed.",
					"Atleast one 'Offer' is displayed!",
					"No 'Offer' is displayed", driver);
	
			if (NoOfOffers > 1) {
				Log.softAssertThat(myAccount.elementLayer.VerifyPageElementDisplayed(Arrays.asList
						("lnkOfferSectionWithArrowsAndSlick"), myAccount),
						"To check Arrow and Carousel displayed when more than one offer is displaying.",
						"Arrow and Carousel displayed when more than one offer is displaying",
						"Arrow and Carousel displayed when more than one offer is not displaying", driver);
	
				Log.softAssertThat(myAccount.verifyOfferSelectedBySlickDot(1),
						"To check 'Offer' is selected by slick dot.",
						"'Offer' is selected by slick dot!",
						"'Offer' is not selected by slick dot", driver);
	
				Log.softAssertThat(myAccount.verifyOfferSelectedByPrevArrow(),
						"To check 'Offer' is selected by prev arrow.",
						"'Offer' is selected by prev arrow!",
						"'Offer' is not selected by prev arrow", driver);
	
				Log.softAssertThat(myAccount.verifyOfferSelectedByNextArrow(),
						"To check 'Offer' is selected by next arrow.",
						"'Offer' is selected by next arrow!",
						"'Offer' is not selected by next arrow", driver);
	
				Log.softAssertThat(myAccount.getOfferSlickDotCount() > 1,
						"To check Each 'Offer' have slick dot.",
						"All the 'Offer' have seperate slickdot!",
						"All 'Offer' are not having seperate slickdot", driver);
	
				if(Utils.isMobile()) {
					Log.softAssertThat(myAccount.swipeCheckInOffers(),
							"To check offers navigating based on swipe.",
							"Offers navigating based on swipe!",
							"Offers not navigating based on swipe", driver);
				}
	
			} else {
				Log.softAssertThat(myAccount.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList
						("lnkOfferSectionWithArrowsAndSlick"), myAccount),
						"To check Arrows are not displaying when there is only one offer.",
						"Arrows are not displaying when there is only one offer!",
						"Arrows are displaying when there is only one offer", driver);
			}*/
	
			//Step-8:
			Log.reference("International Slot is currently not in build.");
	
			//Step 1b
			if(!Utils.isMobile()) {
				myAccount.clickOnBreadCrumb(1);
				Log.softAssertThat(new HomePage(driver).get().getPageLoadStatus(),
						"To check whether the application navigated to home page.",
						"The application navigated to home page!",
						"The application is not navigated to home page", driver);
			}
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_FBB_DROP4_C22631

}// search
