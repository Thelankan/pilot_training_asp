package com.fbb.testscripts.drop3;

import java.util.Arrays;
import java.util.LinkedHashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19746 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	@SuppressWarnings("unused")
	private static String runPlatfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19746(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prodSet = prdData.get("ps_product-set");
		String[] productText = new String[10];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.redirectToPDP(prodSet, driver);//navigateToPDP(prodSet);
			int NoOfProd = pdpPage.getNoOfProductInProductSet();
			String productName = pdpPage.getProductSetName();
			Log.message(i++ + ". Navigated to Product Set Page for Product Set :: " + productName, driver);
			productText = pdpPage.selectAllProductSetSizeBasedOnIndex(0, true);
	
			pdpPage.selectAllProductSetColorBasedOnIndex(0);
			pdpPage.selectAllSplProductSetSizeFamilyBasedOnIndex(0);
			pdpPage.selectAllProductSetWidthBasedOnIndex(0);
	
			float verifyPrice = pdpPage.getSumOfProductPricesInProductSet();
			verifyPrice = (float) (Math.round(verifyPrice * 100.0) / 100.0);
			//Step 8
			LinkedHashMap<String, LinkedHashMap<String, String>> prdIndvData = pdpPage.getProductDetailsFromProdSetHashMap();
			int intProd;
			String[] mainImgName = new String[NoOfProd];
			String mainImgSrc;
			for (intProd = 0; intProd < NoOfProd; intProd++) {
				mainImgSrc = pdpPage.getProdSetIndvProductImgSource(intProd);
				mainImgName[intProd] = mainImgSrc.split("\\/")[mainImgSrc.split("\\/").length-1].split("\\?")[0];
			}
	
			pdpPage.clickAddAllToBag(NoOfProd);
			Log.message(i++ + ". Clicked on Add All To Bag button.", driver);
	
			LinkedHashMap<String, LinkedHashMap<String, String>> prdIndvDataOverlay = pdpPage.getPrdDetailFromATBOverlay();
	
			Log.event("PDP Info :: " + prdIndvData.toString());
			Log.event("ATB Info :: " + prdIndvDataOverlay.toString());
			Log.softAssertThat(Utils.compareLinkedHashMapOfHashMap("Product Details", "PDP", "ATB", prdIndvData, prdIndvDataOverlay),
					"To check the product added with correct variation attributes for various products selected.",
					"The Product added with correct variation attributes for various products selected",
					"The Product are not added with correct variation attributes for various products selected", driver);
	
			//Step 1
			Log.assertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("mdlMiniCartOverLay"), pdpPage),
					"To check the 'Add All To Bag' overlay is displayed.",
					"The 'Add All To Bag' overlay is displayed",
					"The 'Add All To Bag' overlay is not displaying", driver);
	
			Log.softAssertThat(pdpPage.verifyItemAddedMessageComingFromProperty(false),
					"Add to bag Overlay 'Item added' message should come from property.",
					"Add to bag Overlay 'Item added' message is coming from property!",
					"Add to bag Overlay 'Item added' message is not coming from property.", driver);
	
			//Step 2
			pdpPage.closeAddToBagOverlay();
			pdpPage.clickAddAllToBag(NoOfProd);
	
			Log.softAssertThat(pdpPage.verifyOverlayProductName(productText),
					"To check the 'Add All To Bag' overlay is displaying all the products in correct order.",
					"The 'Add All To Bag' overlay is displaying product in correct order",
					"The 'Add All To Bag' overlay is not displaying products in correct order", driver);
	
			//Step 3
			Log.softAssertThat(pdpPage.verifyAllProductsAddedInOverlay(NoOfProd),
					"To check the 'Add All To Bag' overlay is displaying the products uniquely when based on bubbles selected.",
					"The 'Add All To Bag' overlay is displaying product in based on the order product added",
					"The 'Add All To Bag' overlay is not displaying the product based on the order product added", driver);
	
			//Step 4 cannot be automated, because verification is from property file
	
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("lblMiniCartOverlayTitle", "text-align", "center", pdpPage) && 
					pdpPage.elementLayer.verifyInsideElementAlligned("lblMiniCartOverlayTitle", "mdlMiniCartOverLay", "top", pdpPage),
					"Item Added Message should be displayed to the top of the Add to Bag overlay centrally aligned",
					"Item Added Message displayed to the top of the Add to Bag overlay centrally aligned",
					"Item Added Message not displayed to the top of the Add to Bag overlay or centrally aligned", driver);
			//Step 6 -- Verify the product image in add to bag overlay page
			String[] miniCartOverLayImgSrc = pdpPage.getMiniCartOverLayAllImgSourceProductSet(NoOfProd);
			String[] miniCartOverLayImgName = new String[NoOfProd];
			for (intProd = 0; intProd < NoOfProd; intProd++) {
				miniCartOverLayImgName[intProd] = miniCartOverLayImgSrc[intProd].split("\\/")[miniCartOverLayImgSrc[intProd].split("\\/").length-1].split("\\?")[0];
			}
			Log.event("Main image name : "+mainImgName+"Minicart overlay image name : "+miniCartOverLayImgName);
			Log.softAssertThat(Utils.compareTwoArraysAreEqualBySorting(mainImgName, miniCartOverLayImgName),
					"To Check all the product images are displaying in the Overlay.",
					"All the Product images are loaded in the overlay successfully!",
					"The Product images are not loaded properly in the overlay.", driver);
	
			//Step 7
			Log.softAssertThat(pdpPage.verifyTheProductNameRightToProductImage(NoOfProd),
					"The Position of Product name should be in the right side of the product image.",
					"The position of the product name is correct!",
					"The position of the product name is not correct.", driver);
	
			//Step 9
			Log.softAssertThat(pdpPage.verifyTheProductQtyBelowToProductSize(NoOfProd),
					"The Position of Product quantity should be in the below the product size.",
					"The position of the product quantity is correct!",
					"The position of the product quantity is not correct.", driver);
	
	
			Log.softAssertThat(pdpPage.verifyAllProductsAddedInOverlay(NoOfProd),
					"To check the navigation of the product using Control arrows(desktop) and Navigation bubbles(Tablet)",
					"The navigation is working and products are displayed based on the navigation",
					"The products are not displaying based on the navigation of control arrows or bubbles", driver);
	
			//Step 5 -- Verify the Close Modal Optionsâ€‹ in the add to overlay page
			Log.message(i++ + ". By clicking in three ways, Minicart Overlay should be closed.");
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Clicked on Close button in Minicart Overlay.");
	
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("mdlMiniCartOverLay", "display", "none", pdpPage),
					"Mini cart Overlay should be closed.",
					"Mini cart Overlay closed successfully!",
					"Mini cart Overlay not closed.", driver);
	
	
			pdpPage.clickAddAllToBag();
			Log.message(i++ + ". Clicked on 'Add To Bag' Button", driver);	
	
			pdpPage.closeProdSetAddToBagOverlayByClickingEmptySpace();
			Log.message(i++ + ". Clicked on Continue Shopping in Mini cart Overlay!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("mdlMiniCartOverLay", "display", "none", pdpPage),
					"Mini cart Overlay should be closed.",
					"Mini cart Overlay closed successfully!",
					"Mini cart Overlay not closed.", driver);
	
	
			pdpPage.clickAddAllToBag(NoOfProd);
			Log.message(i++ + ". Clicked on 'Add To Bag' Button", driver);	
			//8
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkProductNameActiveInMCOverLay", "lnkProductColorSizeInMCOverlay", pdpPage), 
					"Selected product attributes should be displayed below the product name", 
					"Selected product attributes is displayed below the product name", 
					"Selected product attributes is not displayed below the product name", driver);
	
			//9
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkProductColorSizeInMCOverlay", "lnkProductQuantityInMCOverlay", pdpPage), 
					"Selected product available quantity should be displayed below the product size.", 
					"Selected product available quantity is displayed below the product size.", 
					"Selected product available quantity is not displayed below the product size.", driver);
			//10
			Log.softAssertThat(pdpPage.verifyMCOverlayStockAvailability("In Stock"), 
					"Product Availability and Out of stock details should be displayed in inventory state, based on table.", 
					"Product Availability and Out of stock details is displayed in inventory state, based on table.", 
					"Product Availability and Out of stock details is not displayed in inventory state, based on table.", driver);
			//11
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lnkProductPriceInMCOverlay"), pdpPage), 
					"Price should be the single line item price of the item added.", 
					"Price is the single line item price of the item added.", 
					"Price is not a single line item price of the item added.", driver);
			//12
			Log.softAssertThat(pdpPage.verifyTotalAmount(), 
					"Total amount should be calculated based on the unit and price per unit.", 
					"Total amount is calculated based on the unit and price per unit.", 
					"Total amount is not calculated based on the unit and price per unit.", driver);
			//13
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("recommenationMCOverlay"), pdpPage), 
					"Recommendation should be displayed below the Checkout button", 
					"Recommendation should be displayed below the Checkout button", 
					"Recommendation should be displayed below the Checkout button", driver);
			//14
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("subTotalMCOverlay"), pdpPage), 
					"Total no of line item's and total value of selected line items which are available in the cart should be displayed in 'Items In Your Bag'", 
					"Total no of line item's and total value of selected line items which are available in the cart is displayed in 'Items In Your Bag'", 
					"Total no of line item's and total value of selected line items which are available in the cart is not displayed in 'Items In Your Bag'", driver);
	
			pdpPage.clickOnContinueShoppingInMCOverlay();
			Log.message(i++ + ". Clicked on Continue Shopping in Mini cart Overlay!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("mdlMiniCartOverLay", "display", "none", pdpPage),
					"Mini cart Overlay should be closed.",
					"Mini cart Overlay closed successfully!",
					"Mini cart Overlay not closed.", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP2_C19746


}// search
