package com.fbb.testscripts.drop5;

import java.util.Arrays;
import java.util.LinkedHashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22598 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@SuppressWarnings("unchecked")
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22598(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
		String password = accountData.get("password_global");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver);
		
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
		
		Log.testCaseInfo();
		int i = 1;
		try {
			Object[] placeOrder =GlobalNavigation.addProduct_PlaceOrder(driver, searchKey, i, username + "|" + password);
			HomePage homePage =(HomePage)placeOrder[0];
			i = (Integer) placeOrder[1];
			LinkedHashMap<String, String> orderInfo = (LinkedHashMap<String, String>) placeOrder[4];
			Log.message(i++ + ". Order Details :: " + orderInfo);
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAccount = homePage.navigateToMyAccount();//Navigate to My Account Page
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			OrderHistoryPage orderHistory= myAccount.clickOnOrderHistoryLink();
			Log.message(i++ + ". Navigated to Order History Page.", driver);
			
			Log.softAssertThat(!orderHistory.elementLayer.verifyCssPropertyForElement("lnkOrderHistory", "color", "#000000", orderHistory), 
					"Order History link icon should be highlighted in My Account Navigation", 
					"Order History link icon is highlighted in My Account Navigation as expected!!", 
					"Order History link icon is not highlighted in My Account Navigation", driver);
	
			Log.softAssertThat(orderHistory.elementLayer.verifyAttributeForElement("lnkOrderHistory","style","cursor: text",orderHistory), 
					"Since user is in Order History Page, Order History should then not be clickable in My Account Navigation", 
					"Order History Page, Order History is not be clickable in My Account Navigation as expected!!", 
					"Order History Page, Order History is clickable in My Account Navigation", driver);
	
			//stpe 2: Verify the functionality of Order History label
			if(!Utils.isMobile()) {
				Log.softAssertThat(orderHistory.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkOrderHistory", "lblHistoryHeading", orderHistory), 
						"Order history label should be displayed below the My Account Navigation Pane.", 
						"Order history label is displayed below the My Account Navigation Pane.", 
						"Order history label is not displayed below the My Account Navigation Pane.", driver);
			}else {
				Log.softAssertThat(orderHistory.elementLayer.verifyVerticalAllignmentOfElements(driver, "lstbreadcrumb", "lblHistoryHeading", orderHistory), 
						"Order history label should be displayed below the My Account Navigation Pane.", 
						"Order history label is displayed below the My Account Navigation Pane.", 
						"Order history label is not displayed below the My Account Navigation Pane.", driver);
			}
			
			//step 3: Verify the functionality of Subhead
			if(orderHistory.elementLayer.VerifyElementDisplayed(Arrays.asList("lblHistorySubheading"), orderHistory)) {
				Log.softAssertThat(orderHistory.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblHistoryHeading", "lblHistorySubheading", orderHistory), 
						"Subhead should be displayed below the Order History label.", 
						"Subhead is displayed below the Order History label.", 
						"Subhead is not displayed below the Order History label.", driver);
			}
			else {
				Log.message("Subhead is currently disbaled.");
			}
	
			if(!Utils.isMobile()) {
				//step 4: Verify the functionality of Order Number label
				Log.softAssertThat(orderHistory.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblHistoryHeading", "txtHeaderOrderNumber", orderHistory), 
						"Order number Label should be displayed below the My Account Navigation Pane.", 
						"Order number Label is displayed below the My Account Navigation Pane.", 
						"Order number Label is not displayed below the My Account Navigation Pane.", driver);
				
				//step 5: Verify the functionality of Date Placed label
				Log.softAssertThat(orderHistory.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtHeaderOrderDate", "txtHeaderOrderNumber", orderHistory), 
						"Date Placed Label be displayed next to the Order Number Label", 
						"Date Placed is displayed next to the Order Number Label", 
						"Date Placed is not displayed next to the Order Number Label", driver);
				
				//step 6: Verify the functionality of Order Total label
				Log.softAssertThat(orderHistory.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtHeaderOrderOrderTotal", "txtHeaderOrderDate", orderHistory), 
						"Order Total Label should be displayed next to the Date Placed Label", 
						"Order Total Label is displayed next to the Date Placed Label", 
						"Order Total Label is not displayed next to the Date Placed Label", driver);
				
				//step 7: Verify the functionality of Status label 
				Log.softAssertThat(orderHistory.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtHeaderOrderOrderStatus", "txtHeaderOrderOrderTotal", orderHistory), 
						"Status Label should be displayed next to the Order Total Label.", 
						"Status Label is displayed next to the Order Total Label.", 
						"Status Label is not displayed next to the Order Total Label.", driver);
		
				//step 8: Verify the functionality of Action label 
				Log.softAssertThat(orderHistory.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtHeaderOrderOrderAction", "txtHeaderOrderOrderStatus", orderHistory), 
						"Action label should be displayed next to the Status Label.", 
						"Action label is displayed next to the Status Label.", 
						"Action label is not displayed next to the Status Label.", driver);
			}else {
				//step 6: Verify the functionality of Order Total label
				Log.softAssertThat(orderHistory.verifyOrderListElementsOrderMobile(), 
						"Order Total Label should be displayed next to the Date Placed Label", 
						"Order Total Label is displayed next to the Date Placed Label", 
						"Order Total Label is not displayed next to the Date Placed Label", driver);
			}
	
			//Step-9, Mobile only: Verify the functionality of Default Closed State and Order History Summary
			if(Utils.isMobile()) {
				Log.softAssertThat(orderHistory.verifyMobileOrderListIsClised(), 
						"By default, Order History Summary should be in a closed state", 
						"By default, Order History Summary is in a closed state", 
						"By default, Order History Summary is not in a closed state", driver);
	
				Log.softAssertThat(orderHistory.verifyMobileOrderListElements(), 
						"By default, Order History Summary should display components #Order, #Date, #Total", 
						"Order History Summary displays components #Order, #Date, #Total", 
						"Order History Summary does not display components #Order, #Date, #Total", driver);
	
				Log.softAssertThat(orderHistory.verifyMobileOrderListIsOpened(), 
						"Clicking anywhere in the Order History Summary or on the carat symbol should display components Return Items and View Details.", 
						"Clicking ccordingly displays components Return Items and View Details.", 
						"Clicking ccordingly does not displays components Return Items and View Details.", driver);
			}
	
			//step 10: Verify the functionality of Orders placed in the Order History Summary
			Log.softAssertThat(orderHistory.elementLayer.verifyPageElements(Arrays.asList("orderHistoryTable"), orderHistory), 
					"The system should display orders in the Order History page", 
					"The system is display orders in the Order History page as expected!!", 
					"The system is not display orders in the Order History page", driver);
	
			//step 11-15: Verify the order of elements in Order History Summary
			if(!Utils.isMobile()) {
				Log.softAssertThat(orderHistory.verifyOrderListElementsOrder(), 
						"Order Number, Date Placed, Order Total, Status, View Details should be in order.", 
						"Order Number, Date Placed, Order Total, Status, View Details is shown in order.", 
						"Order Number, Date Placed, Order Total, Status, View Details is not displayed in order.", driver);
			}else {
				Log.softAssertThat(orderHistory.verifyOrderListElementsOrderMobile(), 
						"Order Number, Date Placed, Order Total, Status, View Details should be in order.", 
						"Order Number, Date Placed, Order Total, Status, View Details is shown in order.", 
						"Order Number, Date Placed, Order Total, Status, View Details is not displayed in order.", driver);
			}
	
			Log.softAssertThat(orderHistory.getOrderNumersList().toString().contains(orderInfo.get("OrderNumber")),
					"Place any order and check whether the order number has been populated correctly in the Order History Page",
					"Place any order and check whether the order number has been populated correctly in the Order History Page",
					"Place any order and check whether the order number has been populated correctly in the Order History Page", driver);
			
			Log.softAssertThat(orderHistory.verifyOrderNumbersUnique(),
					"Order numbers should be populated correctly in the Order History Page",
					"Order numbers are populated correctly in the Order History Page",
					"Order numbers are not populated correctly in the Order History Page", driver);
	
			//step 12: Verify the functionality of Date Placed in Order History Summary
			Log.softAssertThat(orderHistory.verifyLatestOrderDate(), 
					"Order date should be populated correctly in the Order History Page",
					"Order date is populated correctly in the Order History Page",
					"Order date is not populated correctly in the Order History Page", driver);
	
			Log.softAssertThat(orderHistory.verifyOrderDateFormat(),
					"Order dates in Order History page should be in correct format.",
					"Order dates in Order History page are in correct format.",
					"Order dates in Order History page are not in correct format.", driver);
	
			//step 13: Verify the functionality of Order Total in Order History Summary
			Log.softAssertThat(orderHistory.verifyOrderTotalDollarSymbol(), 
					"Order Total should be displayed with $ along with the Total Amount", 
					"Order Total is displayed with $ along with the Total Amount as expected!!", 
					"Order Total is not displayed with $ along with the Total Amount", driver);
	
			String orderTotal = orderHistory.getOrderTotalForOrderNumber(orderInfo.get("OrderNumber")).replace("TOTAL", "").trim();
			
			Log.softAssertThat(orderTotal.equals(orderInfo.get("OrderTotal")),
					"Place any order and check whether the Order Total should be populated correctly in the Order History Page for the placed order number",
					"Place any order and check whether the Order Total has been populated correctly in the Order History Page for the placed order number",
					"Place any order and check whether the Order Total has not been populated correctly in the Order History Page for the placed order number", driver);
			
			Log.softAssertThat(orderInfo.get("OrderTotal").contains(orderTotal),
					"Place any order and check whether the Order Total should be populated correctly in the Order History Page for the placed order number",
					"Place any order and check whether the Order Total has been populated correctly in the Order History Page for the placed order number",
					"Place any order and check whether the Order Total has not been populated correctly in the Order History Page for the placed order number", driver);
	
			//step 14: Verify the functionality of Status in Order History Summary
			Log.softAssertThat(orderHistory.verifyOrderStatus(), 
					"System should display different order status in Order History Page", 
					"System is display different order status in Order History Page as expected!!", 
					"System is not display different order status in Order History Page", driver);
	
			//step 16: Verify the functionality of View Details in Order History Summary
			String oldURL = driver.getCurrentUrl();
			Log.softAssertThat(orderHistory.clickOnViewDetails(), 
					"On clicking View Details link, user should be navigated to the Order Detail page.", 
					"On clicking View Details link, user is navigated to the Order Detail page as expected!!", 
					"On clicking View Details link, user is not navigated to the Order Detail page", driver);
	
			String newURL = driver.getCurrentUrl();
			Log.softAssertThat(!newURL.equals(oldURL), 
					"View Details Should be clickable/editable", 
					"View Details is clickable/editable as expected!!", 
					"View Details is not be clickable/editable", driver);
	
			Log.reference("Order Details Page Functionality in C22671 ,22673 , 22675 ,22677,22679 ,22681");
	
			BrowserActions.navigateToBack(driver);
	
			//step 17: Verify the functionality of Breadcrumb
			if(!Utils.isMobile()) {
				Log.softAssertThat(orderHistory.headers.getBreadCrumbText().equals("HOME MY ACCOUNT ORDER HISTORY"), 
						"Breadcrumb should be displayed as Home / My Account / Order History", 
						"Breadcrumb is displayed as Home / My Account / Order History", 
						"Breadcrumb is not displayed as Home / My Account / Order History", driver);
			}else {
				Log.softAssertThat(orderHistory.headers.getBreadCrumbText().equals("MY ACCOUNT"), 
						"Breadcrumb should be displayed as ‘< My Account’", 
						"Breadcrumb is displayed as ‘< My Account’", 
						"Breadcrumb is not displayed as ‘< My Account’", driver);
			}
	
			Log.softAssertThat(orderHistory.verifyBreadcrumbNavigation("Home"), 
					"On clicking the carat Breadcrumb home link it should redirect to Home page", 
					"On clicking the carat Breadcrumb home link it is redirect to Home page as expected!!", 
					"On clicking the carat Breadcrumb home link it is not redirect to Home page", driver);
	
			BrowserActions.navigateToBack(driver);
			Log.softAssertThat(orderHistory.verifyBreadcrumbNavigation("My Account"), 
					"On clicking the carat Breadcrumb My Account link it should redirect to My Account page", 
					"On clicking the carat Breadcrumb My Account link it is redirect to My Account page as expected!!", 
					"On clicking the carat Breadcrumb My Account link it is not redirect to My Account page", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP_C22598

}// search
