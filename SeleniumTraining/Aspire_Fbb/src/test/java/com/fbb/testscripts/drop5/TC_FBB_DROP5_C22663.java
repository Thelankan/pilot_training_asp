package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22663 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22663(String browser) throws Exception {
		Log.testCaseInfo();
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String expCardNum = "5454";
		String CardType = "card_MasterCard";
		
		//Pre-requisite - Account Should have more than one payment information
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		String credential = username + "|" + password;
		{
			GlobalNavigation.registerNewUser(driver, 0, 2, credential);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
	
			PaymentMethodsPage paymentMethods = myAcc.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to Payment Methods Page!", driver);
			
			//Step-1: Verify the display of Card Details
			Log.softAssertThat(paymentMethods.elementLayer.verifyPageElements(Arrays.asList("cardImage"), paymentMethods),
					"Card logo - should be displayed first.", 
					"Card logo - is displayed first.", 
					"Card logo - is not displayed first.", driver);
			
			Log.softAssertThat(paymentMethods.elementLayer.verifyHorizontalAllignmentOfElements(driver, "cardName", "cardImage", paymentMethods),
					"Card name - should be displayed next to Card Logo.",
					"Card name - is displayed next to Card Logo.",
					"Card name - is not displayed next to Card Logo.", driver);
			
			//Step-2: Verify the functionality of Delete
			if(Utils.isMobile()){
				Log.softAssertThat(paymentMethods.elementLayer.verifyVerticalAllignmentOfElements(driver, "cardName", "cardHolderName", paymentMethods),
						"Name on Card [firstName lastName] - should be displayed next to the Card Name.",
						"Name on Card [firstName lastName] - is displayed next to the Card Name.",
						"Name on Card [firstName lastName] - is not displayed next to the Card Name", driver);
			}else{
				Log.softAssertThat(paymentMethods.elementLayer.verifyHorizontalAllignmentOfElements(driver, "cardHolderName", "cardName", paymentMethods),
						"Name on Card [firstName lastName] - should be displayed next to the Card Name.",
						"Name on Card [firstName lastName] - is displayed next to the Card Name.",
						"Name on Card [firstName lastName] - is not displayed next to the Card Name", driver);
			}
			
			if(Utils.isMobile()){
				Log.softAssertThat(paymentMethods.elementLayer.verifyVerticalAllignmentOfElements(driver, "cardHolderName", "cardNumber", paymentMethods),
						"Masked Card Number Should be displayed next to the Name on Card.",
						"Masked Card Number is displayed next to the Name on Card.",
						"Masked Card Number is not displayed next to the Name on Card.", driver);
			}else{
				Log.softAssertThat(paymentMethods.elementLayer.verifyHorizontalAllignmentOfElements(driver, "cardNumber", "cardHolderName", paymentMethods),
						"Masked Card Number Should be displayed next to the Name on Card.",
						"Masked Card Number is displayed next to the Name on Card.",
						"Masked Card Number is not displayed next to the Name on Card.", driver);
			}
			
			//Step-3: Verify the display of Expired Expiration Date
			if(Utils.isMobile()){
				Log.softAssertThat(paymentMethods.elementLayer.verifyVerticalAllignmentOfElements(driver, "cardNumber", "cardExpDate", paymentMethods),
						"Expiration date Should be displayed next to the Masked Card Number.",
						"Expiration date is displayed next to the Masked Card Number.",
						"Expiration date is not displayed next to the Masked Card Number.", driver);
			}else{
				Log.softAssertThat(paymentMethods.elementLayer.verifyHorizontalAllignmentOfElements(driver, "cardExpDate", "cardNumber", paymentMethods),
						"Expiration date Should be displayed next to the Masked Card Number.",
						"Expiration date is displayed next to the Masked Card Number.",
						"Expiration date is not displayed next to the Masked Card Number.", driver);
			}
			
			paymentMethods.addNewCard(CardType,false);
			
			paymentMethods.deleteCard(expCardNum);
			
			Log.softAssertThat(paymentMethods.verifyExpirationDateColor(),
					"Expired date for not expirerd cards should be displayed in Black and expired cards in Red.",
					"Expired date for not expirerd cards are displayed in Black and expired cards in Red.",
					"Expired date for not expirerd cards are not displayed in Black and expired cards in Red.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP5_C22663
}// search
