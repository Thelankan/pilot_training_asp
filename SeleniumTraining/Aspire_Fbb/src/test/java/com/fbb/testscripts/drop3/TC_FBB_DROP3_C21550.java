package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C21550 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	@SuppressWarnings("unused")
	private static String runPlatfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C21550(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String giftCardProd = prdData.get("prd_gc_physical");
		String textToCheck = prdData.get("txt_personalization_msg_max-alert");
		String intialTextToVerify = prdData.get("txt_personalization_msg_counter");
		String intialTextCount = prdData.get("txt_personalization_msg_count");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			SearchResultPage srcRsltPg = homePage.headers.navigateToSLP("Giftcard");
			Log.message(i++ + ". Navigated to SLP Page", driver);
			
			PdpPage pdpPage = srcRsltPg.navigateToPdpByPrdID(giftCardProd);
			/*PdpPage pdpPage = homePage.headers.navigateToPDP(giftCardProd);*/
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.chkOrUnchkAddPersonalMsg(true);
			Log.message(i++ + ". Selecting 'Add a personalized message' check box :: " + pdpPage.getProductName(), driver);
	
			Log.softAssertThat(pdpPage.verifyInitialValueOfTextCounter(intialTextCount, intialTextToVerify) && 
					pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtPersonalMsgCounter"), pdpPage),
					"For verification of the text displaying in Personal Message Text Area initally.",
					"The intial counter number and text are correct!",
					"The intial counter number and text is incorrect", driver);
	
			Log.softAssertThat(pdpPage.verifyTextCounterIncreaseOrDecrease(10, false) && pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtPersonalMsgCounter"), pdpPage),
					"For text count verification in Personal Message Text Area.",
					"Text Count is correctly reducing with or without spaces!",
					"Text Count is not reducing", driver);
	
			Log.softAssertThat(pdpPage.verifyTextCounterIncreaseOrDecrease(10, true) && pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtPersonalMsgCounter"), pdpPage),
					"For text count verification in Personal Message Text Area.",
					"Text Count is correctly increasing if a character is deleted!",
					"Text Count is not increasing if a character is deleted", driver);
	
			Log.softAssertThat(pdpPage.verifyTextCounterIncreaseOrDecrease(330, false, textToCheck) && pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtPersonalMsgCounter"), pdpPage),
					"For text count verification in Personal Message Text Area.",
					"Text 'You have reached the limit' is displayed!",
					"Text 'You have reached the limit' is not displayed!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("txtAreaPersonalMsg"), 330, pdpPage),
					"For text count verification in Personal Message Text Area.",
					"After maximum character the TextArea didnt allow to enter text!",
					"After maximum character the TextArea allowing to enter text!", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP2_C21550


}// search
