package com.fbb.testscripts.drop5;

import java.util.Arrays;
import java.util.HashMap;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22756 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	
	//Test data
	String SSN = "111114444";
	String month = "04";
	String day = "14";
	String year = "1944";
	String random=RandomStringUtils.randomNumeric(5);
	String txtEmail="auto_22756_"+random+"@yopmail.com";
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22756(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		//Load Test Data
		String addressDetails = checkoutData.get("valid_address7");
		HashMap<String, String> userAddressM1 = GlobalNavigation.formatAddressToMap(addressDetails);
		userAddressM1.put("email", txtEmail);
		userAddressM1.put("SSN", SSN);
		userAddressM1.put("month", month);
		userAddressM1.put("day", day);
		userAddressM1.put("year", year);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			PlatinumCardApplication applyPLCC = homePage.footers.navigateToPlatinumCreditCard();
			Log.message(i++ + ". Navigated to PLCC apply page.", driver);
			
			applyPLCC.fillPLCCApplication(userAddressM1);
			Log.message(i++ + ". Filled PLCC application", driver);
			
			applyPLCC.clickCheckUncheckConsent(true);
			Log.message(i++ + ". Checked concent.", driver);
			
			applyPLCC.clickOnRegisterBtn();
			Log.message(i++ + ". Clicked on SUBMIT button.", driver);
			
			//Step-1: Verify the functionality of Header element
			Log.softAssertThat(applyPLCC.elementLayer.verifyTextEquals("divUnderReviewHeader", demandwareData.get("under_review_header"), applyPLCC), 
					"The header should read \"Your Application is Under Review\".", 
					"The header reads \"Your Application is Under Review\".", 
					"The header does not read \"Your Application is Under Review\".", driver);
			
			Log.softAssertThat(applyPLCC.elementLayer.verifyElementWithinElement(driver, "divUnderReviewHeader", "divUnderReviewModal", applyPLCC), 
					"Header should be displayed at the top center of the PLCC Under Review Modal.", 
					"Header is displayed at the top center of the PLCC Under Review Modal.", 
					"Header is not displayed at the top center of the PLCC Under Review Modal.", driver);
			
			//Step-2: Verify the functionality of Subhead when this modal is accessed through Web Instant Credit/My Account
			Log.softAssertThat(applyPLCC.elementLayer.verifyVerticalAllignmentOfElements(driver, "divUnderReviewHeader", "divUnderReviewSubhead", applyPLCC), 
					"Subhead should be displayed below the Header.", 
					"Subhead is displayed below the Header.", 
					"Subhead is not displayed below the Header.", driver);
			
			Log.softAssertThat(applyPLCC.elementLayer.verifyTextEquals("divUnderReviewSubhead", demandwareData.get("under_review_text"), applyPLCC), 
					"Subhead message should come from property.", 
					"Subhead message comes from property.", 
					"Subhead message do not come from property.", driver);
			
			//Step-3: Verify the closing functionality of the PLCC Under Review Modal
			//BrowserActions.clickOnEmptySpaceHorizontal(driver, "divUnderReviewModal", applyPLCC);
			Log.message(i++ + ". Clicked outside the modal.");
			
			Log.softAssertThat(applyPLCC.elementLayer.VerifyElementDisplayed(Arrays.asList("divUnderReviewModal"), applyPLCC), 
					"Clicking anywhere outside the modal should not close the PLCC Under Review Modal", 
					"Clicking outside didn't close PLCC Under Review Modal", 
					"Clicking outside closed PLCC Under Review Modal", driver);
			
			//Step-4: Verify the navigation flow on clicking Continue button
			Log.softAssertThat(applyPLCC.elementLayer.verifyTextEquals("divUnderReviewContinueShopping", "CONTINUE SHOPPING", applyPLCC), 
					"The button message should be displayed as Continue Shopping", 
					"The button message is displayed as Continue Shopping", 
					"The button message is not displayed as Continue Shopping", driver);
			
			Log.softAssertThat(applyPLCC.elementLayer.verifyVerticalAllignmentOfElements(driver, "divUnderReviewSubhead", "divUnderReviewContinueShopping", applyPLCC), 
					"Continue should be displayed below the Subheading element", 
					"Continue is displayed below the Subheading element", 
					"Continue is not displayed below the Subheading element", driver);
			
			//Step-3
			BrowserActions.clickOnElementX(driver, "divUnderReviewContinueShopping", applyPLCC, "Continue Shopping");
			Log.softAssertThat(!applyPLCC.elementLayer.VerifyElementDisplayed(Arrays.asList("divUnderReviewModal"), applyPLCC), 
					"System should allow the user to close this modal by clicking on Continue Shopping button.", 
					"System allows the user to close this modal by clicking on Continue Shopping button.", 
					"System did not allow the user to close this modal by clicking on Continue Shopping button.", driver);
			
			//Step-4
			Utils.waitForPageLoad(driver);
			Log.softAssertThat(new HomePage(driver).get().getPageLoadStatus(), 
					"On clicking the Continue Shopping button, User should be navigated to the brand homepage", 
					"On clicking the Continue Shopping button, User is navigated to the brand homepage", 
					"On clicking the Continue Shopping button, User is not navigated to the brand homepage", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M1_FBB_DROP5_C22756
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP5_C22756(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		
		//Load Test Data
		String prdID = prdData.get("prd_single-price");
		String addressDetails = checkoutData.get("valid_address7");
		HashMap<String, String> userAddressM2 = GlobalNavigation.formatAddressToMap(addressDetails);
		userAddressM2.put("email", txtEmail);
		userAddressM2.put("SSN", SSN.substring(5));
		userAddressM2.put("month", month);
		userAddressM2.put("day", day);
		userAddressM2.put("year", year);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			PdpPage pdp = headers.navigateToPDP(prdID);
			Log.message(i++ + ". Navigated to PDP", driver);
			pdp.addToBag();
			ShoppingBagPage bag = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to cart", driver);
			
			CheckoutPage checkout = bag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Clicked on checkout now.", driver);
			
			String currentURL = driver.getCurrentUrl();
			
			checkout.enterGuestUserEmail(email);
			checkout.continueToShipping();
			Log.message(i++ + ". Continued to shippping", driver);
			
			checkout.fillingShippingDetailsAsGuest("plcc_address", "Ground");
			Log.message(i++ + ". Filled in shipping details for guest", driver);
			
			checkout.clickOnContinue();
			Log.message(i++ + ". Clicked on Continue.", driver);
			
			checkout.continueToPLCCStep2();
			Log.message(i++ + ". Clicked Accept.", driver);
			
			checkout.typeTextInSSN(SSN.substring(5));
			checkout.selectDateMonthYearInPLCC2(day, month, year);
			Log.message(i++ + ". Filled in PLCC step 2 details.", driver);
			
			checkout.checkConsentInPLCC("YES");
			Log.message(i++ + ". Checked consent", driver);
			
			checkout.clickOnAcceptInPLCC();
			Log.message(i++ + ". Clicked accpect in PLCC step2", driver);
			
			//Step-4: Verify the navigation flow on clicking Continue button
			Log.softAssertThat(checkout.elementLayer.verifyTextEquals("btnContinueToCheckout", "CONTINUE TO CHECKOUT", checkout), 
					"The button message should be displayed as Continue To Checkout", 
					"The button message is displayed as Continue To Checkout", 
					"The button message is not displayed as Continue To Checkout", driver);
			
			BrowserActions.clickOnElementX(driver, "btnContinueToCheckout", checkout, "CONTINUE TO CHECKOUT");
			Utils.waitForPageLoad(driver);
			
			Log.softAssertThat(driver.getCurrentUrl().contains(currentURL), 
					"On clicking the Continue Shopping button, User should be navigated to the step they were", 
					"On clicking the Continue Shopping button, User is navigated to the step they were", 
					"On clicking the Continue Shopping button, User is not navigated to the step they were", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M2_FBB_DROP5_C22756

}
