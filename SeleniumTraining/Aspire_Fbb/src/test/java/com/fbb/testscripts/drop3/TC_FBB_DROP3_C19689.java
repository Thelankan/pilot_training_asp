package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19689 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19689(String browser) throws Exception {
		Log.testCaseInfo();

		String productID = prdData.get("prd_variation2");
		final WebDriver driver = WebDriverFactory.get(browser);

		int i=1;
		try{
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated to PdpPage for Product :: " + pdpPage.getProductName(), driver);

			Dimension point = driver.manage().window().getSize();
			if(Utils.isDesktop()){

				//Step-1 Verify the product recommendation headline in PDP page.
				Log.softAssertThat(pdpPage.elementLayer.verifyTextContains("txtRecommendationHeadingDesktop", "You may also like", pdpPage), 
						"\"You may also like \" label should be displayed as Headline", 
						"\"You may also like \" label is displayed as Headline", 
						"\"You may also like \" label is not displayed as Headline", driver);
				
				Log.event("Screen Resolution :: " + point);
				
				if(point.getWidth() > 1024){
					Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "sectionRecommendationDesktop", "sectionProductVariation", pdpPage), 
							"Product Recommendation pane should be displayed right side of the PDP in Vertical view.", 
							"Product Recommendation pane is displayed right side of the PDP in Vertical view.", 
							"Product Recommendation pane is not displayed right side of the PDP in Vertical view.", driver);
				}
				else if(point.getWidth() <= 1024){
					Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionRecommendationDesktop", "divDetailsContent", pdpPage), 
							"Product Recommendation pane should be displayed Horizontal.", 
							"Product Recommendation pane is displayed Horizontal.", 
							"Product Recommendation pane is not displayed Horizontal.", driver);
				}

				//Step -3 Verify the attributes of the recommendation product tile in PDP page.
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodImageRecommendationDesktop","prodNameRecommendationDesktop","prodPriceRecommendationDesktop"), pdpPage), 
						"Attributes should be displayed for the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
						"Attributes displayed for the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
						"Attributes are not displayed for recommendations product tile", driver);

				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodStdPriceRecommendationDesktop","prodSalePriceRecommendationDesktop"), pdpPage), 
						"Price Attributes should be displayed for the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price\"",
						"Price Attributes displayed for the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price.\"",
						"Price Attributes are not displayed in the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price.\"", driver);

				//Step -2 Verify "Quick Shop" view is displayed in product tile below the headline using 1400 view
				if(pdpPage.checkQuickShopIsDisplayedInRecommendation()){
					Log.softAssertThat(pdpPage.checkQuickShopIsDisplayedInRecommendation(),
							"The System should display the 'QUICKSHOP' button on top of the product image when mouse-hover action",
							"The System is displayed the 'QUICKSHOP' button on top of the product image when mouse-hover action",
							"The System is not displayed the 'QUICKSHOP' button on top of the product image when mouse-hover action", driver);
				}
				else{
					Log.reference("Quick shop button temporally removed from PDP.");
				}		

			}
			else{

				//Step-1 Verify the product recommendation headline in PDP page.
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionRecommendationMobileTablet", "divDetailsContent", pdpPage), 
						"Product Recommendation pane should be displayed Horizontal.", 
						"Product Recommendation pane is displayed Horizontal.", 
						"Product Recommendation pane is not displayed Horizontal.", driver);

				Log.softAssertThat(pdpPage.elementLayer.verifyTextContains("txtRecommendationHeadingTabMobile", "You may also like", pdpPage), 
						"\"You may also like \" label should be displayed as Headline", 
						"\"You may also like \" label is displayed as Headline", 
						"\"You may also like \" label is not displayed as Headline", driver);

				//Step -3 Verify the attributes of the recommendation product tile in PDP page.
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodImageRecommendationMobileTablet","prodNameRecommendationMobileTablet","prodPriceRecommendationMobileTablet"), pdpPage), 
						"Attributes displayed for the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
						"Attributes displayed in the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
						"Attributes not displayed recommendations product tile", driver);

				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodStdPriceRecommendationMobileTablet","prodSalePriceRecommendationMobileTablet"), pdpPage), 
						"Price Attributes displayed for the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price\"",
						"Price Attributes displayed in the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price.\"",
						"Price Attributes not displayed recommendations product tile", driver);

				Log.reference("Swipe alternate image functionality is not covered due to swipe limitation in selenium.");
			}

			Log.softAssertThat(pdpPage.verifyRecommendationHaveCurrentProduct(productID), 
					"The recommended products should not include the product being viewed",
					"The recommended products does not include the product being viewed",
					"The recommended products includes the product being viewed", driver);

			String name = pdpPage.clickRecommendationPrdImageByIndex(0);
			String prdName = pdpPage.getProductName();

			Log.softAssertThat(prdName.toLowerCase().contains(name.toLowerCase()), 
					"When user click / tab on the product main image or product title, the system should navigate the user to the selected product detail page",
					"When user click / tab on the product main image or product title, the system is navigating to the selected product detail page",
					"When user click / tab on the product main image or product title, the system is not navigating to the selected product detail page", driver);

			Log.testCaseResult();
			
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
		
	}//TC_FBB_DROP3_C19689
	
}//search
