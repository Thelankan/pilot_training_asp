package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.WishlistLoginPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C21641 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C21641(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String fromEmail = "automation@yopmail.com";
		String toEmail = "automation@yopmail.com";
		String username;
		String password = accountData.get("password_global");
	
		String prd_gift_card = prdData.get("prd_gc_physical");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
		
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			cartPage.removeAllCartProduct();
	
			PdpPage pdpPage = cartPage.headers.redirectToPDP(prd_gift_card, driver); //Add Item To cart with the Quantity of 20
			Log.message(i++ + ". Navigated to Pdp Page with the product!"+  prd_gift_card, driver);
	
			pdpPage.addToBag();
			Log.message(i++ + ". Product Added to Bag.", driver);
	
			cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			//Step-1: Verify the display of Personalized message heading
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productUnitPrice", "txtPersonalizedMessage", cartPage), 
					"The Personalized message heading should be displayed below the Gift card amount", 
					"The Personalized message heading is displayed below the Gift card amount", 
					"The Personalized message heading is not displayed below the Gift card amount", driver);
	
			//Step-2: Verify the display of Gift card Amount.
			Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productUnitPrice", "imgProductSection", cartPage), 
					"The Gift card amount should be displayed right to the Product image", 
					"The Gift card amount is displayed right to the Product image", 
					"The Gift card amount is not displayed right to the Product image", driver);
	
			Log.softAssertThat(cartPage.verifyTotalPrice(0), 
					"The price and total amount has to match with what denominations and quantity selected when user was checking out.", 
					"The price and total amount has to match with what denominations and quantity selected when user was checking out.", 
					"The price and total amount has to match with what denominations and quantity selected when user was checking out.", driver);
			//Step-3: Verify the display of "From" value for Gift Card in cart page
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtPersonalizedMessage", "divProductGiftcartFrom", cartPage), 
					"The 'From' value should be displayed below the Personalized message heading", 
					"The 'From' value should be displayed below the Personalized message heading", 
					"The 'From' value should be displayed below the Personalized message heading", driver);
	
			Log.softAssertThat(cartPage.elementLayer.verifyElementTextContains("divProductGiftcartFrom", fromEmail, cartPage), 
					"The value in the 'From' field should be the same value that the User entered at the gift card PDP", 
					"The value in the 'From' field should be the same value that the User entered at the gift card PDP", 
					"The value in the 'From' field should be the same value that the User entered at the gift card PDP", driver);
	
			//Step-4: Verify the display of "To" value for Gift Card in cart page
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divProductGiftcartFrom", "txtGiftcartTo", cartPage), 
					"The 'To' value should be displayed below the 'From' value heading", 
					"The 'To' value should be displayed below the 'From' value heading", 
					"The 'To' value should be displayed below the 'From' value heading", driver);
	
			Log.softAssertThat(cartPage.elementLayer.verifyElementTextContains("txtGiftcartTo", toEmail, cartPage), 
					"The value in the 'To' field should be the same value that the User entered at the gift card PDP", 
					"The value in the 'To' field should be the same value that the User entered at the gift card PDP", 
					"The value in the 'To' field should be the same value that the User entered at the gift card PDP", driver);
	
			//Step-5: Verify the display of Message Body for Gift card in cart page
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtGiftcartTo", "txtProductGiftcartMessage", cartPage), 
					"The Message body should be displayed below the 'To' field", 
					"The Message body should be displayed below the 'To' field", 
					"The Message body should be displayed below the 'To' field", driver);
	
			Log.softAssertThat(cartPage.elementLayer.verifyElementTextContains("txtProductGiftcartMessage", "Test Personal Message", cartPage), 
					"The value in the Message Body should be the same value that the User entered at the gift card PDP", 
					"The value in the Message Body should be the same value that the User entered at the gift card PDP", 
					"The value in the Message Body should be the same value that the User entered at the gift card PDP", driver);
	
			//Step-6: Verify the functionality of the Quantity Selector for the Gift Card in the Cart
			if(Utils.isMobile())
				Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divItemQuantity", "divUserActionItems", cartPage), 
						"The Quantity selector should be present to the right of the Action Links in the Cart", 
						"The Quantity selector should be present to the right of the Action Links in the Cart", 
						"The Quantity selector should be present to the right of the Action Links in the Cart", driver);
			else
				Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divItemQuantity", "productUnitPrice", cartPage), 
						"The Quantity selector should be displayed to the right of the Gift Card's Variation Attributes", 
						"The Quantity selector should be displayed to the right of the Gift Card's Variation Attributes", 
						"The Quantity selector should be displayed to the right of the Gift Card's Variation Attributes", driver);
	
			String qtyBefore = cartPage.getProductQuantity(0);
	
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("divDecreaseArrow"), cartPage),
					"The decrement arrow should be disabled when the quantity of the Gift Card is at 1", 
					"The decrement arrow should be disabled when the quantity of the Gift Card is at 1", 
					"The decrement arrow should be disabled when the quantity of the Gift Card is at 1", driver);
	
			cartPage.clickOnQuantityArrow("up");
			Log.message(i++ + ". Clicked on Quantity dropdown up arrow for one time.", driver);
			String qtyAfter = cartPage.getProductQuantity(0);
			Log.softAssertThat(!qtyBefore.equals(qtyAfter), 
					"Unlike the Gift Certificate, the increment/decrement arrows should function properly for a Gift Card", 
					"Unlike the Gift Certificate, the increment/decrement arrows should function properly for a Gift Card", 
					"Unlike the Gift Certificate, the increment/decrement arrows should function properly for a Gift Card", driver);
	
			//Step-7: Verify the functionality of the 'Edit' Link for the Gift Card in the Cart
			cartPage.clickOnEditLink(0);
			Log.message(i++ + ". Clicked on Edit Link");
	
			if(Utils.isDesktop()){
				QuickShop qsModal = new QuickShop(driver).get();
				Log.softAssertThat(qsModal.elementLayer.VerifyPageElementDisplayed(Arrays.asList("divQuickShop","selectedQty1"), qsModal), 
						"On-click on Edit link the user should navigate to quickview with the carted variation attributes pre-selected", 
						"On-click on Edit link the user should navigate to quickview with the carted variation attributes pre-selected", 
						"On-click on Edit link the user should navigate to quickview with the carted variation attributes pre-selected", driver);
	
				//To-do: only quantity update verified. If needed we can add From/To/Personal Message validations too.
				qsModal.selectQty("10");
				Log.message(i++ + ". Quantity selected :: 10", driver);
	
				qsModal.addToBag();
				Log.message(i++ + ". Product updated.", driver);
	
				cartPage = new ShoppingBagPage(driver).get();
				Log.softAssertThat(cartPage.getProductQuantity(0).equals("10"), 
						"If the user make a new selection which updates the previous line item in the cart", 
						"If the user make a new selection which updates the previous line item in the cart", 
						"If the user make a new selection which updates the previous line item in the cart", driver);
			}else{
				pdpPage = new PdpPage(driver).get();
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("drpGiftCardSizeSelect","txtGCPersonalMsgTo","txtGCPersonalMsgFrom", "txtAreaPersonalMsg"), pdpPage), 
						"On Tap on Edit link the user should navigate to the PDP with the carted variation attributes pre-selected", 
						"On Tap on Edit link the user should navigate to the PDP with the carted variation attributes pre-selected", 
						"On Tap on Edit link the user should navigate to the PDP with the carted variation attributes pre-selected", driver);
	
				//To-do: only quantity update verified. If needed we can add From/To/Personal Message validations too.
				pdpPage.selectQty("10");
				Log.message(i++ + ". Quantity selected :: 10", driver);
	
				pdpPage.clickOnUpdate();
				Log.message(i++ + ". Product updated.", driver);
	
				cartPage = new ShoppingBagPage(driver).get();
				Log.softAssertThat(cartPage.getProductQuantity(0).equals("10"), 
						"If the user make a new selection which updates the previous line item in the cart", 
						"If the user make a new selection which updates the previous line item in the cart", 
						"If the user make a new selection which updates the previous line item in the cart", driver);
			}
	
			//Step-8: Verify the functionality of the 'Add to Wishlist' link for the Gift Card in the Cart
			if(Utils.isMobile())
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgProductSection", "lnkAddToWishlist", cartPage), 
						"The 'Add to Wishlist' option should be displayed below the Gift Card image", 
						"The 'Add to Wishlist' option should be displayed below the Gift Card image", 
						"The 'Add to Wishlist' option should be displayed below the Gift Card image", driver);
			else
				Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkAddToWishlist", "productUnitPrice", cartPage), 
						"The 'Add to Wishlist' option should be displayed to the right hand side of the Gift Card product section in cart page.", 
						"The 'Add to Wishlist' option should be displayed to the right hand side of the Gift Card product section in cart page.", 
						"The 'Add to Wishlist' option should be displayed to the right hand side of the Gift Card product section in cart page.", driver);
	
			cartPage.clickOnAddToWishlist();
			Log.message(i++ + ". Clicked on Add To Wishlist link.", driver);
	
			WishlistLoginPage  wlLogin = new WishlistLoginPage(driver).get();
			Log.softAssertThat(wlLogin.elementLayer.VerifyPageElementDisplayed(Arrays.asList("divSearchWishlist"), wlLogin), 
					"For a guest User, clicking the link will direct the User to the login page", 
					"For a guest User, clicking the link will direct the User to the login page", 
					"For a guest User, clicking the link will direct the User to the login page", driver);
	
			homePage = wlLogin.headers.navigateToHome();
			Log.message(i++ + ". Navigated to Home Page.", driver);
	
			MyAccountPage myAccount = homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			cartPage = myAccount.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart Page.", driver);
	
			cartPage.clickOnAddToWishlist();
			Log.message(i++ + ". Clicked on Add To Wishlist link.", driver);
	
			Log.softAssertThat(cartPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtInWishList"), cartPage), 
					"Once the item is added to the wishlist, the link text will change to 'Added to wishlist'", 
					"Once the item is added to the wishlist, the link text will change to 'Added to wishlist'", 
					"Once the item is added to the wishlist, the link text will change to 'Added to wishlist'", driver);
	
			WishListPage wlPage= cartPage.headers.navigateToWishListPage();
	
			Log.softAssertThat(!wlPage.elementLayer.verifyTextContains("wishListProductDetails", "automation@yopmail.com", wlPage), 
					"For a registered User, the Gift Card will directly be added to the User's wishlist without the fields 'From', 'To' and the Message Body", 
					"For a registered User, the Gift Card will directly be added to the User's wishlist without the fields 'From', 'To' and the Message Body", 
					"For a registered User, the Gift Card will directly be added to the User's wishlist without the fields 'From', 'To' and the Message Body", driver);
	
			wlPage.removeAllProduct();
			Log.message(i++ + ". Product removed from wish list.", driver);
	
			cartPage = wlPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart Page.", driver);
			//Step-9: Verify the functionality of the 'Remove' link for the Gift Card in the Cart
			cartPage.removeAllCartProduct();
			Log.message(i++ + ". Clicked on Removed link", driver);
	
			Log.softAssertThat(cartPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("prdRowGiftCard"), cartPage), 
					"On-click/Tap on remove link it removes the item from the cart", 
					"On-click/Tap on remove link it removes the item from the cart", 
					"On-click/Tap on remove link it removes the item from the cart", driver);
	
			Log.softAssertThat(cartPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("emptyCart"), cartPage), 
					"If the cart only has one item and the item is removed system should display the empty cart state", 
					"If the cart only has one item and the item is removed system should display the empty cart state", 
					"If the cart only has one item and the item is removed system should display the empty cart state", driver);
	
			//Step-10: Verify if the message body is an optional field
			pdpPage = cartPage.headers.redirectToPDP(prd_gift_card, driver);
			Log.message(i++ + ". Navigated to Gift Card PDP", driver);
	
			pdpPage.selectGCSize();
			Log.message(i++ + ". Gift card amount selected.", driver);
	
			pdpPage.addProductToBag();
			Log.message(i++ + ". Try to add a Gift Card to the Cart without adding any details in the message body section.", driver);
	
			cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.softAssertThat(cartPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("divSingleCartRow"), cartPage),
					"The User should be able to Cart the Gift Card successfully and message should not display in the shopping cart page.",
					"The User should be able to Cart the Gift Card successfully and message should not display in the shopping cart page.",
					"The User should be able to Cart the Gift Card successfully and message should not display in the shopping cart page.", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP_C21643
}// search
