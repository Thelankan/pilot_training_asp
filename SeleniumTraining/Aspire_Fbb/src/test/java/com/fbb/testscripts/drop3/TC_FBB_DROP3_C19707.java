package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.QuickShop;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19707 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	@SuppressWarnings("unused")
	private static String runPlatfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "low", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19707(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String productID = prdData.get("ps_special-product-set").split("\\|")[0];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(productID);
			PlpPage plpPage = pdpPage.headers.navigateToLastSubCategory();
	
			QuickShop quickShop = plpPage.clickQuickShop(productID); //FBBrands_Test_ProductSet2
			Log.message(i++ + ". Quick Shop overlay opened!", driver);
	
			quickShop.isSizeChartApplicable();
			Log.softAssertThat(quickShop.getNumberOfProductsInProductSet() == quickShop.getNumberOfProductsColorSwatchesSelected(),
					"By default color should be selected for all child Products!",
					"By default color is selected!",
					"By default color is not selected!", driver);
	
			//2
			quickShop.zoomProductImage();
			Log.message(i++ + ". Zoom the product image!", driver);
	
			Log.softAssertThat(quickShop.verifyZoomLensAndZoomWindowAreDisplayed(),
					"The primary product image should be zoomed!",
					"The primary product image is zoomed",
					"The primary product image is not zoomed", driver);
	
			Log.softAssertThat(quickShop.verifyZoomedImageDisplayedRightOfPrimaryProductImage(),
					"The zoomed image should be displayed to the right of primary product image!",
					"The zoomed image is displayed to the right of primary product image!",
					"The zoomed image is not displayed to the right of primary product image!", driver);
	
			Log.softAssertThat(quickShop.verifyTopOfZoomedImageAllignsWithTopOfProductImage(),
					"The top of zoomed image should be alligned with the top of primary product image!",
					"The top of zoomed image is alligned with the top of primary product image!",
					"The top of zoomed image is not alligned with the top of primary product image!", driver);
	
			//Log.failsoft("The primary product image in Quick Shop overlay not zoomed on hovering : PXSFCC-3720");
			//Log.failsoft("PXSFCC-3279 - The top of zoomed image does not align with the top of product image");
			//3
			//Log.message("Video link will not be available for SPS : SFCCQA-323");
	
			//4
			Log.softAssertThat(quickShop.verifyIfSelectedThumbnailImageDisplayedAsPrimaryProductImageSpecialProductSet(),
					"The selected thumbnail image should be displayed as primary product image!",
					"The selected thumbnail image is displayed as primary product image!",
					"The selected thumbnail image is not displayed as primary product image!", driver);
	
			//Log.failsoft("The selected thumbnail image is not populated in the product primary image : PXSFCC-2736");
	
			quickShop.clickAlternateImages(1);
			Log.message(i++ + ". Clicked on the alternate image!", driver);
	
			Log.softAssertThat(quickShop.verifyIfSelectedThumbnailImageDisplayedAsPrimaryProductImageSpecialProductSet(),
					"The selected thumbnail image should be displayed as primary product image!",
					"The selected thumbnail image is displayed as primary product image!",
					"The selected thumbnail image is not displayed as primary product image!", driver);
	
			if(quickShop.getNumberOfAlternateImages() > 4)
			{
				Log.softAssertThat(quickShop.verifyAlternateImagesNotActive(),
						"The alternate images should not be active!",
						"The alternate images is not active!",
						"The alternate images is active!", driver);
	
				Log.softAssertThat(quickShop.verifyPreviousLinkEnabled() == false,
						"The Previous link should not be enabled!",
						"The Previous link is not enabled!",
						"The Previous link is enabled!", driver);
	
				Log.softAssertThat(quickShop.verifyNextLinkEnabled() == true,
						"The Next link should be enabled!",
						"The Next link is enabled!",
						"The Next link is not enabled!", driver);
	
				Log.softAssertThat(quickShop.verifyNextLinkGreyedOut() == true,
						"The Next link should be greyed out!",
						"The Next link is greyed out!",
						"The Next link is not greyed out!", driver);
	
				Log.softAssertThat(quickShop.verifyPreviousLinkGreyedOut() == true,
						"The Previous link should be greyed out!",
						"The Previous link is greyed out!",
						"The Previous link is not greyed out!", driver);
	
			}
			else
				Log.failsoft("The product does not have more than 4 alternate images");
	
			//5
			Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayedBelow("txtSpecialProductTitleDesktop", "txtSpecialProductSetPrice", quickShop),
					"The product price should be displayed below the product title!",
					"The product price is displayed below the product title!",
					"The product price is not displayed below the product title!", driver);
	
			//Log.failsoft("Price not displayed : PXSFCC-2950");
			Log.softAssertThat(quickShop.verifySpecialProductSetPriceDisplayed(),
					"The product price should be displayed properly!",
					"The product price is displayed properly!",
					"The product price is not displayed properly!", driver);
	
			//6 
			Log.softAssertThat(quickShop.getSelectedQuantityValue().trim().equals("1"),
					"The default selected value should be 1!",
					"The default selected value is 1!",
					"The default selected value is not 1!", driver);
	
			Log.softAssertThat(quickShop.verifyQtyDropdown(),
					"Values from 1 to 10 must be displayed in the dropdown!",
					"Values from 1 to 10 is displayed in the dropdown!",
					"Values from 1 to 10 is not displayed in the dropdown!", driver);
	
			//5
			Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayedBelow("txtSpecialProductSetPrice", "txtSavingsMessage", quickShop),
					"The savings message should be displayed below the product price!",
					"The savings message is displayed below the product price!",
					"The savings message is not displayed below the product price!", driver);
	
			//7
			Log.softAssertThat(quickShop.getNumberOfProductsInProductSet() == quickShop.getNumberOfProductsSizeSwatchesNotSelected(),
					"By default no size swatch should be selected for any child products!",
					"By default no size swatch is selected!",
					"By default size swatch is selected!", driver);
	
			if(quickShop.isSizeChartApplicable())
			{
				Log.softAssertThat(quickShop.verifySizeChartLabelDisplayedRightOfSizeChartIcon(),
						"The Size Chart label should be displayed right of Size Chart icon!",
						"The Size Chart label is displayed right of Size Chart icon!",
						"The Size Chart label is not displayed right of Size Chart icon!", driver);
	
				Log.softAssertThat(quickShop.verifySizeChartLabel(),
						"The Size Chart label should be displayed properly!",
						"The Size Chart label is displayed properly!",
						"The Size Chart label is not displayed properly!", driver);
	
				quickShop.clickOnSizeChartLink();
				Log.message(i++ + ". Clicked on size chart link!", driver);
	
				Log.softAssertThat(quickShop.elementLayer.VerifyPageElementDisplayed(Arrays.asList("divSizechartContainer"), quickShop),
						"The Size Chart Overlay should be displayed!",
						"The Size Chart is displayed!",
						"The Size Chart is not displayed!", driver);
	
				quickShop = quickShop.closeSizeChartModalPopup();
				Log.message(i++ + ". Clicked on close button on size chart modal!", driver);
	
				Log.softAssertThat(quickShop.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divSizechartContainer"), quickShop),
						"The Size Chart Overlay should be closed!",
						"The Size Chart is closed!",
						"The Size Chart is not closed!", driver);
	
			}else {
				Log.failsoft("Test Data not applicable for Size Chart validations");
			}
	
			Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayedBelow("divSpecialProductVariationContent", "btnAddToBagSpecialProductSet", quickShop),
					"The Add To Bag should be displayed below the product variation attribute!",
					"The Add To Bag is displayed below the product variation attribute!",
					"The Add To Bag is not displayed below the product variation attribute!", driver);
	
			if(Utils.getRunBrowser(driver).toLowerCase().contains("edge") || Utils.getRunBrowser(driver).toLowerCase().contains("firefox"))
			{
				Log.softAssertThat(quickShop.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("flytMiniCart"), quickShop),
						"The mini cart overlay should not be displayed!",
						"The mini cart overlay is not displayed!",
						"The mini cart overlay is displayed!", driver);
			}
			else
			{
				Log.softAssertThat(quickShop.verifySelectSizeErrorForSpecialProductSet(),
						"The error message should be displayed!",
						"The error message is displayed!",
						"The error message is not displayed!", driver);
			}
			//String size1 = quickShop.selectSizeSwatchSpecialProductSet(1, 4);
			quickShop.selectSizeSwatchProductSet(1, 1);
			Log.message(i++ + ". Size swatch selected - Component Product 1!", driver);
	
			Log.softAssertThat(quickShop.verifySelectedSizeIsDisplayed(1),
					"The selected size should get displayed!",
					"The selected size is displayed!",
					"The selected size is not displayed!", driver);
	
			quickShop.selectSizeSwatchProductSet(2, 1);
			Log.message(i++ + ". Size swatch selected - Component Product 2!", driver);
	
			quickShop.selectSizeSwatchProductSet(3, 5);
			Log.message(i++ + ". Size swatch selected - Component Product 3!", driver);
	
			quickShop.addToBagSpecialProductSet();
			Log.message(i++ + ". Clicked on the Add To Bag button!", driver);
	
			Log.softAssertThat(quickShop.elementLayer.VerifyPageElementDisplayed(Arrays.asList("flytMiniCart"), quickShop),
					"The mini cart overlay should get displayed!",
					"The mini cart overlay is displayed!",
					"The mini cart overlay is not displayed!", driver);
	
			Log.softAssertThat(quickShop.verifyVerticalAllignmentOfChildProductsInCart(),
					"The child products should be displayed one below the other in the cart",
					"The child products are displayed one below the other in the cart",
					"The child products are not displayed one below the other in the cart", driver);
	
			quickShop.closeCartOverlay();
			Log.message(i++ + ". Clicked on the close button!", driver);
	
			Log.softAssertThat(quickShop.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("flytMiniCart"), quickShop),
					"The mini cart overlay should get closed!",
					"The mini cart overlay is closed!",
					"The mini cart overlay is not closed!", driver);
	
			quickShop = plpPage.clickQuickShop(productID);
			Log.message(i++ + ". Quick Shop overlay opened!", driver);
	
			quickShop.selectSizeSwatchProductSet(1, 1);
			Log.message(i++ + ". Size swatch selected - Component Product 1!", driver);
	
			quickShop.selectSizeSwatchProductSet(2, 1);
			Log.message(i++ + ". Size swatch selected - Component Product 2!", driver);
	
			quickShop.selectSizeSwatchProductSet(3, 5);
			Log.message(i++ + ". Size swatch selected - Component Product 3!", driver);
	
			String size1 = quickShop.getSelectedSizeSpecialProductSet(1);
			String size2 = quickShop.getSelectedSizeSpecialProductSet(2);
			String size3 = quickShop.getSelectedSizeSpecialProductSet(3);
	
			//1
			pdpPage = quickShop.viewFullDetailForSpecialProductSet();
			Log.message(i++ + ". Navigated to PDP!", driver);
	
			Log.softAssertThat(pdpPage.getSelectedSizeSpecialProductSet(0).equalsIgnoreCase(size1)
					&& pdpPage.getSelectedSizeSpecialProductSet(1).equalsIgnoreCase(size2)
					&& pdpPage.getSelectedSizeSpecialProductSet(2).equalsIgnoreCase(size3),
					"The Size selected in the quick shop overlay should be retained in PDP!",
					"The Size selected in the quick shop overlay is retained in PDP!",
					"The Size selected in the quick shop overlay is not retained in PDP!", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP3_C19707


}// search
