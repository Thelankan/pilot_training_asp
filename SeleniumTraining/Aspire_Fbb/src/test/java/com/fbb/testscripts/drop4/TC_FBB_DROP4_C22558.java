package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22558 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22558(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
		String[] password = {accountData.get("password_global"), "123", "123456789123456789123456"};
	
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Load Regular Price Product
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.getProductName();			
	
			String selectedSize = pdpPage.selectSize();
			Log.message(i++ + ". Size Selected :: '"+selectedSize+"'", driver);
	
			String selectedColor = pdpPage.selectColor();
			Log.message(i++ + ". Selected Color :: '"+selectedColor+"'");
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add to cart button");
	
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Clicked on 'X' in mini cart overlay");
	
			ShoppingBagPage shoppingBag = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on 'My Bag' icon in header");
	
			CheckoutPage checkoutPage = shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Clicked on 'Checkout Now' button in shopping bag page");
	
			checkoutPage.enterGuestUserRandomEmail();
			Log.message(i++ + ". Entered guest email address in Checkout Page");
	
			checkoutPage.continueToShipping();
			Log.message(i++ + ". Clicked on 'Continue' button in Checkout Page");
	
			checkoutPage.fillingShippingDetailsAsGuest("NO", "valid_address1","Ground");
			Log.message(i++ + ". Entered shipping address");
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Clicked on 'Continue' button after filling shipping address");
	
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message(i++ + ". Entered Payment details",driver);
	
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details");
	
			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!");
	
	
			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") || driver.getCurrentUrl().contains("revieworder") ,
					"Order should be placed successfully as a Guest user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a Guest user and Order confirmation page is displayed!",
					"Order is not placed successfully as a Guest user and Order confirmation page is not displayed!", driver);
	
	
			checkoutPage.clickOnEditLinkInOrderReceiptAccountCreation();
			Log.message(i++ + ". Clicked on 'Email Edit Link'");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("contentCreateAccountSection"), checkoutPage), 
					"Create Account content section should be displayed!", 
					"Create Account content section is displayed!",
					"Create Account content section is not displayed!", driver);
	
	
			checkoutPage.clickOnCreateAccountButtonWithoutEnteringPassword();
			Log.message(i++ + ". Clicked on 'Create Account' button without entering paswword");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("txtPasswordError", "color", "rgba(255, 58, 81, 1)", checkoutPage), 
					"Password Error message should be displayed!", 
					"Password Error message is displayed!",
					"Password Error message is not displayed!", driver);
	
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("txtConfirmPasswordError", "color", "rgba(255, 58, 81, 1)", checkoutPage), 
					"Confirm Password Error message should be displayed!", 
					"Confirm Password Error message is displayed!",
					"Confirm Password Error message is not displayed!", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtPasswordError"), checkoutPage), 
					"Password Error message should be displayed!", 
					"Password Error message is displayed!",
					"Password Error message is not displayed!", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtConfirmPasswordError"), checkoutPage), 
					"Confirm Password Error message should be displayed!", 
					"Confirm Password Error message is displayed!",
					"Confirm Password Error message is not displayed!", driver);
	
	
			checkoutPage.enterPassword(password[1]);
			Log.message(i++ + ". Entered Password less than 7 charcters");
	
			checkoutPage.enterconfirmPassword(password[1]);
			Log.message(i++ + ". Entered Password less than 7 charcters");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("txtPasswordError", "color", "rgba(255, 58, 81, 1)", checkoutPage), 
					"Password Error message should be displayed when entering password less than 7 characters!", 
					"Password Error message is displayed when entering password less than 7 characters!",
					"Password Error message is not displayed when entering password less than 7 characters!", driver);
	
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("txtConfirmPasswordError", "color", "rgba(255, 58, 81, 1)", checkoutPage), 
					"Confirm Password Error message should be displayed when entering password less than 7 characters!", 
					"Confirm Password Error message is displayed when entering password less than 7 characters!",
					"Confirm Password Error message is not displayed when entering password less than 7 characters!", driver);
	
			checkoutPage.clearPasswordTextFields();
			Log.message(i++ + ". Cleared Password and confirm Password fields");
	
	
			checkoutPage.enterPassword(password[2]);
			Log.message(i++ + ". Entered Password greater than 20 charcters");
	
			checkoutPage.enterconfirmPassword(password[2]);
			Log.message(i++ + ". Entered Password greater than 20 charcters");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("txtPasswordError", "color", "rgba(255, 58, 81, 1)", checkoutPage), 
					"Password Error message should be displayed when entering password greater than 20 characters!", 
					"Password Error message is displayed when entering password greater than 20 characters!",
					"Password Error message is not displayed when entering password greater than 20 characters!", driver);
	
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("txtConfirmPasswordError", "color", "rgba(255, 58, 81, 1)", checkoutPage), 
					"Confirm Password Error message should be displayed when entering password greater than 20 characters!", 
					"Confirm Password Error message is displayed when entering password greater than 20 characters!",
					"Confirm Password Error message is not displayed when entering password greater than 20 characters!", driver);
	
	
	
			checkoutPage.clearPasswordTextFields();
			Log.message(i++ + ". Cleared Password and confirm Password fields");
	
			checkoutPage.enterPassword(password[0]);
			Log.message(i++ + ". Entered Password between 7 -20 characters");
	
	
			checkoutPage.enterconfirmPassword(password[0]);
			Log.message(i++ + ". Entered Password between 7 -20 characters");
	
			Log.softAssertThat(checkoutPage.getEnteredPassword().equals(checkoutPage.getEnteredConfirmPassword()), 
					"Password and Confirm Password should be same!", 
					"Password and Confirm Password is same!",
					"Password and Confirm Password is not same!", driver);
	
			MyAccountPage myAccountPage = checkoutPage.clickOnCreateAccountButton();
			Log.message(i++ + ". Clicked on create account button !");
	
			Log.softAssertThat(myAccountPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAccountPage), 
					"User Account should be navigated to Account Register page!", 
					"User Account is navigated to Account Register page!", 
					"User Account is not navigated to Account Register page!", 
					driver );	
	
			AddressesPage addressesPage = myAccountPage.clickOnAddressLink();
			Log.message(i++ + ". Clicked on Address tab in my account page !");
	
			Log.softAssertThat(addressesPage.elementLayer.verifyPageElements(Arrays.asList("addressSaved"), addressesPage), 
					"Address should be saved", 
					"Address is saved", 
					"Address is not saved", driver);
	
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C22558
}// search
