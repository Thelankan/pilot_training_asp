package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22625 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22625(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String username = accountData.get("credential_saved_payment_plcc_non-plcc").split("\\|")[0]; 
		String password = accountData.get("credential_saved_payment_plcc_non-plcc").split("\\|")[1];
		String cardType1 = "Visa";
		String cardType2 = "MasterCard";
		String[] savedPLCC = "Jessica London Platinum|jl".split("\\|");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
	
			PaymentMethodsPage paymentMethods = myAcc.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to Payment Methods Page!", driver);
	
			//Step-1: Verify the functionality of Add New Card
			Log.softAssertThat(paymentMethods.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkAddNewCard"), paymentMethods),
					"The Add New Card link should be displayed",
					"The Add New Card link is displayed",
					"The Add New Card link is not displayed", driver);
	
			Log.softAssertThat(paymentMethods.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkAddNewCard", "lblDefaultPaymentIcon", paymentMethods),
					"Add new card should be displayed on right of the Default Payment Label",
					"Add new card is displayed on right of the Default Payment Label",
					"Add new card is notdisplayed on right of the Default Payment Label", driver);
	
			paymentMethods.clickAddNewCardLink();
			Thread.sleep(1000);
			Log.softAssertThat(paymentMethods.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnSave"), paymentMethods),
					"Clicking Add New Card should open add new card field.",
					"Clicking Add New Card opened add new card field.",
					"Clicking Add New Card did not open add new card field.", driver);
			paymentMethods.clickCancelAddNewCard();
	
			Log.reference("Further steps covered in TC C22667 steps 1 to 6");
			Log.reference("Further steps covered in TC C22522 steps 9 to 16");
	
			//Step-2: Verify the functionality of PLCC Message
			if(paymentMethods.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtPLCCMessage"), paymentMethods)) {
				Log.softAssertThat(paymentMethods.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtPLCCMessage", "tooltipPLCC", paymentMethods),
						"PLCC message should be displayed on top of the Saved Cards List.", 
						"PLCC message is displayed on top of the Saved Cards List.", 
						"PLCC message is not displayed on top of the Saved Cards List.", driver);
			}else {
				Log.reference("There are no PLCC saved cards available to validate position of PLCC Message.");
			}
	
			String cardTypePLCC = paymentMethods.getCardType(0);
			Log.messageT(cardTypePLCC);
			Log.softAssertThat(paymentMethods.verifyBrandInPLCCMessage(cardTypePLCC),
					"The PLCC card message should display the correct brand",
					"The PLCC card message displays the correct brand",
					"The PLCC card message does not display the correct brand", driver);
	
			//3 - Verify the functionality of Saved Card List
			Log.reference("Further steps covered in TC C22667 Step 7");
			Log.reference("Further steps covered in TC C22661 Step 1");
			Log.reference("Further steps covered in TC C22663 Steps 1-3");
	
			Log.softAssertThat(paymentMethods.verifyDeleteNotAvailableForPLCC(),
					"The PLCC card should not have delete option",
					"The PLCC card does not have delete option",
					"The PLCC card has delete option", driver);
			
			Log.softAssertThat(paymentMethods.verifyOrderOfSavedCards(),
					"Saved Card List should be displayed one by one below the Add New Card and Default Payment or PLCC Message", 
					"Saved Card List is displayed one by one below the Add New Card and Default Payment or PLCC Message", 
					"Saved Card List is not displayed one by one below the Add New Card and Default Payment or PLCC Message", driver);
			
			Log.softAssertThat(paymentMethods.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkAddNewCard", "rowFirstCard", paymentMethods), 
					"Saved Card List should be displayed one by one below the Add New Card", 
					"Saved Card List is displayed one by one below the Add New Card", 
					"Saved Card List is not displayed one by one below the Add New Card", driver);
	
			//4 - Verify the functionality of Default Payment Card (Desktop)
			Log.softAssertThat(paymentMethods.verifyDefaultCardGreenIndicator(), 
					"Green Indicator should be displayed left of the card logo for the Default Payment Card", 
					"Green Indicator is displayed left of the card logo for the Default Payment Card", 
					"Green Indicator is not displayed left of the card logo for the Default Payment Card", driver);
	
			paymentMethods.makeCardTypeAsDefault(savedPLCC[0]);
			Log.message(i++ + ". Made PLCC card as default payment!", driver);
	
			Log.softAssertThat(paymentMethods.getDefaultCardType().toLowerCase().contains(savedPLCC[1]),
					"The PLCC card should be made default",
					"The PLCC card is made default",
					"The PLCC card is not made default", driver);
	
			Log.softAssertThat(paymentMethods.verifyOrderOfSavedCards(),
					"The non-PLCC cards should be displayed first followed by the PLCC cards",
					"The cards are displayed in correct order",
					"The cards are not displayed in correct order", driver);
	
			//5 - Verify the functionality of Make Default
			paymentMethods.makeCardTypeAsDefault("Visa");
			Log.message(i++ + ". Made non-PLCC card as default payment!", driver);
	
			Log.softAssertThat(paymentMethods.getDefaultCardType().toLowerCase().contains(cardType1.toLowerCase()),
					"The "+ cardType1 +" should be made default",
					"The "+ cardType1 +" is made default",
					"The "+ cardType1 +" is not made default", driver);
	
			Log.softAssertThat(paymentMethods.verifyOrderOfSavedCards(),
					"The non-PLCC cards should be displayed first followed by the PLCC cards",
					"The cards are displayed in correct order",
					"The cards are not displayed in correct order", driver);
	
			Log.softAssertThat(paymentMethods.verifyPositionOfDefaultCardInTheSavedCardsList(cardType1),
					"The default non-PLCC card should be displayed first, after the PLCC cards",
					"The default card is displayed in correct order",
					"The default card is not displayed in correct order", driver);
	
			//5 - Verify the functionality of Make Default
			paymentMethods.makeCardTypeAsDefault("Mastercard");
			Log.message(i++ + ". Made non-PLCC card as default payment!", driver);
	
			Log.softAssertThat(paymentMethods.getDefaultCardType().toLowerCase().contains(cardType2.toLowerCase()),
					"The "+ cardType2 +" should be made default",
					"The "+ cardType2 +" is made default",
					"The "+ cardType2 +" is not made default", driver);
	
			Log.softAssertThat(paymentMethods.verifyOrderOfSavedCards(),
					"The non-PLCC cards should be displayed first followed by the PLCC cards",
					"The cards are displayed in correct order",
					"The cards are not displayed in correct order", driver);
	
			Log.softAssertThat(paymentMethods.verifyPositionOfDefaultCardInTheSavedCardsList("Master Card"),
					"The default non-PLCC card should be displayed first, after the PLCC cards",
					"The default card is displayed in correct order",
					"The default card is not displayed in correct order", driver);
	
			if(Utils.getRunPlatForm().equalsIgnoreCase("mobile")){
				Log.softAssertThat(paymentMethods.verifyMakeDefaultLinkDisplayedForMobile(),
						"The default card should not have Make Default link",
						"The Make Default link is not displayed",
						"The Make Default link is displayed", driver);
			}
			else{
				Log.softAssertThat(paymentMethods.verifyDefaultPaymentIconNotClickable(),
						"The Default Payment icon (green indicator) should not be clickable",
						"The Default Payment icon (green indicator) is not clickable",
						"The Default Payment icon (green indicator) is clickable", driver);
			}
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP5_C22625

}// search
