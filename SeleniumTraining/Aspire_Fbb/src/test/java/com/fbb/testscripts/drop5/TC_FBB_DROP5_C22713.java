package com.fbb.testscripts.drop5;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.GuestEmailPreferencesPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22713 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	@Test(enabled = false, groups = { "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22713(String browser) throws Exception {
		Log.testCaseInfo();
	
		final WebDriver driver = WebDriverFactory.get(browser);
		String brand=Utils.getCurrentBrandShort();
		int i = 1;
		new HomePage(driver, Utils.getWebSite()).get();
		Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
		try {
			driver.get("https://sfcc.qa."+ brand +".plussizetech.com/on/demandware.store/Sites-fbbrands-Site/default/EmailSubscription-Unsubscribe");
			Log.message("Navigated to Email Preferences Unsubscribe Page!", driver);
			GuestEmailPreferencesPage guestEmailPreference = new GuestEmailPreferencesPage(driver).get();
			
			//Step-1: Verify the functionality of Breadcrumb
			if(!Utils.isMobile()) {
				Log.softAssertThat(guestEmailPreference.headers.getBreadCrumbText().trim().equalsIgnoreCase("HOME EMAIL PREFERENCES"), 
						"Breadcrumb should show as Home/Email Preference.", 
						"Breadcrumb shows as Home/Email Preference.", 
						"Breadcrumb does not show as Home/Email Preference.", driver);
			}
			
			guestEmailPreference.headers.clickNthBreadcrumb(0);
			Log.softAssertThat(driver.getCurrentUrl().contains("home"), 
					i++ + ". On clicking Home, User should be navigated to the brand-category-root page.", 
					"On clicking Home, User is navigated to the brand-category-root page.", 
					"On clicking Home, User is not navigated to the brand-category-root page.", driver);
			driver.navigate().back();
	
			//Step-2: Verify the functionality of Header
			if(!Utils.isMobile()) {
				Log.softAssertThat(guestEmailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "homeBreadcrum", "headerText", guestEmailPreference), 
						i++ + ". Header should be located underneath the breadcrumb. ", 
						"Header is located underneath the breadcrumb. ", 
						"Header is not located underneath the breadcrumb. ", driver);
			}
			
			//Step-3: Verify the functionality of Subhead
			Log.softAssertThat(guestEmailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "headerText", "subheadText", guestEmailPreference), 
					i++ + ". Subhead should be located underneath the header.", 
					"Subhead is located underneath the header.", 
					"Subhead is not located underneath the header.", driver);
	
			//Step-4: Verify the functionality of Email Preference Options
			guestEmailPreference.selectUnsubscribe();
			Log.softAssertThat(guestEmailPreference.verifyUnscribeReasonRadioButtons(), 
					i++ + ". User should be able to select any one of the reasons by clicking on the respective radio button.", 
					"User is able to select any one of the reasons.", 
					"User is not able to select all of the reasons.", driver);
			
			//Step-5: Verify the functionality of Email Address
			Log.softAssertThat(guestEmailPreference.verifyEmailFieldEditable(), 
					i++ + ".a) Email field should be editable", 
					"Email field is editable", 
					"Email field is not editable", driver);
			
			Log.softAssertThat(guestEmailPreference.verifyEmailPlaceholderMovestotheTop(), 
					"b) Email place holder should move to top when text is entered", 
					"Email place holder is moved to top", 
					"Email place holder is not moved to top", driver);
			
			Log.softAssertThat(guestEmailPreference.verifyEmailFieldErrorIncorrectValue("asd").equalsIgnoreCase("Please enter a valid email.")
					&& guestEmailPreference.elementLayer.verifyCssPropertyForElement("txtEmailPlaceHolderError", "color", "189, 3, 4", guestEmailPreference), 
					" c&d) Error should thrown when email field is given invalid value", 
					"Error is thrown when email field is given invalid value", 
					"Error is not thrown when email field is given invalid value", driver);
	
			Log.softAssertThat(guestEmailPreference.verifyEmailFieldErrorIncorrectValue("").equalsIgnoreCase("Enter your email address")
					&& guestEmailPreference.elementLayer.verifyCssPropertyForElement("txtEmailPlaceHolderError", "color", "189, 3, 4", guestEmailPreference), 
					" d) Error should thrown when email field is given empty value", 
					"Error is thrown when email field is given empty value", 
					"Error is not thrown when email field is given empty value", driver);
			
			//Step-6: Verify the functionality of Submit
			guestEmailPreference.typeEmail("abc@def.com");
			guestEmailPreference.clickEmailSubmit();
			Log.softAssertThat(guestEmailPreference.elementLayer.verifyAttributeForElement("lblEmailPreferencesSubHeading", "innerHTML", "Thank you for updating your email options.", guestEmailPreference), 
					i++ + "After submiting valid email address, email preference update message should be displayed.", 
					"Email preference update message is displayed correctly.", 
					"Email preference update message is not displayed correctly.", driver);
			
			Log.reference("Step further covered in Test Case ID: 22715");
	
			//Step-7: Verify the functionality of Questions and Answers
			Log.softAssertThat(guestEmailPreference.verifyExpandedState(), 
					i++ +". a) On clicking anywhere in the question content, answer for the respective question should be displayed", 
					"On clicking anywhere in the question content, answer for the respective question is displayed", 
					"On clicking anywhere in the question content, answer for the respective question is not displayed", driver);
	
			Log.softAssertThat(guestEmailPreference.verifyCollapsedState(), 
					" b) On clicking anywhere in the question content, answer for the respective question should be collapsed", 
					"On clicking anywhere in the question content, answer for the respective question is collapsed", 
					"On clicking anywhere in the question content, answer for the respective question is not collapsed", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}

}// search
