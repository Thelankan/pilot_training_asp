package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22578 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22578(String browser) throws Exception {
		Log.testCaseInfo();
	
		String searchKey = prdData.get("prd_po-box-restricted") + "|" + prdData.get("prd_po-box-unrestricted");  
		String email = "test@yopmail.com";
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, email);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
	
			int intialCount = checkoutPage.sizeOfTheCart();
	
			checkoutPage.fillAddressline1Shipping("PO BOX 344");
			Log.message(i++ +". Filled PO Box address.");
			//3
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("divPoBoxBlockOverlay"), checkoutPage), 
					"PO Exclusion overlay should be displayed.", 
					"PO Exclusion overlay is displayed.", 
					"PO Exclusion overlay is not displayed.", driver);
			//4
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtPoBoxBlockOverlayHeading", "txtPoBoxBlockOverlayIntro", checkoutPage), 
					"PO Box Heading message should be displayed on the top of PO Box Exception modal in a gray section box", 
					"PO Box Heading message is displayed on the top of PO Box Exception modal in a gray section box", 
					"PO Box Heading message is not displayed on the top of PO Box Exception modal in a gray section box", driver);
			//5
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtPoBoxBlockOverlayHeading", "txtPoBoxBlockOverlayIntro", checkoutPage), 
					"The Item Introduction Text should be displayed below the PO Box Heading message", 
					"The Item Introduction Text is displayed below the PO Box Heading message", 
					"The Item Introduction Text is not displayed below the PO Box Heading message", driver);
			//6
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtPoBoxBlockOverlayIntro", "poBoxBlockOverlayItemsSection", checkoutPage), 
					"Item Information should be displayed below the Item Introduction Text", 
					"Item Information is displayed below the Item Introduction Text", 
					"Item Information is not displayed below the Item Introduction Text", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("mainImageInPoBoxOverlay","productNameInPoBoxOverlay","productSizeInPoBoxOverlay","productColorInPoBoxOverlay","productPriceInPoBoxOverlay"), checkoutPage)
							|| checkoutPage.elementLayer.verifyPageElements(Arrays.asList("mainImageInPoBoxOverlay","productNameInPoBoxOverlay","productShoeSizeInPoBoxOverlay","productColorInPoBoxOverlay","productPriceInPoBoxOverlay"), checkoutPage),
					"Item Information should contain the componenet image, name , size ,color and price.", 
					"Item Information is contain the componenet image, name , size ,color and price.", 
					"Item Information is not contain the componenet image, name , size ,color and price.", driver);
			//8
	
			if(Utils.isMobile())
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnPoBoxBlockOverlayRemoveItems", "btnEditPOBoxOverlay", checkoutPage), 
						"The 'Edit Your Shipping Address' Action Button should be displayed to the right of the 'Remove Items' (Return to Cart Action) Button.",
						"The 'Edit Your Shipping Address' Action Button is displayed to the right of the 'Remove Items' (Return to Cart Action) Button.",
						"The 'Edit Your Shipping Address' Action Button is not displayed to the right of the 'Remove Items' (Return to Cart Action) Button.", driver);
			}
			else
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnEditPOBoxOverlay", "btnPoBoxBlockOverlayRemoveItems", checkoutPage), 
						"The 'Edit Your Shipping Address' Action Button should be displayed to the right of the 'Remove Items' (Return to Cart Action) Button.",
						"The 'Edit Your Shipping Address' Action Button is displayed to the right of the 'Remove Items' (Return to Cart Action) Button.",
						"The 'Edit Your Shipping Address' Action Button is not displayed to the right of the 'Remove Items' (Return to Cart Action) Button.", driver);
			}
			//9	
			checkoutPage.clickEditInPORestrictedOverlay();
	
	
			Log.softAssertThat(!checkoutPage.elementLayer.verifyPageElements(Arrays.asList("divPoBoxBlockOverlay"), checkoutPage), 
					"PO Exclusion overlay should not be displayed.", 
					"PO Exclusion overlay is not displayed.", 
					"PO Exclusion overlay is displayed.", driver);
	
			checkoutPage.fillAddressline1Shipping("PO BOX 344");
			Log.message(i++ +". Filled PO Box address.");
	
			//7
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "poBoxBlockOverlayItemsSection", "btnPoBoxBlockOverlayRemoveItems", checkoutPage), 
					"The 'Remove Items' (Return to Cart Action Button) should be displayed below the Item Information box section, indented to the left",
					"The 'Remove Items' (Return to Cart Action Button) is displayed below the Item Information box section, indented to the left",
					"The 'Remove Items' (Return to Cart Action Button) is not displayed below the Item Information box section, indented to the left", driver);
			checkoutPage = checkoutPage.clickRemoveItemsPOBoxOverlay();
			int lastcount = checkoutPage.sizeOfTheCart();
			
			Log.softAssertThat(intialCount != lastcount, 
					"PO Restricted product should removed successfully.", 
					"PO Restricted product is removed successfully.", 
					"PO Restricted product is not removed successfully.", driver);
			//1
			Log.softAssertThat(!checkoutPage.elementLayer.verifyPageElements(Arrays.asList("divPoBoxBlockOverlay"), checkoutPage), 
					"PO Exclusion overlay should not be displayed.", 
					"PO Exclusion overlay is not displayed.", 
					"PO Exclusion overlay is displayed.", driver);
			checkoutPage.fillAddressline2Shipping("Text Address");
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e);
		} // catch
		finally {
			Log.endTestCase();
		} // finally
	
	}
}// search
