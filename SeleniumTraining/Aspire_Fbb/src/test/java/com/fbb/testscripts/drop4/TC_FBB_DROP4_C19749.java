package com.fbb.testscripts.drop4;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.headers.HamburgerMenu;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C19749 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C19749(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = prdData.get("prd_variation");
		String usernameGuest;
		String username;
		String password;
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		usernameGuest = username = AccountUtils.generateEmail(driver);
		password = accountData.get("password_global");
	
		//Prerequisite: A registered user account
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//#item 1 - In stock
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBag();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			//Navigate to cart
			ShoppingBagPage cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
	
			checkoutPage.enterGuestUserEmail(username);
	
			checkoutPage.continueToShipping();
	
			//1 - Verify the edit link functionality
	
			Log.softAssertThat(checkoutPage.getGuestEmailID().equals(username),
					"The Guest Email should be displayed",
					"The Guest Email is displayed",
					"The Guest Email is not displayed", driver);
	
			checkoutPage = checkoutPage.clickEditCustomerSignin();
			Log.message(i++ + ". Clicked on Edit link.", driver);
	
			//Log.failsoft("In Guest Checkout page, Edit link functionality is not working, error message validation for fields are not appropriate : PXSFCC-1294");
			//Log.failsoft("Test Case needs to be updated according to comments in PXSFCC-1294");
	
			//3 -When the customer is navigated back to the login page enter a different guest user email id and click continue button
			checkoutPage.enterGuestUserEmail(usernameGuest);
			Log.message(i++ + ". Typed new Guest Email.", driver);
	
			checkoutPage.continueToShipping();
			Log.message(i++ + ". Clicked on continue and navigated to Checkout page.", driver);
	
	
			Log.softAssertThat(checkoutPage.getGuestEmailID().equals(usernameGuest),
					"The new Guest Email should be displayed",
					"The new Guest Email is displayed",
					"The new Guest Email is not displayed", driver);
	
			checkoutPage = checkoutPage.clickEditCustomerSignin();
			Log.message(i++ + ". Clicked on Edit link.", driver);
	
			//2 - When the customer is navigated back to the login page enter valid email and password and click Sign In button
			checkoutPage.typeOnEmail(username);
			Log.message(i++ + ". Typed Email.", driver);
	
			checkoutPage.typeOnPassword(password);
			Log.message(i++ + ". Typed password.", driver);
	
			checkoutPage = (CheckoutPage)checkoutPage.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);
	
			if(Utils.getCurrentBrand().equals(Brand.jl))
				homePage = checkoutPage.headers.chooseBrandFromHeader(Brand.jl);
			else if(Utils.getCurrentBrand().equals(Brand.ww))
				homePage = checkoutPage.headers.chooseBrandFromHeader(Brand.ww);
			else if(Utils.getCurrentBrand().equals(Brand.rm))
				homePage = checkoutPage.headers.chooseBrandFromHeader(Brand.rm);
			else if(Utils.getCurrentBrand().equals(Brand.ks))
				homePage = checkoutPage.headers.chooseBrandFromHeader(Brand.ks);
			else
				Log.fail("Current brand not yet configured.", driver);
	
			MyAccountPage myAcc = null;
	
			if(Utils.getRunPlatForm().equals("desktop") || Utils.getRunPlatForm().equals("tablet"))
			{
				myAcc = homePage.headers.navigateToMyAccount();
				Log.message(i++ + ". Navigated to My Account page.", driver);
			}
			else 
			{
				HamburgerMenu hMenu = (HamburgerMenu) homePage.headers.openCloseHamburgerMenu("open");
				Log.message(i++ + ". Hamburger Menu Opened!");
	
				myAcc = hMenu.navigateToMyAccount();
				Log.message(i++ + ". Navigated to My Account page!");
			}
	
	
			Log.softAssertThat(myAcc.equals(null) == false,
					"The user should be logged in",
					"The user is logged in",
					"The user is not logged in", driver);
	
			cartPage = myAcc.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			cartPage.removeAllItemsFromCart();
			Log.message(i++ + ". Removed All Items from the Cart.", driver);
	
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C19749
}// search
