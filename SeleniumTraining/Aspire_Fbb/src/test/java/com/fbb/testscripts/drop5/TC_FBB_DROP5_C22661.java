package com.fbb.testscripts.drop5;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22661 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22661(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String credentials = accountData.get("credential_plcc_global");
		String email = credentials.split("\\|")[0];
		String password = credentials.split("\\|")[1];
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			MyAccountPage myAcc = headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Navigated to PLCC card holder account.", driver);
			
			PaymentMethodsPage payment = myAcc.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to Payment page.", driver);
			
			//Step-1: Verify the functionality of Card Details
			payment.expandSavedCardList();
			Log.softAssertThat(payment.verifySavedCardLogo(), 
					"Card Logo should be displayed first and left of Card Name", 
					"Card Logo is displayed first and left of Card Name", 
					"Card Logo is not displayed first and left of Card Name", driver);
			
			Log.softAssertThat(payment.verifyDetailsSavedPLCC(), 
					"Verify PLCC card details are in correct order.", 
					"PLCC card details are in correct order.", 
					"PLCC card details are not in correct order.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M1_FBB_DROP5_C22661

}
