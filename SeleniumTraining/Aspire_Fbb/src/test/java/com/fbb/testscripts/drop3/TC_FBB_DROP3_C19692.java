package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19692 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader demandData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "low", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19692(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			//Add Brand to "if" when hemming is available to brand
			if(Utils.getCurrentBrand().equals(Brand.ks)) {
				//Test data
				String prdID = prdData.get("prd_hemming");
				String hemmingMessage = demandData.get("HemmingMessage");
				String hemmingTooltip = prdData.get("hemming_tooltip");
				String brandColor = prdData.get("highlight_color");
				String hemOption = prdData.get("hemming_options");
				String hemOption1 = hemOption.split("\\|")[0];
				String hemOption2 = hemOption.split("\\|")[1];
				
				PdpPage pdp = headers.navigateToPDP(prdID);
				Log.message(i++ + ". Navigated to Product.", driver);
				
				pdp.selectSizeFamily("Big");
				pdp.selectSize();
				Log.message(i++ + ". Selected size", driver);
				
				//Step-1: Verify the title in product details page for Variation product
				Log.softAssertThat(pdp.verifyHemmingCheckboxDisplayed(), 
						"Hemming option should be displayed", 
						"Hemming option is displayed", 
						"Hemming option is not displayed", driver);
				
				Log.softAssertThat(pdp.verifyHemmingTitle(), 
						"The title should be handled via optionName attribute of the product option", 
						"The title is handled via optionName attribute of the product option", 
						"The title is not handled via optionName attribute of the product option", driver);
				
				Log.softAssertThat(pdp.elementLayer.verifyVerticalAllignmentOfElements(driver, "divSizeGrid", "chkProductHemming", pdp), 
						"Hemming option should be displayed below the Size attribute", 
						"Hemming option is displayed below the Size attribute", 
						"Hemming option is not displayed below the Size attribute", driver);
				
				//Step-2: Verify the Checkbox in the Hemming section in product details page for variation product
				Log.softAssertThat(pdp.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtHemmingTitle", "chkProductHemming", pdp)
						&& pdp.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtHemmingMessaging", "chkProductHemming", pdp), 
						"The checkbox should be displayed to the left-hand side below the title HEMMING", 
						"The checkbox is displayed to the left-hand side below the title HEMMING", 
						"The checkbox is not displayed to the left-hand side below the title HEMMING", driver);
				
				Log.softAssertThat(pdp.elementLayer.verifyPageElementsUnChecked(Arrays.asList("chkProductHemming"), pdp), 
						"By default, the Hemming checkbox should be unchecked", 
						"the Hemming checkbox is unchecked", 
						"the Hemming checkbox is checked", driver);
				
				pdp.selectSizeFamily("Big");
				pdp.selectSize();
				Log.message(i++ + ". Selected a BIG size.", driver);
				
				pdp.clickOnHemmingCheckbox("enable");
				Log.message(i++ + ". Checked hemming option", driver);
				
				Log.softAssertThat(pdp.verifyHemmingLengthRange("26", "36"), 
						"For the Big family size, Hemming Length should be 26\" - 36\" ", 
						"For the Big family size, Hemming Length is 26\" - 36\" ", 
						"For the Big family size, Hemming Length is not 26\" - 36\" ", driver);
				
				pdp.selectSizeFamily("Tall");
				pdp.selectSize();
				Log.message(i++ + ". Selected a TALL size.", driver);
				
				pdp.clickOnHemmingCheckbox("enable");
				Log.message(i++ + ". Checked hemming option", driver);
				
				//Step-6
				pdp.clickOnUpdate();
				Log.message(i++ + ". Clicked on add to bag.", driver);
				
				Log.softAssertThat(pdp.elementLayer.VerifyElementDisplayed(Arrays.asList("txtHemmingErrorMessage"), pdp), 
						"A hem length must be selected if checkbox is checked prior to adding to cart.", 
						"A hem length is required if checkbox is checked prior to adding to cart.", 
						"A hem length was not required if checkbox is checked prior to adding to cart.", driver);
				
				Log.softAssertThat(pdp.verifyHemmingLengthRange("31", "38"), 
						"For the tall family size, Hemming Length should be 31\" - 38\" ", 
						"For the tall family size, Hemming Length is 31\" - 38\" ", 
						"For the tall family size, Hemming Length is not 31\" - 38\" ", driver);
				
				//Step-3, 4: Verify the messaging in the Hemming section in product details page for variation product
				Log.softAssertThat(pdp.verifyMessagingDisplayedRightOfHemmingCheckbox(), 
						"Messaging should be displayed next to the checkbox", 
						"Messaging is displayed next to the checkbox", 
						"Messaging is not displayed next to the checkbox", driver);
				
				Log.softAssertThat(pdp.elementLayer.verifyAttributeForElement("txtHemmingMessaging", "innerHTML", hemmingMessage, pdp), 
						"Messaging should be handled via the description attribute of the product option", 
						"Messaging is handled via the description attribute of the product option", 
						"Messaging is not handled via the description attribute of the product option", driver);
				
				//Step-5: Verify the Tips section in the Hemming option in product details page for variation product
				Log.softAssertThat(pdp.verifyHemmingTipsDisplayedRightOfHemmingTitle(), 
						"Tips section should be displayed to the right hand side of the hemming title", 
						"Tips section is displayed to the right hand side of the hemming title", 
						"Tips section is not displayed to the right hand side of the hemming title", driver);
				
				pdp.clickOnTipsInHemmingSection();
				Log.message(i++ + ". Clicked on tooltip.", driver);
				
				Log.softAssertThat(pdp.elementLayer.VerifyElementDisplayed(Arrays.asList("msgTooltip"), pdp), 
						"It should open a flyout ", 
						"It opens a flyout ", 
						"It does not open a flyout ", driver);
				
				Log.softAssertThat(pdp.elementLayer.getAttributeForElement("msgTooltip", "innerHTML", pdp).replaceAll("<br>", " ").contains(hemmingTooltip), 
						"The content within the flyout should be handled via brand-specific content asset", 
						"The content within the flyout is handled via brand-specific content asset", 
						"The content within the flyout is not handled via brand-specific content asset", driver);
				
				pdp.openCloseToolTipOverlay("close");
				Log.softAssertThat(!pdp.elementLayer.VerifyElementDisplayed(Arrays.asList("msgTooltip"), pdp), 
						"On click the close button, the tool tip should close", 
						"On click the close button, the tool tip closes", 
						"On click the close button, the tool tip does not close", driver);
				
				pdp.clickOnTipsInHemmingSection();
				Log.message(i++ + ". Clicked on tooltip.", driver);
				
				BrowserActions.clickOnEmptySpaceVertical(driver, "msgTooltip", pdp);
				Log.softAssertThat(!pdp.elementLayer.VerifyElementDisplayed(Arrays.asList("msgTooltip"), pdp), 
						"On click anywhere outside of the tooltip, the tool tip should close.", 
						"On click anywhere outside of the tooltip, the tool tip closes.", 
						"On click anywhere outside of the tooltip, the tool tip does not close.", driver);
				
				//Step-6: Verify the options Drawer functionality for the Variation product
				Log.softAssertThat(pdp.getSelectedHemmingOption().equals("none"), 
						"No hem length should be selected by default.", 
						"No hem length is selected by default.", 
						"A hem length is not selected by default.", driver);
				
				pdp.clickSelectHemOption(hemOption1);
				Log.message(i++ + ". Selected "+hemOption1, driver);
				pdp.clickSelectHemOption(hemOption2);
				Log.message(i++ + ". Selected "+hemOption2, driver);
				
				Log.softAssertThat(!pdp.verifyHemOptionSelected(hemOption1) && pdp.verifyHemOptionSelected(hemOption2), 
						"User should be able to select one hem length value once at a time", 
						"User is able to select one hem length value once at a time", 
						"User is not able to select one hem length value once at a time", driver);
				
				pdp.clickOnHemmingCheckbox("disable");
				pdp.clickOnHemmingCheckbox("enable");
				Log.message(i++ + ". Selected Hemming", driver);
				
				pdp.clickSelectHemOption(hemOption2);
				Log.message(i++ + ". Selected "+hemOption2, driver);
				Log.softAssertThat(pdp.elementLayer.verifyCssPropertyForElement("liSelectedHemming", "background", brandColor, pdp),
						"Clicking on any one of the hem length, it should be shown in selected state", 
						"Selected hem length is shown in selected state.", 
						"Selected hem length is not shown in selected state.", driver);
				
				//Step-7: Click on another hemming length 
				Log.reference("Step 7 verifications covered in previous step.");
				
				//Step-8,9: Select hemming checkbox & select hemming length, after that, reset the size/color
				String selectedHemming = pdp.getSelectedHemmingOption();
				pdp.selectSize();
				Log.message(i++ + ". Selected a TALL size", driver);
				
				Log.softAssertThat(pdp.getSelectedHemmingOption().equals(selectedHemming), 
						"The system should retain the previously selected option", 
						"The system retains the previously selected option", 
						"The system does not retain the previously selected option", driver);
				
				//Step-10: Select hemming checkbox & select hemming length, after that, reset the size family
				pdp.selectSizeFamily("Big");
				pdp.selectSize();
				Log.message(i++ + ". Selected a BIG size.", driver);
				pdp.clickOnHemmingCheckbox("enable");
				
				Log.softAssertThat(pdp.getSelectedHemmingOption().equals("none"), 
						"The system should NOT retain the previously selected option.", 
						"The system does NOT retain the previously selected option.", 
						"The system retains the previously selected option.", driver);
				
				//Step-11: Click on Add to bag button by selecting an option in checkbox
				pdp.clickSelectHemOption(hemOption1);
				Log.message(i++ + ". Selected "+hemOption1, driver);
				pdp.addProductToBag();
				Log.message(i++ + ". Added Product to bag.");
				ShoppingBagPage bag = headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping bag page.", driver);
				
				Log.softAssertThat(bag.elementLayer.verifyAttributeForElement("divSpecialMessageHemmingValue", "innerHTML", hemOption1, bag), 
						"For successful validation, Selected hemming length should displaying on the cart.", 
						"Selected hemming length is displaying on the cart.", 
						"Selected hemming length is not displaying on the cart.", driver);
				
				bag.removeAllCartProduct();
				pdp = headers.navigateToPDP(prdID);
				Log.message(i++ + ". Navigated to Product.", driver);
				
				//Step-12: Click on Add to bag button by without selecting an option in checkbox
				Log.reference("Step covered as part of step 6.");
				
				//Step-13: Verify by unchecking the selected checkbox
				pdp.selectSizeFamily();
				pdp.selectSize();
				Log.message(i++ + ". Selected size", driver);
				
				pdp.clickOnHemmingCheckbox("enable");
				Log.message(i++ + ". Checked hemming option", driver);
				
				pdp.clickSelectHemOption(hemOption1);
				Log.message(i++ + ". Selected "+hemOption1, driver);
				
				pdp.clickOnHemmingCheckbox("disable");
				Log.message(i++ + ". Unchecked hemming option", driver);
				
				pdp.clickOnHemmingCheckbox("enable");
				Log.message(i++ + ". Checked hemming option", driver);
				
				//Step-14: Verify the Disclaimer message displaying in the Hemming option
				Log.softAssertThat(pdp.elementLayer.verifyVerticalAllignmentOfElements(driver, "divHemmingDrawer", "txtHemmingDisclaimer", pdp), 
						"When selecting the length option check box, disclaimer message should be displayed below the option drawer.", 
						"Disclaimer message is displayed below the option drawer.", 
						"Disclaimer message is not displayed below the option drawer.", driver);
				
				//Step-13 contd			
				Log.softAssertThat(pdp.getSelectedHemmingOption().equals(hemOption1), 
						"When again reselecting the checkbox, the page should retain the previously selected option.", 
						"The page retains the previously selected option.", 
						"The page does not retain the previously selected option.", driver);
				
				pdp.clickOnHemmingCheckbox("disable");
				Log.message(i++ + ". Unchecked hemming option", driver);
				
				Log.softAssertThat(!pdp.elementLayer.VerifyElementDisplayed(Arrays.asList("divHemmingDrawer"), pdp), 
						"If the user unchecks the checkbox, the drawer should be closed.", 
						"If the user unchecks the checkbox, the drawer is closed.", 
						"If the user unchecks the checkbox, the drawer is not closed.", driver);
				
				pdp.addProductToBag();
				Log.message(i++ + ". Added Product to bag.");
				bag = headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shoppingbag page.", driver);
				
				Log.softAssertThat(!bag.elementLayer.VerifyElementDisplayed(Arrays.asList("divSpecialMessageHemming"), bag), 
						"Hemming options should not be included when the product is added to the cart.", 
						"Hemming options is not included when the product is added to the cart.", 
						"Hemming options is included when the product is added to the cart.", driver);
			
			}else {
				Log.reference("Hemming is currently not available on " + Utils.getCurrentBrand().getConfiguration());
			}
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M1_FBB_DROP3_C19692

}
