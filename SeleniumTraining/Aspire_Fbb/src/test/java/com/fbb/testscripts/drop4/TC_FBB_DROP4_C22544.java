package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22544 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "medium", "desktop", "tablet", "mobile"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22544(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("qc_valid");
		String username;
		String password = accountData.get("password_global");
	
		//Create web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			QuickOrderPage quickOrderPage = homePage.headers.navigateToQuickOrder();
			Log.message(i++ + ". Navigated to Quick order Page!", driver);
	
			quickOrderPage.searchItemInQuickOrder(searchKey);
	
			String sizeSelected = quickOrderPage.selectSize();
			Log.message(i++ + ". Size Selected :: '"+sizeSelected+"'", driver);
			
			String sizeFamilySelected = quickOrderPage.selectSizeFamily();
			Log.message(i++ + ". Size Family Selected :: '"+sizeFamilySelected+"'", driver);
			
			String colorSelected = quickOrderPage.selectColor();
			Log.message(i++ + ". Selected Color :: '"+colorSelected+"'", driver);
			
			String prdAdded = quickOrderPage.getProductNameInQOPage();
			Log.message(i++ + ". Product Added in Quick order page is '"+prdAdded+"'");
	
			quickOrderPage.clickAddProductToBag();
			Log.message(i++ + ". Product is added to cart");
	
			quickOrderPage.clickOnMiniCartFlyoutCloseButton();
			Log.message(i++ + ". Mini cart flyout is closed");
	
			ShoppingBagPage shoppingBagPage = quickOrderPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on 'My Bag' icon in header and navigated to shopping bag page");
	
			Log.reference("Refer Test case # 22533 for Step 1");
	
			Log.softAssertThat(shoppingBagPage.verifyProductAddedInCart(prdAdded), 
					"Product added in quick order page should be added to cart", 
					"Product added in quick order page is added to cart", 
					"Product added in quick order page is not added to cart", driver);
	
			if(runPltfrm.equals("mobile")){
				Log.softAssertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("quickOrderCatalogBagdeMobile"), shoppingBagPage), 
						"Quick order badge should be displayed!", 
						"Quick order badge is displayed", 
						"Quick order badge is not displayed", driver);
			}
			else{
	
				Log.softAssertThat(shoppingBagPage.elementLayer.verifyPageElements(Arrays.asList("quickOrderCatalogBagde"), shoppingBagPage), 
						"Quick order badge should be displayed!", 
						"Quick order badge is displayed", 
						"Quick order badge is not displayed", driver);
			}
			Log.testCaseResult();
	
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			GlobalNavigation.removeAllItemsFromQuickOrder(driver);
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C22544
}// search
