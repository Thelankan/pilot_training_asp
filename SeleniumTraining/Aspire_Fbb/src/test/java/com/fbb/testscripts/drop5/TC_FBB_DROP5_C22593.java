package com.fbb.testscripts.drop5;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22593 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader dwreData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22593(String browser) throws Exception {
		Log.testCaseInfo();
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		String invalidEmail = "asf123@";
		String pwShort = "test";
	
		int i = 1;
		try {
			List<String> elementsToBeVerified = null;
			List<String> monthElementsToBeVerified = null;
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message("Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			SignIn signInPage=homePage.headers.clickOnSignInLink(); 
	
			signInPage.clickOnCreateAccount();
	
			//Step-1: Verify the functionality of Headline element
			Log.softAssertThat(signInPage.elementLayer.verifyVerticalAllignmentOfListOfElements(Arrays.asList("breadCrumb","txtHeaderElement","txtSubHeaderElement"), signInPage), 
					"The headline element should be present at the top of the New Customer module", 
					"The headline element is present at the top of the New Customer module", 
					"The headline element is not present at the top of the New Customer module", driver);
	
			//Step-2: Verify the functionality of Subheading element
			Log.message(i++ +". Step-2, The Subheading element should be displayed below the Headline, is covered in Step-1", driver);
	
			//Step-3: Verify the functionality of the Facebook Registration button
			if(signInPage.elementLayer.VerifyElementDisplayed(Arrays.asList("facebookLink"), signInPage)) {
				Log.softAssertThat(signInPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtSubHeaderElement", "facebookLink", signInPage), 
						"The 'Register with Facebook' button should be displayed below the subheading element", 
						"The 'Register with Facebook' button is displayed below the subheading element", 
						"The 'Register with Facebook' button is not displayed below the subheading element", driver);
				
				boolean resultFaceBook=signInPage.verifyFaceBookButton();
				Log.softAssertThat(resultFaceBook, 
						"The 'Register with Facebook' button should take the User to the Facebook Page", 
						"The 'Register with Facebook' button should take the User to the Facebook as expected", 
						"The 'Register with Facebook' button should take the User to the Facebook ", driver);
	
				if(resultFaceBook){
					String url = Utils.getCurrentBrandShort().toLowerCase();
	
					driver.navigate().to("http://sfcc.qa."+url+".plussizetech.com");
					signInPage=homePage.headers.clickOnSignInLink(); 
					signInPage.clickOnCreateAccount();
				}
			}
	
			//Step-4: Verify the functionality of First Name
			//verify Text field is mandatory
			signInPage.clickOnRegisterBtn();
	
			elementsToBeVerified = Arrays.asList("txtFirstNameMandatoryError", "txtLastNameMandatoryError","txtEmailMandatoryError","txtConfirmEmailMandatoryError","txtPasswordMandatoryError","txtConfirmPasswordMandatoryError");
			elementsToBeVerified = Arrays.asList("txtFirstNameMandatoryError");
	
			Log.softAssertThat(signInPage.elementLayer.verifyPageElements(elementsToBeVerified, signInPage), 
					"The First Name Field should be show as mandatory in the Create Account page", 
					"The First Name is showing as mandatory in the Create Account page as expected!!", 
					"The First Name is not show as mandatory in the Create Account page!!", driver);
	
			Log.softAssertThat(signInPage.verifyFieldErrorMessages("test@yop",dwreData.get("FirstNameError"),"FirstName"), 
					"System should display an appropriate error message for Incorrect format of data enter into the FirstName", 
					"System is displayed an appropriate error message for Incorrect format of data enter into the FirstName as expected!!", 
					"System is not display an appropriate error message for Incorrect format of data enter into the FirstName", driver);
	
			Log.softAssertThat(signInPage.enterTextOnField("txtFirstName","FirstName","First Name",signInPage), 
					"User should be able to enter text in the First Name text box", 
					"User is able to enter text in the First Name text box as expected!!", 
					"User is not able to enter text in the First Name text box", driver);
	
			Log.softAssertThat(signInPage.elementLayer.verifyElementDisplayedBelow("txtFirstName", "txtFirstNamePlaceHolder", signInPage), 
					"FirstName Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"FirstName Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"FirstName Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//Step-5: Verify the functionality of Last Name
			elementsToBeVerified = Arrays.asList("txtLastNameMandatoryError");
	
			Log.softAssertThat(signInPage.elementLayer.verifyPageElements(elementsToBeVerified, signInPage), 
					"The Last Name Field should be show as mandatory in the Create Account page", 
					"The Last Name is showing as mandatory in the Create Account page as expected!!", 
					"The Last Name is not show as mandatory in the Create Account page!!", driver);
	
			Log.softAssertThat(signInPage.verifyFieldErrorMessages("#!@sf",dwreData.get("LastNameError"),"LastName"), 
					"System should display an appropriate error message for Incorrect format of data enter into the LastName", 
					"System is displayed an appropriate error message for Incorrect format of data enter into the LastName as expected!!", 
					"System is not display an appropriate error message for Incorrect format of data enter into the LastName", driver);
	
			Log.softAssertThat(signInPage.enterTextOnField("txtLastName","LastName","Last name",signInPage), 
					"User should be able to enter text in the Last Name text box", 
					"User is able to enter text in the Last Name text box as expected!!", 
					"User is not able to enter text in the Last Name text box", driver);
	
			Log.softAssertThat(signInPage.elementLayer.verifyElementDisplayedBelow("txtLastName", "txtLastNamePlaceHolder", signInPage), 
					"LastName Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"LastName Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"LastName Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//Step-6: Verify the functionality of Email Address
			elementsToBeVerified = Arrays.asList("txtEmailMandatoryError");
	
			Log.softAssertThat(signInPage.elementLayer.verifyPageElements(elementsToBeVerified, signInPage), 
					"The Email Id Field should be show as mandatory in the Create Account page", 
					"The Email Id is showing as mandatory in the Create Account page as expected!!", 
					"The Email Id is not show as mandatory in the Create Account page!!", driver);
	
			Log.softAssertThat(signInPage.verifyFieldErrorMessages(invalidEmail, dwreData.get("EmailError"), "Email"), 
					"System should perform regex validation for properly formatted email address", 
					"System is showing error to perform regex validation for properly formatted email address as expected!!", 
					"System is not showing error to perform regex validation for properly formatted email address", driver);
	
			Log.softAssertThat(signInPage.enterTextOnField("txtEmail", email, "Email",signInPage), 
					"User should be able to enter text in the Email Id text box", 
					"User is able to enter text in the Email Id text box as expected!!", 
					"User is not able to enter text in the Email Id text box", driver);
	
			Log.softAssertThat(signInPage.elementLayer.verifyElementDisplayedBelow("txtEmail", "emailPlaceHolder", signInPage), 
					"Email Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"Email Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"Email Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//Step-7: Verify the functionality of Confirm Email Address
			Log.message(i++ +". Step implemented in Step 17");
	
			//Step-8: Verify the functionality of Password
			elementsToBeVerified = Arrays.asList("txtPasswordMandatoryError");
	
			Log.softAssertThat(signInPage.elementLayer.verifyPageElements(elementsToBeVerified, signInPage), 
					"The password Field should be show as mandatory in the Create Account page", 
					"The password is showing as mandatory in the Create Account page as expected!!", 
					"The password is not show as mandatory in the Create Account page!!", driver);
	
			Log.softAssertThat(signInPage.verifyFieldErrorMessages(pwShort,"Please enter at least 8 characters.","Password"), 
					"System should perform password minimum requirements validation on submit for the data combination and its length", 
					"System is perform password minimum requirements validation on submit for the data combination and its length as expected!!", 
					"System is not perform password minimum requirements validation on submit for the data combination and its length", driver);
	
			Log.softAssertThat(signInPage.verifyFieldErrorMessages(pwShort+pwShort,"Please Enter at least 1 letter and 1 number.","Password"), 
					"System should perform validation that password meets the password criteria: 8-10 characters, at least 1 number and 1 letter, all special characters are allowed", 
					"System should perform validation that password meets the password criteria: 8-10 characters, at least 1 number and 1 letter, all special characters are allowed as expected!!", 
					"System should perform validation that password meets the password criteria: 8-10 characters, at least 1 number and 1 letter, all special characters are allowed", driver);
	
			Log.softAssertThat(signInPage.enterTextOnField("txtpasswordField", password, "Password",signInPage), 
					"User should be able to enter text in the Password text box", 
					"User is able to enter text in the Password text box as expected!!", 
					"User is not able to enter text in the Password text box", driver);
	
			Log.softAssertThat(signInPage.elementLayer.verifyElementDisplayedBelow("txtpasswordField", "txtPasswordPlaceHolder", signInPage), 
					"Password Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"Password Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"Password Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			Log.softAssertThat(signInPage.elementLayer.verifyAttributeForElement("txtpasswordField", "type", "password", signInPage), 
					"By default, the data entered by the user should be masked", 
					"By default, the data entered by the user is masked as expected!!", 
					"By default, the data entered by the user is not masked", driver);
	
			//Step-9: Verify the functionality of Password Message
			Log.softAssertThat(signInPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtpasswordField", "txtConfirmPasswordField", signInPage), 
					"The Confirm Password form field should be displayed below the Password field", 
					"The Confirm Password form field is displayed below the Password field", 
					"The Confirm Password form field is not displayed below the Password field", driver);
	
			Log.softAssertThat(signInPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "formFieldSignUp", "txtPasswordMessage", signInPage), 
					"The Password Message element should appear below the Password entry form field", 
					"The Password Message element appears below the Password entry form field", 
					"The Password Message element does not appear below the Password entry form field", driver);
	
			//Step-10: Verify the functionality of Confirm Password
			Log.message(i++ +". Step implemented in Step 17");
	
			//Step-11: Verify the functionality of Show/ Hide
			Log.softAssertThat(signInPage.ClickVerifyPasswordText("text"), 
					"On clicking the Show button system should display the user entered data and Text should changed to Hide", 
					"On clicking the Show button system is displayed the user entered data and Text is changed to Hide as expected!!", 
					"On clicking the Show button system is display the user entered data and Text is not changed to Hide", driver);
	
			Log.softAssertThat(signInPage.ClickVerifyPasswordText("password"), 
					"On clicking the Hide button system should masked the user entered data and Text should changed to Hide", 
					"On clicking the Show button system is masked the user entered data and Text is changed to Hide as expected!!", 
					"On clicking the Show button system is masked the user entered data and Text is not changed to Hide", driver);
	
			//Step-12: Verify the functionality of Birth Month
			monthElementsToBeVerified = Arrays.asList("Birth Month (optional)","January","February","March","April","May","June","July","August","September","October","November","December");
	
			Log.softAssertThat(signInPage.VerifyDropDownSortOrder(monthElementsToBeVerified), 
					"Dropdown should display their sort order of month from January to December", 
					"Dropdown is display their sort order of month from January to December  as expected!!", 
					"Dropdown is not display their sort order of month from January to December ", driver);
	
			Log.softAssertThat(signInPage.selectMonth("January"), 
					"On Selecting the dropdown value ,User should able to selelct", 
					"On Selecting the dropdown value ,User is able to selelct as expected!!", 
					"On Selecting the dropdown value ,User is not able to selelct", driver);
	
			//Step-13: Verify the functionality of Birth Month Message
			Log.softAssertThat(signInPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "selectMonthDrop", "BirthMonthMessage", signInPage), 
					"The Birth Month Message element should be present below the Birth Month dropdown", 
					"The Birth Month Message element is present below the Birth Month dropdown", 
					"The Birth Month Message element is not present below the Birth Month dropdown", driver);
	
			//Step-14: Verify the functionality of Customer ID
			elementsToBeVerified = Arrays.asList("txtCustomerID");
			Log.softAssertThat(signInPage.elementLayer.verifyPageElements(elementsToBeVerified, signInPage), 
					"Customer ID should be a optional field in the signIn Page", 
					"Customer ID is optional field in the signIn Page as expected!!", 
					"Customer ID is not a optional field in the signIn Page", driver);
	
			Log.softAssertThat(signInPage.verifyCheckboxValid("vijay"), 
					"User should be able to enter text in the Customer ID text box", 
					"User is able to enter text in the Customer ID text box as expected!!", 
					"User is not able to enter text in the Customer ID text box", driver);
	
			Log.softAssertThat(signInPage.elementLayer.verifyElementDisplayedBelow("txtCustomerID", "customerIDPlaceHolder", signInPage), 
					"CustomerID Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"CustomerID Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"CustomerID Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//Step-15: Verify the functionality of Customer ID Message
			Log.softAssertThat(signInPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtCustomerID", "customerIdMessage", signInPage), 
					"The Customer ID message element should be present below the Customer ID form field", 
					"The Customer ID message element is present below the Customer ID form field", 
					"The Customer ID message element is not present below the Customer ID form field", driver);
	
			//Step-16: Verify the functionality of Newsletter Signup
			Log.softAssertThat(signInPage.verifyNewsletterDefault(), 
					"Newsletter Signup checkbox should be checked by default", 
					"Newsletter Signup checkbox is checked by default as expected!!", 
					"Newsletter Signup checkbox is not checked by default", driver);
	
			Log.softAssertThat(signInPage.clickOnNewsletterCheckBox(false), 
					"User should be able to uncheck the Newsletter checkbox", 
					"User is able to uncheck the Newsletter checkbox as expected!!", 
					"User is not able to uncheck the Newsletter checkbox", driver);
	
			Log.softAssertThat(signInPage.clickOnNewsletterCheckBox(true), 
					"User should be able to check the Newsletter checkbox", 
					"User is able to check the Newsletter checkbox as expected!!", 
					"User is not able to check the Newsletter checkbox", driver);
	
			//Step-17: Verify the functionality of Create Account
			elementsToBeVerified = Arrays.asList("txtConfirmEmailMandatoryError");
	
			Log.softAssertThat(signInPage.elementLayer.verifyPageElements(elementsToBeVerified, signInPage), 
					"The confirm Email Id Field should be show as mandatory in the Create Account page", 
					"confirm Email Id is showing as mandatory in the Create Account page as expected!!", 
					"confirm Email Id is not show as mandatory in the Create Account page!!", driver);
	
			elementsToBeVerified = Arrays.asList("txtConfirmPasswordMandatoryError");
	
			Log.softAssertThat(signInPage.elementLayer.verifyPageElements(elementsToBeVerified, signInPage), 
					"The Confirm password Id Field should be show as mandatory in the Create Account page", 
					"The Confirm password is showing as mandatory in the Create Account page as expected!!", 
					"The Confirm password is not show as mandatory in the Create Account page!!", driver);
	
			signInPage.enterTextOnField("txtConfirmEmailId","test@yopmail.com","Email confirmation",signInPage);
			signInPage.enterTextOnField("txtConfirmPasswordField","aspire321","Password confirmation",signInPage);
	
			signInPage.clickOnRegisterBtn();
	
			Log.softAssertThat(signInPage.verifyEmailMissmatch("Emails do not match."),
					"The Confirm Email Id Field should require matching Email address.", 
					"The Confirm Email is showing to require matching email address!", 
					"The Confirm Email is not showing to require matching email address.", driver);
	
			Log.softAssertThat(signInPage.verifyPasswordMissmatch("Password and Confirm Password do not match."), 
					"The Confirm password Id Field should require matching password.", 
					"The Confirm password is showing to require matching password as Password firld!", 
					"The Confirm password is not showing to require matching password as Password firld.", driver);
	
			Log.softAssertThat(signInPage.enterTextOnField("txtConfirmEmailId", email, "Email confirmation",signInPage), 
					"User should be able to enter text in the ConfirmEmail text box", 
					"User is able to enter text in the ConfirmEmail text box as expected!!", 
					"User is not able to enter text in the ConfirmEmail text box", driver);
	
			Log.softAssertThat(signInPage.enterTextOnField("txtConfirmPasswordField", password, "Password Confirmation",signInPage), 
					"User should be able to enter text in the Confirm Password text box", 
					"User is able to enter text in the Confirm Password text box as expected!!", 
					"User is not able to enter text in the ConfirmPassword text box", driver);
	
			Log.softAssertThat(signInPage.elementLayer.verifyElementDisplayedBelow("txtConfirmEmailId", "confirmEmailPlaceHolder", signInPage), 
					"Confirm Email Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"Confirm Email Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"Confirm Email Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			Log.softAssertThat(signInPage.elementLayer.verifyElementDisplayedBelow("txtConfirmPasswordField", "confirmPasswordPlaceHolder", signInPage), 
					"Confirm Password Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"Confirm Password Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"Confirm Password Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//Step-18: Verify the functionality of Legal Copy
			Log.softAssertThat(signInPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnSignUp", "txtLegalCopyMessage", signInPage), 
					"The Legal Copy message element should be displayed below the Create Account button.", 
					"The Legal Copy message element is displayed below the Create Account button.", 
					"The Legal Copy message element is not displayed below the Create Account button.", driver);
	
			//Step disabled until account deletion is added to framework.
			/*MyAccountPage myAccount=(MyAccountPage)signInPage.clickOnConfirmRegisterBtn();
			ProfilePage profile= myAccount.navigateToUpdateProfile();
	
			Log.softAssertThat(profile.verifyFirstNameIsCorrect("FirstName"), 
					"The FirstName should be matched with my profile page", 
					"The FirstName is matched with my profile page as expected!!", 
					"The FirstName is not matched with my profile page", driver);
	
			Log.softAssertThat(profile.verifyLastNameIsCorrect("LastName"), 
					"The LastName should be matched with my profile page", 
					"The LastName is matched with my profile page as expected!!", 
					"The LastName is not matched with my profile page", driver);
	
			Log.softAssertThat(profile.verifyEmailAddressIsCorrect(AccountUtils.generateEmail(driver)), 
					"The Email should be matched with my profile page", 
					"The Email is matched with my profile page as expected!!", 
					"The Email is not matched with my profile page", driver);*/
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP_C22593

}// search
