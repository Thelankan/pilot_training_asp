package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.PaypalConfirmationPage;
import com.fbb.pages.PaypalPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22530 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22530(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
		String paypalemail = accountData.get("credential_paypal").split("\\|")[0];
		String paypalpassword = accountData.get("credential_paypal").split("\\|")[1];
		String couponcode= prdData.get("cpn_promo-ship-level");
	
		//Create web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		//Prerequisite: A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, AccountUtils.generateEmail(driver) + "|test123@");
		}
	
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, AccountUtils.generateEmail(driver) + "|test123@");
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];		
	
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", "Ground", "valid_address7");
			Log.message(i++ + ". Shipping Address filled successfully.", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Billing/Payment section.", driver);
	
			checkoutPage.applyPromoCouponCode(couponcode);
	
			checkoutPage.selectPaypalpayment();
			Log.message(i++ + ". Selected 'Paypal' as payment method", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnPaypal"), checkoutPage), 
					"After entering a coupon code, the 'Continue with PayPal' button will not be disabled", 
					"After entering a coupon code, the 'Continue with PayPal' button is not disabled", 
					"After entering a coupon code, the 'Continue with PayPal' button is disabled", driver);
	
			Log.softAssertThat(checkoutPage.getContinuePaypalButtonText().equalsIgnoreCase("CONTINUE WITH PAYPAL"),
					"When selecting PayPal as their Payment Method, the button text should be changed to 'Continue with PayPal'",
					"When selecting PayPal as their Payment Method, the button text is changed to 'Continue with PayPal'",
					"When selecting PayPal as their Payment Method, the button text is not changed to 'Continue with PayPal'", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("rewardCertificateSection"), checkoutPage), 
					"Reward certificate section should not be displayed after selecting paypal radio button!", 
					"Reward certificate section is not displayed after selecting paypal radio button!", 
					"Reward certificate section is displayed after selecting paypal radio button!", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("giftCardSection"), checkoutPage), 
					"Gift card section should not be displayed after verifyPageElementsDoNotExist paypal radio button!", 
					"Gift card section is not displayed after selecting paypal radio button!", 
					"Gift card section is displayed after selecting paypal radio button!", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "promoHeader", "btnPaypal", checkoutPage), 
					"The 'Continue with PayPal' Button should be displayed below the 'Have a Promo Code?' section, indented to the right.", 
					"The 'Continue with PayPal' Button is displayed below the 'Have a Promo Code?' section, indented to the right",
					"The 'Continue with PayPal' Button is not displayed below the 'Have a Promo Code?' section, indented to the right", driver);
	
			if(runPltfrm.equals("mobile") || runPltfrm.equals("tablet")){
				Log.message(++i + "Browser limitation for Payapal in mobile and tablet");
			}
			else{
				PaypalPage paypalPage = checkoutPage.clickOnPaypalButton();
				Log.message(i++ + ". Clicked on Paypal button in shopping bag page");
	
				PaypalConfirmationPage pcp=paypalPage.enterPayapalCredentials(paypalemail,paypalpassword);
				Log.message(i++ + ". Continued with Paypal Credentials.", driver);
				
				pcp.clickContinueConfirmation();
				Log.message(i++ + ". Clicked on Continue button.", driver);
	
				Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPage), 
						"Checkout page should be displayed!", 
						"Checkout page is displayed",
						"Checkout page is not displayed", driver);
	
				checkoutPage.clickOnPlaceOrderButton();
				Log.message(i++ + ". Clicked on Place order button", driver);
	
				Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") || driver.getCurrentUrl().contains("revieworder") ,
						"Order should be placed successfully as a Guest user and Order confirmation page should be displayed!", 
						"Order is placed successfully as a Guest user and Order confirmation page is displayed!",
						"Order is not placed successfully as a Guest user and Order confirmation page is not displayed!", driver);
			}
	
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C22530
}// search
