package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C22633 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C22633(String browser) throws Exception {
		Log.testCaseInfo();
		
		//Get WebDriver Instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.footers.navigateToEGiftCard();
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.selectGCSize();
			pdpPage.enterRecepientEmailAdd("automation@yopmail.com");
			pdpPage.enterRecepientEmailConfirmAdd("automation@yopmail.com");
			pdpPage.checkGCPersonalizedMessage("check");
			pdpPage.enterGCFromAddress("automation@yopmail.com");
			pdpPage.enterGCPersonalMessage("Test Personal Message");
			pdpPage.addProductToBag();
			Log.message(i++ +". Product added to cart",driver);
			
			ShoppingBagPage shopBag = pdpPage.clickOnCheckoutInMCOverlay();
			
			pdpPage = shopBag.clickonImageInCart();
			Log.message(i++ +". Click on product image",driver);
			
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage),
					"System should Navigate to Pdp page.",
					"System Navigated to Pdp page",
					"System not Navigated to Pdp page", driver);
			
			shopBag = pdpPage.headers.clickOnBagLink();
			Log.message(i++ + ". Navigated to Checkout page",driver);
			
			pdpPage = shopBag.clickonProductNameInCart();
			Log.message(i++ +". Click on product name",driver);
			
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage),
					"System should Navigate to Pdp page.",
					"System Navigated to Pdp page",
					"System not Navigated to Pdp page", driver);
			
			shopBag = pdpPage.headers.clickOnBagLink();
			Log.message(i++ + ". Navigated to Checkout page",driver);
			
			Log.softAssertThat(shopBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "productNameSection", "productUnitPrice", shopBag) &&
					shopBag.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productUnitPrice", "imgProductSection", shopBag), 
					"Gift Certificate Amount should be displayed to the right of the Gift Certificate Image and below the name of the item", 
					"Gift Certificate Amount is displayed to the right of the Gift Certificate Image and below the name of the item", 
					"Gift Certificate Amount is not displayed to the right of the Gift Certificate Image and below the name of the item", driver);
			
			Log.softAssertThat(shopBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "productUnitPrice", "txtPersonalizedMessage", shopBag),
					"Personalized message heading should be displayed below the Gift certificate amount.",
					"Personalized message heading is displayed below the Gift certificate amount",
					"Personalized message heading is not displayed below the Gift certificate amount", driver);
			
			Log.softAssertThat(shopBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtPersonalizedMessage", "divProductGiftcartFromValue", shopBag),
					"The 'From' value should be displayed below the Personalized message heading",
					"The 'From' value is displayed below the Personalized message heading",
					"The 'From' value is not displayed below the Personalized message heading", driver);
			
			Log.softAssertThat(shopBag.elementLayer.verifyTextContains("divProductGiftcartFromValue", "automation@yopmail.com", shopBag),
					"The value in the 'From' field should be the same value that the User entered at the gift certificate PDP",
					"The value in the 'From' field is the same value that the User entered at the gift certificate PDP",
					"The value in the 'From' field is not the same value that the User entered at the gift certificate PDP", driver);
			
			Log.softAssertThat(shopBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "divProductGiftcartFromValue", "txtGiftcartToValue", shopBag),
					"The 'To' value should be displayed below the 'From' value heading",
					"The 'To' value is displayed below the 'From' value heading",
					"The 'To' value is not displayed below the 'From' value heading", driver);
			
			Log.softAssertThat(shopBag.elementLayer.verifyTextContains("txtGiftcartToValue", "automation@yopmail.com", shopBag),
					"The value in the 'To' field should be the same value that the User entered at the gift certificate PDP",
					"The value in the 'To' field is the same value that the User entered at the gift certificate PDP",
					"The value in the 'To' field is not the same value that the User entered at the gift certificate PDP", driver);
			
			Log.softAssertThat(shopBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtGiftcartToValue", "txtProductGiftcartMessage", shopBag),
					"The Message body should be displayed below the 'To' field",
					"The Message body is displayed below the 'To' field",
					"The Message body is not displayed below the 'To' field", driver);
			
			Log.softAssertThat(shopBag.elementLayer.verifyTextContains("txtProductGiftcartMessage", "Test Personal Message", shopBag),
					"The value in the Message Body should be the same value that the User entered at the gift certificate PDP",
					"The value in the Message Body is the same value that the User entered at the gift certificate PDP",
					"The value in the Message Body is not the same value that the User entered at the gift certificate PDP", driver);
			
			//SM-3535 and SM-2641 - Felix asked the team to hide the quantity field for gift cards.
			/*if(Utils.isMobile()) {
				Log.softAssertThat(shopBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtPersonalizedMessage", "divItemQuantity", shopBag), 
						"Quantity dropdown should be displayed below the message body", 
						"Quantity dropdown is displayed below the message body", 
						"Quantity dropdown is not displayed below the message body", driver);
			} else {
				Log.softAssertThat(shopBag.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divItemQuantity", "txtPersonalizedMessage", shopBag) &&
						shopBag.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divItemQuantity", "productUnitPrice", shopBag), 
						"The Quantity dropdown should be displayed to the right hand side of the Gift certificate amount and personalized message", 
						"The Quantity dropdown is displayed to the right hand side of the Gift certificate amount and personalized message", 
						"The Quantity dropdown is not displayed to the right hand side of the Gift certificate amount and personalized message", driver);
			}*/
			Log.softAssertThat(shopBag.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("divDecreaseArrow", "divIncreaseArrow"), shopBag),
					"The Quantity dropdown should be hidden.",
					"The Quantity dropdown is hidden",
					"The Quantity dropdown is not hidden", driver);
			
			Log.softAssertThat(shopBag.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lnkAddToWishlist"), shopBag),
					"For Gift certificate 'Add to Wishlist' link should not display.",
					"For Gift certificate 'Add to Wishlist' link is not displaying",
					"For Gift certificate 'Add to Wishlist' link is displaying", driver);
			
			shopBag.clickOnEditLink(0);
			Log.message(i++ +". Clicked on edit link", driver);
			
			if(Utils.isDesktop()) {
				Log.softAssertThat(shopBag.elementLayer.VerifyPageElementDisplayed(Arrays.asList("popUpBonusProduct"), shopBag),
						"Edit overlay should display.",
						"Edit overlay is displaying",
						"Edit overlay is not displaying", driver);
				
				shopBag.closeEditOverlay();
				Log.message(i++ +". Edit overlay closed", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage),
						"Edit link should take the User to the PDP.",
						"Edit link is taking the User to the PDP",
						"Edit link is not taking the User to the PDP", driver);
				
				shopBag = pdpPage.headers.clickOnBagLink();
				Log.message(i++ + ". Navigated to Checkout page",driver);
			}
			
			shopBag.removeAllItemsFromCart();
			Log.message(i++ +". Gift Certificate is removed", driver);
			
			Log.softAssertThat(shopBag.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnRemove"), shopBag),
					"Clicking on the Remove link should remove the Gift Certificate from the Cart.",
					"Clicking on the Remove link is remove the Gift Certificate from the Cart",
					"Clicking on the Remove link is not remove the Gift Certificate from the Cart", driver);
			
			pdpPage = homePage.footers.navigateToEGiftCard();
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.selectGCSize();
			
			pdpPage.checkGCPersonalizedMessage("check");
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ +". Clicking add to bag without filling 'To', 'From', 'Message' fields",driver);
			
			Log.softAssertThat(shopBag.elementLayer.VerifyPageElementDisplayed(Arrays.asList("receipentMailError","receipentConfirmMailError",
					"fromMailError","msgAreaError","mainEGiftError"), shopBag),
					"Adding Gift certificate to cart is not allowed if no detail filled.",
					"Adding Gift certificate to cart is not allowing if no detail filled",
					"Adding Gift certificate to cart is allowing if no detail filled", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	
	}


}// search
