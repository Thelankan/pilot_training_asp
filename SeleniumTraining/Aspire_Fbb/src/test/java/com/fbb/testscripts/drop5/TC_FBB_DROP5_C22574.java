package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.WishlistLoginPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22574 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22574(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + searchKey, driver);
	
			String size=pdpPage.selectSizeSwatch(1);
			Log.message(i++ +". Selected size : " + size);
	
			String color = pdpPage.selectColorSwatch(0);
			Log.message(i++ +". Selected Color : " + color);
	
			WishlistLoginPage wishlistlogin=(WishlistLoginPage)pdpPage.addToWishlist();	
	
			//Step-1: Verify the functionality of Returning Customer Heading
			if(!Utils.isMobile()) {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "currentBreadCrumb", "accountHeading", wishlistlogin),					 
						"Returning Customer should be displayed below the breadcrumb.", 
						"Returning Customer is displayed below the breadcrumb.", 
						"Returning Customer is not displayed below the breadcrumb.", driver);
			}else {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "currentBreadCrumbMobile", "accountHeading", wishlistlogin),					 
						"Returning Customer should be displayed below the breadcrumb.", 
						"Returning Customer is displayed below the breadcrumb.", 
						"Returning Customer is not displayed below the breadcrumb.", driver);
			}
	
			//Step-2: Verify the functionality of Returning Customer Sub Heading	
			Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "accountHeading", "accountSubHeading", wishlistlogin),					 
					"Returning Customer Subheading should be displayed below the heading.", 
					"Returning Customer Subheading is displayed below the heading.", 
					"Returning Customer Subheading is not displayed below the heading.", driver);
	
			//Step-3: Verify the functionality of Email Address Field and Password
			Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "accountSubHeading", "fldEmail", wishlistlogin),					 
					"Email Address Field should be displayed below the Returning Customer Subheading.", 
					"Email Address Field is displayed below the Returning Customer Subheading.", 
					"Email Address Field is not displayed below the Returning Customer Subheading.", driver);
	
			Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldEmail", "fldPassword", wishlistlogin),					 
					"Email Address Field should be displayed above the Password Field.", 
					"Email Address Field is displayed above the Password Field.", 
					"Email Address Field is not displayed above the Password Field.", driver);
	
			Log.softAssertThat(wishlistlogin.verifyEmailandPasswordFieldsAreEditable(),
					"User should be able to enter text in the Email Address and Password",
					"User is able to enter text in the Email Address and Password",
					"User is not able to enter text in the Email Address and Password",driver);
	
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPlaceHolderMovesAbove("fldEmail", "txtemail", "email@gmail.com", wishlistlogin),					 
					"Place holder text 'Email Address' should move to the top portion of the text box when User starts typing in the Email Address field", 
					"Place holder text moved to the top", 
					"Place holder text did not move to the top", driver);
	
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPlaceHolderMovesAbove("fldPassword", "txtpassword", "password", wishlistlogin),					 
					"Place holder text 'Password' should move to the top portion of the text box when User starts typing in the Password field", 
					"Place holder text moved to the top", 
					"Place holder text did not move to the top", driver);
	
			//mandatory field span.error
			wishlistlogin.clearEmailAndPassword();
			wishlistlogin.clickSignInButton();
			Log.message(i++ +". Clicked SignIn button.");
			Log.softAssertThat(wishlistlogin.verifyEmailandPasswordareMandatory(),
					"Email and Password should be mandatory.",
					"Email and Password are mandatory.",
					"Email and Password are not mandatory.",driver);
	
			wishlistlogin.typeOnEmail("qwerty");
			wishlistlogin.typeOnPassword("qwerty");
			wishlistlogin.clickSignInButton();
	
			Log.softAssertThat(wishlistlogin.elementLayer.verifyTextContains("txtEmailError","valid", wishlistlogin),
					"On filling incorrect data format is entered in email address, appropriate error message should be displayed", 
					"Appropriate error message is displayed", 
					"Appropriate error message is not displayed", driver);
	
			wishlistlogin.typeOnEmail("automation@yopmail.com");
			Log.message(i++ + ". Filled Username");
			wishlistlogin.typeOnPassword("");
			Log.message(i++ + ". Filled Password");
			wishlistlogin.clickSignInButton();
	
			Log.softAssertThat(wishlistlogin.elementLayer.verifyTextContains("txtPasswordError","Password", wishlistlogin),
					"On filling incorrect data format is entered in password, appropriate error message should be displayed", 
					"Appropriate error message is displayed", 
					"Appropriate error message is not displayed", driver);
	
			wishlistlogin.typeOnEmail("unregistered@yopmail.com");
			Log.message(i++ +". Filled Username");
			wishlistlogin.typeOnPassword(accountData.get("password_global"));
			Log.message(i++ +". Filled Password");
			Log.softAssertThat(wishlistlogin.verifyPasswordisMaskedorUnmasked("mask"),			 
					"By default the User entered password should be masked", 
					"The password is masked", 
					"The password is not masked", driver);
	
			wishlistlogin.typeOnPassword(accountData.get("password_global"));
			wishlistlogin.clickSignInButton();
			Log.message(i++ +". Clicked SignIn button.");
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("txtSignInError"), wishlistlogin),					 
					"If user try to use a non register email address, system will display error message", 
					"Error message is displayed for a non registered user", 
					"Error message is not displayed for a non registered user", driver);
	
			//blank
			wishlistlogin.typeOnEmail("automation@yopmail.com");
			wishlistlogin.typeOnPassword("");
			wishlistlogin.clickSignInButton();				
			Log.softAssertThat(wishlistlogin.elementLayer.verifyTextContains("txtPasswordError","Password", wishlistlogin),
					"On leaving password field blank, appropriate error message should be displayed", 
					"Appropriate error message is displayed", 
					"Appropriate error message is not displayed", driver);
	
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}//M1_FBB_DROP5_C22574
	
	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP5_C22574(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + searchKey, driver);
	
			String size=pdpPage.selectSizeSwatch(1);
			Log.message(i++ +". Selected size : " + size);
	
			String color = pdpPage.selectColorSwatch(0);
			Log.message(i++ +". Selected Color : " + color);
	
			WishlistLoginPage wishlistlogin=(WishlistLoginPage)pdpPage.addToWishlist();	
	
			//Step-5: Verify the functionality of Show/Hide password Button
			//default
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("showpassword"), wishlistlogin)&&
					wishlistlogin.elementLayer.verifyTextContains("showpassword","SHOW", wishlistlogin),					 
					"Show Password button should be visible", 
					"Show Password button is visible", 
					"Show Password button is not visible", driver);
	
			//on click show				
			wishlistlogin.clickShowPassword();
			Log.message(i++ +". Clicked Show Password");
			Log.softAssertThat(wishlistlogin.verifyPasswordisMaskedorUnmasked("unmask"),					 
					"On clicking 'Show' the link text 'Show' should be replaced with the link text 'Hide", 
					"The password is unmasked", 
					"The password is not unmasked", driver);
	
			Log.softAssertThat(wishlistlogin.elementLayer.verifyTextContains("showpassword","HIDE", wishlistlogin),
					"The link text 'Show' should be replaced with the link text 'Hide'", 
					"The link text 'Show' is replaced with the link text 'Hide'", 
					"The link text 'Show' is not replaced with the link text 'Hide'", driver);
	
			//on click hide
			wishlistlogin.clickShowPassword(); Log.message(i++ +". Clicked Hide Password");
			Log.softAssertThat(wishlistlogin.verifyPasswordisMaskedorUnmasked("mask"),					 
					"On clicking the Hide link, System should mask the password and hide the User entered data", 
					"The password is masked", 
					"The password is not masked", driver);
	
			Log.softAssertThat(wishlistlogin.elementLayer.verifyTextContains("showpassword","SHOW", wishlistlogin),
					"The link text 'Hide' should be replaced with the link text 'Show'", 
					"The link text 'Hide' is replaced with the link text 'Show'", 
					"The link text 'Hide' is not replaced with the link text 'Show'", driver);
	
			//Step-6: Verify the functionality of Remember Me check box
			Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldPassword", "cbRememberme", wishlistlogin), 
					"Remember Me checkbox should be displayed below the Password Field.", 
					"Remember Me checkbox is displayed below the Password Field.", 
					"Remember Me checkbox is not displayed below the Password Field.", driver);
	
			Log.softAssertThat(!wishlistlogin.elementLayer.verifyPageElementsChecked(Arrays.asList("cbRememberme"), wishlistlogin),
					"By default, the 'Remember Me' checkbox should be unchecked", 
					"The 'Remember Me' checkbox is unchecked", 
					"The 'Remember Me' checkbox is not unchecked", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}//M2_FBB_DROP5_C22574
	
	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP5_C22574(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + searchKey, driver);
	
			String size=pdpPage.selectSizeSwatch(1);
			Log.message(i++ +". Selected size : " + size);
	
			String color = pdpPage.selectColorSwatch(0);
			Log.message(i++ +". Selected Color : " + color);
	
			WishlistLoginPage wishlistlogin=(WishlistLoginPage)pdpPage.addToWishlist();	
	
			//Step-7: Verify the functionality of Sign In Button
			if(Utils.isMobile()) {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnSignIn", "cbRememberme", wishlistlogin), 
						"Sign In Button should be displayed right to the Remember Me check box.", 
						"Sign In Button is displayed right to the Remember Me check box.", 
						"Sign In Button is not displayed right to the Remember Me check box.", driver);
			} else {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnSignIn", "lnkForgetPasswordMobile", wishlistlogin), 
						"Sign In Button should be displayed right to the Forget password link.", 
						"Sign In Button is displayed right to the Forget password link.", 
						"Sign In Button is not displayed right to the Forget password link.", driver);
			}
			
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("lblLoginBrands"), wishlistlogin),					 
					"Login brands should be displayed", 
					"Login brands is displayed", 
					"Login brands is not displayed", driver);
	
			if(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("btnFacebook"), wishlistlogin)) {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnSignIn", "btnFacebook", wishlistlogin),					 
						"Facebook button should be displayed below the Sign in button.", 
						"Facebook button is displaying below the Sign in button.", 
						"Facebook button is not displaying below the Sign in button.", driver);
				
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnFacebook", "lblLoginBrands", wishlistlogin),					 
						"Facebook link should be displayed above the Login brands.", 
						"Facebook link is displaying above the Login brands.", 
						"Facebook link is not displaying above the Login brands.", driver);
			}
	
			//Invalid Email Address and Invalid Password
			wishlistlogin.typeOnEmail("asdf@yop");
			Log.message(i++ +". Filled Username");
			wishlistlogin.typeOnPassword("asdfasdf");
			Log.message(i++ +". Filled Password");
			wishlistlogin.clickSignInButton();
			Log.message(i++ +". Clicked SignIn button.");
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("txtEmailError"), wishlistlogin),					 
					"User should not be able to sign in for Invalid Email Address and Invalid Password", 
					"User is not able to sign in for Invalid Email Address and Invalid Password", 
					"User is able to sign in for Invalid Email Address and Invalid Password", driver);
	
			//Invalid Email Address and Valid Password
			wishlistlogin.typeOnEmail("asdf@yop");
			Log.message(i++ +". Filled Username");
			wishlistlogin.typeOnPassword(accountData.get("password_global"));
			Log.message(i++ +". Filled Password");
			wishlistlogin.clickSignInButton();
			Log.message(i++ +". Clicked SignIn button.");
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("txtEmailError"), wishlistlogin),					 
					"User should not be able to sign in for Invalid Email Address and Valid Password", 
					"User is not able to sign in for Invalid Email Address and Valid Password", 
					"User is able to sign in for Invalid Email Address and Valid Password", driver);
	
			//Invalid Email Address and Password Blank
			wishlistlogin.typeOnEmail("asdf!@#");
			Log.message(i++ +". Filled Username");
			wishlistlogin.typeOnPassword("");
			Log.message(i++ +". Filled Password");
			wishlistlogin.clickSignInButton();
			Log.message(i++ +". Clicked SignIn button.");
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("txtEmailError", "txtPasswordError"), wishlistlogin),					 
					"User should not be able to sign in for Invalid Email Address and Password Blank", 
					"User is not able to sign in for Invalid Email Address and Password Blank", 
					"User is able to sign in for Invalid Email Address and Password Blank", driver);
	
			//Valid Email Address and Invalid Password
			wishlistlogin.typeOnEmail("automation@yopmail.com");
			Log.message(i++ +". Filled Username");
			wishlistlogin.typeOnPassword("asdfa"); //password[1]=pssword1
			Log.message(i++ +". Filled Password");
			wishlistlogin.clickSignInButton();
			Log.message(i++ +". Clicked SignIn button.");
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("txtSignInError"), wishlistlogin),					 
					"User should not be able to sign in for Valid Email Address and Invalid Password", 
					"User is not able to sign in for Valid Email Address and Invalid Password", 
					"User is able to sign in for Valid Email Address and Invalid Password", driver);
	
			//Valid Email Address and Password Blank
			wishlistlogin.typeOnEmail("automation@yopmail.com");
			Log.message(i++ +". Filled Username");
			wishlistlogin.typeOnPassword("");
			Log.message(i++ +". Filled Password");
			wishlistlogin.clickSignInButton();
			Log.message(i++ +". Clicked SignIn button.");
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("txtPasswordError"), wishlistlogin),					 
					"User should not be able to sign in for Valid Email Address and Password Blank", 
					"User is not able to sign in for Valid Email Address and Password Blank", 
					"User is able to sign in for Valid Email Address and Password Blank", driver);
	
			//Email Address Blank and Invalid Password
			wishlistlogin.typeOnEmail("");
			Log.message(i++ +". Filled Username");
			wishlistlogin.typeOnPassword("asdf");
			Log.message(i++ +". Filled Password");
			wishlistlogin.clickSignInButton();
			Log.message(i++ +". Clicked SignIn button.");
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("txtSignInError"), wishlistlogin),					 
					"User should not be able to sign in for Email Address Blank and Invalid Password", 
					"User is not able to sign in for Email Address Blank and Invalid Password", 
					"User is able to sign in for Email Address Blank and Invalid Password", driver);
	
			//Email Address Blank and Valid Password
			wishlistlogin.typeOnEmail("");
			Log.message(i++ +". Filled Username");
			wishlistlogin.typeOnPassword(accountData.get("password_global"));
			Log.message(i++ +". Filled Password");
			wishlistlogin.clickSignInButton();
			Log.message(i++ +". Clicked SignIn button.");
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("txtEmailError"), wishlistlogin),					 
					"User should not be able to sign in for Email Address Blank and Valid Password", 
					"User is not able to sign in for Email Address Blank and Valid Password", 
					"User is able to sign in for Email Address Blank and Valid Password", driver);
	
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}//M3_FBB_DROP5_C22574
	
	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP5_C22574(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + searchKey, driver);
	
			String size=pdpPage.selectSizeSwatch(1);
			Log.message(i++ +". Selected size : " + size);
	
			String color = pdpPage.selectColorSwatch(0);
			Log.message(i++ +". Selected Color : " + color);
	
			WishlistLoginPage wishlistlogin=(WishlistLoginPage)pdpPage.addToWishlist();
			boolean btnFacebookEnabled = wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("btnFacebook"), wishlistlogin);
	
			//Step-8: Verify the functionality of Forgot Password Button
			if(Utils.isMobile()) {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnSignIn", "lnkForgetPasswordMobile", wishlistlogin)
						|| wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnSignIn", "lnkForgetPasswordMobile", wishlistlogin), 
						"Forget your password link should be displayed below the Remember Me check box or Left side of the Sign In button.", 
						"Forget your password link should be displayed below the Remember Me check box or Left side of the Sign In button.", 
						"Forget your password link should be displayed below the Remember Me check box or Left side of the Sign In button.", driver);
			} else {
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnSignIn", "lnkForgetPasswordMobile", wishlistlogin), 
						"Forgot Your Password should be displayed below the Sign In Button.", 
						"Forgot Your Password is displayed below the Sign In Button.", 
						"Forgot Your Password is not displayed below the Sign In Button.", driver);
			}
	
			wishlistlogin.clickOnForgetPassword();
			Utils.waitForPageLoad(driver);
	
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("emailTbRequestPassword"), wishlistlogin),					 
					"On clicking/tapping the 'Forgot your password?' link, the User should be navigated to the \"Password Recovery landing page\"", 
					"User navigated to the \"Password Recovery landing page\"", 
					"User could not navigated to the \"Password Recovery landing page\"", driver);
	
			wishlistlogin.typeEmailaddrsinRequestPassword("automation@yopmail.com");
			wishlistlogin.clickSendRequestPassword();
			Log.softAssertThat(wishlistlogin.elementLayer.verifyTextContains("headingRequestPassword","DONE!", wishlistlogin),
					"To check 'Password Request' has sent or not", 
					"The 'Password Request' has sent", 
					"The 'Password Request' has not sent", driver);	
	
			if(btnFacebookEnabled){
				//Step-9: Verify the functionality of Facebook Login
				wishlistlogin.clickcloseForgotPasswordDialog();
				if(!Utils.isMobile()) {
					Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkForgotPwd", "btnFacebook", wishlistlogin), 
							"Sign In With Facebook button should be displayed below the Forgot your Password link.", 
							"Sign In With Facebook button is displayed below the Forgot your Password link.", 
							"Sign In With Facebook button is not displayed below the Forgot your Password link.", driver);
				}
				else {
					Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnSignIn", "btnFacebook", wishlistlogin), 
							"Sign In With Facebook button should be displayed below the Sign In Button.", 
							"Sign In With Facebook button is displayed below the Sign In Button.", 
							"Sign In With Facebook button is not displayed below the Sign In Button.", driver);
				}
				
				String url = wishlistlogin.getFaceBookNavigationLink();
				Log.softAssertThat(url.toLowerCase().contains("facebook"),
						"To check Facebook page is visited or not", 
						"On clicking Facebook navigation link, the facebook page will open", 
						"On clicking Facebook navigation link, the facebook page will not open", driver);
				
				//Step-10: paertial
				Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnFacebook", "loginMessage", wishlistlogin), 
						"Login Messaging should be displayed below the Sign In With Facebook button.", 
						"Login Messaging is displayed below the Sign In With Facebook button.", 
						"Login Messaging is not displayed below the Sign In With Facebook button.", driver);
			}else {
				Log.reference("Facebook Login disabled in build. Relevent verifications skipped.");
			}
	
			//Step-10: Verify the functionality of Login Messaging
			Log.softAssertThat(wishlistlogin.elementLayer.verifyPageElements(Arrays.asList("loginMessage"), wishlistlogin),					 
					"Check login message is displaying or not", 
					"Login message is displaying", 
					"Login message is not displaying", driver);
			
			Log.softAssertThat(wishlistlogin.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnSignIn", "loginMessage", wishlistlogin), 
					"Login Messaging should be displayed below the Sign In button", 
					"Login Messaging is displayed below the Sign In button", 
					"Login Messaging is not displayed below the Sign In button", driver);
	
			String message = wishlistlogin.getloginMessage();
			Log.softAssertThat(wishlistlogin.verifyLoginMessageComingFromProperty(message),					 
					"Check login message is displaying from property or not", 
					"Login message is displaying from property", 
					"Login message is not displaying from property", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}//M4_FBB_DROP5_C22574

}// search
