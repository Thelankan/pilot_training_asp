package com.fbb.testscripts.drop5;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.EmailPreferencePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22698 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22698(String browser) throws Exception {
		Log.testCaseInfo();
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		//Prerequisite: A registered user account
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAcc  = homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Logged in As authenticated user.", driver);
	
			EmailPreferencePage emailPreference = myAcc.clickOnEmailPrefLink();
			Log.message(i++ + ". Navigated to Email Preference Page!", driver);
	
			//1 - Verify the functionality of Brand Catalog Image and Other Brand Image
			if (!Utils.isMobile()) {
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgBrandDesktop", "sisterBrands", emailPreference), 
						"Other Brand Copy should be displayed below the Subscribed Brand Details", 
						"Other Brand Copy are displayed below the Subscribed Brand Details", 
						"Other Brand Copy are not displayed below the Subscribed Brand Details", driver);
			} else {
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgBrandMobile", "sisterBrands", emailPreference), 
						"Other Brand Copy should be displayed below the Subscribed Brand Details", 
						"Other Brand Copy are displayed below the Subscribed Brand Details", 
						"Other Brand Copy are not displayed below the Subscribed Brand Details", driver);
			}
	
			Log.message("Brand Image relative location verification is covered in Step 3.");
	
			//2 - Verify the functionality of Brand Logo and Other Brand Logo
			Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionBrandDivider", "sisterBrands", emailPreference),
					"Sister Brands should be displayed below the Brand divider",
					"Sister Brands is displaying below the Brand divider",
					"Sister Brands is not displaying below the Brand divider", driver);
	
			if(Utils.isMobile()) {
				Log.softAssertThat(emailPreference.elementLayer.verifyListElementSize("imgSisterBrandMobile", 7,  emailPreference),
						"All Sister Brands should be displayed",
						"All Sister Brands are displayed",
						"Some Sister Brands are not displayed", driver);
			} else {
				Log.softAssertThat(emailPreference.elementLayer.verifyListElementSize("imgSisterBrand", 7,  emailPreference),
						"All Sister Brands should be displayed",
						"All Sister Brands are displayed",
						"Some Sister Brands are not displayed", driver);
			}
	
			if (!Utils.isMobile()) {
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblEmailInfo", "imgBrandDesktop", emailPreference), 
						"Brand Catalog image should be displayed below the email info", 
						"Brand Catalog image are displayed below the email info", 
						"Brand Catalog image are not displayed below the email info", driver);
	
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgSisterBrandSingle", "lblSisterBrandDesc", emailPreference), 
						"Brand image should be displayed above the description", 
						"Brand image are displayed below the description", 
						"Brand image are not displayed below the description", driver);
			} else {
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblEmailInfo", "imgBrandMobile", emailPreference) &&
						emailPreference.elementLayer.verifyHorizontalAllignmentOfElements(driver, "imgBrandMobile", "imgBrandNameMobile", emailPreference), 
						"Brand Catalog Image should be displayed at the right side below the Brand Logo",
						"Brand Catalog Image is displayed at the right side below the Brand Logo",
						"Brand Catalog Image is not displayed at the right side below the Brand Logo", driver);
	
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgSisterBrandSingleMobile", "lblSisterBrandDesc", emailPreference), 
						"Brand image should be displayed above the description", 
						"Brand image are displayed below the description", 
						"Brand image are not displayed below the description", driver);
			}
	
			//3 - Verify the functionality of Brand Logo and Other Brand Logo
			Log.softAssertThat(emailPreference.verifyBrandImageLogoRelativePosition(), 
					"Brand Image should be displayed left (and below on mobile) of Brand Logo", 
					"Brand Image is displayed left (and below on mobile) of Brand Logo", 
					"Brand Image is not displayed left (and below on mobile) of Brand Logo", driver);
			
			//4 - Verify the functionality of Other Brand Description
			if (Utils.isMobile()) {
				Log.softAssertThat(emailPreference.verifyBrandDescriptionLogoRelativePosition(),
						"Other Brand Description should be displayed on correct side of the Other Brand Logo",
						"Other Brand Description is displayed on correct side of Other Brand Logo",
						"Other Brand Description is not displayed on correct side of Other Brand Logo", driver);
			} else {
				Log.softAssertThat(emailPreference.verifyBrandDescriptionLogoRelativePosition(),
						"Other Brand Description should be displayed below the Other Brand Logo",
						"Other Brand Description is displayed below the Other Brand Logo",
						"Other Brand Description is not displayed below the Other Brand Logo", driver);
			}
	
			//5 - Verify the functionality of Subscription State
			if(!Utils.isMobile()) {
				Log.softAssertThat(emailPreference.verifyDescriptionSliderRelativePosition(), 
						"Subscription-State should be displayed right of Other Brand Description", 
						"Subscription-State is displayed right of Other Brand Description", 
						"Subscription-State is not displayed right of Other Brand Description", driver);
			} else {
				Log.softAssertThat(emailPreference.verifyDescriptionSliderRelativePosition(), 
						"Subscription-State should be displayed below the Other Brand Image", 
						"Subscription-State is displayed below the Other Brand Image", 
						"Subscription-State is not displayed below the Other Brand Image", driver);
			}
	
			emailPreference.checkUncheckSubscribe(1, false);
			Log.message(i++ + ". Unsubscribed preferences!", driver);
	
			Log.softAssertThat(!emailPreference.verifySubscribedStatus(1),
					"The Brands subscription should be disabled",
					"The Brands subscription is disabled",
					"The Brands subscription is not disabled", driver);
	
			emailPreference.checkUncheckSubscribe(1, true);
			Log.message(i++ + ". Subscribed preferences!", driver);
	
			Log.softAssertThat(emailPreference.verifySubscribedStatus(1),
					"The Brands subscription should be enabled",
					"The Brands subscription is enabled",
					"The Brands subscription is not enabled", driver);
	
			Log.softAssertThat(emailPreference.verifySubscribeLabelOfTheBrands(1, "SUBSCRIBED"),
					"Check the Brands subscription text is displaying correctly when subscribed",
					"The Brands subscription text is displaying correctly when subscribed",
					"The Brands subscription text is not displaying correctly when subscribed", driver);
	
			emailPreference.checkUncheckSubscribe(1, false);
			Log.message(i++ + ". Unsubscribed preferences!", driver);
	
			Log.softAssertThat(!emailPreference.verifySubscribedStatus(1),
					"The Brands subscription should be disabled after unsubscribing a sister brand",
					"The Brands subscription is disabled after unsubscribing a sister brand",
					"The Brands subscription is not disabled after unsubscribing a sister brand", driver);
	
			Log.softAssertThat(emailPreference.verifySubscribeLabelOfTheBrands(1, "SUBSCRIBE"),
					"Check the Brands subscription text is displaying correctly when unsubscribed",
					"The Brands subscription text is displaying correctly when unsubscribed",
					"The Brands subscription text is not displaying correctly when unsubscribed", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP5_C22698

}// search
