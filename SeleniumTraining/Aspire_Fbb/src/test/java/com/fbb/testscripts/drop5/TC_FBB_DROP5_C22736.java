package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlatinumCardLandingPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PreApprovedPlatinumCardPage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22736 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22736(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		// Test data
		String credentials = accountData.get("plcc_preapproved_" + Utils.getCurrentBrandShort().toString());
		String email = credentials.split("\\|")[0];
		String password = credentials.split("\\|")[1];
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			MyAccountPage myAcc = headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged into pre-approved account.", driver);
			
			Footers footers = myAcc.footers;
			PlatinumCardLandingPage platinumLanding = footers.navigateToPlatinumCreditCardLanding();
			Log.message(i++ + ". Navigate to PLCC landing page.", driver);
			
			//Step-1: Verify the display of components available in Authenticated, Pre-Approved Users Landing Page
			Log.softAssertThat(platinumLanding.elementLayer.VerifyElementDisplayed(Arrays.asList("divPreapprovedMessage", "applyButton"), platinumLanding), 
					"Authenticated, Pre-Approved Users Landing Page should be displayed with Content Copy and Accept This Offer button.", 
					"Page is displayed with Content Copy and Accept This Offer button.", 
					"Page is not displayed with Content Copy and Accept This Offer button.", driver);
			
			//Step-2: Verify the display and functionality of Content Copy
			Log.softAssertThat(platinumLanding.elementLayer.verifyElementWithinElement(driver, "divPreapprovedMessage", "divBannerElementAlt", platinumLanding), 
					"Content copy message should be displayed in the center of the Banner available in the Top Content Slot", 
					"Content copy message is displayed in the center of the Banner available in the Top Content Slot", 
					"Content copy message is not displayed in the center of the Banner available in the Top Content Slot", driver);
			
			String msgPreapproved = platinumLanding.elementLayer.getAttributeForElement("divPreapprovedMessage", "innerHTML", platinumLanding);
			String userName = headers.getUserName();
			
			Log.softAssertThat(msgPreapproved.contains(userName), 
					"System should display the user's first name in the Content Copy", 
					"System displays the user's first name in the Content Copy", 
					"System does not display the user's first name in the Content Copy", driver);
			
			Log.softAssertThat(msgPreapproved.startsWith(userName), 
					"System should display the user's first name in the Content Copy", 
					"System displays the user's first name in the Content Copy", 
					"System does not display the user's first name in the Content Copy", driver);
			
			String userLimit = ""+Utils.getNumberInString(msgPreapproved);
			
			Log.softAssertThat(msgPreapproved.indexOf('$') == msgPreapproved.indexOf(userLimit)-1, 
					"System should display the credit limit beginning with $", 
					"System displays the credit limit beginning with $", 
					"System does not display the credit limit beginning with $", driver);
			
			//Step-3: Verify the display and functionality of Complete Your Application (Accept This Offer button)
			Log.softAssertThat(platinumLanding.elementLayer.verifyVerticalAllignmentOfElements(driver, "divPreapprovedMessage", "applyButton", platinumLanding), 
					"Complete Your Application (Accept This Offer button) should be displayed below the Content copy message element.", 
					"Accept This Offer button is be displayed below the Content copy message element.", 
					"Accept This Offer button is not displayed below the Content copy message element.", driver);
			
			PreApprovedPlatinumCardPage preapprovedApply = platinumLanding.clickAcceptThisOffer();
			Log.message(i++ + ". Clicked on Accpet offer.", driver);
			
			Log.softAssertThat(preapprovedApply.elementLayer.VerifyElementDisplayed(Arrays.asList("divProfileSummery"), preapprovedApply), 
					"On click, system should be navigated to the Application page for Pre-Approved Users", 
					"System navigated to the Application page for Pre-Approved Users", 
					"System did not navigate to the Application page for Pre-Approved Users", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M1_FBB_DROP5_C22736

}
