package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.PdpPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C21659 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C21659(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prodId = prdData.get("prd_enlarge");
		String dateFormat = "MMM dd, yyyy";
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
	
			Object[] obj = GlobalNavigation.addProduct_PlaceOrder_G(driver, prodId, i, AccountUtils.generateEmail(driver));
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (Integer) obj[1];
			String address = (String) obj[2];
	
			//Step 1
			if(Utils.isDesktop()) {	
				Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkPrintPageOrderCnf"), checkoutPage),
						"To check fields of the 'Order Conformation' page.",
						"The 'Order Conformation' page should have 'Print' link!",
						"Some fields are not present in the 'Order Conformation' page", driver);
	
				//Step 2
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkPrintPageOrderCnf", "lblOrderNumberMsg", checkoutPage),
						"To check Order Number is displaying left side of the 'Print' link.",
						"The Order Number is displaying left side of the 'Print' link!",
						"The Order Number is not displaying left side of the 'Print' link", driver);
	
				//Step 3
				Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("sectionRecommendationInConfPage"), checkoutPage),
						"To check recommendation section is displayed in the 'Order Conformation' page.",
						"The recommendation section is displayed in the 'Order Conformation' page!",
						"The recommendation section is not displayed in the 'Order Conformation' page", driver);
	
				//Step 6
				Log.softAssertThat(checkoutPage.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("lstRecommendationProdInConfPage"), checkoutPage),
						"To check recommendation section is displayed with products.",
						"The recommendation section is displayed with products!",
						"The recommendation section is not displayed with products", driver);
			}
	
			if (!Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblOrderDateInConfPage","lblOrderPriceInConfPage",
										"lblPaymentMethodInConfPage","lblCardNumberInConfPage","lblCardExpiryInConfPage"), checkoutPage),
						"To check success message is displaying in the 'Order Conformation' page.",
						"The success message is displaying in the 'Order Conformation' page!",
						"The success message is not displaying in the 'Order Conformation' page", driver);
			}
	
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblOrderStatusSectionInConfPage", "lnkViewDetailsInOrdConf_Mobile", checkoutPage),
						"To check order details section is displayed above the 'View Details' link.",
						"The order details section is displayed above the 'View Details' link!",
						"The order details section is not displayed above the 'View Details' link", driver);
	
				//Step 7
				checkoutPage.clickOnViewDetailsLink();
				Log.message(i++ +"Clicked on View Details link");
	
				Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblPaymentMethodInConfPage", 
						"lblBillingAddInConfPage"), checkoutPage),
						"To check order details section is displayed with billing and payment details.",
						"The order details section is displayed with billing and payment details!",
						"The order details section is not displayed with billing and payment details", driver);
	
				//Step 8
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextContains("lnkViewLessInOrdConf_Mobile", "View Less", checkoutPage),
						"To check view less link is displayed if clicked on view details link.",
						"The view less link is displayed if clicked on view details link!",
						"The view less link is not displayed if clicked on view details link", driver);
			}
			//Step 5
			Log.softAssertThat(checkoutPage.verifyOrderDateFormat(dateFormat), 
					"Order placement date should be displayed with first three letters of month followed by day and year in YYYY format", 
					"Order date is displayed correctly", 
					"Order date is not displayed correctly", driver);
			
			if(Utils.isDesktop() || Utils.isTablet()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblOrderNumberMsg", "lblPaymentMethodInConfPage", checkoutPage), 
						"Order Shipment & Payment Details should be displayed below the Reciept Statement", 
						"Order Shipment & Payment Details are displayed below the Reciept Statement", 
						"Order Shipment & Payment Details are not displayed below the Reciept Statement", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblOrderDateInConfPage", 
									"lblOrderPriceInConfPage", "lblPaymentMethodInConfPage"), checkoutPage), 
						"Order placement date, total amount of the order, payment methods should be displayed.", 
						"Order placement date, total amount of the order, payment methods are displayed.", 
						"Order placement date, total amount of the order, payment methods are not displayed.", driver);
				
				Log.softAssertThat(checkoutPage.verifyAddressCorrectlyDisplayedInOrdConfPage(address), 
						"Billing Address should be displayed in Top down fashion", 
						"Billing Address is displayed in Top down fashion", 
						"Billing Address is not displayed in Top down fashion", driver);
			}
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblOrderDateInConfPage", "lblOrderPriceInConfPage"), checkoutPage), 
			"Order placement date, total amount of the order, payment methods should be displayed.", 
			"Order placement date, total amount of the order, payment methods are displayed.", 
			"Order placement date, total amount of the order, payment methods are not displayed.", driver);
			}
			
	
			//Step 6
			Log.softAssertThat(checkoutPage.verifyAddressCorrectlyDisplayedInOrdConfPage(address),
					"To check the address in order confirmation is displaying in correct orderaddress.",
					"The address in order confirmation is displaying in correct orderaddress!",
					"The address in order confirmation is not displaying in correct orderaddress", driver);
	
			//Step 1
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtOrderConfirmationMsg"), checkoutPage),
					"To check success message is displaying in the 'Order Conformation' page.",
					"The success message is displaying in the 'Order Conformation' page!",
					"The success message is not displaying in the 'Order Conformation' page", driver);
	
			//Step 2
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblOrderNumberMsg"), checkoutPage),
					"To check Order Number is displaying in the 'Order Conformation' page.",
					"The Order Number is displaying in the 'Order Conformation' page!",
					"The Order Number is not displaying in the 'Order Conformation' page", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtOrderConfirmationMsg", "lblOrderNumberMsg", checkoutPage),
					"To check Order Number is displaying below the 'Order Confirmation message' text.",
					"The Order Number is displaying below the 'Order Confirmation message' text!",
					"The Order Number is not displaying below the 'Order Confirmation message' text", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblOrderNumberMsg", "lblReceiptMsgOrderCnf", checkoutPage),
					"To check Order Number is displaying above the 'Receipt message' text.",
					"The Order Number is displaying above the 'Receipt message' text!",
					"The Order Number is not displaying above the 'Receipt message' text", driver);
	
			//Step 4
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextContains("lblReceiptMsgOrderCnf", "We've emailed your receipt to", checkoutPage) &&
					checkoutPage.elementLayer.verifyElementTextContains("lblReceiptMsgOrderCnf", "@", checkoutPage) &&
					checkoutPage.elementLayer.verifyElementTextContains("lblReceiptMsgOrderCnf", ".com", checkoutPage),
					"To check 'Receipt message' contains 'We've emailed your receipt to' text with email ID.",
					"The 'Receipt message' contains 'We've emailed your receipt to' text with email ID!",
					"The 'Receipt message' not contains 'We've emailed your receipt to' text with email ID", driver);
	
			//Step 8
			if(Utils.isMobile()) {
				checkoutPage.clickOnViewLessLink();
				Log.message(i++ +"Clicked on View Less link");
	
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextContains("lnkViewDetailsInOrdConf_Mobile", "View Details", checkoutPage),
						"To check view details link is displayed if clicked on view less link.",
						"The view details link is displayed if clicked on view less link!",
						"The view details link is not displayed if clicked on view less link", driver);
	
				Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("lnkOrderCnfPgOrderBillingAdd","lnkOrderCnfPgPayment"), checkoutPage),
						"To check the order details are correctly displaying or not.",
						"The order details such as 'Billing Address', 'Payment Method' are not displayed",
						"The order details such as 'Billing Address', 'Payment Method' are displaying", driver);
	
				Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkViewDetailsInOrdConf_Mobile"), checkoutPage),
						"To check the order details are correctly displaying or not.",
						"The 'View Details' link is displaying",
						"The 'View Details' link is not displaying", driver);
			}
	
			if(Utils.isDesktop()) {
				PdpPage pdpPage = checkoutPage.clickOnRecommendationProductBasedOnIndex(0);
				Log.message(i++ + ". Navigated to Product Details page: "+pdpPage.getProductName());
	
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage),
						"To check the product detail page is displaying when click on any product in the recommendation section.",
						"The product detail page is displaying when click on any product in the recommendation section",
						"The product detail page is not displaying when click on any product in the recommendation section", driver);
			}
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_FBB_DROP4_C21659
}// search
