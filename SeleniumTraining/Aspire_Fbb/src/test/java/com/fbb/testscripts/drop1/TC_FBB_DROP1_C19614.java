package com.fbb.testscripts.drop1;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.customerservice.ContactUsPage;
import com.fbb.pages.customerservice.TrackOrderPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP1_C19614 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP1_C19614(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");

		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
		
		if(Utils.getRunBrowser(driver).equals("ie")){
			throw new SkipException("Due Mouse Hover feature limitation this TC cannot be run in Safari/IE");
		}

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to My Account Page with Credentials("+username+"/"+password+")");
			
			//1
			Headers headers = myAcc.headers;
			headers.mouseOverCustomerService();
			Log.message(i++ + ". Hovered/Tapped on Customer Service button", driver);

			Log.softAssertThat(headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("CustomerSerivceFlyOut"), headers), 
					"Customer Service flyout should display", 
					"Customer Service flyout is displaying", 
					"Customer Service flyout is not displaying", driver);

			//2
			Log.softAssertThat(headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkEmailUsDesktop","imgEmailUsDesktop"), headers), 
					"EMAIL US text & EMAIL US icon should be displayed", 
					"EMAIL US text & EMAIL US icon displayed", 
					"EMAIL US text & EMAIL US icon not displayed", driver);

			ContactUsPage contactUsPage = headers.navigateToContactUs();
			Log.message(i++ + ". Clicked on Email Us Icon.", driver);

			Log.softAssertThat(contactUsPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("readyElement"), contactUsPage), 
					"The Contact Us page should be displayed!",
					"The Contact Us page is displayed!", 
					"The Contact Us page is not displayed.", driver);

			//3
			BrowserActions.scrollToTopOfPage(driver);
			contactUsPage.headers.mouseOverCustomerService();
			Log.message(i++ + ". Customer Service Flyout menu opened!", driver);

			if(headers.getLiveChatStatus()){
				Log.softAssertThat(contactUsPage.headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "imgLiveChatDesktop", "imgEmailUsDesktop", contactUsPage.headers), 
						"LIVE CHAT text and icon should be shown next to the 'Email Us' icon", 
						"LIVE CHAT text and icon shown next to the 'Email Us' icon", 
						"LIVE CHAT text and icon not shown next to the 'Email Us' icon", driver);

				contactUsPage.headers.navigateToLiveChat();
				Log.message(i++ + ". Clicked on Live Chat Icon!", driver);

				Log.softAssertThat(contactUsPage.headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("mdlLiveChat"), contactUsPage.headers), 
						"Chat modal window should opens as a pop up modal", 
						"Chat modal window should opened as pop up modal!",
						"Chat modal window should did not open as expected.", driver);
				
				headers.closeLiveChat();
			}else{
				Log.reference("--->>> Live chat is not available at this time. Skipping related validations.");
			}

			//4
			contactUsPage.headers.navigateToHome();
			contactUsPage.headers.mouseOverCustomerService();
			Log.message(i++ + ". Customer Service Flyout menu opened!", driver);

			List<String> elementsToBeVerified = Arrays.asList("lnkTrackOrderDesktop", "lnkReturnItemsDesktop", "lnkUpdateMyInfoDesktop");
			Log.softAssertThat(contactUsPage.headers.elementLayer.VerifyPageElementDisplayed(elementsToBeVerified, contactUsPage.headers), 
					"The links for Track My Order, Return Items, Update My Information should be displayed below the Quick Links section.",
					"The links are displayed below the Quick Links section!", 
					"The links are not displayed below the Quick Links section.", driver);

			//5
			TrackOrderPage ordersPage = contactUsPage.headers.navigateToTrackMyOrder();
			Log.message(i++ + ". Clicked On Track My Order Link!", driver);

			Log.softAssertThat(ordersPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), ordersPage),
					"Page should be Redirected to Orders Page and Order history should be shown.",
					"Page Redirected to Orders Page and Order history shown",
					"Page not Redirected to Orders Page as expected", driver);

			//6
			contactUsPage.headers.navigateToHome();
			ordersPage.headers.mouseOverCustomerService();
			Log.message(i++ + ". Customer Service Flyout menu opened!", driver);

			ordersPage = ordersPage.headers.navigateToReturnItems();
			Log.message(i++ + ". User navigated to Return Items!", driver);

			Log.softAssertThat(ordersPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), ordersPage),
					"Page should be Redirected to Orders Page and Order history should be shown.",
					"Page Redirected to Orders Page and Order history shown",
					"Page not Redirected to Orders Page as expected", driver);
			//7
			contactUsPage.headers.navigateToHome();
			ordersPage.headers.mouseOverCustomerService();
			Log.message(i++ + ". Customer Service Flyout menu opened!", driver);

			ProfilePage profPage = headers.navigateToUpdateMyInformation();
			Log.message(i++ + ". User navigated to Update My Info!", driver);

			Log.softAssertThat(profPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), profPage),
					"Page should be Redirected to My Acccount Page to Update information.",
					"Page Redirected to My Account Page",
					"Page not Redirected to My Account Page as expected", driver);
			//8
			contactUsPage.headers.navigateToHome();
			profPage.headers.mouseOverCustomerService();
			Log.message(i++ + ". Customer Service Flyout menu opened!", driver);

			if(headers.getLiveChatStatus()){
				Log.softAssertThat(profPage.headers.elementLayer.verifyAttributeForElement("imgLiveChatDesktop", "src", "clear.gif", profPage.headers), 
						"Live Chat link should not be display in the Customer service section", 
						"Live Chat link not displayed in the Customer service section", 
						"Live Chat link displayed in the Customer service section", driver);

				Log.softAssertThat(profPage.headers.elementLayer.verifyComputedCssPropertyForElement("divEmailUsDesktop", "display", "inline-block", profPage.headers), 
						"Email us link should be displayed in the middle of the flyout.", 
						"Email us link displayed in the middle of the flyout.", 
						"Email us link not displayed in the middle of the flyout.", driver);
			}else{
				Log.reference("--->>> Live chat is not available at this time. Skipping related validations.");
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_HEADER_C19614


}// search
