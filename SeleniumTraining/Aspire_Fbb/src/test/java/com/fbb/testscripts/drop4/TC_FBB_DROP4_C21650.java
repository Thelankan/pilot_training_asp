package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C21650 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test( groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C21650(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd1 = prdData.get("prd_sku");

		String username;
		String password = accountData.get("password_global");
	
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
	
		//Prerequisite: Remove All items from cart
		{
			GlobalNavigation.emptyCartForUser(username, password, driver);
		}
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAccount=homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
			
			PdpPage pdpPage = myAccount.headers.redirectToPDP(prd1, driver);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName());
	
			pdpPage.selectQty("10");
			pdpPage.addProductToBag();
			Log.message("10 Qty added.", driver);
			pdpPage.addProductToBag();
			Log.message("10 Qty added.", driver);
			Log.message(i++ + ". Product added with Quantity 20", driver);
	
			SignIn signIn = pdpPage.headers.signOut();
			Log.message(i++ + ". Signed out.", driver);
	
			pdpPage = signIn.headers.redirectToPDP(prd1, driver);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName());
	
			pdpPage.selectQty("10");
			pdpPage.addProductToBag();
			Log.message("10 Qty added.", driver);
			pdpPage.addProductToBag();
			Log.message("10 Qty added.", driver);
			pdpPage.addProductToBag();
			Log.message("10 Qty added.", driver);
			Log.message(i++ + ". Product added with Quantity 30", driver);
	
			ShoppingBagPage cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart Page.", driver);
	
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			checkoutPage.typeOnEmail(username);
			checkoutPage.typeOnPassword(password);
			checkoutPage.clickSignInButton();
	
			cartPage = new ShoppingBagPage(driver).get();
			Log.softAssertThat(cartPage.elementLayer.verifyAttributeForElement("divCartTopBannerCheckOutButton", "disabled", "true", cartPage), 
					"The CheckOut button should be disabled while cart product quantity reached about maximum", 
					"The CheckOut button is disabled while cart product quantity reached about maximum as expected !!", 
					"The CheckOut button is not disabled while cart product quantity reached about maximum!!", driver);
	
			//Verify 'Cart Maximum Quantity Reached' message
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("divQuantityMessage"), cartPage),
					"Maximum Quantity alert message should be displayed while cart page is maximum", 
					"Applied Coupon is removed when clicking on remove button", 
					"Applied Coupon is not removed when clicking on remove button", driver);
	
			cartPage.removeAllItemsFromCart();
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP_C21650
}// search
