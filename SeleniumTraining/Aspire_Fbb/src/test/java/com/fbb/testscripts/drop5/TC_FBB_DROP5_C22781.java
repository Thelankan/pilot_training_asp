package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.PaypalConfirmationPage;
import com.fbb.pages.PaypalPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22781 extends BaseTest{
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22781(String browser) throws Exception{
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
		String paypalemail = accountData.get("credential_paypal").split("\\|")[0];
		String paypalpassword = accountData.get("credential_paypal").split("\\|")[1];
		
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, email);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
			
			checkoutPage.fillingShippingDetailsAsGuest("YES", "valid_address1", "Ground");
			Log.message(i++ + ". Shipping details entered successfully!", driver);
			
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			checkoutPage.selectPaypalpayment();
			Log.message(i++ + ". Paypal seleceted as Payment method.", driver);
			
			PaypalPage paypalPage = checkoutPage.clickOnPaypalButton();
			Log.message(i++ + ". Clicked on Paypal button in shopping bag page");
			
			PaypalConfirmationPage pcp = paypalPage.enterPayapalCredentials(paypalemail, paypalpassword);
			Log.message(i++ + ". Continued with Paypal Credentials.", driver);
			
			pcp.clickContinueConfirmation();
			Log.message(i++ + ". Clicked on Continue button.", driver);

			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPage), 
					"Checkout page should be displayed!", 
					"Checkout page is displayed",
					"Checkout page is not displayed", driver);

			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on Place order button", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") || driver.getCurrentUrl().contains("revieworder") ,
					"Order should be placed successfully as a Guest user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a Guest user and Order confirmation page is displayed!",
					"Order is not placed successfully as a Guest user and Order confirmation page is not displayed!", driver);
			
			
			Log.testCaseResult();
		} //try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	
	} //M1_FBB_DROP5_C22781
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP5_C22781(String browser) throws Exception{
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
		String credentials = accountData.get("credential_plcc_preapproved");
		String paypalemail = accountData.get("credential_paypal").split("\\|")[0];
		String paypalpassword = accountData.get("credential_paypal").split("\\|")[1];
		
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, credentials);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
			
			checkoutPage.closePLCCOfferByNoThanks1();
			Log.message(i++ + ". Clicked on No Thanks.", driver);
			
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", "Ground", "plcc_address1");
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPage.clickOnContinue();
			Log.message(i++ + ". Continued to Payment section", driver);
			
			checkoutPage.selectPaypalpayment();
			Log.message(i++ + ". Paypal seleceted as Payment method.", driver);
			
			PaypalPage paypalPage = checkoutPage.clickOnPaypalButton();
			Log.message(i++ + ". Clicked on Paypal button in shopping bag page");
			
			PaypalConfirmationPage pcp = paypalPage.enterPayapalCredentials(paypalemail,paypalpassword);
			Log.message(i++ + ". Continued with Paypal Credentials.", driver);
			
			pcp.clickContinueConfirmation();
			Log.message(i++ + ". Clicked on Continue button.", driver);

			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPage), 
					"Checkout page should be displayed!", 
					"Checkout page is displayed",
					"Checkout page is not displayed", driver);

			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on Place order button", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") || driver.getCurrentUrl().contains("revieworder") ,
					"Order should be placed successfully as a signed in user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a signed in user and Order confirmation page is displayed!",
					"Order is not placed successfully as a signed in user and Order confirmation page is not displayed!", driver);
			
			Log.testCaseResult();
		} //try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	
	} //M2_FBB_DROP5_C22781
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP5_C22781(String browser) throws Exception{
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
		String credentials = accountData.get("plcc_preapproved_" + Utils.getCurrentBrandShort().toString());
		String paypalemail = accountData.get("credential_paypal").split("\\|")[0];
		String paypalpassword = accountData.get("credential_paypal").split("\\|")[1];
		
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, credentials);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
			
			checkoutPage.closePLCCOfferByNoThanks1();
			Log.message(i++ + ". Clicked on No Thanks.", driver);
			
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", "Ground", "plcc_address1");
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPage.clickOnContinue();
			Log.message(i++ + ". Continued to Payment section", driver);
			
			checkoutPage.openClosePLCCRebuttal("open");
			Log.message(i++ + ". Clicked on Learn More on PLCC Rebuttal Section", driver);
			checkoutPage.openClosePLCCRebuttal("close");
			Log.message(i++ + ". Clicked on No Thanks on PLCC Rebuttal modal", driver);
			
			Log.softAssertThat(!checkoutPage.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkLearnMoreInPLCCRebuttal"), checkoutPage), 
					"System should not longer display the acquisition rebuttal content.", 
					"System longer displays the acquisition rebuttal content.", 
					"System still displays the acquisition rebuttal content.", driver);
			
			checkoutPage.selectPaypalpayment();
			Log.message(i++ + ". Paypal seleceted as Payment method.", driver);
			
			PaypalPage paypalPage = checkoutPage.clickOnPaypalButton();
			Log.message(i++ + ". Clicked on Paypal button in shopping bag page");
			
			PaypalConfirmationPage pcp = paypalPage.enterPayapalCredentials(paypalemail,paypalpassword);
			Log.message(i++ + ". Continued with Paypal Credentials.", driver);
			
			pcp.clickContinueConfirmation();
			Log.message(i++ + ". Clicked on Continue button.", driver);

			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPage), 
					"Checkout page should be displayed!", 
					"Checkout page is displayed",
					"Checkout page is not displayed", driver);

			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on Place order button", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") || driver.getCurrentUrl().contains("revieworder") ,
					"Order should be placed successfully as a signed in user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a signed in user and Order confirmation page is displayed!",
					"Order is not placed successfully as a signed in user and Order confirmation page is not displayed!", driver);
			
			Log.testCaseResult();
		} //try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	
	} //M3_FBB_DROP5_C22781

}
