package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22748 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22748(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		//Test data
		String orderDetails = prdData.get("guest_order_return");
		String orderNumber = orderDetails.split("\\|")[0];
		String orderMail = orderDetails.split("\\|")[1];
		String orderZip = orderDetails.split("\\|")[2];
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			SignIn signIn = headers.navigateToSignInpage();
			Log.message(i++ + ". Navigated to Sign In page", driver);
			
			signIn.fillOrderFields(orderNumber, orderMail, orderZip);
			Log.message(i++ + ". Filled in order details.", driver);
			signIn.clickOnCheckInOrder();
			Log.message(i++ + ". Clicked on check order.", driver);
			
			//Step-1: Verify the functionality of Breadcrumb
			if(!Utils.isMobile()) {
				Log.softAssertThat(headers.getBreadCrumbText().trim().equalsIgnoreCase("Home Check An Order"), 
						"Breadcrumb should be displayed as Home / Check An Order. ", 
						"Breadcrumb is displayed as Home / Check An Order. ", 
						"Breadcrumb is not displayed as Home / Check An Order. ", driver);
			}else {
				boolean leftArrowNoTransform = headers.elementLayer.verifyCssPropertyForElement("breadcrumbMobileArrow", "background-image", "icon_leftarrownavsec", headers);
				boolean leftArrowTransform = headers.elementLayer.verifyCssPropertyForElement("breadcrumbMobileArrow", "background-image", "carat-up", headers)
												&& headers.elementLayer.verifyCssPropertyForElement("breadcrumbMobileArrow", "transform", "-1, 1", headers);
				
				Log.softAssertThat((leftArrowNoTransform || leftArrowTransform) && 
									headers.getBreadCrumbText().trim().equalsIgnoreCase("Home"), 
						"Breadcrumb should be displayed as < Home", 
						"Breadcrumb is displayed as < Home", 
						"Breadcrumb is not displayed as < Home", driver);
			}
			
			//Step-2: Verify the functionality of Order Number
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "breadCrumb", "divOrderNumberPrint", signIn), 
					"Order Number should be displayed below the Breadcrumb", 
					"Order Number is displayed below the Breadcrumb", 
					"Order Number is not displayed below the Breadcrumb", driver);
			
			String nameOnOrder = signIn.elementLayer.getAttributeForElement("txtShippingAddressName", "innerHTML", signIn).trim().replaceAll("[\\t\\n\\r]+"," ");
			Log.softAssertThat(signIn.elementLayer.verifyTextContains("divOrderNumberPrint", nameOnOrder, signIn)
							&& signIn.elementLayer.verifyTextContains("divOrderNumberPrint", "Items from Order Number:", signIn)
							&& signIn.elementLayer.verifyTextContains("divOrderNumberPrint", orderNumber, signIn), 
					"Should be displayed as: {firstName} {lastName}'s Items from Order Number: {orderNumber}", 
					"Is displayed as: {firstName} {lastName}'s Items from Order Number: {orderNumber}", 
					"Is not displayed as: {firstName} {lastName}'s Items from Order Number: {orderNumber}", driver);
			
			//Step-3: Verify the functionality of other available components in Guest Order Details Section
			
			if(Utils.isMobile()) {
				
				Log.softAssertThat(signIn.elementLayer.VerifyElementDisplayed(Arrays.asList("txtReceiptMessage","divOrderDetails"), signIn), 
						"Other Order Details Components should be available with correct components.", 
						"Other Order Details Components is available with correct components.", 
						"Other Order Details Components is not available with correct components.", driver);
			}
			else
			{
				Log.softAssertThat(signIn.elementLayer.VerifyElementDisplayed(Arrays.asList("icoEmailBox","txtReceiptMessage","divOrderDetails"), signIn), 
						"Other Order Details Components should be available with correct components.", 
						"Other Order Details Components is available with correct components.", 
						"Other Order Details Components is not available with correct components.", driver);
			}
			if(Utils.isMobile()) {
				Log.softAssertThat(signIn.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkViewDetails"), signIn), 
						"Order details should be available with 'View Details'", 
						"Order details is available with 'View Details'", 
						"Order details is not available with 'View Details'", driver);
				
				signIn.clickOpenCollapseOrderDetailsMobile(true);
				Log.message(i++ + ". Clicked on View More", driver);
				
				Log.softAssertThat(signIn.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkViewLess"), signIn), 
						"Order details should be available with 'View Less'", 
						"Order details is available with 'View Less'", 
						"Order details is not available with 'View Less'", driver);
			}
			
			//Step-4,5,6 Verify the functionality of available components in Shipment & Line Item Details of Guest Order Details Page
			if(Utils.isMobile()) {
				Log.softAssertThat(signIn.elementLayer.VerifyElementDisplayed(Arrays.asList("divShipment","divShipmentHeading","divShipmentAddress",
						"divShipmentStatus","divShipmentDetails","divShipmentQuantityMobile","lnkWriteReview","lnkBuyAgain"), signIn), 
				"Shipment and Line Item Details should be available with the correct components.", 
				"Shipment and Line Item Details is available with the correct components.", 
				"Shipment and Line Item Details is not available with the correct components.", driver);
			}
			else {
				Log.softAssertThat(signIn.elementLayer.VerifyElementDisplayed(Arrays.asList("divShipment","divShipmentHeading","divShipmentAddress",
						"divShipmentStatus","divShipmentDetails","divShipmentQuantity","lnkWriteReview","lnkBuyAgain"), signIn), 
				"Shipment and Line Item Details should be available with the correct components.", 
				"Shipment and Line Item Details is available with the correct components.", 
				"Shipment and Line Item Details is not available with the correct components.", driver);
			}
			
			//Step-7: Verify the functionality of available components in Order Summary & FAQ of Guest Order Details Page
			Log.softAssertThat(signIn.elementLayer.VerifyElementDisplayed(Arrays.asList("divOrderSummery","divTaxDisclaimer"), signIn), 
					"Order Summary & FAQ Components should be available with correct components", 
					"Order Summary & FAQ Components is available with correct components", 
					"Order Summary & FAQ Components is not available with correct components", driver);
			
			//Step-1 cntd.
			headers.clickNthBreadcrumb(0);
			Log.message(i++ + ". Clicked Home on breadcrumb.", driver);
			
			Log.softAssertThat(new HomePage(driver).get().getPageLoadStatus(), 
					"On clicking Home, User should be navigated to the brand-category-root page.", 
					"On clicking Home, User is navigated to the brand-category-root page.", 
					"On clicking Home, User is not navigated to the brand-category-root page.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M1_FBB_DROP5_C22748

}
