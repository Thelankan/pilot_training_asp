package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22548 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22548(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_choid_of_bonus_eligible = prdData.get("prd_choice-bonus");
			
		//Create web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Load Regular Price Product
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_choid_of_bonus_eligible);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			String selectedSize = pdpPage.selectSize();
			Log.message(i++ + ". Size Selected :: '"+selectedSize+"'", driver);
	
			String selectedColor = pdpPage.selectColor();
			Log.message(i++ + ". Selected Color :: '"+selectedColor+"'");
	
			pdpPage.selectQty("2");
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add to cart button for 1 st product");
	
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Clicked on 'X' in mini cart overlay");		
	
			ShoppingBagPage shoppingBag = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on 'My Bag' icon in header");
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("bonusItemAction"), shoppingBag),
					"Bonus Item Action should be displayed in Shopping bag", 
					"Bonus Item Action is displayed in Shopping bag", 
					"Bonus Item Action is not displayed in Shopping bag", driver);
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("btnBonusProductAdd"), shoppingBag),
					"Add button should be displayed in Shopping bag", 
					"Add button is displayed in Shopping bag", 
					"Add button is not displayed in Shopping bag", driver);
			
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("bonusCartPromo"), shoppingBag),
					"Bonus Cart Promo should be displayed in Shopping bag", 
					"Bonus Cart Promo is displayed in Shopping bag", 
					"Bonus Cart Promo is not displayed in Shopping bag", driver);
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("bonusCalloutMessageDisplay"), shoppingBag),
					"Bonus Callout message should be displayed in Shopping bag", 
					"Bonus Callout message is displayed in Shopping bag", 
					"Bonus Callout message is notdisplayed in Shopping bag", driver);
	
			shoppingBag.clickOnBonusProductAdd();
			Log.message(i++ + ". Clicked on 'Add' button for the bonus product");
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("bonusProductOverlay"), shoppingBag),
					"Bonus Product Overlay should be displayed in Shopping bag", 
					"Bonus Product Overlay is displayed in Shopping bag", 
					"Bonus Product Overlay is not displayed in Shopping bag", driver);
	
			String BonusPrdAdded = shoppingBag.selectBonusProductInModal(1);
			Log.message(i++ + ". Selected bonus product from the modal");
	
			shoppingBag.clickOnBonusProductAddToBag();
			Log.message(i++ + ". Clicked on 'Add to bag' button to add bonus product to bag");
	
			Log.reference("Refer Test case #22533 for step 2");
			Log.softAssertThat(shoppingBag.getBonusItemPrice().equals("FREE"),
					"Bonus Item Price 'FREE' should be displayed", 
					"Bonus Item Price 'FREE' is displayed", 
					"Bonus Item Price 'FREE' is not displayed", driver);
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("qtyIncreaseArrowDisabled","qtyDecreaseArrowDisabled"), shoppingBag),
					"Qty increase and decrease arrow should be disabled", 
					"Qty increase and decrease arrow is disabled", 
					"Qty increase and decrease arrow is not disabled", driver);
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnBonusAddToWishlist"), shoppingBag),
					"Add to Wishlist should not be displayed for bonus product", 
					"Add to Wishlist is not displayed for bonus product", 
					"Add to Wishlist is displayed for bonus product", driver);
	
			shoppingBag.clickOnBonusProductEdit();
			Log.message(i++ + ". Clicked on 'Edit' button for the bonus product");
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("bonusProductOverlay"), shoppingBag),
					"Bonus Product Overlay should be displayed in Shopping bag", 
					"Bonus Product Overlay is displayed in Shopping bag", 
					"Bonus Product Overlay is not displayed in Shopping bag", driver);
	
			shoppingBag.selectBonusProductInModal(2);
			Log.message(i++ + ". Selected bonus product from the modal");
	
			String BonusPrdAdded2 = shoppingBag.selectBonusProductInModal(2);
			Log.message(i++ + ". Selected bonus product from the modal");
	
			shoppingBag.clickOnBonusProductAddToBag();
			Log.message(i++ + ". Clicked on 'Add to bag' button to add bonus product to bag");
	
			Log.softAssertThat(!BonusPrdAdded.equals(BonusPrdAdded2), 
					"Bonus Product should be edited", 
					"Bonus Product is edited", 
					"Product is not edited", driver);
	
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C22548
}// search
