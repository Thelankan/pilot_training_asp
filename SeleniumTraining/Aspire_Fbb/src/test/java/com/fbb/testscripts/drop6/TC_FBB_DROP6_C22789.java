package com.fbb.testscripts.drop6;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP6_C22789 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP6_C22789(String browser) throws Exception {

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo();

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			GiftCardPage giftCardPage = homePage.footers.navigateToGiftCardPage();
			Log.message(i++ + ". Navigated to Gift Card Page!", driver);
			
			giftCardPage.clickOnbalanceCheckUpButton();
			Log.message(i++ + ". Gift Card Balance Lookup overlay opened!", driver);

			//Step-1: Verify the functionality of Card Number Field
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("placeholderCardNumber","placeholderPin"),giftCardPage),
					"Place holder of card Number/pin should be present.",
					"Place holder of card Number/pin are present",
					"Place holder of card Number/pin not present", driver);
			
			Log.softAssertThat(giftCardPage.elementLayer.verifyPlaceHolderMovesAboveForList(Arrays.asList("txtfieldCardnumber","txtFieldCardPin"), Arrays.asList("placeholderCardNumber","placeholderPin"), Arrays.asList("241132413412","1234"), giftCardPage), 
					"Place Holder should move above when typing text in Card Number/PIN fields", 
					"Palce Holder moved above.", 
					"Place Holder doesn't move above.", driver);
			
			//Step-3: Verify the functionality of Submit button
			if(!Utils.isMobile()) {
				Log.softAssertThat(giftCardPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "applyButton", "txtFieldCardPin", giftCardPage), 
						"Submit button should be displayed to the right side of the Card Pin Field", 
						"Submit button is displayed to the right side of the Card Pin Field", 
						"Submit button is not displayed to the right side of the Card Pin Field", driver);
			}else {
				Log.softAssertThat(giftCardPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtFieldCardPin", "applyButton", giftCardPage), 
						"Submit button should be displayed to the right side of the Card Pin Field", 
						"Submit button is displayed to the right side of the Card Pin Field", 
						"Submit button is not displayed to the right side of the Card Pin Field", driver);
			}		
			
			Log.reference("Rest of step 3 is covered in Step 2");

			//Step-2: Verify the functionality of Card Pin Field
			Log.softAssertThat(giftCardPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtfieldCardnumber","txtFieldCardPin",giftCardPage),
					"Card Number should be displayed above PIN",
					"Card Number displayed above PIN",
					"Card Number not displayed above PIN", driver);

			if(Utils.isMobile()) {	
				Log.softAssertThat(giftCardPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtFieldCardPin","applyButton",giftCardPage),
						"Card PIN should be displayed above Apply",
						"Card PIN displayed above Apply",
						"Card PIN not displayed above Apply", driver);
			}

			giftCardPage.clickOnbalanceCheckUpButton();
			giftCardPage.ApplyCardDetails("","");
			Log.softAssertThat(giftCardPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("errorMsgCardName", "errorMsgCardPin"), giftCardPage), 
					"Card Number/PIN should be a mandatory field", 
					"Card Number/PIN are mandatory fields", 
					"Card Number/PIN aren't mandatory fields", driver);

			Log.softAssertThat(giftCardPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("txtfieldCardnumber"), 19, giftCardPage), 
					"The length of the cardNumber should not exceed 19 characters",
					"The length of the cardNumber not exceed 19 characters",
					"The length of the cardNumber exceed 19 characters", driver);

			Log.softAssertThat(giftCardPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("txtFieldCardPin"), 4,giftCardPage), 
					"The length of the cardNumber should not exceed 4 characters",
					"The length of the cardNumber not exceed 4 characters",
					"The length of the cardNumber exceed 4 characters", driver);
			
			String[] giftCardDetail = prdData.getProperty("gift_card_valid_1").split("\\|");
			
			giftCardPage.clickOnbalanceCheckUpButton();
			giftCardPage.ApplyCardDetails("111111","111");
			Log.message(i++ + ". Entering Invalid Card and PIN number");
			
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("invalidcardMsg"), giftCardPage), 
					"Check the error message is getting displayed when entering invalid Gift Card details",
					"The error message is getting displayed when entering invalid Gift Card details",
					"The error message is not displayed when entering invalid Gift Card details", driver);
			
			giftCardPage.ApplyCardDetails("1111111111111111111","1111");
			Log.message(i++ + ". Entering Wrong Card and PIN number");
			
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("invalidcardMsg"), giftCardPage), 
					"Check the error message is getting displayed when entering wrong Gift Card",
					"The error message is getting displayed when entering wrong Gift Card",
					"The error message is not displayed when entering wrong Gift Card", driver);
			
			giftCardPage.ApplyCardDetails(giftCardDetail[0],giftCardDetail[1]);
			Log.message(i++ + ". Entering Valid Card and PIN number");
			
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("totalBalanceMsg"), giftCardPage), 
					"The Gift card balance should get displayed",
					"The Gift card balance is getting displayed",
					"The Gift card balance is not displayed", driver);
			
			giftCardPage.clickClose();
			Log.message(i++ + ". Check Balance Overlay closed");
			
			homePage = giftCardPage.clickOnHomeInBC();
			Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage), 
					"The Home page should get displayed when clicking on 'Home' in breadcrumb",
					"The Home page is getting displayed when clicking on 'Home' in breadcrumb",
					"The Home page is not displayed when clicking on 'Home' in breadcrumb", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_HEADER_C22789

}//TC_FBB_DROP6_C22783
