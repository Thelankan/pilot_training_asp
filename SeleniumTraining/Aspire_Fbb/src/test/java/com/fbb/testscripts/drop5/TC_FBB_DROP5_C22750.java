package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.pages.ordering.GuestOrderStatusLandingPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22750 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22750(String browser) throws Exception {
		//Pre-condition: Given order details should have both products eligible & not eligible for Return.
	
		Log.testCaseInfo();
	
		//Load Test Data
		String orderDetails = prdData.get("guest_order_return");
		String orderNumber = orderDetails.split("\\|")[0];
		String orderEmail = orderDetails.split("\\|")[1];
		String zipcode = orderDetails.split("\\|")[2];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
	
			SignIn signIn = homePage.headers.navigateToSignInpage();
			Log.message(i++ + ". Navigated to Guest order status page.", driver);
	
			GuestOrderStatusLandingPage guestOrderPage = signIn.navigateToGuestOrderStatusPage(orderNumber, orderEmail, zipcode);
			Log.message(i++ + ". Navigated to Order Status Page.", driver);
	
			if (guestOrderPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnReturn"), guestOrderPage)) {
				guestOrderPage.clickOnReturn();
				Log.message(i++ + ". Clicked on Return button.", driver);
			} else {
				Log.fail("Return items not present in the order. Hence test case cannot be executed further!", driver);
			}
	
			//Step-1: Verify the display and functionality of Breadcrumb
			Log.softAssertThat(guestOrderPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "cntPromoBanner", "cntBreadCrumb", guestOrderPage),
					"Breadcrumb should be displayed below the Promotional Content",
					"Breadcrumb is displayed below the Promotional Content",
					"Breadcrumb is not not displayed below the Promotional Content", driver);
	
			if(!Utils.isMobile()) {
				Log.softAssertThat(guestOrderPage.verifyBreadCrumb("HomeCheck An Order"),
						"Breadcrumb should be displayed as Home / Check an Order",
						"Breadcrumb displayed as Home / Check an Order",
						"Breadcrumb not displayed as Home / Check an Order", driver);
			}
	
			if(Utils.isMobile()){
				Log.softAssertThat(guestOrderPage.elementLayer.verifyElementTextEqualTo("lnkBreakCrumbMobile", "HOME", guestOrderPage),
						"Breadcrumb should be displayed as < Order History",
						"Breadcrumb displayed as < Order History",
						"Breadcrumb not displayed as < Order History", driver);
			}
	
			//Step-2: Verify the display and functionality of other components in Guest Order Return Page
			Log.softAssertThat(guestOrderPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("sectionReturnItems","lblReturnSubHeader","lblReturnMessage","lblSpecialCalloutCopy","btnCancel","btnReturnSelectedItems","lblEligibleListHeader"), guestOrderPage),
					"Guest Order Return Page should be displayed with this components :: {Return Items, Sub header, Return Copy, Special Callout Copy, Cancel, Return Selected Items, Eligible for Returns, Checkbox, Product Details, Return QTY, Select Reason, Not Eligible for Returns, Return Reason, Returned, Quantity, Item Total, Reason}",
					"Guest Order Return Page displayed with mentioned components",
					"Guest Order Return Page not displayed all mentioned components", driver);
			
			Log.reference("Covered display components in Test Case Id: C22605 Step 2 to 17");
			Log.reference("Covered functionality in Test Case Id: C22614 Step 1 to 17");
			
			//Step-1.d:  On clicking Home, user should be navigated to the brand-category-root page
			homePage = guestOrderPage.clickOnHomeInBC();
			Log.message(i++ + ". Clicked on Home Link in BreadCrumb.", driver);

			Log.softAssertThat(driver.getCurrentUrl().equals("https://sfcc.qa."+ Utils.getCurrentBrandShort() +".plussizetech.com/"),
					"On clicking Home, user should be navigated to the brand-category-root page",
					"On clicking Home, user navigated to the brand-category-root page",
					"On clicking Home, user not navigated to the brand-category-root page", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP5_C22750

}// search
