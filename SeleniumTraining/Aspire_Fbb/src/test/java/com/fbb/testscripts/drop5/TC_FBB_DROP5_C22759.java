package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22759 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22759(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_size-chart");
		String checkoutRedirect = redirectData.get("checkout");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			String Credential = AccountUtils.generateEmail(driver);// + accountData.get("password_global");
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, Credential);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", "plcc_address1", "Ground");
			Log.message(i++ + ". Shipping details entered successfully!", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section!", driver);
	
			Log.assertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPage),
					"For given Address PLCC Approval Modal should be opened!",
					"For given Address PLCC Approval Modal opened!",
					"For given Address PLCC Approval Modal not opened. Further validations cannot be continued.", driver);
	
			//Step-1: Verify the functionality of Offer Content
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblPLCCCreditLimit"), checkoutPage), 
					"The pre-approved credit limit from ADS should be displayed in the Offer Content",
					"The pre-approved credit limit from ADS displayed in the Offer Content",
					"The pre-approved credit limit from ADS not displayed in the Offer Content", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextContains("lblPLCCCreditLimit", "$", checkoutPage), 
					"Credit limit should be displayed beginning with Dollar Symbol",
					"Credit limit displayed beginning with Dollar Symbol",
					"Credit limit not displayed beginning with Dollar Symbol", driver);
	
			//Step-2: Verify the functionality of Pre-screen Notice
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "cntOfferContent", "cntPreScreenNotice", checkoutPage), 
					"Prescreen notice shoudl be located under the perks of opening a PLCC card.", 
					"Prescreen notice is located under the perks of opening a PLCC card.", 
					"Prescreen notice is not located under the perks of opening a PLCC card.", driver);
			
			//Step-3: Verify the functionality of Legal Copy
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "cntPreScreenNotice", "cntLegalCopy", checkoutPage), 
					"Legal Copy should be located at the bottom of the Step 1 overlay.", 
					"Legal Copy is located at the bottom of the Step 1 overlay.", 
					"Legal Copy is not located at the bottom of the Step 1 overlay.", driver);
	
			//Step-4: Verify the functionality of No Thanks button
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("lblNoThanksGetItToday", "padding", "20px", checkoutPage), 
					"No Thanks button should remain fixed at the bottom of the modal",
					"No Thanks button remain fixed at the bottom of the modal",
					"No Thanks button not fixed at the bottom of the modal", driver);
	
			//Step-5: Verify the functionality of Get it Today
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("lblNoThanksGetItToday", "padding", "20px", checkoutPage), 
					"Get it Today button should remain fixed at the bottom of the modal",
					"Get it Today button should fixed at the bottom of the modal",
					"Get it Today button should not fixed at the bottom of the modal", driver);
	
			checkoutPage.continueToPLCCStep2();
			Log.message(i++ + ". Clicked on 'Get it Today Button'!", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("mdlPLCCApprovalStep2"), checkoutPage), 
					"On clicking Get it Today, system should invoke the Step Two overlay",
					"On clicking Get it Today, system invoked the Step Two overlay",
					"On clicking Get it Today, system didn't invoke the Step Two overlay", driver);
	
			Log.message(i++ + ". Clicked on outside PLCC Modal!", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("mdlPLCCApprovalStep2"), checkoutPage), 
					"System should not close this modal when the user clicks anywhere outside the modal",
					"System not close this modal when the user clicks anywhere outside the modal",
					"System close this modal when the user clicks anywhere outside the modal", driver);
	
			//Step-4: Verify the functionality of No Thanks button continued
			checkoutPage.closePLCCOfferByNoThanks();
			Log.message(i++ + ". Clicked on 'No Thanks' Button!", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("mdlPLCCApprovalStep2"), checkoutPage), 
					"On clicking No Thanks button, system should close the modal",
					"On clicking No Thanks button, system closed the modal",
					"On clicking No Thanks button, system not closing the modal", driver);
	
			driver.navigate().to(Utils.getWebSite() + checkoutRedirect);
			checkoutPage.continueToShipping(Credential);
			Log.message(i++ + ". Navigated to shipping page", driver);
			
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to payment.", driver);
			
			Log.softAssertThat(!checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblPLCCCreditLimit"), checkoutPage), 
					"System should not display this modal again during active session.",
					"System did not display this modal again during active session.",
					"System displayes the modal again during active session.", driver);
	
			//Step-6: Verify the functionality of Close Modal
			checkoutPage.openClosePLCCRebuttal("open");
			Log.message(i++ + ". Clicked on Learn more in PLCC Rebuttal Slot.", driver);
	
			checkoutPage.continueToPLCCStep2InPLCCACQ();
			Log.message(i++ + ". Clicked on Get It Today Button.", driver);
	
			checkoutPage.closePLCCOfferByCloseBtn();
			Log.message(i++ + ". Clicked on 'X' button in PLCC Modal!", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("mdlPLCCApprovalStep2"), checkoutPage), 
					"On clicking the X button, system should close the modal",
					"On clicking the X button, system closed the modal",
					"On clicking the X button, system didn't close the modal", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPage), 
					"User should be return to the Checkout page upon closing the modal",
					"User returned to the Checkout page upon closing the modal",
					"User not returned to the Checkout page upon closing the modal", driver);
	
			//Once dismissed, the System should not display this modal again during their active session - HOLD
			//System should store the session attribute to invoke Acquisition Rebuttal content slot
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP5_C22759

}// search
