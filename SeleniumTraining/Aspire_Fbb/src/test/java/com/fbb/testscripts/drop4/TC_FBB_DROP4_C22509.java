package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22509 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22509(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prodId = prdData.get("prd_variation");
		String guestEmail;
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		guestEmail = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
			new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			Object[] obj = GlobalNavigation.checkout(driver, prodId, i, guestEmail);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (Integer) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", "valid_address7", "Ground");
			Log.message(i++ + ". Shipping details filled successfully.", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment!", driver);
	
			checkoutPage.fillingCardDetails1("card_Visa", false);
			Log.message(i++ + ". Card Details filled successfully.", driver);
	
			checkoutPage.continueToReivewOrder();
			Log.message(i++ + ". Continued to Order Review Section.", driver);
	
			if(Utils.isMobile()) {
				//Step-1(Mobile): Verify the functionality of Place Order Button in the Review & Place Order Section
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "reviewPlaceOrderHeading", "btnPlaceOrder", checkoutPage), 
						"Place Order Button should be displayed to below the Step 2 Module Heading in the Review & Place Order Section. ", 
						"Place Order Button is displayed to below the Step 2 Module Heading in the Review & Place Order Section. ", 
						"Place Order Button is not displayed to below the Step 2 Module Heading in the Review & Place Order Section. ", driver);
	
				//Step-3(Mobile): Verify the display of Place Order Disclaimer
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblReviewDesclaimerInSummary", "btnPlaceOrderRight", checkoutPage), 
						"Place Order Disclaimer should be displayed above the 'Place Order' button.", 
						"Place Order Disclaimer is displayed above the 'Place Order' button.", 
						"Place Order Disclaimer is not displayed above the 'Place Order' button.", driver);
			}else {
				//Step-1: Verify the functionality of Place Order Button in the Review & Place Order Section
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnPlaceOrder", "reviewPlaceOrderHeading", checkoutPage), 
						"The first Place Order button should ne right side of the Place Order Disclaimer.", 
						"The first Place Order button is to right side of the Place Order Disclaimer.", 
						"The first Place Order button is not to right side of the Place Order Disclaimer.", driver);
	
				//Step-3: Verify the display of Place Order Disclaimer
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "reviewPlaceOrderHeading", "lblCheckout_Review_Desclaimer_Message", checkoutPage), 
						"First Place Order Disclaimer should be displayed below the review& order heading.", 
						"First Place Order Disclaimer is displayed below the review& order heading.", 
						"First Place Order Disclaimer is not displayed below the review& order heading.", driver);
				
				if(Utils.isTablet())
				{
					Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnPlaceOrderRight","lblReviewDesclaimerInSummary", checkoutPage), 
							"Second Place Order Disclaimer should be displayed above the 'Place Order' button.", 
							"Second Place Order Disclaimer is displayed above the 'Place Order' button.", 
							"Second Place Order Disclaimer is not displayed above the 'Place Order' button.", driver);
				}
				else
				{

					Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblReviewDesclaimerInSummary","btnPlaceOrderRight", checkoutPage), 
							"Second Place Order Disclaimer should be displayed above the 'Place Order' button.", 
							"Second Place Order Disclaimer is displayed above the 'Place Order' button.", 
							"Second Place Order Disclaimer is not displayed above the 'Place Order' button.", driver);
				}
				
			}
	
			//Step-2: Verify the display of Step 3 Module Heading
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblMiniPaymentSection", "reviewPlaceOrderHeading", checkoutPage), 
					"The Review & Place order heading should be displayed below the Payment Details Section.", 
					"The Review & Place order heading is displayed below the Payment Details Section.", 
					"The Review & Place order heading is not displayed below the Payment Details Section.", driver);
	
			//Step-4: Verify the display of Secure Message
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "secureMsgBelowReviewOrder", "lblReviewDesclaimerInSummary", checkoutPage), 
					"Secure Message should be displayed below the Place Order Disclaimer in the review section.", 
					"Secure Message is displayed below the Place Order Disclaimer in the review section.", 
					"Secure Message is not displayed below the Place Order Disclaimer in the review section.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnPlaceOrderRight", "secureMsgBelowPlaceOrder", checkoutPage), 
					"Secure Message should be displayed below the Place Order button in the order summary page.", 
					"Secure Message is displayed below the Place Order button in the order summary page.", 
					"Secure Message is not displayed below the Place Order button in the order summary page.", driver);
	
			OrderConfirmationPage orderConfPg = checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". clicked on Place order button.", driver);
	
			Log.softAssertThat(orderConfPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderConfPg),
					"To check system navigated to 'Order Conformation' page.",
					"The 'Order Conformation' is displaying!",
					"The 'Order Conformation' is not displaying", driver);
	
			ShoppingBagPage shoppingbagpg = orderConfPg.headers.navigateToShoppingBagPage();
			
			Log.softAssertThat(shoppingbagpg.getProductCountInCart()==0, 
					"Shopping bag should be empty after placing the order!!!", 
					"Shopping bag is empty after placing the order!!!", 
					"Shopping bag is not empty after placing the order!!!", driver);
			
			System.out.println("Shooping Bag size" + shoppingbagpg.getProductCountInCart() );
			
			//If device type is Desktop / tablet, we need to verify the second "place order" button on the page
			if(!Utils.isMobile()) {
				obj = GlobalNavigation.checkout(driver, prodId, i, guestEmail);
				checkoutPage = (CheckoutPage) obj[0];
				i = (Integer) obj[1];
	
				checkoutPage.fillingShippingDetailsAsGuest("YES", "valid_address7", "Ground");
				Log.message(i++ + ". Shipping details filled successfully.", driver);
	
				checkoutPage.continueToPayment();
				Log.message(i++ + ". Continued to Payment!", driver);
	
				checkoutPage.fillingCardDetails1("card_Visa", false);
				Log.message(i++ + ". Card Details filled successfully.", driver);
	
				checkoutPage.continueToReivewOrder();
				Log.message(i++ + ". Continued to Order Review Section.", driver);
				//Step-1: Verify the functionality of Place Order Button in the Review & Place Order Section
				orderConfPg = checkoutPage.clickOnPlaceOrderButtonInRightSideOrderSummary();
				Log.message(i++ + ". Clicked on Place Order button right side of summary section.", driver);
				
				Log.softAssertThat(orderConfPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderConfPg),
						"To check system navigated to 'Order Conformation' page.",
						"The 'Order Conformation' is displaying!",
						"The 'Order Conformation' is not displaying", driver);
				
				shoppingbagpg =orderConfPg.headers.navigateToShoppingBagPage();
				
				Log.softAssertThat(shoppingbagpg.getProductCountInCart()==0, 
						"Shopping bag should be empty after placing the order!!!", 
						"Shopping bag is empty after placing the order!!!", 
						"Shopping bag is not empty after placing the order!!!", driver);
			}
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_FBB_DROP4_C22509
}// search
