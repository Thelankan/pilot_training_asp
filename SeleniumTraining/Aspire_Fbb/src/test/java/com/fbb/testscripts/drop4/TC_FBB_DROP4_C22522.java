package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22522 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkOutData = EnvironmentPropertiesReader.getInstance("checkout");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22522(String browser) throws Exception {
		//Need an user account with PLCC & Non-PLCC Accounts
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = prdData.get("prd_high_cost");
		String username = accountData.get("credential_plcc_non-plcc").split("\\|")[0];
		String password = accountData.get("credential_plcc_non-plcc").split("\\|")[1];
		String giftCard_no = prdData.get("gift_card_$10_1").split("\\|")[0];
		String giftCard_pin = prdData.get("gift_card_$10_1").split("\\|")[1];
		String giftCard_no1 = prdData.get("gift_card_$1000_1").split("\\|")[0];
		String giftCard_pin1 = prdData.get("gift_card_$1000_1").split("\\|")[1];
		String[] cardVisa = checkOutData.get("card_Visa").split("\\|");
	
		//Create web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to My Account.", driver);
			
			PdpPage pdpPage = myAcc.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBag();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			ShoppingBagPage cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Cart Page.", driver);
	
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to CheckoutPage.", driver);
	
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", "Ground", "valid_address7");
			Log.message(i++ + ". Shipping Details filled successfully.", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page.", driver);
	
			//Step-4: Verify the functionality of Amount Remaining
			Log.softAssertThat(checkoutPage.elementLayer.verifyInsideElementAlligned("amtRemaining", "billingCardSection", "top", checkoutPage),
					"Amount Remaining should be displayed on the top of Payment Method selection, indented to the left",
					"Amount Remaining should be displayed on the top of Payment Method selection, indented to the left",
					"Amount Remaining should be displayed on the top of Payment Method selection, indented to the left", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("divRdoCreditCard", "divRdoPaypal"), checkoutPage),
					"System should display the amount to be paid using either Credit card or PayPal",
					"System should display the amount to be paid using either Credit card or PayPal",
					"System should display the amount to be paid using either Credit card or PayPal", driver);
	
			float amountBefore = checkoutPage.getAmountRemaining();
			Log.event("Amount Before :: " + amountBefore);
	
			checkoutPage.applyGiftCardNumbAndPin(giftCard_no, giftCard_pin);
			Log.message(i++ + ". Gift Card applied.", driver);
	
			float amountAfter = checkoutPage.getAmountRemaining();
			Log.event("Amount After :: " + amountAfter);
	
			Log.softAssertThat(amountBefore != amountAfter,
					"Amount Remaining value should change if the User applies a valid Reward Certificate/Gift Certificate",
					"Amount Remaining value should change if the User applies a valid Reward Certificate/Gift Certificate",
					"Amount Remaining value should change if the User applies a valid Reward Certificate/Gift Certificate", driver);
	
			checkoutPage.removeAppliedGiftCard();
			Log.message(i++ + ". Gift card removed.");
	
			checkoutPage.applyGiftCardNumbAndPin(giftCard_no1, giftCard_pin1);
			Log.message(i++ + ". Gift Card applied.", driver);
	
			//Step-1: Verify if the Payment Methods selection section is displayed when there is no remaining balance after applying promotions, reward certificates and gift cards
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("divRdoCreditCard"), checkoutPage),
					"System should not display the Payment Methods selection section when there is no remaining balance after applying promotions, reward certificates and gift cards",
					"System not display payment method selection section.",
					"System displays payment method selection section.", driver);
	
			checkoutPage.removeAppliedGiftCard();
			Log.message(i++ + ". Gift card removed.");
	
			//Step-2: Verify the functionality of default card logic
			//Instance PLCC card creation required. Skipping the step
	
			//Step-3: Verify the functionality of PLCC
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("section_First_PLCC_Card"), checkoutPage),
					"PLCC Card should be displayed.",
					"PLCC Card displayed.",
					"PLCC Card not displayed.", driver);
	
			//Step-5: Verify the functionality of Payment Type Selector
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "amtRemaining", "divRdoCreditCard", checkoutPage),
					"The Payment Type Selector should be displayed below the Amount Remaining indicator",
					"The Payment Type Selector should be displayed below the Amount Remaining indicator",
					"The Payment Type Selector should be displayed below the Amount Remaining indicator", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("divRdoCreditCard", "divRdoPaypal"), checkoutPage) &&
					checkoutPage.elementLayer.verifyAttributeForElement("divCreditCardListSection", "class", "payment-method-expanded", checkoutPage),
					"The payment types should be presented in the form of a Credit Card or PayPal radio button. Credit card should be selected by default.",
					"The payment types should be presented in the form of a Credit Card or PayPal radio button. Credit card should be selected by default.",
					"The payment types should be presented in the form of a Credit Card or PayPal radio button. Credit card should be selected by default.", driver);
	
			checkoutPage.selectSavedCard_Non_PLCC(1); //For Non-PLCC card selection
			Log.message(i++ + ". Non-PLCC payment option selected.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("fldCVV"), checkoutPage),
					"If the User selects a non-PLCC payment method, then the 'Confirm CVV' entry field should appear",
					"If the User selects a non-PLCC payment method, then the 'Confirm CVV' entry field should appear",
					"If the User selects a non-PLCC payment method, then the 'Confirm CVV' entry field should appear", driver);
	
			//Step-6: Verify the functionality of Saved Payment Instruments Count. Note to also remove a saved card from the account and confirm Payment Instruments count displaying the right number or not.
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divRdoCreditCard", "savedPaymentCount", checkoutPage),
					"Saved Payment Instruments Count should be displayed below the Payment Type Selector radio button",
					"Saved Payment Instruments Count should be displayed below the Payment Type Selector radio button",
					"Saved Payment Instruments Count should be displayed below the Payment Type Selector radio button", driver);
	
			//Step-7: Verify the functionality of Add New Credit Card
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "savedPaymentCount", "btnAddNewCard", checkoutPage),
						"The 'Add New Credit Card' link should be displayed below the Saved Payment Instrument Count",
						"The 'Add New Credit Card' link should be displayed below the Saved Payment Instrument Count",
						"The 'Add New Credit Card' link should be displayed below the Saved Payment Instrument Count", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnAddNewCard", "savedPaymentCount", checkoutPage),
						"The 'Add New Credit Card' link should be displayed to the right end of the Saved Payment Instrument Count",
						"The 'Add New Credit Card' link should be displayed to the right end of the Saved Payment Instrument Count",
						"The 'Add New Credit Card' link should be displayed to the right end of the Saved Payment Instrument Count", driver);
			}
			
			checkoutPage.clickOnAddNewPaymentMethod();
			Log.message(i++ + ". Clicked on Add New Credit Card Link.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("fldTxtNameOnCard"), checkoutPage),
					"On clicking the 'Add New Credit Card' link, System should present add new card form",
					"On clicking the 'Add New Credit Card' link, System should present add new card form",
					"On clicking the 'Add New Credit Card' link, System should present add new card form", driver);
	
			checkoutPage.clickOnCancelLinkInPayments();
			Log.message(i++ + ". Clicked on Cancel link.", driver);
			
			//Step-8: Verify the display of PLCC Message
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnAddNewCard", "lblPLCCMessage", checkoutPage),
						"The PLCC Message section should be displayed below the 'Add New Credit Card' link",
						"The PLCC Message section should be displayed below the 'Add New Credit Card' link",
						"The PLCC Message section should be displayed below the 'Add New Credit Card' link", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "savedPaymentCount", "lblPLCCMessage", checkoutPage),
						"The PLCC Message section should be displayed below the Saved Address Payment Count",
						"The PLCC Message section should be displayed below the Saved Address Payment Count",
						"The PLCC Message section should be displayed below the Saved Address Payment Count", driver);
			}
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("lblPLCCMessage", "display", "inline-block", checkoutPage) &&
					checkoutPage.elementLayer.verifyCssPropertyForElement("lblPLCCMessage", "background", "rgb(255, 255, 255)", checkoutPage),
					"The message should be displayed in a separate box section, in a white background",
					"The message should be displayed in a separate box section, in a white background",
					"The message should be displayed in a separate box section, in a white background", driver);
	
			//Step-9: Verify the display of Saved PLCC
			Log.softAssertThat(checkoutPage.elementLayer.verifyInsideElementAlligned("logo_First_PLCC_Card", "section_First_PLCC_Card", "left", checkoutPage),
					"Card logo - should be displayed on the left end of the section",
					"Card logo - should be displayed on the left end of the section",
					"Card logo - should be displayed on the left end of the section", driver);
	
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lbl_First_PLCC_Card_Name_Mobile", "lbl_Selected_Card_holder_Name", checkoutPage),
						"Name on Card will be displayed under the Card Name",
						"Name on Card will be displayed under the Card Name",
						"Name on Card will be displayed under the Card Name", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lbl_Selected_Card_holder_Name", "lbl_First_PLCC_Card_Name", checkoutPage),
						"Name on Card - should be displayed next to Card Name",
						"Name on Card - should be displayed next to Card Name",
						"Name on Card - should be displayed next to Card Name", driver);
			}
			
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lbl_First_PLCC_Available_Credit"), checkoutPage),
					"Available Credit - how much money is left on the PLCC card",
					"Available Credit - how much money is left on the PLCC card",
					"Available Credit - how much money is left on the PLCC card", driver);
	
			checkoutPage.selectSavedCard_PLCC(1);
			Log.message(i++ + ". PLCC payment option selected.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "selectedPLCCCard", "rdoDifferedBilling", checkoutPage),
					"When a PLCC card is selected, the Deferred Payment checkbox displays underneath it",
					"When a PLCC card is selected, the Deferred Payment checkbox displays underneath it",
					"When a PLCC card is selected, the Deferred Payment checkbox displays underneath it", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lbl_UnSelected_Card_holder_Name", "lbl_First_PLCC_Available_Credit_Value", checkoutPage),
						"Open to buy amount - should be displayed next to Name on Card",
						"Open to buy amount - should be displayed next to Name on Card",
						"Open to buy amount - should be displayed next to Name on Card", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lbl_First_PLCC_Available_Credit_Value", "lbl_UnSelected_Card_holder_Name", checkoutPage),
						"Open to buy amount - should be displayed next to Name on Card",
						"Open to buy amount - should be displayed next to Name on Card",
						"Open to buy amount - should be displayed next to Name on Card", driver);
			}
	
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lbl_First_PLCC_Available_Credit_Value", "section_First_PLCC_Rewards", checkoutPage),
						"Reward Points appear under the Open to Buy amount",
						"Reward Points appear under the Open to Buy amount",
						"Reward Points appear under the Open to Buy amount", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "section_First_PLCC_Rewards", "lbl_First_PLCC_Available_Credit_Value", checkoutPage),
						"Reward Points - should be displayed next to the Open to Buy amount",
						"Reward Points - should be displayed next to the Open to Buy amount",
						"Reward Points - should be displayed next to the Open to Buy amount", driver);
			}
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lbl_First_PLCC_Tooltip", "section_First_PLCC_Rewards", checkoutPage),
					"Tool Tip - Should be displayed next to Reward Points",
					"Tool Tip - Should be displayed next to Reward Points",
					"Tool Tip - Should be displayed next to Reward Points", driver);
	
			checkoutPage.selectSavedCard_Non_PLCC(1);
			Log.message(i++ + ". Non-PLCC payment option selected.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("rdoDifferedBilling"), checkoutPage) ||
					checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("rdoDifferedBilling"), checkoutPage),
					"When user chooses a non-PLCC card, the Deferred Payment option goes away.",
					"When user chooses a non-PLCC card, the Deferred Payment option goes away.",
					"When user chooses a non-PLCC card, the Deferred Payment option goes away.", driver);
	
			//Step-10: Verify the display of Saved Non PLCC Card
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "section_First_PLCC_Card", "section_Selected_Non_PLCC_Card", checkoutPage),
					"The Saved Non-PLCC Card should be displayed below the Saved PLCC Card ",
					"The Saved Non-PLCC Card should be displayed below the Saved PLCC Card ",
					"The Saved Non-PLCC Card should be displayed below the Saved PLCC Card ", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyInsideElementAlligned("logo_Selected_Non_PLCC_Card", "section_Selected_Non_PLCC_Card", "left", checkoutPage),
					"Card logo - should be displayed on the left end of the section",
					"Card logo - should be displayed on the left end of the section",
					"Card logo - should be displayed on the left end of the section", driver);
	
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lbl_Selected_Non_PLCC_Card_Name_Mobile", "logo_Selected_Non_PLCC_Card", checkoutPage),
						"Card name - should be displayed next to Card Logo",
						"Card name - should be displayed next to Card Logo",
						"Card name - should be displayed next to Card Logo", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lbl_Selected_Non_PLCC_Card_Name", "logo_Selected_Non_PLCC_Card", checkoutPage),
						"Card name - should be displayed next to Card Logo",
						"Card name - should be displayed next to Card Logo",
						"Card name - should be displayed next to Card Logo", driver);
			}
			
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lbl_Selected_Non_PLCC_Card_Name_Mobile", "lbl_Selected_Non_PLCC_Card_holder_Name", checkoutPage),
						"Name on Card will be displayed under the Card Name",
						"Name on Card will be displayed under the Card Name",
						"Name on Card will be displayed under the Card Name", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lbl_Selected_Non_PLCC_Card_holder_Name", "lbl_Selected_Non_PLCC_Card_Name", checkoutPage),
						"Name on Card - should be displayed next to Card Name",
						"Name on Card - should be displayed next to Card Name",
						"Name on Card - should be displayed next to Card Name", driver);
			}
			
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lbl_Selected_Non_PLCC_Card_holder_Name", "lbl_Selected_Non_PLCC_Card_Number", checkoutPage),
						"Masked Card Number should be displayed below the Name on Card",
						"Masked Card Number should be displayed below the Name on Card",
						"Masked Card Number should be displayed below the Name on Card", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lbl_Selected_Non_PLCC_Card_Number", "lbl_Selected_Non_PLCC_Card_holder_Name", checkoutPage),
						"Masked Card Number - should be displayed next to the Name on Card",
						"Masked Card Number - should be displayed next to the Name on Card",
						"Masked Card Number - should be displayed next to the Name on Card", driver);
			}
			
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lbl_Selected_Non_PLCC_Card_Number", "lbl_Selected_Non_PLCC_Card_Exp_Date", checkoutPage),
						"Expiration Date should be displayed below the Masked Card Number",
						"Expiration Date should be displayed below the Masked Card Number",
						"Expiration Date should be displayed below the Masked Card Number", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lbl_Selected_Non_PLCC_Card_Exp_Date", "lbl_Selected_Non_PLCC_Card_Number", checkoutPage),
						"Expiration Date - should be displayed next to Masked Card Number",
						"Expiration Date - should be displayed next to Masked Card Number",
						"Expiration Date - should be displayed next to Masked Card Number", driver);
			}
			
			//Step-11: Verify the functionality of Saved PLCC/non-PLCC card
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("section_Selected_Non_PLCC_Card", "border", "3px solid", checkoutPage),
					"Clicking anywhere on a Saved PLCC/non-PLCC should border the selected card with a black outline, informing the User that the selection will be used for payment",
					"Clicking anywhere on a Saved PLCC/non-PLCC should border the selected card with a black outline, informing the User that the selection will be used for payment",
					"Clicking anywhere on a Saved PLCC/non-PLCC should border the selected card with a black outline, informing the User that the selection will be used for payment", driver);
	
			//Step-12: Verify the functionality of CVV Form Field for non-PLCC card
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lbl_Selected_Non_PLCC_Card_CVV_PlaceHolder"), checkoutPage),
					"System should display a text box with 'CVV' as a place holder text",
					"System should display a text box with 'CVV' as a place holder text",
					"System should display a text box with 'CVV' as a place holder text", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("fldCVV", "lbl_Selected_Non_PLCC_Card_CVV_PlaceHolder", "333", checkoutPage),
					"The placeholder text should move above when the User starts typing the data",
					"The placeholder text should disappear when the User starts typing the data",
					"The placeholder text should disappear when the User starts typing the data", driver);
	
			checkoutPage.enterCVV("123");
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextEqualTo("fldCVV", "123", checkoutPage),
					"The User should be able to enter the card's CVV for the card being added",
					"The User should be able to enter the card's CVV for the card being added",
					"The User should be able to enter the card's CVV for the card being added", driver);
	
			checkoutPage.enterCVV("sdf");
			boolean txtCondition = checkoutPage.getEnteredCVV().equals("sdf");
	
			checkoutPage.enterCVV("@#$");
			boolean splCharCondition = checkoutPage.getEnteredCVV().equals("@#$");
	
			Log.softAssertThat(txtCondition == false && splCharCondition == false,
					"The System should allow the User to enter only numerals in this field, Entry of alphabets or special characters should be prohibited",
					"The System should allow the User to enter only numerals in this field",
					"The System should allow the User to enter only numerals in this field", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("fldCVV", "class", "required", checkoutPage),
					"This should be a required field",
					"This is a required field",
					"This is a required field", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("fldCVV", "maxlength", "4", checkoutPage),
					"The length of the CVV should be a minimum of 3 digits and a maximum of 4 digits",
					"The length of the CVV should be a minimum of 3 digits and a maximum of 4 digits",
					"The length of the CVV should be a minimum of 3 digits and a maximum of 4 digits", driver);
	
			checkoutPage.selectSavedCard_PLCC(1);
			Log.message(i++ + ". PLCC payment option selected.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("fldCVV"), checkoutPage),
					"System should not display this field if the Selected Card Type is PLCC",
					"System should not display this field if the Selected Card Type is PLCC",
					"System should not display this field if the Selected Card Type is PLCC", driver);
	
			//Step-13: Verify the functionality of CVV Tool Tip non-PLCC card
			checkoutPage.selectSavedCard_Non_PLCC(1);
			Log.message(i++ + ". Non-PLCC payment option selected.", driver);
	
			checkoutPage.clickCVVToolTip();
			Log.message(i++ + ". Clicked on CVV Tooltip.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("mdlCVVTooltipModal"), checkoutPage),
					"On clicking CVV Tool Tip, a tooltip opens up showing the user where the CVV is on different types of cards.",
					"On clicking CVV Tool Tip, a tooltip opens up showing the user where the CVV is on different types of cards.",
					"On clicking CVV Tool Tip, a tooltip opens up showing the user where the CVV is on different types of cards.", driver);
	
			checkoutPage.clickCloseOnCVVToolTip();
			Log.message(i++ + ". CVV Tooltip closed.", driver);
	
			//Step-14: Verify the display of CVV Message
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkCVVMsg", "fldCVV", checkoutPage),
					"The CVV Message should be displayed next to the CVV Form Field with the text 'Please confirm CVV'.",
					"The CVV Message should be displayed next to the CVV Form Field with the text 'Please confirm CVV'.",
					"The CVV Message should be displayed next to the CVV Form Field with the text 'Please confirm CVV'.", driver);
	
			//Step-15: Verify the functionality of Tender Rebuttal 
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldCVV", "tenderRebuttalMsg", checkoutPage) &&
						checkoutPage.elementLayer.verifyElementColor("tenderRebuttalMsg", "#c5530", checkoutPage),
						"The Tender Rebuttal should be displayed below the CVV entry field (in yellow font color)",
						"The Tender Rebuttal should be displayed below the CVV entry field (in yellow font color)",
						"The Tender Rebuttal should be displayed below the CVV entry field (in yellow font color)", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "tenderRebuttalMsg", "lnkCVVMsg", checkoutPage),
						"The Tender Rebuttal should be displayed to the right of CVV Message ",
						"The Tender Rebuttal should be displayed to the right of CVV Message ",
						"The Tender Rebuttal should be displayed to the right of CVV Message ", driver);
			}
			
			//Step-16: Verify the display of Expired Card
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("lbl_Expired_Non_PLCC_ExpDate", "e603c", checkoutPage),
					"displays expiration date in red text",
					"displays expiration date in red text",
					"displays expiration date in red text", driver);
	
			//Step-17: Verify the functionality of Show all Cards
			Log.softAssertThat(checkoutPage.elementLayer.verifyInsideElementAlligned("lnkShowAll", "savedCardsSection", "bottom", checkoutPage),
					"Show all Cards should be displayed below the all saved card details",
					"Show all Cards should be displayed below the all saved card details",
					"Show all Cards should be displayed below the all saved card details", driver);
	
			checkoutPage.clickOnShowAllCards();
			Log.message(i++ + ". Clicked on Show All Cards.", driver);
			
			checkoutPage.enterCVV(cardVisa[5]);
			Log.message(i++ + ". Entering CVV.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("divHiddenCards", "display", "block", checkoutPage),
					"Clicking on the 'Show All Cards' button will expand the area and display the remaining cards saved by the User",
					"Clicking on the 'Show All Cards' button will expand the area and display the remaining cards saved by the User",
					"Clicking on the 'Show All Cards' button will expand the area and display the remaining cards saved by the User", driver);
	
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	}
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP4_C22522(String browser) throws Exception {
		//Need an user account with PLCC & Non-PLCC Accounts
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = prdData.get("prd_low_cost");
		String username = accountData.get("credential_plcc_non-plcc2").split("\\|")[0];
		String password = accountData.get("credential_plcc_non-plcc2").split("\\|")[1];
		
		//Create web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to My Account.", driver);
			
			PdpPage pdpPage = myAcc.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBag();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			ShoppingBagPage cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Cart Page.", driver);
	
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to CheckoutPage.", driver);
	
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", "Ground", "valid_address7");
			Log.message(i++ + ". Shipping Details filled successfully.", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page.", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lbl_First_PLCC_Card_Name_Mobile", "logo_First_PLCC_Card", checkoutPage),
						"Card name - should be displayed next to Card Logo",
						"Card name - should be displayed next to Card Logo",
						"Card name - should be displayed next to Card Logo", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lbl_First_PLCC_Card_Name", "logo_First_PLCC_Card", checkoutPage),
						"Card name - should be displayed next to Card Logo",
						"Card name - should be displayed next to Card Logo",
						"Card name - should be displayed next to Card Logo", driver);
			}
	
			checkoutPage.selectSavedCard_PLCC(1);
			Log.message(i++ + ". First PLCC Card Selected.", driver);
	
			checkoutPage.continueToReivewOrder();
			Log.message(i++ + ". Continued to Order Review Section.", driver);
	
			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on Place Order button.", driver);
	
			//Step-18: Verify that user can place an order with a saved non PLCC card.
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("orderConfirmation"), checkoutPage),
					"User can successfully place an order with a saved PLCC card.",
					"User can successfully place an order with a saved PLCC card.",
					"User can successfully place an order with a saved PLCC card.", driver);
	
			//Step-19: Verify that user can place an order with a saved PLCC card.
			homePage = checkoutPage.headers.navigateToHome();
			Log.message(i++ + ". Navigated to Home Page.", driver);
	
			pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for :: " + pdpPage.getProductName(), driver);
	
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.addProductToBag();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Cart Page.", driver);
	
			checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to CheckoutPage.", driver);
	
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", "Ground", "valid_address7");
			Log.message(i++ + ". Shipping Details filled successfully.", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page.", driver);
	
			checkoutPage.selectSavedCard_Non_PLCC(1);
			Log.message(i++ + ". First Non-PLCC Card Selected.", driver);
	
			checkoutPage.enterCVV("333");
			Log.message(i++ + ". CVV Filled Successfully.", driver);
	
			checkoutPage.continueToReivewOrder();
			Log.message(i++ + ". Continued to Order Review Section.", driver);
	
			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on Place Order button.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("orderConfirmation"), checkoutPage),
					"User can successfully place an order with a saved nonPLCC card.",
					"User can successfully place an order with a saved nonPLCC card.",
					"User can successfully place an order with a saved nonPLCC card.", driver);
	
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	}
}// search
