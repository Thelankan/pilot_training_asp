package com.fbb.testscripts.drop3;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C21663 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C21663(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("qc_valid_promo_callout");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!",driver);
	
			QuickOrderPage quickOrderPage=homePage.footers.navigateToQuickOrder();
			Log.message(i++ + ". Navigated to Quick order Page!", driver);
	
			quickOrderPage.searchItemInQuickOrder(searchKey);
			Log.message(i++ + ". Search with '"+searchKey+"' in quick order page", driver);
	
			//1 Verify the Component of product display information in the Quick order page
			List<String> elementsToBeVerified_ProductName = null;
			if(runPltfrm.equals("mobile")){
				quickOrderPage.scrollToShopNow();
				quickOrderPage.tapShopNow();
				elementsToBeVerified_ProductName = Arrays.asList("txtProductNameMobileNew","txtPrice","divApplicableVariation","divSizeChart","divPromotionCallout");
			}else{
				elementsToBeVerified_ProductName = Arrays.asList("txtProductName","txtPrice","divApplicableVariation","divSizeChart","divPromotionCallout");	
			}
	
			Log.softAssertThat(quickOrderPage.elementLayer.VerifyPageElementDisplayed(elementsToBeVerified_ProductName, quickOrderPage),
					"Product Name, Price, Variations, Size Chart, Promotion Callout message should be displayed.",
					"All the above mentioned items displayed!",
					"Items not displayed as Expected!", driver);
	
			//To verify Default color variation is not selected 
	
			Log.softAssertThat(quickOrderPage.verifydefaultSelectedcolorSwatchesQuickOrder(),
					"Default Color variation should be selected in the Quick Order Page",
					"Default color variation is selected", 
					"Default Color Variation is not selected", driver);
	
			Log.softAssertThat(!quickOrderPage.verifydefaultSelectedsizeSwatchesQuickOrder(),
					"By Default No size variation should be selected in the Quick Order Page",
					"By Default No size variation is not selected as expected", 
					"By Default No size Variation is selected", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP2_C21663


}// search
