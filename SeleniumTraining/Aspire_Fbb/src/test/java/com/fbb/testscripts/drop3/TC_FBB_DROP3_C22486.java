package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C22486 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "low", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C22486(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prodId = prdData.get("ps_special-product-set").split("\\|")[0];
		String breadCrumb_primary_category = prdData.get("ps_special-product-set_primary-category");
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			if(runPltfrm.equals("mobile")) {
				PdpPage pdpPage = homePage.headers.navigateToPDP(prodId);
				String breadCrumbText = headers.getBreadCrumbText();
	
				breadCrumbText = headers.getBreadCrumbText();
	
				Log.softAssertThat(breadCrumbText.toLowerCase().trim().contains(breadCrumb_primary_category.toLowerCase().trim()), 
						"Breadcrumb should have the product primary brand name", 
						"Breadcrumb is having product primary brand name", 
						"Breadcrumb is not having product primary brand name", driver);
	
				//Step 2
				//Log.reference("Step 2 - Product Rating, referring 19726 test script");
	
				//Step 3
				String colorToVerify = pdpPage.selectSingleProductComponentColorBasedOnIndex(1, 0);
				Log.softAssertThat(pdpPage.verifyTheSelectedColorImageIsLoadedSplProductSet(colorToVerify,1),
						"The product image should loaded based on the color selected!",
						"The product image loaded based on the color!",
						"The product image is not loading based on the color!", driver);
	
				//Step 4
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtSpecialProductSetPrice"), pdpPage),
						"The price for the special product set should be displayed",
						"The price for the special product set is displayed",
						"The price for the special product set is not displayed", driver);
	
				Log.softAssertThat(pdpPage.verifySpecialProductSetPriceDisplayed(),
						"The product price should be displayed properly!",
						"The product price is displayed properly!",
						"The product price is not displayed properly!", driver);
				//Step 5
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("splProductSetComponentImage"), pdpPage),
						"The product component image should be displayed properly!",
						"The product component image is displayed properly!",
						"The product component image is not displayed properly!", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("splProductSetColorSwatches"), pdpPage),
						"The product component color swatch should be displayed properly!",
						"The product component color swatch is displayed properly!",
						"The product component color swatch is not displayed properly!", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("splProductSetSizeSwatches"), pdpPage),
						"The product component size swatch should be displayed properly!",
						"The product component size swatch is displayed properly!",
						"The product component size swatch is not displayed properly!", driver);
	
				Log.softAssertThat(pdpPage.verifySpecialProductSetColorPreSelected(),
						"The product component color should be selected defaultly!",
						"The product component color is preselected!",
						"The product component color is not preselected!", driver);
				
				//Need atleast one variation should be out of stock
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("disableColorOutOfStock"), pdpPage),
						"The color should be disabled when the variation is out of stock!",
						"The color is disabled when the variation is out of stock!",
						"The color is not disabled when the variation is out of stock!", driver);
	
				Log.softAssertThat(pdpPage.verifyIfSelectedColorHasBorderInSplPrdSet(1),
						"The selected color swatch should have a border!",
						"The selected color swatch has a border!",
						"The selected color swatch does not have a border!", driver);
	
				pdpPage.mouseHoverAddToBagSPS();
				Log.message(i++ + ". Mouse hovered on Add to Bag button in Special Product Set.", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyAttributeForElement("btnAddToBagPrdSet", "disabled", "true", pdpPage),
						"The select size error message should be displayed!",
						"The select size error message is displayed and the button is disabled!",
						"The select size error message is not displayed and the button is enabled!", driver);
	
				pdpPage.selectSizeSwatchSpecialProductSet(2, 0);
				Log.message(i++ + ". Selected a size swatch!", driver);
	
				Log.softAssertThat(pdpPage.verifySelectSizeErrorForSpecialProductSet() == false,
						"The select size error message should not be displayed!",
						"The select size error message is not displayed!",
						"The select size error message is displayed!", driver);
	
				//verify size out of stock - start
				pdpPage.selectSizeSwatchSpecialProductSet(1, 0);
				Log.message(i++ + ". Selected a size swatch!", driver);
	
				pdpPage.selectColorSwatchSpecialProductSet(1, 1);
				Log.message(i++ + ". Selected a color swatch!", driver);
				
				String outstock = prdData.get("ps_special-product-set-out-of-stock");
				
				String prdId=outstock.split("\\|")[0];
				String colorSwatch=outstock.split("\\|")[1];
				String sizeSwatch=outstock.split("\\|")[2];
				
				Log.softAssertThat(pdpPage.verifySizeIsOutOfStock(prdId,colorSwatch),
						"The size should be out of stock!",
						"The size is out of stock!",
						"The size is not out of stock!", driver);
				
				Log.softAssertThat(pdpPage.verifyColorIsOutOfStock(prdId,sizeSwatch),
						"The color should be out of stock!",
						"The color is out of stock!",
						"The color is not out of stock!", driver);
	
				//Step 6
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtPromoSpecialProductSet"), pdpPage),
						"The special product set components should have promotion call out message!",
						"The special product set components have promotion call out message!",
						"The special product set components is not having promotion call out message!", driver);
	
				//Step 7
				Log.reference("Step 7 - reffered in TC - 19731");
	
				//Step 8
				Log.softAssertThat(pdpPage.getSelectedQuantityValue().equals("1"),
						"The default value of Quantity dropdown should be 1!",
						"The default value of Quantity dropdown is 1!",
						"The default value of Quantity dropdown is not 1!", driver);
	
				Log.softAssertThat(pdpPage.verifyAddToBagButtonIsRightOfQuantityDropdown(),
						"The Quantity dropdown should be displayed to the left of Add To Bag!",
						"The Quantity dropdown is displayed to the left of Add To Bag!",
						"The Quantity dropdown is not displayed to the left of Add To Bag!", driver);
	
				Log.softAssertThat(pdpPage.verifyQtyDrp(),
						"The Quantity dropdown should display values from 1 to 10!",
						"The Quantity dropdown displays values from 1 to 10!",
						"The Quantity dropdown does not display values from 1 to 10!", driver);
	
				String value = pdpPage.selectQuantity(5);
				Log.message(i++ + ". Selected Quantity!", driver);
	
				Log.softAssertThat(pdpPage.getSelectedQuantityValue().equals(value),
						"The selected value of Quantity dropdown should be displayed!",
						"The selected value of Quantity dropdown is displayed!",
						"The selected value of Quantity dropdown is not displayed!", driver);
	
				pdpPage.clickCloseInShippingSection();
				Log.message(i++ + ". Collapsed more info!", driver);
				//Step 10
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("divSocialIcons"), pdpPage),
						"The Social Icons should be displayed!",
						"The Social Icons are displayed!",
						"The Social Icons are not displayed!", driver);
	
				Log.softAssertThat(pdpPage.verifyFacebookShare(),
						"Facebook page should be opened!",
						"Facebook page is opened!",
						"Facebook page is not opened!", driver);
	
				Log.softAssertThat(pdpPage.verifyPinterestShare(),
						"Pinterest page should be opened!",
						"Pinterest page is opened!",
						"Pinterest page is not opened!", driver);
	
				Log.softAssertThat(pdpPage.verifyTwitterShare(),
						"Twitter page should be opened!",
						"Twitter page is opened!",
						"Twitter page is not opened!", driver);
				//Step 11
				Log.reference("Step 11 is done in Test case C19689.");
	
				//Step 12
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkDetailsTab"), pdpPage),
						"The Details tab should be open by default!",
						"The Details tab is open by default!",
						"The Details tab is not open by default!", driver);
	
				//Step 13
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkReviewsTab"), pdpPage),
						"The Reviews tab should be displayed!",
						"The Reviews tab is displayed!",
						"The Reviews tab is not displayed!", driver);
	
				pdpPage.clickReviewsTab();
				Log.message(i++ + ". Clicked Review tab!", driver);
	
				Log.softAssertThat(pdpPage.verifyReviewsTabOpened(),
						"The Reviews tab should be opened!",
						"The Reviews tab is opened!",
						"The Reviews tab is not opened!", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("lnkReviewsTab", "divDetailsContent", pdpPage),
						"The Reviews tab should be displayed above the content!",
						"The Reviews tab is displayed above the content!",
						"The Reviews tab is not displayed above the content!", driver);
	
				//Step 14
				Log.reference("Step 14 is done in Test case C21581.");
				
				pdpPage = homePage.headers.navigateToPDP(prodId);
	
				//Step 8 continuing
				pdpPage.selectAllSplProductSetSizeBasedOnIndex(0);
	
				if(pdpPage.verifySpecialProductSetColorPreSelected()) {
					pdpPage.selectAllSplProductSetColorBasedOnIndex(0);
				}
				
				//Step 9
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtShippingExpand"), pdpPage),
						"The Shipping & Returns section should be open by default!",
						"The Shipping & Returns section is open by default!",
						"The Shipping & Returns section is not open by default!", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnAddToBagSpecialProductSet", "txtShippingAndReturnInfo", pdpPage) &&
						pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtShippingAndReturnInfo", "divSocialIcons", pdpPage),
						"The Shipping & Returns section should displayed between Add to bag and Social icons!",
						"The Shipping & Returns section is displayed between Add to bag and Social icons!",
						"The Shipping & Returns section is not displayed between Add to bag and Social icons!", driver);
	
				pdpPage.addToBagSpecialProductSet();
	
				Log.softAssertThat(pdpPage.checkSplProdSetCompAddedInSamePageOfMiniCart(),
						"Check the Special product set mini cart contains all the component product in line!",
						"The Special product set mini cart contains all the component product in line!",
						"The Special product set mini cart not containing all the component product in line!", driver);
				
				Log.testCaseResult();
			}
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_FBB_DROP2_C22486


}// search
