package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22573 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22573(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String credential= AccountUtils.generateEmail(driver) + "|test123@";
	
		//Pre-requisite - Account Should have more than one payment information
		{
			GlobalNavigation.registerNewUser(driver, 1, 0, credential);
		}
	
		int i = 1;
		try {	
			Object[] obj = GlobalNavigation.checkout(driver, searchKey, i, credential);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			checkoutPage.checkUncheckUseThisAsBillingAddress(false);
			Log.message("Unchecked ");
	
			checkoutPage.continueToPayment(false);
			Log.message(i++ + ". Continued to Billing Address.", driver);
	
			//1
			checkoutPage.selectValueFromSavedBillingAddressesDropdownByIndex(1);
			Log.message(i++ + ". Selected New Address From Dropdown", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkEditPaymentDetails", "lblBillingAddressHeading", checkoutPage),
					"Edit link should be displayed to the right side of the billing address label.", 
					"Edit link is displayed to the right side of the billing address label.",
					"Edit link is not displayed to the right side of the billing address label.", driver);
	
			checkoutPage.clickEditLinkInBillingSection();
			Log.message(i++ + ". Clicked on Edit Link in Billing Section", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("sectionSaveAddressWithFields"), checkoutPage), 
					"On clicking the Edit link, the System should display the 'Add New Address' form pre-filled with the User selected billing address data",
					"On clicking the Edit link, the System display the 'Add New Address' form pre-filled with the User selected billing address data",
					"On clicking the Edit link, the System not display the 'Add New Address' form pre-filled with the User selected billing address data", driver);
	
			checkoutPage.clickCancelInBillingDetails();
	
			//2
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkAddNewBilling", "lnkEditPaymentDetails", checkoutPage),
					"The 'Add New' link should be displayed next to edit link, separated by a pipe (|) symbol", 
					"The 'Add New' link is displayed next to edit link, separated by a pipe (|) symbol",
					"The 'Add New' link is not displayed next to edit link, separated by a pipe (|) symbol", driver);
	
			checkoutPage.clickAddNewBillingAddress();
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("sectionSaveAddressWithFields"), checkoutPage), 
					"On clicking the 'Add New' link, the System should display the 'Add New Address' form with no data pre-filled.",
					"On clicking the 'Add New' link, the System is display the 'Add New Address' form with no data pre-filled.",
					"On clicking the 'Add New' link, the System is not display the 'Add New Address' form with no data pre-filled.", driver);
	
			checkoutPage.clickCancelInBillingDetails();
			//3
	
			if(Utils.isMobile())
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtSavedAddressCount", "addressSelectorListMobile", checkoutPage), 
						"Address Selector should be displayed below the Saved Addresses Count (if available) after a faint line break or below the Billing Address Heading", 
						"Address Selector is displayed below the Saved Addresses Count (if available) after a faint line break or below the Billing Address Heading", 
						"Address Selector is not displayed below the Saved Addresses Count (if available) after a faint line break or below the Billing Address Heading", driver);
			}
			else
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtSavedAddressCount", "addressSelectorList", checkoutPage), 
						"Address Selector should be displayed below the Saved Addresses Count (if available) after a faint line break or below the Billing Address Heading", 
						"Address Selector is displayed below the Saved Addresses Count (if available) after a faint line break or below the Billing Address Heading", 
						"Address Selector is not displayed below the Saved Addresses Count (if available) after a faint line break or below the Billing Address Heading", driver);
			}
	
			checkoutPage.selectValueFromSavedBillingAddressesDropdownByIndex(1);
			Log.message(i++ + ". Selected New Address From Dropdown", driver);
	
			Log.softAssertThat(!checkoutPage.getSelectedOptionBilling().equals("New Address"), 
					"Place holder text 'New Address' should not be displayed inside the Address Selector once the User has selected the billing address from the address selector drop down",
					"Place holder text 'New Address' not displayed inside the Address Selector once the User has selected the billing address from the address selector drop down",
					"Place holder text 'New Address' displayed inside the Address Selector once the User has selected the billing address from the address selector drop down", driver);
	
			//4
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblBillingAddressHeading", "txtSavedAddressCount", checkoutPage), 
					"Saved Addresses Count should be displayed below the Billing Address Heading.", 
					"Saved Addresses Count is displayed below the Billing Address Heading.", 
					"Saved Addresses Count is not displayed below the Billing Address Heading", driver);
	
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C22573
}// search
