package com.fbb.testscripts.drop6;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP6_C22790 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP6_C22790(String browser) throws Exception {

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo();

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			GiftCardPage giftCardPage = homePage.footers.navigateToGiftCardPage();
			Log.message(i++ + ". Navigated to Gift Card Page!", driver);

			giftCardPage.clickOnbalanceCheckUpButton();
			Log.message(i++ + ". Gift Card Balance Lookup overlay opened!", driver);
			
			String[] giftCardDetail = prdData.getProperty("gift_card_valid_1").split("\\|");
			
			giftCardPage.ApplyCardDetails(giftCardDetail[0],giftCardDetail[1]);
			Log.message(i++ + ". Entering Valid Card and PIN number");
			
/*			if(Utils.isMobile()) {*/
				Log.softAssertThat(giftCardPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtFieldCardPin", "totalBalanceMsg", giftCardPage), 
						"The Gift card balance should be displayed below PIN",
						"The Gift card balance is below PIN",
						"The Gift card balance is not below PIN", driver);
/*			} else {
				Log.softAssertThat(giftCardPage.elementLayer.verifyCssPropertyForElement("sectionGiftCardBalance", "float", "left", giftCardPage), 
						"The Gift card balance should aligned left",
						"The Gift card balance is aligned left",
						"The Gift card balance is not aligned left", driver);
			}*/
			
			Log.softAssertThat(giftCardPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "applyButton", "totalBalanceMsg", giftCardPage), 
					"The Gift card available balance text should display below Apply button",
					"The Gift card available balance text is display below Apply button",
					"The Gift card available balance text is not display below Apply button", driver);
			
/*			Log.softAssertThat(giftCardPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtAvailableBalance", "txtMaskedNumber", giftCardPage), 
					"The Gift card available balance text should display above masked card number",
					"The Gift card available balance text is display above masked card number",
					"The Gift card available balance text is not display above masked card number", driver);*/
			
			Log.softAssertThat(giftCardPage.elementLayer.verifyElementTextContains("totalBalance", "$", giftCardPage) , 
					"The Gift card available amount should display $ symbol",
					"The Gift card available amount is displaying $ symbol",
					"The Gift card available amount is not displaying $ symbol", driver);

			/*Log.softAssertThat(giftCardPage.verifyCardNumberIsMasked(),
					"The Gift card number is masked and last four digits should displaying",
					"The Gift card number is masked and last four digits is displaying",
					"The Gift card number is masked and last four digits is not displaying", driver);
			
			if(Utils.isDesktop()) {
				Log.softAssertThat(giftCardPage.elementLayer.verifyAttributeForElement("imgGiftCardResultDesktop", "src", Utils.getCurrentBrand() + "/gift-card-balance", giftCardPage),
						"The Brand specific background image should be displayed in the Gift card result page",
						"The Brand specific background image is displayed in the Gift card result page",
						"The Brand specific background image is not displayed in the Gift card result page", driver);
			} else if (Utils.isTablet()) {
				Log.softAssertThat(giftCardPage.elementLayer.verifyAttributeForElement("imgGiftCardResultTablet", "src", Utils.getCurrentBrand() + "/gift-card-balance", giftCardPage),
						"The Brand specific background image should be displayed in the Gift card result page",
						"The Brand specific background image is displayed in the Gift card result page",
						"The Brand specific background image is not displayed in the Gift card result page", driver);
			} else {
				Log.softAssertThat(giftCardPage.elementLayer.verifyAttributeForElement("imgGiftCardResultMobile", "src", Utils.getCurrentBrand() + "/gift-card-balance", giftCardPage),
						"The Brand specific background image should be displayed in the Gift card result page",
						"The Brand specific background image is displayed in the Gift card result page",
						"The Brand specific background image is not displayed in the Gift card result page", driver);
			}
			
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("lnkAnotherCard"), giftCardPage),
					"The Another card link should get displayed",
					"The Another card link is displayed",
					"The Another card link is not displayed", driver);
			
			if (Utils.isMobile()) {
				Log.softAssertThat(giftCardPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtNoteSection", "lnkAnotherCard", giftCardPage) && 
						!(giftCardPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "sectionGiftCardBalance", "lnkAnotherCard", giftCardPage)),
						"The Another card link should get displayed below the Note section and is center aligned",
						"The Another card link is displayed below the Note section and is center aligned",
						"The Another card link is not displayed below the Note section and is center aligned", driver);
			} else {
				Log.softAssertThat(giftCardPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtNoteSection", "lnkAnotherCard", giftCardPage) && 
						giftCardPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "sectionGiftCardBalance", "lnkAnotherCard", giftCardPage),
						"The Another card link should get displayed below the Note section and left to the balance return template",
						"The Another card link is displayed below the Note section and left to the balance return template",
						"The Another card link is not displayed below the Note section and left to the balance return template", driver);
			}*/
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_HEADER_C22790

}//TC_FBB_DROP6_C22783
