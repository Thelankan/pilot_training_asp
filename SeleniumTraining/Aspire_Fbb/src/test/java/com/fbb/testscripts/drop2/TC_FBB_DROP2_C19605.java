package com.fbb.testscripts.drop2;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19605 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19605(String browser) throws Exception {
		Log.testCaseInfo();

		String testData = prdData.get("no_search_result");
		String searchKey = testData.split("\\|")[0];

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message(i++ + ". Navigated to Search Results page!");

			//Step-1: Verify searching with invalid terms
			Log.softAssertThat(searchResultPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkDidYouMeanSuggestion", "popularSearches", searchResultPage), 
					"Popular searches should display below the \"did you mean\"", 
					"Popular searches displayed below the \"did you mean\"", 
					"Popular searches not displayed below the \"did you mean\"", driver);
			
			Log.softAssertThat(searchResultPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "popularSearches", "btnShopNewArrivals", searchResultPage), 
					"\"Shop New Arrivals\" button should display under Popular searches", 
					"\"Shop New Arrivals\" button displayed under Popular searches", 
					"\"Shop New Arrivals\" button is not displayed under Popular searches", driver);
			
			Log.softAssertThat(searchResultPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "popularSearches", "divTrendingNow", searchResultPage), 
					"\"Trending Now\" products should be displayed below Popular Searches", 
					"\"Trending Now\" products is displayed below Popular Searches", 
					"\"Trending Now\" products is not displayed below Popular Searches", driver);

			//Step-2: Verify search result page after clicking popular searches
			searchResultPage.clickDidYouMeanSuggestionLink();
			Log.message(i++ + ". Clicked on Did You Mean link.", driver);
			Log.softAssertThat(new SearchResultPage(driver).get().getPageLoadStatus(),
					"Search Result Page should be displayed.",
					"Search Result Page displayed.",
					"Something went wrong.", driver);

			//Shop New Arrivals validation ignored due to content asset data issue.
			if(Utils.isMobile())
			{
				searchResultPage = homePage.headers.searchProductKeyword(searchKey);
				Log.message(i++ + ". Navigated to Search Results page!");
			}
			else
			{
				searchResultPage = homePage.headers.searchProductKeyword(searchKey);
				Log.message(i++ + ". Navigated to Search Results page!");
			}

			searchResultPage.clickShopNewArrivalsButton();
			Log.message(i++ + ". Clicked on Shop New Arrivals button!", driver);

			Log.softAssertThat((!searchResultPage.elementLayer.isElementsDisplayed(Arrays.asList("btnShopNewArrivals"), searchResultPage)) &&
					searchResultPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("sectionSearchResult"), searchResultPage),
					"The Search result page should be displayed!",
					"The Search result page is displayed!",
					"The Search result page is not displayed!", driver);
			
			searchResultPage = homePage.headers.searchProductKeyword(searchKey);
			Log.message(i++ + ". Navigated to Search Results page!");
			
			PdpPage pdpPg = searchResultPage.clickTrendingImage();
			
			Log.softAssertThat(pdpPg.elementLayer.verifyPageElements(Arrays.asList("cntPdpContent"), pdpPg), 
					"Page should navigated to PDP page after clicking the trending image!!!", 
					"Page is navigated to PDP page after clicking the trending image!!!", 
					"Page is not navigated to PDP page after clicking the trending image!!!", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_DROP2_C19605


}// search


