package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19723 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	@Test(groups = { "medium", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19723(String browser) throws Exception {
		Log.testCaseInfo();
		String productSet = prdData.getProperty("ps_product-set");
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(productSet);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedleft("cntPdpContent", "lblPrimaryBreadCrumbDesktop", pdpPage),
					"Check the breadcrumb is displayed in left side of the product details page",
					"The breadcrumb is displayed in left side of the product details page",
					"The breadcrumb is not displayed in left side of the product details page", driver);
			
			pdpPage.clickOnHomeBC();
			Log.message(i++ + ". Navigated to Home page");
			
			Log.softAssertThat(new HomePage(driver).get().getPageLoadStatus(),
					"If user tabs/click on home in breadcrumb, system should navigate user to the homepage",
					"If user tabs/click on home in breadcrumb, system navigate user to the homepage",
					"If user tabs/click on home in breadcrumb, system not navigate user to the homepage", driver);
			
			pdpPage = homePage.headers.navigateToPDP(productSet);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			String category = pdpPage.getBreadCrumbLevelText(1);
			
			PlpPage plpPage = pdpPage.headers.navigateToLastSubCategory();
			Log.message(i++ + ". Navigated category :: " + category);
			
			if (Utils.isMobile()) {
				Log.softAssertThat(plpPage.elementLayer.verifyElementTextContains("lblCategoryNameMobile", category, plpPage),
						"if user tabs/click on any other category link, system should navigate user to the selected category page.",
						"if user tabs/click on any other category link, system navigate user to the selected category page.",
						"if user tabs/click on any other category link, system not navigate user to the selected category page.", driver);
			} else {
				Log.softAssertThat(plpPage.elementLayer.verifyElementTextContains("divCurrentBreadCrumbDesktopTablet", category, plpPage),
						"if user tabs/click on any other category link, system should navigate user to the selected category page.",
						"if user tabs/click on any other category link, system navigate user to the selected category page.",
						"if user tabs/click on any other category link, system not navigate user to the selected category page.", driver);
			}
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP3_C19723

	@Test(groups = { "medium", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP3_C19723(String browser) throws Exception {
		Log.testCaseInfo();
		String productSet = prdData.getProperty("ps_product-set");
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(productSet);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			if(Utils.getCurrentBrand().equals(Brand.rm))
			{
				Log.softAssertThat(pdpPage.elementLayer.verifyElementColor("bcLastCategoryNameDesktopTablet", "000", pdpPage),
						"Product name in breadcrumb should be displayed in different color",
						"Product name in breadcrumb displayed in different color",
						"Product name in breadcrumb not displayed in different color", driver);
			}
			else if(Utils.getCurrentBrand().equals(Brand.ww))
			{
				Log.softAssertThat(pdpPage.elementLayer.verifyElementColor("bcLastCategoryNameDesktopTablet", "aaaaaa", pdpPage),
						"Product name in breadcrumb should be displayed in different color",
						"Product name in breadcrumb displayed in different color",
						"Product name in breadcrumb not displayed in different color", driver);
			}
			
			if(Utils.isDesktop()) {
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgRatingStarsPrdSet", "txtProductSetNameDesktop", pdpPage),
					"Product name should be displayed below product ratings",
					"Product name displayed below product ratings",
					"Product name not displayed below product ratings", driver);
			} else {
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgRatingStarsPrdSet_Tab_Mob", "txtProductSetNameTablet", pdpPage),
					"Product name should be displayed below product ratings",
					"Product name displayed below product ratings",
					"Product name not displayed below product ratings", driver);
			}
			
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForPsuedoListElement("lstBreadCrumbLinksDesktopTablet", "after", "content", "/", pdpPage),
					"Categories in the brand category root should be delimited with the symbol ' / '",
					"Categories in the brand category root delimited with the symbol ' / '",
					"Categories in the brand category root not delimited with the symbol ' / '", driver);
	
			if(!Utils.isMobile()){
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodBadge"), pdpPage) &&
						(pdpPage.getXLocOfBadge()==pdpPage.getXLocOfPrimaryImg()),
						"Badge should be displayed & located in left corner",
						"Badge should be displayed & located in left corner",
						"Badge should be displayed & located in left corner", driver);
			}
	
			if(Utils.isDesktop() && !(Utils.getRunBrowser(driver).equalsIgnoreCase("internet explorer") || Utils.getRunBrowser(driver).equalsIgnoreCase("safari"))) {
				pdpPage.zoomProductImage();
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("zoomLensProdImg","zoomedProdImg"), pdpPage),
						"To check the transperant overlay of image is displaying or not.",
						"The image overlay is displaying.",
						"The image overlay is not displaying.", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.verifyElementsAreInSameRow("mainProdImage", "zoomedProdImg", pdpPage),
						"To check the original product image and zoomed image are aligned in same row.",
						"The original product image and zoomed product image are in same row!",
						"The original product image and zoomed product image are not in same row", driver);
			}
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP3_C19723

	@Test(groups = { "medium", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP3_C19723(String browser) throws Exception {
		Log.testCaseInfo();
		String productSet = prdData.getProperty("ps_product-set");
		boolean isVideoProductSet = false;
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(productSet);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			if(isVideoProductSet){
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkVideoMain"), pdpPage),
						"Video Icon should be displayed.",
						"Video icon is displayed",
						"video icon not displayed", driver);
	
				pdpPage.clickMainVideoLink();
	
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("prdVideoContainer"), pdpPage),					 
						"'Video Player' should be displayed", 
						"'Video Player' is displayed and it is taken over product main image", 
						"'Video Player' is not displayed in the place of product image", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("videoPlayer"), pdpPage),
						"To check the product image is overtaken the product main image.",
						"The product image is overtaken the product main image",
						"The product image is not overtaken the product main image", driver);
			}else{
				Log.reference("Given product is not configured with video.");
			}
	
			if(Utils.isDesktop()) {
				int NoOfThumbProdImg = pdpPage.getNoOfThumbnailProdImages();
				if (NoOfThumbProdImg > 4) {
					Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnPrevImageDisable","btnNextImageEnable"), pdpPage),
							"To check the arrows displaying for product image thumbnails.",
							"The 'Prev arrow', 'Next arrow' are displaying when the product have 5 or more thumbnail images",
							"The arrow is not displaying even the thumbnails are having 5 or more product images", driver);
		
					Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnPrevImageDisable"), pdpPage),
							"To check the previous arrow is disabled in the begining of image sequence.",
							"The 'Prev arrow' is displaying and disabled in the begining of image sequence.",
							"The 'Prev arrow' is not disabled", driver);
		
					Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnNextImageDisable"), pdpPage),
							"To check the next arrow is enabled in the begining of image sequence.",
							"The 'Next arrow' is displaying and enabled in the begining of image sequence.",
							"The 'Next arrow' is not enabled", driver);
				
					pdpPage.clickOnSpecifiedAlternateImage(5);
		
					Log.softAssertThat(pdpPage.verifyAlternateImageDisplayStatus(5, "false"),
							"To check the half hidden alternate image is displayed fully when the image is clicked.",
							"The alternate image is displaying fully.",
							"The alternate image is not displaying fully", driver);
		
					pdpPage.scrollAlternateImageInSpecifiedDirection("Next");
		
					Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnNextImageDisable"), pdpPage),
							"To check the next arrow is disabled when the alternate image sequence reached the end.",
							"The 'Next arrow' is displaying and disabled in the end of image sequence.",
							"The 'Next arrow' is not disabled", driver);
		
					Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnPrevImageDisable"), pdpPage),
							"To check the Prev arrow is enabled at the end of image sequence.",
							"The 'Prev arrow' is displaying and enabled at the end of image sequence.",
							"The 'Prev arrow' is not enabled", driver);
		
				} else {
					Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnPrevImageDisable","btnNextImageEnable"), pdpPage),
							"To check the arrows displaying for product image thumbnails.",
							"The product only have 4 or less thumbnail images, thus the arrows are not displayed",
							"The arrow is displaying even the product only have 4 or less thumbnail images", driver);
				}
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP3_C19723
	
	@Test(groups = { "medium", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP3_C19723(String browser) throws Exception {
		Log.testCaseInfo();
		String productSet = prdData.getProperty("ps_product-set");
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(productSet);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			if (Utils.isDesktop()) {
				
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver,"divReviewAndRatingPrdSet", "txtProductSetNameDesktop", pdpPage),
						"The rating must be displayed above the product name!",
						"The rating is displayed above the product name!",
						"The rating is not displayed above the product name!", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divReviewAndRatingPrdSet", "imgProductPrimaryImage", pdpPage),
						"The rating must be displayed to the right of product image!",
						"The rating is displayed to the right of product image!",
						"The rating is not displayed to the right of product image!", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkReviewsPrdSet", "imgRatingStarsPrdSet", pdpPage),
						"The rating must be displayed to the left of number of reviews text!",
						"The rating is displayed to the left of number of reviews text!",
						"The rating is not displayed to the left of number of reviews text!", driver);
				
			} else {
				
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver,"divReviewAndRatingPrdSet_Tab_Mob", "txtProductSetNameTablet", pdpPage),
						"The rating must be displayed above the product name!",
						"The rating is displayed above the product name!",
						"The rating is not displayed above the product name!", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divReviewAndRatingPrdSet_Tab_Mob", "imgProductPrimaryImage", pdpPage) && 
						pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "imgRatingStarsPrdSet_Tab_Mob", "imgProductPrimaryImage", pdpPage),
						"The rating must be displayed to the right of product image!",
						"The rating is displayed to the right of product image!",
						"The rating is not displayed to the right of product image - SM-1411!", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkReviewsPrdSet_Tab_Mob", "imgRatingStarsPrdSet_Tab_Mob", pdpPage),
						"The rating must be displayed to the left of number of reviews text!",
						"The rating is displayed to the left of number of reviews text!",
						"The rating is not displayed to the left of number of reviews text - SM-1411!", driver);
				
			}
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP3_C19723
	
	@Test(groups = { "medium", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M5_FBB_DROP3_C19723(String browser) throws Exception {
		Log.testCaseInfo();
		String productSet = prdData.getProperty("ps_product-set");
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(productSet);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			String[] star = pdpPage.getProductReviewStarInPrdSet();
	
			if(star[1].contains("5") || !(star[0].contains("0"))) {
				Log.softAssertThat(star[1].contains("5") || !(star[0].contains("0")),
						"The product is already reviewed and star should displayed",
						"The product is already reviewed and star is "+star[0]+"."+star[1],
						"The product is not already reviewed", driver);
					
				Log.softAssertThat(pdpPage.elementLayer.VerifyElementDisplayed(Arrays.asList("lblReviewCount"), pdpPage),
						"Check the review count is displaying in the page",
						"The review count is displaying in the page",
						"The review count is not displaying in the page", driver);
				
				pdpPage.clickOnProductReviewCount();
				
				Log.softAssertThat(pdpPage.elementLayer.verifyAttributeForElement("lblReviewCount", "class", "current", pdpPage),
						"Check the review is displaying if we click on the review count",
						"The review is displaying if we click on the review count",
						"The review is not displaying if we click on the review count", driver);
				
			} else {
				Log.softAssertThat(pdpPage.verifyProductReview("BE THE FIRST TO WRITE A REVIEW"), 
						"Product is not reviewed and Be the first to write a review should displayed", 
						"Product is not reviewed and Be the first to write a review is displayed", 
						"Product is not reviewed and Be the first to write a review is not displayed", driver);
			}
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP3_C19723
	
	@Test(groups = { "medium", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M6_FBB_DROP3_C19723(String browser) throws Exception {
		Log.testCaseInfo();
		String productSet = prdData.getProperty("ps_product-set");
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(productSet);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.clickOnReviewTab();
			Log.message(i++ + ". Clicked on Review Tab.", driver);
			
			if (Utils.isDesktop()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductSetNameDesktop", "lblShortDescPrdSet", pdpPage),
						"Check The product set name should display above the Short description",
						"The product set name is displaying above the Short description",
						"The product set name is not displaying above the Short description", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblShortDescPrdSet", "lblMainPriceDetailsPrdSet", pdpPage),
						"Check The product set price should display below the Short description",
						"The product set price is displaying below the Short description",
						"The product set price is not displaying below the Short description", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductSetNameTablet", "lblShortDescPrdSet_Tablet_Mobile", pdpPage),
						"Check The product set name should display above the Short description",
						"The product set name is displaying above the Short description",
						"The product set name is not displaying above the Short description", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblShortDescPrdSet_Tablet_Mobile", "lblMainPriceDetailsPrdSet_Tab_Mob", pdpPage),
						"Check The product set price should display below the Short description",
						"The product set price is displaying below the Short description",
						"The product set price is not displaying below the Short description", driver);
			}
			
			if (!Utils.isMobile()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnTopAddAllToBagPrdSet", "sectionProductSetComponent", pdpPage),
						"Add All to Bag button should be displayed, Immediately before the first component product",
						"Add All to Bag button displayed, Immediately before the first component product",
						"Add All to Bag button not displayed, Immediately before the first component product", driver);
			}

			if (Utils.isDesktop()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblMainPriceDetailsPrdSet", "lblLongDescPrdSet", pdpPage),
						"Check The product set price should display above the Long description",
						"The product set price is displaying above the Long description",
						"The product set price is not displaying above the Long description", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblMainPriceDetailsPrdSet_Tab_Mob", "lblLongDescPrdSet_Tab_Mob", pdpPage),
						"Check The product set price should display above the Long description",
						"The product set price is displaying above the Long description",
						"The product set price is not displaying above the Long description", driver);
			}
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionProductSetComponent", "btnBottomAddAllToBagPrdSet", pdpPage),
					"Add All to Bag button should be displayed, Immediately after the last component product",
					"Add All to Bag button displayed, Immediately after the last component product",
					"Add All to Bag button not displayed, Immediately after the last component product", driver);
			
			if(Utils.isDesktop()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionShareIconsPrdSet", "lblShopTheLookPrdSet", pdpPage),
						"Check The share icon container should display above the shop the look section",
						"The share icon container is displaying above the shop the look section",
						"The share icon container is not displaying above the shop the look section", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "mainProdImage", "lblShopTheLookPrdSet", pdpPage),
						"Check The main product image should display above the shop the look section",
						"The main product image is displaying above the shop the look section",
						"The main product image is not displaying above the shop the look section - SM-1411", driver);
			}
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(pdpPage.getNumberOfAddToAllBtnInPrdSetPdpPage() == 2,
						"Check two 'Add All to Bag' button is getting displayed in the product detail page",
						"Two 'Add All to Bag' button is getting displayed in the product detail page",
						"Two 'Add All to Bag' button is not getting displayed in the product detail page", driver);
			}
			
			//Step-1: Verify Add to Bag overlay is opened.
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblShopTheLookPrdSet", "sectionProductSetComponent", pdpPage),
					"Check the Product Set Component should display below the shop the look section",
					"The Product Set Component is displaying below the shop the look section",
					"The Product Set Component is not displaying below the shop the look section", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionProductSetComponent", "tabReviews", pdpPage),
					"Check the Product Set Component should display above the reviews tab",
					"The Product Set Component is displaying above the reviews tab",
					"The Product Set Component is not displaying above the reviews tab", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("tabReviews", "text-align", "center", pdpPage),
					"Check the review tab is alligned center",
					"The review tab is alligned center",
					"The review tab is not alligned center", driver);
			
			Log.softAssertThat(pdpPage.verifySalePriceHasPriceRangeInPrdSet(),
					"Price range should displays lowest priced item and highest priced item based on the active price",
					"Price range displays lowest priced item and highest priced item based on the active price",
					"Price range not displays lowest priced item and highest priced item based on the active price", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP3_C19723
	
	@Test(groups = { "medium", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M7_FBB_DROP3_C19723(String browser) throws Exception {
		Log.testCaseInfo();
		String productSet = prdData.getProperty("ps_product-set");
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(productSet);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			homePage.footers.navigateToFaceBook();
			Log.message(i++ + ". Clicked on 'Facebook'!", driver);
	
			Log.softAssertThat(driver.getCurrentUrl().contains("facebook"), 
					"Page should be redirected to Facebook Page", 
					"Page redirected to Facebook Page", 
					"Page Not Redirected to Facebook Page", driver);
	
			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);
	
			homePage.footers.navigateToPinterest();
			Log.message(i++ + ". Clicked on 'Pinterest'!", driver);
	
			Log.softAssertThat(driver.getCurrentUrl().contains("pinterest"), 
					"Page should be redirected to Pintrest Page", 
					"Page redirected to Pintrest Page", 
					"Page Not Redirected to Pintrest Page", driver);
	
			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);
	
			homePage.footers.navigateToTwitter();
			Log.message(i++ + ". Clicked on 'Twitter'!", driver);
	
			Log.softAssertThat(driver.getCurrentUrl().contains("twitter"), 
					"Page should be redirected to Twitter Page", 
					"Page redirected to Twitter Page", 
					"Page Not Redirected to Twitter Page", driver);
			
			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP3_C19723
	
	@Test(groups = { "medium", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M8_FBB_DROP3_C19723(String browser) throws Exception {
		Log.testCaseInfo();
		String productSet = prdData.getProperty("ps_product-set");
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.redirectToPDP(productSet, driver);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.selectAllProductSetSizeBasedOnIndex(0);
			Log.message(i++ + ". Selected Size swatch in all products", driver);
			
			pdpPage.clickAddProductToBagInProductSet();
			Log.message(i++ + ". Clicked on 'Add To Bag' Button", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("mdlMiniCartOverLay"), pdpPage), 
					"Add to Bag overlay should be displayed.",
					"Add to Bag overlay displayed!",
					"Add to Bag overlay not displayed.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP3_C19723
}// search
