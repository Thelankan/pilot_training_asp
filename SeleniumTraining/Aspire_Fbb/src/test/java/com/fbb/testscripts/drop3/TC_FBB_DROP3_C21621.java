package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C21621 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	@SuppressWarnings("unused")
	private static String runPlatfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "low", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C21621(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_gc_physical");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to Pdp Page with the product!"+  searchKey, driver);
	
			pdpPage.openCloseToolTipOverlay("open");
			Log.message(i++ + ". Tooltip OverLay for Term&Conditions Opened.",driver);
	
			if(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("toolTipOverLay"), pdpPage)){
				if(pdpPage.getToolTipDiementsion("height")>=260){
					Log.softAssertThat(pdpPage.getToolTipDiementsion("width") >= 330, 
							"Tool tip minimum dimention should be in range of 330 X 260", 
							"Tool tip minimum dimention displayed as expected!", 
							"Tool tip minimum dimention not fit in expected range.", driver);
				}
	
				else if(pdpPage.getToolTipDiementsion("height")>260 && pdpPage.getToolTipDiementsion("height") <= 510){
	
					Log.softAssertThat(pdpPage.getToolTipDiementsion("width") == 330, 
							"when Tool tip hight extent above 260 to 510 then the width should be 330", 
							"when Tool tip hight extent above 260 to 510 then the width is 330 as expected!", 
							"when Tool tip hight extent above 260 to 510 then the width is not 330.", driver);
				}
				else if(pdpPage.getToolTipDiementsion("height")>510 && pdpPage.getToolTipDiementsion("height") <= 600){
	
					Log.softAssertThat(pdpPage.getToolTipDiementsion("width") == 428, 
							"when Tool tip hight extent above 510 to 600 then the width should be 428", 
							"when Tool tip hight extent above 510 to 600 then the width is 428 as expected!", 
							"when Tool tip hight extent above 510 to 600 then the width is not 428", driver);
				}
				else if(pdpPage.getToolTipDiementsion("height") > 600 && pdpPage.getToolTipDiementsion("width") > 428 ){
	
					Log.softAssertThat(!pdpPage.elementLayer.verifyAttributeForElement("toolTipScroller", "style", "display: none", pdpPage), 
							"when Tool tip hight extent above 600 then vertical scroller should be display", 
							"when Tool tip hight extent above 600 then vertical scroller should be display as expected!", 
							"when Tool tip hight extent above 600 then vertical scroller should be display.", driver);
				}
			}
	
			pdpPage.openCloseToolTipOverlay("close");
			Log.message(i++ + ". Tooltip OverLay for Term&Conditions Closed when X button is clicked.",driver);
	
			pdpPage.openCloseToolTipOverlay("open");
			pdpPage.clickOnEmptySpace();
			Log.softAssertThat(pdpPage.elementLayer.verifyAttributeForElement("toolTipOverLay", "style", "display: none", pdpPage) , 
					"Tool tip should be closed when empty space is clicked ", 
					"Tool tip is be closed when empty space is clicked  as expected!", 
					"Tool tip is not closed when empty space is clicked ", driver);
	
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP2_C21621


}// search
