package com.fbb.testscripts.drop3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C21656 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	@SuppressWarnings("unused")
	private static String runPlatfrm = Utils.getRunPlatForm();
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C21656(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String lvl1 = prdData.get("level-2").split("\\|")[0];
		String lvl2 = prdData.get("level-2").split("\\|")[1];
				
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!",driver);
			
			PlpPage plpPage = null;
			
			if(Utils.isDesktop()) {
				plpPage = homePage.headers.navigateTo(lvl1);
			} else {
				plpPage = homePage.headers.navigateTo(lvl1, lvl2);
			}
			Log.message(i++ + ". Navigated to SLP Page with Search Keyword",driver);
			
			String[] prodId = plpPage.getListOfProductId(20);
	
			String[] url = plpPage.getProductUrl(20);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId[0]);
			List<String> prdNames = new ArrayList<String>();
			prdNames.add(pdpPage.getProductName());
			Log.message(i++ + ". Navigated to PDP for Product :: " + prdNames.get(0),driver);
			
			pdpPage.addToBag();
			Log.message(i++ + ". Product added to cart ",driver);
			
			ShoppingBagPage shopBag = pdpPage.headers.clickOnBagLink();
			Log.message(i++ + ". Navigated to cart page",driver);
	
			Log.softAssertThat(shopBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummarySection", "recentlyViewedSection", shopBag),
					"To check the recently viewed section is displayed below the order summary!",
					"The recently viewed section is displayed below the order summary section!",
					"The recently viewed section is not displayed below the order summary section!", driver);
	
			Log.softAssertThat(shopBag.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("recentlyViewedProdPricing", "recentlyViewedProdImage", "recentlyViewedProdName"), shopBag),
					"Check 'Recently Viewed' section have 'Product Name', 'Product price' and 'Product image'",
					"'Recently Viewed' section have 'Product Name', 'Product price' and 'Product image'",
					"'Recently Viewed' section is not having 'Product Name', 'Product price' and 'Product image'", driver);
			
			Log.softAssertThat(shopBag.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("recentlyViewSectionNextArrow","recentlyViewSectionPrevArrow"), shopBag),
					"Check the recently viewed section is not having 'Next' and 'Prev' arrow before visiting 4 product",
					"The recently viewed section is having 'Next' and 'Prev' arrow before visiting 4 product",
					"The recently viewed section is not having 'Next' and 'Prev' arrow before visiting 4 product", driver);
	
			int size = shopBag.getNoOfProdDisplayInRecentView();
			
			Log.softAssertThat(size > 0,
					"To check the recently viewed section is having visited product!",
					"The recently viewed section is having visited product!",
					"The recently viewed section is not having visited product!", driver);
	
			for (int w=2; w< url.length - 3; w++) {
				Utils.openNewTab(driver);
	
				List<String> handle = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(handle.get(handle.size() - 1));
				driver.get(url[w]);
	
				Utils.waitForPageLoad(driver);
			}
	
			
			shopBag = pdpPage.headers.clickOnBagLink();
			Log.message(i++ + ". Navigated to cart page",driver);
	
			size = shopBag.getNoOfProdDisplayInRecentView();
	
			Log.softAssertThat(size == 12,
					"To check the recently viewed section displaying the 12 new viewed product!",
					"The recently viewed section is displaying the 12 new viewed product",
					"The recently viewed section is not displaying the 12 new viewed product!", driver);
	
			if(Utils.isDesktop()) {
				Log.softAssertThat(shopBag.elementLayer.verifyListElementSize("recentlyViewedCurrentProd", 4, shopBag),
						"To check the recently viewed section visible 4 product!",
						"The recently viewed section is visible 4 product",
						"The recently viewed section is not visible 4 product!", driver);
				
				Log.softAssertThat(shopBag.checkNextAndPrevArrowFunction(),
						"Clicking on the right/left arrow should move the carousel and reveal more products!",
						"Clicking on the right/left arrow is moving the carousel and reveal more products",
						"Clicking on the right/left arrow is not moving the carousel and reveal more products", driver);
			}
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(shopBag.elementLayer.VerifyPageElementDisplayed(Arrays.asList("recentlyViewSectionNextArrow","recentlyViewSectionPrevArrow"), shopBag),
						"Check the recently viewed section is having 'Next' and 'Prev' arrow",
						"The recently viewed section is having 'Next' and 'Prev' arrow",
						"The recently viewed section is not having 'Next' and 'Prev' arrow", driver);
			}
			
			if(Utils.isDesktop()) {
				Log.softAssertThat(shopBag.checkQuickShopIsDisplayedInRecentlyViewed(),
						"The System should display the 'QUICKSHOP' button on top of the product image when mouse-hover action",
						"The System is displayed the 'QUICKSHOP' button on top of the product image when mouse-hover action",
						"The System is not displayed the 'QUICKSHOP' button on top of the product image when mouse-hover action", driver);
			}
			
			pdpPage = shopBag.clickOnRecentlyViewedProductName(0);
			
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage),
					"The System will take the User to the PDP of the item when clicking on product name",
					"The System taken the User to the PDP of the item when clicking on product name",
					"The System is not taken the User to the PDP of the item when clicking on product name", driver);
			
			shopBag = pdpPage.headers.clickOnBagLink();
			Log.message(i++ + ". Navigated to cart page",driver);
			
			pdpPage = shopBag.clickOnRecentlyViewedProductImage(0);
			
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage),
					"The System will take the User to the PDP of the item when clicking on product image",
					"The System taken the User to the PDP of the item when clicking on product image",
					"The System is not taken the User to the PDP of the item when clicking on product image", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP3_C21581


}// search
