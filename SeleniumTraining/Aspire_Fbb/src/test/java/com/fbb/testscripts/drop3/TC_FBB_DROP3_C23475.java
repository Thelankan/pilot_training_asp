package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CustomerGalleryModalPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C23475 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C23475(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prodId = prdData.get("prd_review");
		// Get the web driver instance
	
		//Get WebDriver Instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divSocialIcons", "sectionReviewTab", pdpPage), 
					"Review should be displayed below Social links", 
					"Review is displayed below Social links", 
					"Review is not displayed below Social links", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divSocialIcons", "sectionDetailsTab", pdpPage), 
					"Details should be displayed below Social links", 
					"Details is displayed below Social links", 
					"Details is not displayed below Social links", driver);
			
			pdpPage.clickOnDetailsTab();
			Log.message("The detail message are changing for product to product, hence verifying only whether the details is displaying or not");
	
			if (pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtCustomerGalleryHeading"), pdpPage)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divSocialIcons", "txtCustomerGalleryHeading", pdpPage), 
						"Check the Content Gallery displayed below the Share Icons",
						"The Content Gallery displayed below the Share Icons",
						"The Content Gallery is not displayed below the Share Icons", driver);
				
				CustomerGalleryModalPage custGalleryModal =	pdpPage.clickOnCustomerGalleryImages(0);
				Log.message(i++ + ". Clicked on Customer Gallery Image");
				
				Log.softAssertThat(custGalleryModal.elementLayer.VerifyPageElementDisplayed(Arrays.asList("customerGalleryModal"), pdpPage), 
						"Check the Content Gallery Modal displayed",
						"The Content Gallery Modal is displayed",
						"The Content Gallery Modal is not displayed", driver);
				
				Log.softAssertThat(custGalleryModal.elementLayer.VerifyPageElementDisplayed(Arrays.asList("thumpsUpIcon"
						,"flagIcon", "pintrestShareIcon", "facebookShareIcon", "twitterShareIcon"), custGalleryModal), 
						"User should be able to Like, flag and share this picture with other social media",
						"User will able to Like, flag and share this picture with other social media",
						"User will not able to Like, flag and share this picture with other social media", driver);
				
				custGalleryModal.closeCustomerGalleryModal();
				Log.message(i++ + ". Closed Customer Gallery Modal");
				
			} else {
				Log.reference("The given product didnt have Customer Gallery in PDP page");
			}
			
			pdpPage.clickOnReviewTab();
			Log.message(i++ + ". Clicked on review tab");
			
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("sectionReviewVisible"), pdpPage), 
					"Check the review section is opened",
					"The review section is opened",
					"The review section is not opened", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtReviewCount","txtAvgRatingsReview","txtAvgRatingBox"
					,"lnkReviewMyPost","lnkReviewMorePurchase"), pdpPage),
					"Check the review section has average rating, the number of reviews, Comfort and Service meter, overall sizes,"
							+ " recommend the product, best uses for the product, and links for Review More Purchases and My Posts.",
							"The review section has average rating, the number of reviews, Comfort and Service meter, overall sizes" + 
									" recommend the product, best uses for the product, and links for Review More Purchases and My Posts.",
									"Some components are missing in the review section", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("cmbSortReview","txtSearchReview"), pdpPage), 
					"Check the review section has Sort review combo box and Search review text box",
					"The review section has Sort review combo box and Search review text box",
					"The review section is not having Sort review combo box or Search review text box", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtReviewAuthor"), pdpPage), 
					"Check all the review has review name",
					"The reviews has reviewer name",
					"The reviews is not having reviewer name", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtReviewTitle", "txtReviewCustomerStar"), pdpPage), 
					"Check the review section has Sort review combo box and Search review text box",
					"The review section has Sort review combo box and Search review text box",
					"The review section is not having Sort review combo box or Search review text box", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionReviewChart", "cmbSortReview", pdpPage), 
					"Check the sort review displayed below the review section",
					"The sort review is displayed below the review section",
					"The sort review is not displayed below the review section", driver);
			
			if(pdpPage.getNoOfReviews() > 10) {
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkShowMoreReview"), pdpPage), 
						"Show more link should get displayed",
						"Show more link is displayed",
						"Show more link is not displayed", driver);
				
				int beforeShowMore = pdpPage.getNoOfDisplayingReviews();
				
				pdpPage.clickShowMoreReviews();
				Log.message(i++ + ".  Clicked on Show more review link");
				
				int afterShowMore = pdpPage.getNoOfDisplayingReviews();
				
				Log.softAssertThat(beforeShowMore != afterShowMore, 
						"The Show more link load should load more reviews when clicked",
						"The Show more link load is loading more reviews when clicked",
						"The Show more link load is not loading more reviews", driver);
			} else {
				Log.reference("Product is not having enough review");
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lnkShowMoreReview"), pdpPage), 
						"Show more link should not displayed",
						"Show more link is not displayed",
						"Show more link is displayed", driver);
			}
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}


}// search
