package com.fbb.testscripts.drop2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19610 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(enabled = false, groups = { "desktop"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19610(String browser) throws Exception {
		Log.testCaseInfo();
		
		List<String> sortOrder = Arrays.asList(prdData.get("plp_sort-order").split("\\|"));
		String level1 = prdData.get("level-2").split("\\|")[0];
		String level2 = prdData.get("level-2").split("\\|")[1];

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PlpPage plpPage = homePage.headers.navigateTo(level1,level2);
			Log.message(i++ + ". Navigated to Product Listing Page for " + level1 + " >>> " + level2, driver);

			//Headers header = homePage.headers;	

			//1. Verify Sort By bar below Refinement bar.
			Log.softAssertThat(plpPage.refinements.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "refinementFilterOptionsDesktop", "drpSortByCollapsedDesktop", plpPage.refinements),
					"Sort By option should be displayed below Horizondal Refinement",
					"Sort By option displayed below Horizondal Refinement",
					"Sort By option not displayed below Horizondal Refinement", driver);

			// Best Matchesâ€� should be the default sort by option selected			
			Log.softAssertThat((plpPage.elementLayer.verifyElementTextEqualTo("drpSortBySelectedDesktop", "most-popular", plpPage)),					 
					"Default Dropdown should be displayed as configured in BM -(Ref PXSFCC-3388)", 
					"Default Dropdown is displayed as configured in BM", 
					"Default Dropdown is not displayed as configured in BM", driver); 

			//(ii) Lowest priced			
			List<HashMap<String, String>> productsBeforeSort = plpPage.getAllProductDetails();			
			String sortOption = plpPage.refinements.selectSortBy(sortOrder.get(1));
			Log.message(i++ + ". Sort Option(" + sortOption + ") selected from Sorting menu.");			
			List<HashMap<String, String>> productsAfterSort = plpPage.getAllProductDetails();

			Log.softAssertThat(!productsBeforeSort.equals(productsAfterSort),
					"Product List Page should be refreshed",
					"Product List Page refreshed after sorting",
					"Product List Page did not Refresh after sorting", driver);

			Log.softAssertThat(plpPage.refinements.verifySortApplied(sortOption, plpPage),
					"Products should be sorted as per the selected option and listed in PLP",
					"Products sorted as per the selected option",
					"Products not sorted as per the selected sorting option", driver);

			//(ii) Highest priced			
			List<HashMap<String, String>> productsBeforeSort1 = plpPage.getAllProductDetails();			
			String sortOption2 = plpPage.refinements.selectSortBy(sortOrder.get(2));
			Log.message(i++ + ". Sort Option(" + sortOption2 + ") selected from Sorting menu.");			
			List<HashMap<String, String>> productsAfterSort1 = plpPage.getAllProductDetails();

			Log.softAssertThat(!productsBeforeSort1.equals(productsAfterSort1),
					"Product List Page should be refreshed",
					"Product List Page refreshed after sorting",
					"Product List Page not Refreshed after sorting", driver);

			Log.softAssertThat(plpPage.refinements.verifySortApplied(sortOption2, plpPage),
					"Products should be sorted as per the selected option and listed in PLP",
					"Products sorted as per the selected option",
					"Products not sorted as per the selected sorting option", driver);

			//2.  Click on downward arrow mark in "Sort by" drop down box
			plpPage.clickSortbyArrow("down");
			Log.message(i++ + ". Clicked Down arrow mark in 'Sort By'.");

			List<String> sortbyoptions=plpPage.getSortbylist();
			Log.message("<br>");
			Log.message("<b>Expected Result : </b>The following options should be available in the Sort By dropdown.");
			Log.softAssertThat(Utils.ListsContains(sortbyoptions,sortOrder),
					"Refinements should be listed according to the configured sequence in the storefront catalog.",
					"Refinements listed according to the configured sequence in the storefront catalog.",
					"Refinements not listed according to the configured sequence in the storefront catalog.", driver);

			//3.  Click on the upward arrow mark in "Sort by" dropdown box
			plpPage.clickSortbyArrow("up");
			Log.message(i++ + ". Clicked upward arrow mark in 'Sort By'.");			
			Log.softAssertThat(plpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("sortBySelectionListDesktop"), plpPage), 
					"Dropdown list should be closed.",
					"Dropdown list closed.",
					"Dropdown list did not closed.", driver);

			//4.  Drop down should be open
			plpPage.clickSortbyArrow("down");
			Log.message(i++ + ". Clicked Down arrow mark in 'Sort By'.");
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("sortBySelectionListDesktop"), plpPage), 
					"Drop down should be open.",
					"Drop down is open.",
					"Drop down did not open.", driver);

			//5. Click anywhere inside the sort by bar again
			plpPage.clickSortbyArrow("up");
			Log.message(i++ + ". Clicked 'Sort By' tab.");			
			Log.softAssertThat(plpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("sortBySelectionListDesktop"), plpPage), 
					"Dropdown list should be closed.",
					"Dropdown list closed.",
					"Dropdown list did not closed.", driver);


			//6. Click downward arrow mark in "Sort by" drop and click on anywhere on the site outside
			//the sort drop down.
			plpPage.clickSortbyArrow("down");
			Log.message(i++ + ". Clicked Down arrow mark in 'Sort By'.");
			plpPage.clickAnywhereInPlpPage();
			Log.message(i++ + ". Clicked outside 'Sory by' form.");	

			Log.softAssertThat(plpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("sortBySelectionListDesktop"), plpPage), 
					"Dropdown list should be closed.",
					"Dropdown list closed.",
					"Dropdown list did not closed.", driver);


			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_DROP2_C19610


}// search


