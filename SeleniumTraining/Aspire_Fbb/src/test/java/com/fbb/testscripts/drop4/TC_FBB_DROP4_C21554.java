package com.fbb.testscripts.drop4;

import java.util.Arrays;

import com.fbb.support.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.pages.MiniCartPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C21554 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C21554(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String variation1 = prdData.get("prd_withcolor_img");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Fullbeauty Home Page.", driver);
	
			if(Utils.isDesktop())
				homePage.headers.mouseOverMyBag();
			else
				homePage.headers.tabMiniCartIcon();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			//Step-14: Hover on Bag indicator icon and verify the behavior; 
			//Step-22: Verify mini cart flyout not displaying, when the user have an empty shopping bag or cart = 0.
			Log.softAssertThat(homePage.headers.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("divMiniCartOverlay"), homePage.headers),
					"When shopping bag is empty, Mini Cart flyout should not be displayed.",
					"When shopping bag is empty, Mini Cart flyout not displayed.",
					"When shopping bag is empty, Mini Cart flyout displayed.", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(variation1);
			Log.message(i++ + ". Navigated to PDP page for :: " + pdpPage.getProductName(), driver);
	
			String pdpColorCode = pdpPage.getSelectedColorCode(true);
			pdpPage.selectSize();
			pdpPage.clickAddProductToBag();
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Product added to Bag.", driver);
			if(Utils.isDesktop())
				pdpPage.headers.mouseOverMyBag();
			else
				pdpPage.headers.tabMiniCartIcon();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			MiniCartPage miniCart = new MiniCartPage(driver).get();
	
			Log.softAssertThat(pdpPage.headers.elementLayer.verifyPageElements(Arrays.asList("divMiniCartOverlay"), pdpPage.headers),
					"When hovered/tapped on Bag Indicator icon, Mini Cart flyout should be displayed.",
					"When hovered/tapped on Bag Indicator icon, Mini Cart flyout displayed.",
					"When hovered/tapped on Bag Indicator icon, Mini Cart flyout not displayed.", driver);
	
			Log.softAssertThat(pdpPage.headers.elementLayer.verifyInsideElementAlligned("lblMiniCartCountDesktop", "topMenuUtilityBar", "right", pdpPage.headers),
					"Bag indicator should be displayed on the top right of the utility bar.",
					"Bag indicator displayed on the top right of the utility bar.",
					"Bag indicator not displayed on the top right of the utility bar.", driver);
	
			Log.softAssertThat(pdpPage.headers.elementLayer.verifyCssPropertyForElement("iconMyBagQty", "background", "bag-icon-black.svg", pdpPage.headers),
					"Bag indicator should be displayed as an icon with the numeric representation of the number of items in the shopping cart.",
					"Bag indicator displayed as an icon with the numeric representation of the number of items in the shopping cart.",
					"Bag indicator not displayed as an icon with the numeric representation of the number of items in the shopping cart.", driver);
	
			//Step-1: Verify the functionality of Product image
			Log.softAssertThat(miniCart.elementLayer.verifyInsideElementAlligned("imgProductImage", "scrollSection", "left", miniCart), 
					"Product Image should be displayed to the left hand side of the Mini Cart",
					"Product Image displayed to the left hand side of the Mini Cart",
					"Product Image not displayed to the left hand side of the Mini Cart", driver);
	
			ShoppingBagPage cart = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Home page.", driver);
	
			if(Utils.isDesktop())
				cart.headers.mouseOverMyBag();
			else
				cart.headers.tabMiniCartIcon();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			miniCart = new MiniCartPage(driver).get();
			pdpPage = miniCart.clickOnProductImage();
			Log.message(i++ + ". Clicked on Product Image.", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("pdpMain"), pdpPage), 
					"Clicking on the Product Image should take us to the PDP page",
					"Clicking on the Product Image take us to the PDP page",
					"Clicking on the Product Image not take us to the PDP page", driver);
	
			if(Utils.isDesktop())
				pdpPage.headers.mouseOverMyBag();
			else
				pdpPage.headers.clickAndHoldOnBag();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			miniCart = new MiniCartPage(driver).get();
			String miniCartColorCode = miniCart.getProductColorCode();
	
			Log.event("PDP Color Code :: " + pdpColorCode);
			Log.event("MiniCart Color Code :: " + miniCartColorCode);
			Log.softAssertThat(miniCartColorCode.equals(pdpColorCode),
					"The Product Image should reflect the variation of the color selected",
					"The Product Image reflect the variation of the color selected",
					"The Product Image not reflect the variation of the color selected", driver);
	
			//Step-2: Verify the functionality of Brand name below product image in the Cart
			//Step commented due to SM-3841 : will be removed after TC change
			/*Log.softAssertThat(miniCart.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "imgProductImage", "imgBrandLogoUnderPrdImage", miniCart),
					"Brand Name should be displayed below the product image",
					"Brand Name displayed below the product image",
					"Brand Name not displayed below the product image", driver);*/
	
			//Step-3: Verify the functionality of Product name
			Log.softAssertThat(miniCart.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkProductName", "imgProductImage", miniCart),
					"Product name should be displayed to the right hand side of the product image on top of all the product attributes",
					"Product name displayed to the right hand side of the product image on top of all the product attributes",
					"Product name not displayed as expected", driver);
	
			pdpPage = miniCart.clickOnProductName();
			Log.message(i++ + ". Clicked on Product Name.", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("pdpMain"), pdpPage),
					"Clicking on the Product Name should take us to the PDP page",
					"Clicking on the Product Name take us to the PDP page",
					"Clicking on the Product Name did not take us to the PDP page", driver);
	
			if(Utils.isDesktop())
				pdpPage.headers.mouseOverMyBag();
			else
				pdpPage.headers.clickAndHoldOnBag();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			//miniCart = new MiniCartPage(driver).get();
	
			//Step-4: Verify the functionality of Variation attributes
			Log.softAssertThat(miniCart.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkProductName", "divProductAttributes", miniCart),
					"Variation attributes should be displayed below the Product name",
					"Variation attributes displayed below the Product name",
					"Variation attributes not displayed below the Product name", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C21554	
	
	@Test(groups = { "high", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP4_C21554(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String variation1 = prdData.get("prd_variation1");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Fullbeauty Home Page.", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(variation1);
			Log.message(i++ + ". Navigated to PDP page for :: " + pdpPage.getProductName(), driver);
			pdpPage.selectSizeBasedOnIndex(0);
			pdpPage.addToBag();
			Log.message(i++ + ". Product added to Bag.", driver);
			
			if(Utils.isDesktop())
				pdpPage.headers.mouseOverMyBag();
			else
				pdpPage.headers.clickAndHoldOnBag();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			MiniCartPage miniCart = new MiniCartPage(driver).get();
			
			//validation for update product attributes
			String sizeBefore = miniCart.getSizeByPrdIndex(0);
			ShoppingBagPage cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Cart Page.", driver);
	
			cartPage.updateSizeByPrdIndex(0,1);
			
			Log.message(i++ + ". Size updated.", driver);
	
			if(Utils.isDesktop())
				cartPage.headers.mouseOverMyBag();
			else
				cartPage.headers.clickAndHoldOnBag();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			miniCart = new MiniCartPage(driver).get();
			String sizeAfter = miniCart.getSizeByPrdIndex(0);
	
			Log.event("Size Before :: " + sizeBefore);
			Log.event("Size After :: " + sizeAfter);
	
			Log.softAssertThat(!sizeBefore.equals(sizeAfter),
					"The Variation Attributes should reflect the latest changes as made by the User",
					"The Variation Attributes reflect the latest changes as made by the User",
					"The Variation Attributes not reflect the latest changes as made by the User", driver);
	
			//Step-5: Verify the functionality of Quantity
			Log.softAssertThat(miniCart.elementLayer.verifyTextContains("lblQuantity", "qty", miniCart),
					"Quantity should be displayed with label 'QTY' below the variation attributes",
					"Quantity displayed with label 'QTY' below the variation attributes",
					"Quantity not displayed with label 'QTY' below the variation attributes", driver);
	
			Log.softAssertThat(miniCart.elementLayer.verifyPageElements(Arrays.asList("divQuantityValue"), miniCart),
					"Quantity should display the number of unit items added for the product.",
					"Quantity display the number of unit items added for the product.",
					"Quantity did not display the number of unit items added for the product.", driver);
	
			//validation for update product attributes
			String qtyBefore = miniCart.getQtyByPrdIndex(0);
			float totalBefore = miniCart.getSubTotal();
			float subtotalBefore = miniCart.getSubTotal();
			cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Cart Page.", driver);
	
			cartPage.updateQuantityByPrdIndex(0, "2");
			Log.message(i++ + ". Quantity updated.", driver);
	
			if(Utils.isDesktop())
				cartPage.headers.mouseOverMyBag();
			else
				cartPage.headers.clickAndHoldOnBag();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			miniCart = new MiniCartPage(driver).get();
			String qtyAfter = miniCart.getQtyByPrdIndex(0);
			float totalAfter = miniCart.getSubTotal();
			float subtotalAfter = miniCart.getSubTotal();
	
			Log.event("Quantity Before :: " + qtyBefore);
			Log.event("Quantity After :: " + qtyAfter);
	
			Log.event("Total Before :: " + totalBefore);
			Log.event("Total After :: " + totalAfter);
	
			Log.event("SubTotal Before :: " + subtotalBefore);
			Log.event("SubTotal After :: " + subtotalAfter);
	
			Log.softAssertThat(!qtyBefore.equals(qtyAfter),
					"Quantity should reflect the latest changes as made by the User",
					"Quantity reflect the latest changes as made by the User",
					"Quantity not reflect the latest changes as made by the User", driver);
	
			Log.softAssertThat(totalBefore != totalAfter,
					"Total should reflect the latest changes as made by the User",
					"Total reflect the latest changes as made by the User",
					"Total not reflect the latest changes as made by the User", driver);
	
			Log.softAssertThat(subtotalBefore != subtotalAfter,
					"Sub Total should reflect the latest changes as made by the User",
					"Sub Total reflect the latest changes as made by the User",
					"Sub Total not reflect the latest changes as made by the User", driver);
	
			//Step-6: Verify the display of Availability
			Log.softAssertThat(miniCart.elementLayer.verifyTextContains("lblAvailability", "availability", miniCart),
					"Availability should be displayed with label 'AVAILABILITY' below the quantity field",
					"Availability displayed with label 'AVAILABILITY' below the quantity field",
					"Availability not displayed with label 'AVAILABILITY' below the quantity field", driver);
	
			Log.softAssertThat(miniCart.elementLayer.verifyPageElements(Arrays.asList("divAvailabilityValue"), miniCart),
					"Availability should display the stock (Inventory status) of product in the mini cart",
					"Availability display the stock (Inventory status) of product in the mini cart",
					"Availability not display the stock (Inventory status) of product in the mini cart", driver);
	
			//Step-7: Verify the functionality of Price.
			Log.softAssertThat(miniCart.elementLayer.verifyTextContains("lblPrice", "price", miniCart),
					"Price should displayed with the label \"Price\" underneath the Availability field.",
					"Price is displayed with the label \"Price\" underneath the Availability field.",
					"Price not displayed with the label \"Price\" underneath the Availability field.", driver);
	
			Log.softAssertThat(miniCart.elementLayer.verifyPageElements(Arrays.asList("divPriceValue"), miniCart),
					"Price should be price per single product.",
					"Price displayed price per single product.",
					"Price not displayed per single product.", driver);

			if(Utils.getCurrentBrand().equals(Brand.ks)){
				Log.softAssertThat(miniCart.elementLayer.verifyElementColor("divPriceSingle", "02953", miniCart),
						"Single Price should be displayed in appropriate color.",
						"Single price displayed in appropriate color",
						"Price not displayed in appropriate color.", driver);
			}else {
				Log.softAssertThat(miniCart.elementLayer.verifyElementColor("divPriceSingle", "#000", miniCart),
						"Single Price should be displayed in black color.",
						"Single price displayed in black color",
						"Price not displayed in black color.", driver);
			}

			if(Utils.getCurrentBrand().equals(Brand.ks)) {
				Log.softAssertThat(miniCart.elementLayer.verifyElementColor("divPriceSales", "891635", miniCart),
						"Sale price should be displayed in red color.",
						"Sale price displayed in red color.",
						"Sale price not displayed in red color.", driver);
			}else {
				Log.softAssertThat(miniCart.elementLayer.verifyElementColor("divPriceSales", "bd34", miniCart),
						"Sale price should be displayed in red color.",
						"Sale price displayed in red color.",
						"Sale price not displayed in red color.", driver);
			}

			Log.softAssertThat(miniCart.elementLayer.verifyCssPropertyForElement("divPriceSingle", "text-decoration", "line-through", miniCart),
					"Original price(if sale price exist), should be displayed with a strikethrough.",
					"Original price displayed with strikethrough.",
					"Original price not displayed with strikethrough.", driver);
			
			if(Utils.isDesktop())
				cartPage.headers.mouseOverMyBag();
			else
				cartPage.headers.clickAndHoldOnBag();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			//Step-8: Verify the functionality of Total price.
			Log.softAssertThat(miniCart.elementLayer.verifyTextContains("lblTotalLabel", "total", miniCart),
					"Total price should be displayed below the availability with the label 'TOTAL'. ",
					"Total price displayed below the availability with the label 'TOTAL'. ",
					"Total price not displayed below the availability with the label 'TOTAL'. ", driver);
	
			//Step-9: Verify the display of Item divider
			Log.softAssertThat(miniCart.elementLayer.verifyCssPropertyForListElement("divProductSection", "border-bottom", "1px solid", miniCart),
					"Item divider should be a thin line which separates one product from another in the Minicart flyout",
					"Item divider be a thin line which separates one product from another in the Minicart flyout",
					"Item divider did not be a thin line which separates one product from another in the Minicart flyout", driver);
	
			//Step-10: Verify the functionality of Mini cart content slot
			Log.softAssertThat(miniCart.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divProductAttributes", "miniCartContentSlot", miniCart),
					"Mini cart content slot should be displayed below all the products.",
					"Mini cart content slot displayed below all the products.",
					"Mini cart content slot not displayed below all the products.", driver);
	
			//Step-11: Verify the functionality of Sub total
			Log.softAssertThat(miniCart.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "miniCartContentSlot", "divSubTotal", miniCart),
					"Sub total should be displayed below the mini cart slot centrally aligned",
					"Sub total displayed below the mini cart slot centrally aligned",
					"Sub total not displayed below the mini cart slot centrally aligned", driver);
	
			Log.softAssertThat(miniCart.calculateSubTotal() == miniCart.getSubTotal(),
					"Subtotal should be the sum of all the unit prices of the products added to the Minicart",
					"Subtotal be the sum of all the unit prices of the products added to the Minicart",
					"Subtotal not be the sum of all the unit prices of the products added to the Minicart", driver);
	
			//Step-13: Verify the functionality of Checkout button
			Log.softAssertThat(miniCart.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divSubTotal", "divCheckOutButton", miniCart) &&
					miniCart.elementLayer.verifyCssPropertyForElement("divCheckOutButton", "text-align", "center", miniCart),
					"Checkout button should be displayed below the Subtotal and it should be centrally aligned",
					"Checkout button displayed below the Subtotal and it should be centrally aligned",
					"Checkout button not displayed below the Subtotal and it should be centrally aligned", driver);
	
			cartPage = miniCart.clickOnCheckOut();
			Log.message(i++ + ". Clicked on Checkout Button.", driver);
	
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("btnCheckoutNow"), cartPage),
					"Clicking on the Checkout button should take us to the shopping cart page.",
					"Clicking on the Checkout button take us to the shopping cart page.",
					"Clicking on the Checkout button not take us to the shopping cart page.", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C21554	
	
	@Test(groups = { "high", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP4_C21554(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String variation1 = prdData.get("prd_variation");
		String variation2 = prdData.get("prd_variation1");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Fullbeauty Home Page.", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(variation1);
			Log.message(i++ + ". Navigated to PDP page for :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBag();
			Log.message(i++ + ". Product added to Bag.", driver);
			
			ShoppingBagPage cartPage = pdpPage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on Bag Indicator.", driver);
	
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("btnCheckoutNow"), cartPage),
					"On click/double-tap, User should navigate to Cart page.",
					"On click/double-tap, User navigate to Cart page.",
					"On click/double-tap, User not navigate to Cart page.", driver);
	
			String indicatorCountBefore = cartPage.headers.getMiniCartCount(); 
			pdpPage = cartPage.headers.navigateToPDP(variation2);
			Log.message(i++ + ". Navigated to PDP Page for :: " + pdpPage.getProductName());
	
			pdpPage.addToBag();
			Log.message(i++ + ". Product added to Bag.", driver);
	
			String indicatorCountAfter = pdpPage.headers.getMiniCartCount();
			//Step-16: Verify the 'My Bag' icon's item quantity indicator by adding and removing an item to the cart.
			Log.softAssertThat(!indicatorCountAfter.equals(indicatorCountBefore),
					"Upon adding/removing items from the Cart, the quantity indicator in the 'My Bag' icon should show an incremented/decremented quantity",
					"Upon adding/removing items from the Cart, the quantity indicator in the 'My Bag' icon showned incremented/decremented quantity",
					"Upon adding/removing items from the Cart, the quantity indicator in the 'My Bag' icon not showned incremented/decremented quantity", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C21554	
	
	@Test(groups = { "high", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP4_C21554(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String giftCert = prdData.get("prd_gc_certificate");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Fullbeauty Home Page.", driver);
	
			PdpPage pdpPage = homePage.headers.redirectToPDP(giftCert, driver);
			Log.message(i++ + ". Navigated to PDP page for :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBag();
			Log.message(i++ + ". Gift Certificate added to Bag.", driver);
	
			if(Utils.isDesktop())
				pdpPage.headers.mouseOverMyBag();
			else
				pdpPage.headers.clickAndHoldOnBag();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			MiniCartPage miniCart = new MiniCartPage(driver).get();
			//Step-17: Verify the behavior when a Gift Certificate is added
			Log.softAssertThat(miniCart.elementLayer.verifyPageElements(Arrays.asList("lnkProductName","lblQuantity","lblGiftCertPrice","lblGiftCertTotal","divGiftCardFrom","divGiftCardTo"), miniCart),
					"Gift Certificate should be displayed just like a standard/variation product ",
					"Gift Certificate displayed just like a standard/variation product ",
					"Gift Certificate not displayed just like a standard/variation product ", driver);
	
			pdpPage = miniCart.clickOnProductImage();
			Log.message(i++ + ". Clicked on Gift Certifiate.", driver);
	
			Log.event(pdpPage.getProductType()+"||electronic-gc");
			Log.softAssertThat(pdpPage.getProductType().equals("electronic-gc"),
					"Clicking on the Gift Certificate should takes user to the PDP page of the Gift Certificate",
					"Clicking on the Gift Certificate takes user to the PDP page of the Gift Certificate",
					"Clicking on the Gift Certificate not takes user to the PDP page of the Gift Certificate", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C21554
	
	@Test(groups = { "high", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M5_FBB_DROP4_C21554(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String giftCard = prdData.get("prd_gc_physical");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Fullbeauty Home Page.", driver);
	
			PdpPage pdpPage = homePage.headers.redirectToPDP(giftCard, driver);
			Log.message(i++ + ". Navigated to PDP page for :: " + pdpPage.getProductName());
	
			pdpPage.addToBag();
			Log.message(i++ + ". Gift Card added to Bag.", driver);
	
			if(Utils.isDesktop())
				pdpPage.headers.mouseOverMyBag();
			else
				pdpPage.headers.clickAndHoldOnBag();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			MiniCartPage miniCart = new MiniCartPage(driver).get();
			//Step-18: Verify the behavior when a Gift Card is added
			Log.softAssertThat(miniCart.elementLayer.verifyPageElements(Arrays.asList("lnkProductName","divProductAttributesGC","lblQuantity","lblPriceGC","lblTotal"), miniCart),
					"Gift Card should be displayed just like a standard/variation product ",
					"Gift Card displayed just like a standard/variation product ",
					"Gift Card not displayed just like a standard/variation product ", driver);
	
			pdpPage = miniCart.clickOnProductImage();
			Log.message(i++ + ". Clicked on Gift Card.", driver);
	
			Log.softAssertThat(pdpPage.getProductType().equals("physical-gc"),
					"Clicking on the Gift Card should takes user to the PDP page of the Gift Card",
					"Clicking on the Gift Card takes user to the PDP page of the Gift Card",
					"Clicking on the Gift Card not takes user to the PDP page of the Gift Card", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C21554
	
	@Test(groups = { "high", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M6_FBB_DROP4_C21554(String browser) throws Exception {
		Log.testCaseInfo();
	
		String sps = prdData.get("ps_special-product-set").split("\\|")[0];
		String prod1 = prdData.get("prd_variation1");
		String prod2 = prdData.get("prd_variation2");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		Dimension dimension = new Dimension(1100,800);
		driver.manage().window().setSize(dimension);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Fullbeauty Home Page.", driver);
	
			if(Utils.isTablet()) {
				PdpPage pdpPage = homePage.headers.navigateToPDP(prod1);
				Log.message(i++ +". Navigated to Product : "+pdpPage.getProductName());
				pdpPage.addToBag();
				Log.message(i++ + ". Product added to cart");
				
				homePage.headers.navigateToPDP(prod2);
				Log.message(i++ +". Navigated to Product : "+pdpPage.getProductName());
				pdpPage.addToBag();
				Log.message(i++ + ". Product added to cart");
			}
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(sps);
			Log.message(i++ + ". Navigated to PDP page for :: " + pdpPage.getProductName());
			pdpPage.selectAllSplProductSetColorBasedOnIndex(0);
			pdpPage.selectAllSplProductSetSizeBasedOnIndex(0);
			pdpPage.selectAllSplProductSetSizeFamilyBasedOnIndex(0);
			pdpPage.addToBagSpecialProductSet();
			Log.message(i++ + ". Special Product Set added to Bag.", driver);
			
			pdpPage.closeAddToBagOverlay();
			
			if(Utils.isDesktop())
				pdpPage.headers.mouseOverMyBag();
			else
				pdpPage.headers.clickAndHoldOnBag();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			MiniCartPage miniCart = new MiniCartPage(driver).get();
	
			//Step-19: Verify the display of Special Product Set
			Log.softAssertThat(miniCart.elementLayer.verifyPageElements(Arrays.asList("lnkProductName","divProductAttributes","lblQuantity","lblPrice","lblTotal"), miniCart),
					"Special Product Set should be displayed with the following attributes: 'QTY', 'PRICE', 'TOTAL'",
					"Special Product Set displayed with the following attributes: 'QTY', 'PRICE', 'TOTAL'",
					"Special Product Set not displayed with the following attributes: 'QTY', 'PRICE', 'TOTAL'", driver);
	
			Log.softAssertThat(miniCart.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "lblAvailability", "spsFirstChildPrd", miniCart),
					"All the set components from the product set should be displayed beneath the Product Set with a slight indentation to the right.",
					"All the set components from the product set displayed beneath the Product Set with a slight indentation to the right.",
					"All the set components from the product set not displayed beneath the Product Set with a slight indentation to the right.", driver);
	
			int color = ElementLayer.getListElement("spsChildPrdColor", miniCart).size();
			int size = ElementLayer.getListElement("spsChildPrdSize", miniCart).size();
			int availability = ElementLayer.getListElement("spsChildPrdAvailability", miniCart).size();
			Log.softAssertThat(color == size && color == availability,
					"All variation/standard products listed under the Product Set should have the Product image & the Product name and the following attributes: 'COLOR', 'SIZE' and 'AVAILABILITY'",
					"All variation/standard products listed under the Product Set have the Product image & the Product name and the following attributes: 'COLOR', 'SIZE' and 'AVAILABILITY'",
					"All variation/standard products listed under the Product Set not have the Product image & the Product name and the following attributes: 'COLOR', 'SIZE' and 'AVAILABILITY'", driver);
	
			pdpPage = miniCart.clickOnProductImage();
			Log.message(i++ + ". Clicked on Special Product Set.", driver);
	
			Log.softAssertThat(pdpPage.getProductType().equals("specialproductset"),
					"Clicking on the Special Product Set should take us to the PDP page of the Special Product Set",
					"Clicking on the Special Product Set take us to the PDP page of the Special Product Set",
					"Clicking on the Special Product Set not take us to the PDP page of the Special Product Set", driver);
	
			if(Utils.isDesktop())
				pdpPage.headers.mouseOverMyBag();
			else
				pdpPage.headers.clickAndHoldOnBag();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			miniCart = new MiniCartPage(driver).get();
			
			//Step-20: Verify the behavior of scroll bar in the Minicart flyout
			Log.softAssertThat(miniCart.elementLayer.verifyPageElements(Arrays.asList("scrollBar"), miniCart),
					"Scroll Bar should be displayed to the right extreme side of the Mini Cart when the number of products are more than the fixed height of the fly-out in the mini cart.",
					"Scroll Bar displayed to the right extreme side of the Mini Cart when the number of products are more than the fixed height of the fly-out in the mini cart.",
					"Scroll Bar not displayed to the right extreme side of the Mini Cart when the number of products are more than the fixed height of the fly-out in the mini cart.", driver);
	
			if(Utils.isDesktop())
				pdpPage.headers.mouseOverMyBag();
			else
				pdpPage.headers.clickAndHoldOnBag();
			Log.message(i++ + ". Mouse Hovered/Tapped on Minicart Icon.", driver);
	
			miniCart = new MiniCartPage(driver).get();
			//Step-21: Verify the close-out behavior of the Mini Cart
			pdpPage.headers.mouseOverCustomerService();
			if(Utils.isDesktop()) {
				Log.softAssertThat(pdpPage.headers.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divMiniCartOverlay"), pdpPage.headers),
						"When the Mini Cart is open, move the mouse pointer out of field from the Mini Cart should closes/collapses",
						"When the Mini Cart is open, move the mouse pointer out of field from the Mini Cart closes/collapses",
						"When the Mini Cart is open, move the mouse pointer out of field from the Mini Cart not closes/collapses", driver);
			}
			if(Utils.isTablet()) {
				Log.softAssertThat(pdpPage.headers.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divMiniCartOverlay"), pdpPage.headers),
						"When the Mini Cart is open, tap anywhere outside the Mini Cart area and then check should closes/collapses",
						"When the Mini Cart is open, tap anywhere outside the Mini Cart area and then check closes/collapses",
						"When the Mini Cart is open, tap anywhere outside the Mini Cart area and then check not closes/collapses", driver);
			}
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C21554
}// search
