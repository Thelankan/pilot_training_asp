package com.fbb.testscripts.drop1;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.customerservice.ContactUsPage;
import com.fbb.pages.customerservice.FaqPage;
import com.fbb.pages.customerservice.ReturnAndExchangePage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP1_C19657 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	
	//Few parts of this test case been commented due to improper URLs
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP1_C19657(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			Headers headers = homePage.headers;
			Footers footers = homePage.footers;
			footers.scrollToFooter();
			Log.message(i++ + ". Scrolled to Footer section!", driver);

			//Step-1: Verify the email sign up section
			Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("fldEmailSignUpFooter"), homePage.footers), 
					"Check the Email signup text box is displaying in the page", 
					"The Email signup text box is displaying in the page", 
					"The Email signup text box is not displaying in the page", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("btnEmailSignUpMobile"), homePage.footers), 
						"Check the Email signup button is displaying in the page", 
						"The Email signup button is displaying in the page", 
						"The Email signup button is not displaying in the page", driver);
			} else {
				Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("btnEmailSignUpDesktopTablet"), homePage.footers), 
						"Check the Email signup text box is displaying in the page", 
						"The Email signup text box is displaying in the page", 
						"The Email signup text box is not displaying in the page", driver);
			}
			
			homePage.footers.clickOnEmailSignUp();
			
			if(Utils.isMobile()) {
				Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("txtEmailSignUpErrorMobile"), homePage.footers), 
						"Check the Email signup text box is displaying error when email ID field is Empty", 
						"The Email signup text box is displaying an error when email ID field is Empty", 
						"The Email signup text box is not displaying an error when email ID field is Empty", driver);
			} else {
				Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("txtEmailSignUpErrorDesktop"), homePage.footers), 
						"Check the Email signup text box is displaying error when email ID field is Empty", 
						"The Email signup text box is displaying an error when email ID field is Empty", 
						"The Email signup text box is not displaying an error when email ID field is Empty", driver);
			}
			
			
			//Invalid Email Id format
			homePage.footers.typeInEmailSignUp("invalidemail");
			
			homePage.footers.clickOnEmailSignUp();
			
			Log.softAssertThat(homePage.footers.getErrorMessage(), 
					"Check the Email signup text box is displaying error when invalid email ID is entered", 
					"The Email signup text box is displaying error when invalid email ID is entered", 
					"The Email signup text box is not displaying error when invalid email ID is entered", driver);
			
			//Valid Email ID
			homePage.footers.typeInEmailSignUp("automationfbb@yopmail.com");
			homePage.footers.clickOnEmailSignUp();
			
			if(Utils.isMobile()) {
				Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("lblEmailSignUpSuccessMsgMobile"), homePage.footers), 
						"Check the success message is displaying when valid email ID is entered and signup", 
						"The success message is displaying when valid email ID is entered and signup", 
						"The success message is displaying when valid email ID is entered and signup", driver);
			} else {
				Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("lblEmailSignUpSuccessMsgDesktop"), homePage.footers), 
						"Check the success message is displaying when valid email ID is entered and signup", 
						"The success message is displaying when valid email ID is entered and signup", 
						"The success message is displaying when valid email ID is entered and signup", driver);
			}
			
			//Step-7: Verify the background display in Footer Section
			String footerBGColor = "rgba(244, 244, 244, 1)";
			Log.softAssertThat(homePage.footers.elementLayer.verifyCssPropertyForElement("footerContainer","background-color", footerBGColor, homePage.footers), 
					"Check the footer container is displaying in grey background", 
					"The footer container is displaying in grey background", 
					"The footer container is not displaying in grey background", driver);
			
			//Step-5: Click/Tap on any Brand logos in footer section
			Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("imgFullBeauty", "imgSwimSuits", "imgEllos", 
					"imgBrylaneHome", "imgBCO"), homePage.footers), 
					"Check the brand images are displaying in the footers", 
					"The brand images 'Full Beauty', 'Swim Suits', 'Ellos', 'Brylane Home', 'BCO' are displaying in the footers", 
					"Not all the brands are displaying in the footers", driver);
			
			List<String> elementsToBeVerified = null;
			
			if(Utils.getCurrentBrand().equals(Brand.jl)) {
				elementsToBeVerified = Arrays.asList("imgWomanWithin","imgRoamans", "imgKingSize");
			} else if(Utils.getCurrentBrand().equals(Brand.rm)) {
				elementsToBeVerified = Arrays.asList("imgJessicaLondon","imgWomanWithin", "imgKingSize");
			} else if (Utils.getCurrentBrand().equals(Brand.ww)){
				elementsToBeVerified = Arrays.asList("imgJessicaLondon","imgRoamans", "imgKingSize");
			} else {
				elementsToBeVerified = Arrays.asList("imgJessicaLondon","imgRoamans", "imgWomanWithin");
			}
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(elementsToBeVerified, homePage.footers), 
					"Check the brand images are displaying in the footers", 
					"The other two brand images are displaying in the footers", 
					"The brand images are not displaying in the footers", driver);
			
			if(!(Utils.getCurrentBrand().equals(Brand.jl))) {
				footers.navigateToJessicaLondonBrand();
				Log.message(i++ + ". Clicked on JessicaLondon Brand.", driver);
				
				Log.softAssertThat(driver.getCurrentUrl().contains("jessicalondon"), 
						"Page should be redirected to JessicaLondon Page", 
						"Page redirected to JessicaLondon Page", 
						"Page Not Redirected to JessicaLondon Page", driver);
				
				driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
				Log.message(i++ + ". Switched To FullBeauty Website.", driver);
			}
			
			if(!(Utils.getCurrentBrand().equals(Brand.ww))) {
				footers.navigateToWomanWithinBrand();
				Log.message(i++ + ". Clicked on WomanWithin Brand.", driver);
				
				Log.softAssertThat(driver.getCurrentUrl().contains("womanwithin"), 
						"Page should be redirected to WomanWithin Page", 
						"Page redirected to WomanWithin Page", 
						"Page Not Redirected to WomanWithin Page", driver);
				
				driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
				Log.message(i++ + ". Switched To FullBeauty Website.", driver);
			}
			
			if(!(Utils.getCurrentBrand().equals(Brand.ks))) {
				footers.navigateToKingSizeBrand();
				Log.message(i++ + ". Clicked on KingSize Brand.", driver);
				
				Log.softAssertThat(driver.getCurrentUrl().contains("kingsize"), 
						"Page should be redirected to KingSize Page", 
						"Page redirected to KingSize Page", 
						"Page Not Redirected to KingSize Page", driver);
				
				driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
				Log.message(i++ + ". Switched To FullBeauty Website.", driver);
			}
			
			if(!(Utils.getCurrentBrand().equals(Brand.rm))) {
				footers.navigateToRoamansBrand();
				Log.message(i++ + ". Clicked on Roamans Brand.", driver);
				
				Log.softAssertThat(driver.getCurrentUrl().contains("roamans"), 
						"Page should be redirected to Roamans Page", 
						"Page redirected to Roamans Page", 
						"Page Not Redirected to Roamans Page", driver);
				
				driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
				Log.message(i++ + ". Switched To FullBeauty Website.", driver);
			}
			
			footers.navigateToEllosBrand();
			Log.message(i++ + ". Clicked on Ellos Brand.", driver);
			
			Log.softAssertThat(driver.getCurrentUrl().contains("ellos"), 
					"Page should be redirected to Ellos Page", 
					"Page redirected to Ellos Page", 
					"Page Not Redirected to Ellos Page", driver);
			
			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);
			
			footers.navigateToSwimSuitsBrand();
			Log.message(i++ + ". Clicked on SwimSuits Brand.", driver);
			
			Log.softAssertThat(driver.getCurrentUrl().contains("swimsuitsforall"), 
					"Page should be redirected to SwimSuits Brand Page", 
					"Page redirected to SwimSuits Brand Page", 
					"Page Not Redirected to SwimSuits Brand Page", driver);
			
			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);
			
			footers.navigateToBrylaneHome();
			Log.message(i++ + ". Clicked on Brylane Brand.", driver);
			
			Log.softAssertThat(driver.getCurrentUrl().contains("brylanehome"), 
					"Page should be redirected to Brylane Page", 
					"Page redirected to Brylane Page", 
					"Page Not Redirected to Brylane Page", driver);
			
			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);
			
			footers.navigateToBCO();
			Log.message(i++ + ". Clicked on Bargain Catalog Outlet Brand.", driver);
			
			Log.softAssertThat(driver.getCurrentUrl().contains("bcoutlet"), 
					"Page should be redirected to Bargain Catalog Outlet Brand Page", 
					"Page redirected to Bargain Catalog Outlet Brand Page", 
					"Page Not Redirected to Bargain Catalog Outlet Brand Page", driver);
			
			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);
			
			footers.navigateToFullBeautyBrand();
			Log.message(i++ + ". Clicked on FullBeauty Brand.", driver);
			
			Log.softAssertThat(driver.getCurrentUrl().contains("fullbeauty"), 
					"Page should be redirected to FullBeauty Brand Page", 
					"Page redirected to FullBeauty Brand Page", 
					"Page Not Redirected to FullBeauty Brand Page", driver);
			
			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);
			
			//Step-2: Scroll down to the Footer and click/Tap Social links 
			footers.navigateToInstagram();
			Log.message(i++ + ". Navigated to 'Instagram'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("instagram"), 
					"Page should be redirected to Instagram Page", 
					"Page redirected to Instagram Page", 
					"Page Not Redirected to Instagram Page", driver);

			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);

			footers.navigateToFaceBook();
			Log.message(i++ + ". Clicked on 'Facebook'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("facebook"), 
					"Page should be redirected to Facebook Page", 
					"Page redirected to Facebook Page", 
					"Page Not Redirected to Facebook Page", driver);

			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);

			footers.navigateToPinterest();
			Log.message(i++ + ". Clicked on 'Pinterest'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("pinterest"), 
					"Page should be redirected to Pintrest Page", 
					"Page redirected to Pintrest Page", 
					"Page Not Redirected to Pintrest Page", driver);

			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);

			footers.navigateToTwitter();
			Log.message(i++ + ". Clicked on 'Twitter'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("twitter"), 
					"Page should be redirected to Twitter Page", 
					"Page redirected to Twitter Page", 
					"Page Not Redirected to Twitter Page", driver);

			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);

			footers.navigateToYoutube();
			Log.message(i++ + ". Clicked on 'Youtube'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("youtube"), 
					"Page should be redirected to Youtube Page", 
					"Page redirected to Youtube Page", 
					"Page Not Redirected to Youtube Page", driver);

			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);
			
			//Step-4: Click/Tap on the Footer links in the Footer section
			footers.navigateToTopQuestions();
			Log.message(i++ + ". Clicked on 'Top Questions'!", driver);

			Log.softAssertThat(new FaqPage(driver).get().isPageLoaded, 
					"Page should be redirected to 'Top Questions' Page", 
					"Page redirected to 'Top Questions' Page", 
					"Page Not Redirected to 'Top Questions' Page", driver);
			BrowserActions.navigateToBack(driver);

			footers.navigateToContactUs();
			Log.message(i++ + ". Clicked on 'Contact Us'!", driver);

			
			Log.softAssertThat(new ContactUsPage(driver).get().isPageLoaded, 
					"Page should be redirected to 'Contact Us' Page", 
					"Page redirected to 'Contact Us' Page", 
					"Page Not Redirected to 'Contact Us' Page", driver);
			BrowserActions.navigateToBack(driver);

			if(!headers.getLiveChatStatus()){
				Log.message(i++ + ". Live Chat service will be available during working hours only.");
			}else{
				footers.navigateToLiveChat();
				Log.message(i++ + ". Clicked on 'Live Chat'!", driver);

				Log.softAssertThat(homePage.headers.elementLayer.verifyPageElements(Arrays.asList("mdlLiveChat"), homePage.headers), 
						"Live Chat Modal Should be Shown", 
						"Live Chat Modal Shown", 
						"Live Chat Modal not Shown", driver);

				headers.closeLiveChat();
			}
			driver.get(Utils.getWebSite());
			BrowserActions.scrollToBottomOfPage(driver);
			footers.navigateToReturnCenter();
			Log.message(i++ + ". Clicked on 'Return Center'!", driver);

			Log.softAssertThat(new ReturnAndExchangePage(driver).get().isPageLoaded, 
					"Page should be redirected to 'Return Center' Page", 
					"Page redirected to 'Return Center' Page", 
					"Page Not Redirected to 'Return Center' Page", driver);
			BrowserActions.navigateToBack(driver);

			//Since these URL's aren't configured properly, skipping these lines
			/*footers.navigateToEmailSubscription();
			Log.message(i++ + ". Clicked on 'Email-Subscription'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains(""), 
					"Page should be redirected to 'EMail Subscription' Page", 
					"Page redirected to 'EMail Subscription' Page", 
					"Page Not Redirected to 'EMail Subscription' Page", driver);
			BrowserActions.navigateToBack(driver);*/

			footers.navigateToCustomerService();
			Log.message(i++ + ". Clicked on 'Customer Service'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains(""), 
					"Page should be redirected to 'Customer Service' Page", 
					"Page redirected to 'Customer Service' Page", 
					"Page Not Redirected to 'Customer Service' Page", driver);
			BrowserActions.navigateToBack(driver);

			footers.navigateToGiftCardPage();
			Log.message(i++ + ". Clicked on 'Gift Card Page'!", driver);

			Log.softAssertThat(new GiftCardPage(driver).get().isPageLoaded, 
					"Page should be redirected to 'Gift Card Balance' Page", 
					"Page redirected to 'Gift Card Balance' Page", 
					"Page Not Redirected to 'Gift Card Balance' Page", driver);
			BrowserActions.navigateToBack(driver);

			//Temporarily Commented for currently link not available 
			//Ordering section
			/*footers.navigateToShippingAndHandling();
			Log.message(i++ + ". Clicked on 'Shipping & Handling'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains(""), 
					"Page should be redirected to 'Shipping & Handling' Page", 
					"Page redirected to 'Shipping & Handling' Page", 
					"Page Not Redirected to 'Shipping & Handling' Page", driver);
			BrowserActions.navigateToBack(driver);*/

			footers.navigateToInternationalShipping();
			Log.message(i++ + ". Clicked on 'International Shipping'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains(""), 
					"Page should be redirected to 'International Shipping' Page", 
					"Page redirected to 'International Shipping' Page", 
					"Page Not Redirected to 'International Shipping' Page", driver);
			BrowserActions.navigateToBack(driver);

			//Temporarily Commented for currently link not available 
			/*footers.navigateToEasyReturns();
			Log.message(i++ + ". Clicked on 'Easy Returns'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains(""), 
					"Page should be redirected to 'Easy Returns' Page", 
					"Page redirected to 'Easy Returns' Page", 
					"Page Not Redirected to 'Easy Returns' Page", driver);
			BrowserActions.navigateToBack(driver);*/

			/*footers.navigateToOrderStatus();
			Log.message(i++ + ". Clicked on 'Order Status'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains(""), 
					"Page should be redirected to 'Order Status' Page", 
					"Page redirected to 'Order Status' Page", 
					"Page Not Redirected to 'Order Status' Page", driver);
			BrowserActions.navigateToBack(driver);*/

			//Account section
			footers.navigateToYourAccount();
			Log.message(i++ + ". Clicked on 'Your Account'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("Login-Show") || driver.getCurrentUrl().contains("account"), 
					"Page should be redirected to 'Your Account' Page", 
					"Page redirected to 'Your Account' Page", 
					"Page Not Redirected to 'Your Account' Page", driver);

			footers.navigateToOrderStatusUnderAccountSection();
			Log.message(i++ + ". Clicked on 'Order Status' under Account section!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("order"), 
					"Page should be redirected to 'Order Status' Page", 
					"Page redirected to 'Order Status' Page", 
					"Page Not Redirected to 'Order Status' Page", driver);

			footers.navigateToWishLists();
			Log.message(i++ + ". Clicked on 'Wish List'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("wishlist"), 
					"Page should be redirected to 'Wish List'! Page", 
					"Page redirected to 'Wish List'! Page", 
					"Page Not Redirected to 'Wish List'! Page", driver);

			footers.navigateToRewardPoints();
			Log.message(i++ + ". Clicked on 'Reward Points'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("platinum-card"), 
					"Page should be redirected to 'Reward Points' Page", 
					"Page redirected to 'Reward Points' Page", 
					"Page Not Redirected to 'Reward Points' Page", driver);

			//About section
			Brand brandName = Utils.getCurrentBrand();
			Log.message(i++ + ". Got the current brand name!", driver);

			footers.navigateToAboutBrand(brandName);
			Log.message(i++ + ". Clicked on 'About " + brandName + "'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("about"), 
					"Page should be redirected to About Page", 
					"Page redirected to About Page", 
					"Page Not Redirected to About Page", driver);

			driver.get(Utils.getWebSite());
			BrowserActions.scrollToBottomOfPage(driver);
			footers.navigateToFullBeautyBrandCareer();
			driver = Utils.switchWindows(driver, "careers/where-would-you-fit", "url", "false");
			Log.message(i++ + ". Clicked on 'Full Beauty Brand Career'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("career"), 
					"Page should be redirected to 'Full Beauty Brand Career' Page", 
					"Page redirected to 'Full Beauty Brand Career' Page", 
					"Page Not Redirected to 'Full Beauty Brand Career' Page", driver);

			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			driver.get(Utils.getWebSite());
			BrowserActions.scrollToBottomOfPage(driver);
			footers.navigateToAboutFullBeautyBrands();
			driver = Utils.switchWindows(driver, "company-profile", "url", "NO");
			Log.message(i++ + ". Clicked on 'About Full Beauty Brands'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("company-profile"), 
					"Page should be redirected to 'About Full Beauty Brands' Page", 
					"Page redirected to 'About Full Beauty Brands' Page", 
					"Page Not Redirected to 'About Full Beauty Brands' Page", driver);
			//String currentURL = driver.getCurrentUrl();
			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			/*footers.navigateToInTheCommunity();
			Log.message(i++ + ". Clicked on 'In The Community'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("community"), 
					"Page should be redirected to 'In The Community' Page", 
					"Page redirected to 'In The Community' Page", 
					"Page Not Redirected to 'In The Community' Page", driver);

			//3
			driver.get(Utils.getWebSite());
			BrowserActions.scrollToBottomOfPage(driver);
			footers.navigateToNthBrand(2);
			Log.message(i++ + ". Navigated to Brand Home Page!", driver);

			Log.softAssertThat(!driver.getCurrentUrl().equals(currentURL), 
					"Page should be redirected to "+ Utils.getCurrentBrand()+" Page", 
					"Page redirected to "+ Utils.getCurrentBrand()+" Page", 
					"Page Not Redirected to "+ Utils.getCurrentBrand()+" Page", driver);

			driver.get(Utils.getWebSite());
			BrowserActions.scrollToBottomOfPage(driver);
			currentURL = driver.getCurrentUrl();
			footers.navigateToNthBrand(3);
			Log.message(i++ + ". Clicked on 'Woman Within Brands'!", driver);

			Log.softAssertThat(!driver.getCurrentUrl().equals(currentURL), 
					"Page should be redirected to "+ Utils.getCurrentBrand()+" Page", 
					"Page redirected to "+ Utils.getCurrentBrand()+" Page", 
					"Page Not Redirected to "+ Utils.getCurrentBrand()+" Page", driver);*/

			Log.reference("Other brand navigation validation skipped.");
			
			//Step-6: Click/Tap the Legal links in the footer
			driver.get(Utils.getWebSite());
			BrowserActions.scrollToBottomOfPage(driver);
			footers.navigateToPrivacyPolicy();
			Log.message(i++ + ". Clicked on 'Privacy Policy'!", driver);

			driver = Utils.switchWindows(driver, "privacy-policy", "url", "NO");
			
			Log.softAssertThat(driver.getCurrentUrl().contains("privacy-policy"), 
					"Page should be redirected to Privacy Policy Page", 
					"Page redirected to Privacy Policy Page", 
					"Page Not Redirected to Privacy Policy Page", driver);

			driver = Utils.switchWindows(driver, "plussizetech", "url", "NO");
			BrowserActions.scrollToBottomOfPage(driver);
			footers.navigateToTermsOfUse();
			Log.message(i++ + ". Clicked on 'Terms of use'!", driver);

			driver = Utils.switchWindows(driver, "terms-of-use", "url", "NO");
			Log.softAssertThat(driver.getCurrentUrl().contains("terms"), 
					"Page should be redirected to 'Terms of use' Page", 
					"Page redirected to 'Terms of use' Page", 
					"Page Not Redirected to 'Terms of use' Page", driver);

			//Temporarily Commented for currently link not available 
			/*BrowserActions.scrollToBottomOfPage(driver);
			footers.navigateToSupplyChainsAct();
			Log.message(i++ + ". Clicked on 'Supply Chains Act'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("supply"), 
					"Page should be redirected to 'Supply Chains Act' Page", 
					"Page redirected to 'Supply Chains Act' Page", 
					"Page Not Redirected to 'Supply Chains Act' Page", driver);*/

			//Log.failsoft("Failing test script since list of links aren't available yet...");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_FOOTER_C19657


}// search
