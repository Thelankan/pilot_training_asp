package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22780 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, priority = 1, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22780(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String credentilasPreApproved = accountData.get("plcc_preapproved_" + Utils.getCurrentBrandShort().toString());
		String email = credentilasPreApproved.split("\\|")[0];
		String password = credentilasPreApproved.split("\\|")[1];
		String searchKey = prdData.get("prd_variation");
		String preapprovedRebuttalMsg = prdData.get("acquisition_rebuttal_preapproved_msg");
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged in to preapproved user account.", driver);
			
			String userName = headers.getUserName();
			
			PdpPage pdp = headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to product.", driver);
			
			pdp.addToBag();
			Log.message(i++ + ". Product added to cart.", driver);
			
			ShoppingBagPage shoppingBag = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag page.", driver);
			
			CheckoutPage checkout = shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigate to Checkout page", driver);
			
			checkout.closePLCCOfferByNoThanks1();
			Log.message(i++ + ". Clicked on No Thanks.", driver);
			
			checkout.clickOnContinue();
			Log.message(i++ + ". Clicked on Continue.", driver);
			
			checkout.openClosePLCCRebuttal("open");
			Log.message(i++ + ". Clicked Learn More to open acquisition rebuttal modal.", driver);
			
			//Step-1: Verify the functionality of Offer Content
			Log.softAssertThat(checkout.elementLayer.verifyElementWithinElement(driver, "divAcquistionRebuttalPreApprovedMessage", "divAcquistionRebuttalBanner", checkout), 
					"The pre-approved credit limit from ADS should get dynamically inserted into the respective content asset banner.", 
					"The pre-approved credit limit is inserted into the respective content asset banner.", 
					"The pre-approved credit limit is not inserted into the respective content asset banner.", driver);
			
			String rebuttalWelcome = checkout.elementLayer.getAttributeForElement("divAcquistionRebuttalWelcome", "innerHTML", checkout);
			String preapprovedMsg = checkout.elementLayer.getAttributeForElement("divAcquistionRebuttalPreApprovedMessage", "innerHTML", checkout);
			String preapprovedLimit = Utils.getNumberInString(preapprovedMsg)+"";
			
			Log.softAssertThat(preapprovedMsg.indexOf(preapprovedLimit) == preapprovedMsg.indexOf('$')+1, 
					"Credit limit should be displayed beginning with $ and should be separated by comma (if applicable).", 
					"Credit limit is displayed beginning with $", 
					"Credit limit is not displayed beginning with $", driver);
			
			Log.softAssertThat(rebuttalWelcome.toLowerCase().startsWith(userName.toLowerCase()), 
					"System should dynamically insert the user's firstName into the respective content asset banner.", 
					"System inserted the user's firstName into the respective content asset banner.", 
					"System did not insert the user's firstName into the respective content asset banner.", driver);
			
			Log.softAssertThat(checkout.elementLayer.verifyElementTextContains("divAcquistionRebuttalPreApprovedMessage", preapprovedRebuttalMsg, checkout), 
					"Text in the Offer Content should be handled via the brand-specific content asset IDs.", 
					"Text in the Offer Content is handled via the brand-specific content asset IDs.", 
					"Text in the Offer Content is not handled via the brand-specific content asset IDs.", driver);
			
			//Step-4: Verify the functionality of No Thanks button
			if(!Utils.isMobile()) {
				BrowserActions.clickOnEmptySpaceHorizontal(driver, "divAcquistionRebuttal", checkout);
				
				Log.softAssertThat(checkout.elementLayer.VerifyElementDisplayed(Arrays.asList("divAcquistionRebuttal"), checkout), 
						"Clicking anywhere outside the modal does not close the respective modal. ", 
						"Modal is not closed.", 
						"Modal is closed.", driver);
			}
			
			checkout.closePLCCOfferByNoThanks();
			Log.message(i++ + ". Clicked on No Thanks");
			
			Log.softAssertThat(!checkout.elementLayer.VerifyElementDisplayed(Arrays.asList("divAcquistionRebuttal"), checkout), 
					"On clicking No Thanks, system should close the modal and the user should be navigated back to the current step in checkout flow. ", 
					"Modal is closed.", 
					"Modal is not closed.", driver);
			
			headers.navigateToHome();
			headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag page.", driver);
			
			shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigate to Checkout page", driver);
			
			Log.softAssertThat(!checkout.elementLayer.VerifyPageElementDisplayed(Arrays.asList("mdlPLCCRebuttal"), checkout), 
					"User should no longer see any pre-approval modals for the duration of the session and the rebuttal content should be removed from the Checkout Step 2 for the duration of the session", 
					"Acquisiotion rebuttal is not shown.", 
					"Acquisiotion rebuttal is shown.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	} // M1_FBB_DROP5_C22780
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, priority = 2, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP5_C22780(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String credentilasPreApproved = accountData.get("plcc_preapproved_" + Utils.getCurrentBrandShort().toString());
		String email = credentilasPreApproved.split("\\|")[0];
		String password = credentilasPreApproved.split("\\|")[1];
		String searchKey = prdData.get("prd_variation");
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged in to preapproved user account.", driver);
			
			PdpPage pdp = headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to product.", driver);
			
			pdp.addToBag();
			Log.message(i++ + ". Product added to cart.", driver);
			
			ShoppingBagPage shoppingBag = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag page.", driver);
			
			CheckoutPage checkout = shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigate to Checkout page", driver);
			
			checkout.closePLCCOfferByNoThanks1();
			Log.message(i++ + ". Clicked on No Thanks.", driver);
			
			checkout.clickOnContinue();
			Log.message(i++ + ". Clicked on Continue.", driver);
			
			checkout.openClosePLCCRebuttal("open");
			Log.message(i++ + ". Clicked Learn More to open acquisition rebuttal modal.", driver);
			
			//Step-2: Verify the functionality of Pre-Screen Notice
			Log.softAssertThat(checkout.elementLayer.verifyVerticalAllignmentOfElements(driver, "divAcquistionRebuttalBanner", "divPLCCPrescreenMessage", checkout), 
					"The prescreen notice should be displayed underneath Offer Content.", 
					"The prescreen notice is displayed underneath Offer Content.", 
					"The prescreen notice is not displayed underneath Offer Content.", driver);
			
			//Step-3: Verify the functionality of Legal Copy
			Log.softAssertThat(checkout.elementLayer.verifyVerticalAllignmentOfElements(driver, "divPLCCPrescreenMessage", "divPLCCLegalCopy", checkout), 
					"The Legal Copy section should be located underneath the prescreen notice.", 
					"The Legal Copy section is located underneath the prescreen notice.", 
					"The Legal Copy section is not located underneath the prescreen notice.", driver);
			
			String legalCopy = checkout.elementLayer.getAttributeForElement("lnkPLCCLegalCopyClickHere", "href", checkout);
			Log.softAssertThat(legalCopy.contains(Utils.getCurrentBrand().getConfiguration().toLowerCase())
							&& legalCopy.contains(redirectData.get("comenity_manage")), 
					"There should be a link in Legal Copy section that takes the user to the Financial Terms of the Account page.", 
					"There is a link in Legal Copy section that takes the user to the Financial Terms of the Account page.", 
					"There is not a link in Legal Copy section that takes the user to the Financial Terms of the Account page.", driver);
			
			//Step-5: Verify the functionality of Get It Today
			checkout.continueToPLCCStep2InPLCCACQ();
			Log.message(i++ + ". Clicked on GET IT TODAY", driver);
			
			Log.softAssertThat(checkout.elementLayer.VerifyElementDisplayed(Arrays.asList("mdlPLCCApprovalStep2"), checkout), 
					"On clicking Get It Today button, the system should invoke Step Two overlay and sets a session value to qualify the customer for the rebuttal promotion", 
					"Step Two overlay is invoked.", 
					"Step Two overlay is not invoked.", driver);
			
			Log.softAssertThat(checkout.verifyStepTwoPLCCVisibleFull(), 
					"There should be no portions of the Step 2 Overlay are cut off when user accepts the acquisition rebuttal offer. All of the PLCC application should be visible by the user.", 
					"Entire Step Two overlay is visoble to user.", 
					"Part of Step Two overlay is cut off from view of the user.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	} // M2_FBB_DROP5_C22780

}
