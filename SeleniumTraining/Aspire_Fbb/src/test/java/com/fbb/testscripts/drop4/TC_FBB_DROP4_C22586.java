package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22586 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22586(String browser) throws Exception {
		Log.testCaseInfo();
	
		// Load Test Data
		String searchKey = prdData.get("prd_variation");
		String username;
		String address[] = {"valid_address1", "valid_address7"};
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + searchKey, driver);
	
			String size=pdpPage.selectSizeSwatch(1);
			Log.message(i++ +"--->>>Selected size :: " + size);
	
			String color = pdpPage.selectColorSwatch(0);
			Log.message(i++ +"--->>>Selected Color :: " + color);
	
			pdpPage.clickAddProductToBag();
	
			ShoppingBagPage shoppingbagPage=pdpPage.clickOnCheckoutInMCOverlay();
			CheckoutPage checkout=shoppingbagPage.clickOnCheckoutNowBtn();
	
			SignIn signin = new SignIn(driver);
	
			signin.typeGuestEmail(username);
			Log.message(i++ + ". Typed Guest Email.", driver);
	
			CheckoutPage checkoutPage = signin.clickOnContinueInGuestLogin();
			Log.message(i++ + ". Clicked on continue and navigated to Checkout page.", driver);
			
			checkoutPage.fillingShippingDetailsAsGuest("NO", address[0], "Ground");
			checkout.checkUncheckUseThisAsBillingAddress(false);
			checkout.continueToPayment(false);
	
			checkout.waitforPaymentDetailstoLoad();
			//step-1: Verify the functionality of the elements present in the Billing Address module
			Log.softAssertThat(!(pdpPage.elementLayer.verifyPageElements(Arrays.asList("btnselectPaymentMethod"), checkout)),
					"Address Form fields and Select Payment Method Button should be Displayed", 
					"Address Form fields and Select Payment Method Button is Displayed", 
					"Address Form fields and Select Payment Method Button is not Displayed", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("firstName", "aria-required", "true", checkoutPage), 
					"First Name field should be a mandatory field.", 
					"First Name field  is a mandatory field.", 
					"First Name field is not a mandatory field.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("lastName", "aria-required", "true", checkoutPage), 
					"Last Name field should be a mandatory field.", 
					"Last Name field  is a mandatory field.", 
					"Last Name field is not a mandatory field.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("address1", "aria-required", "true", checkoutPage), 
					"Address1 field should be a mandatory field.", 
					"Address1 field  is a mandatory field.", 
					"Address1 field is not a mandatory field.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("postalCode", "aria-required", "true", checkoutPage), 
					"PostalCode field should be a mandatory field.", 
					"PostalCode field  is a mandatory field.", 
					"PostalCode field is not a mandatory field.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("city", "aria-required", "true", checkoutPage), 
					"City field should be a mandatory field.", 
					"City field  is a mandatory field.", 
					"City field is not a mandatory field.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("firstName", "firstNamePlaceholder", "fName", checkoutPage), 
					"To check the Firstname placeholder is going up when entering value.",
					"The Firstname placeholder is going up when entering value!",
					"The Firstname placeholder is not going up when entering value", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("lastName", "lastNamePlaceholder", "lName", checkoutPage),
					"To check the Lastname placeholder is going up when entering value.",
					"The Lastname placeholder is going up when entering value!",
					"The Lastname placeholder is not going up when entering value", driver);	
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("address1", "address1Placeholder", "address1", checkoutPage),
					"To check the Address1 placeholder is going up when entering value.",
					"The Address1 placeholder is going up when entering value!",
					"The Address1 placeholder is not going up when entering value", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("address2", "address2Placeholder", "address2", checkoutPage),
					"To check the Address2 placeholder is going up when entering value.",
					"The Address2 placeholder is going up when entering value!",
					"The Address2 placeholder is not going up when entering value", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("postalCode", "postalPlaceholder", "123456", checkoutPage),
					"To check the Postalcode placeholder is going up when entering value.",
					"The Postalcode placeholder is going up when entering value!",
					"The Postalcode placeholder is not going up when entering value", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("city", "cityPlaceholder", "City1", checkoutPage),
					"To check the City placeholder is going up when entering value.",
					"The City placeholder is going up when entering value!",
					"The City placeholder is not going up when entering value", driver);
			
			checkoutPage.enterFirstName("$#$%");
			checkoutPage.enterLastName("$#$%");
			checkoutPage.enterCity("A");
			checkoutPage.enterPostalCode("$#$%");
			checkoutPage.enterPhoneCode("###");
			checkoutPage.enterAddress1("Adress");
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyElementDisplayed(Arrays.asList("errorMsgPostalcode"
					,"errorMsgPhone","errorMsgFirstName","errorMsgcity","errorMsgLastName"), checkoutPage),
					"To check the invalid error message is displaying.",
					"The invalid error message is displaying!",
					"The invalid error message is not displaying", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "billingAddressFields", "sectionContinueBilling", checkoutPage),
					"To check the continue button is displayed below the billing address fields.",
					"The continue button is displayed below the billing address fields!",
					"The continue button is not displayed below the billing address fields", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyTxtElementsNotEmpty(Arrays.asList("firstName", "lastName", "address1"
					,"address2" , "postalCode", "city"), checkoutPage),
					"To check Billing fields are editable.",
					"Billing fields are editable!",
					"Billing fields are not editable", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}
}// search
