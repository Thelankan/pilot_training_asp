package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C23363 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C23363(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prodId = prdData.get("prd_review");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.clickOnReviewTab();
	
			if(runPltfrm.equals("mobile")) {
	
			} else {
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("sectionReviewVisible"), pdpPage), 
						"Check the review section is opened",
						"The review section is opened",
						"The review section is not opened", driver);
			}
			pdpPage.clickOnWriteAReview();
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("sectionWriteAReviewScreen"), pdpPage), 
					"Check the write a review screen is opened",
					"The write a review screen is opened",
					"The write a review screen is not opened - PXSFCC-3761", driver);
			try {
				pdpPage.clickOnReviewStar(1);
			} catch(Exception e) {
				Log.fail("The write a review screen is not opened for mobile - PXSFCC-3761");
			}
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("reviewRatingStar"), pdpPage), 
					"Check the write a review rating star is selected",
					"The user can select star how many stars the product should receive",
					"The user cannot be able to select star for the product", driver);
	
			pdpPage.clickOnFitRadioButton(1);
	
			pdpPage.clickOnProsAndConsCheckBox(1, 2);
	
			pdpPage.clickOnProsAndConsCheckBox(2, 3);
	
			pdpPage.enterWriteReviewTitle("Title");
	
			pdpPage.enterWriteReviewDesc();
	
			Log.softAssertThat(pdpPage.checkWriteReviewTitleAndDescAreNotEmpty(), 
					"Check the Review title and description is entered",
					"The Review title and description are entered",
					"The Review title and description are not entered", driver);
	
			Log.softAssertThat(pdpPage.checkOnProsAndConsCheckBoxSelected(2), 
					"Check the pros and cons can be selected more than one",
					"The pros and cons can be selected more than one",
					"Cannot able to select pros and cons more than one", driver);
	
			Log.softAssertThat(pdpPage.checkReviewFitRadioButtonIsSelected(), 
					"Check the Fit radio button can be selected",
					"The Fit radio button can be selected",
					"The Fit radio button cannot be able to select", driver);
	
			Log.message("The image and video functionality in 'Write a Review' modal cannot be verified");
	
			pdpPage.enterLocationInReview("Miami");
	
			pdpPage.clickReadReviewGuideLine();
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnCloseGuideLine"), pdpPage), 
					"Check the Guidelines for review is displaying",
					"The Guidelines for review is displaying",
					"The Guidelines for review is not displaying", driver);
	
			pdpPage.clickCloseGuideLine();
			Log.reference(i++ + ". Cannot submit review due to email validation required.");
			
			/*pdpPage.clickOnSubmitReview();
			Log.message(i++ + ". Clicked on Submit button in review modal.", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblReviewThankYou"), pdpPage), 
					"Check the review is submitted",
					"The review is submitted",
					"The review is not submitted", driver);*/
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_FBB_DROP2_C23363


}// search
