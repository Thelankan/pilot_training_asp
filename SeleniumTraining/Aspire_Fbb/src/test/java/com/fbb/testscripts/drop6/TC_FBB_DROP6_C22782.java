package com.fbb.testscripts.drop6;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.customerservice.ContactConfirmationPage;
import com.fbb.pages.customerservice.ContactUsPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP6_C22782 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();

	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP6_C22782(String browser) throws Exception{
		Log.testCaseInfo();
		
		//Get the webdriver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		//Test data
		String validEmail = accountData.get("valid_format_email");
		String invalidEmail = validEmail.split("@")[0];
		String invalidEmailError = demandwareData.get("EmailError");
		String fName = "First";
		String lName = "Last";
		String validZIP = "10016";
		String invalidZIP = "!@#$%";
		String invalidZIPError = demandwareData.get("ZIPError2");
		String topic = "Order Status";
		String subTopic = "Where's my order/backorder?";
		String message = subTopic;

		int i = 1;
		try
		{
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to FBB Home Page.", driver);

			CustomerService customerservice = homePage.headers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to Customer Service page.", driver);
			
			ContactUsPage contactUsPage = customerservice.clickOnMailWithUs();
			Log.message(i++ +". Navigated to Contact Us page.");
			
			//Step-1: Verify the functionality of Left Navigation
			Log.reference("Functionality of Left Navigation verified in test case C22794 step 1");
			
			//Step-2: Verify the functionality of Form Heading
			if(Utils.isMobile()) {
				Log.softAssertThat(contactUsPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "csPageHeadinglabelMobile", "csCurrentHeading", contactUsPage),
						"Form Heading should be displayed below the Customer Service Label",
						"Form Heading is displayed below the Customer Service Label",
						"Form Heading is not displayed below the Customer Service Label", driver);
			}else{
				Log.softAssertThat(contactUsPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "csPageHeadinglabel", "csCurrentHeading", contactUsPage),
						"Form Heading should be displayed below the Customer Service Label",
						"Form Heading is displayed below the Customer Service Label",
						"Form Heading is not displayed below the Customer Service Label", driver);
			}
			
			//Step-3: Verify the functionality of Contact Sub-Text
			Log.softAssertThat(contactUsPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "csCurrentHeading", "formSubText", contactUsPage)
							|| contactUsPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "csCurrentHeading", "formSubTextRM", contactUsPage),
					"Contact Sub-Text should be displayed below the Form heading.",
					"Contact Sub-Text is displayed below the Form heading.",
					"Contact Sub-Text is not displayed below the Form heading.", driver);
			
			//Step-4: Verify the display of Contact Form
			Log.softAssertThat(contactUsPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "formSubText", "formContactUS", contactUsPage)
							|| contactUsPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "formSubTextRM", "formContactUS", contactUsPage),
					"Contact Form should be displayed below the Contact Sub text.",
					"Contact Form is displayed below the Contact Sub text.",
					"Contact Form is not displayed below the Contact Sub text.", driver);
			
			Log.softAssertThat(contactUsPage.elementLayer.verifyElementTextContains("emailAddressPlaceHolder", "Email Address", contactUsPage)
							&& contactUsPage.elementLayer.verifyElementTextContains("firstNamePlaceHolder", "First Name", contactUsPage)
							&& contactUsPage.elementLayer.verifyElementTextContains("lastNamePlaceHolder", "Last Name", contactUsPage)
							&& contactUsPage.elementLayer.verifyElementTextContains("zipCodePlaceHolder", "Zip Code", contactUsPage)
							&& contactUsPage.elementLayer.verifyElementTextContains("categoryPlaceHolder", "Topic", contactUsPage)
							&& contactUsPage.elementLayer.verifyElementTextContains("subcategoryPlaceHolder", "Sub-Topic", contactUsPage)
							&& contactUsPage.elementLayer.verifyElementTextContains("messagePlaceHolder", "Your Message", contactUsPage),
					"Placeholder texts should be displayed correctly.",
					"Placeholder texts are displayed correctly.",
					"Placeholder texts are not displayed correctly.", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(contactUsPage.elementLayer.verifyVerticalAllignmentOfListOfElements(Arrays.asList("emailval", 
							"firstName", "lastName", "zipCodeValue", "contactusTopic", "contactusSubTopic", "contactusMessage"), contactUsPage),
						"All fields should be displayed vertically in order",
						"All fields are displayed vertically in order",
						"All fields are not displayed vertically in order", driver);
			}else {
				Log.softAssertThat(contactUsPage.elementLayer.verifyVerticalAllignmentOfListOfElements(Arrays.asList("emailval", 
							"firstName", "lastName", "zipCodeValue"), contactUsPage),
						"Column 1 fields should be displayed vertically in order",
						"Fields are displayed vertically in order",
						"Fields are not displayed vertically in order", driver);
			}
			
			//Step-5: Verify the functionality of Contact Form
			// Verify fields are empty by default
			Log.softAssertThat(contactUsPage.verifyFieldsDefaultEmpty(), 
					"For Unauthenticated User: Fields Default should be Empty", 
					"For Unauthenticated User: Fields Default is Empty", 
					"For Unauthenticated User: Fields Default is not Empty", driver);
			
			// Verify mandatory fields
			Log.softAssertThat(contactUsPage.elementLayer.verifyAttributeForListOfElement(Arrays.asList("emailval","firstName","lastName","zipCodeValue","contactusTopic","contactusSubTopic","contactusMessage"), "aria-required","true", contactUsPage), 
					"All Contact Us Fields should be mandatory", 
					"All Contact Us Fields are mandatory",
					"All Contact Us Fields are not mandatory",driver);
						
			// Verify error message for incorrect email address
			contactUsPage.fillContactUSForm(invalidEmail, fName, lName, invalidZIP, topic, subTopic, message);
			Log.message(i++ + ". Filled in Form", driver);
			BrowserActions.clickOnElementX(driver, "submitButton", contactUsPage, "Submit button");
			Log.softAssertThat(contactUsPage.getDisplayedEmailErrorMessage().equalsIgnoreCase(invalidEmailError), 
					"Appropriate Error message should be displayed when entering invalid Email Id", 
					"Appropriate Error message is displayed when entering invalid Email Id",
					"Appropriate Error message is not displayed when entering invalid Email Id", driver);

			Log.softAssertThat(contactUsPage.getDisplayedZipCodeErrorMessage().equals(invalidZIPError), 
					"Appropriate Error message should be displayed for invalid Zip code", 
					"Appropriate Error message is displayed for invalid Zip code",
					"Appropriate Error message is not displayed for invalid Zip code",driver);
			
			//Enter value in fields and verify field's placeholder text positions   
			Log.softAssertThat(contactUsPage.verifyFieldsEditable(), 
					"Fields should be editable", 
					"Fields is editable",
					"Fields is not editable", driver);
			
			contactUsPage.fillContactUSForm(validEmail, fName, lName, validZIP, topic, subTopic, message);
			Log.message(i++ + ". Filled in Form", driver);
			
			Log.softAssertThat(contactUsPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutPadding(driver, "emailAddressPlaceHolder", "emailval", contactUsPage), 
					"Email Address Place Holder text should be displayed at the top", 
					"Email Address Place Holder text is displayed at the top",
					"Email Address Place Holder text is not displayed at the top", driver);

			Log.softAssertThat(contactUsPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutPadding(driver, "firstNamePlaceHolder", "firstName", contactUsPage), 
					"firstName Place Holder text should be displayed at the top", 
					"firstName Place Holder text is displayed at the top",
					"firstName Place Holder text is not displayed at the top", driver);

			Log.softAssertThat(contactUsPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutPadding(driver, "lastNamePlaceHolder", "lastName", contactUsPage),
					"lastName Place Holder text should be displayed at the top", 
					"lastName Place Holder text is displayed at the top",
					"lastName Place Holder text is not displayed at the top", driver);

			Log.softAssertThat(contactUsPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutPadding(driver, "zipCodePlaceHolder", "zipCodeValue", contactUsPage),
					"zipCode Place Holder text should be displayed at the top", 
					"zipCode Place Holder text is displayed at the top",
					"zipCode Place Holder text is not displayed at the top", driver);

			Log.softAssertThat(contactUsPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutPadding(driver, "messagePlaceHolder", "contactusMessage", contactUsPage),
					"Message placeholder text should be displayed at the top", 
					"Message placeholder text is displayed at the top",
					"Message placeholder text is not displayed at the top", driver);
			
			//Step-6: Verify the display and functionality of Contact Module
			Log.reference("Step 6 covered in C22773 and C22774");
			
			//Step-7: Verify the functionality of Submit button
			ContactConfirmationPage contactConfirmPage = contactUsPage.clickOnSubmitButton();
			Log.message(i++ + ". Clicked on Submit button.", driver);

			Log.softAssertThat(contactConfirmPage.elementLayer.verifyPageElements(Arrays.asList("customerConfirmText"), contactConfirmPage),
					"customer Confirmation Text should be displayed.",
					"customer Confirmation displayed!",
					"customer Confirmation not displayed", driver); 

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e)
		{
			Log.exception(e, driver);
		} // Ending catch block
		finally{
			Log.endTestCase();
			driver.quit();
		}// Ending finally
	}//M1_FBB_DROP6_C22782
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP6_C22782(String browser) throws Exception{
		Log.testCaseInfo();
		
		//Get the webdriver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String username1=AccountUtils.generateEmail(driver);
		String password1_2=accountData.get("password_global");
		String username2=AccountUtils.generateEmail(driver).replace("1@", "2@");
		
		{
			GlobalNavigation.registerNewUser(driver, 1, 0, username1 + "|" + password1_2);
		}
		
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username2 + "|" + password1_2);
		}

		int i = 1;
		try
		{
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to FBB Home Page.", driver);

			CustomerService customerservice = homePage.headers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to Customer Service page.", driver);

			ContactUsPage contactUsPage = customerservice.clickOnMailWithUs();
			Log.message(i++ +". Navigated to Contact Us page.");
			
			Log.message(i++ + ". Authenticated User with default address", driver);

			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username1, password1_2, false);
			Log.message(i++ + ". Logged in Successfully.", driver);
			
			customerservice = myAcc.headers.navigateToCustomerService();
			Log.message(i++ + ". Navigate to customer service");

			customerservice.clickOnMailWithUs();
			Log.message(i++ + ". Navigated to Contact Us page");

			// Verify fields are empty by default
			Log.softAssertThat(contactUsPage.verifyFieldsDefaultEnteredValues(true), 
					"Authenticated User with default address is already saved Fields Default should be filled with default values", 
					"Fields Default is filled with default values", 
					"Fields Default is not filled with default values", driver);

			Log.softAssertThat(contactUsPage.verifyContactUsFieldsMandatory(), 
					"All Contact Us Fields should be mandatory", 
					"All Contact Us Fields are mandatory",
					"All Contact Us Fields are not mandatory", driver);

			if(Utils.isMobile()) {
				contactUsPage.headers.openCloseHamburgerMenu("open");
				Log.message(i++ +"Opened Hamburger menu.", driver);
			}

			myAcc = homePage.headers.navigateToMyAccount();
			SignIn signIn = myAcc.signOutAccount();
			
			Log.message(i++ +". Authenticated User without default address");
			
			myAcc = signIn.headers.navigateToMyAccount(username2, password1_2, false);
			Log.message(i++ + ". Logged in Successfully.", driver);
			
			customerservice = myAcc.headers.navigateToCustomerService();
			Log.message(i++ + ". Navigate to customer service");

			customerservice.clickOnMailWithUs();
			Log.message(i++ +". Navigated to Contact Us page", driver);

			Log.softAssertThat(contactUsPage.verifyFieldsDefaultEnteredValues(false), 
					"Authenticated User with default address not saved: Fields Default should be filled with default values", 
					"Fields Default is filled with default values", 
					"Fields Default is not filled with default values", driver);

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e)
		{
			Log.exception(e, driver);
		} // Ending catch block
		finally{
			Log.endTestCase();
			driver.quit();
		}// Ending finally
	}//M2_FBB_DROP6_C22782
	
}//TC_FBB_DROP6_C22782
