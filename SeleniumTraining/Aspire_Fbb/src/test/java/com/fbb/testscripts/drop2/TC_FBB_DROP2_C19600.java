package com.fbb.testscripts.drop2;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19600 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19600(String browser) throws Exception {
		Log.testCaseInfo();

		String[] searchKey = prdData.get("search_suggestion_terms").split("\\|");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			Headers header = homePage.headers;
			if(Utils.isMobile())
				header.clickOnSearchIconMobile();

			//Step-1: Verify search suggestion for 2 character
			header.typeTextInSearchField(searchKey[0]);
			Log.message(i++ + ". Typed text("+ searchKey[0] +") into primary search box!");

			Log.softAssertThat(header.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("autoSuggestPopup"), header),
					"Search suggestion should not be displayed below search box.",
					"Search suggestion not displayed below search box!",
					"Search suggestion displayed below search box.", driver);

			//Step-2: Verify search suggestion for 2+ or 3 character
			header.typeTextInSearchField(searchKey[1]);
			Log.message(i++ + ". Typed text("+ searchKey[1] +") into primary search box!");

			Log.softAssertThat(header.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkSearchOriginal"), header),
					"Letters typed into search box should appear highlighted in suggestions",
					"Letters typed into search box highlighted in suggestions!",
					"Letters typed into search box not highlighted in suggestions", driver);

			Log.softAssertThat(header.verifySearchPhraseInEnhancedSearch(searchKey[1]),
					"Left side of the search suggestion box,Letters typed into search box should be highlighted in suggestions.",
					"Left side of the search suggestion box,Letters typed into search box highlighted in suggestions!",
					"Left side of the search suggestion box,Letters typed into search box not highlighted in suggestions", driver);

			if(Utils.isMobile()){
				Log.softAssertThat(header.elementLayer.verifyVerticalAllignmentOfElements(driver, "divProductSuggestions", "divPhraseSuggestions", header), 
						"Brand/category suggestion displays on the bottom of search suggestions.", 
						"Brand/category suggestion displayed on the bottom of search suggestions.", 
						"Brand/category suggestion did not display on the bottom of search suggestions.", driver);
			}
			else {
				Log.softAssertThat(header.elementLayer.VerifyElementDisplayed(Arrays.asList("divProductSuggestions", "divPhraseSuggestions"), header)
						&& header.elementLayer.verifyHorizontalAllignmentOfElementsWithoutScroll(driver, "divProductSuggestions", "divPhraseSuggestions", header), 
						"Brand/category suggestion drawer displays to the left of search suggestions.", 
						"Brand/category suggestion drawer displayed to the left of search suggestions.", 
						"Brand/category suggestion drawer did not display to the left of search suggestions.", driver);
			}

			//Step-3: Verify product suggestions are displayed in suggestion box
			Log.softAssertThat(header.getProductNameFromSearchSuggestion().toString().toLowerCase().contains(searchKey[1].toLowerCase()),
					"Products that meet the search term should be displayed in Search suggestion box",
					"Products that meet the search term displayed in Search suggestion box", 
					"Search suggestion box not displayed matching products.", driver);

			//Step-4: Click on product image link from suggestion box
			Object obj = header.selectProductFromSearchSuggestion("2");
			Log.softAssertThat(obj.getClass().getName().contains("PdpPage"),
					"When clicking on the Product image, Page should be redirected to appropriate Product Details Page.",
					"Page redirected to appropriate Product Details Page.",
					"Page not redirected to appropriate Product Details Page.", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_DROP2_C19600


}// search


