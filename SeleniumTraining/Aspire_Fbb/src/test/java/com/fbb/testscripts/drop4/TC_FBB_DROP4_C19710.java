package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C19710 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C19710(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_gift_certificate = prdData.get("prd_gc_certificate");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		String credential = AccountUtils.generateEmail(driver) + "|test123@";
		//Pre-requisite - Account Should have more than one payment information
		{
			GlobalNavigation.registerNewUser(driver, 0, 2, credential);
		}
	
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.checkout(driver, prd_gift_certificate, i, credential);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			//Step-1: Verify the 'Signed in as' section of the Checkout page
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("divSignInSection", "background", "rgb(245, 245, 245) none repeat scroll 0% 0% / auto padding-box border-box", checkoutPage), 
					"The 'Signed in as' section heading should be displayed inside a boxed section", 
					"The 'Signed in as' section heading displayed inside a boxed section", 
					"The 'Signed in as' section heading not displayed inside a boxed section", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("divSignInSection", "background", "rgb(245, 245, 245) none repeat scroll 0% 0% / auto padding-box border-box", checkoutPage), 
					"Boxed section should be displayed with gray background", 
					"Boxed section displayed with gray background", 
					"Boxed section displayed with gray background", driver);
	
			Log.softAssertThat(checkoutPage.getEmailIDFromSignIn().equals(credential.split("\\|")[0]), 
					"The 'Signed in as' section contain the email address of the User who is placing the Order after a faint line break", 
					"The 'Signed in as' contains the email address of the User.", 
					"The 'Signed in as' section does not contain the email address of the User as expected.", driver);
	
			//Step-2: Verify the 'Shipping Details' section of the Checkout page
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divSignInSection", "divCheckoutShipping", checkoutPage),
					"The 'Shipping Details' heading should be displayed below the 'Signed in as' section", 
					"The 'Shipping Details' heading displayed below the 'Signed in as' section", 
					"The 'Shipping Details' heading not displayed below the 'Signed in as' section", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("divShippingSectionContent", "background", "auto padding-box border-box", checkoutPage), 
					"The shipping details should be present inside a gray boxed section",
					"The shipping details present inside a gray boxed section",
					"The shipping details not present inside a gray boxed section", driver);
	
			String emailVerification = "Email: automation@yopmail.com";
			Log.softAssertThat(checkoutPage.getEmailIDFromShippingSection().toLowerCase().equals(emailVerification.toLowerCase()), 
					"The 'Shipping Address' section should carry the email address of the User in the format: 'Email: <email address>'",
					"The 'Shipping Address' section have the email address of the User in the format: 'Email: <email address>'",
					"The 'Shipping Address' section doesn't carry the email address of the User in the format: 'Email: <email address>'", driver);
	
			Log.softAssertThat(checkoutPage.getEmailIDFromShippingSection().contains("automation@yopmail.com"), 
					"The email address in the Shipping Address section should be the same as that of the User's signed in email address",
					"The email address in the Shipping Address section is same as that of the User's signed in email address",
					"The email address in the Shipping Address section not same as that of the User's signed in email address", driver);
	
			Log.softAssertThat(checkoutPage.getShippingTypeInShippingDetails().equals("Delivery via email"), 
					"The 'Delivery Options' section should carry the text: 'Delivery via email' ",
					"The 'Delivery Options' section have the text: 'Delivery via email' ",
					"The 'Delivery Options' section doesn't carry the text: 'Delivery via email' ", driver);
	
			if(runPltfrm.equals("mobile")){
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblEmailInShippingDetails", "shippingType", checkoutPage), 
						"The 'Delivery Options' section should appear below the 'Shipping Address' section",
						"The 'Delivery Options' section appear below the 'Shipping Address' section",
						"The 'Delivery Options' section not appear below the 'Shipping Address' section", driver);
			}
	
			//Step-3: Verify the 'Payment Details' section of the Checkout page
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divCheckoutShipping", "divCheckoutBilling", checkoutPage), 
					"The 'Payment Details' section heading should be displayed below the 'Shipping Details' section",
					"The 'Payment Details' section heading displayed below the 'Shipping Details' section",
					"The 'Payment Details' section heading not displayed below the 'Shipping Details' section", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("divCheckoutBillingContent", "background", "rgb(245, 245, 245) none repeat scroll 0% 0% / auto padding-box border-box", checkoutPage), 
					"The sections 'Billing Address', 'Credit/Debit Card', 'Reward Certificate', 'Promo Code' and 'Gift Card' should be present inside a gray sectioned box",
					"The sections 'Billing Address', 'Credit/Debit Card', 'Reward Certificate', 'Promo Code' and 'Gift Card' should be present inside a gray sectioned box",
					"The sections 'Billing Address', 'Credit/Debit Card', 'Reward Certificate', 'Promo Code' and 'Gift Card' should be present inside a gray sectioned box", driver);
	
			//ER-4: Hold due to multiple scenario
			if(checkoutPage.elementLayer.VerifyElementDisplayed(Arrays.asList("btnselectPaymentMethod"), checkoutPage)){
	
				checkoutPage.fillingBillingDetailsAsSignedUser("NO", "NO", "valid_address7");
				Log.message(i++ + ". Billing Details entered successfully!", driver);
	
				checkoutPage.clickselectPaymentMethod();
				Log.message(i++ + ". Continued to Payment section.", driver);
				checkoutPage = new CheckoutPage(driver).get();
			}
	
			if(runPltfrm.equals("mobile")){
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkEditBilling", "lblBillingAddressHeading", checkoutPage), 
						"The 'Edit' link should be present on the right hand side of the 'Payment Details' section heading",
						"The 'Edit' link present on the right hand side of the 'Payment Details' section heading",
						"The 'Edit' link not present on the right hand side of the 'Payment Details' section heading", driver);
			}else{
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkEditBilling", "divCheckoutBilling", checkoutPage), 
						"The 'Edit' link should be present on the right hand side of the 'Payment Details' section heading",
						"The 'Edit' link present on the right hand side of the 'Payment Details' section heading",
						"The 'Edit' link not present on the right hand side of the 'Payment Details' section heading", driver);
			}
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divCheckoutBillingBlock", "divCheckoutPayment", checkoutPage), 
					"The 'Credit/Debit Card' section should be present under the 'Billing Address' section",
					"The 'Credit/Debit Card' section present under the 'Billing Address' section",
					"The 'Credit/Debit Card' section not present under the 'Billing Address' section", driver);
	
			checkoutPage.expandCollapseRewardCertSection("expand");
			Log.message(i++ + ". Reward Certificate section Expanded!", driver);
	
			checkoutPage.expandCollapsePromoCodeSection("expand");
			Log.message(i++ + ". Promo Code section Expanded!", driver);
	
			/*checkoutPage.expandCollapseGiftCardSection("expand");
			Log.message(i++ + ". Giftcard section Expanded!", driver);*/
	
			if(runPltfrm.equals("mobile")){
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfListOfElements(Arrays.asList("txtRewardCode","txtCouponCode"), checkoutPage), //,"txtGiftCardCode" 
						"The 'Reward Certificate', 'Promo Code' should be vertically arranged below each other",
						"The 'Reward Certificate', 'Promo Code' vertically arranged below each other",
						"The 'Reward Certificate', 'Promo Code' not vertically arranged below each other", driver);
			}else{
				Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("rewardCardRightSection","couponRightSection"), "float", "right", checkoutPage), //,"giftCardRightSection" 
						"The 'Reward Certificate', 'Promo Code' should appear in the right section of the gray box and display 'None'",
						"The 'Reward Certificate', 'Promo Code' appeared as expected",
						"The 'Reward Certificate', 'Promo Code' not appeared as expected", driver);
			}
	
			checkoutPage.expandCollapseRewardCertSection("collapse");
			Log.message(i++ + ". Reward Certificate section collapsed!", driver);
	
			checkoutPage.expandCollapsePromoCodeSection("collapse");
			Log.message(i++ + ". Promo Code section collapsed!", driver);
	
			/*checkoutPage.expandCollapseGiftCardSection("collapse");
			Log.message(i++ + ". Giftcard section collapsed!", driver);*/
	
			BrowserActions.scrollUp(driver);
	
			Log.assertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("savedCardsSection"), checkoutPage), 
					"The 'Credit/Debit Card' section should display the card details of the User in the expected format",
					"The 'Credit/Debit Card' section display the card details of the User in the expected format",
					"The 'Credit/Debit Card' section not display the card details of the User in the expected format", driver);
	
			//Need to confirm with FSD for behavior change.
			/*if(runPltfrm.equals("desktop")){
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "logoPaymentType", "lblCardNumberInMiniPayment", checkoutPage), 
						"The Card issuer logo should appear to the right of the Card's details",
						"The Card issuer logo appeared to the right of the Card's details",
						"The Card issuer logo not appear to the right of the Card's details", driver);
			}*/

			checkoutPage.selectSavedCard(1);
			checkoutPage.enterCVV("333");
			checkoutPage.continueToReivewOrder();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details", driver);
	
			//Step-4: Verify the 'Review & Place Order' section of the Checkout process
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divCheckoutBilling", "divCheckoutReview_PlaceOrder", checkoutPage), 
					"The 'Review & Place Order' should be present below the 'Payment Details' section",
					"The 'Review & Place Order' present below the 'Payment Details' section",
					"The 'Review & Place Order' not present below the 'Payment Details' section", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblCheckout_Review_Desclaimer_Message"), checkoutPage), 
					"The 'Review & Place Order' section should display a disclaimer message below the 'Review & Place Order' section heading",
					"The 'Review & Place Order' section displays a disclaimer message below the 'Review & Place Order' section heading",
					"The 'Review & Place Order' section not displays a disclaimer message below the 'Review & Place Order' section heading", driver);
	
			if(runPltfrm.equals("mobile")){
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnPlaceOrderInReview", "lblCheckout_Review_Desclaimer_Message", checkoutPage), 
						"The disclaimer message should appear below the 'Place Order' button",
						"The disclaimer message appears below the 'Place Order' button",
						"The disclaimer message not appears below the 'Place Order' button", driver);
			}else{
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnPlaceOrderInReview", "lblCheckout_Review_Desclaimer_Message", checkoutPage), 
						"The 'Place Order' button should be present to the right side of the disclaimer message",
						"The 'Place Order' button present to the right side of the disclaimer message",
						"The 'Place Order' button not present to the right side of the disclaimer message", driver);
			}
	
			if(runPltfrm.equals("mobile")){
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "headerReview_PlaceOrder", "btnPlaceOrderInReview", checkoutPage), 
						"The 'Place Order' button should displayed below Section heading.",
						"The 'Place Order' button displayed below Section heading.",
						"The 'Place Order' button not displayed below Section heading.", driver);
			}
	
			//Step-5: Verify the 'Items in the Shopping Bag' section of the Checkout process
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "headerReview_PlaceOrder", "headerShoppingCartList", checkoutPage), 
					"The 'Items in the Shopping Bag' section heading should be displayed below the disclaimer message in the 'Review & Place Order' section",
					"The 'Items in the Shopping Bag' section heading displayed below the disclaimer message in the 'Review & Place Order' section",
					"The 'Items in the Shopping Bag' section heading not displayed below the disclaimer message in the 'Review & Place Order' section", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkEditShoppingCart", "headerShoppingCartList", checkoutPage), 
					"An 'Edit' link should appear on the right side of the 'Items in the Shopping Bag' section heading",
					"An 'Edit' link appeared on the right side of the 'Items in the Shopping Bag' section heading",
					"An 'Edit' link not appeared on the right side of the 'Items in the Shopping Bag' section heading", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("tblShoppingCartItems"), checkoutPage), 
					"All the Gift Certificates added by the User should appear as individual line items in 'Items in the Shopping Bag' section",
					"All the Gift Certificates added by the User appeared as individual line items in 'Items in the Shopping Bag' section",
					"All the Gift Certificates added by the User not appeared as individual line items in 'Items in the Shopping Bag' section", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("firstItemInCartTable", "border-top", "1px solid rgb(221, 221, 221)", checkoutPage), 
					"The first item in the list should be present below a faint line break below the 'Items in the Shopping Bag' section heading",
					"The first item in the list present below a faint line break below the 'Items in the Shopping Bag' section heading",
					"The first item in the list not present below a faint line break below the 'Items in the Shopping Bag' section heading", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lnkGiftCardPrdNameInCartList","lblGCFromEmailInCartList","lblGCToEmailInCartList"), checkoutPage), //"lblGiftCardSkuInCartList", 
					"System should display, Gift Certificates heading, Gift card ID, From email address, To email address",
					"System displays, Gift Certificates heading, Gift card ID, From email address, To email address",
					"System not displays, Gift Certificates heading, Gift card ID, From email address, To email address", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblGCPersonalMsgInCartList"), checkoutPage), 
					"If user add Personalized Message it should display message, Delivery - Via Email.",
					"If user add Personalized Message it display message, Delivery - Via Email.",
					"If user add Personalized Message it not display message, Delivery - Via Email.", driver);
	
			//Step-6: Verify the display of the 'Order Summary' section in the Checkout page
			if(runPltfrm.equals("desktop")){
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "checkoutOrderSummary", "primaryContentInCheckout", checkoutPage), 
						"The 'Order Summary' section should appear to the top right side of the Checkout page",
						"The 'Order Summary' section appear to the top right side of the Checkout page",
						"The 'Order Summary' section not appear to the top right side of the Checkout page", driver);
			}else{
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "primaryContentInCheckout", "checkoutOrderSummary", checkoutPage), 
						"The 'Order Summary' section should be present below the 'Items in the Shopping Bag' section",
						"The 'Order Summary' section present below the 'Items in the Shopping Bag' section",
						"The 'Order Summary' section not present below the 'Items in the Shopping Bag' section", driver);
			}
	
			if(runPltfrm.equals("mobile")){
				Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("checkoutOrderTotalTable", "padding", "10px", checkoutPage), 
						"The 'Order Summary' section heading should be present inside the section box",
						"The 'Order Summary' section heading present inside the section box",
						"The 'Order Summary' section heading not present inside the section box", driver);
			}else if(runPltfrm.equals("desktop")){
				Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("checkoutOrderTotalTable", "padding", "25px", checkoutPage), 
						"The 'Order Summary' section heading should be present inside the section box",
						"The 'Order Summary' section heading present inside the section box",
						"The 'Order Summary' section heading not present inside the section box", driver);
			}else{
				if(Utils.getCurrentBrandShort().equals("ww")){
					Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("checkoutOrderTotalTable", "padding", "20px 20px 33px 19px", checkoutPage), 
							"The 'Order Summary' section heading should be present inside the section box",
							"The 'Order Summary' section heading present inside the section box",
							"The 'Order Summary' section heading not present inside the section box", driver);
				}else{
					Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("checkoutOrderTotalTable", "padding", "25px", checkoutPage), 
							"The 'Order Summary' section heading should be present inside the section box",
							"The 'Order Summary' section heading present inside the section box",
							"The 'Order Summary' section heading not present inside the section box", driver);
				}
			}
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("checkoutSummaryHeading", "border-bottom", "1px solid rgb(221, 221, 221)", checkoutPage), 
					"The 'Subtotal' field should be present after a faint line break below the section heading",
					"The 'Subtotal' field present after a faint line break below the section heading",
					"The 'Subtotal' field not present after a faint line break below the section heading", driver);
	
			Log.softAssertThat(checkoutPage.calculateSubtotalInCartList().equals(checkoutPage.getSubtotalInSummary()), 
					"The 'Subtotal' field should list the total price of all the products in the order",
					"The 'Subtotal' field list the total price of all the products in the order",
					"The 'Subtotal' field not list the total price of all the products in the order", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblShippingTypeInSummary"), checkoutPage), 
					"The 'Standard Shipping, USA' field should be present below the 'Subtotal' field",
					"The 'Standard Shipping, USA' field present below the 'Subtotal' field",
					"The 'Standard Shipping, USA' field not present below the 'Subtotal' field", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblShippingTypeInSummary", "lblSalesTaxInSummary", checkoutPage), 
					"The 'Estimated Sales Tax' field should appear below the 'Standard Shipping, USA' field and should display the value $0",
					"The 'Estimated Sales Tax' field appear below the 'Standard Shipping, USA' field and should display the value $0",
					"The 'Estimated Sales Tax' field not appear below the 'Standard Shipping, USA' field and should display the value $0", driver);
	
			if(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblRemainingTotalInSummary"), checkoutPage))
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("lblRemainingTotalInSummary", "border-top", "1px solid rgb(221, 221, 221)", checkoutPage), 
						"The 'Total' field should appear after a faint line break and display the 'Subtotal' field's value",
						"The 'Total' field appear after a faint line break and display the 'Subtotal' field's value",
						"The 'Total' field not appear after a faint line break and display the 'Subtotal' field's value", driver);
			}else{
				Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("lblOrderTotalInSummary", "border-top", "1px solid rgb(221, 221, 221)", checkoutPage), 
						"The 'Total' field should appear after a faint line break and display the 'Subtotal' field's value",
						"The 'Total' field appear after a faint line break and display the 'Subtotal' field's value",
						"The 'Total' field not appear after a faint line break and display the 'Subtotal' field's value", driver);
			}
			
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblReviewDesclaimerInSummary"), checkoutPage), 
					"Outside the section box, a disclaimer message should be displayed",
					"Outside the section box, a disclaimer message displayed",
					"Outside the section box, a disclaimer message not displayed", driver);
	
			if(runPltfrm.equals("tablet")){
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnPlaceOrderInOrderSummary", "lblReviewDesclaimerInSummary", checkoutPage), 
						"The 'Place Order' button should be present right side of the disclaimer message",
						"The 'Place Order' button present present right side of the disclaimer message",
						"The 'Place Order' button not present present right side of the disclaimer message", driver);
			}else{
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblReviewDesclaimerInSummary", "btnPlaceOrderInOrderSummary", checkoutPage), 
						"The 'Place Order' button should be present below the disclaimer message",
						"The 'Place Order' button present below the disclaimer message",
						"The 'Place Order' button not present below the disclaimer message", driver);
			}
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C19710
}// search
