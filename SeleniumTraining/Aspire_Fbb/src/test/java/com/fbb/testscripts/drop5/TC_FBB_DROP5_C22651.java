package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22651 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22651(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String product1 = prdData.get("prd_variation1");
		String product2 = prdData.get("prd_variation2");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
	
		//Prerequisite - Account Should have more than one payment information
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
	
		int i = 1;
		WishListPage wishlist = null;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAccount=homePage.headers.signInToMyAccountPage(username, password);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
			
			PdpPage pdp = myAccount.headers.navigateToPDP(product1);
			pdp.selectSize();
			pdp.addToWishlist();
			Log.message(i++ + ". Added 1st product to wishlist",driver);
			
			pdp.headers.navigateToPDP(product2);
			pdp.selectSize();
			pdp.addToWishlist();
			Log.message(i++ + ". Added 2nd product to wishlist",driver);
	
			wishlist=pdp.headers.navigateToWishListPage();
	
			boolean wishListEmpty= wishlist.elementLayer.VerifyElementDisplayed(Arrays.asList("lblWishListEmpty"), wishlist);
			//Step-1: Verify the functionality of Share Icons
			Log.softAssertThat(!wishListEmpty, 
					"Wish list is not empty, continuing to Step 1 tests.", 
					"Wishlist is empty, skipping Step 2 test", driver);
			if(!wishListEmpty) {
				Log.softAssertThat(wishlist.elementLayer.verifyVerticalAllignmentOfElements(driver, "wishlistHeading", "shareOptions", wishlist), 
						"Share Icons should be displayed below the My Wishlist Heading", 
						"Share Icons are displayed below the My Wishlist Heading", 
						"Share Icons are not displayed below the My Wishlist Heading", driver);
	
				Log.softAssertThat(wishlist.validateShareIcons(), 
						"Clicking on icons should lead to site specific modal.", 
						"Clicking on icons leads to site specific modal.", 
						"Clicking on icons does not lead to site specific modal.", driver);
			}
	
			// Step-2: Verify the functionality of Make List Public verifyTextEquals
			Log.softAssertThat(!wishListEmpty, "Wish list is not empty, continuing to Step 2 tests.", 
					"Wishlist is empty, skipping Step 3 test", driver);
	
			if(!wishListEmpty) {
				if(!Utils.isMobile()) {
					Log.softAssertThat(wishlist.elementLayer.verifyPageElements(Arrays.asList("btnMakeWishlistpublictoggle"), wishlist)
									&& wishlist.elementLayer.verifyTextEquals("btnMakeWishlistpublictoggle", "MAKE LIST PUBLIC", wishlist),
							"By default, System should display Make List Public",
							"By default, System displayed Make List Public",
							"By default, System did not display Make List Public", driver);
					
					wishlist.clickMakeWishlistPublicToggle("public");
					Log.softAssertThat((wishlist.elementLayer.verifyPageElements(Arrays.asList("btnMakeWishlistpublictoggle"), wishlist)
									&& wishlist.elementLayer.verifyTextEquals("btnMakeWishlistpublictoggle", "MAKE LIST PRIVATE", wishlist)),
							"When user clicks on Make List Public, then system should make Wishlist searchable and update link to Make List Private",
							"Link is updated to ‘Make List Private’", "Link is not updated to Make List Private", driver);
		
					boolean temp1 = wishlist.elementLayer.verifyElementTextWithDWREProp(Arrays.asList("btnMakeWishlistpublictoggle"), "wishlist.listpublic", wishlist);
					wishlist.clickMakeWishlistPublicToggle("private");
					Log.message(i++ + ". Made wishlist private", driver);
		
					Log.softAssertThat((wishlist.elementLayer.verifyPageElements(Arrays.asList("btnMakeWishlistpublictoggle"), wishlist)
									&& wishlist.elementLayer.verifyTextEquals("btnMakeWishlistpublictoggle", "MAKE LIST PUBLIC", wishlist)),
							"When user clicks on ‘Make List Private’, then system should remove wishlist from searchable index and revert link back to Make List Public.",
							"Link is updated to ‘Make List Public’", "Link is not updated to Make List Public", driver);
		
					// Text handled via the following property files from account.properties:
					// wishlist.makelistpublic and wishlist.makelistprivate
					boolean temp2 = wishlist.elementLayer.verifyElementTextWithDWREProp(Arrays.asList("btnMakeWishlistpublictoggle"), "wishlist.listprivate", wishlist);
					Log.softAssertThat(!(temp1 && temp2),
							"Create a Wishlist's Property should be handled by wishlist.createawishlist from account.properties.",
							"Create a Wishlist's Property is handled by wishlist.createawishlist from account.properties.",
							"Create a Wishlist's Property is not handled by wishlist.createawishlist from account.properties.", driver);
				}
				else {
					Log.softAssertThat(wishlist.elementLayer.verifyPageElements(Arrays.asList("btnMakeWishlistpublictoggleMobile"), wishlist)
									&& wishlist.elementLayer.verifyTextEquals("btnMakeWishlistpublictoggleMobile", "MAKE LIST PUBLIC", wishlist),
							"By default, System should display Make List Public",
							"By default, System displayed Make List Public",
							"By default, System did not display Make List Public", driver);
					
					wishlist.clickMakeWishlistPublicToggle("public");
					Log.message(i++ + ". Made wishlist public", driver);
					
					Log.softAssertThat((wishlist.elementLayer.verifyPageElements(Arrays.asList("btnMakeWishlistpublictoggleMobile"), wishlist)
									&& wishlist.elementLayer.verifyTextEquals("btnMakeWishlistpublictoggleMobile", "MAKE LIST PRIVATE", wishlist)),
							"When user clicks on Make List Public, then system should make Wishlist searchable and update link to Make List Private",
							"Link is updated to ‘Make List Private’", "Link is not updated to Make List Private", driver);
		
					boolean temp1 = wishlist.elementLayer.verifyElementTextWithDWREProp(Arrays.asList("btnMakeWishlistpublictoggleMobile"), "wishlist.listpublic", wishlist);
					wishlist.clickMakeWishlistPublicToggle("private");
					Log.message(i++ + ". Made wishlist private", driver);
		
					Log.softAssertThat((wishlist.elementLayer.verifyPageElements(Arrays.asList("btnMakeWishlistpublictoggleMobile"), wishlist)
									&& wishlist.elementLayer.verifyTextEquals("btnMakeWishlistpublictoggleMobile", "MAKE LIST PUBLIC", wishlist)),
							"When user clicks on ‘Make List Private’, then system should remove wishlist from searchable index and revert link back to Make List Public.",
							"Link is updated to ‘Make List Public’", "Link is not updated to Make List Public", driver);
		
					// Text handled via the following property files from account.properties:
					// wishlist.makelistpublic and wishlist.makelistprivate
					boolean temp2 = wishlist.elementLayer.verifyElementTextWithDWREProp(Arrays.asList("btnMakeWishlistpublictoggleMobile"), "wishlist.listprivate", wishlist);
					Log.softAssertThat(!(temp1 && temp2),
							"Create a Wishlist's Property should be handled by wishlist.createawishlist from account.properties.",
							"Create a Wishlist's Property is handled by wishlist.createawishlist from account.properties.",
							"Create a Wishlist's Property is not handled by wishlist.createawishlist from account.properties.", driver);
				}
			}
	
			//Step-3: Verify the functionality of Public URL 
			if(Utils.isMobile()) {
				Log.softAssertThat(wishlist.elementLayer.verifyVerticalAllignmentOfElements(driver, "shareOptions", "shareLinkUrl", wishlist),
						"Public URL should be displayed below the Share Icons", 
						"Public URL is displayed below the Share Icons", 
						"Public URL is not displayed below the Share Icons", driver);
			} else {
				Log.softAssertThat(wishlist.elementLayer.verifyHorizontalAllignmentOfElements(driver, "shareLinkUrl", "shareOptions", wishlist),
						"Public URL should be displayed right of the Share Icons", 
						"Public URL is displayed right of the Share Icons", 
						"Public URL is not displayed right of the Share Icons", driver);
			}
	
			//Step-4: Verify the functionality of Copy Link
			Log.softAssertThat(wishlist.elementLayer.verifyHorizontalAllignmentOfElements(driver, "copylink", "copyText", wishlist), 
					"Copy Link should be displayed right of the Public URL", 
					"Copy Link is displayed right of the Public URL", 
					"Copy Link is not displayed right of the Public URL", driver);
	
			if(Utils.isDesktop()) {
				Log.softAssertThat((wishlist.verifyCopyText()),
						"When user clicks on Copy link, then system should copy the public URL to the user’s clipboard. \n"
								+ "When user tries to paste the link in any new tab of the browser or any note pad, then system should be able to paste the copied link.", 
						"The system is able to copy the public URL to the user’s clipboard and paste the copied link", 
						"The system is not able to  copy the public URL to the user’s clipboard paste the copied link", driver);
			}
	
			//Step-5: Verify the functionality of Brand Logo
			Log.softAssertThat(!wishlist.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgProductItem", "imgItemBrand", wishlist),
					"Brand Logo should not be displayed on the bottom of the Item Image for each respective Item added", 
					"Brand Logo are not displayed on the bottom of the Item Images for each respective Item added", 
					"Brand Logo are displayed on the bottom of the Item Images for each respective Item added", driver);
	
			//Step-6: Verify the functionality of Make This Public
			if(!Utils.isMobile()) {
				Log.softAssertThat(!(wishlist.elementLayer.verifyElementTextContains("btnMakeWishlistpublictoggle","MAKE LIST PRIVATE",wishlist)),			 
						"By default, Make This Public checkbox should be checked.", 
						"By default, Make List Public is selected", 
						"By default, Make List Public is not selected", driver);
			}else {
				Log.softAssertThat(!(wishlist.elementLayer.verifyElementTextContains("btnMakeWishlistpublictoggleMobile","MAKE LIST PRIVATE",wishlist)),			 
						"By default, Make This Public checkbox should be checked.", 
						"By default, Make List Public is selected", 
						"By default, Make List Public is not selected", driver);
			}
	
			wishlist.clickMakeWishlistPublicToggle("private");
			Log.message(i++ + ". Made wishlist private.", driver);
	
			//Step-7: Verify the functionality of Edit
			if(Utils.isMobile()) {
				Log.softAssertThat(wishlist.elementLayer.verifyVerticalAllignmentOfElements(driver, "wishListItemName", "lnkEditMobile", wishlist),						 
						"Edit should be displayed below the Item added ", 
						"Edit is displayed below the Item added ", 
						"Edit is not displayed below the Item added ", driver);
			} else {
				Log.softAssertThat(wishlist.elementLayer.verifyVerticalAllignmentOfElements(driver, "wishListItemName", "lnkEdit", wishlist) &&
						wishlist.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkEdit", "wishListItemName", wishlist), 
						"Edit should be displayed on bottom right corner of the Item added ", 
						"Edit is displayed on bottom right corner of the Item added ", 
						"Edit is not displayed on bottom right corner of the Item added ", driver);
			}
	
			String initialSize = wishlist.getSelectedSize(0);
			int prdCount = wishlist.getItemCount();
			if(Utils.isDesktop()) {
				wishlist.clickEditLink(0);
				Log.message(i++ +". Clicked on edit link", driver);
		
				Log.softAssertThat(wishlist.elementLayer.VerifyElementDisplayed(Arrays.asList("divQuickShop"), wishlist), 
						"When user clicks on Edit link, it should user to Quickview.", 
						"Quickview is shown to user after clicking edit.", 
						"Quickview is not shown to user after clicking edit.", driver);
				
				if(wishlist.selectSizeInModal()) {
					Log.message(i++ +". Selected new size");
		
					wishlist.clickUpdateOnModal();
					Log.message(i++ +". Clicked on update on modal");
		
					Log.softAssertThat(!(wishlist.getSelectedSize(0).equals(initialSize)) , 
							"Original variation product should be replaced with new variation of product", 
							"Original variation product is replaced with new variation of product", 
							"Original variation product is not replaced with new variation of product", driver);
		
					wishlist.clickEditLink(0);
					Log.message(i++ +". Clicked on edit link", driver);
		
					wishlist.selectSizeInModal();
					Log.message(i++ +". Selected new size");
		
					wishlist.clickAddWishlistModal();
					Log.message(i++ +". Clicked on Wishlist in Modal");
		
					Log.softAssertThat(wishlist.getItemCount() != prdCount, 
							"New Variant should be added to wishlist.", 
							"New Variant is added to wishlist.", 
							"New Variant is not added to wishlist.", driver);
					
					//Step-8: Verify the functionality of Remove
					prdCount = wishlist.getItemCount();
					wishlist.clickRemoveLink(0);
					Log.message(i++ +". Clicked on Remove link", driver);
		
					Log.softAssertThat(prdCount != wishlist.getItemCount(), 
							"When user clicks on the remove link, the item should be removed from Wishlist.", 
							"When user clicks on the remove link, the item is removed from Wishlist.", 
							"When user clicks on the remove link, the item is not removed from Wishlist.", driver);
					}
				else {
					Log.message("Step 8 skipped due to error in previous step casuing failure of adding more product to wishlist.");
				}
			
				wishlist.clickOnEmailShare();
			}
			//Mobile and Tablet
			else {
				wishlist.clickEditLinkTabletMobile(0);
				Log.message(i++ +". Clicked on edit link", driver);
				if(wishlist.elementLayer.VerifyElementDisplayed(Arrays.asList("pdpMain"), wishlist)) {
					PdpPage prodPage = new PdpPage(driver);
					prodPage.clickSelectUnselectedSize();
					Log.message(i++ +". Selected different size.", driver);
					prodPage.clickOnUpdate();
					Log.message(i++ +". Clicked on Update button", driver);
					
					Log.event("Original size: "+ initialSize);
					Log.event("Updated size: "+ wishlist.getSelectedSize(0));
					
					Log.softAssertThat(!(wishlist.getSelectedSize(0).equals(initialSize)) , 
							"Original variation product should be replaced with new variation of product", 
							"Original variation product is replaced with new variation of product", 
							"Original variation product is not replaced with new variation of product", driver);
					
					wishlist.clickEditLinkTabletMobile(0);
					Log.message(i++ +". Clicked on edit link", driver);
					prodPage.clickSelectUnselectedSize();
					Log.message(i++ +". Selected different size", driver);
					prodPage.addToWishlist();
					Log.message(i++ +". Added to Wishlist", driver);
					
					Log.event("Original count: " + prdCount);
					Log.event("Updated count: " + wishlist.getItemCount());
					
					Log.softAssertThat(wishlist.getItemCount() != prdCount, 
							"New Variant should be added to wishlist.", 
							"New Variant is added to wishlist.", 
							"New Variant is not added to wishlist.", driver);
				}
				
				if(wishlist.getItemCount() != prdCount) {
					//Step-8: Verify the functionality of Remove
					prdCount = wishlist.getItemCount();
					wishlist.clickRemoveLink(0);
					Log.message(i++ +". Clicked on Remove link", driver);
		
					Log.softAssertThat(prdCount != wishlist.getItemCount(), 
							"When user clicks on the remove link, the item should be removed from Wishlist.", 
							"When user clicks on the remove link, the item is removed from Wishlist.", 
							"When user clicks on the remove link, the item is not removed from Wishlist.", driver);
				}else {
					Log.message("Step skipped due to failure in previous step.");
				}
				
			}
	
			//Step-9: Verify the functionality of No Longer Available
			Log.reference("Verify the functionality of No Longer Available covered in the test case TC22628 - Step 12");
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveWishListProducts(driver);
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}//TC_FBB_DROP5_C22651

}// search
