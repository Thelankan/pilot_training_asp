package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C21556 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C21556(String browser) throws Exception {
		Log.testCaseInfo();
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		String credential = AccountUtils.generateEmail(driver) + "|test123@";
		//Pre-requisite - Account Should have more than one payment information
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, credential);
		}
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home page.", driver);
	
			homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to My Account Page.", driver);		
	
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
			
			//Clicking in 
			PlpPage plppage = cartPage.clickShopNewArrivalButton();
			
			Log.softAssertThat(plppage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), plppage),
					"Clicking on the 'Shop What's New' button should take to the page of the respective brand as per configured in the content.", 
					"Clicking on the 'Shop What's New' button is take to the page of the respective brand as per configured in the content.", 
					"Clicking on the 'Shop What's New' button is not take to the page of the respective brand as per configured in the content.", driver);
	
			cartPage = homePage.headers.navigateToShoppingBagPage();
			
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("trendNowSlickContainer"), cartPage),
					"Trend Now container slot should be displayed in the empty shopping bag..", 
					"Trend Now container slot is displayed in the empty shopping bag.", 
					"Trend Now container slot is not displayed in the empty shopping bag.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "trendNowHeading", "trendNowSlickContainer", cartPage),
					"Trend Now image slick container should be displayed below the heading.",
					"Trend Now image slick container is displayed below the heading.",
					"Trend Now image slick container is not displayed below the heading.", driver);
			
			PdpPage pdppage =	cartPage.clickTrendNowImage();
			
			Log.softAssertThat(pdppage.elementLayer.verifyPageElements(Arrays.asList("cntPdpContent"), pdppage),
					"Clicking on the 'Trend Now' button should take to the page of the respective brand as per configured in the content.", 
					"Clicking on the 'Trend Now' button is take to the page of the respective brand as per configured in the content.", 
					"Clicking on the 'Trend Now' button is not take to the page of the respective brand as per configured in the content.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C19710
}// search
