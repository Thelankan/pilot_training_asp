package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22766 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22766(String browser) throws Exception {
		//Unauthenticated or Logged out user
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Footers footers = homePage.footers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			//Step-1: Please verify header & footer content at the same time
			Log.softAssertThat(homePage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "txtPLCCPromoDesktop", "divHomeInnerWrapper", homePage), 
					"Header Promotional Content should be displayed above banner.", 
					"Header Promotional Content is displayed above banner.", 
					"Header Promotional Content is not displayed above banner.", driver);
			
			String headerPromo = homePage.getPLCCPromoText().split("!1")[0].replaceAll("\\s+", "");
			String footerMarketing = homePage.getFooterPLCCText().split("!1")[0].replaceAll("\\s+", "");
			
			Log.softAssertThat(headerPromo.equalsIgnoreCase(footerMarketing), 
					"Marketing messages section should have the same message the header promo content.", 
					"Marketing messages section has the same message the header promo content.", 
					"Marketing messages section do not have the same message the header promo content.", driver);
			
			//Step-2: Verify the display of Header Content for Unknown Customer Groups - Unauthenticated User
			Log.softAssertThat(headers.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkApplyNow"), headers)
							&& footers.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkPlatinumCreditCard"), footers), 
					"System should display the Apply Now content to the user in the header/footer section of the Site", 
					"System displays Apply Now content to the user in the header/footer section of the Site", 
					"System do not display Apply Now content to the user in the header/footer section of the Site", driver);
			
			Log.softAssertThat(headers.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkLearnMore"), headers)
							&& footers.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkPlatinumCreditCardLanding"), footers), 
					"System should display the Learn More content to the user in the header/footer section of the Site", 
					"System displays Learn More content to the user in the header/footer section of the Site", 
					"System do not display Learn More content to the user in the header/footer section of the Site", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M1_FBB_DROP5_C22766
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP5_C22766(String browser) throws Exception {
		//Non pre-approved user
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, email + "|" + password);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Footers footers = homePage.footers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged into non pre-approved account.", driver);
			
			headers.navigateToHome();
			Log.message(i++ + ". Navigated to Homepage.", driver);
			
			//Step-1: Please verify header & footer content at the same time
			Log.softAssertThat(homePage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "txtPLCCPromoDesktop", "divHomeInnerWrapper", homePage), 
					"Header Promotional Content should be displayed above banner.", 
					"Header Promotional Content is displayed above banner.", 
					"Header Promotional Content is not displayed above banner.", driver);
			
			String headerPromo = homePage.getPLCCPromoText().split("!1")[0].replaceAll("\\s+", "");
			String footerMarketing = homePage.getFooterPLCCText().split("!1")[0].replaceAll("\\s+", "");
			
			Log.softAssertThat(headerPromo.equalsIgnoreCase(footerMarketing), 
					"Marketing messages section should have the same message the header promo content.", 
					"Marketing messages section has the same message the header promo content.", 
					"Marketing messages section do not have the same message the header promo content.", driver);
			
			//Step-3: Verify the display of Header Content for Unknown Customer Groups - Authenticated User
			Log.softAssertThat(headers.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkApplyNow"), headers)
							&& footers.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkPlatinumCreditCard"), footers), 
					"System should display the Apply Now content to the user in the header/footer section of the Site", 
					"System displays Apply Now content to the user in the header/footer section of the Site", 
					"System do not display Apply Now content to the user in the header/footer section of the Site", driver);
	
			Log.softAssertThat(headers.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkLearnMore"), headers)
							&& footers.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkPlatinumCreditCardLanding"), footers), 
					"System should display the Learn More content to the user in the header/footer section of the Site", 
					"System displays Learn More content to the user in the header/footer section of the Site", 
					"System do not display Learn More content to the user in the header/footer section of the Site", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M2_FBB_DROP5_C22766
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP5_C22766(String browser) throws Exception {
		//Pre-approved user
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String preapprovedCredentials = accountData.get("plcc_preapproved_" + Utils.getCurrentBrandShort().toString());
		String email = preapprovedCredentials.split("\\|")[0];
		String password = preapprovedCredentials.split("\\|")[1];
		String productID = prdData.get("prd_variation1");

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged into pre-approved account.", driver);
			
			headers.navigateToHome();
			Log.message(i++ + ". Navigated to Homepage.", driver);
			
			//Step-1: Please verify header & footer content at the same time
			Log.softAssertThat(homePage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "txtPLCCPromoDesktop", "divHomeInnerWrapper", homePage), 
					"Header Promotional Content should be displayed above banner.", 
					"Header Promotional Content is displayed above banner.", 
					"Header Promotional Content is not displayed above banner.", driver);
			
			String headerPromo = homePage.getPLCCPromoText().toLowerCase();
			String footerMarketing = homePage.getFooterPLCCText().toLowerCase();
			
			Log.softAssertThat(headerPromo.contains(footerMarketing.split("\\.")[0]) && Utils.getNumberInString(headerPromo) == Utils.getNumberInString(footerMarketing), 
					"Marketing messages section should have the same message the header promo content.", 
					"Marketing messages section has the same message the header promo content.", 
					"Marketing messages section do not have the same message the header promo content.", driver);
			
			//Step-4: Verify the display of Header Content for Pre-Approved Customers
			Log.softAssertThat(headerPromo.contains("accept now")
							&& footerMarketing.contains("accept now"), 
					"System should display the Accept Now content to the user in the header/footer section of the Site", 
					"System displays Accept Now content to the user in the header/footer section of the Site", 
					"System do not display Accept Now content to the user in the header/footer section of the Site", driver);
	
			Log.softAssertThat(headerPromo.contains("learn more")
							&& footerMarketing.contains("learn more"), 
					"System should display the Learn More content to the user in the header/footer section of the Site", 
					"System displays Learn More content to the user in the header/footer section of the Site", 
					"System do not display Learn More content to the user in the header/footer section of the Site", driver);
			
			//Step-9: Verify if the User is still able to see the Header content after rejecting the pre-approval
			PdpPage pdp = headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated to product", driver);
			
			pdp.addToBag();
			Log.message(i++ + ". Added product to bag.", driver);
			
			ShoppingBagPage shoppingBag = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag page.", driver);
			
			CheckoutPage checkout = shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigate to Checkout page", driver);
			
			checkout.closePLCCOfferByNoThanks1();
			Log.message(i++ + ". Clicked on No Thanks.", driver);
			
			checkout.clickOnContinue();
			Log.message(i++ + ". Clicked on Continue.", driver);
			
			checkout.openClosePLCCRebuttal("open");
			Log.message(i++ + ". Clicked Learn More to open acquisition rebuttal modal.", driver);
			
			checkout.closePLCCOfferByNoThanks();
			Log.message(i++ + ". Clicked on No Thanks");
			
			headers.navigateToHome();
			Log.message(i++ + ". Navigated to Homepage", driver);
			
			headerPromo = homePage.getPLCCPromoText().toLowerCase();
			Log.softAssertThat(headerPromo.contains("accept now")
							&& headerPromo.contains("learn more"), 
					"The User should be able to see the PLCC header content.", 
					"The User is able to see the PLCC header content", 
					"The User is not able to see the PLCC header content", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M3_FBB_DROP5_C22766
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP5_C22766(String browser) throws Exception {
		//PLCC card holder OTB
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String credentialsPLCCHolder = accountData.get("credential_saved_" + Utils.getCurrentBrandShort().toString());
		String email = credentialsPLCCHolder.split("\\|")[0];
		String password = credentialsPLCCHolder.split("\\|")[1];
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Footers footers = homePage.footers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged into PLCC holder account.", driver);
			
			headers.navigateToHome();
			Log.message(i++ + ". Navigated to Homepage.", driver);
			
			//Step-1: Please verify header & footer content at the same time
			Log.softAssertThat(homePage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "txtPLCCPromoDesktop", "divHomeInnerWrapper", homePage), 
					"Header Promotional Content should be displayed above banner.", 
					"Header Promotional Content is displayed above banner.", 
					"Header Promotional Content is not displayed above banner.", driver);
			
			String headerPromo = homePage.getPLCCPromoText().split("!")[0];
			String footerMarketing = homePage.getFooterPLCCText().split("!")[0];
			
			Log.softAssertThat(headerPromo.equalsIgnoreCase(footerMarketing), 
					"Marketing messages section should have the same message the header promo content.", 
					"Marketing messages section has the same message the header promo content.", 
					"Marketing messages section do not have the same message the header promo content.", driver);
			
			//Step-5: Verify the display of Header Content for Customer holding Current Brand PLCC with OTB > $0
			Log.softAssertThat(headers.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkRewardsHeader"), headers)
							&& footers.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkRewardsFooter"), footers), 
					"System should display the Rewards content to the user in the header/footer section of the Site", 
					"System displays Rewards content to the user in the header/footer section of the Site", 
					"System do not display Rewards content to the user in the header/footer section of the Site", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M4_FBB_DROP5_C22766
	
}
