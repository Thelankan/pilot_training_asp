package com.fbb.testscripts.drop4;

import java.util.Arrays;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22524 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22524(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String cardDetails = checkoutProperty.getProperty("card_MasterCard");
		String prd_any = prdData.get("prd_variation");
		String cardType = cardDetails.split("\\|")[0];
		String cardHolderName = cardDetails.split("\\|")[1];
		String cardNumber = cardDetails.split("\\|")[2];//
		String cardExpMonth = cardDetails.split("\\|")[3];
		String cardExpYear = cardDetails.split("\\|")[4];
		String ValidCVV = "333";
		String InValidCVV = "33";
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String credential = AccountUtils.generateEmail(driver) + "|test123@";
	
		//Pre-requisite - Account Should have more than one payment information
		{
			GlobalNavigation.registerNewUser(driver, 0, 2, credential, cardNumber);
		}
	
		int i = 1;
		try {
	
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, credential);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", "Ground", "valid_address7");
			Log.message(i++ + ". Entered shipping address", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Billing / Payment Section.", driver);
	
			checkoutPage.clickOnAddNewPaymentMethod();
			Log.message(i++ + ". Clicked on 'Add New Payment Method' button.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "rdoCreditCard", "sectionCardType", checkoutPage),
					"The Select Card Type dropdown should be displayed below the Credit Card radio button",
					"The Select Card Type dropdown should be displayed below the Credit Card radio button",
					"The Select Card Type dropdown should be displayed below the Credit Card radio button", driver);
			
			//Step-1: Verify the functionality of Select Credit Card Type dropdown
			checkoutPage.clickOnCreditCardDrp();
			Log.message(i++ + ". Clicked on Credit card Dropdown", driver);
	
			Log.softAssertThat(checkoutPage.getCardTypeSelectListState().equals("opened"), 
					"On clicking the Select Credit Card Type dropdown, System should display a drop menu of cards",
					"On clicking the Select Credit Card Type dropdown, System displays a drop menu of cards",
					"On clicking the Select Credit Card Type dropdown, System does not display a drop menu of cards", driver);
	
			Log.softAssertThat(checkoutPage.verifyCardOrder(), 
					"Cards should be sorted in the list first by non-PLCC by card name and then by PLCC by card name",
					"Cards sorted in the list first by non-PLCC by card name and then by PLCC by card name",
					"Cards not sorted in the list first by non-PLCC by card name and then by PLCC by card name", driver);
	
			checkoutPage.selectCardType(cardType);
			Log.message(i++ + ". Payment Method selected :: " + cardType);
	
			Log.softAssertThat(checkoutPage.getCreditCardType().replace(" ", "").equalsIgnoreCase(cardType.replace(" ", "")), 
					"When User select the Credit from the drop down, selected card name should display in the field",
					"When User select the Credit from the drop down, selected card name displays in the field",
					"When User select the Credit from the drop down, selected card name not display in the field", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("fldTxtNameOnCard","lblNameOnCard", cardHolderName, checkoutPage), 
					"The placeholder text should aligned top of the field when the User starts typing the data in Name on Card Field",
					"The placeholder text aligned top of the field when the User starts typing the data",
					"The placeholder text not aligned top of the field when the User starts typing the data", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("fldTxtCardNo","lblTxtCardNo", cardNumber, checkoutPage), 
					"The placeholder text should aligned top of the field when the User starts typing the data in Card Number",
					"The placeholder text aligned top of the field when the User starts typing the data",
					"The placeholder text not aligned top of the field when the User starts typing the data", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("fldTxtCvvNo","lblTxtCvvNo", ValidCVV, checkoutPage), 
					"The placeholder text should aligned top of the field when the User starts typing the data in CVV Field",
					"The placeholder text aligned top of the field when the User starts typing the data",
					"The placeholder text not aligned top of the field when the User starts typing the data", driver);
	
			//Step-2: Verify the functionality of the Name on Card field
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "selectCardType", "fldTxtNameOnCard", checkoutPage) ||
					checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "drpCardType", "fldTxtNameOnCard", checkoutPage),
					"The Name on Card field should be displayed below the 'Select Card Type' dropdown",
					"The Name on Card field should be displayed below the 'Select Card Type' dropdown",
					"The Name on Card field should be displayed below the 'Select Card Type' dropdown", driver);
			
			checkoutPage.setCardHolderNameInPayment(cardHolderName);
			Log.message(i++ + ". Card holder name given :: " + cardHolderName);
	
			Log.softAssertThat(checkoutPage.getCardHolderNameInPayment().equals(cardHolderName), 
					"User should be able to enter data into the Name on Card field",
					"User can able to enter data into the Name on Card field",
					"User not able to enter data into the Name on Card field", driver);
	
			checkoutPage.elementLayer.verifyTxtFldElementAllows("fldTxtNameOnCard", "special", checkoutPage, "Name on Card Field");
			boolean status = checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblNameOnCardError"), checkoutPage);
			Log.softAssertThat(status, 
					"The System should allow the User to enter only alphabets in Name field, Entry of numerals or special characters should be prohibited",
					"The System allows the User to enter only alphabets in this field, Entry of numerals or special characters prohibited",
					"User can able to enter other than alphabets, Entry of numerals or special characters not prohibited", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("fldTxtNameOnCard", "aria-required", "true", checkoutPage), 
					"This is a required field, so User will be unable to leave Name field as blank",
					"This is a required field, so User not able to leave this field as blank",
					"This is not a required field, so User able to leave this field as blank", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("fldTxtNameOnCard"), 26, checkoutPage), 
					"The length of the name should not exceed 26 characters",
					"The length of the name not exceed 26 characters",
					"The length of the name exceed 26 characters", driver);
	
			//Step-3: Verify the functionality of Card Number
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldTxtNameOnCard", "fldTxtCardNo", checkoutPage),
					"The Card Number field should be displayed below the Name on Card field",
					"The Card Number field should be displayed below the Name on Card field",
					"The Card Number field should be displayed below the Name on Card field", driver);
			
			checkoutPage.setCardNumberInPayment(cardNumber);
			Log.message(i++ + ". Entered card number :: " + cardNumber);
	
			Log.softAssertThat(checkoutPage.getCardNumberInPayment().equals(cardNumber), 
					"User should be able to enter the number on the card that the User is adding",
					"User can able to enter the number on the card that the User is adding",
					"User not able to enter the number on the card that the User is adding", driver);
	
			Log.softAssertThat((!checkoutPage.elementLayer.verifyTxtFldElementAllows("fldTxtCardNo", "special", checkoutPage, "Card Number Field") &&
					!checkoutPage.elementLayer.verifyTxtFldElementAllows("fldTxtCardNo", "alphabet", checkoutPage, "Card Number Field")), 
					"The System should allow the User to enter only numbers in this field, Entry of alphabets or special characters should be prohibited",
					"The System allows the User to enter only numbers in this field, Entry of alphabets or special characters prohibited",
					"User can able to enter other than numbers, Entry of alphabets or special characters Not prohibited", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("fldTxtCardNo", "aria-required", "true", checkoutPage), 
					"This is a required field, so User will be unable to leave this field as blank",
					"This is a required field, so User not able to leave this field as blank",
					"This is not a required field, so User able to leave this field as blank", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("fldTxtCardNo"), 16, checkoutPage), 
					"The length of the card number should not exceed 16 digits",
					"The length of the card number exceed 16 digits",
					"The length of the card number not exceed 16 digits", driver);
	
			//Step-4: Verify the functionality of Expiration Month 
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldTxtCardNo", "selectExpMonth", checkoutPage) ||
					checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldTxtCardNo", "drpExpMonth", checkoutPage),
					"Expiration Month dropdown should be displayed below the Card Number field",
					"Expiration Month dropdown should be displayed below the Card Number field",
					"Expiration Month dropdown should be displayed below the Card Number field", driver);
			
			checkoutPage.clickOnExpiryMonthDrp();
			Log.message(i++ + ". clicked on Expiry Month drop down.", driver);
	
			Log.softAssertThat(checkoutPage.verifyExpMonthDrp(), 
					"On clicking the Expiration Month drop down, System should display a drop menu of months from 1 to 12 (top down)",
					"On clicking the Expiration Month drop down, System displays a drop menu of months from 1 to 12 (top down)",
					"On clicking the Expiration Month drop down, System not displays a drop menu of months from 1 to 12 (top down)", driver);
	
			checkoutPage.selectExpiryMonth(cardExpMonth);
			Log.message(i++ + ". Selected Expiry Month :: "+ cardExpMonth);
	
			Log.softAssertThat(checkoutPage.getExpiryMonth().equalsIgnoreCase(cardExpMonth), 
					"When User selects the Expiration Month from the drop down, selected Expiration Month should display in the field",
					"When User selects the Expiration Month from the drop down, selected Expiration Month displays in the field",
					"When User selects the Expiration Month from the drop down, selected Expiration Month not display in the field", driver);
	
			//Step-5: Verify the functionality of Expiration Year 
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "sectionExpYear", "sectionExpMonth", checkoutPage),
					"Expiration Year dropdown should be displayed to the right of the Expiration Month dropdown.",
					"Expiration Year dropdown should be displayed to the right of the Expiration Month dropdown.",
					"Expiration Year dropdown should be displayed to the right of the Expiration Month dropdown.", driver);
			
			checkoutPage.clickOnExpiryYearDrp();
			Log.message(i++ + ". clicked on Expiry Year drop down.", driver);
	
			Log.softAssertThat(checkoutPage.verifyExpYearDrp(), 
					"On clicking the Expiration Year drop down, System should display a menu of years - current year plus 11 years (top down)",
					"On clicking the Expiration Year drop down, System displays a menu of years - current year plus 11 years (top down)",
					"On clicking the Expiration Year drop down, System not display a menu of years - current year plus 11 years (top down)", driver);
	
			checkoutPage.selectExpiryYear(cardExpYear);
			Log.message(i++ + ". Selected Expiry Year :: "+ cardExpYear);
	
			Log.softAssertThat(checkoutPage.getExpiryYear().equals(cardExpYear), 
					"When User selects the Expiration Year from the drop down, selected Expiration Year should display in the field",
					"When User selects the Expiration Year from the drop down, selected Expiration Year displays in the field",
					"When User selects the Expiration Year from the drop down, selected Expiration Year not displays in the field", driver);
	
			//Step-6: Verify the functionality of CVV field 
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "selectExpMonth", "fldTxtCvvNo", checkoutPage) ||
					checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "drpExpMonth", "fldTxtCvvNo", checkoutPage),
					"The CVV field should be displayed below the Expiration Month dropdown",
					"The CVV field should be displayed below the Expiration Month dropdown",
					"The CVV field should be displayed below the Expiration Month dropdown", driver);
			
			checkoutPage.setCVVInPayment(InValidCVV);
			Log.message(i++ + ". Invalid CVV("+ InValidCVV +") entered");
	
			Log.softAssertThat(checkoutPage.getCVVInPayment().equals(InValidCVV), 
					"The User should be able to enter the card's CVV for the card being added",
					"The User able to enter the card's CVV for the card being added",
					"The User not able to enter the card's CVV for the card being added", driver);
	
			Log.softAssertThat((!checkoutPage.elementLayer.verifyTxtFldElementAllows("fldTxtCvvNo", "special", checkoutPage, "CVV number Field") &&
					!checkoutPage.elementLayer.verifyTxtFldElementAllows("fldTxtCvvNo", "alphabet", checkoutPage, "CVV Number Field")), 
					"The System should allow the User to enter only numbers in this field, Entry of alphabets or special characters should be prohibited",
					"The System allows the User to enter only numbers in this field, Entry of alphabets or special characters prohibited",
					"User can able to enter other than numbers, Entry of alphabets or special characters not prohibited", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("fldTxtCvvNo", "aria-required", "true", checkoutPage), 
					"This is a required field, so User will be unable to leave this field as blank",
					"This is a required field, so User not able to leave this field as blank",
					"This is not a required field, so User able to leave this field as blank", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("fldTxtCvvNo"), 4, checkoutPage), 
					"The length of the card number should not exceed 4 digits",
					"The length of the card number exceed 4 digits",
					"The length of the card number not exceed 4 digits", driver);
	
			//Step-7: Verify the functionality of Save Card checkbox 
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldTxtCvvNo", "chkBoxSaveCard", checkoutPage),
					"The Save Card check box should be displayed below the CVV field",
					"The Save Card check box should be displayed below the CVV field",
					"The Save Card check box should be displayed below the CVV field", driver);
			
			//Hold (a) due to verification in MyAccount -> Payments Page - verified at last
			checkoutPage.checkMakeThisDefaultPaymentChkBox(true);
			Log.message(i++ + ". Make This Default Payment checkbox checked.", driver);
	
			checkoutPage.checkMakeSaveCardChkBox(false);
			Log.message(i++ + ". Tried to un-check Save this card checkbox.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsChecked(Arrays.asList("chkBoxSaveCard"), checkoutPage), 
					"User should not be able to deselect the save card option if the 'Make it default' checkbox is selected",
					"User not be able to deselect the save card option if the 'Make it default' checkbox is selected",
					"User able to deselect the save card option if the 'Make it default' checkbox is selected", driver);
	
			//Step-8: Verify the functionality of Make Default 
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "chkBoxSaveCard", "chkBoxMakeThisDefaultPayment", checkoutPage),
					"The Make Default checkbox should be displayed below the Save Card checkbox.",
					"The Make Default checkbox should be displayed below the Save Card checkbox.",
					"The Make Default checkbox should be displayed below the Save Card checkbox.", driver);
			
			//(a) verification in MyAccount -> Payments Page - verified at last
			//(b) already completed in Step-7(B)
			//Hold (c) due to not applicable in this scenario
	
			//Step-9: Verify the functionality of Payment Methods Accepted element  
			
			if(Utils.isMobile())
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "contPaymentMethodsAccepted", "selectCardType", checkoutPage) ||
						checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "contPaymentMethodsAccepted", "drpCardType", checkoutPage), 
						"Payment Methods Accepted section should be displayed to the right of the Select Card Type dropdown",
						"Payment Methods Accepted section should be displayed to the right of the Select Card Type dropdown",
						"Payment Methods Accepted section should be displayed to the right of the Select Card Type dropdown", driver);
			}
			else
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "contPaymentMethodsAccepted", "selectCardType", checkoutPage) ||
						checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "contPaymentMethodsAccepted", "drpCardType", checkoutPage), 
						"Payment Methods Accepted section should be displayed to the right of the Select Card Type dropdown",
						"Payment Methods Accepted section should be displayed to the right of the Select Card Type dropdown",
						"Payment Methods Accepted section should be displayed to the right of the Select Card Type dropdown", driver);
			}
	
			//Step-10: Verify the functionality of Save link
			/*Log.softAssertThat(!checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkSaveInPayments", "chkBoxSaveCard", checkoutPage), 
					"The Save link should be displayed to the right of the Save Card checkbox",
					"The Save link should be displayed to the right of the Save Card checkbox",
					"The Save link should be displayed to the right of the Save Card checkbox", driver);*/
			Log.reference(i++ + ". Save link not available in Add New Payment method Section.");
			
			checkoutPage.continueToReivewOrder();
			Log.message(i++ + ". Clicked on Continue in Payments Section.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblNameOnCardError"), checkoutPage), 
					"If the data is not valid system should display error message",
					"If the data is not valid system displays error message",
					"If the data is not valid system not displays error message", driver);
	
			//Step-12:a
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "chkBoxMakeThisDefaultPayment", "savedCardsSection", checkoutPage),
					"The Saved Payment Instruments should be displayed below the Make it Default checkbox",
					"The Saved Payment Instruments should be displayed below the Make it Default checkbox",
					"The Saved Payment Instruments should be displayed below the Make it Default checkbox", driver);
			
			checkoutPage.clickOnCancelLinkInPayments();
			Log.message(i++ + ". Clicked on Cancel link In Add New Payment method.");
			
			//Step-11: 
			Log.softAssertThat(!checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkCancelInPayments", "selectCardType", checkoutPage), 
					"The Cancel link should be displayed to the right of Save link",
					"The Cancel link should be displayed to the right of Save link",
					"The Cancel link should be displayed to the right of Save link", driver);
			//ER-2
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("divNewCardSection", "display", "none", checkoutPage), 
					"On clicking the Cancel link, the System should discard all the entered information and collapses the Add new card form",
					"On clicking the Cancel link, the System discarded all the entered information and collapses the Add new card form",
					"On clicking the Cancel link, the System did not discard all the entered information or not collapses the Add new card form", driver);
	
			checkoutPage.clickOnAddNewPaymentMethod();
			Log.message(i++ + ". Clicked on Add New Payment method link.", driver);
	
			//Step-12: ER-2
			HashMap<String, String> cardInfo = checkoutPage.selectSavedCard();
			Log.message(i++ + ". Selected Saved Card :: " + cardInfo.get("CardType") + " - " + cardInfo.get("CardNumber"), driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("divNewCardSection", "display", "none", checkoutPage), 
					"Clicking on any card in the Saved Payment Instruments section should replace any information entered in the Add New Card form fields with the data saved by the User for the selected card",
					"Clicking on any card in the Saved Payment Instruments section replaces any information entered in the Add New Card form fields with the data saved by the User for the selected card",
					"Clicking on any card in the Saved Payment Instruments section not replaces any information entered in the Add New Card form fields with the data saved by the User for the selected card", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "savedPaymentCount", "savedCardsSection", checkoutPage),
					"The Count of Payment Methods will appear on top of the Saved Payments Instrument module",
					"The Count of Payment Methods will appear on top of the Saved Payments Instrument module",
					"The Count of Payment Methods will appear on top of the Saved Payments Instrument module", driver);
			
			checkoutPage.getNoOfSavedCards();
			//Step-11: Verify the functionality of Cancel link
			checkoutPage.clickOnAddNewPaymentMethod();
			Log.message(i++ + ". Clicked on Add New Payment method link.", driver);
	
			//The below code is for Debugging purpose only
			/*HashMap<String, String> cardInfo1 = checkoutPage.fillingCardDetails1("card_Visa",true,true);
			Log.message(i++ + ". Entered valid Card details :: " + cardInfo1.get("CardType") + " - " + cardInfo1.get("CardNumber"), driver);
	
			//checkoutPage.setCVVInPayment(ValidCVV);
			//Log.message(i++ + ". Valid CVV filed.", driver);
	
			checkoutPage.clickOnSaveLinkInPayments();
			Log.message(i++ + ". Clicked on Save link in Add New Payment Methods", driver);*/
	
			/*for(int x=1; x <= 4 && checkoutPage.getNoOfSavedCards() != (noOfSavedCards + 1); x++){
				HashMap<String, String>cardInfo1 = checkoutPage.fillingCardDetails1("cards_"+x,true,true);
				Log.message(i++ + ". Entered valid Card details :: " + cardInfo1.get("CardType") + " - " + cardInfo1.get("CardNumber"), driver);
	
				checkoutPage.clickOnSaveLinkInPayments();
				Log.message(i++ + ". Clicked on Save link in Add New Payment Methods", driver);
			}*/
	
			checkoutPage.fillingCardDetails1("card_Visa",true,true);
			Log.message(i++ + ". Entered valid Card details", driver);
			
			checkoutPage.continueToReivewOrder();
			Log.message(i++ + ". Clicked on Continue in Payments Section.", driver);
			
			checkoutPage.clickEditPaymentDetail();
			Log.message(i++ + ". Clicked on edit link in Payments Section.", driver);
			
			/*Log.assertThat(checkoutPage.getNoOfSavedCards() == (noOfSavedCards + 1), 
					"If the data is valid, the newly entered payment instrument details should be added to the saved payment instrument list",
					"If the data is valid, the newly entered payment instrument details added to the saved payment instrument list",
					"If the data is valid, the newly entered payment instrument details not added to the saved payment instrument list", driver);*/
			Log.reference(i++ + ". Since Save button not available in functionality, skipping validation:: If the data is valid, the newly entered payment instrument details should be added to the saved payment instrument list");
	
			Log.reference(i++ + ". Step-11: Functionality of Save Link validations ignored due to <a href='https://fullbeautybrands.atlassian.net/browse/PXSFCC-5413'>PXSFCC-5413</a>");
	
			//Step-12: Verify the functionality of Saved Payment Instruments
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("savedCardsSection", "innerHTML", "input", checkoutPage), 
					"The User should be allowed to select any card present in the Saved Payment Instruments section",
					"The User allowed to select any card present in the Saved Payment Instruments section",
					"The User not allowed to select any card present in the Saved Payment Instruments section", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP4_C22524
}// search