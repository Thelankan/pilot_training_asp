package com.fbb.testscripts.drop2;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19593 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "critical", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19593(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		//Load test data
		String globalNavigation = prdData.get("levels");
		String index = prdData.get("prod_index");
		String[] levels = globalNavigation.split("\\|");
		
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, email + "|" + password);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			PlpPage plp = null;
			
			if(levels.length == 1) {
				plp = headers.navigateTo(levels[0]);
			} else if (levels.length == 2) {
				plp = headers.navigateTo(levels[0], levels[1]);
			} else {
				plp = headers.navigateTo(levels[0], levels[1], levels[2]);
			}
			Log.message(i++ + ". Navigated to " + levels[levels.length - 1] + " PLP", driver);
			
			//Step-1: Verify the number of products displayed in Product Listing Page on initial load
			Log.softAssertThat(plp.getProductTileCount() <= 60, 
					"The Product Listing Page should show only up to 60 products on initial load", 
					"The Product Listing Page shows less than or 60 products on initial load", 
					"The Product Listing Page shows more than 60 products on initial load", driver);
			
			//Step-2: Verify the display of Breadcrumb in the Product Listing Page
			Log.softAssertThat(plp.elementLayer.verifyVerticalAllignmentOfElements(driver, "divBreadcrumb", "divContentSlotHeader", plp), 
					"Breadcrumb should be displayed above slot cat-banner.", 
					"Breadcrumb is displayed above slot cat-banner.", 
					"Breadcrumb is not displayed above slot cat-banner.", driver);
			
			Log.softAssertThat(headers.getBreadCrumbText().startsWith("HOME"), 
					"The build of brand category root should be displayed as 'HOME'.", 
					"The build of brand category root is displayed as 'HOME'.", 
					"The build of brand category root is not displayed as 'HOME'.", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyCssPropertyForPsuedoElement("breadcrumbElement", ":after", "content", "/", headers), 
					"Categories in the brand category root should be delimited with the symbol '/'.", 
					"Categories are correctly delimited.", 
					"Categories are not correctly delimited.", driver);
			
			Log.softAssertThat(headers.getLastBreadcrumb().toLowerCase().contains(levels[levels.length - 1].toLowerCase()), 
					"Category name should be displayed as the breadcrumb value.", 
					"Category name is displayed as the breadcrumb value.", 
					"Category name is not displayed as the breadcrumb value.", driver);
			
			//Step-3:
			Log.softAssertThat(plp.elementLayer.verifyAttributeForElement("categoryBannerText", "innerHTML", levels[levels.length - 1], plp), 
					"Banner should be displayed with the corresponding category information ", 
					"Banner is displayed with the corresponding category information ", 
					"Banner is not displayed with the corresponding category information ", driver);
			
			Log.softAssertThat(plp.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "categoryBannerText", "divHorizontalRefinement", plp), 
					"Banner should be displayed on top of the horizontal refinement.", 
					"Banner is displayed on top of the horizontal refinement.", 
					"Banner is not displayed on top of the horizontal refinement.", driver);
			
			//Step-4:
			Log.softAssertThat(plp.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divHorizontalRefinement", "lblResultCountTablet", plp), 
					"Product count should be displayed below the horizontal refinement bar", 
					"Product count is displayed below the horizontal refinement bar", 
					"Product count is not displayed below the horizontal refinement bar", driver);
			
			if(Utils.isTablet()) {
				Log.softAssertThat(plp.elementLayer.verifyCssPropertyForElement("lblResultCountTablet", "text-align", "center", plp), 
						"Product count should be displayed centered.", 
						"Product count is displayed centered.", 
						"Product count is not displayed centered.", driver);
			}
			
			//Step-5, 6, 7
			Log.message("<br>"+i++ +". Steps 5, 6, 7 are ad placements configured in BM.");
			
			//Step-8:
			BrowserActions.scrollToBottomOfPage(driver);
			Log.softAssertThat(Integer.parseInt(plp.elementLayer.getElementCSSValue("btnBackToTop", "right", plp).replaceAll("[^0-9]", "")) >=
							Integer.parseInt(plp.elementLayer.getElementCSSValue("divProductTileContainer", "padding-right", plp).replaceAll("[^0-9]", "")), 
					"Back to Top button should be displayed at right side of the product grid.", 
					"Back to Top button is displayed at right side of the product grid.", 
					"Back to Top button is not displayed at right side of the product grid.", driver);
			
			plp.clickBackToTopButton();
			Log.message(i++ + ". Clicked on the Back To Top Button", driver);
			Log.softAssertThat(plp.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("btnBackToTop"), plp),
					"Back To Top button should not be visible.",
					"Back To Top button is not visible.", 
					"Back To Top button is visible.", driver);
			
			//Step-9: Verify Product tile in product list page
			Log.softAssertThat(plp.getNumberOfProductTilesPerRow() == 3, 
					"Only 3 product tiles should be displayed per row.", 
					"Only 3 product tiles are displayed per row.", 
					"3 product tiles are not displayed per row.", driver);
			
			Log.softAssertThat(plp.verifyProductTileContent(), 
					"Product tiles should load with correct elements.", 
					"Product tiles loaded with correct elements.", 
					"Product tiles didn't load with correct elements.", driver);
			
			//Step-10: Verify click on color swatches
			Log.softAssertThat(plp.verifyProductImageUpdate(Integer.parseInt(index)), 
					"Main image should update based on the selected color.", 
					"Main image updated based on the selected color.", 
					"Main image did not update based on the selected color.", driver);
			
			//Step-11: Verify click on product main image or product name
			plp.clickProductByIndex(1);
			Utils.waitForPageLoad(driver);
			Log.softAssertThat(new PdpPage(driver).get().getPageLoadStatus(), 
					"System should redirect user to the PDP page.", 
					"System redirected user to the PDP page.", 
					"System did not redirect user to the PDP page.", driver);
			driver.navigate().back();
			
			//Step-12: Verify "View More" button in Product list page
			if(plp.getProductTileCount() == 60) {
				Log.softAssertThat(plp.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionSearchResults", "btnViewMoreButton", plp), 
						"\"View More\" button should be displayed below the product grid", 
						"\"View More\" button is displayed below the product grid", 
						"\"View More\" button is not displayed below the product grid", driver);
			}
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	} // M1_FBB_DROP2_C19593

}
