package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22485 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	//private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider", enabled = false)
	public void M1_FBB_DROP4_C22485(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = prdData.get("prd_variation");	    
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			homePage.headers.navigateToMyAccount("jeff@gmail.com", "aspire123", true);
			Log.message(i++ + ". Logged in As authenticated user.", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + prd_any, driver);
	
			String size=pdpPage.selectSize();
			Log.message(i++ +"--->>>Selected size :: " + size);
	
			String color = pdpPage.selectColor();
			Log.message(i++ +"--->>>Selected Color :: " + color);
	
			pdpPage.clickAddProductToBag();		
			Log.message(i++ + ". Clicked on Add to bag Button.", driver);
	
			ShoppingBagPage shoppingbagPage=pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Clicked on Checkout button in ATB Overlay.", driver);
			//Step-1: Verify the components available in the PLCC Content				
			if(Utils.isDesktop())
			{Log.softAssertThat(shoppingbagPage.elementLayer.verifyPageElements(Arrays.asList("divPLCCcontentslotExpanded"), shoppingbagPage),					 
					"The content slot must be expanded by default", 
					"The content slot is expanded by default", 
					"The content slot is not expanded by default", driver);}
			else {
				Log.softAssertThat(shoppingbagPage.elementLayer.verifyPageElements(Arrays.asList("divPLCCcontentslotMobileTablet"), shoppingbagPage),					 
						"The content slot must be expanded by default", 
						"The content slot is expanded by default", 
						"The content slot is not expanded by default", driver);					
			}
			Log.softAssertThat(shoppingbagPage.elementLayer.verifyPageElements(Arrays.asList("expandclosePLCCcontentslot"), shoppingbagPage),					 
					"User First Name should be displayed", 
					"User First Name is displayed", 
					"User First Name is not displayed", driver);
	
			Log.softAssertThat(shoppingbagPage.elementLayer.verifyPageElements(Arrays.asList("expandclosePLCCcontentslot","learnmorelinkPlcc"), shoppingbagPage),					 
					"Expand/Collapse Button and Learn more Link should be displayed", 
					"Expand/Collapse Button and Learn more Link are displayed", 
					"Expand/Collapse Button or Learn more Link is not displayed", driver);
	
			Log.softAssertThat(shoppingbagPage.elementLayer.verifyPageElements(Arrays.asList("creditlimitPlcc"), shoppingbagPage),					 
					"PLCC Limit Offer should be displayed", 
					"PLCC Limit Offer is displayed", 
					"PLCC Limit Offer is not displayed", driver);
	
			//Step-2: Verify the display of Content Slot when it is collapsed
			shoppingbagPage.clickplccExpand();
			Log.softAssertThat(shoppingbagPage.elementLayer.verifyPageElements(Arrays.asList("expandclosePLCCcontentslot"), shoppingbagPage),					 
					"User First Name should be displayed", 
					"User First Name is displayed", 
					"User First Name is not displayed", driver);
	
			Log.softAssertThat(shoppingbagPage.elementLayer.verifyPageElements(Arrays.asList("expandclosePLCCcontentslot","learnmorelinkPlcc"), shoppingbagPage),					 
					"Expand/Collapse Button and Learn more Link should be displayed", 
					"Expand/Collapse Button and Learn more Link are displayed", 
					"Expand/Collapse Button or Learn more Link is not displayed", driver);
	
			//A Global Asset content slot should be displayed to the left of the PLCC content section that will carry
			//the logo of the brand that is offering the PLCC
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("logoPlccBrand"), shoppingbagPage),					 
					"The logo of the brand that is offering the PLCC should be displayed", 
					"The logo of the brand that is offering the PLCC is displayed", 
					"The logo of the brand that is offering the PLCC is not displayed", driver);
			Log.softAssertThat(!shoppingbagPage.elementLayer.verifyPageElements(Arrays.asList("creditlimitPlcc"), shoppingbagPage),					 
					"PLCC Limit Offer should be displayed", 
					"PLCC Limit Offer is displayed", 
					"PLCC Limit Offer is not displayed", driver);
	
			//Another configurable text will inform the User about the pre-approved offer for the PLCC
	
			shoppingbagPage.clickplccExpand();
			//Step-3: Verify the display of Content Slot when it is expanded
			Log.softAssertThat(shoppingbagPage.elementLayer.verifyPageElements(Arrays.asList("logoPlccBrand"), shoppingbagPage),					 
					"The logo of the brand that is offering the PLCC should be displayed", 
					"The logo of the brand that is offering the PLCC is displayed", 
					"The logo of the brand that is offering the PLCC is not displayed", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}
}// search
