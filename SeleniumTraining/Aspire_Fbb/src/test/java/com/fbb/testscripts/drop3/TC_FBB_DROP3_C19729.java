package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.WishlistLoginPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19729 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	@SuppressWarnings("unused")
	private static String runPlatfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19729(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("ps_product-set");
		String product5 = prdData.get("ps_product-set_products").split("\\|")[4];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			String prdName = pdpPage.getProductName();
			//1
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtshopthelook", "ProdSetIndvProdImages", pdpPage), 
					" Component Product Image should display below shop the look section.", 
					" Component Product Image is display below shop the look section.", 
					" Component Product Image is not display below shop the look section.", driver);
	
			//2. Click on the View full details link in the component product attribute
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "ProdSetIndvProdImages", "viewfullDetailProduct", pdpPage), 
					"View full details link should be displayed below the Component product image.", 
					"View full details link is displayed below the Component product image.", 
					"View full details link is not displayed below the Component product image.", driver);
	
			pdpPage.clickViewFullDetailsLinkInComponentProduct(0);
	
			Log.softAssertThat(pdpPage.verifyCurrentProduct(prdName), 
					"In the PDP page, Bread crumb should be displayed as category path of the product set.", 
					"In the PDP page, Bread crumb is displayed as category path of the product set", 
					"In the PDP page, Bread crumb is not displayed as category path of the product set.", driver);
	
			pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + searchKey, driver);
	
			//3 
			Log.message("Step -3 Item number in the product set components is not verified due to the PXSFCC-4182 since it is not applicable for the product set.");
	
			// 4 
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productSetDetails", "ProdSetIndvProdImages", pdpPage), 
					"Component product attributes should be displayed to the right hand side of component product Image.",
					"Component product attributes is displayed to the right hand side of component product Image.", 
					"Component product attributes is not displayed to the right hand side of component product Image.", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtProductPrice","productSetproductName","productSetProductvariation","lnkSizechart"), pdpPage), 
					"Product Name/Price/Promotional message/Variation/Size Chart attributes should be displayed.", 
					"Product Name/Price/Promotional message/Variation/Size Chart attributes is displayed ", 
					"Product Name/Price/Promotional message/Variation/Size Chart attributes is not displayed ", driver);
			//5
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productSetproductName", "ProdSetIndvProdImages", pdpPage), 
					"Product name should be displayed to the right hand side of the component product image.",
					"Product name is displayed to the right hand side of the component product image.", 
					"Product name is not displayed to the right hand side of the component product image.", driver);
	
			//6
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productSetproductName", "txtProductPrice", pdpPage), 
					"Product pricing should be displayed below the component product name.", 
					"Product pricing is displayed below the component product name.", 
					"Product pricing is not displayed below the component product name", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productSalePrice", "txtProductPrice", pdpPage), 
					"When there is more than one price in the active sale price book, it should display a strike through on the list price and should display the sale price to the right.", 
					"When there is more than one price in the active sale price book, it is display a strike through on the list price and is display the sale price to the right.", 
					"When there is more than one price in the active sale price book, it is not display a strike through on the list price and not display the sale price to the right.", driver);
			//8
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductPrice", "productSetProductvariation", pdpPage), 
					"Variation attributes should be displayed below the pricing.", 
					"Variation attributes is displayed below the pricing.", 
					"Variation attributes is not displayed below the pricing.", driver);
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("productColorSwatches","productSizeSwatches"), pdpPage), 
					"Color Swatch and Size swatch variation attribute should displayed", 
					"Color Swatch and Size swatch variation attribute is displayed", 
					"Color Swatch and Size swatch variation attribute is not displayed", driver);
	
			//9
			Log.softAssertThat(pdpPage.verifyDefaultColorSelectedInproductSet()==pdpPage.getNoOfProductInProductSet(), 
					" The Color should be pre-selected by default and it should display the default color from the PDP.", 
					" The Color is pre-selected by default and it is display the default color from the PDP", 
					" The Color is not pre-selected by default and it is not display the default color from the PDP", driver);
	
			//10
			Log.softAssertThat(!pdpPage.elementLayer.verifyPageElements(Arrays.asList("sizeSwatchesSelected"),pdpPage), 
					"By default, no size should be pre-selected.", 
					"By default, no size is pre-selected.", 
					"By default, size is pre-selected.", driver);
	
			Log.softAssertThat(pdpPage.addToCartButtonDisabledState().equalsIgnoreCase("true"),
					"The select size error message should be displayed!",
					"The select size error message is displayed!",
					"The select size error message is not displayed!", driver);
	
			//pdpPage.selectSingleProductComponentColorBasedOnIndex(3, 4);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("swatchSizeOutOfStock"), pdpPage),
					"Out of stock size variations will display as greyed out.", 
					"Out of stock size variations is display as greyed out.", 
					"Out of stock size variations is not display as greyed out.", driver);
	
			pdpPage.selectAllProductSetSizeBasedOnIndex(0, true);
			String[] selectedSize = pdpPage.getAllSelectedSize();
			Log.softAssertThat(pdpPage.verifyAddtoBagErrorMessage(1,"ADD TO BAG"), 
					"After selecting the size the ADD TO BAG buttons will displayed.", 
					"After selecting the size the ADD TO BAG buttons is displayed.", 
					"After selecting the size the ADD TO BAG buttons is not displayed.", driver);
	
			Log.softAssertThat(pdpPage.verifySelectedSizevalueLabel(1,selectedSize[0]), 
					"Selected size name should display next by the size field.", 
					"Selected size name is display next by the size field.", 
					"Selected size name is not display next by the size field.", driver);
			//11
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("SizeFamilySelectable"),pdpPage), 
					"By default, Regular variation value should be pre selected or another selection is pre defined from user's previous interaction", 
					"By default, Regular variation value is pre selected or another selection is pre defined from user's previous interaction", 
					"By default, Regular variation value is not pre selected or another selection is pre defined from user's previous interaction", driver);
	
			Log.softAssertThat(pdpPage.verifySelectedSizeFamilyLabel(1,"Women's"), 
					"Selected variation attributes value should be displayed near Size Shop label", 
					"Selected variation attributes value is displayed near Size Shop label", 
					"Selected variation attributes value is not displayed near Size Shop label", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkSizechart", "productSizeSwatches", pdpPage), 
					"Size chart attribute should be displayed to the right hand side of the Size attribute",
					"Size chart attribute is displayed to the right hand side of the Size attribute", 
					"Size chart attribute is not displayed to the right hand side of the Size attribute", driver);
	
			//12
			pdpPage.clickSizeChartLinkProdset(product5);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("modalSizechart"),pdpPage), 
					"When clicked on Size chart, it should display a Size Guide Modal popup.", 
					"When clicked on Size chart, it is display a Size Guide Modal popup.", 
					"When clicked on Size chart, it is not display a Size Guide Modal popup.", driver);
	
			pdpPage.closeSizeChartModalPopup();
	
			//13
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtMonogrammingTitle"),pdpPage), 
					"Monogramming product option should display in the product.", 
					"Monogramming product option is displayed in the product.", 
					"Monogramming product option is not display in the product.", driver);
	
			Log.message("Hemming is not verified in the test case since it is moved to out of scope");
	
			Log.softAssertThat(!pdpPage.verifyMonogrammingCheckboxSelected(), 
					"By default, the product option checkbox should be unchecked.", 
					"By default, the product option checkbox is unchecked.", 
					"By default, the product option checkbox checked.", driver);
	
			pdpPage.clickOnMonogrammingCheckbox("enable");
	
			Log.softAssertThat(pdpPage.verifyMonogrammingOptionsDrawerDisplayed(), 
					"When clicked on the checkbox, the options drawer should open below with the drawer anchored to the bottom of the option list.", 
					"When clicked on the checkbox, the options drawer is open below with the drawer anchored to the bottom of the option list.", 
					"When clicked on the checkbox, the options drawer is not open below with the drawer anchored to the bottom of the option list.", driver);
	
			//14
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productSizeSwatches", "txtProdAvailStatus", pdpPage), 
					"Inventry message should display below the size variations.", 
					"Inventry message is display below the size variations.", 
					"Inventry message is not display below the size variations", driver);
	
			Log.softAssertThat(pdpPage.getProdAvailStatus().equalsIgnoreCase("In Stock"), 
					"Appropriate inventry message should displayed.", 
					"Appropriate inventry message is displayed.", 
					"Appropriate inventry message is not displayed.", driver);
	
			//15
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "wishlist", "btnAddToBagSpecialProductSet", pdpPage), 
					"Add to wishlist option should be displayed right side of the add to bag button.", 
					"Add to wishlist option is displayed right side of the add to bag button.", 
					"Add to wishlist option is not displayed right side of the add to bag button.", driver);
	
			WishlistLoginPage whisList = (WishlistLoginPage) pdpPage.addToWishlist();
	
			Log.softAssertThat(whisList.elementLayer.verifyPageElements(Arrays.asList("divSearchWishlist"), whisList), 
					"When the user not Logged in the application Page should be navigated to Login Page.", 
					"When the user not Logged in the application Page is navigated to Login Page", 
					"When the user not Logged in the application Page is not navigated to Login Page", driver);
	
			pdpPage = whisList.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + searchKey, driver);
			//16
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("SizeFamilySelectable"),pdpPage), 
					"By default, Regular variation value should be pre selected or another selection is pre defined from user's previous interaction", 
					"By default, Regular variation value is pre selected or another selection is pre defined from user's previous interaction", 
					"By default, Regular variation value is not pre selected or another selection is pre defined from user's previous interaction", driver);
	
			Log.softAssertThat(pdpPage.verifySelectedSizeFamilyLabel(1,"Women's"), 
					"Selected variation attributes value should be displayed near Size Shop label", 
					"Selected variation attributes value is displayed near Size Shop label", 
					"Selected variation attributes value is not displayed near Size Shop label", driver);
			//17
			pdpPage.selectAllProductSetSizeBasedOnIndex(0);
			Log.softAssertThat(pdpPage.getSelectedQuantityValue().equals("1"),
					"The default value of Quantity dropdown should be 1!",
					"The default value of Quantity dropdown is 1!",
					"The default value of Quantity dropdown is not 1!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnAddToBagSpecialProductSet", "txtSelectedQuantity", pdpPage), 
					"The Quantity dropdown should be displayed to the left of Add To Bag!",
					"The Quantity dropdown is displayed to the left of Add To Bag!",
					"The Quantity dropdown is not displayed to the left of Add To Bag!", driver);
	
	
			String value = pdpPage.selectQuantity(5);
			Log.message(i++ + ". Selected Quantity!", driver);
	
			Log.softAssertThat(pdpPage.getSelectedQuantityValue().equals(value),
					"The selected value of Quantity dropdown should be displayed!",
					"The selected value of Quantity dropdown is displayed!",
					"The selected value of Quantity dropdown is not displayed!", driver);
	
			//18
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProdAvailStatus", "btnAddToBagSpecialProductSet", pdpPage), 
					"Inventry message should display below the size variations.", 
					"Inventry message is display below the size variations.", 
					"Inventry message is not display below the size variations", driver);
	
	
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}


}// search
