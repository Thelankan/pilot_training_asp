package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22511 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22511(String browser) throws Exception {
		Log.testCaseInfo();

		//Load Test Data
		String prd_any = prdData.get("prd_variation");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, AccountUtils.generateEmail(driver));
			CheckoutPage checkout = (CheckoutPage) obj[0];
			i = (Integer) obj[1];

			Log.reference("Edit link valications-->Covered functionality in the test case id: C22514 Step 1 to 7");

			checkout.fillingShippingDetailsAsGuest("YES", "valid_address7", "Ground");
			Log.message(i++ + ". Shipping Address details filled successfully.", driver);

			checkout.continueToPayment();
			Log.message(i++ + ". Continued to Billing/Payment Page.", driver);

			//Step-1:  Verify the functionality of Module Heading
			Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("lblPaymentDetailsHeading"), checkout),			 
					"The Billing Section Heading element should not be clickable", 
					"The Billing Section Heading element is not clickable", 
					"The Billing Section Heading element is clickable", driver);

			//Step-2: Verify the functionality of Module Heading
			Log.softAssertThat(checkout.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblPaymentDetailsHeading", "lblBillingAddressHeading", checkout), 
					"Billing Address heading should be displayed below the Module Heading.", 
					"Billing Address heading is displayed below the Module Heading.", 
					"Billing Address heading is not displayed below the Module Heading.", driver);

			//3
			Log.softAssertThat(checkout.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btneditBillingAddress", "lblBillingAddressHeading", checkout), 
					"The Edit link should be displayed to the right of Billing Address Heading.", 
					"The Edit link is displayed to the right of Billing Address Heading.", 
					"The Edit link is not displayed to the right of Billing Address Heading", driver);

			//Verify the functionality of Billing Address Retained state
			Log.softAssertThat(checkout.compareShippingBillingAddress(), 
					"The Billing Address Retained State should be displayed address and nickname is not present in address for guest user.", 
					"The Billing Address Retained State is displayed address and nickname is not present in address for quest user.", 
					"The Billing Address Retained State is not displayed address and nickname is not present in address for quest user.", driver);

			//Verify the functionality of the edit button
			checkout.clickEditBillingAddress();
			Log.message(i++ + ". Clicked on Edit link.", driver);

			Log.softAssertThat(checkout.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtBillingNickNameFld"), checkout), 
					"For a guest User, the Address nickname will not be present in this section",
					"For a guest User, the Address nickname will not be present in this section",
					"For a guest User, the Address nickname will not be present in this section", driver);

			Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("billingEditFirstname"), checkout), 
					"On clicking the Edit link, the Retained Billing address section view should be changed to Add New Billing Address State with the previously selected address information pre-filled.", 
					"On clicking the Edit link, the Retained Billing address section view is changed to Add New Billing Address State with the previously selected address information pre-filled.", 
					"On clicking the Edit link, the Retained Billing address section view is not changed to Add New Billing Address State with the previously selected address information pre-filled.", driver);

			checkout.clickselectPaymentMethod();
			Log.message(i++ + ". Clicked on Select Payment Method button.", driver);

			Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("billingCardSection"), checkout),
					"Usershould to enter their CC details or select PayPal as the form of payment.", 
					"User is able to enter their CC details or select PayPal as the form of payment.", 
					"User is not able to enter their CC details or select PayPal as the form of payment", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally

	}

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP4_C22511(String browser) throws Exception {
		Log.testCaseInfo();

		//Load Test Data
		String prd_any = prdData.get("prd_variation");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String credentials = AccountUtils.generateEmail(driver) + "|" + accData.get("password_global");
		
		{
			GlobalNavigation.registerNewUser(driver,0,0, credentials);
		}

		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, credentials);
			CheckoutPage checkout = (CheckoutPage) obj[0];
			i = (Integer) obj[1];

			checkout.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", "Ground", "valid_address7");
			Log.message(i++ + ". Shipping Address details filled successfully.", driver);

			checkout.continueToPayment();
			Log.message(i++ + ". Continued to Billing/Payment Page.", driver);

			//Verify the functionality of the edit button
			checkout.clickEditBillingAddress();
			Log.message(i++ + ". Clicked on Edit link.", driver);

			Log.softAssertThat(checkout.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtBillingNickNameFld"), checkout), 
					"For logged in user, with no saved address nickname will not be present in this section.",
					"For logged in user, with no saved address nickname will not be present in this section.",
					"For logged in user, with no saved address nickname will not be present in this section.", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	}
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP4_C22511(String browser) throws Exception {
		Log.testCaseInfo();

		//Load Test Data
		String prd_any = prdData.get("prd_variation");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String credentials = AccountUtils.generateEmail(driver).replace("@", "1@") + "|" + accData.get("password_global");

		{
			GlobalNavigation.registerNewUser(driver, 1, 0, credentials);
		}

		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, credentials);
			CheckoutPage checkout = (CheckoutPage) obj[0];
			i = (Integer) obj[1];

			checkout.continueToPayment();
			Log.message(i++ + ". Continued to Billing/Payment Page.", driver);

			Log.softAssertThat(checkout.compareShippingBillingAddress(), 
					"For logged in user with saved address, it will display in the defined format with Nick name",
					"For logged in user with saved address, it will display in the defined format with Nick name",
					"For logged in user with saved address, it will display in the defined format with Nick name", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally

	}
}// search
