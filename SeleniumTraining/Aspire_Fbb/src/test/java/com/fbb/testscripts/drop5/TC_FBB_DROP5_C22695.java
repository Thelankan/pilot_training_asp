package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.EmailPreferencePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22695 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "medium", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22695(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String username;
		String usernameNew;
		String password = accountData.get("password_global");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
		usernameNew = username.replace("@", "new@");
	
		//Prerequisite: A register user account not subscribed for emails
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password);
	
			EmailPreferencePage emailPreference = myAcc.clickOnEmailPrefLink();
			Log.message(i++ + ". Navigated to Email Preference Page!", driver);
	
			//1 - Verify the functionality of Email Preference
			if(!Utils.isMobile()) {
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "myAccountHeader", "lblEmailPref", emailPreference)
						&& emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblEmailPref", "lblCurrentEmail", emailPreference)
						&& !emailPreference.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lblEmailPref", "lblCurrentEmail", emailPreference),
						"Email Preference Heading should be displayed on the top left side",
						"Email Preference Heading is displayed on the top left side",
						"Email Preference Heading is not displayed on the top left side", driver);
			}else {
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtBreadCrumbElementMobile", "lblEmailPref", emailPreference),
						"Email Preference Heading should be displayed below My Account.",
						"Email Preference Heading is displayed below My Account.",
						"Email Preference Heading is not displayed below My Account.", driver);
			}
	
			Log.softAssertThat(emailPreference.elementLayer.verifyElementTextWithDWREProp(Arrays.asList("lblEmailPref"), "account.email.header", emailPreference),
					"The Email Preference title should be displayed correctly",
					"The Email Preference title is displayed correctly",
					"The Email Preference title is not displayed correctly", driver);
	
			//2 - Verify the functionality of Current Address
			Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblEmailPref", "lblEmailSubPref", emailPreference),
					"The current email should be displayed below the Email Preferences Heading.",
					"The current email is displayed below the Email Preferences Heading.",
					"The current email is not displayed below the Email Preferences Heading.", driver);
	
			Log.softAssertThat(emailPreference.getCurrentEmail().equals(username),
					"The current email : "+ username +" should be displayed",
					"The current email : "+ username +"  is displayed",
					"The current email : "+ username +"  is not displayed", driver);
	
			if(!Utils.isMobile()) {
				Log.softAssertThat(emailPreference.elementLayer.VerifyPageElementDisplayed(Arrays.asList("divSecondaryOption"), emailPreference),
						"The My Account navigation options should get displayed",
						"The My Account navigation  options are displayed",
						"The My Account navigation  options are not displayed", driver);
			}
	
			Log.softAssertThat(emailPreference.verifyCurrentEmailMessage(),
					"The current email message content should be displayed correctly",
					"The current email message is displayed",
					"The current email message is not displayed", driver);
	
			//3 - Verify the functionality of Edit
			ProfilePage profile = emailPreference.clickEdit();
			Log.message(i++ + ". Clicked Edit & Navigated to Profile Page!", driver);
	
			profile.typeOnEmailAddress(usernameNew);
			profile.typeOnConfirmEmailAddress(usernameNew);
			profile.typeOnPassword(password);
			Log.message(i++ + ". Updated to new email address!", driver);
	
			myAcc = profile.clickUpdateInfoBtn();
			Log.message(i++ + ". Clicked Update Info button!", driver);
	
			emailPreference = myAcc.clickOnEmailPrefLink();
			Log.message(i++ + ". Navigated to Email Preference Page!", driver);
	
			Log.softAssertThat(emailPreference.getCurrentEmail().equals(usernameNew),
					"The updated email : "+ usernameNew +" should be displayed",
					"The updated email : "+ usernameNew +"  is displayed",
					"The updated email : "+ usernameNew +"  is not displayed. " + emailPreference.getCurrentEmail() + " is displayed.", driver);
	
			//Revert to old username
			profile = emailPreference.clickEdit();
			Log.message(i++ + ". Clicked Edit & Navigated to Profile Page!", driver);
	
			profile.typeOnEmailAddress(username);
			profile.typeOnConfirmEmailAddress(username);
			profile.typeOnPassword(password);
			Log.message(i++ + ". Reverted to old email address!", driver);
	
			myAcc = profile.clickUpdateInfoBtn();
			Log.message(i++ + ". Clicked Update Info button!", driver);
	
			emailPreference = myAcc.clickOnEmailPrefLink();
			Log.message(i++ + ". Navigated to Email Preference Page!", driver);
	
			Log.softAssertThat(emailPreference.getCurrentEmail().equals(username),
					"The current email : "+ username +" should be displayed",
					"The current email : "+ username +"  is displayed",
					"The current email : "+ username +"  is not displayed. " + emailPreference.getCurrentEmail() + " is displayed.", driver);
	
			Log.softAssertThat(emailPreference.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkEditCurrentEmail", "lblCurrentEmail", emailPreference),
					"Check the current email is displaying left to the edit link",
					"The current email is displaying left to the edit link",
					"The current email is not displaying left to the edit link", driver);
	
			//4 - Verify the functionality of Brand Image/Logo
			if(Utils.isMobile())
			{
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblCurrentEmail", "imgBrandMobile", emailPreference),
						"The Brand image should be displayed below the User's email address",
						"The Brand image is displayed below the User's email address",
						"The Brand image is not displayed below the User's email address", driver);
	
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblCurrentEmail", "imgBrandNameMobile", emailPreference),
						"The Brand Name should be displayed below the User's email address",
						"The Brand Name is displayed below the User's email address",
						"The Brand Name is not displayed below the User's email address", driver);
			}
			//For Desktop and Tablet
			else
			{
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblCurrentEmail", "imgBrandDesktop", emailPreference),
						"The Brand image should be displayed below the User's email address",
						"The Brand image is displayed below the User's email address",
						"The Brand image is not displayed below the User's email address", driver);
	
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblCurrentEmail", "imgBrandNameDesktop", emailPreference),
						"The Brand Name should be displayed below the User's email address",
						"The Brand Name is displayed below the User's email address",
						"The Brand Name is not displayed below the User's email address", driver);
	
			}
	
			//5 - Verify the functionality of Email Benefits Copy
			if(Utils.isMobile())
			{
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgBrandNameMobile", "lblEmailBenefits", emailPreference),
						"The Brand Name should be displayed below the User's email address",
						"The Brand Name is displayed below the User's email address",
						"The Brand Name is not displayed below the User's email address", driver);
			}
			//For Desktop and Tablet
			else
			{
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgBrandNameDesktop", "lblEmailBenefits", emailPreference),
						"The Brand image should be displayed below the User's email address",
						"The Brand image is displayed below the User's email address",
						"The Brand image is not displayed", driver);
			}
	
			//6 - Verify the functionality of Preference Options
			Log.softAssertThat(emailPreference.verifyAllPreferenceOptionsUnselectedByDefault(),
					"All Preference Options should be unselected by default for new subscriber",
					"All Preference Options are unselected by default for new subscriber",
					"All Preference Options are not unselected by default for new subscriber", driver);
	
			//7 - Verify the functionality of Subscribe button
			Log.softAssertThat(emailPreference.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnSubscribe"), emailPreference),
					"The Subscribe button should be displayed",
					"The Subscribe button is displayed",
					"The Subscribe button is not displayed", driver);
	
			//6 contd.
			if(emailPreference.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkEditPreferences"), emailPreference)) {
				emailPreference.clickEditPreferences();
				
				Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblEmailBenefits", "divPreferenceOptions", emailPreference), 
						"Preference options should be displayed below the Email Benefits Copy message", 
						"Preference options is displayed below the Email Benefits Copy message", 
						"Preference options is not displayed below the Email Benefits Copy message", driver);
		
				emailPreference.checkUncheckPreferenceOption(0, true);
				Log.message(i++ + ". Enabled a preference option!", driver);
		
				Log.softAssertThat(emailPreference.verifyPreferenceOptionSelected(0),
						"The Preference Option should be selected",
						"The Preference Option section is selected",
						"The Preference Option section is not selected", driver);
		
				emailPreference.checkUncheckPreferenceOption(1, true);
				Log.message(i++ + ". Enabled a second preference option!", driver);
		
				Log.softAssertThat(emailPreference.verifyPreferenceOptionSelected(0)
						&&emailPreference.verifyPreferenceOptionSelected(1),
						"The user should be able to select multiple Preference Options",
						"The multiple Preference Options are selected",
						"The multiple Preference Options are not selected", driver);
		
				emailPreference.checkUncheckPreferenceOption(0, false);
				Log.message(i++ + ". Deselected a preference option!", driver);
		
				Log.softAssertThat(emailPreference.verifyPreferenceOptionSelected(0) == false,
						"The Preference Option should be deselected",
						"The Preference Option section is deselected",
						"The Preference Option section is not deselected", driver);
			}
	
			//9 -Verify the functionality of My Account Navigation
			Log.softAssertThat(emailPreference.verifyEmailPreferencesLinkNotClickable(),
					"The Email Preference link should not be clickable",
					"The Email Preference link is not clickable",
					"The Email Preference link is clickable", driver);
			
			String valueToCheckCSS = prdData.get("highlight_color_2");
			Log.softAssertThat(emailPreference.elementLayer.verifyCssPropertyForElement("lnkEmailPref", "color", valueToCheckCSS, emailPreference),
					"The Email Preference link should be highlighted",
					"The Email Preference link is highlighted",
					"The Email Preference link is not highlighted", driver);
	
	
			//8 - Verify the functionality of Breadcrumb
			if(!Utils.isMobile())
			{
				if(emailPreference.elementLayer.verifyPageElements(Arrays.asList("promoBanner"), emailPreference)) {
					Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "promoBanner", "txtBreadCrumbCurrentElementDesktop", emailPreference),
							"Breadcrumb should be displayed below the promotional content.",
							"Breadcrumb is displayed below the promotional content.",
							"Breadcrumb is not displayed below the promotional content.", driver);
				}
				else {
					Log.softAssertThat(emailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "globalNavigation", "txtBreadCrumbCurrentElementDesktop", emailPreference),
							"Breadcrumb should be displayed below the Global Navigation menu.",
							"Breadcrumb is displayed below the Global Navigation menu.",
							"Breadcrumb is not displayed below the Global Navigation menu.", driver);
				}
	
				Log.softAssertThat(emailPreference.verifyBreadCrumb(),
						"Breadcrumb should be displayed as Home / My Account / Email Preferences",
						"Breadcrumb is displayed as Home / My Account / Email Preferences",
						"Breadcrumb is not displayed as Home / My Account / Email Preferences", driver);
	
				myAcc = (MyAccountPage) emailPreference.clickBreadCrumb("my account");
				Log.message(i++ + ". Clicked on My Account in bread crumb!", driver);
	
				emailPreference = myAcc.clickOnEmailPrefLink();
				Log.message(i++ + ". Navigated to Payments page!", driver);
	
				homePage = (HomePage) emailPreference.clickBreadCrumb("home");
				Log.message(i++ + ". Clicked on Home in bread crumb!", driver);
	
			}
			else //Mobile
			{
				Log.softAssertThat(emailPreference.getMobileBreadCrumb().equals("MY ACCOUNT"),
						"The breadcrumb current element should be displayed",
						"The breadcrumb current element is displayed",
						"The breadcrumb current element is not displayed", driver);
	
				myAcc = emailPreference.clickBackArrowInBreadcrumb();
				Log.message(i++ + ". Clicked on My Account in bread crumb!", driver);
			}
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.resetSubscriptionStatus(driver);
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP5_C22695

}// search
