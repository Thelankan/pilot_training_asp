package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19740 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	@SuppressWarnings("unused")
	private static String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "medium", "desktop", "tablet", "mobile"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19740(String browser) throws Exception {
		Log.testCaseInfo();

		//Load web driver
		final WebDriver driver = WebDriverFactory.get(browser);

		//Load Test Data
		String searchKey = prdData.get("qc_out_of_stock").split("\\|")[0];
		String color = prdData.get("qc_out_of_stock").split("\\|")[1];
		String searchKey1 = prdData.get("qc_alternateImg>3");
		String searchKey2 = prdData.get("qc_SizeChart");
		String searchKey3 = prdData.get("qc_Monogram");
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");

		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			MyAccountPage myAccount=homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);

			QuickOrderPage quickOrderPage = myAccount.headers.navigateToQuickOrder();
			Log.message(i++ + ". Navigated to Quick order Page!", driver);
			
			quickOrderPage.removeAllItemsFromQuickOrder();

			quickOrderPage.searchItemInQuickOrder(searchKey);
			Log.message(i++ + ". Search with '"+searchKey+"' in quick order page", driver);

			if(Utils.isMobile()){
				quickOrderPage.openCloseShopNowMobile(true);
				Log.message(i++ + ". Opened 'Shop Now' on mobile", driver);
			}

			//Step-1
			if(Utils.isMobile()) {
				Log.softAssertThat(quickOrderPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("imgPrimaryImage_Mobile","quickOrderCatalogNumberMobile", "productInformationSection",  "btnClose_Mobile","btnAddToBag"), quickOrderPage), 
						"Product Image, Quick order item number, Product Information, Remove Button, Product Option, Availability Messaging, Quantity Selector, Add to Bag button should be displayed", 
						"Product Image, Quick order item number, Product Information, Remove Button, Product Option, Availability Messaging, Quantity Selector, Add to Bag button is displayed", 
						"Product Image, Quick order item number, Product Information, Remove Button, Product Option, Availability Messaging, Quantity Selector, Add to Bag button is not displayed", driver);
			} else {
				Log.softAssertThat(quickOrderPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("imgPrimaryImage","quickOrderCatalogNumberDesktopTab", "productInformationSection",  "btnClose","btnAddToBag"), quickOrderPage), 
						"Product Image, Quick order item number, Product Information, Remove Button, Product Option, Availability Messaging, Quantity Selector, Add to Bag button should be displayed", 
						"Product Image, Quick order item number, Product Information, Remove Button, Product Option, Availability Messaging, Quantity Selector, Add to Bag button is displayed", 
						"Product Image, Quick order item number, Product Information, Remove Button, Product Option, Availability Messaging, Quantity Selector, Add to Bag button is not displayed", driver);
			}

			//Step-2
			if(Utils.isMobile()) {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnAddToCart", "imgPrimaryImage_Mobile",  quickOrderPage),
						"Product image should be displayed to the left side of Add to bag button",
						"Product image displayed in the left side of Add to bag button!",
						"Product image not displayed in the left side of Add to bag button", driver);
			} else {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnAddToCart", "imgPrimaryImage",  quickOrderPage),
						"Product image should be displayed to the left side of Add to bag button",
						"Product image displayed in the left side of Add to bag button!",
						"Product image not displayed in the left side of Add to bag button", driver);
			}

			//Step-3
			if(!Utils.isMobile()) {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgPrimaryImage", "divAlternateImages", quickOrderPage), 
						"Alternate images should display below the main image.",  
						"Alternate images should display below the main image.", 
						"Alternate images should display below the main image.", driver);

				quickOrderPage.selectAlternateImageByIndex(1);
				Log.message(i++ + ". 2nd Alternate Image selected.", driver);
				
				if(Utils.getCurrentBrand().equals(Brand.rm))
					Log.softAssertThat(quickOrderPage.elementLayer.verifyCssPropertyForElement("selectedAltImage", "border-color", "rgb(51, 30, 83)", quickOrderPage), 
							"Selected image displays underlined",  
							"Selected image displays underlined", 
							"Selected image displays underlined", driver);
				else
					Log.softAssertThat(quickOrderPage.elementLayer.verifyCssPropertyForElement("lnkSelectedAltImage", "border-color", "rgb(236, 0, 140)", quickOrderPage), 
							"Selected image displays underlined",  
							"Selected image displays underlined", 
							"Selected image displays underlined", driver);

				Log.softAssertThat(quickOrderPage.getselectedAltImgCode().equals(quickOrderPage.getPrimaryImgCode()), 
						"On click, the selected image populates the main product image space",  
						"On click, the selected image populates the main product image space", 
						"On click, the selected image populates the main product image space", driver);

				if(Utils.getCurrentBrand().equals(Brand.rm))
					Log.softAssertThat(quickOrderPage.elementLayer.verifyCssPropertyForElement("selectedAltImage", "border-color", "rgb(51, 30, 83)", quickOrderPage), 
							"On-Hover, the underline displays and is retained on-click",  
							"On-Hover, the underline displays and is retained on-click", 
							"On-Hover, the underline displays and is retained on-click", driver);
				else
					Log.softAssertThat(quickOrderPage.elementLayer.verifyCssPropertyForElement("lnkSelectedAltImage", "border-color", "rgb(236, 0, 140)", quickOrderPage), 
							"On-Hover, the underline displays and is retained on-click",  
							"On-Hover, the underline displays and is retained on-click", 
							"On-Hover, the underline displays and is retained on-click", driver);

				
			}

			//Step-4: Verify Zoom image
			if(Utils.isDesktop()) {
				quickOrderPage.zoomProductImage();
				Log.message(i++ + ". Zoom the product image!", driver);

				Log.softAssertThat(quickOrderPage.verifyZoomLensAndZoomWindowAreDisplayed(), 
						"On hover on main image Zoom image should display.",  
						"On hover on main image Zoom image should display.", 
						"On hover on main image Zoom image should display.", driver);

				Log.softAssertThat(quickOrderPage.elementLayer.verifyHorizontalAllignmentOfElementsWithoutScroll(driver, "divZoomWindowContainer", "imgPrimaryImage", quickOrderPage),
						"The zoomed image should be displayed to the right of primary product image!",
						"The zoomed image is displayed to the right of primary product image!",
						"The zoomed image is not displayed to the right of primary product image!", driver);

				Log.softAssertThat(quickOrderPage.verifyTopOfZoomedImageAllignsWithTopOfProductImage(),
						"The top of zoomed image should be alligned with the top of primary product image!",
						"The top of zoomed image is alligned with the top of primary product image!",
						"The top of zoomed image is not alligned with the top of primary product image!", driver);

			}
			//Step-5: Verify the display of Quick order Item number 
			if(Utils.isMobile()) {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "quickOrderCatalogNumberMobile", "imgPrimaryImage_Mobile", quickOrderPage), 
						"Quick order catalog number should be displayed below the product main image ", 
						"Quick order catalog number is displayed below the product main image", 
						"Quick order catalog number is not displayed below the product main image", driver);
			} else {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgPrimaryImage", "quickOrderCatalogNumberDesktopTab", quickOrderPage), 
						"Quick order catalog number should be displayed below the product main image ", 
						"Quick order catalog number is displayed below the product main image", 
						"Quick order catalog number is not displayed below the product main image", driver);
			}

			quickOrderPage.selectColor();
			Log.message(i++ + ". Color should be selected");

			quickOrderPage.selectSize();
			Log.message(i++ + ". Size should be selected");

			//Step-6
			if(Utils.isMobile()) {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgPrimaryImage_Mobile", "productInformationSection", quickOrderPage), 
						"Product information section should be displayed  below Primary image section",
						"Product information section is displayed below Primary image section!",
						"Product information section is not displayed below Primary image section", driver);
			} else {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyHorizontalAllignmentOfElements(driver,  "productInformationSection", "imgPrimaryImage", quickOrderPage),
						"Product information section should be displayed to the right side of Primary image section",
						"Product information section is displayed in the right side of Primary image section!",
						"Product information section is not displayed in the right side of Primary image section", driver);

				Log.softAssertThat(quickOrderPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblProductDetails"), quickOrderPage), 
						"By default, THE DETAILS tab displays when catalog search item is loaded", 
						"By default, THE DETAILS tab displays when catalog search item is loaded", 
						"By default, THE DETAILS tab displays when catalog search item is loaded", driver); 

				quickOrderPage.expandCollapseProductDetails("expand");
				Log.message(i++ + ". Clicked on Read More in product details.", driver);

				Log.softAssertThat(quickOrderPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("hiddenDataInProductDetail"), quickOrderPage), 
						"READ MORE: On-click details drawer expands to display additional information of the CQO item and link changes to CLOSE", 
						"READ MORE: On-click details drawer expands to display additional information of the CQO item and link changes to CLOSE", 
						"READ MORE: On-click details drawer expands to display additional information of the CQO item and link changes to CLOSE", driver); 

				quickOrderPage.expandCollapseProductDetails("collapse");
				Log.message(i++ + ". Clicked on Close in product details.", driver);

				Log.softAssertThat(quickOrderPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnReadMore"), quickOrderPage), 
						"CLOSE: On-click module collapses and goes back to Read More state", 
						"CLOSE: On-click module collapses and goes back to Read More state", 
						"CLOSE: On-click module collapses and goes back to Read More state", driver); 
			}
			
			if(Utils.isMobile()) {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver,  "productName_Mobile", "imgPrimaryImage_Mobile", quickOrderPage),
						"Product name should be displayed below of the main image",
						"Product name is be displayed below of the main image",
						"Product name is not displayed below of the main image", driver);
			} else {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyHorizontalAllignmentOfElements(driver,  "productName", "imgPrimaryImage", quickOrderPage),
						"Product name should be displayed right-hand side of the main image",
						"Product name is be displayed right-hand side of the main image",
						"Product name is not displayed right-hand side of the main image", driver);
			}

			if(Utils.isMobile()) {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productName_Mobile", "colorAttribute", quickOrderPage), 
						"Color attributes should be displayed below the product name and selling price/promotional messages.", 
						"Color attributes is displayed below the product name and selling price/promotional messages", 
						"Color attributes is not displayed below the product name and selling price/promotional messages", driver);
			} else {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productName", "colorAttribute", quickOrderPage), 
						"Color attributes should be displayed below the product name and selling price/promotional messages.", 
						"Color attributes is displayed below the product name and selling price/promotional messages", 
						"Color attributes is not displayed below the product name and selling price/promotional messages", driver);
			}


			quickOrderPage.selectColor(color);
			Log.message(i++ + ". Color should be selected");

			Log.softAssertThat(quickOrderPage.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("lstSizeSwatchesUnSelectable"), quickOrderPage), 
					" Out of stock size variations for the color should be greyed out", 
					" Out of stock size variations for the color is greyed out", 
					" Out of stock size variations for the color is not greyed out", driver);


			quickOrderPage.selectSize();

			if(Utils.isMobile()) {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgPrimaryImage_Mobile", "txtPrice", quickOrderPage), 
						"The price should displayed underneath the product name and to the right of the product image.", 
						"The price is displayed underneath the product name and to the right of the product image.",
						"The price is not displayed underneath the product name and to the right of the product image.",driver);
			} else {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtPrice", "imgPrimaryImage", quickOrderPage), 
						"The price should displayed right to the product name and to the right of the product image.", 
						"The price is displayed right to the product name and to the right of the product image.",
						"The price is not displayed right to the product name and to the right of the product image.",driver);
			}

			//Step-6 

			if(Utils.isMobile()) {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnClose_Mobile", "productInformationSection", quickOrderPage),
						"Close button should be displayed to the right side of Product information section",
						"Close button displayed in the right side of Product information section!",
						"Close button not displayed in the right side of Product information section", driver);
			} else {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnClose", "productInformationSection", quickOrderPage),
						"Close button should be displayed to the right side of Product information section",
						"Close button displayed in the right side of Product information section!",
						"Close button not displayed in the right side of Product information section", driver);
			}

			//Step-8
			Log.softAssertThat(quickOrderPage.elementLayer.verifyPageElements(Arrays.asList("availabilityMessaging"), quickOrderPage), 
					"Availability Messaging should be displayed",
					"Availability Messaging is displayed",
					"Availability Messaging is not displayed", driver);

			Log.softAssertThat(quickOrderPage.elementLayer.verifyElementDisplayedBelow("selectedSize", "txtPrdAvailabilityMessaging", quickOrderPage), 
					"Product availability messaging should be displayed below the product options", 
					"Product availability messaging is displayed below the product options!", 
					"Product availability messaging is not displayed below the product options", driver);
			//Step-9
			Log.softAssertThat(quickOrderPage.elementLayer.verifyElementDisplayedBelow("txtPrdAvailabilityMessaging", "drpQuantity",   quickOrderPage), 
					"Qty dropdown should be displayed below the Product availability messaging ", 
					"Qty dropdown is displayed below the Product availability messaging ", 
					"Qty dropdown is not displayed below the Product availability messaging ", driver);
			//Step-10
			Log.softAssertThat(quickOrderPage.elementLayer.verifyHorizontalAllignmentOfElements(driver,  "btnAddToCart", "qtyDropDown",  quickOrderPage) ||
					quickOrderPage.elementLayer.verifyHorizontalAllignmentOfElements(driver,  "btnAddToCart", "selectedQty1",  quickOrderPage),
					"Add to bag button should be displayed to the right side of Qty drodown",
					"Add to bag button displayed in the right side of Qty drodown!",
					"Add to bag button not displayed in the right side of Qty drodown", driver);

			//quickOrderPage.searchItemInQuickOrder(searchKey);
			//Log.message(i++ + ". Search with '"+searchKey+"' in quick order page", driver);

			if(Utils.isMobile()){
				quickOrderPage.openCloseShopNowMobile(true);
				Log.message(i++ + ". Opened 'Shop Now' on mobile", driver);
			}
			//Step-5 
			Log.softAssertThat(quickOrderPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("catalogPriceExpired"), quickOrderPage), 
					"Catalogue Price Expired should be displayed", 
					"Catalogue Price Expired is displayed", 
					"Catalogue Price Expired is not displayed", driver);

			if(Utils.isMobile()) {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgPrimaryImage_Mobile", "catalogPriceExpired", quickOrderPage), 
						"Catalog price expired should be displayed  below Primary image section",
						"Catalog price expired is displayed below Primary image section!",
						"Catalog price expired is not displayed below Primary image section", driver);
			} else {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "catalogPriceExpired", "productPrice",   quickOrderPage),
						"Catalog price expired should be displayed to the right side of Product price",
						"Catalog price expired is displayed in the right side of Product price!",
						"Catalog price expired is not displayed in the right side of Product price", driver);
			}
			
			quickOrderPage.removeAllItemsFromQuickOrder();
			Log.message(i++ + ". All the Items removed from the Quick order page!", driver);
			
			quickOrderPage.searchItemInQuickOrder(searchKey1);
			Log.message(i++ + ". Search with '"+searchKey1+"' in quick order page", driver);
			
			int NoOfThumbProdImg = quickOrderPage.getNoOfThumbnailProdImages();
			Log.message(i++ + ". No of Thumbnails in the product : "+NoOfThumbProdImg, driver);
			if (NoOfThumbProdImg > 3) {
				Log.softAssertThat(quickOrderPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnPrevImageDisable","btnNextImageEnable"), quickOrderPage),
						"To check the arrows displaying for product image thumbnails.",
						"The 'Prev arrow', 'Next arrow' are displaying when the product have 5 or more thumbnail images",
						"The arrow is not displaying even the thumbnails are having 5 or more product images", driver);

				Log.softAssertThat(quickOrderPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnPrevImageDisable"), quickOrderPage),
						"To check the previous arrow is disabled in the begining of image sequence.",
						"The 'Prev arrow' is displaying and disabled in the begining of image sequence.",
						"The 'Prev arrow' is not disabled", driver);

				Log.softAssertThat(quickOrderPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnNextImageDisable"), quickOrderPage),
						"To check the next arrow is enabled in the begining of image sequence.",
						"The 'Next arrow' is displaying and enabled in the begining of image sequence.",
						"The 'Next arrow' is not enabled", driver);

				if(Utils.isTablet()) {
					quickOrderPage.selectAlternateImageByIndex(3);

					Log.softAssertThat(quickOrderPage.verifyAlternateImageDisplayStatus(3, "false"),
							"To check the half hidden alternate image is displayed fully when the image is clicked.",
							"The alternate image is displaying fully.",
							"The alternate image is not displaying fully", driver);
				}

				quickOrderPage.scrollAlternateImageInSpecifiedDirection("Next");

				Log.softAssertThat(quickOrderPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnNextImageDisable"), quickOrderPage),
						"To check the next arrow is disabled when the alternate image sequence reached the end.",
						"The 'Next arrow' is displaying and disabled in the end of image sequence.",
						"The 'Next arrow' is not disabled", driver);

				Log.softAssertThat(quickOrderPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnPrevImageDisable"), quickOrderPage),
						"To check the Prev arrow is enabled at the end of image sequence.",
						"The 'Prev arrow' is displaying and enabled at the end of image sequence.",
						"The 'Prev arrow' is not enabled", driver);

			} else {
				Log.softAssertThat(!quickOrderPage.elementLayer.verifyPageElements(Arrays.asList("btnPrevImageDisable","btnNextImageEnable"), quickOrderPage),
						"To check the arrows displaying for product image thumbnails.",
						"The product only have 4 or less thumbnail images, thus the arrows are not displayed",
						"The arrow is displaying even the product only have 4 or less thumbnail images", driver);
			}
			
			quickOrderPage.removeAllItemsFromQuickOrder();
			Log.message(i++ + ". All the Items removed from the Quick order page!", driver);
			
			quickOrderPage.searchItemInQuickOrder(searchKey2);
			Log.message(i++ + ". Search with '"+searchKey2+"' in quick order page", driver);
			
			if(quickOrderPage.elementLayer.verifyPageElements(Arrays.asList("sizeChart"), quickOrderPage)) {
			Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "colorAttribute", "sizeChart", quickOrderPage),
					"Size cart should be displayed below the color attribute", 
					"Size cart is displayed below the color attribute", 
					"Size cart is not displayed below the color attribute", driver);
				
			quickOrderPage.clickOnSizeChart();

			Log.softAssertThat(quickOrderPage.elementLayer.verifyPageElements(Arrays.asList("sizeChartDescription"), quickOrderPage), 
					"When user clicks on the size cart should navigate to appropriate content library folder.", 
					"When user clicks on the size cart is navigate to appropriate content library folder", 
					"When user clicks on the size cart is not navigate to appropriate content library folder", driver);

			quickOrderPage.closeSizeChart();
			
			Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sizeAnchor", "sizeChartLabel", quickOrderPage),
					"Size attirbute should be displayed below the size chart", 
					"Size attirbute is displayed below the  size chart", 
					"Size attirbute is not displayed below the size chart", driver);
			
			} else {
				Log.failsoft("Size chart not displayed in the product",driver);
			}
			
			quickOrderPage.removeAllItemsFromQuickOrder();
			Log.message(i++ + ". All the Items removed from the Quick order page!", driver);
			
			quickOrderPage.searchItemInQuickOrder(searchKey3);
			Log.message(i++ + ". Search with '"+searchKey2+"' in quick order page", driver);
			
			Log.softAssertThat(quickOrderPage.elementLayer.verifyPageElements(Arrays.asList("chkEnableMonogramming"), quickOrderPage), 
					"Product Options should be displayed ", 
					"Product Options is displayed !", 
					"Product Options is not displayed !", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally

	}// TC_FBB_DROP3_C19740


}// search