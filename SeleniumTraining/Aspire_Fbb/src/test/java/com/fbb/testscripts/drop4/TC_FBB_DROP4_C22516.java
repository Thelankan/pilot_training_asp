package com.fbb.testscripts.drop4;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22516 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22516(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prodId = prdData.get("prd_price>100"); //Product should accept any two global coupons
		String[] couponCode = prdData.get("cpn_discount_ship").split("\\|");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prodId, i, AccountUtils.generateEmail(driver));
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (Integer) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", "valid_address7", "Ground");
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section.", driver);
			checkoutPage.fillingCardDetails1("card_Visa", false);
	
			//Step 1a
			Log.softAssertThat((!(checkoutPage.checkPromoCodeSectionExpadedOrNot())),
					"To verify the 'Have a Promo Code' section is collapsed.",
					"The 'Have a Promo Code' section is collapsed.",
					"The 'Have a Promo Code' section is not collapsed.", driver);
	
			Log.softAssertThat(!(checkoutPage.checkPromoCodeSectionExpadedOrNot()),
					"To verify the 'Have a Promo Code' arrow is going upward when not selected.",
					"The 'Have a Promo Code' arrow is displaying downward.",
					"The 'Have a Promo Code' arrow is not displaying downward.", driver);
	
			checkoutPage.expandOrColapsePromoCodeSection(true);
			
			checkoutPage.clickApplyPromoCouponCode();
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblCouponEmptyError"), checkoutPage),
					"When the 'Apply' button is clicked when the Promo code field empty, the System should display an error message",
					"The System is displaying an error message.",
					"The System is not displaying an error message.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "expandedPromoCodeSection", "txtCouponCode", checkoutPage),
					"The Expand/Collapse Button should be displayed to the top right of coupon code section",
					"The Expand/Collapse Button is displayed to the top right of coupon code section",
					"The Expand/Collapse Button is not displayed to the top right of coupon code section", driver);
			
			Log.softAssertThat((checkoutPage.checkPromoCodeSectionExpadedOrNot()),
					"To verify the 'Have a Promo Code' arrow is going upward when selected.",
					"The 'Have a Promo Code' arrow is going upward.",
					"The 'Have a Promo Code' arrow is not going upward.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtCouponCode","btnCouponApply"), checkoutPage),
					"To check fields of the 'Have a Promo code' section.",
					"The 'Coupon Code' text box and 'Apply Button' are present in the 'Have a PromoCode' section!",
					"Some fields are not present in the 'Have a PromoCode' section", driver);
	
			//2 
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "icoPromoCodeArrow", "couponToolTipsIcon", checkoutPage), 
					"The tool tip icon should be positioned to the left of the expand/collapse caret button.", 
					"The tool tip icon is positioned to the left of the expand/collapse caret button.", 
					"The tool tip icon is not positioned to the left of the expand/collapse caret button.", driver);
	
			checkoutPage.clickonCouponTooltip();
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("couponTooltipContent"), checkoutPage),
					"Clicking it will open a box with information about promo codes.",
					"Clicking it open a box with information about promo codes.",
					"Clicking it is not open a box with information about promo codes.", driver);
	
			checkoutPage.closeRewardSectionToolTip();
	
			//3
	
			checkoutPage.expandOrColapsePromoCodeSection(true);
			checkoutPage.enterPromoCouponCode("rw");
			checkoutPage.clickApplyPromoCouponCode();
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "promoHeader", "couponErrorMessage", checkoutPage),
					"The Coupon/Reward Error (if any) should be displayed below the 'Have a Promo code?' heading in a red font color.",
					"The Coupon/Reward Error (if any) is displayed below the 'Have a Promo code?' heading in a red font color.", 
					"The Coupon/Reward Error (if any) is not displayed below the 'Have a Promo code?' heading in a red font color.", driver);
	
			String colorCode = null;
			if(Utils.getCurrentBrand().equals(Brand.ww)) {
				colorCode = "9900";
			} else if(Utils.getCurrentBrand().equals(Brand.rm)){
				colorCode = "bd34";
			} else if(Utils.getCurrentBrand().equals(Brand.ks)){
				colorCode = "bd34";
			}else{
				colorCode = "bd34";
				Log.failsoft("Current brand not configured. Continueing with color code(bd34)");
			}
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("couponErrorMessage", colorCode, checkoutPage), 
					"Coupon error message should be displayed in red color.", 
					"Coupon error message is displayed in red color.", 
					"Coupon error message is not displayed in red color.", driver);
			
			checkoutPage.enterPromoCouponCode("invalid");
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyTextContains("couponErrorMessage", "invalid", checkoutPage), 
					"Coupon error message should display promo coupon code.", 
					"Coupon error message is display promo coupon code.", 
					"Coupon error message is not display promo coupon code.", driver);
			
			
	
			//4
			checkoutPage.applyPromoCouponCode(couponCode[1]);//Correct promo code
			checkoutPage.clickApplyPromoCouponCode();
			
			if(Utils.isMobile()) {
				checkoutPage.enterPromoCouponCode("invalid");
				
				checkoutPage.clickApplyPromoCouponCode();
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnSeeDetails", "couponErrorMessage", checkoutPage),
						"Check the Coupon/Reward Error message will display below the Coupon/Reward information.",
						"Coupon/Reward Error message will display below the Coupon/Reward information.", 
						"Coupon/Reward Error message will not display below the Coupon/Reward information.", driver);
			}
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnSeeDetails"), checkoutPage), 
					"SeeMore Link should be displayed when the applied the active coupons.", 
					"SeeMore Link is displayed when the applied the active coupons.", 
					"SeeMore Link is not displayed when the applied the active coupons.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnSeeDetails", "appliedCoupon", checkoutPage), 
					"The 'See details' link should be present at the end of a wrapped promotion description.", 
					"The 'See details' link is present at the end of a wrapped promotion description.", 
					"The 'See details' link is not present at the end of a wrapped promotion description.", driver);
	
			checkoutPage.clickOnSeeMoreLink();
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("discountDetails"), checkoutPage), 
					"After clicking SeeMore Link should be displayed coupons details.", 
					"After clicking SeeMore Link is displayed coupons details.", 
					"After clicking SeeMore Link is not displayed coupons details.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lnkRemoveLinkCoupon"), checkoutPage), 
					"The 'X' link should be displayed in coupon section.", 
					"The 'X' link is displayed in coupon section.", 
					"The 'X' link is not displayed in coupon section.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkRemoveLinkCoupon", "lblWrappedCouponDetails", checkoutPage), 
					"The 'X' link should be displayed to the right of the applied Promotion code.", 
					"The 'X' link is displayed to the right of the applied Promotion code.", 
					"The 'X' link is not displayed to the right of the applied Promotion code.", driver);
	
			//Step 5a
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("appliedCoupon"), checkoutPage),
					"To check entered promotion code is applied or not.",
					"The entered promo code is applied!",
					"The entered promo code is not applied", driver);
	
			//Step 3a
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnRemoveCoupon","btnSeeDetails"), checkoutPage),
					"To check 'Remove', 'See More' links are displaying/not.",
					"The 'Remove', 'See More' links are displaying!",
					"The 'Remove', 'See More' links are not displaying!", driver);
	
			//Step 3c
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("btnHideDetails"), checkoutPage),
					"To verify the 'Hide Details' link is displaying when the 'See More' is clicked.",
					"The 'Hide Details' link is displayed!",
					"The 'Hide Details' link is not displayed!", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtCouponCodeDetails"), checkoutPage),
					"To verify the description of the promotion is displaying when the 'See More' is clicked.",
					"The description of the promotion is displayed!",
					"The description of the promotion is not displayed!", driver);
			//Step 3b
			checkoutPage.removeAppliedCoupon();
			Log.message(i++ + ". Clicked on Remove Link", driver);
	
			checkoutPage.expandOrColapsePromoCodeSection(true);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("appliedCoupon"), checkoutPage),
					"To check the promo code is removed/not.",
					"The promo code is removed!",
					"The promo code is not removed", driver);
			//Step 4b
	
			checkoutPage.applyMultiplePromoCouponCode(couponCode);//Correct promo code
	
			//Step 4c
			Log.softAssertThat(checkoutPage.verifyAppliedCouponCodesAreInVerticalArrangement(),
					"To verify the coupons are vertically arranged.",
					"The coupons are vertically arranged!",
					"The coupons are not vertically arranged!", driver);
	
			//Step 5b
			Log.softAssertThat(checkoutPage.checkPromoCouponCodeTextFieldIsEmpty(),
					"To verify the text is cleared in the from the promo coupon text box.",
					"The text is cleared when the code is applied from the promo coupon text box!",
					"The text is not cleared from the promo coupon text box when the code is applied!", driver);
	
			checkoutPage.expandOrColapsePromoCodeSection(false);
			//Step 1b
			Log.softAssertThat((!(checkoutPage.checkPromoCodeSectionExpadedOrNot())),
					"To verify the 'Have a Promo Code' section is collapsed.",
					"The 'Have a Promo Code' section is collapsed.",
					"The 'Have a Promo Code' section is not collapsed", driver);
			
			checkoutPage.expandOrColapsePromoCodeSection(true);
	
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "promoHeader", "txtCouponCode", checkoutPage), 
						"Promo code entry field should located on the above of the Have a promo code? section.", 
						"Promo code entry field is located on the above of the Have a promo code? section.", 
						"Promo code entry field is not located on the above of the Have a promo code? section.", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtCouponCode", "promoHeader", checkoutPage),
						"Promo code entry field should located on the right side of the Have a promo code? section.", 
						"Promo code entry field is located on the right side of the Have a promo code? section.",
						"Promo code entry field is not located on the right side of the Have a promo code? section.", driver);
			}
			
			//7
			checkoutPage.fillingCardDetails1("card_Visa", false);
	
			checkoutPage.continueToReivewOrder();
			Log.message(i++ + ". Continued to Order Review Section.", driver);
	
			//8
			OrderConfirmationPage orderConfPg = checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". clicked on Place order button.", driver);
	
			Log.softAssertThat(orderConfPg.elementLayer.VerifyPageElementDisplayed(Arrays.asList("readyElement"), orderConfPg),
					"To check system navigated to 'Order Conformation' page.",
					"The 'Order Conformation' is displaying!",
					"The 'Order Conformation' is not displaying", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_FBB_DROP4_C22516
}// search
