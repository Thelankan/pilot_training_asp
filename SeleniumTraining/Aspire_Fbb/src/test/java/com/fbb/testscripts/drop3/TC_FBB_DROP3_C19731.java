package com.fbb.testscripts.drop3;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19731 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	@SuppressWarnings("unused")
	private static String runPlatfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19731(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prodId = prdData.get("Stock_Product");
		String[] prodDetail = {prdData.get("Stock_Variance1"), prdData.get("Stock_Variance2"), prdData.get("Stock_Variance3"),
				prdData.get("Stock_Variance4"), prdData.get("Stock_Variance5"), prdData.get("Stock_Variance6"),
				prdData.get("Stock_Variance7")};
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			int intCount;
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			String prodStatus;
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			for (intCount = 0; intCount < prodDetail.length; intCount++) {
				String[] prodInput = prodDetail[intCount].split("\\_");
	
				if(prodInput[2].toLowerCase().contains("Out of Stock".toLowerCase())) {
	
					Utils.waitForPageLoad(driver);
					if(!pdpPage.getSelectedColorVariant().contains(prodInput[1])) {
						pdpPage.selectColor(prodInput[1]);
					}
	
					Log.softAssertThat(pdpPage.verifyOutOfStockSize(prodInput[0]),		 				 
							"Out of stock size variations will display as greyed out.", 
							"Out of stock size variations is displayed as unselectable.", 
							"Out of stock size variations is unavailable or displayed as selectable.", driver);
				} else {
					Utils.waitForPageLoad(driver);
					pdpPage.selectSize(prodInput[0]);
					Utils.waitForPageLoad(driver);
	
					if(!pdpPage.getSelectedColorVariant().contains(prodInput[1])) {
						pdpPage.selectColor(prodInput[1]);
					}
					Utils.waitForPageLoad(driver);
	
					prodStatus = pdpPage.getProdAvailStatus();
					Utils.waitForPageLoad(driver);
	
					Log.softAssertThat(pdpPage.verifyProdAvailabilityStatus(prodStatus, prodInput[2]),
							"To check the availability status message.",
							"The status of product is correctly displayed : " +prodStatus,
							"The status of product is not displayed correctly : " +prodStatus, driver);
				}
			}
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	}// TC_FBB_DROP2_C19731


}// search
