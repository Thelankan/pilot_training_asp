package com.fbb.testscripts.drop2;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19669 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19669(String browser) throws Exception {
		Log.testCaseInfo();
	
		String lvl1 = prdData.get("level-2").split("\\|")[0];
		String lvl2 = prdData.get("level-2").split("\\|")[1];
				
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!",driver);
			
			PlpPage plpPage = null;
			
			if(Utils.isDesktop()) {
				plpPage = homePage.headers.navigateTo(lvl1);
			} else {
				plpPage = homePage.headers.navigateTo(lvl1, lvl2);
			}
			Log.message(i++ + ". Navigated to SLP Page with Search Keyword",driver);
			
			Log.softAssertThat(plpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtProductName","txtProductImages"
					,"txtProdPrice","txtProdPromoMessage","txtClearencePromoMessage","txtProdFeatureMsg","imgProductBadge"
					,"txtSwatchList"), plpPage),
					"",
					"",
					"", driver);
			
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally
	
	}// TC_FBB_DROP2_C19591


}// search


