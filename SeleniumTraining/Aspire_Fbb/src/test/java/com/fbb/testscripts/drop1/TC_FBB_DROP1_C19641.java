package com.fbb.testscripts.drop1;

import java.util.Arrays;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.MiniCartPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.headers.HamburgerMenu;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP1_C19641 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP1_C19641(String browser) throws Exception {
		Log.testCaseInfo();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String productID = prdData.get("prd_variation");
		String categLvl1 = prdData.get("level-1");
		String navLevels = prdData.get("level-2");
		

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			Headers headers = homePage.headers;
			Log.softAssertThat(headers.elementLayer.verifyCssPropertyForElement("fixedHeader", "position", "static", headers), 
					"Before scrolled down, global header should be sticky header.", 
					"Before scrolled down, global header is sticky header..", 
					"Before scrolled down, global header is not sticky header.", driver);
			//Step-1: Check the Sticky Header
			Log.softAssertThat(headers.checkGlobalHeaderIsStickyWhenScrollDown(), 
					"When scrolled down, global header should be replaced with sticky header.", 
					"When scrolled down, global header is replaced with sticky header..", 
					"When scrolled down, global header is not replaced with sticky header.", driver);

			//Step-2: Under the sticky header, mouse hovers on root categories. Desktop only
			headers.mouseHoverOnCategoryByCategoryName(categLvl1);
			
			Log.message(i++ + ". Mouse hovered on 3rd Category.");
			Log.softAssertThat(headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("currentLevel2"), headers), 
					"The flyout should displays when mouse hover on the root category.", 
					"The flyouts displayed when mouse hover on the root categories.", 
					"The flyouts did not display when mouse hover on the root categories.", driver);
			
			PlpPage plp = headers.navigateTo(navLevels.split("\\|")[0].trim());
			Log.softAssertThat(plp.getCategoryName().trim().equalsIgnoreCase(navLevels.split("\\|")[0].trim()), 
					"If user clicks on any root category, system should navigate user to the category list page", 
					"System navigates user to the category list page", 
					"System does not navigate user to the category list page", driver);


			//Step-7: Tap on mini Cart"My bag" icon
			// The Mini Cart does not display if there are no products in the Cart
			MiniCartPage miniCart = headers.mouseOverMiniCart();

			Log.softAssertThat(!miniCart.verifyMiniCartFlyout(), 
					"Mini-cart flyout should not display when emply.", 
					"Mini-cart flyout did not display.", 
					"Mini-cart flyout opened.", driver);

			// If user adds item on the cart, on mouse hover mini cart flyout will display.
			PdpPage pdpPage = headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
			//pdpPage.selectColor();
			pdpPage.addToBag();
			Log.message(i++ + ". Product Added to Cart.", driver);
			miniCart = headers.mouseOverMiniCart();

			Log.softAssertThat(miniCart.verifyMiniCartFlyout(), 
					"Mini-cart flyout should  display when not emply.", 
					"Mini-cart flyout displayed.", 
					"Mini-cart flyout did not open.", driver);

			//Clicking on the Checkout button should take us to the shopping cart page.
			ShoppingBagPage cartPage= miniCart.clickOnCheckOut();

			Log.softAssertThat(cartPage!= null, 
					"Clicking on the Checkout button should to the shopping cart page", 
					"Clicking on the Checkout button opened the shopping cart page", 
					"Clicking on the Checkout button did not open the shopping cart page", driver);

			//Clicking on the mini cart icon should take us to the shopping cart page.
			driver.navigate().back();
			cartPage= headers.navigateToShoppingBagPage();
			Log.softAssertThat(cartPage!= null, 
					"Clicking on the My Bag button should to the shopping cart page", 
					"Clicking on the My Bag button opened the shopping cart page", 
					"Clicking on the My Bag button did not open the shopping cart page", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// M1_FBB_DROP1_C19641


	@Test(groups = { "critical", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP1_C19641(String browser) throws Exception {
		Log.testCaseInfo();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String inValidUser = "auto@yop";
		String inValidPassword = "asdfasdf";

		String validUser = AccountUtils.generateEmail(driver);
		String validPassword = accountData.get("password_global");
		
		//Prerequisite: A registered user account if not exists
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, validUser +"|"+ validPassword);
		}
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			Headers headers = homePage.headers;
			HamburgerMenu hMenu = null;

			//Step-1: Check the Sticky Header
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,500)", "");
			if(Utils.isTablet()) {
				Log.softAssertThat(headers.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkHamburger", "brandLogo", "btnSearchIcon", "lnkSignInDesktop", "iconMyBag" ), headers), 
						"Hamburger menu, Brand logo, Search button, sign in options, Bag icon should be visible as sticky.", 
						"Hamburger menu, Brand logo, Search button, sign in options, Bag icon are visible as sticky.", 
						"Hamburger menu, Brand logo, Search button, sign in options, Bag icon are not visible as sticky.", driver);
			}
			if(Utils.isMobile()) {
				Log.softAssertThat(headers.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkHamburger", "brandLogo", "btnSearchIconMobile", "iconMyBag" ), headers), 
						"Hamburger menu, Brand logo, Search button, Bag icon should be visible as sticky", 
						"Hamburger menu, Brand logo, Search button, Bag icon are visible as sticky", 
						"Hamburger menu, Brand logo, Search button, Bag icon are not visible as sticky", driver);
			}

			//Step-3: Tap on Hamburger icon Tablet & Mobile Only
			hMenu = (HamburgerMenu) headers.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger Menu Opened!");

			Log.softAssertThat(hMenu.elementLayer.VerifyPageElementDisplayed(Arrays.asList("navigationCategory1"), hMenu),
					"Global Navigation menu should be displayed in Hamburger Menu.",
					"Global Navigation menu displayed in Hamburger Menu!",
					"Global Navigation menu not displayed in Hamburger Menu!", driver);

			//X is an Image. So cannot be automate like it shows X only. Instead close button presence verified
			Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("btnCloseHamburger"), hMenu),
					"Hamburger icon should become cross(X) symbol.",
					"Hamburger icon became cross(X) symbol.",
					"Hamburger icon not changed as cross(X) symbol.", driver);

			//Unauthenticated User - mobile
			if(runPltfrm.equals("mobile")){
				Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("lnkSignIn"), hMenu),
						"For Unauthenticated user Sign in section should be displayed.",
						"For Unauthenticated user Sign in section displayed.",
						"For Unauthenticated user Sign in section not displayed.", driver);
			}

			// Step-4: Tap on selected brand logo Tablet & Mobile Only
			headers = (Headers) hMenu.openCloseHamburgerMenu("close");
			Log.message(i++ + ". Hamburger Menu closed and Clicking on Brand Logo.");

			headers.navigateToHome();
			Log.message(i++ + ". Navigated to HomePage!");

			// Verify Alignment of H-Menu,Logo
			if(runPltfrm.equals("tablet")){
				if((Utils.getCurrentBrand().equals(Brand.jl)||Utils.getCurrentBrand().equals(Brand.rm))){
					Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "brandLogo", "lnkHamburger", headers),
							"Brands Logo should be displayed next to Hamburger Menu icon",
							"Brands Logo displayed next to Hamburger Menu icon",
							"Brands Logo not displayed next to Hamburger Menu icon", driver);
				}else{
					// Verify Alignment of H-Menu,Logo
					Log.softAssertThat(headers.elementLayer.verifyCenterAllignmentOfElement("brandLogo", headers),
							"Brand Logo should be displayed in the center of the Screen",
							"Brand Logo displayed as expected!",
							"Brand Logo not displayed as expected.", driver);
				}
			}

			// Verify Alignment of H-Menu,Logo,Search Icon,Bag in JL&WW&RM - Mobile
			if(runPltfrm.equals("mobile")){
				Log.softAssertThat(headers.elementLayer.verifyHorizondalAllignmentOfListOfElements(Arrays.asList(
						"lnkHamburger",
						"brandLogo",
						"iconSearch",
						"lnkViewBagMobile"), headers),
						"The Elements should be displayed in below order </br> Hamburger Icon >>> Brand Logo >>> Search Icon >>> Bag Icon",
						"Elements displayed as expected",
						"Elements not displayed as expected", driver);		
			}

			//Step-5: Tap on search icon Tablet & Mobile Only
			//1) Search box should be displayed below the sticky headers.
			if(Utils.isMobile()) {
				headers.clickOnSearchIconMobile();
				Log.softAssertThat(headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "brandLogo", "txtSearchMobile", headers),
						"Search Box should be displayed below the Headers.",
						"Search Box displayed below the Headers.",
						"Search Box not displayed below the Headers.", driver);
			}

			// User should be allow to add or edit the search term in search box
			String txtToType = headers.typeTextInSearchField("Dresses");
			String txtTyped = headers.getEnteredTextFromSearchTextBox();

			Log.softAssertThat(txtToType.equalsIgnoreCase(txtTyped),
					"User should be allows to add or edit the search term in search Box.",
					"User allowed to add or edit the search term in textbox!",
					"User not allowed to make changed in search textbox.", driver);	

			//Step-6: Tap on Sign in link Tablet Only
			if(Utils.isTablet()){
				BrowserActions.refreshPage(driver);
				//Sign In Flyout should be opened 
				headers.mouseOverSignIn();
				Log.message(i++ + ". Tapped on SignIn in Tablet", driver);

				Log.softAssertThat(headers.elementLayer.VerifyPageElementDisplayed(Arrays.asList("flytSignInUnregisteredDesktop"), headers),
						"SignIn Flyout should be opened!",
						"SignIn Flyout opened!",
						"Sign In Flyout not opened.", driver);	

				//Error message should be shown when sign in with invalid user name and password.
				headers.typeUserNameInFlyout(inValidUser);
				headers.typePasswordInFlyout(inValidPassword);
				Log.message(i++ + ". Entered Invalid User Mail and Password.", driver);
				Log.message(i++ + ". Clicking on SignIn Button.");
				SignIn signIn = (SignIn) headers.clickOnSignInDesktop();

				Log.softAssertThat(signIn.elementLayer.VerifyPageElementDisplayed(Arrays.asList("txtSignInError"), signIn),
						"Appropriate Error messages should be displayed.",
						"Error Messages displayed as Expected!",
						"Error Messages not displayed.", driver);	

				//User should be allowed Sign in to FBB App using valid User name and password
				headers.mouseOverSignIn();
				headers.typeUserNameInFlyout(validUser);
				headers.typePasswordInFlyout(validPassword);
				Log.message(i++ + ". Entered valid User Mail and Password.", driver);
				Log.message(i++ + ". Clicking on SignIn Button.", driver);
				Object obj = headers.clickOnSignInDesktop();

				Log.softAssertThat(obj.getClass().getName().contains("MyAccountPage"),
						"User should be able to login.",
						"User login successfully!",
						"User Login failed.", driver);	
			}
			
			BrowserActions.refreshPage(driver);
			SearchResultPage slp = headers.searchProductKeyword("Casual");
			Log.message("Searched for Casual", driver);
			PdpPage pdpPage = slp.navigateToPdp();
			pdpPage.addToBag();

			if(Utils.isTablet()) {
				headers.clickAndHoldOnBag();
				Log.message(i++ + ". First time clicked on the My Bag.", driver);
				
				Log.softAssertThat(headers.elementLayer.verifyCssPropertyForElement("flytMyBag", "display", "block", headers),
						"My Bag Flyout should be opened!",
						"My Bag Flyout opened!",
						"My Bag Flyout not opened.", driver);
			}

			ShoppingBagPage bagPage = headers.clickOnBagLink();
			Log.message(i++ + ". Second Time clicked on the My Bag.", driver);

			Log.softAssertThat(bagPage.getPageLoadStatus(),
					"Shopping Bag page should be opened!",
					"Shopping Bag page opened!",
					"Shopping Bag page did not opened.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveAllProducts(driver);
			driver.quit();
		} // finally

	}// M2_FBB_DROP1_C19641

}// search
