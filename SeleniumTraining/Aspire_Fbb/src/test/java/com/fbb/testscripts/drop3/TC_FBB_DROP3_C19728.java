package com.fbb.testscripts.drop3;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.WishlistLoginPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19728 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;

	String runPltfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19728(String browser) throws Exception {

		//HashMap<String, String> testData = new HashMap<String, String>();//TestDataExtractor.initTestData(workbookName, sheetName);

		//String brand = testData.get("Brand");
		String productID = prdData.get("prd_variation");

		String btnErrMessage = "Please Select Size";

		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo("Variation attributes visibility & position in PDP verifications.");
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.selectColor();
			
			Log.softAssertThat(pdpPage.addToCartButtonDisabled().equalsIgnoreCase("true"), 
					"Add To Bag button should be disabled.",
					"Add to Bag button disabled!",
					"Add to bag Button not disabled.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyTextContains("btnAddToBag", btnErrMessage, pdpPage),
					"Error Message should be displayed on the Button",
					"Error Message displayed on the Button!",
					"Error Message not displayed on the Button", driver);
			
			pdpPage.selectSize();
			
			//Step-1: In Add to Bag overlay Singular Standard/Variation Product should be shown. txtProductNameMobileTablet
			if(Utils.isMobile()){
				List<String> elementsToVerify = Arrays.asList("divReviewAndRatingMobile", "txtProductNameMobile","lblRegularPrice","divColorSwatches","tblSizeSwatch","btnAddToCart","wishlist");//,"lnkSizechart"
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(elementsToVerify, pdpPage), 
						"The elements(Reviews, Product Name, Selling Price, Color Attribs, Size, Inventory, Quantity, Add-to-Bag, Add-to-Wishlist, Social Icons) should be displayed.", // Size Chart,
						"Above mentioned elements displayed!",
						"Not all the above mentioned elements displayed.", driver);
			}else{
				List<String> elementsToVerify = Arrays.asList("txtProdAvailStatus", "divReviewAndRating","txtProductNameDesktop","lblRegularPrice","divColorSwatches","tblSizeSwatch","btnAddToCart","wishlist");//,"lnkSizechart"
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(elementsToVerify, pdpPage), 
						"The elements(Reviews, Product Name, Selling Price, Color Attribs, Size, Inventory, Quantity, "
						+ "Add-to-Bag, Add-to-Wishlist, Social Icons) should be displayed.",// Size Chart,
						"Above mentioned elements displayed!",
						"Not all the above mentioned elements displayed.", driver);
			}

			//Step-3: Verify the product name in product detail page
			//As per the change suggested in SM-1236 - REVIEWS should display BELOW the PRODUCT NAME 
			if(Utils.isMobile()){
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductNameMobile", "divReviewAndRatingMobile", pdpPage), 
						"Product name should be displayed above the reviews in the PDP.", 
						"Product name displayed above the reviews in the PDP.", 
						"Product name not displayed above the reviews in the PDP.", driver);
			}else{
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductNameDesktop", "divReviewAndRating", pdpPage), 
						"Product name should be displayed above the reviews in the PDP.", 
						"Product name displayed above the reviews in the PDP.", 
						"Product name not displayed above the reviews in the PDP.", driver);
			}

			//Step-4: Verify the selling price in product detail page
			if(Utils.isMobile()){
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductNameMobile", "lblOriginalPrice", pdpPage), 
						"Selling price should be displayed below the Product name in the PDP", 
						"Selling price displayed below the Product name in the PDP", 
						"Selling price not displayed below the Product name in the PDP", driver);
			}else{
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductNameDesktop", "lblOriginalPrice", pdpPage), 
						"Selling price should be displayed below the Product name in the PDP", 
						"Selling price displayed below the Product name in the PDP", 
						"Selling price not displayed below the Product name in the PDP", driver);
			}

			//Step-5: Verify the color attributes in Product detail page
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblOriginalPrice", "divColorGrid", pdpPage), 
					"Color attributes should be displayed below the product name and selling price/promotional messages", 
					"Color attributes displayed below the product name and selling price/promotional messages", 
					"Color attributes not displayed below the product name and selling price/promotional messages", driver);

			if(!Utils.isMobile()){
				Log.softAssertThat(pdpPage.verifyAllThumbnailsDisplayInPage(), 
						"The PDP should Load all variation images",
						"The PDP Loaded all variation images",
						"The PDP doesn't Load all variation images", driver);
			}

			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProdAvailStatus", "divQtySection", pdpPage), 
					"Quantity dropdown should be displayed below the inventory state", 
					"Quantity dropdown displayed below the inventory state", 
					"Quantity dropdown not displayed below the inventory state", driver);

			//​By default, the quantity should displayed as a 1. The drop down should be displayed upto 10.
			Log.softAssertThat(pdpPage.getQuantity() == 1,
					"​By default, the quantity should displayed as a 1!",
					"​By default, the quantity displayed as a 1!", 
					"By default, the quantity not displayed as a 1!", driver);

			Log.softAssertThat(pdpPage.verifyQtyDrp(),
					"The drop down should be displayed upto 10",
					"​The drop down displayed upto 10", 
					"The drop down not displayed upto 10", driver);

			//Step-23: Verify the Shipping and returns in PDP
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnAddToCart", "txtShippingAndReturnInfo", pdpPage), 
					"Shipping and Returns should be displayed below add to bag button", 
					"Shipping and Returns displayed below add to bag button", 
					"Shipping and Returns not displayed below add to bag button", driver);

			//Step-18: Verify the add to bag button in PDP
			//Add to bag button should be displayed below the inventory state.

			pdpPage.selectSize();
			pdpPage.selectColor();
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProdAvailStatus", "btnAddToCart", pdpPage), 
					"Add to bag button should be displayed below the inventory state.", 
					"Add to bag button displayed below the inventory state.", 
					"Add to bag button not displayed below the inventory state.", driver);

			if(Utils.isMobile() || Utils.isTablet()){
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnAddToCart", "lnkAddToWishList", pdpPage), 
						"Add to Bag button should be displayed above the add to wish list link.", 
						"Add to Bag button displayed above the add to wish list link.", 
						"Add to Bag button not displayed above the add to wish list link.", driver);
			
			}
			
			if(Utils.isDesktop()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodImageRecommendationDesktop","prodNameRecommendationDesktop","prodPriceRecommendationDesktop"), pdpPage), 
						"Attributes displayed for the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
						"Attributes displayed in the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
						"Attributes not displayed recommendations product tile", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodStdPriceRecommendationDesktop"),pdpPage) ||
						pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodSalePriceRecommendationDesktop"), pdpPage), 
						"Price Attributes displayed for the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price\"",
						"Price Attributes displayed in the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price.\"",
						"Price Attributes not displayed recommendations product tile", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodStdPriceRecommendationMobileTablet"),pdpPage) ||
						pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodSalePriceRecommendationMobileTablet"), pdpPage),
						"Price Attributes displayed for the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price\"",
						"Price Attributes displayed in the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price.\"",
						"Price Attributes not displayed recommendations product tile", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodImageRecommendationMobileTablet","prodNameRecommendationMobileTablet","prodPriceRecommendationMobileTablet"), pdpPage), 
						"Attributes displayed for the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
						"Attributes displayed in the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
						"Attributes not displayed recommendations product tile", driver);
			}
			
			Log.softAssertThat(pdpPage.verifyRecommendationHaveCurrentProduct(productID), 
					"The recommended products should not include the product being viewed",
					"The recommended products is not included the product being viewed",
					"The recommended products is included the product being viewed", driver);
			
			Dimension point = driver.manage().window().getSize();
			
			if(Utils.isDesktop()) {
				Log.event("Screen Resolution :: " + driver.manage().window().getSize());
				if(point.getWidth() >= 1200) {
					Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "sectionRecommendationDesktop", "sectionProductVariation", pdpPage), 
							"Product Recommendation pane should be displayed right side of the PDP in Vertical view.", 
							"Product Recommendation pane is displayed right side of the PDP in Vertical view.", 
							"Product Recommendation pane is not displayed right side of the PDP in Vertical view.", driver);
					
					Log.softAssertThat(pdpPage.elementLayer.verifyTextContains("txtRecommendationHeadingDesktop", "You may also like", pdpPage), 
							"\"You may also like \" label should be displayed as Headline", 
							"\"You may also like \" label is displayed as Headline", 
							"\"You may also like \" label is not displayed as Headline", driver);
				}
				
				/*Dimension setPoint = new Dimension(1024,768);
				
				driver.manage().window().setSize(setPoint);
				
				point = driver.manage().window().getSize();*/
				
				if(point.getWidth() >= 1024 && point.getWidth() < 1200) {
					
					Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divSocialIcons", "divDetailsContent", pdpPage), 
							"Product Recommendation pane should be displayed below the Social icons.", 
							"Product Recommendation pane is displayed below the Social icons.", 
							"Product Recommendation pane is not displayed below the Social icons.", driver);
					
					Log.softAssertThat(pdpPage.elementLayer.verifyTextContains("txtRecommendationHeadingTabMobile", "You may also like", pdpPage), 
							"\"You may also like \" label should be displayed as Headline", 
							"\"You may also like \" label is displayed as Headline", 
							"\"You may also like \" label is not displayed as Headline", driver);
					
					Log.failsoft("PXSFCC-5320 - Quick shop button not displaying in Recommendation");
				}
					
					
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divSocialIcons", "divDetailsContent", pdpPage), 
						"Product Recommendation pane should be displayed below the Social icons.", 
						"Product Recommendation pane is displayed below the Social icons.", 
						"Product Recommendation pane is not displayed below the Social icons.", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyTextContains("txtRecommendationHeadingTabMobile", "You may also like", pdpPage), 
						"\"You may also like \" label should be displayed as Headline", 
						"\"You may also like \" label is displayed as Headline", 
						"\"You may also like \" label is not displayed as Headline", driver);

				
			}
			
			String name = pdpPage.clickRecommendationPrdImageByIndex(0);
			
			String prdName = pdpPage.getProductName();
			
			Log.softAssertThat(prdName.toLowerCase().contains(name.toLowerCase()), 
					"When user click / tab on the product main image or product title, the system should navigate the user to the selected product detail page",
					"When user click / tab on the product main image or product title, the system is navigating to the selected product detail page",
					"When user click / tab on the product main image or product title, the system is not navigating to the selected product detail page", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_DROP3_C19728_1

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP3_C19728(String browser) throws Exception {
		Log.testCaseInfo();
		
		String productID = prdData.get("prd_variation");

		final WebDriver driver = WebDriverFactory.get(browser);

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.selectSize();
			pdpPage.selectColor();

			HashMap<String, String> productInfo = pdpPage.getProductInfo();
			Log.message(i++ + ". Product Information :: " + productInfo.toString(), driver);

			//Step-1: Verify Add to Bag overlay is opened.
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on 'Add To Bag' Button", driver);

			//19728 - Step-19: Verify the Add to bag successful validation in PDP (size of the item should be available in stock)
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("mdlMiniCartOverLay"), pdpPage), 
					"Add to Bag overlay should be displayed.",
					"Add to Bag overlay displayed!",
					"Add to Bag overlay not displayed.", driver);

			//Step-11: ER-4: If the customer does not have any items in their cart, then items in shopping bag= number of items added
			Log.event("--->Total Count in MCO ::" + pdpPage.getTotalItemCountInMCOverlay());
			Log.event("--->Total Count in MCart ::" + pdpPage.getTotalProductQty());
			Log.softAssertThat(pdpPage.getTotalItemCountInMCOverlay().equals(pdpPage.getTotalProductQty()), 
					"If the customer does not have any items in their cart, then items in shopping bag = number of items added", 
					"Items in shopping bag equal to number of items added", 
					"Items in shopping bag not equal to number of items added", driver);

			//Step-2: Verify the Item added message in add to bag overlay page
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("lblMiniCartOverlayTitle", "text-align", "center", pdpPage) && 
					pdpPage.elementLayer.verifyInsideElementAlligned("lblMiniCartOverlayTitle", "mdlMiniCartOverLay", "top", pdpPage),
					"Item Added Message should be displayed to the top of the Add to Bag overlay centrally aligned",
					"Item Added Message displayed to the top of the Add to Bag overlay centrally aligned",
					"Item Added Message not displayed to the top of the Add to Bag overlay or centrally aligned", driver);

			String mainImgSrc = pdpPage.getPrimaryImgSource();
			mainImgSrc = mainImgSrc.split("\\/")[mainImgSrc.split("\\/").length-1].split("\\?")[0];
			String miniCartOverLayImgSrc = pdpPage.getMiniCartOverLayImgSource();
			miniCartOverLayImgSrc = miniCartOverLayImgSrc.split("\\/")[miniCartOverLayImgSrc.split("\\/").length-1];

			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "titleShoppingBagPopUp", "imgPrimaryImageInMCOverLay", pdpPage),
					"Main image of the selected color variation should be displayed below the item added message.",
					"Main image of the selected color variation displayed below the item added message!",
					"Main image of the selected color variation not displayed below the item added message.", driver);

			//Step-5: Verify the product name in add to bag overlay page
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkProductNameInMCOverLay", "imgPrimaryImageInMCOverLay", pdpPage), 
					"Product name should be displayed right side of the product image.",
					"Product name displayed right side of the product image.",
					"Product name not displayed right side of the product image.", driver);

			//Step-6: Verify the attributes in add to bag overlay page
			HashMap<String, String> prdDataInMCOverlay = pdpPage.getProductDetailsFromMCOverLay();
			//List<String> dataOrderInMCOverlay = Arrays.asList("Color","Size","Qty","Avail","Price","Total");
			List<String> dataToBeVerified =  Arrays.asList("lblProductColorInMCOverLay","lblProductSizeInMCOverLay","lblProductQtyInMCOverLay","lblProductAvailInMCOverLay","lblProductPriceInMCOverLay","lblProductTotalInMCOverLay");
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfListOfElements(dataToBeVerified, pdpPage), 
					"Available variation attributes for selected product, to be displayed in the following order : COLOR,SIZE,QTY,AVAILABILITY,PRICE,TOTAL", 
					"Variation Attributes for selected product displayed as expected!", 
					"Variation Attributes for selected product not displayed as expected", driver);

			dataToBeVerified =  Arrays.asList("Color","Size","Qty");
			Log.reference(i++ + ". Product Details from PDP :: " + productInfo.toString());
			Log.reference(i++ + ". Product Details from MCO :: " + prdDataInMCOverlay.toString());
			Log.softAssertThat(Utils.compareTwoHashMap(productInfo, prdDataInMCOverlay, dataToBeVerified),
					"Product attributes should be exactly as selected by the User before clicking on the 'Add to Bag' button",
					"Product attributes are exactly as selected by the User before clicking on the 'Add to Bag' button",
					"Product attributes aren't exactly as selected by the User before clicking on the 'Add to Bag' button", driver);

			Log.event("--->>Product Price in PDP: " + productInfo.get("Price"));
			Log.event("--->>Product Price in PDP: " + prdDataInMCOverlay.get("Price"));
			Log.softAssertThat(prdDataInMCOverlay.get("Price").contains(productInfo.get("Price")), 
					"Price should be the single line item price of the item added.",
					"The single line item price of the item added is Displayed!",
					"Price not the single line item price of the item added.", driver);

			//Step-10: Verify the total in add to bag overlay page
			//Hold due to Total attribute not displayed in site. 

			//Step-11: Verify the Items in your cart in the add to bag overlay page
			if(Utils.isMobile()){
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblItemInUrBagMCOverlayMobile"), pdpPage),
						"Total no of line item's and total value of selected line items which are available in the cart should be displayed in 'Items In Your Bag'",
						"Number of Line items and total amount displayed as Expected!",
						"Number of Line items and total amount not displayed as Expected", driver);
			}else{
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("lblItemInUrBagMCOverlay"), pdpPage),
						"Total no of line item's and total value of selected line items which are available in the cart should be displayed in 'Items In Your Bag'",
						"Number of Line items and total amount displayed as Expected!",
						"Number of Line items and total amount not displayed as Expected", driver);
			}

			if(Utils.isMobile()){
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblProductPriceInMCOverLay", "lblItemInUrBagMCOverlayMobile", pdpPage), 
						"Items in your bag should displayed below the Price section in Add to bag overlay.",
						"Items in your bag displayed below the Price section in Add to bag overlay.",
						"Items in your bag not displayed below the Price section in Add to bag overlay.", driver);
			}else{
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblProductPriceInMCOverLay", "lblItemInUrBagMCOverlay", pdpPage), 
						"Items in your bag should displayed below the Price section in Add to bag overlay.",
						"Items in your bag displayed below the Price section in Add to bag overlay.",
						"Items in your bag not displayed below the Price section in Add to bag overlay.", driver);
			}

			String noOfItemsInCart = pdpPage.headers.getMiniCartCount();
			String itemNoInMCOverlay = pdpPage.getTotalItemCountInMCOverlay();
			String costInMCFlyt = pdpPage.minicart.getTotalCost();
			String costInMCOverlay = pdpPage.getTotalCostInMCOverlay();
			Log.event("No Of. Item in cart :: " + noOfItemsInCart);
			Log.event("No Of. Item in MCO  :: " + itemNoInMCOverlay);
			Log.event("Item Cost in Cart   :: " + costInMCFlyt);
			Log.event("Item Cost in MCOVer :: " + costInMCOverlay);

			Log.softAssertThat(itemNoInMCOverlay.equals(noOfItemsInCart) && costInMCFlyt.equals(costInMCOverlay), 
					"Items in your bag should display the total number of line items in the cart and the total cart value.", 
					"Items in your bag display the total number of line items in the cart and the total cart value.", 
					"Items in your bag not displaying the total number of line items in the cart and the total cart value.", driver);
			//ER-4 - covered in Step-1

			String totalQtyBySumOfAllPrd = pdpPage.getSumOfAllProductQtyInMCOverlay();
			Log.softAssertThat(itemNoInMCOverlay.equals(totalQtyBySumOfAllPrd), 
					"If the customer has items in their cart, then the system sums the number of items existing & added",
					"The system sums the number of items existing & added",
					"the system did not sums the number of items existing & added", driver);

			Log.softAssertThat(pdpPage.getItemInYouBagMCOverlay().toLowerCase().contains("items in your bag") ||
					pdpPage.getItemInYouBagMCOverlay().toLowerCase().contains("item in your bag") , 
					"Items should displayed in the following format : [# of cart items] Item(s) In Your Bag: $[cart total]", 
					"Items displayed in expected format!", 
					"Items not displayed in expected format.", driver);

			//Step-12: Verify the checkout in add to bag overlay page
			ShoppingBagPage cartPage = pdpPage.clickOnCheckoutInMCOverlay();
			Log.softAssertThat(cartPage.getPageLoadStatus(), 
					"While clicking on 'Checkout' button,page should be redirected to Cart page.",
					"Page redirected to Cart Page!",
					"Page not redirected to Cart Page.", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_DROP3_C19728_2

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP3_C19728(String browser) throws Exception {

		//HashMap<String, String> testData = new HashMap<String, String>();//TestDataExtractor.initTestData(workbookName, sheetName);

		//String brand = testData.get("Brand");
		String productID = prdData.get("prd_variation");//testData.get("SearchKey").split("\\|")[0].split("_")[0];

		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo("ATB Overlay close functionality verifications");
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);

			pdpPage.selectColor();
			pdpPage.selectSize();
			Log.message(i++ + ". Size & Color variation selected.", driver);

			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add To bag buttons.");

			//Step-3: Verify the Close Modal Options​ in the add to bag overlay page
			Log.message(i++ + ". By clicking in three ways, Minicart Overlay should be closed.", driver);
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Clicked on Close button in Minicart Overlay.", driver);

			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("btnCloseMiniCartOverLay"), pdpPage),
					"Mini cart Overlay should be closed.",
					"Mini cart Overlay closed successfully!",
					"Mini cart Overlay not closed.", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on 'Add To Bag' Button", driver);	

			BrowserActions.clickOnElementX(driver, "btnContinueShoppingInMCOverlay", pdpPage, "Continue Button");
			Log.message(i++ + ". Clicked on Continue Shopping in Mini cart Overlay!", driver);

			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("btnCloseMiniCartOverLay"), pdpPage),
					"Mini cart Overlay should be closed.",
					"Mini cart Overlay closed successfully!",
					"Mini cart Overlay not closed.", driver);

			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on 'Add To Bag' Button", driver);	

			pdpPage.clickOnContinueShoppingInMCOverlay();
			Log.message(i++ + ". Clicked on Continue Shopping in Mini cart Overlay!", driver);

			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("btnCloseMiniCartOverLay"), pdpPage),
					"Mini cart Overlay should be closed.",
					"Mini cart Overlay closed successfully!",
					"Mini cart Overlay not closed.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("divSocialIcons"), pdpPage),
					"The Social Icons should be displayed!",
					"The Social Icons are displayed!",
					"The Social Icons are not displayed!", driver);

			Log.softAssertThat(pdpPage.verifyFacebookShare(),
					"Facebook page should be opened!",
					"Facebook page is opened!",
					"Facebook page is not opened!", driver);

			Log.softAssertThat(pdpPage.verifyPinterestShare(),
					"Pinterest page should be opened!",
					"Pinterest page is opened!",
					"Pinterest page is not opened!", driver);

			Log.softAssertThat(pdpPage.verifyTwitterShare(),
					"Twitter page should be opened!",
					"Twitter page is opened!",
					"Twitter page is not opened!", driver);
			
			WishlistLoginPage wlLoginPage =  (WishlistLoginPage) pdpPage.addToWishlist();
			
			Log.softAssertThat(wlLoginPage.elementLayer.VerifyElementDisplayed(Arrays.asList("divSearchWishlist"), wlLoginPage),
					"If user not logged in,page should be navigated to the login page to login.",
					"If user not logged in,page should be navigated to the login page to login!",
					"If user not logged in,page should not be navigated to the login page to login.", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally

	}// TC_FBB_DROP3_C19728_3

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP3_C19728(String browser) throws Exception {

		String productID = prdData.get("sku_stock<5_variation").split("\\|")[0];
		String size = prdData.get("sku_stock<5_variation").split("\\|")[2];
		String color = prdData.get("sku_stock<5_variation").split("\\|")[1];

		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");

		//new Products(driver).get().setInventory(productID, "inventory_FullBeautyBrands", "4");
		Log.testCaseInfo("Inventory and Wishlist verifications");
		int i = 1;
		try {

			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.navigateToPDP(productID);
			pdpPage.selectColor(color);
			pdpPage.selectSize(size);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.selectQty("2"); //Quantify of the product should be greater than Product ATS in inventory
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("inventoryState"), pdpPage), 
					"Based on the quantity selection inventory message should update.",
					"Based on the quantity selection inventory message updated!",
					"Based on the quantity selection inventory message not updated.", driver);

			if(pdpPage.getQuantityListSize() < 10) {
				Log.softAssertThat(pdpPage.getQuantityListSize() == pdpPage.getStockValuefrmInventoryMsg(), 
						"When stock availability is less than 10, it should display Quantity values upto inventory value.", 
						"Quantity drop down displays values upto inventory value only.", 
						"Quantity drop down displays more than inventory value.", driver);
			}

			Object obj = pdpPage.addToWishlist();
			Log.message(i++ + ". Clicked on Add To WishList link.", driver);

			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("divSearchWishlist"), obj), 
					"Page should be navigated to the login page to login or create an account",
					"User navigated to Login Page!",
					"User not navigated to Login Page.", driver);

			driver.get(Utils.getWebSite());
			homePage = new HomePage(driver).get();
			Log.message(i++ + ". Navigated to Home Page.",driver);

			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Logged in as :: " + username);

			pdpPage = myAcc.headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);

			pdpPage.selectSize(size);
			Log.message(i++ + ". Selected Available Size : " + size, driver);

			pdpPage.selectColor(color);
			Log.message(i++ + ". Selected Available Color : " + color, driver);
			
			Log.message(i++ + ". Inventory message covered in C19731", driver);
			
			if(Utils.isTablet() || Utils.isMobile()) {
				
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnAddToBag", "lnkAddToWishList", pdpPage) ||
						pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkAddToWishList", "lnkShippingExpand", pdpPage), 
						"Add to Wishlist link should be in between Add To Bag and Shipping & Returns drop down.",
						"Add to Wishlist link is in between Add To Bag and Shipping & Returns drop down.",
						"Add to Wishlist link is not in between Add To Bag and Shipping & Returns drop down.", driver);
			}
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnAddToBag", "lnkShippingExpand", pdpPage), 
					"Shipping and Returns should be displayed below Add To Bag button.",
					"Shipping and Returns is displayed below Add To Bag button.",
					"Shipping and Returns is not displayed below Add To Bag button.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.VerifyElementDisplayed(Arrays.asList("lnkShippingSectionPrdInfo"), pdpPage), 
					"This Shipping and Returns section tells the user what the shipping \"rules\".",
					"This Shipping and Returns section tells the user what the shipping \"rules\".",
					"This Shipping and Returns section not displaying the shipping \"rules\".", driver);
			
			WishListPage wlPage = (WishListPage)pdpPage.addToWishlist();
			Log.message(i++ + ". Clicked on Add To WishList link.", driver);
			
			Log.softAssertThat(wlPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), wlPage), 
					"Item should be successfully able to add item into the wishlist account",
					"Item successfully added into the wishlist account",
					"Item successfully not added into the wishlist account", driver);

			Log.testCaseResult();
			
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			GlobalNavigation.RemoveWishListProducts(driver);
			driver.quit();
		} // finally

	}// TC_FBB_DROP3_C19728_4

}// search
