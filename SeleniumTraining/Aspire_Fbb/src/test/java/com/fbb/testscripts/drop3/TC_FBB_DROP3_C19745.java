package com.fbb.testscripts.drop3;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19745 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	@SuppressWarnings("unused")
	private static String runPlatfrm = Utils.getRunPlatForm();
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "low", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19745(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = prdData.get("prd_monogram");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			String browsername=Utils.getRunBrowser(driver);
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			String size = pdpPage.selectSize();
			Log.message("--->>>Selected size :: " + size);
	
			String color = pdpPage.selectColor();
			Log.message("--->>>Selected Color :: " + color);
	
			pdpPage.clickOnMonogrammingCheckbox("enable");
			Log.message(i++ + ". The Monogramming checkbox is checked!", driver);
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + " Clicked add to bag!", driver);
	
			//1. Verify the components in the Product Option Error Handling page
			List<String> elementsToBeVerified2 = null;
			elementsToBeVerified2 = Arrays.asList("txtMonogrammingTitle", "divMonogramLocation","divMonogramFont","divMonogramColor");
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtMonogrammingTitle"), pdpPage),
					"The Monogramming Option Name and Field Texts(Dropdowns) should be displayed!",
					"The Monogramming Option Name and Field Texts(Dropdowns) are displayed!",
					"The Monogramming Option Name and Field Texts(Dropdowns) are not displayed!", driver);
	
			elementsToBeVerified2=Arrays.asList("monogramingTextField");
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageListElementDisplayed(elementsToBeVerified2, pdpPage),
					"Texts fields should be displayed!",
					"Texts fields are displayed!",
					"Texts fields are not displayed!", driver);
			
			String cssColor = "rgb(189, 3, 4)";
			if(browsername.equals("MicrosoftEdge"))	{
				if(Utils.getCurrentBrand().equals(Brand.rm)) {
					cssColor = "rgb(189, 3, 4)";
				} else if(Utils.getCurrentBrand().equals(Brand.ww)) {
					cssColor = "rgba(255, 58, 81)";
				} else if(Utils.getCurrentBrand().equals(Brand.ks)) {
					cssColor = "rgba(137, 22, 53)";
				}
			} else {
				if(Utils.getCurrentBrand().equals(Brand.rm)) {
					cssColor = "rgba(189, 3, 4, 1)";
				} else if(Utils.getCurrentBrand().equals(Brand.ww)) {
					cssColor = "rgba(255, 58, 81, 1)";
				} else if(Utils.getCurrentBrand().equals(Brand.ks)) {
					cssColor = "rgba(137, 22, 53, 1)";
				}
			}
			
			Log.softAssertThat(pdpPage.validatePlaceholderTextColorforMonogrammingTextfield() &&
					pdpPage.errorMsgVerificationInMonogramFields(cssColor),			 
						"It should display error messaging in red Color for the empty or incomplete fields", 
						"Error messaging is displayed in red Color", 
						"Error messaging is not displayed in red color!", driver);
			
			//2. Verify the Option name in the error message
			if(browsername.equals("MicrosoftEdge"))
			{		
				Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("txtMonogrammingTitle", "color", "rgb(231, 0, 0)", pdpPage),			 
						"Option name should be displayed in red color.", 
						"Option name is displayed in red Color", 
						"Option name is not displayed in red color!", driver); 
			}
			else
			{
				Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("txtMonogrammingTitle", "color", "rgba(231, 0, 0, 1)", pdpPage),			 
						"Option name should be displayed in red color.", 
						"Option name is displayed in red Color", 
						"Option name is not displayed in red color!", driver); 
			}
	
			//3. Verify the Field text in the error message 
			//When the "Drop Dropdown" isn't selected,it should display the error message: â€œPlease select {option name}â€�	
			elementsToBeVerified2=Arrays.asList("monogramFontoptions");
			List<String> elementsToBeVerified3=Arrays.asList("monogramLocationoptions");
			List<String> elementsToBeVerified4=Arrays.asList("monogramColoroptions");
			
			if(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("drpColorSingleSelect"), pdpPage)) {
				Log.reference("Only one color is available, So color error message cannot be verified");
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyListElementTextEqualTo(elementsToBeVerified4,0,"Select Color",pdpPage),
						"'Select Color' message should be displayed. ", 
						"'Select Color' message is displayed. ", 
						"'Select Color' message is not displayed. ", driver); 
			}
			if(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("drpLocationSingleSelect"), pdpPage)) {
				Log.reference("Only one location is available, So location error message cannot be verified");
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyListElementTextEqualTo(elementsToBeVerified3,0,"Select Location",pdpPage),
						"'Select Location' message should be displayed. ", 
						"'Select Location' message is displayed. ", 
						"'Select Location' message is not displayed. ", driver); 
			}
			if(pdpPage.elementLayer.VerifyPageElementDisplayed(Arrays.asList("drpFontSingleSelect"), pdpPage)) {
				Log.reference("Only one font is available, So font error message cannot be verified");
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyListElementTextEqualTo(elementsToBeVerified2,0,"Select Font",pdpPage),
						"'Select Font' message should be displayed. ", 
						"'Select Font' message is displayed. ", 
						"'Select Font' message is not displayed. ", driver);
			}
			
			Log.softAssertThat(pdpPage.validatePlaceholderTextforMonogrammingTextfield(),			 
					"'Enter Text' message should be displayed. ", 
					"'Enter Text' message is displayed. ", 
					"'Enter Text' message is not displayed. ", driver);
	
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase();
			driver.quit();
		} // finally
	
	}


}// search
