package com.Testscripts;

import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.Pageobject.FlipkartHomePagePOM;
import com.Utilityfiles.Basetest;

public class FlipkartHomePageTest extends Basetest {

FlipkartHomePagePOM fpom;

@BeforeClass
public void Loadurl() throws InterruptedException {
	
//driver.navigate().to("https://www.naukri.com/");
driver.navigate().to("https://www.makemytrip.com/");
Thread.sleep(5000);
	
}
	
//@Test
//public void FK01_Flipkart_clicklogin() throws InterruptedException {
//	
//	System.out.println("test case 0ne");
//	Thread.sleep(5000);
//	boolean loginoverlay=fpom.flipkart_loginoverlay.isDisplayed();
//	System.out.println(loginoverlay);
//	if (loginoverlay==true) {
//		System.out.println("Entred in if loop for closing");
//		fpom.flipkart_signuplinkClose.click();
//	}
//	
//	fpom.flipkart_signuplink.click();
//	System.out.println("Clicked on login button");
//	Thread.sleep(5000);
//
//	
//}

@Test
public void FK02_Flipkart_Login() throws InterruptedException {
	
	System.out.println("entering Test case two");
	String parenthandle=driver.getWindowHandle();
	System.out.println("Parent Handle:-" + parenthandle);
	Set<String> allhandles=driver.getWindowHandles();
	System.out.println("All handles:-"+ allhandles);
	for (String s1:allhandles) {

		if (!s1.equals(parenthandle)) {
			driver.switchTo().window(s1);
			driver.close();
		}
		
	}

	driver.switchTo().window(parenthandle);
	Thread.sleep(3000);
	
	fpom.Naukari_companiesbutton.click();
	
//	boolean logintext=fpom.flipkart_logintext.isDisplayed();
//	System.out.println("Login Text:-"+ logintext);
//	fpom.Flipkart_login();
	
}

}
