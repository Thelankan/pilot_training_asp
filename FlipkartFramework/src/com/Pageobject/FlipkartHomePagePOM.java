package com.Pageobject;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;


public class FlipkartHomePagePOM extends LoadableComponent<FlipkartHomePagePOM> {

WebDriver driver;

@FindBy(linkText="Login & Signup")
public WebElement flipkart_signuplink;

@FindBy(linkText="//form[@autocomplete='on']")
public WebElement flipkart_loginoverlay;

@FindBy(linkText="//button[@class='_2AkmmA _29YdH8']")
public WebElement flipkart_signuplinkClose;

@FindBy(xpath="//span[@class='_1hgiYz']/span[contains(text(),'Login')]")
public WebElement flipkart_logintext;

@FindBy(xpath="//input[@class='_2zrpKA']")
public WebElement flipkart_loginusername;

@FindBy(xpath="//input[@class='_2zrpKA _3v41xv']")
public WebElement flipkart_loginpassword;

@FindBy(xpath="//button[@class='_2AkmmA _1LctnI _7UHT_c']")
public WebElement flipkart_loginbutton;

@FindBy(xpath="//div[contains(text(),'Companies')]")
public WebElement Naukari_companiesbutton;



public FlipkartHomePagePOM(WebDriver driver) {
	this.driver=driver;
	PageFactory.initElements(driver, this);
}

public void Flipkart_login() {
	
	flipkart_signuplink.click();
	flipkart_loginusername.sendKeys("vinaylanka066@gmail.com");
	flipkart_loginpassword.sendKeys("Flipkart");
	flipkart_loginbutton.click();
	
}

protected void isLoaded() throws Error {
	// TODO Auto-generated method stub
	
}

protected void load() {
	//assertTrue(driver.getTitle().contains(title));
	
}

}
