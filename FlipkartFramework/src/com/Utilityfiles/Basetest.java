package com.Utilityfiles;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;

public class Basetest {

public WebDriver driver;
	
@Parameters({"browser","driverpath"})	
@BeforeTest
public void Beforetest(String browsername,String driverpath) throws InterruptedException {
	
System.out.println("Browsername"+browsername);

if (browsername.equals("firefox")) {
	System.setProperty("webdriver.gecko.driver",driverpath);
	driver=new FirefoxDriver();
	driver.manage().window().setSize(new Dimension(1920, 1080));
	Thread.sleep(3000);
}

else if (browsername.equals("chrome")) {
	System.setProperty("webdriver.chrome.driver",driverpath);
	driver=new ChromeDriver();
	driver.manage().window().maximize();
}

else if (browsername.equals("ie")) {
	System.setProperty("webdriver.ie.driver",driverpath);
	driver=new InternetExplorerDriver();
	driver.manage().window().maximize();
}
		
}
	
@BeforeClass
public void Loadurl() throws InterruptedException {
	
driver.get("https://www.flipkart.com/");
Thread.sleep(3000);
	
}

@BeforeMethod
public void Beforemethod() {
	
}

@AfterMethod
public void Aftermethod() {
	
}

@AfterClass
public void AfterClass() {
	
}

@AfterTest
public void AfterTest() {
	//driver.quit();
}
	



}
